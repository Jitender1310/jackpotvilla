var theme = {
    homeslider: function () {
        new Swiper('.home-slider', {
            loop: true,
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
            pagination: {
                el: '.slider__pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.slider__button-next',
                prevEl: '.slider__button-prev',
            }
        });
    },
    gameslider: function () {
        new Swiper('.game_slider', {
            loop: true,
            pagination: {
                el: '.slider__pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.slider__button-next',
                prevEl: '.slider__button-prev',
            }
        });
    },
    testimonialslider: function () {
        new Swiper('.testimonial-slider', {
            loop: true,
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
            navigation: {
                nextEl: '.testimonial-slider-right',
                prevEl: '.testimonial-slider-left',
            }
        });
    },
    promotionsslider: function () {
        new Swiper('.promotions-slider', {
            loop: true,
            slidesPerView: 1,
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
            pagination: {
                el: '.promotions-slider-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.promotions-slider-right',
                prevEl: '.promotions-slider-left',
            }
        });
    },
    screenshotsslider: function () {
        new Swiper('.screenshots-slider', {
            loop: true,
            slidesPerView: 3,
            autoplay: {
                delay: 2000,
                disableOnInteraction: false,
            },
            pagination: {
                el: '.screenshots-slider-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.screenshots-slider-right',
                prevEl: '.screenshots-slider-left',
            }
        });
    },
    bannersslider: function () {
        new Swiper('.banners-slider', {
            loop: true,
            slidesPerView: 1,
            autoplay: {
                delay: 2000,
                disableOnInteraction: false,
            },
            pagination: {
                el: '.swiper-pagination',
              },
        });
    }
}
setTimeout(function () {
    theme.homeslider();
    theme.testimonialslider();
    theme.screenshotsslider();
    theme.promotionsslider();
    theme.bannersslider();
}, 2000);
$(".anime").each(function () {
    var $demo_class = $(this);
    var $demo_duration = $(this).data('dur');
    setInterval(function () {
        $demo_class.toggleClass('move');
    }, $demo_duration);
})
var header = document.querySelector(".header-parent");
var navigation = document.querySelector(".navigation-wrapper");
window.onload = function () {
    header.style.height = header.childNodes[0].clientHeight + "px";
    // 
    navigation.style.height = navigation.childNodes[0].clientHeight + "px";
}
window.onscroll = function () {
    if (window.pageYOffset >= (navigation.offsetTop - header.childNodes[0].clientHeight)) {
        // navigation.childNodes[0].style.position = "fixed";
    //     navigation.childNodes[0].style.top = header.clientHeight + "px";
    //     navigation.childNodes[0].classList.add = "onactive";
    // } else {
    //     navigation.childNodes[0].style.position = "absolute";
        // navigation.childNodes[0].style.top = "0px";
    }
}

    
$(document).ready(function() {
    // Swiper: Slider
        new Swiper('.swiper-container.flx_boxes', {
            loop: true,
            navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
            slidesPerView: 5,
            autoplay:{
                delay: 3000,
                disableOnInteraction: false
            },
          // paginationClickable: true,
            // spaceBetween: 20,
            breakpoints: {
                1920: {
                    slidesPerView: 5,
                    // slidesPerColumn: 2,
                    // spaceBetween: 30
                },
                1028: {
                    slidesPerView: 3,
                    // slidesPerColumn: 2,
                    // spaceBetween: 30
                }
            }
  
        });
    });
  
    $(document).ready(function() {
    // Swiper: Slider
        new Swiper('.swiper-container.jackpot_slot', {
            
            navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
            slidesPerView: 5,
          //   autoplay: true,
          pagination: {
              el: ".swiper-pagination",
              clickable: true
          },
          watchSlidesProgress: true,
          watchSlidesVisibility: true,
          paginationClickable: true,
          loop: true,
          autoplay: {
          delay: 3000,
          disableOnInteraction: false
      },
            // spaceBetween: 20,
            breakpoints: {
                1920: {
                    slidesPerView: 5,
                    // spaceBetween: 30
                },
                1028: {
                    slidesPerView: 3,
                    // spaceBetween: 30
                }
            }
  
        });
        
    });


$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1000,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination:true,
        transitionStyle:"backSlide",
        autoPlay:true
    });
});