webServices.service('registerService', function ($http) {
    return {
        register: function (userObj) {
            console.log(userObj);
            var postData = $.param(userObj);
            var url = apiBaseUrl + "register";
            return $http.post(url, postData, config);
        },
        countries: function(){
            var url = apiBaseUrl + "country";
            return $http.get(url);
        }
    };
});
