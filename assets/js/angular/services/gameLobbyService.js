webServices.service('gameLobbyService', function ($http) {
    return {
        get_lobby_section: function () {
            var obj = {};//initialing empty obj
            var postData = $.param(obj);
            var url = apiBaseUrl + "game_lobby";
            return $http.post(url, postData, config);
        },
        join_tournament: function (cloned_tournaments_id) {
            var postData = "tournament_id=" + cloned_tournaments_id;
            var url = apiBaseUrl + "tournaments/join";
            return $http.post(url, postData, config);
        },
        game_progress: function (cloned_tournaments_id) {
            var postData = "cloned_tournaments_id=" + cloned_tournaments_id;
            var url = apiBaseUrl + "tournament_progress?";
            return $http.post(url, postData, config);
        },
        unJoin: function (cloned_tournaments_id) {
            var postData = "tournament_id=" + cloned_tournaments_id;
            var url = apiBaseUrl + "tournaments/unjoin";
            return $http.post(url, postData, config);
        },
        get_tournament_info: function (cloned_tournaments_id) {
            var postData = "tournament_id=" + cloned_tournaments_id;
            var url = apiBaseUrl + "tournaments/info?";
            return $http.post(url, postData, config);
        }
    };
});