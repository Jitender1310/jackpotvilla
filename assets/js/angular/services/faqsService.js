webServices.service('faqsService', function ($http) {
    return {
        get_faqs_list: function () {
            var url = apiBaseUrl + "faqs";
            return $http.get(url);
        }
    };
});