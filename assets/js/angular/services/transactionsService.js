webServices.service('transactionsService', function ($http) {
    return {
        get_transactions: function ( filterObj ) {
            var url = apiBaseUrl + "transactions";
            var getParams = $.param(filterObj);
            return $http.get(url + '?' + getParams);
        }
    };
});