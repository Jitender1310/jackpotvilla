webServices.service('forgotPasswordService', function ($http) {
    return {
        submit_request: function (userObj) {
            var postData = $.param(userObj);
            var url = apiBaseUrl + "forgot_password";
            return $http.post(url, postData, config);
        },
        reset_password_with_code  : function(data){
            var postData = $.param(data);
            var url = apiBaseUrl + "reset_password";
            return $http.post(url, postData, config);
        }
    };
});