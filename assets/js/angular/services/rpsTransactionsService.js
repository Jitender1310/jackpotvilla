webServices.service('rpsTransactionsService', function ($http) {
    return {
        get_rpsTransactions: function (filterObj) {
            var url = apiBaseUrl + "rps_transactions";
            var getParams = $.param(filterObj);
            return $http.get(url + '?' + getParams);
        }
    };
});