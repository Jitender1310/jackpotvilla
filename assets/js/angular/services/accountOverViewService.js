webServices.service('accountOverViewService', function ($http) {
    return {
        get: function () {
            var url = apiBaseUrl + "account_overview";
            return $http.get(url);
        }
    };
});