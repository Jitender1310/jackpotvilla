webServices.service('preferencesService', function ($http) {
    return {
        get: function () {
            var url = apiBaseUrl + "preferences";
            return $http.get(url);
        },
        update: function (userObj) {
            var postData = $.param(userObj);
            var url = apiBaseUrl + "preferences/update";
            return $http.post(url, postData, config);
        }
    };
});