webServices.service('withdrawService', function ($http) {
    return {
        getRequestsList: (data) => {
            var postData = $.param(data);
            var url = apiBaseUrl + "withdraw";
            return $http.post(url, postData, config);
        },
        checkJokerStatus: () => {
            var url = apiBaseUrl + "withdraw/check_joker_status";
            return $http.get(url);
        },
        validateWithdrawRequest: (obj) => {
            var postData = $.param(obj);
            var url = apiBaseUrl + "withdraw/validate_request";
            return $http.post(url, postData, config);
        },
        sendWithDrawlRequest: (obj) => {
            var postData = $.param(obj);
            var url = apiBaseUrl + "withdraw/send_withdrawl_request";
            return $http.post(url, postData, config);
        },
        getIfscDetails: (ifsc_code) => {
            var url = apiBaseUrl + "ifsc_details?ifsc_code=" + ifsc_code;
            return $http.get(url);
        },
        cancelWithdrawalRequest: (obj) => {
            var postData = $.param(obj);
            var url = apiBaseUrl + "withdraw/cancel_request";
            return $http.post(url, postData, config);
        }
    };
});