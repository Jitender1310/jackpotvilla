webServices.service('paymentHistoryService', function ($http) {
    return {
        get_history: function (data) {
            var postData = $.param(data);
            var url = apiBaseUrl + "payment_history";
            return $http.post(url, postData, config);
        }
    };
});