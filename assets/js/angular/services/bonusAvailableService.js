webServices.service('bonusAvailableService', function($http){
    return {
        get: function(){
            var url = apiBaseUrl + "bonus_coupons";
            return $http.get(url);
        },
        validate_bonus_code:function(obj){
            var postData = $.param(obj);
            var url = apiBaseUrl + "deposit_money_request/validate_bonus_code";
            return $http.post(url, postData, config);
        }
    };
});