webServices.service('bringAFriendService', function ($http) {
    return {
        submitForm: function (data) {
            var url = apiBaseUrl + "bring_a_friend/send_invitations";
            return $http.post(url, data, config);
        }
    };
});