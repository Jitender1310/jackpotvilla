webServices.service('bonusHistoryService', function ($http) {
    return {
        get_bonus_history: function (data) {
            var postData = $.param(data);
            var url = apiBaseUrl + "bonus_history";
            return $http.post(url, postData, config);
        }
    };
});