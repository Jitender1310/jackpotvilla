webServices.service('changePasswordService', function ($http) {
    return {
        update_password: function (userObj) {
            var postData = $.param(userObj);
            var url = apiBaseUrl + "update_password";
            return $http.post(url, postData, config);
        }
    };
});