webServices.service('levelInfoService', function ($http) {
    return {
        get_expertize_level_details : function(){
            var url = apiBaseUrl + "level_info";
            return $http.get(url);
        }
    };
});