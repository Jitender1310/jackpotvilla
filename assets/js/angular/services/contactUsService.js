webServices.service('contactUsService', function ($http) {
    return {
        submit_form: function (data) {
            var postData = $.param(data);
            var url = apiBaseUrl + "contact_us";
            return $http.post(url, postData, config);
        }
    };
});