webServices.service('casinoGamesService', function ($http) {
    return {
        all: function(){
            var url = apiBaseUrl + "casino_games/all";
            return $http.get(url);
        },
        loadGame: function(game_id, play_token){
            var url = apiBaseUrl + "casino_games/gameUrl?id=" + game_id;
            var headers = {
                token : play_token
            };
            return $http.get(url, {headers: headers});
        }
    };
});
