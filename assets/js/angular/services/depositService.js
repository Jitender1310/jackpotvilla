webServices.service('depositService', function ($http) {
    return {
        create_deposit_request : function(obj){
            var postData = $.param(obj);
            var url = apiBaseUrl + "deposit_money_request/create";
            return $http.post(url, postData, config);
        }
    };
});