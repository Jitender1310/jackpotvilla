webServices.service('profileService', function ($http) {
    return {
        get_profile: function () {
            var url = apiBaseUrl + "profile";
            return $http.get(url);
        },
        profile_update: function (userObj) {
            var postData = $.param(userObj);
            var url = apiBaseUrl + "profile/update";
            return $http.post(url, postData, config);
        },
        update_new_email: function (userObj) {
            var postData = $.param(userObj);
            var url = apiBaseUrl + "profile/update_email";
            return $http.post(url, postData, config);
        },
        update_new_mobile: function (userObj) {
            var postData = $.param(userObj);
            var url = apiBaseUrl + "profile/update_mobile";
            return $http.post(url, postData, config);
        },
        send_email_verification_code: function () {
            var url = apiBaseUrl + "profile/send_email_verification_code";
            return $http.get(url);
        },
        verify_otp: function (userObj) {
            var postData = $.param(userObj);
            var url = apiBaseUrl + "profile/verify_otp";
            return $http.post(url, postData, config);
        },
        resend_otp: function () {
            var url = apiBaseUrl + "profile/resend_otp";
            return $http.get(url);
        },
        update_kyc: function (fd) {
            //var postData = $.param(obj);
            var url = apiBaseUrl + "profile/update_kyc";
            return $http.post(url, fd, fileConfig);
        },
        get_avatars:function(){
            var url = apiBaseUrl + "default_avatars";
            return $http.get(url);
        },
        set_avatar:function(obj){
            var postData = $.param(obj);
            var url = apiBaseUrl + "profile/update_avatar";
            return $http.post(url, postData, config);
        }
    };
});