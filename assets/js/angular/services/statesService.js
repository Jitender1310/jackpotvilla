webServices.service('statesService', function ($http) {
    return {
        get_states: function () {
            var url = apiBaseUrl + "states";
            return $http.get(url);
        }
    };
});