webServices.service('chipsService', function ($http) {
    return {
        get_chips: function () {
            var url = apiBaseUrl + "profile";
            return $http.get(url);
        },
        get_notifications: function() {
            var url = apiBaseUrl + "offer_notifications";
            return $http.get(url);
        },
        reset_chips: function () {
            var url = apiBaseUrl + "reset_funchips";
            return $http.post(url, {}, config);
        },
        redeem_rps: function (redeem_points) {
            var postData = $.param({"redeem_points": redeem_points});
            var url = apiBaseUrl + "redeem_rps";
            return $http.post(url, postData, config);
        }
    };
});