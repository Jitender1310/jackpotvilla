webServices.service('loginService', function ($http) {
    return {
        verify_login: function (userObj) {
            var postData = $.param(userObj);
            var url = apiBaseUrl + "login";
            return $http.post(url, postData, config);
        },
        social_login: function (userObj) {
            var postData = $.param(userObj);
            var url = apiBaseUrl + "social_login";
            return $http.post(url, postData, config);
        }
    };
});