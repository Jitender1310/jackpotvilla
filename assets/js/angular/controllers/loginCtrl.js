$(function () {
    $("#loginForm").validate({
        rules: {
            username: {
                required: true,
                nowhitespace: true
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 32,
                nowhitespace: true
            }
        },
        messages: {
            username: {
                required: "Please enter username",
                email: "Please enter valid registered email",
                nowhitespace: true
            },
            password: {
                required: "Please enter password",
                minlength: "Invalid Password",
                maxlength: "Invalid Password",
                nowhitespace: "Invalid Password"
            }
        },errorPlacement: function (error, element) {
    // error.insertAfter($(element).parents('.iconbox')); 
}
    });
});
app.controller("loginCtrl", function ($scope, loginService, loaderService) {
    $scope.user = {
        username: "",
        password: ""
    };
/*****-----GOOGLE SIGNOUT START---------********/

    google_signOut = function () {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function(){
            
        });
    }
/*****-----GOOGLE SIGNOUT END---------********/
});

var googleUser = {};
window.addEventListener('load',function() {
//    gapi.load('auth2', function() {
//        auth2 = gapi.auth2.init({
//            client_id: '1016768204382-42tuasfoh21s0vdpoucs94mraqra6npl.apps.googleusercontent.com',
//            cookiepolicy: 'single_host_origin',
//        });
//
//        auth2.attachClickHandler( document.querySelector("#googleLoginBtn"), {}, function(googleUser){
//            var scope = angular.element( $('#social_login_div') ).scope();
//            scope.$apply(function(){
//               scope.google_signin(googleUser);
//            });
//        });
//        auth2.attachClickHandler( document.querySelector("#googleLoginBtn2"), {}, function(googleUser){
//            var scope = angular.element( $('#social_login_div') ).scope();
//            scope.$apply(function(){
//               scope.google_signin(googleUser);
//            });
//        });
//    });
});