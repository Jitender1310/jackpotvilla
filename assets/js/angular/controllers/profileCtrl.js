jQuery.validator.addMethod("nameregex", function (value, element) {
    return this.optional(element) || /^[a-z\s]+$/i.test(value);
}, "Only alphabetical characters");
$(function () {
    $("#profileForm").validate({
        errorPlacement: function (error, element) {
            error.appendTo(element.closest(".form-group"));
        },
        rules: {
            firstname: {
                required: true,
                nameregex: true,
            },
            lastname: {
                required: true,
                nameregex: true,
            },
            date_of_birth: {
                required: true
            },
            gender: {
                required: true
            },
            address_line_1: {
                required: true
            },
            states_id: {
                required: true
            },
            city: {
                required: true,
                nameregex: true,
            },
            pin_code: {
                required: true
            }
        },
        messages: {
            firstname: {
                required: "Please enter your first name.",
                nameregex: "Firstname should be alphabetical"
            },
            lastname: {
                required: "Please enter your last name.",
                nameregex: "Lastname should be alphabetical"
            },
            gender: {
                required: "Please choose gender."
            },
            date_of_birth: {
                required: "Please enter a valid DOB."
            },
            address_line_1: {
                required: "Please enter your address"
            },
            states_id: {
                required: "Please choose a valid State."
            },
            city: {
                required: "Please enter your city name.",
                nameregex: "City name should be alphabetical"
            },
            pin_code: {
                required: "Please enter valid PIN."
            }
        }
    });
});
app.controller("profileCtrl", function ($timeout, $scope, profileService, statesService, loaderService) {
    $scope.getProfile = function () {
        profileService.get_profile().then(function (response) {
            $scope.profileObj = response.data.data;
        });
    };

    $timeout(function () {
        $scope.getProfile();
    }, 500);

    $scope.showEditProfilePopup = function () {
        angular.element("#editProfileModal").modal("show");
        $scope.editProfileObj = angular.copy($scope.profileObj);
    };

    $scope.showEditEmailPopup = function () {
        angular.element("#editEmailModal").modal("show");
        $scope.editProfileObj = angular.copy($scope.profileObj);
    };

    $scope.showEditMobilePopup = function () {
        angular.element("#editMobileModal").modal("show");
        $scope.editProfileObj = angular.copy($scope.profileObj);
    };

    $scope.showKycPopup = function () {
        angular.element("#editKycModal").modal("show");
        $scope.editProfileObj = angular.copy($scope.profileObj);
    };


    statesService.get_states().then(function (response) {
        $scope.statesList = response.data.data;
    });

    $scope.updateProfile = function () {
        $scope.ep_error = {};
        if ($("#profileForm").valid()) {
            loaderService.getLoading();
            profileService.profile_update($scope.editProfileObj).then(function (response) {
                loaderService.closeLoading();
                if (response.data.status === "invalid_form") {
                    $scope.ep_error = response.data.data;
                }
                if (response.data.status === "valid") {
                    location.reload();
                }
            })
        }
    };

    $scope.updateNewEmail = function () {
        loaderService.getLoading();
        profileService.update_new_email($scope.editProfileObj).then(function (response) {
            loaderService.closeLoading();
            if (response.data.status === "invalid_form") {
                $scope.ep_error = response.data.data;
            }
            if (response.data.status === "valid") {
                swal(response.data.title, response.data.message, "success");
            }
        })
    };

    $scope.show_otp_field = false;
    $scope.updateNewMobile = function () {
        loaderService.getLoading();
        profileService.update_new_mobile($scope.editProfileObj).then(function (response) {
            loaderService.closeLoading();
            if (response.data.status === "invalid_form") {
                $scope.ep_error = response.data.data;
            }
            if (response.data.status === "valid") { 
                swal(response.data.title, response.data.message, "success");
                $scope.show_otp_field = true;
            }
        });
    };

    $scope.verifyOTP = function () {
        loaderService.getLoading();
        profileService.verify_otp($scope.editProfileObj).then(function (response) {
            loaderService.closeLoading();
            if (response.data.status === "invalid_form") {
                $scope.ep_error = response.data.data;
            }
            if (response.data.status === "valid") {
                swal(response.data.title, response.data.message, "success");
                location.reload();
            }
        });
    };

    $scope.resendOTP = function () {
        loaderService.getLoading();
        profileService.resend_otp().then(function (response) {
            loaderService.closeLoading();
            if (response.data.status === "invalid_form") {
                $scope.ep_error = response.data.data;
            }
            if (response.data.status === "valid") {
                swal(response.data.title, response.data.message, "success");
                angular.element("#editMobileModal").modal("show");
                $scope.editProfileObj = angular.copy($scope.profileObj);
                $scope.show_otp_field = true;
            }
        });
    };



    $scope.resendVerificationKey = function () {
        loaderService.getLoading();
        profileService.send_email_verification_code().then(function (response) {
            loaderService.closeLoading();
            if (response.data.status === "invalid_form") {
                $scope.ep_error = response.data.data;
            }
            if (response.data.status === "valid") {
                swal(response.data.title, response.data.message, "success");
            }
        })
    };

    $scope.updateKycDetails = function () {

        var fd = new FormData();
        var ObjectKeys = Object.keys($scope.editProfileObj);
        console.log(ObjectKeys);

        for (var i = 0; i < ObjectKeys.length; i++) {
            if (ObjectKeys[i] === "address_proof" || ObjectKeys[i] === "pan_card") {
                continue;
            }
            if (typeof $scope.editProfileObj[ObjectKeys[i]] === 'object' || $scope.editProfileObj[ObjectKeys[i]].constructor === Array) {
                fd.append(ObjectKeys[i], JSON.stringify($scope.editProfileObj[ObjectKeys[i]]));
            } else {
                fd.append(ObjectKeys[i], $scope.editProfileObj[ObjectKeys[i]]);
            }
        }

        if (document.getElementById("address_proof") != null) {
            fd.append("address_proof", document.getElementById("address_proof").files[0]);
        }

        if (document.getElementById("pan_card") != null) {
            fd.append("pan_card", document.getElementById("pan_card").files[0]);
        }

        $scope.kyc_error = {};
        loaderService.getLoading();
        profileService.update_kyc(fd).then(function (response) {
            loaderService.closeLoading();
            if (response.data.status === "valid") {
                swal(response.data.title, response.data.message, "success");
                location.reload();
            } else if (response.data.status === "invalid_form") {
                $scope.kyc_error = response.data.data;
            } else if (response.data.status === "invalid") {
                swal(response.data.title, response.data.message, "warning");
                //$scope.noty('warning', response.data.title, response.data.message);
            }
        });
    };

    $scope.showChooseAvatarPopup = function () {
        angular.element("#editAvatarModal").modal("show");
        profileService.get_avatars().then(function (response) {
            $scope.avatars = response.data.data;
        });
    };

    $scope.setAvatar = function (image_id) {
        loaderService.getLoading();
        profileService.set_avatar({'avatar_id': image_id}).then(function (response) {
            loaderService.closeLoading();
            angular.element("#editAvatarModal").modal("hide");
            $scope.getProfile();
        });
    };
});