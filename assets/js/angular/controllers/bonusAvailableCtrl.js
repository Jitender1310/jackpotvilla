app.controller('bonusAvailableCtrl', function($scope, $rootScope, bonusAvailableService, loaderService){
    $scope.coupons = [];
    
    function init(){
        loaderService.getLoading();
        bonusAvailableService.get().then(function(response){
            loaderService.closeLoading();
            $scope.coupons = response.data.data.list;
        });
    }
    
    init();
    
    $scope.applyBonusCodeHandler = function( bonus_code ){
        $rootScope.$emit('bonus_code_apply_event', bonus_code);
    };
});