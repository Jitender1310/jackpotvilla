$(function () {
    $("#profileForm").validate({
        errorPlacement: function (error, element) {
            error.appendTo(element.closest(".form-group"));
        },
        rules: {
            current_password: {
                required: true,
                minlength: 6,
                maxlength: 32
            },
            new_password: {
                required: true,
                minlength: 6,
                maxlength: 32
            },
            confirm_new_password: {
                required: true,
                equalTo: '#uppassword'
            }
        },
        messages: {
            current_password: {
                required: "Enter your old password",
                minlength: "Please enter valid ",
                maxlength: 32
            },
            new_password: {
                required: "Enter new password",
            },
            confirm_new_password: {
                required: "Reenter the password",
                equalTo: "Password mismatch"
            }
        }
    });
});
app.controller("changePasswordCtrl", function ($scope, changePasswordService) {


    function initializeObject() {
        $scope.passwordObj = {
            current_password: '',
            new_password: '',
            confirm_new_password: ''
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };

    $scope.UpdatePassword = function () {
        $scope.error_message = "";
        $scope.cp_error = {};
        if (angular.element("#updatePasswordForm").valid()) {
            changePasswordService.update_password($scope.passwordObj).then(function (response) {
                if (response.data.status === "valid") {
                    swal(response.data.title, response.data.message, "success");
                    initializeObject();
                } else if (response.data.status === "invalid_form") {
                    $scope.cp_error = response.data.data;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };

});