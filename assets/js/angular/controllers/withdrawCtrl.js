//jQuery.validator.addMethod("nameregex", function (value, element) {
//    return this.optional(element) || /^[a-z\s]+$/i.test(value);
//}, "Only alphabetical characters");

$(function () {
    $.validator.addMethod("nameregex", function (value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
    }, "Only alphabetical characters");
    $("#withdrawl_form").validate({
        errorPlacement: function (error, element) {
            error.appendTo(element.closest(".form-group"));
        },
        rules: {
            transfer_type: {
                required: true
            },
            transfer_to: {
                required: true
            },
            request_amount: {
                required: true,
                digits: true,
//                max: 10000,
//                min: 100
            },
            account_number: {
                required: true,
                digits: true
            },
            confirm_account_number: {
                required: true,
                equalTo: '#account_number'
            },
            ifsc_code: {
                required: true,
            },
            account_holder_name: {
                required: true,
                nameregex: true
            },
            paytm_mobile_number: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10
            },
            paytm_person_name: {
                required: true
            }
        },
        messages: {
            transfer_type: {
                required: "Please choose Payment Transaction type"
            },
            transfer_to: {
                required: "Please select withdrawal type.",
            },
            account_number: {
                required: "Please enter Account number.",
                digits: "Plese enter valid account number"
            },
            confirm_account_number: {
                required: "Re Enter the Account number",
                equalTo: "Account number mismatch"
            },
            branch_name: {
                required: "Please enter branch name.",
            },
            ifsc_code: {
                required: "Please enter ifsc code.",
            },
            bank_name: {
                required: "Please enter bank name."
            },
            request_amount: {
                required: "Please enter Amount.",
                digits: "Amount should be numeric.",
//                max: "Maximum amount to withdraw: Rs 10000.",
//                min: "Minimum amount to withdraw: Rs 100.",
            },
            paytm_mobile_number: {
                required: "Please enter paytm mobile number.",
                digits: "Please enter valid mobile number.",
                minlength: "Please enter valid mobile number.",
                maxlength: "Please enter valid mobile number."
            },
            paytm_person_name: {
                required: "Please enter paytm person name.",
            },
            account_holder_name: {
                required: "Please enter account holder name",
                nameregex: "Please enter valid account holder name"
            }
        }
    });
});
app.controller("withdrawCtrl", function ($sce, $timeout, $scope, withdrawService, loaderService) {
    $scope.filter = {
        page: 1
    };
    function initializeObject() {
        $scope.withdrawObj = {
            transfer_type: 'IMPS',
            request_amount: '',
            account_number: '',
            confirm_account_number: '',
            ifsc_code: '',
            bank_name: '',
            branch_name: '',
        };
    }
    initializeObject();
    $scope.ResetForm = () => initializeObject();

    $scope.getRequestsList = () => {
        loaderService.getLoading();
        withdrawService.getRequestsList($scope.filter).then(response => {
            loaderService.closeLoading();
            $scope.requestsList = response.data.data.data;
            $scope.requestsListPagination = response.data.pagination;
            if ($scope.requestsListPagination.pagination.length > 0) {
                $scope.requestsListPagination.pagination = $sce.trustAsHtml($scope.requestsListPagination.pagination);
            }
        });
    };
    $timeout(function () {
        $scope.getRequestsList();
    }, 500);
    $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filter.page = $(this).attr("data-ci-pagination-page");
        $scope.getRequestsList();
    });


    $scope.ValidateWithdrawlRequest = () => {
        if ($("#withdrawl_form").valid()) {
            loaderService.getLoading();
            withdrawService.validateWithdrawRequest($scope.withdrawObj).then(response => {
                loaderService.closeLoading();
                if (response.data.status === "valid") {
                    $scope.withdraw_validted_obj = response.data.details;
                    angular.element("#withdrawDetailsModalForm").modal("show");
                } else if (response.data.status === "invalid") {
                    swal(response.data.title, response.data.message, "warning");
                    return false;
                } else if (response.data.status === "invalid_form") {
                    $scope.ep_error = response.data.data;
                    return false;
                }
            });
        }
    };

    $scope.CheckJokerStatus = () => {
        withdrawService.checkJokerStatus().then(response => {
            loaderService.closeLoading();
            if (response.data.status === "valid") {
                $scope.clubDetails = response.data.data.data;
            }
            if (response.data.status === "invalid_form") {
                swal(response.data.title, response.data.message, "warning");
                return false;
            }
        });
    };
    $scope.CheckJokerStatus();

    $scope.RequestWithdrawl = () => {
        $scope.ep_error = {};
        loaderService.getLoading();
        withdrawService.sendWithDrawlRequest($scope.withdrawObj).then(response => {
            loaderService.closeLoading();
            if (response.data.status === "valid") {
                swal(response.data.title, response.data.message, "success");
                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    setTimeout(() => {
                        location.href = baseurl + "mobile_app/withdraw";
                    }, 3000);
                }else{
                    setTimeout(() => {
                        location.href = baseurl + "withdraw_transactions";
                    }, 3000);
                }
            }
            if (response.data.status === "invalid") {
                swal(response.data.title, response.data.message, "warning");
            }
            if (response.data.status === "invalid_form") {
                $scope.ep_error = response.data.data;
            }
        });
    };

    $scope.getIfscDetails = () => {
        if ($scope.withdrawObj.ifsc_code.length != 11) {
            $scope.withdrawObj.branch_name = "";
            $scope.withdrawObj.bank_name = "";
            return;
        }
        loaderService.getLoading();
        withdrawService.getIfscDetails($scope.withdrawObj.ifsc_code).then(response => {
            loaderService.closeLoading();
            if (response.data.status === "valid") {
                $scope.withdrawObj.branch_name = response.data.data.ifsc_detais.BRANCH
                $scope.withdrawObj.bank_name = response.data.data.ifsc_detais.BANK
            }
            if (response.data.status === "invalid") {
                swal(response.data.title, response.data.message, "warning");
            }
            if (response.data.status === "invalid_form") {
                $scope.ep_error = response.data.data;
            }
        })
    }

    $scope.CancleWithdrawRequest = (obj) => {
        console.log(obj);
        swal({
            title: "Are you sure?",
            text: "You want to cancel this withdrawal request",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                loaderService.getLoading();
                withdrawService.cancelWithdrawalRequest(obj).then(response => {
                    loaderService.closeLoading();
                    if (response.data.status === "valid") {
                        swal(response.data.title, response.data.message, "success");
                        $scope.getRequestsList();
                    }
                    if (response.data.status === "invalid") {
                        swal(response.data.title, response.data.message, "warning");
                    }
                    if (response.data.status === "invalid_form") {
                        swal(response.data.title, response.data.message, "warning");
                    }
                });
            }
        });
    }

});