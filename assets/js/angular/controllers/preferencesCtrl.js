app.controller("preferencesCtrl", function ($scope, preferencesService, loaderService) {
    $scope.getPreferences = function () {
        loaderService.getLoading();
        preferencesService.get().then(function (response) {
            loaderService.closeLoading();
            $scope.preferencesObj = response.data.data;
        });
    };
    $scope.getPreferences();

    $scope.updatePreferences = function () {
        loaderService.getLoading();
        preferencesService.update($scope.preferencesObj).then(function (response) {
            loaderService.closeLoading();
            if (response.data.status === "invalid") {
                swal(response.data.title, response.data.message, "warning");
            }
            if (response.data.status === "valid") {
                swal(response.data.title, response.data.message, "success");
            }
        });
    };
});