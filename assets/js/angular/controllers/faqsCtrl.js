app.controller("faqsCtrl", function ($scope, faqsService, loaderService, $http) {
    $scope.faqs = [];

    faqsService.get_faqs_list().then(function (response) {
        $scope.faqs = response.data.data;
        console.log($scope.faqs);
    });
});
