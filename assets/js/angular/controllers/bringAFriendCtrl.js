$(document).ready(function(){
  $(".invite_friends").click(function(){
      
        var phonesStr = $("input[name=phones ]").val();
        var emailsStr = $("input[name=emails ]").val();
        //console.log(typeof tagStr);
        var base_url = $(this).attr("data-url");
        $.ajax({url: base_url+"?phones="+phonesStr+"&emails="+emailsStr, success: function(result){
        }});
        location.reload(true);
    });
});
app.controller('bringAFriendCtrl', function ($scope, bringAFriendService, loaderService) {
    var tempObj = {
        name: '',
        email_or_mobile: '',
        emails: '',
        phones: ''
    };
    function initializeObj() {
        $scope.contacts = [];
        $scope.contacts.push(angular.copy(tempObj));
    }
    initializeObj();

    $scope.addNewContactRow = function (current_index) {
        if (current_index + 1 == $scope.contacts.length) {
            if ($scope.contacts[current_index].name != '' &&
                    ($scope.contacts[current_index].email != '' ||
                            $scope.contacts[current_index].mobile != '')) {
                $scope.contacts.push(angular.copy(tempObj));
            }
        }
    };

    $scope.removeThisContact = function (current_index) {
        $scope.contacts.splice(current_index, 1);
        if ($scope.contacts.length === 0) {
            $scope.contacts.push(angular.copy(tempObj));
        }
    };

    $scope.openContactModal = function () {
        initializeObj();
        //angular.element('#invide_contact_selection_modal').modal('show');
    };

    $scope.submitInviteFriends = function () {
        loaderService.getLoading();
        console.log($scope.contacts );
        bringAFriendService.submitForm($scope.contacts).then(function (response) {
            loaderService.closeLoading();
            if (response.data.status === "valid") {
                initializeObj();
                angular.element('#invide_contact_selection_modal').modal('hide');
                swal(response.data.title, response.data.message, "success");
            } else if (response.data.status === "invalid") {
                swal(response.data.title, response.data.message, "error");
            }
        });
    }

    $scope.copyToClipboard = function (value) {
        if (navigator.share) {
                navigator.share({
                  title: 'Register at '+ SITE_TITLE + 'and get Rs. '+WELCOME_BONUS+' bonus',
                  text: 'I love playing #'+SITE_TITLE+'. Join me @'+SITE_TITLE+' to get your Rs.'+WELCOME_BONUS+' free joining bonus by clicking this link:' +value,
                  url: value
                }).then(() => {
                  console.log('Thanks for sharing!');
                })
                .catch(err => {
                  console.log(`Couldn't share because of`, err.message);
                });
        } else {
            var tempInput = document.createElement("input");
            tempInput.style = "position: absolute; left: -1000px; top: -1000px";
            tempInput.value = value;
            document.body.appendChild(tempInput);
            tempInput.select();
            document.execCommand("copy");
            document.body.removeChild(tempInput);
            swal('Referral Link has been copied to clipboard : ' + value);
        }        
    };
});