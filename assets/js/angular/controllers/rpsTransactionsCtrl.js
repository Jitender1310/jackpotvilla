app.controller("rpsTransactionsCtrl", function ($scope, rpsTransactionsService, loaderService, $sce) {

    $scope.filter = {txn_type: 'All', page: 1}
    $scope.rpsTransactions = [];
    $scope.pagination = {};

    $scope.getTxnHistory = function () {
        loaderService.getLoading();
        rpsTransactionsService.get_rpsTransactions($scope.filter).then(function (response) {
            loaderService.closeLoading();
            $scope.rpsTransactions = response.data.data.data;
            $scope.rpsTransactionHistoryPagination = response.data.pagination;
            if ($scope.rpsTransactionHistoryPagination.pagination.length > 0) {
                $scope.rpsTransactionHistoryPagination.pagination = $sce.trustAsHtml($scope.rpsTransactionHistoryPagination.pagination);
            }
        });
    };

    $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filter.page = $(this).attr("data-ci-pagination-page");
        $scope.getTxnHistory();
    });

});