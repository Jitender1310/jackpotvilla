$(function () {
    $("#profileForm").validate({
        errorPlacement: function (error, element) {
            error.appendTo(element.closest(".form-group"));
        },
        rules: {
            credits_count: {
                required: true,
                number: true
            }
        }, messages: {
            credits_count: {
                required: "Enter valid amount",
                number: "Mobile shoule be numerical only"
            }
        }
    });
});

app.controller("depositCtrl", function ($scope, $rootScope, depositService, bonusAvailableService, loaderService) {

    $scope.deposit = {
        bonus_code: '',
        credits_count: '',
        source:'website',
        payment_gateway : '',
    };

    if($.url().param("source")!== undefined){
        $scope.deposit.source = $.url().param("source");
    }

    if($.url().param("version_code")!== undefined){
        $scope.deposit.version_code = $.url().param("version_code");
    }

    $scope.createDepositRequest = function () {
        loaderService.getLoading();
        $scope.bonusApplyNotifiction = {};
        depositService.create_deposit_request($scope.deposit).then(function (response) {
            loaderService.closeLoading();
            $scope.chipsObj.wallet_account.fun_chips = response.data.data.fun_chips;
            if (response.data.status === "valid") {
                //location.href = baseurl + "payment_controller?transaction_id=" + response.data.data.transaction_id; // Changes done for demo
		location.href = "https://test.a2zbetting.com/payment_response_controller?orderId=" + response.data.data.transaction_id + "&txStatus=SUCCESS&referenceId=nik_" + response.data.data.transaction_id;
            } else if (response.data.status === "invalid") {
                $scope.bonusApplyNotifiction = {
                    css: "text-danger",
                    message: response.data.message
                };
            }
        });
    };

    $rootScope.$on('bonus_code_apply_event', function (e, bonus_code) {
        $scope.deposit.bonus_code = bonus_code;
        $scope.validateBonusCode();
    });

    $scope.validateBonusCode = function () {
        $scope.bonusApplyNotifiction = {};
        if ($scope.deposit.bonus_code == "" && $scope.deposit.credits_count == "") {
            return;
        }
        $scope.couponValidating = true;
        bonusAvailableService.validate_bonus_code($scope.deposit).then(function (response) {
            $scope.couponValidating = false;
            if (response.data.status == "valid") {
                $scope.bonusApplyNotifiction = {
                    css: "text-success",
                    message: response.data.data.message
                };
            } else if (response.data.status == "invalid") {
                $scope.bonusApplyNotifiction = {
                    css: "text-danger",
                    message: response.data.message
                };
            }
        });
    };
});
