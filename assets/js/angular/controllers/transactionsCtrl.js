app.controller("transactionsCtrl", function ($scope, transactionsService, loaderService, $sce) {
    
    $scope.filter = { txn_type:'All', page:1 }
    $scope.transactions = [];
    $scope.pagination = {};
    
    $scope.getTxnHistory = function () {
        loaderService.getLoading();
        transactionsService.get_transactions($scope.filter).then(function (response) {
            loaderService.closeLoading();
            $scope.transactions = response.data.data.data;
            $scope.transactionHistoryPagination = response.data.pagination;
            if ($scope.transactionHistoryPagination.pagination.length > 0) {
                $scope.transactionHistoryPagination.pagination = $sce.trustAsHtml($scope.transactionHistoryPagination.pagination);
            }
        });
    };
    
     $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filter.page = $(this).attr("data-ci-pagination-page");
        $scope.getTxnHistory();
    });
    
});