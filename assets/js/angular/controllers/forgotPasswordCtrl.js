$(function () {
    $("#forgotPasswordForm").validate({
        rules: {
            username: {
                required: true,
                nowhitespace: true
            }
        },
        messages: {
            username: {
                required: "Please enter username",
                nowhitespace: true
            }
        }
    });
});

app.controller("forgotPasswordCtrl", function ($scope, forgotPasswordService, loaderService) {
    $scope.fp = {
        username: ""
    };
    $scope.submitRequest = function () {
        $scope.fp_error = {};
        if ($("#forgotPasswordForm").valid()) {
            loaderService.getLoading();
            forgotPasswordService.submit_request($scope.fp).then(function (response) {
                loaderService.closeLoading();
                if (response.data.status === "invalid_form") {
                    $scope.fp_error = response.data.data;
                }
                if (response.data.status === "invalid") {
                    swal(response.data.title, response.data.message, "warning");
                }
                if (response.data.status === "valid") {
                    swal(response.data.title, response.data.message, "success");
                }
            });
        }
    };
});