app.controller("bonusHistoryCtrl", function ($scope, bonusHistoryService, $sce, loaderService) {
    
    $scope.filter = {
        name_sort: "name_asc",
        sort_by: "name_asc", //rating,min_order,fast_delivery
        cusines: [],
        quick: [],
        search_key: '',
        page:1
    };
    $scope.getBonusHistory = function () {
        loaderService.getLoading();
        bonusHistoryService.get_bonus_history($scope.filter).then(function (response) {
            loaderService.closeLoading();
            $scope.bonusHistoryList = response.data.data.data;
            $scope.bonusHistoryPagination = response.data.pagination;
            if ($scope.bonusHistoryPagination.pagination.length > 0) {
                    $scope.bonusHistoryPagination.pagination = $sce.trustAsHtml($scope.paymentHistoryPagination.pagination);
            }
        });
    };
    $scope.getBonusHistory();

    $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filter.page = $(this).attr("data-ci-pagination-page");
        $scope.getBonusHistory();
    });

});