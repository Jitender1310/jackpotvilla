app.controller("gameLobbyCtrl", function ($scope, gameLobbyService, loaderService, playNowService, $rootScope) {
    //sfs code
    $scope.tab_id = 1;

    var useEncryption = true;

    // Create configuration object
    var config = {};
    config.host = $rootScope.GAME_SERVER_ID;
    config.port = useEncryption ? 8443 : 8080;
    config.useSSL = useEncryption;
    config.zone = "RummyZone";

    var sfs = new SFS2X.SmartFox(config);
    sfs.addEventListener(SFS2X.SFSEvent.CONNECTION, onConnection, this);
    sfs.connect($rootScope.GAME_SERVER_ID, config.port);

    //popup window size properties
    var params = 'width=1100';
    params += ', height=670';
    var playNowToken;
    $scope.accessToken = "";

    $scope.selectedGameType = "";
    $scope.selectedSubGameType = "";
    $scope.loading = false;
    $scope.getLobbySection = function () {
        if ($scope.loading === false) {
            loaderService.getLoading();
        }
        gameLobbyService.get_lobby_section($scope.user).then(function (response) {
            $scope.loading = true;
            if (response.data.status === "invalid_form") {
                $scope.login_error = response.data.data;
            }
            if (response.data.status === "valid") {
                loaderService.closeLoading();
                $scope.gameLobbyObj = response.data.data;
                if ($scope.selectedGameType == "") {
                    $scope.selectedGameType = "cash";
                }
                if ($scope.selectedSubGameType == "") {
                    $scope.selectedSubGameType = "pool";
                }
                $scope.updateGameListTable();
            }
        });
    };

    $scope.getLobbySection();

    setInterval(function () {
        $scope.getLobbySection();
    }, 50000);


//    $scope.timeInSeconds('2019-04-01 18:00:00');
    $scope.setGameType = function (selectedGameType) {
        $scope.selectedGameType = selectedGameType;
        if ($scope.selectedGameType === "tournaments") {
            $scope.gameListTable = [];
        }
        $scope.updateGameListTable();
    };
    $scope.GetGameProgress = function (cloned_tournaments_id) {
        loaderService.getLoading();
        gameLobbyService.game_progress(cloned_tournaments_id).then(function (response) {
            loaderService.closeLoading();
            if (response.data.status === "valid") {
                $scope.gameProgress = response.data.data.round_info;
            }
        });
    };
    $scope.GetTablesInfo = function (obj) {
        $scope.roundno = obj.round_number;
        loaderService.getLoading();
//        console.log(obj.players_info);
        if (obj.players_info.length > 0) {
            loaderService.closeLoading();
            $scope.tablesInfo = obj.players_info;
        } else {
            $scope.table_error_msg = "No Tables Found";
        }
    };
    $scope.GetPlayers = function (obj) {
        $scope.tableid = obj.tournamentTableId;
        loaderService.getLoading();
//        console.log(obj[index].players_info);
        if (obj.players.length > 0) {
            loaderService.closeLoading();
            $scope.playersInfo = obj.players;
        } else {
            $scope.players_error_msg = "No Players Found";
        }
    };

    $scope.setSubGameType = function (selectedSubGameType) {
        $scope.selectedSubGameType = selectedSubGameType;
        $scope.updateGameListTable();
    };

    $scope.showMyJoinedGamePopup = function () {
        loaderService.getLoading();
        sendExtensionRequest();
    };

    $scope.showJoinedPlayersPopup = function () {
        angular.element('#tournamentDetails').modal("hide");
        setTimeout(function () {
            angular.element("#tournamentJoinedPlayersPopup").modal("show");
        }, 500);
    };


    angular.element('#tournamentJoinedPlayersPopup').on('hidden.bs.modal', function () {
        angular.element("#tournamentDetails").modal("show");
    });

    $scope.joinTournament = function (cloned_tournaments_id) {
        loaderService.getLoading();
        gameLobbyService.join_tournament(cloned_tournaments_id).then(function (response) {
            loaderService.closeLoading();
            if (response.data.status === "invalid_kyc") {
                swal(response.data.title, response.data.message, "warning");
            }
            if (response.data.status === "invalid") {
                swal(response.data.title, response.data.message, "warning");
            }
            if (response.data.status === "valid") {
                $scope.tournamentDetailsObj.is_joined = 1;
                swal(response.data.title, response.data.message, "success");
            }
        });

        if ($scope.tournamentDetailsObj) {
            $scope.showTournamentDeatilsPopup($scope.tournamentDetailsObj);
        }

    };

    $scope.unJoinTournament = function (cloned_tournaments_id) {
        loaderService.getLoading();
        gameLobbyService.unJoin(cloned_tournaments_id).then(function (response) {
            loaderService.closeLoading();
            if (response.data.status === "invalid") {
                swal(response.data.title, response.data.message, "warning");
            }
            if (response.data.status === "valid") {
                $scope.tournamentDetailsObj.is_joined = 0;
                swal(response.data.title, response.data.message, "success");
            }

        });
    };


    $scope.durationSpendTimer = function ($ct, obj) {
        // Get todays date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = now - $ct;
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        if (days === 0) {
            return hours + "h " + minutes + "m " + seconds + "s ";
        } else {
            return days + "d " + hours + "h " + minutes + "m " + seconds + "s ";
        }
    }

    $scope.countDownTimer = function ($ct, obj) {
        // Get todays date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = $ct - now;

        if (distance < 0) {
            //recall tournament row
            if (obj.tournament_status === "Running") {
                return $scope.durationSpendTimer($ct, obj);
            } else {
                clearInterval(timerSource);
                $scope.showTournamentDeatilsPopup(obj);
            }
        }

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        if (days === 0) {
            return hours + "h " + minutes + "m " + seconds + "s ";
        } else {
            return days + "d " + hours + "h " + minutes + "m " + seconds + "s ";
        }
    }

    var timerSource = "";
    $scope.showTournamentDeatilsPopup = function (obj) {
        $scope.gameProgress = "";
        $scope.tablesInfo = "";
        $scope.playersInfo = "";
        $scope.roundno = "";
        $scope.tableid = "";
        $scope.tab_id = 1;

        gameLobbyService.get_tournament_info(obj.cloned_tournaments_id).then(function (response) {
            if (response.data.status == "valid") {
                $scope.tournamentDetailsObj = response.data.data.tournament_details;
                timerSource = setInterval(function () {
                    if ($scope.tournamentDetailsObj.tournament_status === "Created") {
                        $scope.tournamentDetailsObj.starting_timer = $scope.countDownTimer($scope.tournamentDetailsObj.js_registration_start_date_time, obj);
                        $scope.$apply();
                    } else if ($scope.tournamentDetailsObj.tournament_status === "Registration_Start") {
                        $scope.tournamentDetailsObj.closing_timer = $scope.countDownTimer($scope.tournamentDetailsObj.js_registration_close_date_time, obj);
                        $scope.$apply();
                    } else if ($scope.tournamentDetailsObj.tournament_status === "Registration_Closed") {
                        $scope.tournamentDetailsObj.tournament_start_timer = $scope.countDownTimer($scope.tournamentDetailsObj.js_tournament_date_time, obj);
                        $scope.$apply();
                    } else if ($scope.tournamentDetailsObj.tournament_status === "Running") {
                        $scope.tournamentDetailsObj.tournament_duration = $scope.durationSpendTimer($scope.tournamentDetailsObj.js_tournament_date_time, obj);
                        $scope.$apply();
                    }
                }, 1000);
                angular.element("#tournamentDetails").modal("show");
            } else {
                swal(response.data.title, response.data.message, "warning");
                location.reload();
            }
        });
    };


//    angular.element('#tournamentDetails').on('hidden.bs.modal', function () {
//        console.log(timerSource.close());
//        timerSource.close();
//    });

    $scope.gameListTable = [];

    $scope.applyFilters = true;

    $scope.updateGameListTable = function () {

        if ($.url().param("category") == "tournaments") {
            $scope.selectedGameType = "tournaments";
            if ($.url().param("type") == "Premium") {
                $scope.selectedSubGameType = "Premium Tournaments";
            }
        }

        if ($scope.selectedGameType === "tournaments") {
            $scope.gameListTable = [];
            for (var i = 0, l = $scope.gameLobbyObj.tournaments.length; i < l; i++) {
                console.log($scope.gameLobbyObj.tournaments[i].name);
                if ($scope.selectedSubGameType === $scope.gameLobbyObj.tournaments[i].name) {
                    $scope.gameListTable = $scope.gameLobbyObj.tournaments[i].tournaments_list;
                    console.log($scope.gameLobbyObj.tournaments[i].tournaments_list);
                }
            }
        } else {
            $scope.selectedGameType;
            $scope.selectedSubGameType;
            $scope.gameListTable = $scope.gameLobbyObj[$scope.selectedGameType][$scope.selectedSubGameType];
        }
        console.log($scope.gameListTable);
    };

    $scope.openPlayNowWindow = function (gameToken, accessToken, gameType) {
        playNowService.get_token().then(function (response) {
            playNowToken = response.data.data.play_token;
            var rand_string = response.data.data.rand_string;
            $scope.accessToken = accessToken;
            //$scope.accessToken = response.data.data.access_token;
            //var windowName = "playWindow" + rand_string;
            var windowName = playNowToken;
            // window.open(baseurl + "playtool/index.html?" + accessToken + "&" + playNowToken + "&" + gameToken + "&" + gameType, windowName, params);
            window.open(baseurl + "www/index.html?" + accessToken + "&" + playNowToken + "&" + gameToken + "&" + gameType, windowName, params);
        });
    };

    $scope.openPlayNowTournamentWindow = function (cloned_tournaments_id, accessToken, gameType) {
        playNowService.get_token().then(function (response) {
            playNowToken = response.data.data.play_token;
            var rand_string = response.data.data.rand_string;
            $scope.accessToken = accessToken;
//$scope.accessToken = response.data.data.access_token;
            var windowName = rand_string;
            // window.open(baseurl + "playtool/index.html?" + accessToken + "&" + cloned_tournaments_id + "&" + cloned_tournaments_id + "&" + gameType, windowName, params);
            window.open(baseurl + "www/index.html?" + accessToken + "&" + playNowToken + "&" + gameToken + "&" + gameType, windowName, params);
        });
    };

    $scope.takeSeat = function (obj) {
        console.log(obj.userGameToken);
        console.log(obj);
        var windowName = obj.userGameToken;
        // window.open(baseurl + "playtool/index.html?" + obj.accessToken + "&" + obj.userGameToken + "&" + obj.responsePacket.token + "&" + obj.responsePacket.gameType, windowName, params);
        window.open(baseurl + "www/index.html?" + accessToken + "&" + playNowToken + "&" + gameToken + "&" + gameType, windowName, params);
    };


    function onConnection(evtParams) {
        console.log("Connecting request to SFS Server");
        if (evtParams.success) {
            //console.log("Connected to SmartFoxServer 2X!");
            sfs.addEventListener(SFS2X.SFSEvent.LOGIN, onLogin, this);
            sfs.addEventListener(SFS2X.SFSEvent.LOGIN_ERROR, onLoginError, this);
            sfs.addEventListener(SFS2X.SFSEvent.EXTENSION_RESPONSE, onExtensionResponse, this);

            playNowService.get_token().then(function (response) {
                $scope.accessToken = response.data.data.access_token;
                sfs.send(new SFS2X.LoginRequest(response.data.data.rand_string, "", null, "RummyZone"));
            });
        } else
            console.log("Connection failed. Is the server running at all?");
    }


    function onLogin(evtParams) {
        console.log("Login successful!");
        setInterval(function () {
            console.log("Ping.....");
            var params = new SFS2X.SFSObject();
            params.putUtfString("USER_ID", $scope.accessToken);
            sfs.send(new SFS2X.ExtensionRequest("CLIENT_PING", params));
        }, 5000);
    }

    function onLoginError(evtParams) {
        console.log("Login failure: " + evtParams.errorMessage);
    }

    function sendExtensionRequest() {
        // Send two integers to the Zone extension and get their sum in return
        var params = new SFS2X.SFSObject();
        params.putUtfString("USER_ID", $scope.accessToken);
        sfs.send(new SFS2X.ExtensionRequest("GET_JOIN_GAME_LIST", params));
    }

    $scope.joinedListTable = [];
    function onExtensionResponse(evtParams) {

        if (evtParams.cmd == "SERVER_STATICS") {
            var responsePacket = JSON.parse(evtParams.params.getText("RESPONSE_PACKET"));
            $rootScope.onlinePlayers = responsePacket.totalOnlinePlayers;
            $rootScope.$apply();
        } else if (evtParams.cmd == "TABLE_GROUP_STATICS") {
            // We expect an integer called "sum"
            var responsePacket = JSON.parse(evtParams.params.getText("RESPONSE_PACKET"));

            var gameType = responsePacket.gameType.toLowerCase();
            var gameSubType = responsePacket.gameSubType.toLowerCase();
            for (var j = 0, jl = $scope.gameLobbyObj[gameType][gameSubType].length; j < jl; j++) {
                if ($scope.gameLobbyObj[gameType][gameSubType][j].token == responsePacket.token) {
                    $scope.gameLobbyObj[gameType][gameSubType][j].active_players = responsePacket.totalActivePlayers2;
                    $scope.gameLobbyObj[gameType][gameSubType][j].regPlayers = responsePacket.regPlayers;
                    $scope.$apply();
                }
            }
            //console.log(responsePacket);
        } else if (evtParams.cmd == "GET_JOIN_GAME_LIST") {
            $scope.joinedListTable = [];
            var responsePacket = JSON.parse(evtParams.params.getText("RESPONSE_PACKET"));
            console.log(responsePacket);
            loaderService.closeLoading();
            if (responsePacket.message == "JAVA_EXCEPTION_OCCURRED_IN_JOIN_GAME_LIST") {
                swal("Oops!", "Connection error!", "warning");
            } else if (responsePacket.errorCode == 1) {
                swal("Oops!", "Connection error!", "warning");
            } else {
                angular.element("#joinedGameList").modal("show");
                $scope.joinedListTable = responsePacket.responsePacket.alreadyJoinedGameBeans;
                $scope.joinedListTournaments = responsePacket.responsePacket.alreadyJoinedTournamentBeans;
                $scope.$apply();
            }
        }
    }
});
