app.controller('gameTrackerCtrl', function ($scope, gameTrackerService, loaderService) {
    $scope.getMyGameTracker = function () {
        loaderService.getLoading();
        gameTrackerService.get_my_game_tracker().then(function (response) {
            loaderService.closeLoading();
            $scope.gameTrackerObj = response.data.data.game_tracker_info;
            $('[data-toggle="popover"]').popover();
            setInterval(function () {
                $('[data-toggle="popover"]').popover()
            }, 3000);
        });
    };
    $scope.getMyGameTracker();

    $scope.goToFilterTournaments = function (obj) {
        location.href = baseurl + "game_lobby?category=tournaments&type=Premium&frequency=" + obj.frequency + "&frequency_category=" + obj.title;
    };
});