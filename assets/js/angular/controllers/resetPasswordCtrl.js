$(function () {
    $("#reset_password_form").validate({
        rules: {
            password: {
                required: true,
                minlength: 6,
                maxlength: 32
            },
            cnf_password: {
                required: true,
                minlength: 6,
                maxlength: 32,
                equalTo: "#restPasswordId"
            },
        },
        messages: {
            password: {
                required: "Enter New Password ",
                minlength: "Minimum 6 Characters required",
                maxlength: "Maximum 32 Characters only"
            },
            cnf_password: {
                required: "Enter Confirm Password ",
                minlength: "Minimum 6 Characters required",
                maxlength: "Maximum 32 Characters only",
                equalTo: "Password and Confirm Password should be Same"
            }
        }
    });
});
app.controller("resetPasswordCtrl", function ($scope, forgotPasswordService, loaderService) {
    $scope.myObj = {};

    $scope.doResetPasswordWithCode = function (verificationCode) {
        $scope.rp_error = {};
        $scope.myObj.forgot_password_verfication_code = verificationCode;
        if ($("#reset_password_form").valid()) {
            loaderService.getLoading();

            forgotPasswordService.reset_password_with_code($scope.myObj).then(function (response) {
                loaderService.closeLoading();
                if (response.data.status === "invalid_form") {
                    $scope.rp_error = response.data.data;
                }
                if (response.data.status === "invalid") {
                    swal(response.data.title, response.data.message, "warning");
                }
                if (response.data.status === "valid") {
                    swal(response.data.title, response.data.message, "success");
                    location.href = baseurl+"?access_token="+getUrlVars()["access_token"];
                }
                ;
            });
        }
    };
});
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}