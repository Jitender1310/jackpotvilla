$(function () {
    $("#registerForm").validate({
        rules: {
            username: {
                required: true,
                nowhitespace: true
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 32,
                nowhitespace: true
            },
            email: {
                required: true,
                email: true,
                nowhitespace: true
            },
            mobile:{
                required: true,
                minlength: 10,
                maxlength: 10,
                digits:true,
                nowhitespace: true
            },
            country: {
                required: true
            }
        },
        messages: {
            username: {
                required: "Please enter username",

                nowhitespace: true
            },
            password: {
                required: "Please enter password",
                minlength: "Invalid Password",
                maxlength: "Invalid Password",
                nowhitespace: "Invalid Password"
            },
            email: {
                required: "Please enter you email",
                email: "Please enter valid registered email"
            },
            mobile:{
                required: "Please enter valid mobile number",
                minlength: "Please enter valid mobile number",
                maxlength: "Please enter valid mobile number",
                digits:"Please enter valid mobile number",
                nowhitespace: "Please enter valid mobile number"
            },
            country: {
                required: " Please select a country"
            }
        },errorPlacement: function (error, element) {
    error.appendTo($(element).parents('.form-group'));
}
    });
});
app.controller("registerCtrl", function ($scope, registerService, loaderService) {
    $scope.reg_user = {
        username: "",
        password: "",
        email: "",
        country: "",
        referred_by_code:""
    };

    $scope.countries = [];

    $scope.doRegister = function () {
        $scope.reg_error = {};
        if ($("#registerForm").valid()) {
            loaderService.getLoading();
            $scope.registerBtn = true;
            registerService.register($scope.reg_user).then(function (response) {
                $scope.registerBtn = false;
                loaderService.closeLoading();
                if (response.data.status === "invalid_form") {
                    $scope.reg_error = response.data.data;
                    $scope.registerBtn = true;
                }
                if (response.data.status === "valid") {
                    location.href = baseurl + "registration_success";
                }
            })
        }
    };

    $scope.getCountries = function () {
        $scope.reg_error = {};
        registerService.countries().then(function (response) {
            $scope.countries = response.data.data;
        })
    };

    $scope.getCountries();
});

app.controller("loginCtrl", function ($scope, loginService, loaderService) {
    $scope.user = {
        username: "",
        password: ""
    };

    $scope.doLogin = function () {
        $scope.login_error = {};
        if ($("#loginForm").valid()) {
            loaderService.getLoading();
            $scope.loginBtn = true;
            loginService.verify_login($scope.user).then(function (response) {
                loaderService.closeLoading();
                $scope.loginBtn = false;
                if (response.data.status === "invalid_form") {
                    $scope.login_error = response.data.data;
                }
                if (response.data.status === "valid") {
                    location.reload();
                }
            })
        }
    };

    /*****-----FACEBOOK LOGIN---------********/


    $scope.doSocialLogin = function (data) {
        loaderService.getLoading();
        loginService.social_login(data).then(function (response) {
            loaderService.closeLoading();
            if (response.data.status === "invalid_form") {
                $scope.login_error = response.data.data;
            }
            if (response.data.status === "valid") {
                location.reload();
            }
        })
    };
    // Load the JavaScript SDK asynchronously
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    window.fbAsyncInit = function () {
        // FB JavaScript SDK configuration and setup
        FB.init({
            appId: '23609294', // FB App ID
            cookie: true, //2366837603409294 enable cookies to allow the server to access the session
            xfbml: true, // parse social plugins on this page
            version: 'v2.8' // use graph api version 2.8
        });

        // Check whether the user already logged in
        FB.getLoginStatus(function (response) {
            if (response.status === 'connected') {
                //display user data
                //getFbUserData();
            }
        });
    };



// Facebook login with JavaScript SDK
    $scope.fbLogin = function () {
        FB.login(function (response) {
            if (response.authResponse) {
                // Get and display the user profile data
                $scope.getFbUserData();
            } else {
                //document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
            }
        }, {scope: 'email'});
    }

// Fetch the user profile data from facebook
    $scope.getFbUserData = function () {
        FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
                function (response) {
                    console.log(response);
                    if (document.getElementById('fbLink')) {
                        document.getElementById('fbLink').setAttribute("onclick", "fbLogout()");
                    }
                    var data = {
                        firstname: response.first_name,
                        lastname: response.last_name,
                        email: response.email,
                        facebook_account_id: response.id
                    };
                    $scope.doSocialLogin(data);
                    //document.getElementById('userData').innerHTML = '<p><b>FB ID:</b> ' + response.id + '</p><p><b>Name:</b> ' + response.first_name + ' ' + response.last_name + '</p><p><b>Email:</b> ' + response.email + '</p><p><b>Gender:</b> ' + response.gender + '</p><p><b>Locale:</b> ' + response.locale + '</p><p><b>Picture:</b> <img src="' + response.picture.data.url + '"/></p><p><b>FB Profile:</b> <a target="_blank" href="' + response.link + '">click to view profile</a></p>';
                });
    }
// Logout from facebook
    $scope.fbLogout = function () {
        FB.logout(function () {
            if (document.getElementById('fbLink')) {
                document.getElementById('fbLink').setAttribute("onclick", "fbLogin()");
            }
            document.getElementById('fbLink').innerHTML = '<img src="fblogin.png"/>';
            document.getElementById('userData').innerHTML = '';
            document.getElementById('status').innerHTML = 'You have successfully logout from Facebook.';
        });
    }
    /*****-----FACEBOOK LOGIN ENDS---------********/




    /*****-----GOOGLE LOGIN STARTS---------********/

    google_signin = function (googleUser) {
        //console.log(googleUser);
        //return;
        var profile = googleUser.getBasicProfile();
//        var buttonData = document.getElementsByClassName("logoutbtn");
//        console.log(buttonData);
        //return;
        var data = {
            firstname: profile.getName(),
            lastname:'',
            email: profile.getEmail(),
            google_account_id: profile.getId(),
            profile_pic: profile.getImageUrl()
        };

        $scope.doSocialLogin(data);
    }

    /*****-----GOOGLE LOGIN ENDS---------********/

});
