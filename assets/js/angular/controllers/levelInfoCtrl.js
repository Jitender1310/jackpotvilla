app.controller('levelInfoCtrl', function($scope, levelInfoService, loaderService){
    $scope.expertizeLevelObj = [];
    $scope.getCurrentLevelInfo = function(){
        loaderService.getLoading();
        levelInfoService.get_expertize_level_details().then(function(response){
            loaderService.closeLoading();
            $scope.expertizeLevelObj = response.data.data;
        });
    };
    $scope.getCurrentLevelInfo();
});