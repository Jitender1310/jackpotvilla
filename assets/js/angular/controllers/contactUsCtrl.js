//angular.element( '#contact_us_form' ).validate({
$(function () {
    $("#contact_us_form").validate({
        errorPlacement: function(error, element){
            error.appendTo(element.closest(".form-group"));
        },
        rules: {
            email:{
                required:true,
                email:true
            },
            query:{
                required:true
            },
            captcha:{
                required:true
            },
            message:{
                required:true
            }
        },
        messages:{
            email:{
                required:'Email Address is Required field.',
                email:'Email address is not valid.'
            },
            query:{
                required:'Please Selected Query'
            },
            captcha:{
                required:'Please Enter Captcha'
            },
            message:{
                required:'Message is a required field'
            }
        }
    });
});

app.controller('contactUsCtrl', function($scope, contactUsService, loaderService){
    
    $scope.get_captcha = function(){
        $scope.captcha_image = "";
        $scope.captcha_image = apiBaseUrl + "contact_us/captcha?rand=" + Math.random().toString().replace('0.', '');
    };
    
    $scope.get_captcha();
    
    function initializeObject(){
        $scope.contactObj = {
            email:'',
            username:'',
            query:'',
            captcha:'',
            message:''
        }
    }
    
    initializeObject();
    
    $scope.submitContact = function(){
        $scope.contactObj.captcha = $("#g-recaptcha-response").val();
        $scope.error_message = "";
        $scope.cp_error = {};
        
        if ($("#contact_us_form").valid()) {
            loaderService.getLoading();
            contactUsService.submit_form($scope.contactObj).then(function(response){
                loaderService.closeLoading();
                if (response.data.status === "valid") {
                    $scope.error_message = response.data.message;
                    initializeObject();
                    location.reload();
                } else if (response.data.status === "invalid_form") {
                    $scope.cp_error = response.data.data;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };
});