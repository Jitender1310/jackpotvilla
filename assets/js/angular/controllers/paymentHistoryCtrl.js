app.controller("paymentHistoryCtrl", function ($scope, paymentHistoryService, $sce, loaderService) {
    
    $scope.filter = {
        name_sort: "name_asc",
        sort_by: "name_asc", //rating,min_order,fast_delivery
        cusines: [],
        quick: [],
        search_key: '',
        page:1
    };
    $scope.getPaymentHistory = function () {
        loaderService.getLoading();
        paymentHistoryService.get_history($scope.filter).then(function (response) {
            loaderService.closeLoading();
            $scope.paymentHistoryList = response.data.data.data;
            $scope.paymentHistoryPagination = response.data.pagination;
            if ($scope.paymentHistoryPagination.pagination.length > 0) {
                    $scope.paymentHistoryPagination.pagination = $sce.trustAsHtml($scope.paymentHistoryPagination.pagination);
            }
        });
    };
    $scope.getPaymentHistory();

    $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filter.page = $(this).attr("data-ci-pagination-page");
        $scope.getPaymentHistory();
    });

});