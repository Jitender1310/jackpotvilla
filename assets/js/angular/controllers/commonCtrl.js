app.controller("commonCtrl", function ($scope, chipsService, loaderService, $http) {
    $scope.getChips = function () {
        chipsService.get_chips().then(function (response) {
            $scope.chipsObj = response.data.data;
        });
    };
    $scope.getChips();
    $scope.getNotifications = () => {
        chipsService.get_notifications().then(response => {
            $scope.notificationsList = response.data.data.list;
        });
    };
    $scope.getNotifications();
    $scope.showOfferNotificationsPopup = () => {
        angular.element("#offerNotifications").modal("show");
    };
    $scope.resetFunChips = function () {
        loaderService.getLoading();
        chipsService.reset_chips().then(function (response) {
            loaderService.closeLoading();
            $scope.chipsObj.wallet_account.fun_chips = response.data.data.fun_chips;
            if (response.data.status === "valid") {
                swal(response.data.title, response.data.message, "success");
            } else if (response.data.status === "invalid") {
                swal(response.data.title, response.data.message, "warning");
            }
        });
    };
    $scope.redeemRps = function (selected_points) {
        $scope.redeem_rps_points = selected_points;
        if (confirm("Are you sure you want to redeem " + selected_points + " rps points?")) {
            chipsService.redeem_rps(selected_points).then(function (response) {
                if (response.data.status === "valid") {
                    swal(response.data.title, response.data.message, "success");
                    location.reload();
                } else if (response.data.status === "invalid_form") {
                    swal(response.data.title, response.data.message, "warning");
                } else if (response.data.status === "invalid") {
                    swal(response.data.title, response.data.message, "warning");
                }
            });
        }
    };
    $scope.homepanels = [
        {
            id: 1,
            head: 'Classic Indian Rummy',
            content: 'The classic Indian rummy is a popular variant of 13 cards Rummy. It is a mix of Gin rummy and Rummy 500. What makes the game exciting are the complications that arise from forming sets with the odd number of cards dealt to them. Not only are the rules of the game simple, it challenges the player�s memory, observation skills, concentration and quick thinking to win.',
        },
        {
            id: 2,
            head: 'Great Rummy Experience',
            content: 'The ever growing trend of rummy in India calls for a gaming arena that offers the best experience to its players. Let�s take a break from our monotonous lives and indulge in the fascinating world of rummy. Register today, with CLover Rummy and enjoy with exciting tournaments, bonuses, rewards and more!'
        },
        {
            id: 3,
            head: 'Safe and Secure',
            content: 'Mr. Rummy ensures safe and fair play. User�s personal information is held confidential. The information you give us is used only to improve your gaming experience. We do not share your information. We hold the protection of your privacy in the highest regard. The safety of your financial information is guaranteed.'
        },
        {
            id: 4,
            head: 'Fair play',
            content: 'Rummy is acceptably a game of skill. Although offline rummy is mostly played amongst friends and family with little to no dirt, online rummy is completely different since it is played among strangers. In order to avoid foul play and unfair practices, we at CLover Rummy strictly follow a fair play policy to present the best and fair gaming experience to our users.'
        },
        {
            id: 5,
            head: 'Deposit and Instant withdrawal',
            content: 'Players of CLover Rummy can make easy deposits to their cash accounts. Play cash games to win exciting prizes enjoy easy withdrawals with our flexible withdrawal policy! Join now.'
        },
        {
            id: 6,
            head: 'Great Rewards, Offers and Bonuses',
            content: 'CLover Rummy offers you a variety of generous rewards and bonuses such as the welcome bonus, top up bonus and the popular refer a friend bonus. With the most minimalistic of activities, you can gain exciting rewards and bonuses.'
        }
    ]
    $scope.pactive = [];
    $scope.pactive[0] = true;
    $scope.pshow = function (index) {
        // $scope.pactive = false;
        $scope.pactive[0] = false;
        $scope.pactive[index] = true;
    };
    
    $scope.phide = function (index) {
        if ($scope.pactive[0] != true) {
            $scope.pactive[0] = true;
            $scope.pactive[index] = false;
        }
    };

});
