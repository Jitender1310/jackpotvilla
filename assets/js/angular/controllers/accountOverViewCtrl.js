app.controller("accountOverViewCtrl", function ($scope, accountOverViewService, loaderService) {
    $scope.getAccountOverViewDetails = function () {
        loaderService.getLoading();
        accountOverViewService.get().then(function (response) {
            loaderService.closeLoading();
            $scope.acObj = response.data.data;
        });
    };
    $scope.getAccountOverViewDetails();

});