var googleUser = {};
var startApp = function () {
    gapi.load('auth2', function () {
        // Retrieve the singleton for the GoogleAuth library and set up the client.
        auth2 = gapi.auth2.init({
            client_id: '706066242573-nmmsv1bbnuquc7pkisd7ulvt438uad82.apps.googleusercontent.com',
            cookiepolicy: 'single_host_origin',
            // Request scopes in addition to 'profile' and 'email'
            //scope: 'additional_scope'
        });
        attachSignin(document.getElementById('googleLoginBtn'));
    });
};

function attachSignin(element) {
    //console.log(element.id);
    auth2.attachClickHandler(element, {},
            function (googleUser) {
                var data = {
                    firstname: googleUser.getBasicProfile().getGivenName(),
                    lastname: googleUser.getBasicProfile().getFamilyName(),
                    username: googleUser.getBasicProfile().getEmail()
                };
                console.log(data);
                doSocialLogin(data);
                /*document.getElementById('name').innerText = "Signed in: " +
                 googleUser.getBasicProfile().getName();*/
            },
            function (error) {
                // alert(JSON.stringify(error, undefined, 2));
            });
}

function onLoadCallback() {
    gapi.client.setApiKey('693069904905-b8kghsi9qq7ahh9mhppd0ip851vjuca0.apps.googleusercontent.com'); //set your API KEY
    gapi.client.load('plus', 'v1', function () {}); //Load Google + API
}

function onSuccess(googleUser) {
    console.log('Logged in as: ' + googleUser.getBasicProfile().getName());
}

function onFailure(error) {
    console.log(error);
}
