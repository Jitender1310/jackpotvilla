-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 19, 2019 at 02:27 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rummy_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_downloads`
--

CREATE TABLE `app_downloads` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bonus_coupons`
--

CREATE TABLE `bonus_coupons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `coupon_code` varchar(100) NOT NULL,
  `bonus_type` enum('Deposit Bonus','Occasional Bonus') NOT NULL DEFAULT 'Deposit Bonus',
  `title` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `from_date_time` datetime NOT NULL,
  `to_date_time` datetime NOT NULL,
  `terms_conditions` text NOT NULL,
  `created_by_user_id` bigint(20) UNSIGNED NOT NULL,
  `updated_by_user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bonus_coupons`
--

INSERT INTO `bonus_coupons` (`id`, `coupon_code`, `bonus_type`, `title`, `description`, `from_date_time`, `to_date_time`, `terms_conditions`, `created_by_user_id`, `updated_by_user_id`, `created_at`, `updated_at`, `status`) VALUES
(1, 'BONUS123', 'Deposit Bonus', 'DEPOSIT100', 'Hello use and get 100 rupees bonus', '2019-02-13 00:00:00', '2019-02-14 00:00:00', '0sfd', 2, 2, '1550052546', '1550053834', 1),
(2, 'bonus100', 'Deposit Bonus', 'Good evening', 'fsdafsda', '2019-02-13 15:49:00', '2019-03-02 15:49:00', 'sfdafsa', 0, 0, '1550052546', '1550053794', 1);

-- --------------------------------------------------------

--
-- Table structure for table `deposit_money_transactions`
--

CREATE TABLE `deposit_money_transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `players_id` bigint(20) UNSIGNED NOT NULL,
  `comment` varchar(200) NOT NULL,
  `credits_count` int(10) UNSIGNED NOT NULL,
  `transaction_id` varchar(100) NOT NULL,
  `payment_gateway_id` varchar(100) NOT NULL,
  `bonus_code` varchar(20) NOT NULL DEFAULT '',
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `transaction_status` enum('Pending','Failed','Cancelled','Success') NOT NULL DEFAULT 'Pending',
  `payment_gateway_response_log` text NOT NULL,
  `signature` varchar(200) NOT NULL DEFAULT ' '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deposit_money_transactions`
--

INSERT INTO `deposit_money_transactions` (`id`, `players_id`, `comment`, `credits_count`, `transaction_id`, `payment_gateway_id`, `bonus_code`, `created_at`, `updated_at`, `status`, `transaction_status`, `payment_gateway_response_log`, `signature`) VALUES
(1, 19, 'Transaction Initiated at 19-02-2019 10:51:12 AM Transaction Success at 19-02-2019 10:51:19 AM', 500, 'DP1902201910175921', '66966', '', '2019-02-19 10:51:12', '', 1, 'Success', '{\"orderId\":\"DP1902201910175921\",\"orderAmount\":\"500.00\",\"referenceId\":\"66966\",\"txStatus\":\"SUCCESS\",\"paymentMode\":\"NET_BANKING\",\"txMsg\":\"Transaction Successful\",\"txTime\":\"2019-02-19 10:51:14\",\"signature\":\"DJ396JT\\/Gge7JDk4yMJg0UpdvkA7eLdF9BG5JhUGa0c=\"}', 'XY/34i6dph0lSC9eZchrsTwx03Y3pIJY4Y74OKLJVGM='),
(2, 19, 'Transaction Initiated at 19-02-2019 10:51:39 AM Transaction Success at 19-02-2019 10:51:46 AM', 250, 'DP1902201910853955', '66967', '', '2019-02-19 10:51:38', '', 1, 'Success', '{\"orderId\":\"DP1902201910853955\",\"orderAmount\":\"250.00\",\"referenceId\":\"66967\",\"txStatus\":\"SUCCESS\",\"paymentMode\":\"NET_BANKING\",\"txMsg\":\"Transaction Successful\",\"txTime\":\"2019-02-19 10:51:41\",\"signature\":\"mzH2\\/AS9zxOejMillF23hhaUFlY7CxRCfuEhJPlShG8=\"}', 'j9Y9makOE5eUgupvGvju1MUhtsupOnD0roizIBm+bZU='),
(3, 19, 'Transaction Initiated at 19-02-2019 10:51:55 AM Transaction Success at 19-02-2019 10:52:00 AM', 250, 'DP1902201910609356', '66968', '', '2019-02-19 10:51:55', '', 1, 'Success', '{\"orderId\":\"DP1902201910609356\",\"orderAmount\":\"250.00\",\"referenceId\":\"66968\",\"txStatus\":\"SUCCESS\",\"paymentMode\":\"NET_BANKING\",\"txMsg\":\"Transaction Successful\",\"txTime\":\"2019-02-19 10:51:55\",\"signature\":\"rnmfIinqG\\/bbQhQXiNnlcxKOuDBEtX3dChllmeKYyPI=\"}', '7mrEGbXgR9xMoIFTYg4X3q5/sOEr9NO5OaLew162ikk='),
(4, 19, 'Transaction Initiated at 19-02-2019 10:56:57 AM Transaction Success at 19-02-2019 10:57:04 AM', 500, 'DP1902201910753490', '66969', '', '2019-02-19 10:56:57', '', 1, 'Success', '{\"orderId\":\"DP1902201910753490\",\"orderAmount\":\"500.00\",\"referenceId\":\"66969\",\"txStatus\":\"SUCCESS\",\"paymentMode\":\"NET_BANKING\",\"txMsg\":\"Transaction Successful\",\"txTime\":\"2019-02-19 10:56:58\",\"signature\":\"rL\\/guQ4UVzuYmsKdy5Fx9K19wbOXOXWZGl1UAMkoAMQ=\"}', '+RtF7fWkyQm1FIVKG91xUSGife11k60kBTTQPRsXqgQ='),
(5, 19, 'Transaction Initiated at 19-02-2019 04:21:02 PM Transaction Success at 19-02-2019 04:21:09 PM Transaction Success at 19-02-2019 04:26:36 PM', 250, 'DP1902201916332962', '67098', '', '2019-02-19 16:21:02', '', 1, 'Success', '{\"orderId\":\"DP1902201916332962\",\"orderAmount\":\"250.00\",\"referenceId\":\"67098\",\"txStatus\":\"SUCCESS\",\"paymentMode\":\"NET_BANKING\",\"txMsg\":\"Transaction Successful\",\"txTime\":\"2019-02-19 16:21:04\",\"signature\":\"BDkHz+5JxH3JnQxds\\/ahQSvpLlC0gy1\\/4eDlwUoL0Rw=\"}', 'obIkINIPgb6283Mau3FBpe3hcPydhfmbjdkndvlVNx4='),
(6, 19, 'Transaction Initiated at 19-02-2019 04:32:43 PM Transaction Success at 19-02-2019 04:35:10 PM', 100, 'DP1902201916856775', '67107', '', '2019-02-19 16:32:43', '', 1, 'Success', '{\"orderId\":\"DP1902201916856775\",\"orderAmount\":\"100.00\",\"referenceId\":\"67107\",\"txStatus\":\"SUCCESS\",\"paymentMode\":\"NET_BANKING\",\"txMsg\":\"Transaction Successful\",\"txTime\":\"2019-02-19 16:33:37\",\"signature\":\"vHoeeICNOnP9ORiEp7hZL1HQPvoVy1gp583FlhN3Bu0=\"}', '94UbhOaOdqQgpI6hlWpADKZqHviz+E70EyZoWEQvLOg='),
(7, 27, 'Transaction Initiated at 19-02-2019 04:58:35 PM Transaction Success at 19-02-2019 04:58:45 PM', 250, 'DP1902201916738982', '67117', '', '2019-02-19 16:58:34', '', 1, 'Success', '{\"orderId\":\"DP1902201916738982\",\"orderAmount\":\"250.00\",\"referenceId\":\"67117\",\"txStatus\":\"SUCCESS\",\"paymentMode\":\"NET_BANKING\",\"txMsg\":\"Transaction Successful\",\"txTime\":\"2019-02-19 16:58:39\",\"signature\":\"jMqagD2zbLFeJ06GC8oJKeGi\\/36RtFGEFr4ZMS54z4g=\"}', 'C6CfcLuJbB2hiamTotYSZcIAonKB4V8ADuxs/N7C7bs='),
(8, 27, 'Transaction Initiated at 19-02-2019 05:00:11 PM Transaction Success at 19-02-2019 05:00:18 PM', 500, 'DP1902201917267346', '67118', '', '2019-02-19 17:00:11', '', 1, 'Success', '{\"orderId\":\"DP1902201917267346\",\"orderAmount\":\"500.00\",\"referenceId\":\"67118\",\"txStatus\":\"SUCCESS\",\"paymentMode\":\"NET_BANKING\",\"txMsg\":\"Transaction Successful\",\"txTime\":\"2019-02-19 17:00:13\",\"signature\":\"qusjVkzxvWQOXraP4gEn1u7iQrLvXt8P18pZ3FTH9IE=\"}', 'XOxfidE7mbTnKnNQPJX/AdaiFKJaVK2njVLkrcpKQeg='),
(9, 27, 'Transaction Initiated at 19-02-2019 05:22:49 PM Transaction Success at 19-02-2019 05:22:57 PM', 100000, 'DP1902201917719401', '67125', '', '2019-02-19 17:22:49', '', 1, 'Success', '{\"orderId\":\"DP1902201917719401\",\"orderAmount\":\"100000.00\",\"referenceId\":\"67125\",\"txStatus\":\"SUCCESS\",\"paymentMode\":\"NET_BANKING\",\"txMsg\":\"Transaction Successful\",\"txTime\":\"2019-02-19 17:22:52\",\"signature\":\"3dEWpMzhNhogxo4SHTvoh5XloLU1r13mSEPgsr4JVuw=\"}', '10zEM2SwE55o/viX41lnd7Swn7HmmRHwMeSSdzVLyic=');

-- --------------------------------------------------------

--
-- Table structure for table `games`
--

CREATE TABLE `games` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `active` bit(1) NOT NULL,
  `_uuid` varchar(255) NOT NULL,
  `deals` enum('2','3','6') NOT NULL DEFAULT '2',
  `entry_fee` bigint(20) UNSIGNED NOT NULL,
  `game_sub_type` enum('Points','Pool','Deals') NOT NULL DEFAULT 'Points',
  `game_title` varchar(60) NOT NULL,
  `game_type` enum('Cash','Practice') NOT NULL DEFAULT 'Cash',
  `number_of_cards` enum('13','21') NOT NULL DEFAULT '13',
  `number_of_deck` enum('1','2') NOT NULL DEFAULT '1',
  `point_value` bigint(20) UNSIGNED NOT NULL,
  `pool_deal_prize` bigint(20) UNSIGNED NOT NULL,
  `pool_game_type` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `reward_points` bigint(20) UNSIGNED NOT NULL,
  `seats` enum('2','6') NOT NULL DEFAULT '2',
  `vip` bit(1) NOT NULL DEFAULT b'0',
  `token` varchar(100) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `games`
--

INSERT INTO `games` (`id`, `active`, `_uuid`, `deals`, `entry_fee`, `game_sub_type`, `game_title`, `game_type`, `number_of_cards`, `number_of_deck`, `point_value`, `pool_deal_prize`, `pool_game_type`, `reward_points`, `seats`, `vip`, `token`, `created_at`, `updated_at`, `status`) VALUES
(1, b'1', 'KhpZNACADHuDmOQ8yF1wktTNgXMprj', '2', 8000, 'Points', 'Points (13)', 'Cash', '13', '1', 100, 0, 0, 0, '2', b'0', 'KhpZNACADHuDmOQ8yF1wktTNgXMprj', '1549274063', '1550578236', 1),
(3, b'1', '1gBGRvU9PXwLOIOsthjId17Hk8rWKu', '2', 80, 'Points', 'Points (13)', 'Cash', '13', '1', 10, 0, 0, 0, '2', b'0', 'eMgicVOo4W8WdOJ3lwz2RqEpevvjRd', '1549633259', '1550578226', 1),
(4, b'1', 'PwKbBDdMzNxiEFPiB9h5aF1QN4TPkS', '2', 8000, 'Points', 'Points (13)', 'Practice', '13', '1', 100, 0, 0, 0, '2', b'0', '0ZnXyAOMweTNC7kPSuRdHaqXEi0lIQ', '1549691042', '1550578247', 1),
(5, b'0', 'PwKbBDdMzNxiEFPiB9hD5aF1SDFQN4TPkS', '3', 8000, 'Points', 'Points (13)', 'Practice', '13', '1', 100, 0, 0, 0, '2', b'1', '0ZnXyAOMweTNC7kPSuRdHaqXSDFEi0lIQ', '1549691042', '1550578261', 1),
(6, b'1', 'TVhpDNmN46Q6H8UiJ0MjR2LTQNwe0W', '2', 100, 'Pool', 'Pool (13)', 'Practice', '13', '1', 0, 500, 101, 0, '2', b'0', '3Mq9OUUkHRsDAd2xNigYlDCc7Dyvki', '1549969177', '1550034245', 1);

-- --------------------------------------------------------

--
-- Table structure for table `game_play`
--

CREATE TABLE `game_play` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `game_detail_json` longtext,
  `games_id` bigint(20) NOT NULL DEFAULT '0',
  `rooms_id` bigint(20) NOT NULL DEFAULT '0',
  `game_sub_type` enum('Points','Pool','Deals') NOT NULL DEFAULT 'Points',
  `game_type` enum('Practice','Cash') NOT NULL DEFAULT 'Practice',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `global_configuration_constants`
--

CREATE TABLE `global_configuration_constants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key_name` varchar(100) NOT NULL,
  `value` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `updated_at` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `global_configuration_constants`
--

INSERT INTO `global_configuration_constants` (`id`, `key_name`, `value`, `status`, `updated_at`) VALUES
(1, 'SUPPORT_EMAIL', 'support@mrrummy.com', 1, '1549689071'),
(2, 'CONTACT_EMAIL', 'support@mrrummy.com', 1, '1549689071'),
(5, 'NO_REPLAY_MAIL', 'no_replfdsfdsfdafdssay@mrrummy.com', 1, '1549694533'),
(6, 'BONUS_ON_EMAIL_VERIFIED', '100', 1, '1549689071'),
(7, 'BONUS_ON_MOBILE_VERIFIED', '100', 1, '1549689071'),
(8, 'BONUS_ON_REFER_FRIEND', '100', 1, '1549689071'),
(9, 'MAX_FUN_CHIPS', '10000', 1, '1549689071'),
(10, 'SITE_TITLE', 'CLover Rummy', 1, '1549689071'),
(11, 'BCC_EMAIL', 'manikanta@thecolourmoon.com', 1, '1549689071'),
(12, 'BIRTH_DAY_BONUS_PERCENTAGE', '10', 1, '1549689071'),
(13, 'GST_PERCENTAGE', '10', 1, '1549689071'),
(14, 'TDS_PERCENTAGE', '10', 1, '1549689071'),
(15, 'ADMIN_COMMISSION_PERCENTAGE', '10', 1, '1549689071');

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `table_name` varchar(100) NOT NULL,
  `table_primary_key_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `action` varchar(250) NOT NULL,
  `created_at` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `table_name`, `table_primary_key_id`, `user_id`, `action`, `created_at`) VALUES
(1, 'users', 2, 2, 'update', '1549005458'),
(2, 'states', 4, 2, 'add', '1549007624'),
(3, 'states', 4, 2, 'update', '1549007756'),
(4, 'states', 4, 2, 'update', '1549007778'),
(5, 'states', 4, 2, 'update', '1549007794'),
(6, 'states', 4, 2, 'delete', '1549007817'),
(7, 'modules', 4, 2, 'add', '1549008744'),
(8, 'roles', 8, 2, 'update', '1549013540'),
(9, 'roles', 8, 2, 'delete', '1549013544'),
(10, 'roles', 17, 2, 'delete', '1549013551'),
(11, 'users', 3, 2, 'update', '1549014824'),
(12, 'modules', 4, 2, 'update', '1549015014'),
(13, 'modules', 7, 2, 'add', '1549015033'),
(14, 'players', 3, 2, 'update', '1549020170'),
(15, 'players', 4, 2, 'add', '1549020862'),
(16, 'players', 4, 2, 'update', '1549020902'),
(17, 'players', 4, 2, 'update', '1549020945'),
(18, 'modules', 8, 2, 'add', '1549025263'),
(19, 'modules', 9, 2, 'add', '1549025273'),
(20, 'players', 5, 2, 'add', '1549027817'),
(21, 'players', 5, 2, 'update', '1549027833'),
(22, 'modules', 10, 2, 'add', '1549261252'),
(23, 'games', 1, 2, 'add', '1549274063'),
(24, 'games', 1, 2, 'update', '1549275382'),
(25, 'games', 1, 2, 'update', '1549275531'),
(26, 'modules', 11, 2, 'add', '1549275772'),
(27, 'modules', 12, 2, 'add', '1549275918'),
(28, 'modules', 13, 2, 'add', '1549275934'),
(29, 'users', 2, 2, 'update', '1549350982'),
(30, 'players', 3, 2, 'update', '1549351672'),
(31, 'players', 8, 2, 'update', '1549430206'),
(32, 'games', 2, 2, 'add', '1549631934'),
(33, 'games', 1, 2, 'update', '1549631954'),
(34, 'games', 1, 2, 'update', '1549631960'),
(35, 'games', 1, 2, 'update', '1549632105'),
(36, 'games', 1, 2, 'update', '1549632206'),
(37, 'games', 1, 2, 'update', '1549632339'),
(38, 'games', 2, 2, 'update', '1549632417'),
(39, 'games', 2, 2, 'update', '1549632458'),
(40, 'games', 1, 2, 'update', '1549632486'),
(41, 'games', 3, 2, 'add', '1549633281'),
(42, 'modules', 14, 2, 'add', '1549688105'),
(43, 'games', 4, 2, 'add', '1549691064'),
(44, 'global_configuration_constants', 5, 2, 'update', '1549694524'),
(45, 'global_configuration_constants', 5, 2, 'update', '1549694533'),
(46, 'games', 6, 2, 'add', '1549969177'),
(47, 'games', 6, 2, 'update', '1550033740'),
(49, 'games', 6, 2, 'update', '1550034039'),
(50, 'games', 6, 2, 'update', '1550034200'),
(51, 'games', 6, 2, 'update', '1550034245'),
(52, 'modules', 15, 2, 'add', '1550034367'),
(53, 'modules', 16, 2, 'add', '1550034796'),
(54, 'modules', 17, 2, 'add', '1550038362'),
(55, 'modules', 18, 2, 'add', '1550038373'),
(56, 'players', 28, 2, 'add', '1550050248'),
(57, 'players', 29, 2, 'add', '1550050266'),
(58, 'players', 30, 2, 'add', '1550050289'),
(59, 'bonus_coupons', 1, 2, 'update', '1550051697'),
(60, 'bonus_coupons', 2, 2, 'add', '1550052546'),
(61, 'bonus_coupons', 2, 2, 'update', '1550053201'),
(62, 'bonus_coupons', 1, 2, 'update', '1550053790'),
(63, 'bonus_coupons', 2, 2, 'update', '1550053794'),
(64, 'bonus_coupons', 1, 2, 'update', '1550053815'),
(65, 'bonus_coupons', 1, 2, 'update', '1550053834'),
(66, 'players', 30, 2, 'update', '1550226394'),
(67, 'players', 30, 2, 'update', '1550226628'),
(68, 'players', 30, 2, 'update', '1550226632'),
(69, 'players', 30, 2, 'update', '1550226637'),
(70, 'players', 30, 2, 'update', '1550227202'),
(71, 'players', 30, 2, 'update', '1550227247'),
(72, 'games', 3, 2, 'update', '1550578226'),
(73, 'games', 1, 2, 'update', '1550578236'),
(74, 'games', 4, 2, 'update', '1550578247'),
(75, 'games', 5, 2, 'update', '1550578261');

-- --------------------------------------------------------

--
-- Table structure for table `join_play_transactions`
--

CREATE TABLE `join_play_transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `players_id` bigint(20) UNSIGNED NOT NULL,
  `games_id` bigint(20) UNSIGNED NOT NULL,
  `rooms_id` bigint(20) NOT NULL,
  `bid_amount` double NOT NULL,
  `game_type` enum('Cash','Practice') NOT NULL DEFAULT 'Practice',
  `taken_from_deposit_balance` bigint(20) NOT NULL,
  `taken_from_withdrawal_balance` bigint(20) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `join_play_transactions`
--

INSERT INTO `join_play_transactions` (`id`, `players_id`, `games_id`, `rooms_id`, `bid_amount`, `game_type`, `taken_from_deposit_balance`, `taken_from_withdrawal_balance`, `created_date_time`) VALUES
(1, 19, 0, 1, 100, 'Practice', 100, 0, '2019-02-12 16:34:54'),
(2, 19, 0, 1, 50, 'Practice', 50, 0, '2019-02-12 16:35:19'),
(3, 19, 0, 1, 50, 'Practice', 50, 0, '2019-02-12 16:38:27'),
(4, 19, 0, 1, 50, 'Practice', 50, 0, '2019-02-12 16:39:18'),
(5, 19, 0, 1, 50, 'Practice', 50, 0, '2019-02-12 16:39:32'),
(6, 19, 0, 1, 50, 'Practice', 50, 0, '2019-02-12 16:40:07'),
(7, 19, 0, 1, 50, 'Practice', 50, 0, '2019-02-12 16:40:34'),
(8, 19, 0, 1, 50, 'Practice', 50, 0, '2019-02-12 16:41:28'),
(9, 22, 4, 1, 100, 'Practice', 100, 0, '2019-02-13 16:00:30'),
(10, 22, 6, 2, 100, 'Practice', 100, 0, '2019-02-13 16:01:30'),
(11, 22, 4, 3, 100, 'Practice', 100, 0, '2019-02-13 16:01:37'),
(12, 27, 4, 3, 100, 'Practice', 100, 0, '2019-02-13 16:04:39'),
(13, 22, 4, 4, 100, 'Practice', 100, 0, '2019-02-13 16:10:40'),
(14, 31, 4, 4, 100, 'Practice', 100, 0, '2019-02-13 16:10:56'),
(15, 22, 4, 5, 100, 'Practice', 100, 0, '2019-02-13 16:14:18'),
(16, 22, 4, 6, 100, 'Practice', 100, 0, '2019-02-13 16:16:41'),
(17, 27, 6, 7, 100, 'Practice', 100, 0, '2019-02-13 16:32:26'),
(18, 22, 6, 7, 100, 'Practice', 100, 0, '2019-02-13 16:32:31'),
(19, 27, 4, 8, 100, 'Practice', 100, 0, '2019-02-13 16:43:04'),
(20, 22, 4, 8, 100, 'Practice', 100, 0, '2019-02-13 16:43:08'),
(21, 27, 4, 9, 100, 'Practice', 100, 0, '2019-02-13 16:53:31'),
(22, 22, 4, 9, 100, 'Practice', 100, 0, '2019-02-13 16:53:46'),
(23, 27, 4, 10, 100, 'Practice', 100, 0, '2019-02-13 16:55:21'),
(24, 22, 4, 10, 100, 'Practice', 100, 0, '2019-02-13 16:55:40'),
(25, 22, 6, 11, 100, 'Practice', 100, 0, '2019-02-13 16:56:10'),
(26, 27, 6, 11, 100, 'Practice', 100, 0, '2019-02-13 16:56:36'),
(28, 22, 4, 13, 100, 'Practice', 100, 0, '2019-02-13 17:57:49'),
(29, 27, 4, 13, 100, 'Practice', 100, 0, '2019-02-13 17:57:56'),
(30, 27, 4, 14, 100, 'Practice', 100, 0, '2019-02-13 18:33:16'),
(31, 22, 4, 14, 100, 'Practice', 100, 0, '2019-02-13 18:33:59'),
(32, 22, 6, 15, 100, 'Practice', 100, 0, '2019-02-14 15:29:41'),
(33, 22, 4, 16, 100, 'Practice', 100, 0, '2019-02-14 15:44:37'),
(34, 22, 4, 17, 100, 'Practice', 100, 0, '2019-02-14 15:54:41'),
(35, 22, 6, 18, 100, 'Practice', 100, 0, '2019-02-14 16:03:02'),
(36, 22, 4, 19, 100, 'Practice', 100, 0, '2019-02-14 16:06:52'),
(37, 22, 4, 20, 100, 'Practice', 100, 0, '2019-02-14 16:16:56'),
(38, 22, 4, 21, 100, 'Practice', 100, 0, '2019-02-14 16:21:50'),
(39, 22, 4, 22, 100, 'Practice', 100, 0, '2019-02-14 16:23:07'),
(40, 22, 4, 23, 100, 'Practice', 100, 0, '2019-02-14 16:23:59'),
(41, 22, 4, 24, 100, 'Practice', 100, 0, '2019-02-14 16:26:01'),
(42, 22, 4, 25, 100, 'Practice', 100, 0, '2019-02-14 16:29:57'),
(43, 27, 4, 26, 100, 'Practice', 100, 0, '2019-02-14 16:45:35'),
(44, 27, 4, 27, 100, 'Practice', 100, 0, '2019-02-14 16:59:20'),
(45, 27, 4, 28, 100, 'Practice', 100, 0, '2019-02-14 17:03:27'),
(47, 27, 4, 30, 100, 'Practice', 100, 0, '2019-02-14 17:09:21'),
(48, 22, 4, 31, 100, 'Practice', 100, 0, '2019-02-14 17:28:37'),
(51, 27, 4, 34, 100, 'Practice', 100, 0, '2019-02-14 17:46:37'),
(52, 27, 4, 35, 100, 'Practice', 100, 0, '2019-02-14 18:23:11'),
(53, 27, 4, 37, 100, 'Practice', 100, 0, '2019-02-15 14:08:59'),
(54, 27, 4, 38, 100, 'Practice', 100, 0, '2019-02-15 14:16:53'),
(55, 27, 4, 39, 100, 'Practice', 100, 0, '2019-02-15 14:21:48'),
(56, 27, 4, 40, 100, 'Practice', 100, 0, '2019-02-15 14:37:28'),
(57, 28, 4, 40, 100, 'Practice', 100, 0, '2019-02-15 14:37:34'),
(58, 27, 4, 41, 100, 'Practice', 100, 0, '2019-02-15 14:46:15'),
(59, 28, 4, 41, 100, 'Practice', 100, 0, '2019-02-15 14:46:21'),
(60, 27, 4, 42, 100, 'Practice', 100, 0, '2019-02-15 14:53:09'),
(61, 28, 4, 42, 100, 'Practice', 100, 0, '2019-02-15 14:53:16'),
(62, 27, 4, 43, 100, 'Practice', 100, 0, '2019-02-15 14:56:53'),
(63, 28, 4, 43, 100, 'Practice', 100, 0, '2019-02-15 14:56:59'),
(64, 27, 4, 44, 100, 'Practice', 100, 0, '2019-02-15 15:03:27'),
(65, 28, 4, 44, 100, 'Practice', 100, 0, '2019-02-15 15:03:33'),
(66, 27, 4, 45, 100, 'Practice', 100, 0, '2019-02-15 15:09:56'),
(67, 28, 4, 45, 100, 'Practice', 100, 0, '2019-02-15 15:10:02'),
(68, 27, 4, 46, 100, 'Practice', 100, 0, '2019-02-15 15:22:04'),
(69, 28, 4, 46, 100, 'Practice', 100, 0, '2019-02-15 15:22:10'),
(70, 27, 4, 47, 100, 'Practice', 100, 0, '2019-02-15 15:24:50'),
(71, 28, 4, 47, 100, 'Practice', 100, 0, '2019-02-15 15:24:56'),
(72, 28, 4, 47, 100, 'Practice', 100, 0, '2019-02-15 15:30:13'),
(73, 27, 4, 48, 100, 'Practice', 100, 0, '2019-02-15 17:00:55'),
(74, 28, 4, 48, 100, 'Practice', 100, 0, '2019-02-15 17:01:01'),
(75, 22, 4, 49, 100, 'Practice', 100, 0, '2019-02-15 17:59:29'),
(76, 27, 4, 50, 100, 'Practice', 100, 0, '2019-02-16 11:38:11'),
(77, 28, 4, 50, 100, 'Practice', 100, 0, '2019-02-16 11:38:17'),
(78, 27, 4, 51, 100, 'Practice', 100, 0, '2019-02-16 11:42:06'),
(79, 28, 4, 51, 100, 'Practice', 100, 0, '2019-02-16 11:42:12');

-- --------------------------------------------------------

--
-- Table structure for table `login_logs`
--

CREATE TABLE `login_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `ip_address` varchar(25) NOT NULL,
  `address` text NOT NULL,
  `created_at` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `module_name` varchar(100) NOT NULL,
  `parent_module_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Modules table primary key',
  `display_order` tinyint(4) NOT NULL DEFAULT '10',
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `module_name`, `parent_module_id`, `display_order`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Master Menu', 0, 10, '1549006266', '1549006266', b'1'),
(2, 'States', 1, 10, '1549006266', '1549006266', b'1'),
(3, 'Modules', 1, 10, '1549006266', '1549006266', b'1'),
(4, 'Roles & Employees', 1, 1, '1549008744', '1549015014', b'1'),
(5, 'Roles', 4, 2, '1549008744', '', b'1'),
(6, 'Employees', 4, 3, '1549008744', '', b'1'),
(7, 'Players', 0, 1, '1549015033', '', b'1'),
(8, 'Real Players', 7, 1, '1549025263', '', b'1'),
(9, 'Bot Players', 7, 2, '1549025273', '', b'1'),
(10, 'Game', 1, 4, '1549261252', '', b'1'),
(11, 'Game Play Data', 0, 3, '1549275772', '', b'1'),
(12, 'Practice Games', 11, 1, '1549275917', '', b'1'),
(13, 'Cash Games', 11, 2, '1549275934', '', b'1'),
(14, 'Global Configurations', 1, 15, '1549688105', '', b'1'),
(15, 'Feedback', 0, 10, '1550034367', '', b'1'),
(16, 'Reported Problems', 15, 1, '1550034796', '', b'1'),
(17, 'Promotion System', 0, 10, '1550038362', '', b'1'),
(18, 'Bonus System', 17, 1, '1550038373', '', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE `players` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(20) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `new_email` varchar(100) NOT NULL COMMENT 'This is temp column when user updated with new mail value will be stored, once verified then new email updated to permanent email',
  `mobile` varchar(100) NOT NULL,
  `new_mobile` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `salt` varchar(100) NOT NULL,
  `gender` enum('Male','Female','Other') DEFAULT 'Male',
  `date_of_birth` date NOT NULL,
  `address_line_1` varchar(200) NOT NULL,
  `address_line_2` varchar(200) NOT NULL,
  `states_id` bigint(20) UNSIGNED DEFAULT '0',
  `city` varchar(50) NOT NULL,
  `pin_code` varchar(10) NOT NULL,
  `my_referral_code` varchar(100) NOT NULL,
  `referred_by_code` varchar(100) NOT NULL,
  `access_token` varchar(150) NOT NULL,
  `welcome_message_seen` bit(1) NOT NULL DEFAULT b'0',
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `last_login` varchar(100) NOT NULL,
  `email_verification_key` varchar(100) NOT NULL,
  `email_verified` bit(1) NOT NULL DEFAULT b'0',
  `mobile_verified` bit(1) NOT NULL DEFAULT b'0',
  `password_reset_key` varchar(100) NOT NULL,
  `facebook_account_id` varchar(50) NOT NULL,
  `wallet_account_id` bigint(20) UNSIGNED NOT NULL,
  `bot_player` bit(1) NOT NULL DEFAULT b'0',
  `bot_status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `expertise_level` enum('JOKER','CLUB','HEART','DIAMOND','SAPDE') NOT NULL DEFAULT 'JOKER',
  `play_token` varchar(100) NOT NULL,
  `otp` int(6) UNSIGNED DEFAULT '0',
  `address_proof_type` varchar(100) NOT NULL,
  `address_proof` varchar(100) NOT NULL,
  `pan_card_number` varchar(100) NOT NULL,
  `pan_card` varchar(100) NOT NULL,
  `address_proof_verified_at` varchar(25) NOT NULL DEFAULT ' ',
  `pan_card_verified_at` varchar(25) NOT NULL DEFAULT ' ',
  `address_proof_rejected_at` varchar(25) NOT NULL DEFAULT ' ',
  `pan_card_rejected_at` varchar(25) NOT NULL DEFAULT ' ',
  `address_proof_rejected_reason` varchar(250) NOT NULL,
  `pan_card_rejected_reason` varchar(250) NOT NULL,
  `address_proof_status` enum('Pending Approval','Rejected','Approved','Not Submitted') DEFAULT 'Not Submitted',
  `pan_card_status` enum('Pending Approval','Rejected','Approved','Not Submitted') DEFAULT 'Not Submitted',
  `contact_by_sms` bit(1) NOT NULL DEFAULT b'1',
  `contact_by_phone` bit(1) NOT NULL DEFAULT b'1',
  `contact_by_email` bit(1) NOT NULL DEFAULT b'1',
  `speak_language` varchar(20) NOT NULL DEFAULT 'Hindi',
  `sms_subscriptions` bit(1) NOT NULL DEFAULT b'1',
  `newsletter_subscriptions` bit(1) NOT NULL DEFAULT b'1',
  `forgot_password_verification_link` varchar(100) NOT NULL DEFAULT ' ',
  `device_type` varchar(100) NOT NULL DEFAULT '',
  `device_token` varchar(100) NOT NULL,
  `profile_pic` varchar(100) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `players`
--

INSERT INTO `players` (`id`, `username`, `email`, `new_email`, `mobile`, `new_mobile`, `password`, `salt`, `gender`, `date_of_birth`, `address_line_1`, `address_line_2`, `states_id`, `city`, `pin_code`, `my_referral_code`, `referred_by_code`, `access_token`, `welcome_message_seen`, `firstname`, `lastname`, `last_login`, `email_verification_key`, `email_verified`, `mobile_verified`, `password_reset_key`, `facebook_account_id`, `wallet_account_id`, `bot_player`, `bot_status`, `expertise_level`, `play_token`, `otp`, `address_proof_type`, `address_proof`, `pan_card_number`, `pan_card`, `address_proof_verified_at`, `pan_card_verified_at`, `address_proof_rejected_at`, `pan_card_rejected_at`, `address_proof_rejected_reason`, `pan_card_rejected_reason`, `address_proof_status`, `pan_card_status`, `contact_by_sms`, `contact_by_phone`, `contact_by_email`, `speak_language`, `sms_subscriptions`, `newsletter_subscriptions`, `forgot_password_verification_link`, `device_type`, `device_token`, `profile_pic`, `created_at`, `updated_at`, `status`) VALUES
(19, 'manikanta', 'manikanta@thecolourmoon.com', 'manikanta@thecolourmoon.com', '9553517603', '9553517603', '15d2f24434ae9bb4a4e938f06b2ca88f', '7YDKbBUQFk', 'Male', '2019-11-01', '', '', 0, '', '', 'bXbgi6', '', '09HXdIeAeJkP1w0vbrKEEFJgLXTiBSvo', b'1', '', '', '2019-02-19 10:43:18', '1Z4bg2No8n5T0XrbCSSG7IzHRCI3pG', b'0', b'0', '', '', 22, b'0', 'Active', 'JOKER', '40rpnFKIzD3I1ZQxzg5zUaI4jsnsaVKlH6BKCPM8XoDDrCaGnC', 0, 'Aadhaar Card', 'address_proof1550229679240638582163392830202230690521.jpg', 'sdafdsfsda', 'address_proof15502296792406385821633928302022306905211.jpg', '2019-11-01 01:11:11', '2019-11-01 01:11:11', '2019-11-01 01:11:11', '2019-11-01 01:11:11', '', '', 'Pending Approval', 'Pending Approval', b'1', b'1', b'1', 'Hindi', b'1', b'1', '', '', '', '', '1549606961', '1550553198', b'1'),
(20, 'player1', 'player1@player.com', 'player1@player.com', '1234567890', '1234567890', '736b5d4a771212cf60642c5e4de563d0', 'qsC3e3lrYx', 'Male', '2019-11-01', '', '', 0, '', '', '0jfip4', '', 'eCJLAO5CWGWdhkDynu6uXaisHcGf8Vgq', b'0', '', '', '2019-02-08 13:43:37', 'wlZFOopKdxKkd1DxMCDpzxGb9czXBU', b'0', b'0', '', '', 23, b'0', 'Active', 'JOKER', '', 0, '', '', '', '', '2019-11-01 01:11:11', '2019-11-01 01:11:11', '2019-11-01 01:11:11', '2019-11-01 01:11:11', '', '', 'Pending Approval', 'Pending Approval', b'1', b'1', b'1', 'Hindi', b'1', b'1', ' ', 'UNITY_WEBGL', '', '', '1549611062', '', b'1'),
(21, 'player2', 'player2@player.com', 'player2@player.com', '1234567891', '1234567891', '8e4df454aa21140692395dbb1dd0b6a3', 'GT0nhURFXy', 'Male', '2019-11-01', '', '', 0, '', '', 'oWZnVZ', '', 'kFq8BNORv1qHBcW4TgEYN3jZACOQNg1J', b'0', '', '', '2019-11-01 01:11:11', 'JrlEMG83LucvJIa121g4P6yFmbHqWY', b'0', b'0', '', '', 24, b'0', 'Active', 'JOKER', '', 0, '', '', '', '', '2019-11-01 01:11:11', '2019-11-01 01:11:11', '2019-11-01 01:11:11', '2019-11-01 01:11:11', '', '', 'Pending Approval', 'Pending Approval', b'1', b'1', b'1', 'Hindi', b'1', b'1', ' ', 'UNITY_WEBGL', '', '', '1549611120', '', b'1'),
(22, 'pushpendra', 'pushpendra@thecolourmoon.com', 'pushpendra@thecolourmoon.com', '9719325299', '9719325299', '8e90166bb37200a4a598b32e5834526d', 'bkNVfinbKH', 'Male', '2019-02-08', '', '', 0, '', '', 'pJ6utF', '', 'FyeZQJSJw56yZxJXLVguH8ERRWyVcURn', b'0', '', '', '2019-02-19 16:27:05', 'Tu6rbSKsh6YhZzFlO3jEX26GgdkkCJ', b'0', b'0', '', '', 25, b'0', 'Active', 'JOKER', 'vgxjycjWlwziAilrvE9H6G54vBz0Qj5QSZ8wunYxkcLJPSx00Q', 0, '', '', '', '', '', '', '', '', '', '', 'Pending Approval', 'Pending Approval', b'1', b'1', b'1', 'Hindi', b'1', b'1', '', 'UNITY_WEBGL', '', '', '1549618387', '1550573825', b'1'),
(23, 'pushpendra1', 'pushpendra1@thecolourmoon.com', 'pushpendra1@thecolourmoon.com', '1230456456', '1230456456', '270cada13148218a01b1a819bb8bebf7', 'FM1apNiE7Y', 'Male', '2019-02-08', '', '', 0, '', '', 'TaWnId', '', 'VmP1fcoDuGCoVHfLbzX9alDXMQMTI48A', b'0', '', '', '2019-02-09 15:29:51', 'qJ3V2I5paZippXoY0YMgNZVr1p4dRp', b'0', b'0', '', '', 26, b'0', 'Active', 'JOKER', '', 0, '', '', '', '', '', '', '', '', '', '', 'Pending Approval', 'Pending Approval', b'1', b'1', b'1', 'Hindi', b'1', b'1', ' ', 'UNITY_WEBGL', '', '', '1549619285', '', b'1'),
(24, 'pushpendra2', 'pushpendra2@thecolourmoon.com', 'pushpendra2@thecolourmoon.com', '1234567892', '1234567892', '6d8368052a4f4d6ad7d706f308adda32', 'BF83dX7gyB', 'Male', '2019-02-08', '', '', 0, '', '', 'd1XvNV', '', 'YhHCo9P3S09JQfNtm2EFsxbfyoKzcYgS', b'0', '', '', '0000-00-00 00:00:00', '2W2vg7QmrphP5PKGf8nMVi5NWygZpD', b'0', b'0', '', '', 27, b'0', 'Active', 'JOKER', '', 0, '', '', '', '', '', '', '', '', '', '', 'Pending Approval', 'Pending Approval', b'1', b'1', b'1', 'Hindi', b'1', b'1', ' ', 'UNITY_WEBGL', '', '', '1549619379', '', b'1'),
(25, 'pushpendra3', 'pushpendra3@thecolourmoon.com', 'pushpendra3@thecolourmoon.com', '1234567893', '1234567893', '6a568e6f61d502a68fe43c1b865058ac', 'mqs7i2n1pz', 'Male', '2019-02-08', '', '', 0, '', '', 'Xo7UVN', '', 'xnui8jTA8IRk0AtZo3al9mtSoBPgRdsi', b'0', '', '', '', '8uECLJKdnNuBJRuGRVmeKbZkNh9uk7', b'0', b'0', '', '', 28, b'0', 'Active', 'JOKER', '', 0, '', '', '', '', ' ', ' ', ' ', ' ', '', '', 'Pending Approval', 'Pending Approval', b'1', b'1', b'1', 'Hindi', b'1', b'1', ' ', 'UNITY_WEBGL', '', '', '1549619643', '', b'1'),
(26, 'pushpendra4', 'pushpendra4@thecolourmoon.com', 'pushpendra4@thecolourmoon.com', '1234567894', '1234567894', 'f54e68a14a75b5508ec2a4fc9ddcba59', 'OdUQYnKtB7', 'Male', '2019-02-08', '', '', 0, '', '', 'JGHa96', '', 'wrkisN1RnRk2fyLd7DcWDSDSxtZFmUb2', b'0', '', '', '2019-02-09 15:30:20', 'RpENptx7I2UFwuobJqcyxkpLtSXASk', b'0', b'0', '', '', 29, b'0', 'Active', 'JOKER', '', 0, '', '', '', '', ' ', ' ', ' ', ' ', '', '', 'Pending Approval', 'Pending Approval', b'1', b'1', b'1', 'Hindi', b'1', b'1', ' ', 'UNITY_WEBGL', '', '', '1549626379', '', b'1'),
(27, 'player4', 'player1@thecolourmoon.com', 'player1@thecolourmoon.com', '1234567853', '1234567853', '1265c6d66c67f769a5f0a860a67554ff', 'j9xmX2XzOc', 'Male', '2019-02-11', '', '', 0, '', '', '60Auhf', '', 'qxtBURJjxYaS53Po5Mvqt30Eye23MUEL', b'0', '', '', '2019-02-19 17:24:12', 'CB0xxRkjVi7fmn4SDPfPVL1EJ7OOSs', b'0', b'0', '', '', 30, b'0', 'Active', 'JOKER', 'a4NrOm5xsTOtLzy3Aduxbsw2SgsXIftVlAgewWC7SLe5W8J7yZ', 0, '', '', '', '', ' ', ' ', ' ', ' ', '', '', 'Pending Approval', 'Pending Approval', b'1', b'1', b'1', 'Hindi', b'1', b'1', ' ', 'UNITY_WEBGL', '', '', '1549871037', '1550577253', b'1'),
(28, 'rameshb', NULL, '', '', '', '03b272008ec0ac011f22aae3e9db56b1', 'BSNrPDInjyWT1HRshuZTp4LNx', 'Male', '2019-02-13', '', '', 1, '', '', 'LDJYCL', '', '0Y5WOtKAlVuXFpkXWyfWdPNWwnrWIYMt', b'0', 'Ramesh', 'Bot', '', 'alUpQuVhj6PbYdz32aTWDFXSbFFGBG', b'1', b'1', '', '', 32, b'1', 'Active', 'JOKER', '', 0, '', '', '', '', ' ', ' ', ' ', ' ', '', '', 'Pending Approval', 'Pending Approval', b'1', b'1', b'1', 'Hindi', b'1', b'1', ' ', '', '', '', '1550050248', '', b'1'),
(29, 'krishb', NULL, '', '', '', '97e9163411a3695f2332ed252be2779f', 'ld89Kt90BUY9rwtVwWBGNyEUL', 'Male', '2019-02-13', '', '', 1, '', '', 'JpeHTT', '', 'jET0fVLIehRpMDCkjUfHpeFAMrdWeTKD', b'0', 'Krish', 'B', '', 'xve0ThpfAIGXXhlJHpDPWQphPMHCcK', b'1', b'1', '', '', 33, b'1', 'Active', 'JOKER', '', 0, '', '', '', '', ' ', ' ', ' ', ' ', '', '', 'Pending Approval', 'Pending Approval', b'1', b'1', b'1', 'Hindi', b'1', b'1', ' ', '', '', '', '1550050266', '', b'1'),
(30, 'piyush1', '', '', '', '', 'ff2f7dbddf037cac9a290c401533a35a', 'cQ1sV9pJaJTNf70GgT8QLAs9P', 'Male', '2019-02-15', '', '', 1, '', '', 'dBHdSW', '', 'UGPZ5JVsxZF8DOKm6Cj8B5YZOk0SFWa7', b'0', 'Piyush', 'Bot', '', 'fM83wrdwBVLgtICF961AwSy919fFYV', b'1', b'1', '', '', 34, b'1', 'Inactive', 'JOKER', '', 0, '', '', '', '', ' ', ' ', ' ', ' ', '', '', 'Pending Approval', 'Pending Approval', b'1', b'1', b'1', 'Hindi', b'1', b'1', ' ', '', '', '', '1550050289', '1550227247', b'1'),
(31, 'player11', 'playr11@gm.com', 'playr11@gm.com', '1234561234', '1234561234', 'b343365f1f105f6e3fa8d57f985a4562', 'WW6ZUlIl9D', 'Male', '2019-02-13', '', '', 0, '', '', 'wtqh1A', '', 'TvRuSDARm9Fosr5eK4hRxgqCgnPqHNqY', b'0', '', '', '2019-02-14 14:58:29', '4HJLig2hst0cVggbAMuDLn0iJVoNde', b'0', b'0', '', '', 35, b'0', 'Active', 'JOKER', 'yhSVLCRu60f20CdT6vltgGFxmNbpCz2FpMVLTMjMxiMzXiRHMN', 0, '', '', '', '', ' ', ' ', ' ', ' ', '', '', 'Pending Approval', 'Pending Approval', b'1', b'1', b'1', 'Hindi', b'1', b'1', ' ', 'UNITY_STANDALONE_WIN', '', '', '1550052698', '1550136510', b'1'),
(32, 'mukesh', 'mukesh@yopmail.com', 'mukesh@yopmail.com', '9133399110', '9133399110', '0cdb7407d8ff5bd441e14b1c3909d634', 'sLFMfgISf6', 'Male', '2019-02-18', '', '', 0, '', '', 'BxudpT', '', 'GPzcGwa8U9h8eNaEyUpSzfF37c7S1itp', b'1', '', '', '', 'MCOGJfjbheiNWhmnp20GiCRRHAfTb8', b'1', b'0', '', '', 36, b'0', 'Active', 'JOKER', 'qBWSgJGa1jydzzFSdoVW6u5lFHbt9JlS4U2bmDfSGiBfKcDWL2', 0, '', '', '', '', ' ', ' ', ' ', ' ', '', '', 'Not Submitted', 'Not Submitted', b'1', b'1', b'1', 'Hindi', b'1', b'1', ' ', '', '', '', '1550471113', '', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `player_login_logs`
--

CREATE TABLE `player_login_logs` (
  `id` bigint(20) NOT NULL,
  `created_at_date` date NOT NULL,
  `created_at_time` time NOT NULL,
  `login_success` bit(1) NOT NULL,
  `player_id` bigint(20) UNSIGNED NOT NULL,
  `address` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `practice_game_play_transactions`
--

CREATE TABLE `practice_game_play_transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `transaction_amount` double NOT NULL,
  `games_id` bigint(20) UNSIGNED NOT NULL,
  `wallet_account_id` bigint(20) UNSIGNED NOT NULL,
  `players_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `transaction_type` enum('Credit','Debit') NOT NULL,
  `rooms_id` bigint(20) UNSIGNED NOT NULL,
  `created_date_time` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `report_problems`
--

CREATE TABLE `report_problems` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `players_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `type_of_issue` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `version_code` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_name` varchar(50) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role_name`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Accounts', '1518692158', '', b'1'),
(2, 'Administrator', '1518692175', '', b'1'),
(4, 'Corporate Staff', '1518692202', '', b'1'),
(7, 'IT', '1518692237', '', b'1'),
(8, 'Kitchens', '1518692244', '1549013544', b'0'),
(9, 'Marketing', '1518692252', '', b'1'),
(10, 'Security', '1518692259', '1522751287', b'1'),
(11, 'Trainer', '1518692267', '', b'1'),
(12, 'Developer', '1518692267', '1518692267', b'1'),
(13, 'Tester', '1518692267', '1522751634', b'0'),
(14, 'Customer', '1520660827', '', b'1'),
(15, 'Software Tester', '1521551736', '1522751377', b'0'),
(17, 'Center Admin', '1523358175', '1549013551', b'0'),
(18, 'CENTER', '1523358421', '1523358460', b'0'),
(19, 'CA', '1523358831', '1523358846', b'0'),
(22, 'Digital Marketing', '1527171954', '', b'1'),
(24, 'Management', '1528271672', '', b'1'),
(25, 'Finance', '1529396394', '', b'1'),
(26, 'Admin', '1538978287', '', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `role_wise_access_rights`
--

CREATE TABLE `role_wise_access_rights` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `roles_id` bigint(20) UNSIGNED NOT NULL,
  `modules_id` bigint(20) UNSIGNED NOT NULL,
  `view_permission` bit(1) NOT NULL DEFAULT b'0',
  `add_permission` bit(1) NOT NULL DEFAULT b'0',
  `edit_permission` bit(1) NOT NULL DEFAULT b'0',
  `delete_permission` bit(1) NOT NULL DEFAULT b'0',
  `all_permission` bit(1) NOT NULL DEFAULT b'0' COMMENT 'This is dummy only',
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_wise_access_rights`
--

INSERT INTO `role_wise_access_rights` (`id`, `roles_id`, `modules_id`, `view_permission`, `add_permission`, `edit_permission`, `delete_permission`, `all_permission`, `created_at`, `updated_at`, `status`) VALUES
(1, 14, 4, b'0', b'0', b'0', b'0', b'0', '1549013562', '', b'1'),
(2, 14, 5, b'0', b'0', b'0', b'0', b'0', '1549013562', '', b'1'),
(3, 14, 6, b'0', b'0', b'0', b'0', b'0', '1549013562', '', b'1'),
(4, 14, 1, b'0', b'0', b'0', b'0', b'0', '1549013562', '', b'1'),
(5, 14, 2, b'0', b'0', b'0', b'0', b'0', '1549013562', '', b'1'),
(6, 14, 3, b'0', b'0', b'0', b'0', b'0', '1549013562', '', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `room_ref_id` varchar(20) NOT NULL,
  `created_date_time` datetime NOT NULL,
  `destroyed_date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `room_ref_id`, `created_date_time`, `destroyed_date_time`) VALUES
(1, '155005383009200', '2019-02-13 16:00:30', '0000-00-00 00:00:00'),
(2, '155005389010010', '2019-02-13 16:01:30', '0000-00-00 00:00:00'),
(3, '155005389680320', '2019-02-13 16:01:37', '0000-00-00 00:00:00'),
(4, '155005443920330', '2019-02-13 16:10:40', '0000-00-00 00:00:00'),
(5, '155005465790440', '2019-02-13 16:14:18', '0000-00-00 00:00:00'),
(6, '155005480057150', '2019-02-13 16:16:41', '0000-00-00 00:00:00'),
(7, '155005574530860', '2019-02-13 16:32:26', '0000-00-00 00:00:00'),
(8, '155005638339070', '2019-02-13 16:43:04', '0000-00-00 00:00:00'),
(9, '155005701113980', '2019-02-13 16:53:31', '0000-00-00 00:00:00'),
(10, '155005712047090', '2019-02-13 16:55:21', '0000-00-00 00:00:00'),
(11, '1550057169729100', '2019-02-13 16:56:10', '0000-00-00 00:00:00'),
(12, '155006066575600', '2019-02-13 17:54:26', '0000-00-00 00:00:00'),
(13, '155006086937510', '2019-02-13 17:57:49', '0000-00-00 00:00:00'),
(14, '155006299559720', '2019-02-13 18:33:16', '0000-00-00 00:00:00'),
(15, '155013838204200', '2019-02-14 15:29:41', '0000-00-00 00:00:00'),
(16, '155013927788800', '2019-02-14 15:44:37', '0000-00-00 00:00:00'),
(17, '155013988157200', '2019-02-14 15:54:41', '0000-00-00 00:00:00'),
(18, '155014038238700', '2019-02-14 16:03:02', '0000-00-00 00:00:00'),
(19, '155014061298800', '2019-02-14 16:06:52', '0000-00-00 00:00:00'),
(20, '155014121660200', '2019-02-14 16:16:56', '0000-00-00 00:00:00'),
(21, '155014151029400', '2019-02-14 16:21:50', '0000-00-00 00:00:00'),
(22, '155014158729210', '2019-02-14 16:23:07', '0000-00-00 00:00:00'),
(23, '155014163965320', '2019-02-14 16:23:59', '0000-00-00 00:00:00'),
(24, '155014176210230', '2019-02-14 16:26:01', '0000-00-00 00:00:00'),
(25, '155014199813800', '2019-02-14 16:29:57', '0000-00-00 00:00:00'),
(26, '155014293609600', '2019-02-14 16:45:35', '0000-00-00 00:00:00'),
(27, '155014376061700', '2019-02-14 16:59:20', '0000-00-00 00:00:00'),
(28, '155014400750900', '2019-02-14 17:03:27', '0000-00-00 00:00:00'),
(29, '155014420346100', '2019-02-14 17:06:43', '0000-00-00 00:00:00'),
(30, '155014436221000', '2019-02-14 17:09:21', '0000-00-00 00:00:00'),
(31, '155014551737110', '2019-02-14 17:28:37', '0000-00-00 00:00:00'),
(32, '155014636822600', '2019-02-14 17:42:47', '0000-00-00 00:00:00'),
(33, '155014657205910', '2019-02-14 17:46:11', '0000-00-00 00:00:00'),
(34, '155014659839620', '2019-02-14 17:46:37', '0000-00-00 00:00:00'),
(35, '155014879215800', '2019-02-14 18:23:11', '0000-00-00 00:00:00'),
(36, '155014943951700', '2019-02-14 18:33:59', '0000-00-00 00:00:00'),
(37, '155021994057800', '2019-02-15 14:08:59', '0000-00-00 00:00:00'),
(38, '155022041461200', '2019-02-15 14:16:53', '0000-00-00 00:00:00'),
(39, '155022070963200', '2019-02-15 14:21:48', '0000-00-00 00:00:00'),
(40, '155022165028200', '2019-02-15 14:37:28', '0000-00-00 00:00:00'),
(41, '155022217678400', '2019-02-15 14:46:15', '0000-00-00 00:00:00'),
(42, '155022259135500', '2019-02-15 14:53:09', '0000-00-00 00:00:00'),
(43, '155022281525700', '2019-02-15 14:56:53', '0000-00-00 00:00:00'),
(44, '155022320903600', '2019-02-15 15:03:27', '0000-00-00 00:00:00'),
(45, '155022359807700', '2019-02-15 15:09:56', '0000-00-00 00:00:00'),
(46, '155022432576100', '2019-02-15 15:22:04', '0000-00-00 00:00:00'),
(47, '155022449250400', '2019-02-15 15:24:50', '0000-00-00 00:00:00'),
(48, '155023025687700', '2019-02-15 17:00:55', '0000-00-00 00:00:00'),
(49, '155022320903700', '2019-02-15 17:59:29', '0000-00-00 00:00:00'),
(50, '155029729252100', '2019-02-16 11:38:11', '0000-00-00 00:00:00'),
(51, '155029752777300', '2019-02-16 11:42:06', '0000-00-00 00:00:00'),
(52, '155049323040200', '2019-02-18 18:04:04', '0000-00-00 00:00:00'),
(53, '155049339076700', '2019-02-18 18:06:44', '0000-00-00 00:00:00'),
(54, '155049351272500', '2019-02-18 18:08:46', '0000-00-00 00:00:00'),
(55, '155049381038300', '2019-02-18 18:13:44', '0000-00-00 00:00:00'),
(56, '155049408843300', '2019-02-18 18:18:22', '0000-00-00 00:00:00'),
(57, '155049430986000', '2019-02-18 18:22:04', '0000-00-00 00:00:00'),
(58, '155049571807100', '2019-02-18 18:45:31', '0000-00-00 00:00:00'),
(59, '155055486625300', '2019-02-19 11:11:19', '0000-00-00 00:00:00'),
(60, '155055549315300', '2019-02-19 11:21:46', '0000-00-00 00:00:00'),
(61, '155055609134100', '2019-02-19 11:31:44', '0000-00-00 00:00:00'),
(62, '155055649208000', '2019-02-19 11:38:25', '0000-00-00 00:00:00'),
(63, '155055667043500', '2019-02-19 11:41:23', '0000-00-00 00:00:00'),
(64, '155055685193900', '2019-02-19 11:44:25', '0000-00-00 00:00:00'),
(65, '155055725020700', '2019-02-19 11:51:03', '0000-00-00 00:00:00'),
(66, '155055745278400', '2019-02-19 11:54:26', '0000-00-00 00:00:00'),
(67, '155055766410500', '2019-02-19 11:57:57', '0000-00-00 00:00:00'),
(68, '155055810339000', '2019-02-19 12:05:16', '0000-00-00 00:00:00'),
(69, '155055861357600', '2019-02-19 12:13:47', '0000-00-00 00:00:00'),
(70, '155055981633500', '2019-02-19 12:33:49', '0000-00-00 00:00:00'),
(71, '155055998155100', '2019-02-19 12:36:34', '0000-00-00 00:00:00'),
(72, '155056018018900', '2019-02-19 12:39:53', '0000-00-00 00:00:00'),
(73, '155056047355600', '2019-02-19 12:44:47', '0000-00-00 00:00:00'),
(74, '155056450339910', '2019-02-19 13:51:56', '0000-00-00 00:00:00'),
(75, '155056453041320', '2019-02-19 13:52:24', '0000-00-00 00:00:00'),
(76, '155056458017200', '2019-02-19 13:53:13', '0000-00-00 00:00:00'),
(77, '155056461262810', '2019-02-19 13:53:45', '0000-00-00 00:00:00'),
(78, '155056484908800', '2019-02-19 13:59:10', '0000-00-00 00:00:00'),
(79, '155056495044520', '2019-02-19 13:59:10', '0000-00-00 00:00:00'),
(80, '155056575350910', '2019-02-19 14:12:47', '0000-00-00 00:00:00'),
(81, '155056779861500', '2019-02-19 14:46:52', '0000-00-00 00:00:00'),
(82, '155056786312010', '2019-02-19 14:47:56', '0000-00-00 00:00:00'),
(83, '155056952988920', '2019-02-19 15:15:43', '0000-00-00 00:00:00'),
(84, '155057267725540', '2019-02-19 16:08:10', '0000-00-00 00:00:00'),
(85, '155057295954500', '2019-02-19 16:12:53', '0000-00-00 00:00:00'),
(86, '155057317074900', '2019-02-19 16:16:24', '0000-00-00 00:00:00'),
(87, '155057332661800', '2019-02-19 16:18:59', '0000-00-00 00:00:00'),
(88, '155057376311900', '2019-02-19 16:26:16', '0000-00-00 00:00:00'),
(89, '155057383303010', '2019-02-19 16:27:26', '0000-00-00 00:00:00'),
(90, '155057402654420', '2019-02-19 16:30:40', '0000-00-00 00:00:00'),
(91, '155057609809900', '2019-02-19 17:05:11', '0000-00-00 00:00:00'),
(92, '155057726125900', '2019-02-19 17:24:34', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `id` int(11) NOT NULL,
  `site_title` varchar(100) DEFAULT NULL,
  `logo` varchar(100) NOT NULL,
  `favicon` varchar(100) NOT NULL,
  `no_reply_email_id` varchar(100) NOT NULL COMMENT 'With this id an automated email will go to order placed customers, forgot password email',
  `contact_number` varchar(100) NOT NULL,
  `contact_email` varchar(60) DEFAULT NULL,
  `support_email` varchar(100) DEFAULT NULL,
  `address` text,
  `google_analytics_id` varchar(13) DEFAULT NULL,
  `google_maps_api_key` varchar(100) DEFAULT NULL,
  `facebook_app_id` varchar(100) DEFAULT NULL,
  `facebook_secret_key` varchar(100) DEFAULT NULL,
  `google_client_id` varchar(100) DEFAULT NULL,
  `google_secret_key` varchar(100) DEFAULT NULL,
  `one_signal_web_push_notifications` varchar(100) DEFAULT NULL,
  `tax_calculation_with` enum('Global','Restaurant Wise') NOT NULL DEFAULT 'Global',
  `delivery_calculation_settings` enum('Distance Based','Order Based') NOT NULL DEFAULT 'Distance Based',
  `active_payment_gateway` enum('Razor Pay','Instamojo') NOT NULL DEFAULT 'Instamojo',
  `payment_gateway_mode` enum('Live','Test') NOT NULL DEFAULT 'Test',
  `seo_title` text,
  `seo_keywords` text,
  `seo_description` text,
  `status` bit(1) NOT NULL DEFAULT b'1',
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `site_title`, `logo`, `favicon`, `no_reply_email_id`, `contact_number`, `contact_email`, `support_email`, `address`, `google_analytics_id`, `google_maps_api_key`, `facebook_app_id`, `facebook_secret_key`, `google_client_id`, `google_secret_key`, `one_signal_web_push_notifications`, `tax_calculation_with`, `delivery_calculation_settings`, `active_payment_gateway`, `payment_gateway_mode`, `seo_title`, `seo_keywords`, `seo_description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Mr Rummy', '534f2da02be6c88a0400fbd867d57e7b.png', '72d4e704dd68d53c205640df609686db.png', 'no_reply@cloverrummy.com', '9502735455', 'support@cloverrummy.com', '', '<p>&nbsp;Dwaraka Nagar, Visakhapatnam, Andhra Pradesh 530016</p>\r\n', '', '', '', '', '', '', 'bd8aeb84-89c2-4c75-a42a-3cfb63f88760', 'Global', 'Order Based', 'Instamojo', 'Test', 'Go4Food', NULL, NULL, b'1', '1478613170', '1547307858');

-- --------------------------------------------------------

--
-- Table structure for table `sms_gateway_settings`
--

CREATE TABLE `sms_gateway_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `sender_id` varchar(6) NOT NULL,
  `sms_gateway_username` varchar(25) NOT NULL,
  `sms_gateway_password` varchar(25) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sms_gateway_settings`
--

INSERT INTO `sms_gateway_settings` (`id`, `sender_id`, `sms_gateway_username`, `sms_gateway_password`, `created_at`, `updated_at`, `status`) VALUES
(1, 'MrRumy', 'CLover Rummy', 'vizag@123', '1547187398', '1547187803', 1);

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `state_name` varchar(100) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) DEFAULT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `state_name`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Andhra Pradesh', '2019-01-31 17:00:00', NULL, b'1'),
(2, 'Rajasthan', '2019-01-31 17:00:00', NULL, b'1'),
(3, 'Tamil Nadu', '2019-01-31 17:00:00', NULL, b'1'),
(4, 'Karnataka', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT '0',
  `fullname` varchar(100) NOT NULL,
  `username` varchar(40) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `salt` varchar(200) DEFAULT NULL,
  `token` varchar(200) NOT NULL,
  `login_status` bit(1) NOT NULL DEFAULT b'1',
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `fullname`, `username`, `password`, `salt`, `token`, `login_status`, `created_at`, `updated_at`, `status`) VALUES
(1, NULL, 'Sudhi', 'SUDHI123', '8be25ecf401b388af4119a4381f99561', 'XtsRZh04g4', 'dksuhklsjjfdkshfdsfjhdsfjfdsafsdafsdkldshfdklshjfkldjshfkdsfh232', b'1', '1518693636', '1530681773', b'0'),
(2, 12, 'Manikanta Kunisetty', 'MANI123', 'f9aa8dd11c04261ba283b8a00ef73b13', 'HskxJb4Zky', 'dksuhklsjjfdkfdsfsdfsdfdsjkldshfdklshjfkldjshfkdlsfdsfh232', b'1', '1518693636', '1549350982', b'1'),
(3, 12, 'Manikanta Kunisetty', 'teja123', '75cb4a02d1210a28b3bad976e9a8614e', 'MuhAbFZd6G', 'Ashskdjncvh451654JDkhsnckhjd', b'1', '1518693636', '1549014824', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `user_meta`
--

CREATE TABLE `user_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `users_id` bigint(20) UNSIGNED NOT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `mobile` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `gender` enum('Male','Female','Other') DEFAULT NULL,
  `address` text,
  `referred_by` varchar(100) DEFAULT NULL,
  `dob` date NOT NULL,
  `joining_date` date DEFAULT NULL,
  `payroll_status` bit(1) DEFAULT b'1',
  `pan_no` varchar(100) DEFAULT NULL,
  `esi_no` varchar(100) DEFAULT NULL,
  `pf_no` varchar(100) DEFAULT NULL,
  `license_no` varchar(100) DEFAULT NULL,
  `bank_name` varchar(100) DEFAULT NULL,
  `account_no` varchar(100) DEFAULT NULL,
  `ifsc_code` varchar(100) DEFAULT NULL,
  `micr_code` varchar(100) DEFAULT NULL,
  `branch_name` varchar(100) DEFAULT NULL,
  `ctc` varchar(100) DEFAULT NULL,
  `net_salary` varchar(100) DEFAULT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_meta`
--

INSERT INTO `user_meta` (`id`, `users_id`, `designation`, `mobile`, `email`, `gender`, `address`, `referred_by`, `dob`, `joining_date`, `payroll_status`, `pan_no`, `esi_no`, `pf_no`, `license_no`, `bank_name`, `account_no`, `ifsc_code`, `micr_code`, `branch_name`, `ctc`, `net_salary`, `created_at`, `updated_at`, `status`) VALUES
(1, 3, 'Developer', '9553517603', 'teja@thecolourmoon.com', 'Male', 'Pedda Bazar, Kunkalamarru\nKaramchedu', NULL, '2019-02-20', '2019-01-30', b'1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '25000', '25000', '1549014824', '', b'1'),
(2, 2, 'Developer', '9553517603', 'manikantak49@gmail.com', 'Male', 'Pedda Bazar, Kunkalamarru\nKaramchedu', NULL, '2019-01-29', '2019-02-05', b'1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1549350982', '', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `user_wise_access_rights`
--

CREATE TABLE `user_wise_access_rights` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `modules_id` bigint(20) UNSIGNED NOT NULL,
  `view_permission` bit(1) NOT NULL DEFAULT b'0',
  `add_permission` bit(1) NOT NULL DEFAULT b'0',
  `edit_permission` bit(1) NOT NULL DEFAULT b'0',
  `delete_permission` bit(1) NOT NULL DEFAULT b'0',
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_wise_access_rights`
--

INSERT INTO `user_wise_access_rights` (`id`, `user_id`, `modules_id`, `view_permission`, `add_permission`, `edit_permission`, `delete_permission`, `created_at`, `updated_at`, `status`) VALUES
(1, 3, 4, b'1', b'1', b'1', b'1', '1549014840', '', b'1'),
(2, 3, 5, b'1', b'1', b'1', b'1', '1549014840', '', b'1'),
(3, 3, 6, b'1', b'1', b'1', b'1', '1549014840', '', b'1'),
(4, 3, 1, b'1', b'1', b'1', b'1', '1549014841', '', b'1'),
(5, 3, 2, b'1', b'1', b'1', b'1', '1549014841', '', b'1'),
(6, 3, 3, b'1', b'1', b'1', b'1', '1549014841', '', b'1'),
(7, 2, 4, b'1', b'1', b'1', b'1', '1549014862', '', b'1'),
(8, 2, 5, b'1', b'1', b'1', b'1', '1549014862', '', b'1'),
(9, 2, 6, b'1', b'1', b'1', b'1', '1549014862', '', b'1'),
(10, 2, 1, b'1', b'1', b'1', b'1', '1549014862', '', b'1'),
(11, 2, 2, b'1', b'1', b'1', b'1', '1549014862', '', b'1'),
(12, 2, 3, b'1', b'1', b'1', b'1', '1549014862', '', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `wallet_account`
--

CREATE TABLE `wallet_account` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fun_chips` bigint(20) NOT NULL,
  `in_play_fun_chips` bigint(20) NOT NULL,
  `real_chips_deposit` bigint(20) NOT NULL,
  `in_play_real_chips` bigint(20) NOT NULL,
  `real_chips_withdrawal` bigint(20) NOT NULL,
  `total_bonus` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `_uuid` varchar(255) NOT NULL,
  `wallet_account_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wallet_account`
--

INSERT INTO `wallet_account` (`id`, `fun_chips`, `in_play_fun_chips`, `real_chips_deposit`, `in_play_real_chips`, `real_chips_withdrawal`, `total_bonus`, `_uuid`, `wallet_account_name`) VALUES
(1, 100, 0, 0, 0, -200, 0, 'tz2Hdo21Fc', 'Game Wallet'),
(2, 100, 0, 0, 0, -120, 0, '4ZqjDiASIJ', 'GST Wallet'),
(3, 100, 0, 0, 0, 356, 0, 'aIwfWAphxE', 'TDS Wallet'),
(4, 100, 0, 0, 0, 600, 0, 'X0VK71pMhG', 'Admin Commission Wallet'),
(9, 100, 0, 0, 0, 0, 0, 'dGSr6qJsiI', ''),
(10, 100, 0, 0, 0, 0, 0, 'JiV8ObCRS1', ''),
(11, 100, 0, 0, 0, 0, 0, 'A9XslWjMRg', ''),
(12, 100, 0, 0, 0, 0, 0, 'o4l7B04dry', ''),
(13, 100, 0, 0, 0, 0, 0, 'UDzPcvTMr4', ''),
(14, 100, 0, 0, 0, 0, 0, 'qlX7GxMibt', ''),
(15, 100, 0, 0, 0, 0, 0, 'bsMgglY2V1', ''),
(16, 100, 0, 0, 0, 0, 0, '73J0575Kud', ''),
(17, 100, 0, 0, 0, 0, 0, 'bRY5Xvz3mm', ''),
(18, 100, 0, 0, 0, 0, 0, 'jIcdkqSM2K', ''),
(19, 10000, 0, 0, 0, 0, 0, 'qqMbQ13Z1Z', ''),
(22, 9550, 450, 2350, 0, 0, 0, 'h8IojPBo7f', ''),
(23, 100, 0, 0, 0, 0, 0, 'nmHgO2ygGa', ''),
(24, 100, 0, 0, 0, 0, 0, 'tn5aM1vfK6', ''),
(25, -3900, -100, 0, 0, 0, 0, 'Hp6eFgolgh', ''),
(26, 100, 0, 0, 0, 0, 0, 'SOqbkCbLwn', ''),
(27, 100, 0, 0, 0, 0, 0, 'GD0RWCrS6Z', ''),
(28, 100, 0, 0, 0, 0, 0, 'rc0ECQFzL5', ''),
(29, 100, 0, 0, 0, 0, 0, 'rRx1gpn2Zs', ''),
(30, 80500, 2600, 100750, 0, -2040, 0, 'MRb14XMg75', 'Player'),
(32, 89000, 2600, 1550000, 0, 5286, 0, 'aYf6LEjbrj', 'Player'),
(33, 14100, 400, 2500000, 0, 0, 0, 'Bt5EWe0UNG', 'Player'),
(34, 10000, 0, 2150000, 0, 0, 0, 'VJG36qCpUP', 'Player'),
(35, 9900, 100, 0, 0, 0, 0, 'Y4qHelHAb0', 'Player'),
(36, 10000, 0, 250, 0, 0, 0, 'vFJZNEJGpk', 'Player');

-- --------------------------------------------------------

--
-- Table structure for table `wallet_transaction`
--

CREATE TABLE `wallet_transaction` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `game_ref_id` bigint(20) NOT NULL,
  `transaction_id` varchar(15) DEFAULT NULL,
  `transaction_amount` double NOT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `transaction_type` varchar(255) DEFAULT NULL,
  `games_id` bigint(20) UNSIGNED NOT NULL,
  `rooms_id` bigint(20) UNSIGNED NOT NULL,
  `game_type` enum('Cash','Practice') NOT NULL,
  `individual_bid_amount` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wallet_transaction`
--

INSERT INTO `wallet_transaction` (`id`, `game_ref_id`, `transaction_id`, `transaction_amount`, `created_date_time`, `remark`, `transaction_type`, `games_id`, `rooms_id`, `game_type`, `individual_bid_amount`) VALUES
(1, 7395439754937594, '201902130706086', 200, '2019-02-13 19:06:08', 'Amount Debited To Players', 'GAME STARTED', 6, 11, 'Practice', 100),
(2, 155022165028200, '201902150237450', 200, '2019-02-15 14:37:45', 'Amount Debited to players', 'GAME STARTED', 4, 40, 'Practice', 100),
(3, 155022217678400, '201902150246328', 200, '2019-02-15 14:46:32', 'Amount Debited to players', 'GAME STARTED', 4, 41, 'Practice', 100),
(4, 155022259135500, '201902150254059', 200, '2019-02-15 14:54:05', 'Amount Debited to players', 'GAME STARTED', 4, 42, 'Practice', 100),
(5, 155022259135500, '201902150255009', 200, '2019-02-15 14:55:00', 'Amount Debited to players', 'GAME STARTED', 4, 42, 'Practice', 100),
(6, 155057609809900, '201902190505495', 200, '2019-02-19 17:05:48', 'Amount settled to players', 'GAME ENDED', 1, 91, 'Cash', 100),
(7, 155057609809900, '201902190505499', 0, '2019-02-19 17:05:49', 'Amount Debited to GST Wallet', NULL, 0, 0, 'Cash', 0),
(8, 155057609809900, '201902190505491', 0, '2019-02-19 17:05:49', 'Amount Debited to TDS Wallet', NULL, 0, 0, 'Cash', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wallet_transaction_cr_account`
--

CREATE TABLE `wallet_transaction_cr_account` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `wallet_transaction_id` bigint(20) UNSIGNED NOT NULL,
  `transaction_amount` double DEFAULT NULL,
  `wallet_account_id` bigint(20) UNSIGNED NOT NULL COMMENT 'CR Account'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wallet_transaction_cr_account`
--

INSERT INTO `wallet_transaction_cr_account` (`id`, `wallet_transaction_id`, `transaction_amount`, `wallet_account_id`) VALUES
(1, 1, 100, 25),
(2, 1, 100, 30),
(3, 2, 100, 30),
(4, 2, 100, 32),
(5, 3, 100, 30),
(6, 3, 100, 32),
(7, 4, 100, 30),
(8, 4, 100, 32),
(9, 5, 100, 30),
(10, 5, 100, 32),
(11, 6, 200, 1),
(12, 6, 200, 1),
(13, 7, 0, 32),
(14, 8, 20, 32);

-- --------------------------------------------------------

--
-- Table structure for table `wallet_transaction_dr_account`
--

CREATE TABLE `wallet_transaction_dr_account` (
  `id` bigint(20) NOT NULL,
  `wallet_transaction_id` bigint(20) UNSIGNED NOT NULL,
  `transaction_amount` double DEFAULT NULL,
  `wallet_account_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Dr Account'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wallet_transaction_dr_account`
--

INSERT INTO `wallet_transaction_dr_account` (`id`, `wallet_transaction_id`, `transaction_amount`, `wallet_account_id`) VALUES
(1, 1, 200, 1),
(2, 2, 200, 1),
(3, 3, 200, 1),
(4, 4, 200, 1),
(5, 5, 200, 1),
(6, 6, 1602, 32),
(7, 6, 2000, 30),
(8, 6, 200, 4),
(9, 6, 200, 32),
(10, 7, 0, 2),
(11, 8, 20, 3);

-- --------------------------------------------------------

--
-- Table structure for table `wallet_transaction_history`
--

CREATE TABLE `wallet_transaction_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ref_id` varchar(50) NOT NULL,
  `transaction_type` enum('Credit','Debit') NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `closing_balance` double NOT NULL DEFAULT '0',
  `created_date_time` datetime NOT NULL,
  `wallet_account_id` bigint(20) UNSIGNED NOT NULL,
  `remark` varchar(250) NOT NULL,
  `type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wallet_transaction_history`
--

INSERT INTO `wallet_transaction_history` (`id`, `ref_id`, `transaction_type`, `amount`, `closing_balance`, `created_date_time`, `wallet_account_id`, `remark`, `type`) VALUES
(1, 'mKfoJs.0U13c', 'Credit', 250, 250, '2019-02-19 16:26:36', 22, 'Add amount', 'Add Cash'),
(3, 'mKfoJ0U13c', 'Credit', 100, 2350, '2019-02-19 16:35:10', 22, 'Add amount', 'Add Cash'),
(4, '1736168874', 'Credit', 250, 250, '2019-02-19 16:58:44', 30, 'Add amount', 'Add Cash'),
(5, '3977963044', 'Credit', 500, 750, '2019-02-19 17:00:17', 30, 'Add amount', 'Add Cash'),
(6, '8324167175', 'Debit', 100, 1549900, '2019-02-19 17:05:11', 32, '155057609809900-1 - Entry', '155057609809900-1 - Entry'),
(7, '9323032442', 'Debit', 100, 650, '2019-02-19 17:05:11', 30, '155057609809900-1 - Entry', '155057609809900-1 - Entry'),
(8, '1400419656', 'Credit', 100000, 98850, '2019-02-19 17:22:57', 30, 'Amount deposited to wallet', 'Add Cash'),
(9, '0858094440', 'Debit', 100, 1551782, '2019-02-19 17:24:34', 32, '155057726125900-1 - Entry', 'Game Entry'),
(10, '1431974145', 'Debit', 100, 98750, '2019-02-19 17:24:34', 30, '155057726125900-1 - Entry', 'Game Entry'),
(11, '1145840657', 'Credit', 200, 400, '2019-02-19 17:24:58', 4, '1 - Amount credited to withdrawl wallet', 'Commission'),
(12, '4141092204', 'Credit', 178, 178, '2019-02-19 17:24:58', 3, '1 - Amount credited to withdrawl wallet', 'TAX'),
(13, '1459324668', 'Credit', 178, -22, '2019-02-19 17:24:58', 2, '1 - Amount credited to withdrawl wallet', 'TAX'),
(14, '0227876598', 'Credit', 1602, 1422, '2019-02-19 17:24:58', 2, '1 - Amount credited to withdrawl wallet', 'Game Won'),
(15, '8674995772', 'Credit', 2000, 100850, '2019-02-19 17:24:58', 30, '1 - Game Lost', 'Game Lost'),
(16, '5803359025', 'Debit', 100, 1553484, '2019-02-19 17:25:09', 32, '155057726125900-2 - Entry', 'Game Entry'),
(17, '1227334883', 'Debit', 100, 98570, '2019-02-19 17:25:09', 30, '155057726125900-2 - Entry', 'Game Entry'),
(18, '0466230726', 'Credit', 200, 600, '2019-02-19 17:26:56', 4, '1 - Amount credited to withdrawl wallet', 'Commission'),
(19, '7614239947', 'Credit', 178, 356, '2019-02-19 17:26:56', 3, '1 - Amount credited to withdrawl wallet', 'TAX'),
(20, '7537913762', 'Credit', 178, 18, '2019-02-19 17:26:56', 2, '1 - Amount credited to withdrawl wallet', 'TAX'),
(21, '7974813810', 'Credit', 1602, 1462, '2019-02-19 17:26:56', 2, '1 - Amount credited to withdrawl wallet', 'Game Won'),
(22, '0038698977', 'Credit', 2000, 100670, '2019-02-19 17:26:56', 30, '1 - Game Lost', 'Game Lost');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_downloads`
--
ALTER TABLE `app_downloads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bonus_coupons`
--
ALTER TABLE `bonus_coupons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `coupon_code` (`coupon_code`);

--
-- Indexes for table `deposit_money_transactions`
--
ALTER TABLE `deposit_money_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `players_id` (`players_id`);

--
-- Indexes for table `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `token` (`token`);

--
-- Indexes for table `game_play`
--
ALTER TABLE `game_play`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `global_configuration_constants`
--
ALTER TABLE `global_configuration_constants`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key_name`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `table_primary_key_id` (`table_primary_key_id`);

--
-- Indexes for table `join_play_transactions`
--
ALTER TABLE `join_play_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `players_id` (`players_id`),
  ADD KEY `rooms_id` (`rooms_id`);

--
-- Indexes for table `login_logs`
--
ALTER TABLE `login_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_module_id` (`parent_module_id`),
  ADD KEY `display_order` (`display_order`);

--
-- Indexes for table `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `my_referral_code` (`my_referral_code`),
  ADD UNIQUE KEY `access_token` (`access_token`),
  ADD UNIQUE KEY `wallet_id` (`wallet_account_id`),
  ADD KEY `referred_by_code` (`referred_by_code`),
  ADD KEY `states_id` (`states_id`);

--
-- Indexes for table `player_login_logs`
--
ALTER TABLE `player_login_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK1r0e7bxr2hc3icssoqp93km9s` (`player_id`);

--
-- Indexes for table `practice_game_play_transactions`
--
ALTER TABLE `practice_game_play_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `games_id` (`games_id`),
  ADD KEY `wallet_account_id` (`wallet_account_id`);

--
-- Indexes for table `report_problems`
--
ALTER TABLE `report_problems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`role_name`);

--
-- Indexes for table `role_wise_access_rights`
--
ALTER TABLE `role_wise_access_rights`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roles_id` (`roles_id`,`modules_id`),
  ADD KEY `modules_id` (`modules_id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `room_ref_id` (`room_ref_id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms_gateway_settings`
--
ALTER TABLE `sms_gateway_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `state_name` (`state_name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `token` (`token`);

--
-- Indexes for table `user_meta`
--
ALTER TABLE `user_meta`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_id` (`users_id`);

--
-- Indexes for table `user_wise_access_rights`
--
ALTER TABLE `user_wise_access_rights`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roles_id` (`user_id`,`modules_id`);

--
-- Indexes for table `wallet_account`
--
ALTER TABLE `wallet_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallet_transaction`
--
ALTER TABLE `wallet_transaction`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `transaction_id` (`transaction_id`);

--
-- Indexes for table `wallet_transaction_cr_account`
--
ALTER TABLE `wallet_transaction_cr_account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wallet_account_id` (`wallet_account_id`),
  ADD KEY `wallet_transaction_id` (`wallet_transaction_id`);

--
-- Indexes for table `wallet_transaction_dr_account`
--
ALTER TABLE `wallet_transaction_dr_account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wallet_account_id` (`wallet_account_id`),
  ADD KEY `wallet_transaction_id` (`wallet_transaction_id`);

--
-- Indexes for table `wallet_transaction_history`
--
ALTER TABLE `wallet_transaction_history`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ref_id` (`ref_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_downloads`
--
ALTER TABLE `app_downloads`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bonus_coupons`
--
ALTER TABLE `bonus_coupons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `deposit_money_transactions`
--
ALTER TABLE `deposit_money_transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `games`
--
ALTER TABLE `games`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `game_play`
--
ALTER TABLE `game_play`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `global_configuration_constants`
--
ALTER TABLE `global_configuration_constants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `join_play_transactions`
--
ALTER TABLE `join_play_transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `players`
--
ALTER TABLE `players`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `player_login_logs`
--
ALTER TABLE `player_login_logs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `practice_game_play_transactions`
--
ALTER TABLE `practice_game_play_transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `report_problems`
--
ALTER TABLE `report_problems`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `role_wise_access_rights`
--
ALTER TABLE `role_wise_access_rights`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sms_gateway_settings`
--
ALTER TABLE `sms_gateway_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_meta`
--
ALTER TABLE `user_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_wise_access_rights`
--
ALTER TABLE `user_wise_access_rights`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `wallet_account`
--
ALTER TABLE `wallet_account`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `wallet_transaction`
--
ALTER TABLE `wallet_transaction`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `wallet_transaction_cr_account`
--
ALTER TABLE `wallet_transaction_cr_account`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `wallet_transaction_dr_account`
--
ALTER TABLE `wallet_transaction_dr_account`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `wallet_transaction_history`
--
ALTER TABLE `wallet_transaction_history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `deposit_money_transactions`
--
ALTER TABLE `deposit_money_transactions`
  ADD CONSTRAINT `deposit_money_transactions_ibfk_1` FOREIGN KEY (`players_id`) REFERENCES `players` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `login_logs`
--
ALTER TABLE `login_logs`
  ADD CONSTRAINT `login_logs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `players`
--
ALTER TABLE `players`
  ADD CONSTRAINT `players_ibfk_1` FOREIGN KEY (`wallet_account_id`) REFERENCES `wallet_account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `player_login_logs`
--
ALTER TABLE `player_login_logs`
  ADD CONSTRAINT `player_login_logs_ibfk_1` FOREIGN KEY (`player_id`) REFERENCES `players` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_wise_access_rights`
--
ALTER TABLE `role_wise_access_rights`
  ADD CONSTRAINT `role_wise_access_rights_ibfk_1` FOREIGN KEY (`modules_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_wise_access_rights_ibfk_2` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `user_meta`
--
ALTER TABLE `user_meta`
  ADD CONSTRAINT `user_meta_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_wise_access_rights`
--
ALTER TABLE `user_wise_access_rights`
  ADD CONSTRAINT `user_wise_access_rights_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wallet_transaction_cr_account`
--
ALTER TABLE `wallet_transaction_cr_account`
  ADD CONSTRAINT `wallet_transaction_cr_account_ibfk_1` FOREIGN KEY (`wallet_account_id`) REFERENCES `wallet_account` (`id`),
  ADD CONSTRAINT `wallet_transaction_cr_account_ibfk_2` FOREIGN KEY (`wallet_transaction_id`) REFERENCES `wallet_transaction` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wallet_transaction_dr_account`
--
ALTER TABLE `wallet_transaction_dr_account`
  ADD CONSTRAINT `wallet_transaction_dr_account_ibfk_1` FOREIGN KEY (`wallet_account_id`) REFERENCES `wallet_account` (`id`),
  ADD CONSTRAINT `wallet_transaction_dr_account_ibfk_2` FOREIGN KEY (`wallet_transaction_id`) REFERENCES `wallet_transaction` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
