<?php

class Newsletter_subscribe_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function save($data) {
        $email = $data["email"];
        $this->db->where("email", $email);
        if ($this->db->get("newsletter_subscribers")->num_rows() == 0) {
            $data["created_at"] = CREATED_DATE_TIME;
            if ($this->db->insert('newsletter_subscribers', $data)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public function send_download_link_to_mobile($data) {
        $sms_message = "Good News!"
                . "The " . SITE_TITLE . " App is now available on Android & iOS. So go ahead & enjoy a rich Rummy experience & win cash prizes."
                . "Downlaod Now"
                . "Android : " . DOWNLOAD_ANDROID_APP_LINK
                . "iOS : " . DOWNLOAD_IOS_APP_LINK;
        send_message($sms_message, $data["mobile"]);
        return true;
    }

}
