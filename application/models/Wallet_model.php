<?php

class Wallet_model extends CI_Model {

    public $table_name;

    public function __construct() {
        parent::__construct();
        $this->table_name = "wallet_account";
    }

    function create_wallet($wallet_account_name = "") {
        $uuid = $this->generate_wallet_token();
        $data = [
            "fun_chips" => MAX_FUN_CHIPS,
            "in_play_fun_chips" => 0,
            "real_chips_deposit" => 0,
            "in_play_real_chips" => 0,
            "real_chips_withdrawal" => 0,
            "total_bonus" => 0,
            "total_rps_points"=>0,
            "_uuid" => $uuid,
            "wallet_account_name" => $wallet_account_name
        ];

        $ether['url'] = 'http://15.206.29.182:3001/eth/create';
        $headers = array(
            "Cache-Control: no-cache",
            "Content-Type: application/json"
        );
        $ether['headers'] = $headers;
        $eth_response = curlGet($ether);
        $eth_decode_response = json_decode($eth_response, true);

        if($eth_decode_response['result'] == "success"){
            $data['eth_address'] = $eth_decode_response['address'];
            $data['eth_private_key'] = $eth_decode_response['privateKey'];
        }

        $bitcoin['url'] = 'http://15.206.29.182:3001/btc/create';
        $headers = array(
            "Cache-Control: no-cache",
            "Content-Type: application/json"
        );
        $bitcoin['headers'] = $headers;
        $btc_response = curlGet($bitcoin);
        $btc_decode_response = json_decode($btc_response, true);

        if($btc_decode_response['result'] == "success"){
            $data['btc_address'] = $btc_decode_response['address'];
            $data['btc_private_key'] = $btc_decode_response['privateKey'];
        }

        $this->db->set($data);
        $this->db->insert($this->table_name);
        return $this->db->insert_id();
    }

    function generate_wallet_token() {
        $uuid = generateRandomString(10);
        if ($this->db->get_where("wallet_account", ["_uuid" => $uuid])->num_rows() == 0) {
            return $uuid;
        } else {
            return $this->generate_wallet_token();
        }
    }

    function get_wallet_account_id_by_players_id($players_id){
        $this->db->select("wallet_account_id");
        $this->db->where("(access_token = '$players_id' OR id= '$players_id')");
        $wallet_account_id = $this->db->get("players")->row()->wallet_account_id;
        return $wallet_account_id;
    }

    function get_wallet_details_by_players_id($players_id){
        $wallet_account_id = $this->get_wallet_account_id_by_players_id($players_id);
        $wallet_info = $this->get_wallet($wallet_account_id);
        unset($wallet_info->id);
        return $wallet_info;
    }

    function get_wallet($wallet_account_id) {
        $this->db->limit(1);
        $this->db->where("id", $wallet_account_id);
        $row = $this->db->get($this->table_name)->row();
        $row->total_cash_balance = round($row->real_chips_deposit + $row->real_chips_withdrawal, 2);
        return $row;
    }

    function get_wallet_by_uuid($_uid) {
        $this->db->where("_uuid", $_uid);
        $row = $this->db->get($this->table_name)->row();
        return $row;
    }

    function reset_funchips($players_id) {
        $player_details = $this->player_login_model->get_player_details($players_id);
        $wallet_info = $this->get_wallet($player_details->wallet_account_id);
        if ($wallet_info->fun_chips >= MAX_FUN_CHIPS) {
            return false;
        } else {
            $this->db->where("id", $player_details->wallet_account_id);
            $this->db->set("fun_chips", MAX_FUN_CHIPS);
            $this->db->update($this->table_name);
            ping_to_sfs_server(SFS_COMMAND_USER_CHIPS_UPDATED . $player_details->access_token);
            return true;
        }
    }

    function set_updated_real_chips_deposit($updated_real_chips, $wallet_account_id) {
        $this->db->set("real_chips_deposit",$updated_real_chips);
        $this->db->where("id", $wallet_account_id);
        $this->db->update($this->table_name);
        return true;
    }

    function update_real_chips_deposit($wallet_account_uuid, $addon_amount) {
        $this->db->set("real_chips_deposit", "real_chips_deposit+" . $addon_amount, false);
        $this->db->where("_uuid", $wallet_account_uuid);
        $this->db->update($this->table_name);
        //Send sms if wallet increased
        return true;
    }

    function check_sufficient_credits_has_or_not($game_type, $bid_amount, $players_id) {
        $player_details = $this->player_login_model->get_player_details($players_id);
        $wallet_info = $this->get_wallet($player_details->wallet_account_id);

        if ($game_type == "Practice") {
            if ($bid_amount <= $wallet_info->fun_chips) {
                return true;
            }
        } else if ($game_type == "Cash") {
            if ($bid_amount <= $wallet_info->real_chips_deposit) {
                return true;
            } else if ($bid_amount <= ($wallet_info->real_chips_deposit + $wallet_info->real_chips_withdrawal)) {
                return true;
            }
        }
        return false;
    }

    function convert_wallet_chips_to_inplay($game_type, $bid_amount, $players_id, $is_tournament = false) {
        $player_details = $this->player_login_model->get_player_details($players_id);
        $wallet_info = $this->get_wallet($player_details->wallet_account_id);
        if ($game_type == "Practice") {
            $updated_in_play_fun_chips = $wallet_info->in_play_fun_chips + $bid_amount;
            $updated_fun_chips = $wallet_info->fun_chips - $bid_amount;

            $this->db->set("in_play_fun_chips", $updated_in_play_fun_chips);
            $this->db->set("fun_chips", $updated_fun_chips);
            $this->db->where("id", $player_details->wallet_account_id);
            $this->db->update($this->table_name);
            return [
                "taken_from_deposit_balance" => $bid_amount,
                "taken_from_withdrawal_balance" => 0
            ];
        } else if ($game_type == "Cash") {
            if ($bid_amount <= $wallet_info->real_chips_deposit) {

                if(!$is_tournament){
                    $updated_in_play_real_chips = $wallet_info->in_play_real_chips + $bid_amount;
                    $this->update_in_play_real_chips($updated_in_play_real_chips, $player_details->wallet_account_id);
                }

                $updated_real_chips_deposit = $wallet_info->real_chips_deposit - $bid_amount;
                $this->set_updated_real_chips_deposit($updated_real_chips_deposit, $player_details->wallet_account_id);

                $arr = [
                    "taken_from_deposit_balance" => $bid_amount,
                    "taken_from_withdrawal_balance" => 0
                ];
                return $arr;
            } else if ($bid_amount <= ($wallet_info->real_chips_deposit + $wallet_info->real_chips_withdrawal)) {

                if(!$is_tournament){
                    $updated_in_play_real_chips = $wallet_info->in_play_real_chips + $bid_amount;
                }

                $arr = [];
                if (($wallet_info->real_chips_deposit - $bid_amount) > 0) {
                    $updated_real_chips_deposit = $wallet_info->real_chips_deposit - $bid_amount;
                    $arr["taken_from_deposit_balance"] = $bid_amount;
                } else {
                    $updated_real_chips_deposit = 0;
                    $arr["taken_from_deposit_balance"] = $wallet_info->real_chips_deposit;
                }
                $arr["taken_from_withdrawal_balance"] = $bid_amount - $arr["taken_from_deposit_balance"];
                $updated_real_chips_withdrawal =  $wallet_info->real_chips_withdrawal - $arr["taken_from_withdrawal_balance"];

                $this->db->set("real_chips_deposit", $updated_real_chips_deposit);
                $this->db->set("real_chips_withdrawal", $updated_real_chips_withdrawal);
                if(!$is_tournament){
                    $this->db->set("in_play_real_chips", $updated_in_play_real_chips);
                }
                $this->db->where("id", $player_details->wallet_account_id);
                $this->db->update($this->table_name);

                return $arr;
            }
        }
    }

    function undo_wallet_chips_from_inplay($game_type, $taken_from_deposit_balance, $taken_from_withdrawal_balance, $players_id){
        $player_details = $this->player_login_model->get_player_details($players_id);
        $wallet_info = $this->get_wallet($player_details->wallet_account_id);
        if ($game_type == "Practice") {
            $updated_in_play_fun_chips = $wallet_info->in_play_fun_chips - $taken_from_deposit_balance;
            $updated_fun_chips = $wallet_info->fun_chips + $taken_from_deposit_balance;

            $this->db->set("in_play_fun_chips", $updated_in_play_fun_chips);
            $this->db->set("fun_chips", $updated_fun_chips);
            $this->db->where("id", $player_details->wallet_account_id);
            $this->db->update($this->table_name);
            return true;
        }else if ($game_type == "Cash") {

            $updated_in_play_real_chips = $wallet_info->in_play_real_chips - ($taken_from_deposit_balance + $taken_from_withdrawal_balance);
            $updated_real_chips_deposit = $wallet_info->real_chips_deposit + $taken_from_deposit_balance;
            $updated_real_chips_withdrawal = $wallet_info->real_chips_withdrawal + $taken_from_withdrawal_balance;

            $this->db->set("real_chips_deposit", $updated_real_chips_deposit);
            $this->db->set("real_chips_withdrawal", $updated_real_chips_withdrawal);
            $this->db->set("in_play_real_chips", $updated_in_play_real_chips);
            $this->db->where("id", $player_details->wallet_account_id);
            $this->db->update($this->table_name);
            return true;
        }
    }

    function get_wallet_account_id_by_player_access_token($player_access_token){
        $this->db->select("wallet_account_id");
        $this->db->where("access_token", $player_access_token);
        $row = $this->db->get("players")->row();
        return $row->wallet_account_id;
    }

    function set_updated_fun_chips($updated_funchips, $wallet_account_id){
        $this->db->set("fun_chips", $updated_funchips);
        $this->db->where("id", $wallet_account_id);
        $this->db->update($this->table_name);
        return true;
    }

    function set_updated_real_chips_withdrawl($updated_real_chips_withdrawl, $wallet_account_id){
        $this->db->set("real_chips_withdrawal", $updated_real_chips_withdrawl);
        $this->db->where("id", $wallet_account_id);
        $this->db->update($this->table_name);
        return true;
    }

    function update_in_play_fun_chips($updated_in_play_fun_chips, $wallet_account_id){
        $this->db->set("in_play_fun_chips", $updated_in_play_fun_chips);
        $this->db->where("id", $wallet_account_id);
        $this->db->update($this->table_name);
        return true;
    }

    function update_in_play_real_chips($updated_in_play_real_chips, $wallet_account_id){
        $this->db->set("in_play_real_chips", $updated_in_play_real_chips);
        $this->db->where("id", $wallet_account_id);
        $this->db->update($this->table_name);
        return true;
    }

    function set_updated_rps_points($updated_rps_points, $wallet_account_id, $players_id, $expiry_date = null, $updated_expertize_level_id=null){
        $this->db->set("total_rps_points", $updated_rps_points);
        $this->db->where("id", $wallet_account_id);
        if(isset($expiry_date)){
            $this->db->set("rps_level_expiry_date", $expiry_date);
        }
        $this->db->update($this->table_name);

        if(isset($updated_expertize_level_id)){
            $this->db->where("(access_token = '$players_id' OR id= '$players_id')");
            $this->db->set("expertise_level", $updated_expertize_level_id);
            $this->db->update("players");
            //Ping sfs server and notification to show a popup for level is updated
        }
        return true;
    }

    function add_signup_bonus_amount($wallet_account_id){
        if(SIGN_UP_CASH_AMOUNT >0){
            $transaction_data = [
                "transaction_type" => "Credit",
                "amount" => SIGN_UP_CASH_AMOUNT,
                "wallet_account_id" => $wallet_account_id,
                "remark" => "Welcome Cash Bonus added to wallet",
                "type" => "Sign Up Bonus Cash",
            ];
            $this->wallet_transactions_model->create_transaction($transaction_data);

            $w_row = $this->wallet_model->get_wallet($wallet_account_id);
            $updated_real_chips_deposit = $w_row->real_chips_deposit + SIGN_UP_CASH_AMOUNT;
            $this->set_updated_real_chips_deposit($updated_real_chips_deposit, $wallet_account_id);
        }
    }

    function add_mobile_verified_amount($wallet_account_id) {
        if (CASH_ON_MOBILE_VERIFIED > 0) {
            if ($this->db->get_where("wallet_transaction_history", ["type" => "Mobile Verified Bonus Cash", "wallet_account_id" => $wallet_account_id])->num_rows() == 0) {
                $transaction_data = [
                    "transaction_type" => "Credit",
                    "amount" => CASH_ON_MOBILE_VERIFIED,
                    "wallet_account_id" => $wallet_account_id,
                    "remark" => "Cash added to wallet against mobile verified",
                    "type" => "Mobile Verified Bonus Cash",
                ];
                $this->wallet_transactions_model->create_transaction($transaction_data);

                $w_row = $this->wallet_model->get_wallet($wallet_account_id);
                $updated_real_chips_deposit = $w_row->real_chips_deposit + CASH_ON_MOBILE_VERIFIED;
                $this->set_updated_real_chips_deposit($updated_real_chips_deposit, $wallet_account_id);
            }
        }
    }

    function add_email_verified_amount($wallet_account_id) {
        if (CASH_ON_EMAIL_VERIFIED > 0) {
            if ($this->db->get_where("wallet_transaction_history", ["type" => "Email Verified Bonus Cash", "wallet_account_id" => $wallet_account_id])->num_rows() == 0) {
                $transaction_data = [
                    "transaction_type" => "Credit",
                    "amount" => CASH_ON_EMAIL_VERIFIED,
                    "wallet_account_id" => $wallet_account_id,
                    "remark" => "Cash added to wallet against email verified",
                    "type" => "Email Verified Bonus Cash",
                ];
                $this->wallet_transactions_model->create_transaction($transaction_data);

                $w_row = $this->wallet_model->get_wallet($wallet_account_id);
                $updated_real_chips_deposit = $w_row->real_chips_deposit + CASH_ON_EMAIL_VERIFIED;
                $this->set_updated_real_chips_deposit($updated_real_chips_deposit, $wallet_account_id);
            }
        }
    }

    function get_players_id_by_wallet_account_id($wallet_account_id){
        $this->db->where("wallet_account_id", $wallet_account_id);
        $this->db->select("id");
        $this->db->limit(1);
        return $this->db->get("players")->row()->id;
    }

    function set_updated_bonus_chips($updated_bonus_chips, $wallet_account_id) {
        $this->db->set("total_bonus", $updated_bonus_chips);
        $this->db->where("id", $wallet_account_id);
        $this->db->update($this->table_name);
        return true;
    }

}
