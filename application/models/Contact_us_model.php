<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Contact_us_model
 *
 * @author KrishnaPadam
 */
class Contact_us_model extends CI_Model {
    public function __construct(){
        parent::__construct();
    }
    
    public function save($data){
        if( $this->db->insert('contact_us', $data) ) {
            return true;
        } else {
            return false;
        }
    }
    
    public function send_notification_email($data){
        $message = $this->load->view('mail_templates/contact_form_submitted_email', $data, TRUE);
        $this->email->from(NO_REPLY_MAIL, SITE_TITLE);
        $this->email->to(CONTACT_ENQUIRES_RECEIVE_MAIL);
        if (BCC_EMAIL) {
            $this->email->bcc(BCC_EMAIL);
        }
        $this->email->subject('One User has submitted Contact Form.');
        $this->email->message( $message );
        $send_email = $this->email->send();

        //To send an email to the commenting user
        $message = "Dear ".$data["username"].".<br>Your comment has been sent successfully to A2Z Betting<br>
            query : ".$data["query"]."<br>
            Message : ".$data["message"];
        $this->email->from(NO_REPLY_MAIL, SITE_TITLE);
        $this->email->to($data["email"]);
        if(BCC_EMAIL){ $this->email->bcc(BCC_EMAIL); }
        $this->email->subject('comment sent to A2Z Betting.');
        $this->email->message( $message );
        $this->email->send();

        if($send_email){
	    return true;
	}
        return false;
    }
}
