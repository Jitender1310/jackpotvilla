<?php

class Tournament_categories_model extends CI_Model {

    private $table_name = "tournament_categories";

    function add($data) {
        $this->db->set($data);
        $this->db->set("created_at", time());
        $response = $this->db->insert($this->table_name);
        if ($response) {
            $inserted_id = $this->db->insert_id();
            $this->log_model->create_log($this->table_name, __FUNCTION__, $inserted_id);
        }
        return $response;
    }

    function update($data, $id) {
        $this->db->set($data);
        $this->db->where("id", $id);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        if ($response) {
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
        }
        return $response;
    }

    function delete($id) {
        $this->db->where("id", $id);
        $this->db->set("status", 0);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        if ($response) {
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
        }
        return $response;
    }

    function get() {
        $this->db->where("status", 1);
        $data = $this->db->get($this->table_name)->result();
       
 
        if ($data) {
            foreach ($data as $item) {
                if ($item->image != '') {
                    $item->image = BACK_END_SERVER_URL_FILE_UPLOAD_FOLDER_IMG_PATH . $item->image;
                } else {
                    $item->image = "";
                }
                $item->created_date_time = date(DATE_TIME_HUMAN_READABLE_FORMAT, $item->created_at);
            }
        } else {
            return [];
        }
        return $data;
    }

    function is_name_exists($title, $id) {
        $this->db->where("status", 1);
        if ($id) {
            $this->db->where("(id!=$id)");
        }
        $this->db->where("name", $title);
        return $this->db->get($this->table_name)->num_rows();
    }

    function get_tournament_category_name($club_type_id) {
        $column_name = "name";
        $this->db->select($column_name);
        $this->db->where("id", $club_type_id);
        $result = $this->db->get($this->table_name);
        if ($result->num_rows()) {
            return $result->row()->{$column_name};
        }
    }

}
