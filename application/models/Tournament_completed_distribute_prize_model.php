<?php

class Tournament_completed_distribute_prize_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function distribute_prizes($cloned_tournaments_id) {

        $this->db->trans_begin();

        $tournament_details = $this->tournaments_model->get_tournament_row($cloned_tournaments_id, 0);
        $max_players = $tournament_details->max_players;
        $tournament_details->joined_players_list = $this->tournaments_model->get_only_played_players_tournament($tournament_details->cloned_tournaments_id, $max_players);

        $entry_fee = $tournament_details->entry_value;
        $expected_prize = $max_players * $entry_fee;
        $joined_players_count = count($tournament_details->joined_players_list);

        /* echo "Max Players :" . $max_players;
          echo "\n";
          echo "Entry Fee :" . $entry_fee;
          echo "\n";
          echo "Expected Prize : " . $expected_prize;
          echo "\n";
          echo "Joined Players : " . $joined_players_count;
          echo "\n";
         */

        if ($tournament_details->entry_type == "Cash") {
            $expected_prize = $expected_prize - ($expected_prize * $tournament_details->tournament_commission_percentage / 100);
           /*echo "Tournament Commission : " . $tournament_details->tournament_commission_percentage . "%";
            echo "\n";*/
        }

        /*
          echo "Expected Prize After deduct commission : " . $expected_prize;
          echo "\n"; */

        //After joined players calculation
        $real_prize = $joined_players_count * $entry_fee;
        if ($tournament_details->entry_type == "Cash") {
            $admin_commission_amount = ($real_prize * $tournament_details->tournament_commission_percentage / 100);
            if(!isset($tournament_details->premium_category)){
                //update admin comission
                $transaction_data = [
                    "transaction_type" => "Credit",
                    "amount" => $admin_commission_amount,
                    "wallet_account_id" => ADMIN_COMMISSION_WALLET_ACCOUNT_ID,
                    "remark" => $tournament_details->ref_id." - Tournament Amount credited to withdrawl wallet",
                    "type" => "Commission",
                ];
                
                $this->wallet_transactions_model->create_transaction($transaction_data);
                $w_row = $this->wallet_model->get_wallet(ADMIN_COMMISSION_WALLET_ACCOUNT_ID);
                $updated_withdrawl_chips = $w_row->real_chips_withdrawal + $admin_commission_amount;
                $this->wallet_model->set_updated_real_chips_withdrawl($updated_withdrawl_chips,ADMIN_COMMISSION_WALLET_ACCOUNT_ID);
                
                $total_gst_amount = ($admin_commission_amount * GST_PERCENTAGE)/100;
                
                //update gst comission
                $transaction_data = [
                    "transaction_type" => "Credit",
                    "amount" => $total_gst_amount,
                    "wallet_account_id" => GST_WALLET_ACCOUNT_ID,
                    "remark" => $tournament_details->ref_id." - Tournament Amount credited to withdrawl wallet",
                    "type" => "GST",
                ];
                $this->wallet_transactions_model->create_transaction($transaction_data);
                $w_row = $this->wallet_model->get_wallet(GST_WALLET_ACCOUNT_ID);
                $updated_withdrawl_chips = $w_row->real_chips_withdrawal + $total_gst_amount;
                $this->wallet_model->set_updated_real_chips_withdrawl($updated_withdrawl_chips,GST_WALLET_ACCOUNT_ID);      
            }
            $real_prize = $real_prize - $admin_commission_amount;
        }
        //finding joined players percentage
        $joined_players_percentage = 100 - ((($expected_prize - $real_prize) / $expected_prize ) * 100);

        $this->credit_prize_amouts_to_players($tournament_details, $joined_players_percentage);

        
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function credit_prize_amouts_to_players($tournament_details, $joined_players_percentage) {
        $prize_distributions = [];

        $total_tds_amount = 0;
        
        foreach ($tournament_details->prize_distributions as $item) {
            for ($i = $item->from_rank; $i <= $item->to_rank; $i++) {
                $actual_prize_amount = $item->prize_value - ($item->prize_value * $joined_players_percentage / 100);
                if ($actual_prize_amount <= $item->min_prize_value) {
                    $actual_prize_amount = $item->min_prize_value;
                }
                foreach ($tournament_details->joined_players_list as $player_row) {
                    if ($i == $player_row->position_in_tournament) {
                        $prize_distributions[] = [
                            "rank_id" => $i,
                            "prize_amount" => $actual_prize_amount,
                            "players_id" => $player_row->players_id
                        ];
                        $this->db->where("id", $player_row->id);
                        $this->db->where("players_id", $player_row->players_id);
                        $this->db->set("given_prize_money", $actual_prize_amount);
                        $this->db->update("joined_players_for_tournaments");


                        $transaction_data = [
                            "transaction_type" => "Credit",
                            "amount" => $actual_prize_amount,
                            "wallet_account_id" => $player_row->wallet_account_id,
                            "remark" => $tournament_details->ref_id . " - Amount credited to withdrawl wallet",
                            "type" => "Tournament Won"
                        ];
                        $this->wallet_transactions_model->create_transaction($transaction_data);

                        $w_row = $this->wallet_model->get_wallet($player_row->wallet_account_id);
                        $updated_withdrawl_chips = $w_row->real_chips_withdrawal + $item->winnings;
                        $this->wallet_model->set_updated_real_chips_withdrawl($updated_withdrawl_chips, $player_row->wallet_account_id);


                        if ($actual_prize_amount >= MINIMUM_WINNING_AMOUNT_TO_APPLY_TDS) {
                            $tds_amount = ($actual_prize_amount * TDS_PERCENTAGE) / 100;
                            $total_tds_amount += $tds_amount;
                            $transaction_data = [
                                "transaction_type" => "Debit",
                                "amount" => $tds_amount,
                                "wallet_account_id" => $player_row->wallet_account_id,
                                "remark" => $tournament_details->ref_id . " - Amount debited against TDS",
                                "type" => "TDS"
                            ];
                            $this->wallet_transactions_model->create_transaction($transaction_data);

                            $w_row = $this->wallet_model->get_wallet($player_row->wallet_account_id);
                            $updated_withdrawl_chips = $w_row->real_chips_withdrawal + $item->winnings;
                            $this->wallet_model->set_updated_real_chips_withdrawl($updated_withdrawl_chips, $player_row->wallet_account_id);
                        }

                        break;
                    }
                }
            }
        }

        if ($total_tds_amount) {
            $transaction_data = [
                "transaction_type" => "Credit",
                "amount" => $total_tds_amount,
                "wallet_account_id" => TDS_WALLET_ACCOUNT_ID,
                "remark" => $tournament_details->ref_id . " Tournament - Amount credited to withdrawl wallet",
                "type" => "TDS",
            ];
            $this->wallet_transactions_model->create_transaction($transaction_data);
            $w_row = $this->wallet_model->get_wallet(TDS_WALLET_ACCOUNT_ID);
            $updated_withdrawl_chips = $w_row->real_chips_withdrawal + $total_tds_amount;
            $this->wallet_model->set_updated_real_chips_withdrawl($updated_withdrawl_chips, TDS_WALLET_ACCOUNT_ID);
        }

        
        return $prize_distributions;
    }

}
