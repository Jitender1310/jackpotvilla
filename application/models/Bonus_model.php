<?php

class Bonus_model extends CI_Model {

    private $table_name;

    public function __construct() {
        parent::__construct();
        $this->table_name = "bonus_coupons";
    }

    function get_available_coupons() {
        $this->db->select("id, coupon_code, bonus_type, title, description, from_date_time, to_date_time, terms_conditions");
        $this->db->where("('" . CREATED_DATE_TIME . "' >= from_date_time  AND '" . CREATED_DATE_TIME . "' <= to_date_time )");
        $this->db->where("status", 1);
        $result = $this->db->get($this->table_name)->result();
        return $result;
    }

    function get_released_bonus($wallet_account_id) {
        $this->db->select("sum(utilized_amount) as released_bonus");
        $this->db->where("is_expired", 0);
        $this->db->where("wallet_account_id", $wallet_account_id);
        $rb = $this->db->get("bonus_transaction_history")->row()->released_bonus;
        if ($rb) {
            return $rb;
        }
        return 0;
    }

    function get_active_bonus($wallet_account_id) {
        $this->db->select("sum(amount) as active_bonus");
        $this->db->where("is_expired", 0);
        $this->db->where("wallet_account_id", $wallet_account_id);
        $rb = $this->db->get("bonus_transaction_history")->row()->active_bonus;
        if ($rb) {
            return $rb;
        }
        return 0;
    }

    function get_pending_bonus($wallet_account_id) {
        $pending_bonus = $this->get_active_bonus($wallet_account_id) - $this->get_released_bonus($wallet_account_id);
        return $pending_bonus;
    }

}
