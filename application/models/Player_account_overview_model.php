<?php

class Player_account_overview_model extends CI_Model {

    function get_account_view_details($players_id) {
        
        $this->load->model("bonus_model");

        $data = $this->player_profile_model->get_profile_information($players_id);
        $wallet_account_id = $data->wallet_info->id;
        
        $arr = [
            "deposit_balance" => $data->wallet_account->real_chips_deposit,
            "reward_points" => $data->wallet_account->total_rps_points,
            "withdrawal_balance" => $data->wallet_account->real_chips_withdrawal,
            "level_status" => 100,
            "total_cash_balance" => $data->wallet_account->real_chips_deposit + $data->wallet_account->real_chips_withdrawal,
            "practice_account_chips" => $data->wallet_account->fun_chips,
            "release_bonus" => $this->bonus_model->get_released_bonus($wallet_account_id),
            "pending_bonus" => $this->bonus_model->get_pending_bonus($wallet_account_id),
            //"active_bonus" => $data->wallet_account->total_bonus
            "active_bonus" => $this->bonus_model->get_active_bonus($wallet_account_id)
        ];
        return $arr;
    }

}
