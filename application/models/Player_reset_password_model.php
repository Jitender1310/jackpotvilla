<?php

class Player_reset_password_model extends CI_Model{
    public $table_name = "players";

    public function __construct() {
        parent::__construct();
        $this->table_name = "players";
    }
    
    function reset_with_new_password($data, $players_id){
        $salt = generateRandomString(10);
        $new_password = $data['password'];
        $password = $data['password'];
        $enc_password = md5($password . $salt);
        $data["password"] = $enc_password;
        $data["salt"] = $salt;
        
        $this->db->set($data);
        $this->db->where("id", $players_id);
        $this->db->set("updated_at", time());
        $this->db->update($this->table_name);
        $access_token = $this->db->select("access_token")->get_where($this->table_name, ["id"=>$players_id])->row()->access_token;
        $this->player_notifications_model->send_password_resetted($new_password, $access_token);
        return true;
    }
    
    function update_reset_password_link($players_id){
        $player_details = $this->player_profile_model->get_profile_information($players_id, true);
        $data = [
            "forgot_password_verification_link" => generateRandomNumber(35)
        ];
        $this->db->set($data);
        $this->db->where("id", $player_details->id);
        $this->db->update($this->table_name);
        $this->player_notifications_model->send_reset_password_link($players_id);
        return true;
    }
   
    function get_players_id_by_forgot_password_code($forgot_password_verification_link){
        $this->db->where("forgot_password_verification_link",$forgot_password_verification_link);
        $this->db->select("id");
        $row = $this->db->get($this->table_name)->row();
        if($row){
            return $row->id;
        }
        return false;
    }
    
    function reset_password_by_forgotten_password_verification_link($forgot_password_verification_link, $password, $players_id){
        
        $salt = generateRandomString(10);
        $enc_password = md5($password . $salt);
        $data["password"] = $enc_password;
        $data["salt"] = $salt;
        
        $this->db->where("forgot_password_verification_link", $forgot_password_verification_link);
        $this->db->where("id", $players_id);
        $this->db->set("forgot_password_verification_link", "");
        $this->db->set($data);
        $this->db->update($this->table_name);
        return true;
    }
    function update_password_message($players_id){
        $player_details = $this->db->where(array("id"=>$players_id))->get($this->table_name)->row();
        $this->player_notifications_model->send_update_password_message($player_details);
        
    }
}