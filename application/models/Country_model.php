<?php

class Country_model extends CI_Model {

    private $table_name = "countries";

    function get_all() {
        $this->db->where("active", 1);
        $data = $this->db->get($this->table_name)->result();
        return $data;
    }
}
