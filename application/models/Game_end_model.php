<?php

class Game_end_model extends CI_Model {

    private $game_title;
    private $game_sub_type;
    private $games_id;

    public function __construct() {
        parent::__construct();
    }

    function finish_game($player_access_tokens, $rooms_ref_id, $games_token, $game_ref_id, $player_access_tokens_with_points_joinings, $bid_amount, $split_type) {
        $this->db->select("id, game_type, point_value, game_sub_type, game_title, pool_deal_prize, pool_game_type");
        $this->db->where("token", $games_token);
        $game_row = $this->db->get("games")->row();
        $point_value = $game_row->point_value;
        $pool_game_type = $game_row->pool_game_type;
        $game_type = $game_row->game_type;
        $game_sub_type = $game_row->game_sub_type;
        $pool_deal_prize = $game_row->pool_deal_prize;

        $this->game_title = $game_row->game_title;
        $this->game_sub_type = $game_row->game_sub_type;
        $this->games_id = $game_row->id;




        $players = explode(",", $player_access_tokens);
        $number_of_players = count($players);
        

        $players_points_arr = explode(",", $player_access_tokens_with_points_joinings);

        $players_data = [];
        $total_loose_points = 0;
        //mapping player ids
        $no_of_players = 0;
        for ($i = 0; $i < count($players_points_arr); $i++) {
            $p_row = explode("___", $players_points_arr[$i]);
            $this->db->select("id, access_token, wallet_account_id");
            $this->db->where("access_token", $p_row[0]);
            $pr = $this->db->get("players")->row();
            $players_data[] = (object) [
                        "players_id" => $pr->id,
                        "access_token" => $p_row[0],
                        "loose_points" => $p_row[1],
                        "is_winner" => $p_row[2],
                        "number_of_joinings"=>$p_row[3],
                        "wallet_account_id" => $pr->wallet_account_id
            ];
            $no_of_players += $p_row[3];
            $total_loose_points += $p_row[1];
        }
        
        //$total_bid_amount = $bid_amount * $number_of_players;
        $total_bid_amount = $bid_amount * $no_of_players;
        
        log_message("error", print_r($players_data, true));
        log_message("error", "Total No of players " . $no_of_players);
        log_message("error", "Total Bid amount " . $total_bid_amount);

       
        //check room ref id 
        $this->db->select("id");
        $this->db->where("room_ref_id", $rooms_ref_id);
        $room_row = $this->db->get("rooms")->row();
        if ($room_row) {
            $rooms_id = $room_row->id;
        } else {
            return "INVALID_ROOM";
        }
        
        if($game_type=="Practice"){
            return $this->run_for_practice_game($game_row, $players_data, $number_of_players, $total_bid_amount, $bid_amount, $split_type, $total_loose_points);
        }
        
        /*--------Cash Game Start--------*/
        $this->db->trans_begin();

        $total_loss_points = 0;
        
        if ($game_sub_type == "Points") {
            $total_loss_points = 0;
            $total_admin_commission_amount = 0;
            $total_gst_amount = 0;
            $total_tds_amount = 0;
            $total_winning_amount = 0;
            
            foreach ($players_data as $item) {
                if ($item->is_winner) {
                    //commission calculation amount goest to admin from winner winning amount
                    $total_winning_amount = $total_loose_points * $point_value;
                    
                    $total_admin_commission_amount = $this->get_admin_commission_amount($total_winning_amount);
                    $total_gst_amount = $this->get_gst_amount($total_admin_commission_amount);
                    $total_tds_amount = $this->get_tds_amount($total_winning_amount);
                    
                    $this->update_admin_commission_amount($total_admin_commission_amount, $game_ref_id);
                    $this->update_gst_commission_amount($total_gst_amount, $game_ref_id);
                    $this->update_tds_amount($total_tds_amount, $game_ref_id);
                    
                    //$item->winnings = ($total_winning_amount - ($total_admin_commission_amount +  $total_gst_amount)) - $total_tds_amount;
                    $item->winnings = ($total_winning_amount - ($total_admin_commission_amount)) - $total_tds_amount;

                    //convert bonus
                    $this->convert_bonus_amount($item->winnings, $game_ref_id, $item->wallet_account_id);

                    /* --Adding amount as game won to winnter wallet start-- */
                    $transaction_data = [
                        "transaction_type" => "Credit",
                        "amount" => $item->winnings + $bid_amount,
                        "wallet_account_id" => $item->wallet_account_id,
                        "remark" => $game_ref_id . " - Amount credited to withdrawl wallet",
                        "type" => "Game Won",
                        "game_title" => $this->game_title,
                        "game_sub_type" => $this->game_sub_type,
                        "games_id" => $this->games_id
                    ];
                    $this->wallet_transactions_model->create_transaction($transaction_data);
                   /*--Adding amount as game won to winnter wallet end--*/
                    
                    //updating real chips withdrawl
                    $w_row = $this->wallet_model->get_wallet($item->wallet_account_id);
                    $updated_withdrawl_chips = $w_row->real_chips_withdrawal + $item->winnings + $bid_amount;
                    $this->wallet_model->set_updated_real_chips_withdrawl($updated_withdrawl_chips, $item->wallet_account_id);

                    //updating in play real chips
                    $updated_in_play_real_chips = $w_row->in_play_real_chips - $bid_amount;
                    $this->wallet_model->update_in_play_real_chips($updated_in_play_real_chips, $item->wallet_account_id);
                    
                } else {
                    $w_row = $this->wallet_model->get_wallet($item->wallet_account_id);
                    $item->winnings = "-" . ($item->loose_points * $point_value);
                    
                    $transaction_data = [
                        "transaction_type" => "Credit",
                        "amount" => $bid_amount - ($item->loose_points * $point_value),
                        "wallet_account_id" => $item->wallet_account_id,
                        "remark" => $game_ref_id." - Game Lost",
                        "type" => "Game Lost",
                        "game_title" => $this->game_title,
                        "game_sub_type" => $this->game_sub_type,
                        "games_id" => $this->games_id
                    ];
                    $this->wallet_transactions_model->create_transaction($transaction_data);
                    
                    
                    $updated_withdrawl_chips = $w_row->real_chips_withdrawal +($bid_amount- ($item->loose_points * $point_value));
                    $this->wallet_model->set_updated_real_chips_withdrawl($updated_withdrawl_chips, $item->wallet_account_id);

                    $updated_in_play_real_chips = $w_row->in_play_real_chips - $bid_amount;
                    $this->wallet_model->update_in_play_real_chips($updated_in_play_real_chips, $item->wallet_account_id);
                    
                    //convert bonus
                    $this->convert_bonus_amount(($item->loose_points * $point_value), $game_ref_id, $item->wallet_account_id);
                    
                }
            }
        } else {
            //for deals and pools
            
            //only for pool game Manual and Auto split will be Happened
            foreach ($players_data as $item) {
                if ($item->is_winner) {
                    $total_winning_players++;
                    if ($game_sub_type === "Pool") {
                        $this->game_tracker_model->add_played_record($bid_amount, $item->players_id, $game_ref_id, $pool_game_type);
                    }
                }
                $this->rps_model->add_rps_points_game_played($bid_amount, $item->players_id, $game_ref_id, $item->wallet_account_id);
            }
            if($split_type == "Auto"){
                $total_winning_players = 0;
                $total_admin_commission_amount = $this->get_admin_commission_amount($total_bid_amount);
                $total_gst_amount = $this->get_gst_amount($total_admin_commission_amount);
               

                $this->update_admin_commission_amount($total_admin_commission_amount, $game_ref_id);
                $this->update_gst_commission_amount($total_gst_amount, $game_ref_id);
                $total_tds_amount = 0;
                //$total_bid_amount = $total_bid_amount - $total_admin_commission_amount - $total_gst_amount;
                $total_bid_amount = $total_bid_amount - $total_admin_commission_amount;
                
                $winning_amount_for_each_player = $total_bid_amount/$total_winning_players;
                foreach ($players_data as $item) {
                    if ($item->is_winner) {
                        $tds_amount = 0;
                        $winning_amount_excluded_bid_amount = ($winning_amount_for_each_player - $bid_amount);

                        log_message("error", "Winning amount exclude Bid amount " . $winning_amount_excluded_bid_amount);
                        log_message("error", "Winning amount for each player " . $winning_amount_for_each_player);
                        log_message("error", "Bid Amount " . $bid_amount);
                        log_message("error", "Min amount to apply TDS" . MINIMUM_WINNING_AMOUNT_TO_APPLY_TDS);


                        if ($winning_amount_excluded_bid_amount >= MINIMUM_WINNING_AMOUNT_TO_APPLY_TDS) {
                            $tds_amount = ($winning_amount_excluded_bid_amount * TDS_PERCENTAGE) / 100;
                            $total_tds_amount += $tds_amount;
                        }
                        $winning_amount_for_each_player = $winning_amount_for_each_player - $tds_amount;
                        $item->winnings = $winning_amount_for_each_player;
                        
                        /*--Adding amount as game won to winnter wallet start--*/
                        $transaction_data = [
                            "transaction_type" => "Credit",
                            "amount" => $item->winnings,
                            "wallet_account_id" => $item->wallet_account_id,
                            "remark" => $game_ref_id . " - Amount credited to withdrawal wallet",
                            "type" => "Game Won",
                            "game_title" => $this->game_title,
                            "game_sub_type" => $this->game_sub_type,
                            "games_id" => $this->games_id
                        ];
                        $this->wallet_transactions_model->create_transaction($transaction_data);
                       /*--Adding amount as game won to winnter wallet end--*/
                        
                        //updating real chips withdrawl
                        $w_row = $this->wallet_model->get_wallet($item->wallet_account_id);
                        //$updated_withdrawl_chips = $w_row->real_chips_withdrawal + $item->winnings + $bid_amount;
                        $updated_withdrawl_chips = $w_row->real_chips_withdrawal + $item->winnings;
                        $this->wallet_model->set_updated_real_chips_withdrawl($updated_withdrawl_chips, $item->wallet_account_id);

                        //updating in play real chips
                        $updated_in_play_real_chips = $w_row->in_play_real_chips - $bid_amount;
                        $this->wallet_model->update_in_play_real_chips($updated_in_play_real_chips, $item->wallet_account_id);
                    } else {
                        $item->winnings = "-" . $bid_amount;
                        $w_row = $this->wallet_model->get_wallet($item->wallet_account_id);
                        $updated_in_play_real_chips = $w_row->in_play_real_chips - $bid_amount;
                        $this->wallet_model->update_in_play_real_chips($updated_in_play_real_chips, $item->wallet_account_id);
                        //Convert bonus
                        $this->convert_bonus_amount($bid_amount, $game_ref_id, $item->wallet_account_id);
                    }
                }
                //update tds wallet
                $this->update_tds_amount($total_tds_amount, $game_ref_id);
            }else if($split_type == "Manual"){
                $total_admin_commission_amount = $this->get_admin_commission_amount($total_bid_amount);
                $total_gst_amount = $this->get_gst_amount($total_admin_commission_amount);
               
                $this->update_admin_commission_amount($total_admin_commission_amount, $game_ref_id);
                $this->update_gst_commission_amount($total_gst_amount, $game_ref_id);
                $total_tds_amount = 0;
                //$total_bid_amount = $total_bid_amount - $total_admin_commission_amount - $total_gst_amount;
                $total_bid_amount = $total_bid_amount - $total_admin_commission_amount;
                
                if($pool_game_type == 101){
                    $drop_value = 20;
                }else if($pool_game_type == 201){
                    $drop_value = 25;
                }
                
                $total_player_individual_winnings = 0;
                $total_winning_players = 0;
                foreach ($players_data as $item) {
                    if($item->is_winner){
                        $total_winning_players++;
                        //log_message("error", "((".$pool_game_type." - ".$item->loose_points.") /". $drop_value." ) *".$bid_amount);
                        $item->player_individual_winnings = floor((($pool_game_type-1) - $item->loose_points) / $drop_value ) * $bid_amount;
                        $total_player_individual_winnings += $item->player_individual_winnings;
                    }
                }
                $total_remaining_bid_amount = $total_bid_amount- $total_player_individual_winnings;
                $winning_amount_for_each_player = (int) $total_remaining_bid_amount / $total_winning_players;

                foreach ($players_data as $item) {
                    if ($item->is_winner == 1) {
                        
                        $item->winnings = $winning_amount_for_each_player + $item->player_individual_winnings;
                        
                        $tds_amount = 0;
                        $winning_amount_excluded_bid_amount = ($item->winnings - $bid_amount);

                        log_message("error", "Winning amount exclude Bid amount " . $winning_amount_excluded_bid_amount);
                        log_message("error", "Winning amount for each player " . $winning_amount_for_each_player);
                        log_message("error", "Bid Amount " . $bid_amount);
                        log_message("error", "Min amount to apply TDS" . MINIMUM_WINNING_AMOUNT_TO_APPLY_TDS);

                        if ($winning_amount_excluded_bid_amount >= MINIMUM_WINNING_AMOUNT_TO_APPLY_TDS) {
                            $tds_amount = ($winning_amount_excluded_bid_amount * TDS_PERCENTAGE) / 100;
                            $total_tds_amount += $tds_amount;
                        }
                        $item->winnings = $item->winnings - $tds_amount;
                        
                        /*--Adding amount as game won to winnter wallet start--*/
                        $transaction_data = [
                            "transaction_type" => "Credit",
                            "amount" => $item->winnings,
                            "wallet_account_id" => $item->wallet_account_id,
                            "remark" => $game_ref_id . " - Amount credited to withdrawl wallet",
                            "type" => "Game Won",
                            "game_title" => $this->game_title,
                            "game_sub_type" => $this->game_sub_type,
                            "games_id" => $this->games_id
                        ];
                        $this->wallet_transactions_model->create_transaction($transaction_data);
                       /*--Adding amount as game won to winnter wallet end--*/
                        
                        //updating real chips withdrawl
                        $w_row = $this->wallet_model->get_wallet($item->wallet_account_id);
                        $updated_withdrawl_chips = $w_row->real_chips_withdrawal + $item->winnings;
                        $this->wallet_model->set_updated_real_chips_withdrawl($updated_withdrawl_chips, $item->wallet_account_id);

                        //updating in play real chips
                        $updated_in_play_real_chips = $w_row->in_play_real_chips - $bid_amount;
                        $this->wallet_model->update_in_play_real_chips($updated_in_play_real_chips, $item->wallet_account_id);
                    } else {
                        $item->winnings = "-" . $bid_amount;
                        $w_row = $this->wallet_model->get_wallet($item->wallet_account_id);
                        $updated_in_play_real_chips = $w_row->in_play_real_chips - $bid_amount;
                        $this->wallet_model->update_in_play_real_chips($updated_in_play_real_chips, $item->wallet_account_id);
                    }
                    //convert to bonus
                    $this->convert_bonus_amount($bid_amount, $game_ref_id, $item->wallet_account_id);
                }
                //update tds wallet
                $this->update_tds_amount($total_tds_amount, $game_ref_id);
            }else {
                $total_admin_commission_amount = $this->get_admin_commission_amount($total_bid_amount);
                $total_gst_amount = $this->get_gst_amount($total_admin_commission_amount);
               
                $this->update_admin_commission_amount($total_admin_commission_amount, $game_ref_id);
                $this->update_gst_commission_amount($total_gst_amount, $game_ref_id);
                $total_tds_amount = 0;
                $total_bid_amount = $total_bid_amount - $total_admin_commission_amount;
                
                foreach ($players_data as $item) {
                    if ($item->is_winner) {
                        
                        $item->winnings = $total_bid_amount;

                        $winning_amount_excluded_bid_amount = $item->winnings - $bid_amount;
                        log_message("error", "Winning amount exclude Bid amount " . $winning_amount_excluded_bid_amount);
                        log_message("error", "Winning amount for each player " . $winning_amount_for_each_player);
                        log_message("error", "Bid Amount " . $bid_amount);
                        log_message("error", "Min amount to apply TDS " . MINIMUM_WINNING_AMOUNT_TO_APPLY_TDS);
                        $tds_amount = 0;
                        if ($winning_amount_excluded_bid_amount >= MINIMUM_WINNING_AMOUNT_TO_APPLY_TDS) {
                            $tds_amount = ($winning_amount_excluded_bid_amount * TDS_PERCENTAGE) / 100;
                            $total_tds_amount += $tds_amount;
                        }
                        
                        $item->winnings = $item->winnings - $tds_amount;
                        
                        /*--Adding amount as game won to winnter wallet start--*/
                        $transaction_data = [
                            "transaction_type" => "Credit",
                            "amount" => $item->winnings,
                            "wallet_account_id" => $item->wallet_account_id,
                            "remark" => $game_ref_id . " - Amount credited to withdrawl wallet",
                            "type" => "Game Won",
                            "game_title" => $this->game_title,
                            "game_sub_type" => $this->game_sub_type,
                            "games_id" => $this->games_id
                        ];
                        $this->wallet_transactions_model->create_transaction($transaction_data);
                       /*--Adding amount as game won to winnter wallet end--*/
                        
                        //updating real chips withdrawl
                        $w_row = $this->wallet_model->get_wallet($item->wallet_account_id);
                        $updated_withdrawl_chips = $w_row->real_chips_withdrawal + $item->winnings;
                        $this->wallet_model->set_updated_real_chips_withdrawl($updated_withdrawl_chips, $item->wallet_account_id);

                        //updating in play real chips
                        $updated_in_play_real_chips = $w_row->in_play_real_chips - $bid_amount;
                        $this->wallet_model->update_in_play_real_chips($updated_in_play_real_chips, $item->wallet_account_id);
                        
                    } else {
                        $item->winnings = "-" . $bid_amount;
                        $w_row = $this->wallet_model->get_wallet($item->wallet_account_id);
                        $updated_in_play_real_chips = $w_row->in_play_real_chips - $bid_amount;
                        $this->wallet_model->update_in_play_real_chips($updated_in_play_real_chips, $item->wallet_account_id);
                        //convert to bonus
                        $this->convert_bonus_amount($bid_amount, $game_ref_id, $item->wallet_account_id);
                    }
                }
                //convert to bonus
                $this->convert_bonus_amount($bid_amount, $game_ref_id, $item->wallet_account_id);
                //update tds wallet
                $this->update_tds_amount($total_tds_amount, $game_ref_id);
            }
        }
        
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();

            $players_game_details = [];
            foreach ($players_data as $item) {
                $p_wallet = $this->wallet_model->get_wallet_details_by_players_id($item->players_id);
                $players_game_details[] = [
                    "updated_chips" => $p_wallet,
                    "winnings" => $item->winnings,
                    "admin_commission_amount" => 0,
                    "tds_amount" => 0,
                    "gst_amount" => 0,
                    "access_token"=> $item->access_token
                ];
            }

            $arr = [
                "admin_commission_percentage" => ADMIN_COMMISSION_PERCENTAGE,
                "tds_percentage" => TDS_PERCENTAGE,
                "gst_percentage" => GST_PERCENTAGE,
                "admin_commission_amount" => $total_admin_commission_amount,
                "tds_amount" => $total_tds_amount,
                "gst_amount" => $total_gst_amount,
                "players_game_details" => $players_game_details
            ];
            return $arr;
        }
    }

    function generate_transaction_id() {
        $transaction_id = date("Ymdhis") . generateRandomNumber(3);
        if ($this->db->get_where("wallet_transaction", ["transaction_id" => $transaction_id])->num_rows() == 0) {
            return $transaction_id;
        } else {
            return $this->generate_transaction_id();
        }
    }
    
    function run_for_practice_game($game_row=[], $players_data=[], $number_of_players, $total_bid_amount, $bid_amount, $split_type, $total_loose_points){
        $point_value = $game_row->point_value;
        $pool_game_type = $game_row->pool_game_type;
        $game_type = $game_row->game_type;
        $game_sub_type = $game_row->game_sub_type;
        $pool_deal_prize = $game_row->pool_deal_prize;
        $this->db->trans_begin();
        if ($game_sub_type == "Points") {
            foreach ($players_data as $item) {
                if ($item->is_winner) {
                    $item->winnings = $total_loose_points * $point_value;
                    $w_row = $this->wallet_model->get_wallet($item->wallet_account_id);
                    $updated_fun_chips = $w_row->fun_chips + $item->winnings + $bid_amount;
                    $this->wallet_model->set_updated_fun_chips($updated_fun_chips, $item->wallet_account_id);

                    $updated_in_play_fun_chips = $w_row->in_play_fun_chips - $bid_amount;
                    $this->wallet_model->update_in_play_fun_chips($updated_in_play_fun_chips, $item->wallet_account_id);
                } else {
                    $item->winnings = "-" . ($item->loose_points * $point_value);

                    $w_row = $this->wallet_model->get_wallet($item->wallet_account_id);
                    $updated_fun_chips = $w_row->fun_chips +($bid_amount- ($item->loose_points * $point_value));
                    $this->wallet_model->set_updated_fun_chips($updated_fun_chips, $item->wallet_account_id);

                    $updated_in_play_fun_chips = $w_row->in_play_fun_chips - $bid_amount;
                    $this->wallet_model->update_in_play_fun_chips($updated_in_play_fun_chips, $item->wallet_account_id);
                }
            }
        } else {
            //for deals and pools

            //only for pool game Manual and Auto split will be Happened
            if($split_type == "Auto"){
                $total_winning_players = 0;
                foreach ($players_data as $item) {
                    if ($item->is_winner) {
                        $total_winning_players++;
                    }
                }
                $winning_amount_for_each_player = $total_bid_amount/$total_winning_players;
                foreach ($players_data as $item) {
                    if ($item->is_winner) {
                        //log_message("error", print_r($item, true));
                        $item->winnings = $winning_amount_for_each_player;//instead of pool prize number of players into bid amount is the prize
                        $w_row = $this->wallet_model->get_wallet($item->wallet_account_id);
                        $updated_fun_chips = $w_row->fun_chips + $item->winnings;
                        $this->wallet_model->set_updated_fun_chips($updated_fun_chips, $item->wallet_account_id);

                        $updated_in_play_fun_chips = $w_row->in_play_fun_chips - $bid_amount;
                        $this->wallet_model->update_in_play_fun_chips($updated_in_play_fun_chips, $item->wallet_account_id);
                    } else {
                        $item->winnings = "-" . $bid_amount;
                        $w_row = $this->wallet_model->get_wallet($item->wallet_account_id);
                        $updated_in_play_fun_chips = $w_row->in_play_fun_chips - $bid_amount;
                        $this->wallet_model->update_in_play_fun_chips($updated_in_play_fun_chips, $item->wallet_account_id);
                    }
                }
            }else if($split_type == "Manual"){
                if($pool_game_type == 101){
                    $drop_value = 20;
                }else if($pool_game_type == 201){
                    $drop_value = 25;
                }
                $total_player_individual_winnings = 0;
                $total_winning_players = 0;
                foreach ($players_data as $item) {
                    if($item->is_winner){
                        $total_winning_players++;
                        //log_message("error", "((".$pool_game_type." - ".$item->loose_points.") /". $drop_value." ) *".$bid_amount);
                        $item->player_individual_winnings = floor((($pool_game_type-1) - $item->loose_points) / $drop_value ) * $bid_amount;
                        $total_player_individual_winnings += $item->player_individual_winnings;
                    }
                }
                $total_remaining_bid_amount = $total_bid_amount- $total_player_individual_winnings;
                $winning_amount_for_each_player = (int) $total_remaining_bid_amount / $total_winning_players;

//                log_message("error", "Pool Game Type ".$pool_game_type);
//                log_message("error", "Drop Value ".$drop_value);
//                log_message("error", "Total Bid Amount ".$total_bid_amount);
//                log_message("error", "Number of players ".$number_of_players);
//                log_message("error", "Winning Amount for each player : ".$winning_amount_for_each_player);

                foreach ($players_data as $item) {
                    if ($item->is_winner == 1) {
                        //log_message("error", print_r($item, true));
                        $item->winnings = $winning_amount_for_each_player + $item->player_individual_winnings;//instead of pool prize number of players into bid amount is the prize
                        $w_row = $this->wallet_model->get_wallet($item->wallet_account_id);
                        $updated_fun_chips = $w_row->fun_chips + $item->winnings;
                        $this->wallet_model->set_updated_fun_chips($updated_fun_chips, $item->wallet_account_id);

                        $updated_in_play_fun_chips = $w_row->in_play_fun_chips - $bid_amount;
                        $this->wallet_model->update_in_play_fun_chips($updated_in_play_fun_chips, $item->wallet_account_id);
                    } else {
                        $item->winnings = "-" . $bid_amount;
                        $w_row = $this->wallet_model->get_wallet($item->wallet_account_id);
                        $updated_in_play_fun_chips = $w_row->in_play_fun_chips - $bid_amount;
                        $this->wallet_model->update_in_play_fun_chips($updated_in_play_fun_chips, $item->wallet_account_id);
                    }
                }
            }else {
                foreach ($players_data as $item) {
                    if ($item->is_winner) {
                        $item->winnings = $total_bid_amount;//instead of pool prize number of players into bid amount is the prize
                        $w_row = $this->wallet_model->get_wallet($item->wallet_account_id);
                        $updated_fun_chips = $w_row->fun_chips + $item->winnings;
                        $this->wallet_model->set_updated_fun_chips($updated_fun_chips, $item->wallet_account_id);

                        $updated_in_play_fun_chips = $w_row->in_play_fun_chips - $bid_amount;
                        $this->wallet_model->update_in_play_fun_chips($updated_in_play_fun_chips, $item->wallet_account_id);
                    } else {
                        $item->winnings = "-" . $bid_amount;
                        $w_row = $this->wallet_model->get_wallet($item->wallet_account_id);
                        $updated_in_play_fun_chips = $w_row->in_play_fun_chips - $bid_amount;
                        $this->wallet_model->update_in_play_fun_chips($updated_in_play_fun_chips, $item->wallet_account_id);
                    }
                }
            }

        }

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } 
        $this->db->trans_commit();
        $players_game_details = [];
        foreach ($players_data as $item) {
            $p_wallet = $this->wallet_model->get_wallet_details_by_players_id($item->players_id);
            $players_game_details[] = [
                "updated_chips" => $p_wallet,
                "winnings" => $item->winnings,
                "admin_commission_amount" => 0,
                "tds_amount" => 0,
                "gst_amount" => 0,
                "access_token"=> $item->access_token
            ];
        }

        $arr = [
            "admin_commission_percentage" => 0,
            "tds_percentage" => 0,
            "gst_percentage" => 0,
            "admin_commission_amount" => 0,
            "tds_amount" => 0,
            "gst_amount" => 0,
            "players_game_details" => $players_game_details,
            "total_bid_amount" => $total_bid_amount
        ];
        //log_message("error", print_r($arr, true));
        return $arr;
    }
    
    
    function get_admin_commission_amount($winning_amount){
        $total_admin_commission_amount = ($winning_amount * ADMIN_COMMISSION_PERCENTAGE)/100;
        return $total_admin_commission_amount;
    }
    
    

    function get_gst_amount($total_admin_commission_amount){
        $total_gst_amount = ($total_admin_commission_amount * GST_PERCENTAGE)/100;
        return $total_gst_amount;
    }
    
    function get_tds_amount($winning_amount){
        if($winning_amount< MINIMUM_WINNING_AMOUNT_TO_APPLY_TDS){
            return 0;
        }
        $total_admin_commission_amount = $this->get_admin_commission_amount($winning_amount);
        //$total_gst_amount = $this->get_gst_amount($total_admin_commission_amount);
        
        //$winnings = $winning_amount - ($total_admin_commission_amount +  $total_gst_amount);
        $winnings = $winning_amount - ($total_admin_commission_amount);
        $total_tds_amount = ($winnings * TDS_PERCENTAGE) / 100;
        return $total_tds_amount;
    }
    
    function update_admin_commission_amount($total_admin_commission_amount, $game_ref_id){
        $transaction_data = [
            "transaction_type" => "Credit",
            "amount" => $total_admin_commission_amount,
            "wallet_account_id" => ADMIN_COMMISSION_WALLET_ACCOUNT_ID,
            "remark" => $game_ref_id." - Amount credited to withdrawl wallet",
            "type" => "Commission",
            "game_title" => $this->game_title,
            "game_sub_type" => $this->game_sub_type,
            "games_id" => $this->games_id
        ];
        $this->wallet_transactions_model->create_transaction($transaction_data);
        $w_row = $this->wallet_model->get_wallet(ADMIN_COMMISSION_WALLET_ACCOUNT_ID);
        $updated_withdrawl_chips = $w_row->real_chips_withdrawal + $total_admin_commission_amount;
        $this->wallet_model->set_updated_real_chips_withdrawl($updated_withdrawl_chips,ADMIN_COMMISSION_WALLET_ACCOUNT_ID);
    }
    
    function update_gst_commission_amount($total_gst_amount, $game_ref_id){
        $transaction_data = [
            "transaction_type" => "Credit",
            "amount" => $total_gst_amount,
            "wallet_account_id" => GST_WALLET_ACCOUNT_ID,
            "remark" => $game_ref_id." - Amount credited to withdrawl wallet",
            "type" => "GST",
            "game_title" => $this->game_title,
            "game_sub_type" => $this->game_sub_type,
            "games_id" => $this->games_id
        ];
        $this->wallet_transactions_model->create_transaction($transaction_data);
        $w_row = $this->wallet_model->get_wallet(GST_WALLET_ACCOUNT_ID);
        $updated_withdrawl_chips = $w_row->real_chips_withdrawal + $total_gst_amount;
        $this->wallet_model->set_updated_real_chips_withdrawl($updated_withdrawl_chips,GST_WALLET_ACCOUNT_ID);      
    }
    
    function update_tds_amount($total_tds_amount, $game_ref_id){
        if($total_tds_amount==0){
            return;
        }
        $transaction_data = [
            "transaction_type" => "Credit",
            "amount" => $total_tds_amount,
            "wallet_account_id" => TDS_WALLET_ACCOUNT_ID,
            "remark" => $game_ref_id . " - Amount credited to withdrawal wallet",
            "type" => "TDS",
            "game_title" => $this->game_title,
            "game_sub_type" => $this->game_sub_type,
            "games_id" => $this->games_id
        ];
        $this->wallet_transactions_model->create_transaction($transaction_data);
        $w_row = $this->wallet_model->get_wallet(TDS_WALLET_ACCOUNT_ID);
        $updated_withdrawl_chips = $w_row->real_chips_withdrawal + $total_tds_amount;
        $this->wallet_model->set_updated_real_chips_withdrawl($updated_withdrawl_chips,TDS_WALLET_ACCOUNT_ID);
    }
    
    
    /*function convert_bonus_amount($bid_amount, $game_ref_id, $wallet_account_id){
        //convert bonus and credit to deposit balance and detuct bonus points
        $bonus_amount = ($bid_amount * CASH_GAME_BONUS_CONVERSION_PERCENTAGE) / 100;
        $converstion_amount = $bonus_amount;

        $w_row = $this->wallet_model->get_wallet($wallet_account_id);
        log_message("error", "Bonus available In Wallet is " . $w_row->total_bonus . ", Now converting Points : " . $bonus_amount);
        if ($w_row->total_bonus <= 0) {
            return;
        }
        if ($w_row->total_bonus < $bonus_amount) {
            $bonus_amount = $w_row->total_bonus;
        }

        log_message("error", "Game Type : " . $this->game_title . ", Game Sub Type : " . $this->game_sub_type);
        log_message("error", "Bonus available In Wallet is " . $w_row->total_bonus . ", Now converting Points : " . $bonus_amount);

        $this->db->where("is_expired", 0);
        $this->db->where("transaction_type", "Credit");
        $this->db->where("expiry_date  >= ", date("Y-m-d"));
        $this->db->where("wallet_account_id", $wallet_account_id);
        $this->db->where("(amount - utilized_amount) > 0.01");
        $this->db->order_by("expiry_date", "asc");
        $result = $this->db->get("bonus_transaction_history")->result();

        #log_message("error", "Bonus rows " . print_r($result, true));

        foreach ($result as $item) {
            if ($bonus_amount <= 0) {
                break;
            }
            $availabe_money_on_this_row = $item->amount - $item->utilized_amount; //Available Bonus amount

            if ($availabe_money_on_this_row <= $bonus_amount) {
                $item->utilized_amount = $item->utilized_amount + $availabe_money_on_this_row;
                $debited_amount = $availabe_money_on_this_row;
                $bonus_amount = $bonus_amount - $debited_amount;
            } else {
                $item->utilized_amount = $item->utilized_amount + $bonus_amount;
                $debited_amount = $bonus_amount;
                $bonus_amount = $bonus_amount - $debited_amount; //means 0
            }
            
            $bonus_amount = $item->amount;
            
            $bonus_data = [
                "transaction_type" => "Debit",
                "amount" => $debited_amount,
                "wallet_account_id" => $wallet_account_id,
                "expiry_date" => null,
                "remark" => "Bonus converted to real chips on game play - " . $game_ref_id,
                "type" => "Bonus Conversion",
                "bonus_code" => "",
                "bonus_transaction_history_id" => $item->id,
                "game_title" => $this->game_title,
                "game_sub_type" => $this->game_sub_type,
                "games_id" => $this->games_id
            ];
            $bonus_transaction_history_id = $this->bonus_transactions_model->create_bonus_transaction($bonus_data);

            $this->db->set("bonus_transaction_history_id", $bonus_transaction_history_id);
            $this->db->set("utilized_amount", $item->utilized_amount);
            $this->db->where("id", $item->id);
            $this->db->update("bonus_transaction_history");
        }
        
        $transaction_data = [
            "transaction_type" => "Credit",
            "amount" => $converstion_amount,
            "wallet_account_id" => $wallet_account_id,
            "remark" => $game_ref_id . " - " . CASH_GAME_BONUS_CONVERSION_PERCENTAGE . "% Bonus points converted to deposit Amount credited to deposit wallet",
            "type" => "Bonus Converted On Cash Game Play",
            "game_title" => $this->game_title,
            "game_sub_type" => $this->game_sub_type,
            "games_id" => $this->games_id
        ];
        $this->wallet_transactions_model->create_transaction($transaction_data);
        
        $updated_real_chips = $bonus_amount + $w_row->real_chips_deposit;
        $this->wallet_model->set_updated_real_chips_deposit($updated_real_chips, $wallet_account_id);
        
    }*/
    function convert_bonus_amount($bid_amount, $game_ref_id, $wallet_account_id) {
	#log_message("error", "Bonsus converstion fnc called " . print_r(func_get_args(), true));
	//convert bonus and credit to deposit balance and detuct bonus points
	$bonus_amount = ($bid_amount * CASH_GAME_BONUS_CONVERSION_PERCENTAGE) / 100;
	$converstion_amount = $bonus_amount;

	$w_row = $this->wallet_model->get_wallet($wallet_account_id);
	log_message("error", "Bonus available In Wallet is " . $w_row->total_bonus . ", Now converting Points : " . $bonus_amount);
	if ($w_row->total_bonus <= 0) {
		return;
	}
	if ($w_row->total_bonus < $bonus_amount) {
		$bonus_amount = $w_row->total_bonus;
	}

	log_message("error", "Game Type : " . $this->game_title . ", Game Sub Type : " . $this->game_sub_type);
	log_message("error", "Bonus available In Wallet is " . $w_row->total_bonus . ", Now converting Points : " . $bonus_amount);

	$this->db->where("is_expired", 0);
	$this->db->where("transaction_type", "Credit");
	$this->db->where("expiry_date >= ", date("Y-m-d"));
	$this->db->where("wallet_account_id", $wallet_account_id);
	$this->db->where("(amount - utilized_amount) > 0.01");
	$this->db->order_by("expiry_date", "asc");
	$result = $this->db->get("bonus_transaction_history")->result();

	#log_message("error", "Bonus rows " . print_r($result, true));

	foreach ($result as $item) {
		if ($bonus_amount <= 0) {
			break;
	}
	$availabe_money_on_this_row = $item->amount - $item->utilized_amount; //Available Bonus amount

	if ($availabe_money_on_this_row <= $bonus_amount) {
		$item->utilized_amount = $item->utilized_amount + $availabe_money_on_this_row;
		$debited_amount = $availabe_money_on_this_row;
		$bonus_amount = $bonus_amount - $debited_amount;
	} else {
		$item->utilized_amount = $item->utilized_amount + $bonus_amount;
		$debited_amount = $bonus_amount;
		$bonus_amount = $bonus_amount - $debited_amount; //means 0
	}

	$bonus_data = [
		"transaction_type" => "Debit",
		"amount" => $debited_amount,
		"wallet_account_id" => $wallet_account_id,
		"expiry_date" => null,
		"remark" => "Bonus converted to real chips on game play - " . $game_ref_id,
		"type" => "Bonus Conversion",
		"bonus_code" => "",
		"bonus_transaction_history_id" => $item->id,
		"game_title" => $this->game_title,
		"game_sub_type" => $this->game_sub_type,
		"games_id" => $this->games_id
	];
	$bonus_transaction_history_id = $this->bonus_transactions_model->create_bonus_transaction($bonus_data);

	$this->db->set("bonus_transaction_history_id", $bonus_transaction_history_id);
	$this->db->set("utilized_amount", $item->utilized_amount);
	$this->db->where("id", $item->id);
	$this->db->update("bonus_transaction_history");
	}


	$transaction_data = [
		"transaction_type" => "Credit",
		"amount" => $converstion_amount,
		"wallet_account_id" => $wallet_account_id,
		"remark" => $game_ref_id . " - " . CASH_GAME_BONUS_CONVERSION_PERCENTAGE . "% Bonus points converted to deposit Amount credited to deposit wallet",
		"type" => "Bonus Converted On Cash Game Play",
		"game_title" => $this->game_title,
		"game_sub_type" => $this->game_sub_type,
		"games_id" => $this->games_id
	];
	$this->wallet_transactions_model->create_transaction($transaction_data);

	$updated_real_chips = $converstion_amount + $w_row->real_chips_deposit;
	$this->wallet_model->set_updated_real_chips_deposit($updated_real_chips, $wallet_account_id);
    }
}
