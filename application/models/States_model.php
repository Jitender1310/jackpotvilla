<?php

class States_model extends CI_Model {

    function get_states() {
        $this->db->select("id, state_name");
        $this->db->where("status", 1);
        $this->db->order_by("state_name", "asc");
        $data = $this->db->get("states")->result();
        return $data;
    }

    function get_state_name($id) {
        if (!$id) {
            return "";
        }
        $this->db->select("state_name");
        $this->db->where("id", $id);
        $state_name = $this->db->get("states")->row()->state_name;
        if ($state_name) {
            return $state_name;
        }
        return "";
    }

}
