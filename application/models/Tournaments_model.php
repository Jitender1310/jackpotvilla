<?php

class Tournaments_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->check_and_update_tournament_reg_closed();
        //$this->check_and_update_tournament_reg_started();
    }

    function get_game_lobby_tournaments($players_id) {
        $categories = $this->get_tournament_categories();
        foreach ($categories as $item) {
            $item->tournaments_list = $this->get_available_tournaments_by_category_id($item->id, $players_id);
        }
        return $categories;
    }

    function get_tournament_categories() {
        $this->db->select("id, name, name as display_menu_name, image");
        $this->db->where("status", 1);
        $this->db->order_by("priority", "asc");
        $result = $this->db->get("tournament_categories")->result();
        foreach ($result as $item) {
            $item->display_menu_name = str_replace(" Tournaments", "", $item->name);
            if ($item->image) {
                $item->image = BACK_END_SERVER_URL_FILE_UPLOAD_FOLDER_IMG_PATH . $item->image;
            } else {
                $item->image = "";
            }
        }
        return $result;
    }

    function get_completed_past_tournaments($category_id) {
        $this->db->where("tournament_categories_id", $category_id);
        $this->db->where("status", 1);
        $this->db->where("active", 1);

        $this->db->where("display_upto_on_website_after_complete_tournament > ", date("Y-m-d"));

        $this->db->where("display_upto_on_website_after_complete_tournament IS NOT NULL");
        $this->db->where("(tournament_status = 'Completed')");
        $this->db->order_by("registration_start_date", "desc");

        //$this->db->where("CONCAT(registration_start_date, ' ', registration_start_date) < ", date("Y-m-d H:i:00"));
        //$this->db->where("CONCAT(registration_start_date, ' ', registration_start_date) > ", date("Y-m-d H:i:00", strtotime("-10 days")));
        // $this->db->where("registration_start_date > ", date("Y-m-d", strtotime("today")));

        $this->db->limit(6);
        $data = $this->db->get("cloned_tournaments");
        $result = $data->result();
        
//  echo $this->db->last_query(); die;
        
        foreach ($result as $item) {
            $item->is_past = 1;
        }
        return $result;
    }

    function get_available_tournaments_by_category_id($category_id, $players_id) {
        $this->db->where("tournament_categories_id", $category_id);
        $this->db->where("status", 1);
        $this->db->where("active", 1);

        //$hours = date("H:i:00", strtotime("-4 hours"));

        $this->db->where("(tournament_status = 'Created' OR tournament_status = 'Registration_Start' OR "
                . "tournament_status = 'Registration_Closed' OR tournament_status = 'Running')");

        $this->db->order_by("registration_start_date", "asc");
        $data = $this->db->get("cloned_tournaments");

        $upcoming_tournaments = $data->result();
        $completed_tournaments = $this->get_completed_past_tournaments($category_id);
        

        $arr = [];
        foreach ($upcoming_tournaments as $item) {
            $item->is_past = 0;
            $arr[] = $item;
        }

        foreach ($completed_tournaments as $item) {
            $arr[] = $item;
        }
        
        $result = $arr;
        foreach ($result as $item) {

            $item->number_of_registered_users = $this->get_number_of_joined_users($item->id);

            $item->tournament_structure = $this->get_tournament_structure($item->tournament_structure, $item->max_players);
            $item->prize_distributions = $this->get_tournament_prize_distributions($item->id);

            $item->prize_distributions = calculate_prize_positions($item->max_players, $item->entry_value, $item->number_of_registered_users, $item->prize_distributions, $item->entry_type);
            $item->total_prize_amount = 0;
            foreach ($item->prize_distributions as $p_item) {
                $item->total_prize_amount += $p_item->prize_value;
            }

            $item->tournament_category_name = $this->get_tournament_category_name($item->tournament_categories_id);
            $item->cloned_tournaments_id = $item->id;
            unset($item->id);
            unset($item->no_of_bots);
            unset($item->winning_probability_to);
            unset($item->created_at);
            unset($item->updated_at);
            unset($item->winning_probability_to);
            unset($item->active);
            unset($item->status);
            
            if ($item->registration_start_date == date("Y-m-d")) {
                $item->display_registration_start_date_time = "Today " . date("h:i A", strtotime($item->registration_start_date . " " . $item->registration_start_time));
            } else if ($item->registration_start_date == date("Y-m-d", strtotime("+1 day"))) {
                $item->display_registration_start_date_time = "Tomorrow " . date("h:i A", strtotime($item->registration_start_date . " " . $item->registration_start_time));
            } else {
                $item->display_registration_start_date_time = convert_date_time_to_display_format($item->registration_start_date . " " . $item->registration_start_time);
            }

            if ($item->registration_close_date == date("Y-m-d")) {
                $item->display_registration_close_date_time = "Today " . date("h:i A", strtotime($item->registration_close_date . " " . $item->registration_close_time));
            } else if ($item->registration_start_date == date("Y-m-d", strtotime("+1 day"))) {
                $item->display_registration_close_date_time = "Tomorrow " . date("h:i A", strtotime($item->registration_close_date . " " . $item->registration_close_time));
            } else {
                $item->display_registration_close_date_time = convert_date_time_to_display_format($item->registration_close_date . " " . $item->registration_close_time);
            }

            if ($item->tournament_start_date == date("Y-m-d")) {
                $item->display_tournament_date_time = "Today " . date("h:i A", strtotime($item->tournament_start_date . " " . $item->tournament_start_time));
            } else if ($item->tournament_start_date == date("Y-m-d", strtotime("+1 day"))) {
                $item->display_tournament_date_time = "Tomorrow " . date("h:i A", strtotime($item->tournament_start_date . " " . $item->tournament_start_time));
            } else {
                $item->display_tournament_date_time = convert_date_time_to_display_format($item->tournament_start_date . " " . $item->tournament_start_time);
            }
            
            $item->js_registration_start_date_time =  strtotime($item->registration_start_date . " " . $item->registration_start_time) * 1000;
            $item->js_registration_close_date_time = strtotime($item->registration_close_date . " " . $item->registration_close_time) * 1000;
            $item->js_tournament_date_time = strtotime($item->tournament_start_date . " " . $item->tournament_start_time) * 1000;
            
            
            if ($item->allowed == "All") {
                $club_types = $this->club_types_model->get_club_types();
                $allowed_club_types = [];
                foreach ($club_types as $c_item) {
                    $allowed_club_types[] = $c_item->name;
                }
                $item->allowed_club_types_text = implode(", ", $allowed_club_types);
            } else {
                $selected_club_types = explode(",", $item->allowed_club_types);
                $allowed_club_types = [];
                for ($i = 0; $i < count($selected_club_types); $i++) {
                    $this->db->where("id", $selected_club_types[$i]);
                    $club_types = $this->club_types_model->get_club_types();
                    foreach ($club_types as $c_item) {
                        $allowed_club_types[] = $c_item->name;
                    }
                }
                $item->allowed_club_types_text = implode(", ", $allowed_club_types);
            }

            $item->is_joined = $this->check_is_joined($item->cloned_tournaments_id, $players_id);
            $item->is_waiting_list = 0;
            if ($item->number_of_registered_users >= $item->max_players) {
                $item->is_waiting_list = 1;
            }
        }
        
//        print_r($result);
//        die;
        return $result;
    }

    function check_is_joined($cloned_tournaments_id, $players_id) {
        $this->db->where("players_id", $players_id);
        $this->db->where("player_status", "Joined");
        $this->db->where("cloned_tournaments_id", $cloned_tournaments_id);
        $cnt = $this->db->get("joined_players_for_tournaments")->num_rows();
        if ($cnt) {
            return 1;
        }
        return 0;
    }

    function get_tournament_row($id, $players_id) {
        $this->db->where("id", $id);
        $this->db->where("status", 1);
        $this->db->where("active", 1);
        $data = $this->db->get("cloned_tournaments");
        if (!$data->num_rows()) {
            return [];
        }
        $item = $data->row();

        $item->cloned_tournaments_id = $item->id;
        //unset($item->id);
        unset($item->no_of_bots);
        unset($item->winning_probability_to);
        unset($item->status);
        unset($item->active);

        if ($item->allowed == "All") {
            $club_types = $this->club_types_model->get_club_types();
            $allowed_club_types = [];
            foreach ($club_types as $c_item) {
                $allowed_club_types[] = $c_item->name;
            }
            $item->allowed_club_types_text = implode(", ", $allowed_club_types);
        } else {
            $selected_club_types = explode(",", $item->allowed_club_types);
            $allowed_club_types = [];
            for ($i = 0; $i < count($selected_club_types); $i++) {
                $this->db->where("id", $selected_club_types[$i]);
                $club_types = $this->club_types_model->get_club_types();
                foreach ($club_types as $c_item) {
                    $allowed_club_types[] = $c_item->name;
                }
            }
            $item->allowed_club_types_text = implode(", ", $allowed_club_types);
        }

        $item->number_of_registered_users = $this->get_number_of_joined_users($item->id);
        $item->tournament_structure = $this->get_tournament_structure($item->tournament_structure, $item->max_players);
        $item->prize_distributions = $this->get_tournament_prize_distributions($item->id);

        $item->prize_distributions = calculate_prize_positions($item->max_players, $item->entry_value, $item->number_of_registered_users, $item->prize_distributions, $item->entry_type);
        $item->total_prize_amount = 0;
        foreach ($item->prize_distributions as $p_item) {
            $item->total_prize_amount += $p_item->prize_value;
        }



        if ($item->registration_start_date == date("Y-m-d")) {
            $item->display_registration_start_date_time = "Today " . date("h:i A", strtotime($item->registration_start_date . " " . $item->registration_start_time));
        } else if ($item->registration_start_date == date("Y-m-d", strtotime("+1 day"))) {
            $item->display_registration_start_date_time = "Tomorrow " . date("h:i A", strtotime($item->registration_start_date . " " . $item->registration_start_time));
        } else {
            $item->display_registration_start_date_time = convert_date_time_to_display_format($item->registration_start_date . " " . $item->registration_start_time);
        }

        if ($item->registration_close_date == date("Y-m-d")) {
            $item->display_registration_close_date_time = "Today " . date("h:i A", strtotime($item->registration_close_date . " " . $item->registration_close_time));
        } else if ($item->registration_start_date == date("Y-m-d", strtotime("+1 day"))) {
            $item->display_registration_close_date_time = "Tomorrow " . date("h:i A", strtotime($item->registration_close_date . " " . $item->registration_close_time));
        } else {
            $item->display_registration_close_date_time = convert_date_time_to_display_format($item->registration_close_date . " " . $item->registration_close_time);
        }

        if ($item->tournament_start_date == date("Y-m-d")) {
            $item->display_tournament_date_time = "Today " . date("h:i A", strtotime($item->tournament_start_date . " " . $item->tournament_start_time));
        } else if ($item->tournament_start_date == date("Y-m-d", strtotime("+1 day"))) {
            $item->display_tournament_date_time = "Tomorrow " . date("h:i A", strtotime($item->tournament_start_date . " " . $item->tournament_start_time));
        } else {
            $item->display_tournament_date_time = convert_date_time_to_display_format($item->tournament_start_date . " " . $item->tournament_start_time);
        }

        $item->js_registration_start_date_time = strtotime($item->registration_start_date . " " . $item->registration_start_time) * 1000;
        $item->js_registration_close_date_time = strtotime($item->registration_close_date . " " . $item->registration_close_time) * 1000;
        $item->js_tournament_date_time = strtotime($item->tournament_start_date . " " . $item->tournament_start_time) * 1000;
        


        $item->is_joined = $this->check_is_joined($item->id, $players_id);
        $item->is_waiting_list = 0;
        if ($item->number_of_registered_users >= $item->max_players) {
            $item->is_waiting_list = 1;
        }
        
        $item->joined_players_list = $this->get_joined_players($item->cloned_tournaments_id);
        return $item;
    }
    
    function get_joined_players($id){
        $this->db->select("players.username as player_name");
        $this->db->from("joined_players_for_tournaments");
        $this->db->join("players","players.id=joined_players_for_tournaments.players_id");
        $this->db->where("joined_players_for_tournaments.cloned_tournaments_id", $id);
        $data = $this->db->get()->result();
        //echo $this->db->last_query(); die;
        if($data){
            return $data;
        }else{
            return [];
        }
    }
    
    function get_only_played_players_tournament($id, $limit){
        $this->db->select("players.username as player_name, players.wallet_account_id, joined_players_for_tournaments.*");
        $this->db->from("joined_players_for_tournaments");
        $this->db->where("player_status", "Joined");
        $this->db->join("players","players.id=joined_players_for_tournaments.players_id");
        $this->db->where("joined_players_for_tournaments.cloned_tournaments_id", $id);
        $this->db->limit($limit);
        $data = $this->db->get()->result();
        if($data){
            return $data;
        }else{
            return [];
        }
    }

    function get_tournament_prize_distributions($id) {
        $this->db->select("from_rank, to_rank, min_prize_value, prize_value, extra_prize_info");
        $this->db->where("cloned_tournaments_id", $id);
        $this->db->order_by("from_rank", "asc");
        $data = $this->db->get("cloned_tournaments_prize_distribution_config")->result();
        if ($data) {
            foreach ($data as $item) {
                $item->no_of_players = ($item->to_rank - $item->from_rank) + 1;
                $item->display_position = ordinal($item->from_rank);
            }
            return $data;
        }
        return [];
    }

//        function get_prize_distributions_count() {
//            $prize_distributions_count = count($this->get_tournament_prize_distributions($id));
//            return $prize_distributions_count;
//        }

    function get_tournament_category_name($id) {
        $this->db->where("id", $id);
        $this->db->select("name");
        $name = $this->db->get("tournament_categories")->row()->name;
        return $name;
    }

    function get_tournament_structure($tournament_json_obj, $max_players) {
        $sturcture_obj = json_decode($tournament_json_obj);
        $current_round = 0;
        $arr = [];
        for ($i = 0; $i < $max_players; $i++) {
            $info = calculate_rounds($sturcture_obj, $current_round, $max_players);
            if (count((array) $info)) {
                $arr[] = $info;
                if ($info->qualified >= 6) {
                    $current_round = $info->round_number;
                    $max_players = $info->qualified;
                } else {
                    break;
                }
            }
        }

//        print_r($arr);
//        die;

        return get_round_titles($arr);
    }

    function get_number_of_joined_users($cloned_tournaments_id) {
        $this->db->where("player_status", "Joined");
        $this->db->where("cloned_tournaments_id", $cloned_tournaments_id);
        $cnt = $this->db->get("joined_players_for_tournaments")->num_rows();
        if ($cnt) {
            return $cnt;
        }
        return 0;
    }
    
    function check_and_update_tournament_reg_started(){
        $this->db->where("tournament_status", "Created");
        $this->db->set("tournament_status", "Registration_Start");
        $this->db->where("concat(registration_start_date, ' ', registration_start_time) < ", date("Y-m-d H:i:s"));
        $this->db->update("cloned_tournaments");
    }

    function check_and_update_tournament_reg_closed(){
        $this->db->where("tournament_status", "Registration_Start");
        $this->db->set("tournament_status", "Registration_Closed");
        $this->db->where("concat(registration_close_date, ' ', registration_close_time) < ", date("Y-m-d H:i:s"));
        $this->db->update("cloned_tournaments");
    }
}
