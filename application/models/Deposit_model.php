<?php

class Deposit_model extends CI_Model {

    private $table_name;

    public function __construct() {
        parent::__construct();
        $this->table_name = "deposit_money_transactions";
    }

    function create_request($data) {
        $data["transaction_id"] = $this->get_transaction_id();
        $data["comment"] = "Transaction Initiated at " . date("d-m-Y h:i:s A");
        $this->db->set($data);
        $this->db->set("created_at", CREATED_DATE_TIME);
        $this->db->insert($this->table_name);
        return $data["transaction_id"];
    }

    function get_transaction_id() {
        $uuid = "DP" . date("dmYH") . generateRandomNumber(6);
        if ($this->db->get_where($this->table_name, ["transaction_id" => $uuid])->num_rows() == 0) {
            return $uuid;
        } else {
            return $this->get_transaction_id();
        }
    }

    function get_depositing_row($transaction_id) {
        $this->db->where("transaction_id", $transaction_id);
        $row = $this->db->get($this->table_name)->row();
        if ($row) {
            $row->player_details = $this->player_profile_model->get_profile_information($row->players_id);
            return $row;
        }
        return false;
    }

    function update_with_signature($hash, $transaction_id) {
        $this->db->set("signature", $hash);
        $this->db->where("transaction_id", $transaction_id);
        $this->db->update($this->table_name);
        if ($this->db->affected_rows()) {
            return true;
        }
        return true;
    }

    function mark_as_transaction_successful($transaction_id, $pg_ref_id, $response_obj_json) {

        $row = $this->get_depositing_row($transaction_id);
        $wallet_account_uuid = $row->player_details->wallet_account->_uuid;

        $wallet_account_id = $this->wallet_model->get_wallet_by_uuid($wallet_account_uuid)->id;

        $this->db->trans_begin();

        $this->db->where("transaction_id", $transaction_id);
        $this->db->set("comment", $row->comment . " Transaction Success at " . date("d-m-Y h:i:s A"));
        //$this->db->set("updated_at",time());
        $this->db->set("payment_gateway_id", $pg_ref_id);
        $this->db->set("payment_gateway_response_log", $response_obj_json);
        $this->db->set("transaction_status", "Success");
        $this->db->update($this->table_name);
        if ($this->db->affected_rows()) {

            $transaction_data = [
                "transaction_type" => "Credit",
                "amount" => $row->credits_count,
                "wallet_account_id" => $wallet_account_id,
                "remark" => "Amount deposited to wallet",
                "type" => "Add Cash",
            ];

            $this->wallet_transactions_model->create_transaction($transaction_data);

            $this->wallet_model->update_real_chips_deposit($wallet_account_uuid, $row->credits_count);

            $wallet_info = $this->wallet_model->get_wallet_by_uuid($wallet_account_uuid);

            $this->player_notifications_model->send_payment_success_message($row->players_id, $row->credits_count, $wallet_info->real_chips_deposit, $transaction_id);

            $this->check_transaction_and_update_bonus_points($transaction_id);

            $this->check_referal_bonus_and_update($transaction_id);

            $this->db->trans_complete();

            $slab_detail = $this->get_loyalty_slab_by_id($wallet_info->loyalty_slab_id);
            $bonus_percentage = $slab_detail->bonus_percentage;
            $credit_bonus_amount = ($row->credits_count * $bonus_percentage) / 100;
            $current_balance = $wallet_info->real_chips_deposit + $credit_bonus_amount;

            $history_data = array(
                'transaction_type' => 'Credit Bonus',
                'amount' => $credit_bonus_amount,
                'wallet_account_id' => $wallet_account_id,
                'remark' => 'Deposit Bonus',
                'type' => 'Deposit Bonus',
                'game_title' => $gameReference,
                'game_sub_type' => $gameReference,
                'games_id' => $game_id,
                'closing_balance' => $current_balance,
                'created_date_time' => date('Y-m-d H:i:s')
            );
            $this->games_model->add_wallet_transaction_history($history_data);
            $this->wallet_model->set_updated_real_chips_deposit($current_balance, $wallet_account_id);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                //ping sfs
                ping_to_sfs_server(SFS_COMMAND_USER_CHIPS_UPDATED . $row->player_details->access_token);
            }
        } else {
            return false;
        }
    }

    function get_loyalty_slab_by_id($slab_id){
        $this->db->from('loyalty_slabs');
        $this->db->where('id', $slab_id);
        return $this->db->get()->row();
    }

    function mark_as_transaction_pending($transaction_id, $pg_ref_id, $response_obj_json) {
        $row = $this->get_depositing_row($transaction_id);
        $this->db->trans_begin();

        $this->db->where("transaction_id", $transaction_id);
        $this->db->set("comment", $row->comment . " Status Pending at " . date("d-m-Y h:i:s A"));
        //$this->db->set("updated_at",time());
        $this->db->set("payment_gateway_id", $pg_ref_id);
        $this->db->set("payment_gateway_response_log", $response_obj_json);
        $this->db->set("transaction_status", "Pending");
        $this->db->update($this->table_name);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
        }
    }

    function mark_as_transaction_faliure($transaction_id, $pg_ref_id, $response_obj_json) {

        $row = $this->get_depositing_row($transaction_id);
        $wallet_account_uuid = $row->player_details->wallet_account->_uuid;

        $this->db->trans_begin();

        $this->db->where("transaction_id", $transaction_id);
        $this->db->set("comment", $row->comment . " Transaction Failed at " . date("d-m-Y h:i:s A"));
        //$this->db->set("updated_at",time());
        $this->db->set("payment_gateway_id", $pg_ref_id);
        $this->db->set("payment_gateway_response_log", $response_obj_json);
        $this->db->set("transaction_status", "Failed");
        $this->db->update($this->table_name);
        if ($this->db->affected_rows()) {
            $wallet_info = $this->wallet_model->get_wallet_by_uuid($wallet_account_uuid);
            $this->player_notifications_model->send_payment_failure_message($row->players_id, $row->credits_count, $wallet_info->real_chips_deposit, $transaction_id);
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
            }
        } else {
            return false;
        }
    }

    function get_payment_history($filters = [], $players_id) {
        $only_cnt = false;
        if (!isset($filters["start"]) && !isset($filters["limit"])) {
            $only_cnt = true;
        }
        $this->db->select("comment, transaction_id, transaction_status");
        $this->db->select("created_at, credits_count");
        $this->db->where("players_id", $players_id);
        $this->db->from($this->table_name);
        if ($only_cnt) {
            $cnt = $this->db->get()->num_rows();
            //echo $this->db->last_query(); die;
            return $cnt;
        }
        $this->db->order_by("id", "desc");
        $this->db->limit($filters["limit"], $filters["start"]);
        $data = $this->db->get();
        if ($data->num_rows() == 0) {
            return [];
        }
        $data = $data->result();
        foreach ($data as $item) {
            if ($item->transaction_status == "Success") {
                $item->transaction_bootstrap_class = "text-success";
            } else {
                $item->transaction_bootstrap_class = "text-danger";
            }
            $item->created_at = date(DATE_TIME_FORMAT, strtotime($item->created_at));
        }
        return $data;
    }

    function check_referal_bonus_and_update($transaction_id) {
        $row = $this->get_depositing_row($transaction_id);
        $credits_count = $row->credits_count;
        $wallet_account_uuid = $row->player_details->wallet_account->_uuid;
        $wallet_account_id = $this->wallet_model->get_wallet_by_uuid($wallet_account_uuid)->id;
        if ($row->transaction_status != "Success") {
            return [];
        } else {
            if ($this->wallet_transactions_model->check_is_it_first_transaction_to_apply_welcome_bonus($wallet_account_id) == true) {
                $code = $row->player_details->referred_by_code;
                if (isset($code)) {
                    $this->db->select("wallet_account_id");
                    $this->db->limit(1);
                    $refered_person_row = $this->db->get("players")->row();
                    if ($refered_person_row) {
                        $bonus_points = $credits_count;

                        if (MAX_REFERRAL_BONUS_ON_DEPOSIT <= $bonus_points) {
                            $bonus_points = MAX_REFERRAL_BONUS_ON_DEPOSIT;
                        }

                        $expiry_date = date("Y-m-d", strtotime("+1 Month"));
                        $bonus_data = [
                            "transaction_type" => "Credit",
                            "amount" => $bonus_points,
                            "wallet_account_id" => $refered_person_row->wallet_account_id,
                            "expiry_date" => $expiry_date,
                            "remark" => "Referral bonus added",
                            "type" => "Referral bonus Bonus",
                            "bonus_code" => $code
                        ];
                        $this->bonus_transactions_model->create_bonus_transaction($bonus_data);
                    }
                }
            }
        }
    }

    function check_transaction_and_update_bonus_points($transaction_id) {
        $row = $this->get_depositing_row($transaction_id);
        $credits_count = $row->credits_count;
        $wallet_account_uuid = $row->player_details->wallet_account->_uuid;
        $wallet_account_id = $this->wallet_model->get_wallet_by_uuid($wallet_account_uuid)->id;
        if ($row->transaction_status != "Success") {
            return [];
        }
        if ($row->bonus_code == WELCOME_BONUS_CODE) {
            //check number of times used
            $this->db->where("wallet_account_id", $wallet_account_id);
            $this->db->where("bonus_code", WELCOME_BONUS_CODE);
            if ($this->db->get("bonus_transaction_history")->num_rows() == 0) {
                if ($this->wallet_transactions_model->check_is_it_first_transaction_to_apply_welcome_bonus($wallet_account_id) == true) {
                    $bonus_points = 0;
                    if ($credits_count < WELCOME_BONUS) {
                        $bonus_points = (int) $credits_count;
                    } else {
                        $bonus_points = (int) WELCOME_BONUS;
                    }
                    $expiry_date = date("Y-m-d", strtotime("+" . WELCOME_BONUS_VALIDITY_DAYS . " days"));

                    $bonus_data = [
                        "transaction_type" => "Credit",
                        "amount" => $bonus_points,
                        "wallet_account_id" => $wallet_account_id,
                        "expiry_date" => $expiry_date,
                        "remark" => "Welcome bonus added",
                        "type" => "Welcome Bonus",
                        "bonus_code" => WELCOME_BONUS_CODE
                    ];
                    $this->bonus_transactions_model->create_bonus_transaction($bonus_data);
                }
            }
        } else if ($row->bonus_code) {
            $response = $this->bonus_transactions_model->check_other_coupon_codes_validation($row->bonus_code, $row->credits_count, $row->players_id);
            if (is_numeric($response)) {
                $oc_row = $this->bonus_transactions_model->get_occassional_bonus_row_by_bonus_code($row->bonus_code);
                if ($oc_row) {
                    $expiry_date = date("Y-m-d", strtotime("+" . $oc_row->valid_days . " days"));
                } else {
                    $dp_row = $this->bonus_transactions_model->get_deposit_bonus_row_by_bonus_code($row->bonus_code);
                    $expiry_date = date("Y-m-d", strtotime("+" . $dp_row->valid_days . " days"));
                }

                $credit_to = $this->bonus_transactions_model->get_bonus_coupon_credit_to_type($row->bonus_code);
                if ($credit_to == "Deposit Wallet") {
                    $transaction_data = [
                        "transaction_type" => "Credit",
                        "amount" => $response,
                        "wallet_account_id" => $wallet_account_id,
                        "remark" => "Bonus $row->bonus_code code amount deposited to wallet",
                        "type" => "Add Cash Bonus",
                    ];
                    $this->wallet_transactions_model->create_transaction($transaction_data);

                    $this->wallet_model->update_real_chips_deposit($wallet_account_uuid, $row->bonus_credits);
                } else if ($credit_to == "Bonus Wallet") {
                    $bonus_data = [
                        "transaction_type" => "Credit",
                        "amount" => $response,
                        "wallet_account_id" => $wallet_account_id,
                        "expiry_date" => $expiry_date,
                        "remark" => "Coupon Bonus added",
                        "type" => "Coupon Bonus",
                        "bonus_code" => $row->bonus_code
                    ];
                    $this->bonus_transactions_model->create_bonus_transaction($bonus_data);
                }
            }
        } else {
            //check and update birth day bonus
            $player_details = $this->player_login_model->get_player_details($row->players_id);
            if(date("m-d") == date("m-d",strtotime($player_details->date_of_birth))){
                //ToDO
                $birth_bonus_points = (int)($row->credits_count * (BIRTH_DAY_BONUS_PERCENTAGE / 100));
                if($birth_bonus_points > MAX_BIRTHDAY_BONUS_POINTS){
                    $birth_bonus_points = MAX_BIRTHDAY_BONUS_POINTS;
                }
                $expiry_date = date("Y-m-d", strtotime("+" . BIRTHDAY_BONUS_VALIDITY_DAYS . " days"));

                $bonus_data = [
                    "transaction_type" => "Credit",
                    "amount" => $birth_bonus_points,
                    "wallet_account_id" => $wallet_account_id,
                    "expiry_date" => $expiry_date,
                    "remark" => "Birthday bonus added",
                    "type" => "Birthday Bonus",
                    "bonus_code" => "BIRTHDAY"
                ];
                $this->bonus_transactions_model->create_bonus_transaction($bonus_data);
            }
        }

        return true;
    }

}
