<?php

class Tournament_call_off_model extends CI_Model {

    function unjoin_all_joined_persons($cloned_tournaments_id) {

        $this->db->where("cloned_tournaments_id", $cloned_tournaments_id);
        $joined_players = $this->db->get("joined_players_for_tournaments")->result();

        foreach ($joined_players as $item) {
            $this->tournaments_joining_model->unjoin($cloned_tournaments_id, $item->players_id);
        }

        return true;
    }

}
