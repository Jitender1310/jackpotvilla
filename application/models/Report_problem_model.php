<?php
class Report_problem_model extends CI_Model{
    
    private $table_name;
    
    function __construct() {
        parent::__construct();
        $this->table_name = "report_problems";
    }
    
    function create($data, $players_id){
        $this->db->set($data);
        $this->db->set("players_id", $players_id);
        $this->db->set("created_at", CREATED_DATE_TIME);
        $this->db->update($this->table_name);
        //Send email to admin about this problem
        return true;
    }
}