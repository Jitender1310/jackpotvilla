<?php

class Rps_transactions_model extends CI_Model{
    
    public $table_name;
    public $txn_ref_number;
    
    public function __construct() {
        parent::__construct();
        $this->table_name = "rps_transaction_history";
    }
    
    function create_transaction($data){
        
        $data["ref_id"] = $this->generate_ref_id();
        $this->set_txn_ref_number($data["ref_id"]);
        $this->txn_ref_number = $data["ref_id"];
        $w_row = $this->wallet_model->get_wallet($data["wallet_account_id"]);
        $current_balance = $w_row->total_rps_points;
        
        if($data["transaction_type"] == "Credit"){
            $closing_balance = $current_balance + $data["amount"];
        }else if($data["transaction_type"] == "Debit"){
            $closing_balance = $current_balance - $data["amount"];
        }
        
        $this->db->set($data);
        $this->db->set("closing_balance", $closing_balance);
        $this->db->set("created_date_time", CREATED_DATE_TIME);
        $this->db->insert($this->table_name);
        return true;
    }
    
    function set_txn_ref_number($txn_ref_number){
        $this->txn_ref_number = $txn_ref_number;
    }
    
    function get_txn_ref_number(){
        return $this->txn_ref_number;
    }
    
    function generate_ref_id() {
        $ref_id = generateRandomNumber(10);
        if ($this->db->get_where($this->table_name, ["ref_id" => $ref_id])->num_rows() == 0) {
            return $ref_id;
        } else {
            return $this->generate_ref_id();
        }
    }
    
    function get_rps_transactions_history($filters=[],$players_id){
        $only_cnt = false;
        if (!isset($filters["start"]) && !isset($filters["limit"])) {
            $only_cnt = true;
        }
        $this->db->select($this->table_name.'.*');
        $this->db->from($this->table_name);
        $this->db->join('players', 'players.wallet_account_id = '.$this->table_name.'.wallet_account_id', 'LEFT');
        $this->db->where('players.id', $players_id);
        $this->db->where('players.status', 1);
        
        if(isset($filters["txn_type"]) && $filters['txn_type'] != 'All'){
            $this->db->where($this->table_name.'.transaction_type', $filters['txn_type']);
        }

        if($only_cnt){
            $cnt = $this->db->get()->num_rows();
            //echo $this->db->last_query(); die;
            return $cnt;
        }
        $this->db->limit($filters["limit"], $filters["start"]);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
        if($data->num_rows()==0){
            return [];
        }
        $data = $data->result();
        foreach($data as $item){
            if($item->transaction_type=="Credit"){
                $item->transaction_bootstrap_class = "text-success";
            }else{
                $item->transaction_bootstrap_class = "text-danger";
            }
            $item->created_date_time =  date(DATE_TIME_FORMAT, strtotime($item->created_date_time));
        }
        return $data;
    }
}