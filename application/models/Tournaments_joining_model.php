<?php

class Tournaments_joining_model extends CI_Model {

    function join($tournaments_id, $players_id) {

        $player_details = $this->player_login_model->get_player_details($players_id);

        $wallet_alccount_id = $player_details->wallet_account_id;


        //check email verified or not
//        if($player_details->email_verified==0){
//            return "EMAIL_VERIFICATION_REQUIRED";
//        }
        //check kyc verified or not
        /* if($player_details->kyc_status!="Verified"){
          return "KYC_VERIFICATION_PENDING";
          } */

        //check atleast one cash transaction done or not
        if ($this->is_joined_or_not($tournaments_id, $players_id)) {
            return "ALREADY_JOINED";
        }

        if (!$this->wallet_transactions_model->check_atleast_one_cash_transaction_done_or_not($wallet_alccount_id)) {
            return "CASH_TRANSACTION_REQUIRED";
        }

        //check is player played atleast one cash Game
        if (!$this->games_model->check_player_played_at_least_one_cash_game($players_id)) {
            return "CASH_GAME_PLAY_REQUIRED";
        }

        $tournament_details = $this->tournaments_model->get_tournament_row($tournaments_id, $players_id);

        if ($tournament_details->tournament_status === "Rejected") {
            return "TOURNAMENT_REJECTED";
        }

        if ($tournament_details->tournament_status === "Registration_Closed") {
            return "TOURNAMENT_REGISTRATION_CLOSED";
        }

        if ($tournament_details->tournament_status === "Completed") {
            return "TOURNAMENT_COMPLETED";
        }

        if ($tournament_details->tournament_status === "Running") {
            return "TOURNAMENT_RUNNING";
        }

        if ($tournament_details->tournament_status !== "Registration_Start") {
            return "REGISTRATION_NOT_STARTED";
        }

        if ($tournament_details->is_joined === 1) {
            return "ALREADY_REGISTERED";
        }

        if ($tournament_details->allowed != "All") {
            if (!in_array($player_details->expertise_level, explode(',', $tournament_details->allowed_club_types))) {
                return "ONLY_FOR_EXPERTS";
            }
        }
        $premimum_category_id = $this->db->get_where("tournament_categories", ["name" => "Premium", "status" => 1])->row()->id;
        $this->db->trans_begin();
        $store_chips = false;
        //check wallet amount is available or not if it non premium tournament

        if ($tournament_details->entry_type == "Cash" && ($tournament_details->tournament_categories_id != $premimum_category_id)) {
            //if it is cash tournament then detuct amount
            if ($this->wallet_model->check_sufficient_credits_has_or_not("Cash", $tournament_details->entry_value, $players_id) == false) {
                return "INSUFFICIENT_FUNDS";
            } else {

                $transaction_data = [
                    "transaction_type" => "Debit",
                    "amount" => $tournament_details->entry_value,
                    "wallet_account_id" => $wallet_alccount_id,
                    "remark" => $tournament_details->ref_id . " - " . " Join",
                    "type" => "Tournament Entry",
                    "game_title" => "Tournament",
                    "game_sub_type" => "Tournament",
                    "games_id" => $tournaments_id
                ];
                //log_message("error", print_r($transaction_data, true));

                $this->wallet_transactions_model->create_transaction($transaction_data);

                $chips_detucted_response = $this->wallet_model->convert_wallet_chips_to_inplay("Cash", $tournament_details->entry_value, $players_id);
                $store_chips = true;
            }
        } else if ($tournament_details->tournament_categories_id == $premimum_category_id) {
            //if its a premium tournament then need to check valid for premium or not

            $frequency_array = ["daily" => 0, "weekly" => 1, "monthly" => 2]; //just for understanding purpose
            $premium_category_array = ["Bronze" => 0, "Silver" => 1, "Gold" => 2, "Platinum" => 3]; //just for understanding purpose

            if ($tournament_details->premium_category == "Bronze") {
                $premium_category_index = 0;
            } else if ($tournament_details->premium_category == "Silver") {
                $premium_category_index = 1;
            } else if ($tournament_details->premium_category == "Gold") {
                $premium_category_index = 2;
            } else if ($tournament_details->premium_category == "Platinum") {
                $premium_category_index = 3;
            }

            /*
              if ($tournament_details->entry_value == 25) {
              $premium_category_index = 0;
              } else if ($tournament_details->entry_value == 50) {
              $premium_category_index = 1;
              } else if ($tournament_details->entry_value == 100) {
              $premium_category_index = 2;
              } else if ($tournament_details->entry_value == 500) {
              $premium_category_index = 3;
              } */

            $game_tracker_info = $this->game_tracker_model->get_my_game_tracker($players_id);
            if ($tournament_details->frequency == "Daily") {
                $frequency_index = 0;
            } else if ($tournament_details->frequency == "Weekly") {
                $frequency_index = 1;
            } else if ($tournament_details->frequency == "Monthly") {
                $frequency_index = 2;
            }

            if ($game_tracker_info[$frequency_index]["items"][$premium_category_index]["eligible"] == 0) {
                return "NOT_ELIGIBLE_FOR_PREMIUM_TOURNAMENTS";
            }
        }

        $id = $this->db->select("max(id) as id")->get("joined_players_for_tournaments")->row()->id + 1;

        $this->db->set("id", $id);
        $this->db->set("joined_date_time", CREATED_DATE_TIME);
        $this->db->set("players_id", $players_id);
        $this->db->set("cloned_tournaments_id", $tournaments_id);
        $this->db->set("player_status", "Joined");

        if ($store_chips) {
            $this->db->set("taken_from_deposit_balance", $chips_detucted_response["taken_from_deposit_balance"]);
            $this->db->set("taken_from_withdrawal_balance", $chips_detucted_response["taken_from_withdrawal_balance"]);
        }

        $this->db->insert("joined_players_for_tournaments");

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function unjoin($tournaments_id, $players_id) {
        $tournament_details = $this->tournaments_model->get_tournament_row($tournaments_id, $players_id);

        if (!$this->is_joined_or_not($tournaments_id, $players_id)) {
            return "ALREADY_UNJOINED";
        }
        
        if ($tournament_details->tournament_status === "Rejected") {
            return "TOURNAMENT_REJECTED";
        }
        
        if ($tournament_details->tournament_status === "Registration_Closed") {
            return "TOURNAMENT_REGISTRATION_CLOSED";
        }
        
        if ($tournament_details->tournament_status === "Running") {
            return "TOURNAMENT_RUNNING";
        }
        
        if ($tournament_details->tournament_status === "Completed") {
            return "TOURNAMENT_COMPLETED";
        }
        
        
        
        
        
        $this->db->trans_begin();

        $player_details = $this->player_login_model->get_player_details($players_id);

        $wallet_alccount_id = $player_details->wallet_account_id;

        $this->db->where("players_id", $players_id);
        $this->db->where("cloned_tournaments_id", $tournaments_id);
        $this->db->where("player_status", "Joined");
        $joined_player_row = $this->db->get("joined_players_for_tournaments")->row();

        $this->db->where("players_id", $players_id);
        $this->db->where("cloned_tournaments_id", $tournaments_id);
        $this->db->where("player_status", "Joined");
        $this->db->delete("joined_players_for_tournaments");

        //if it is cash tournament then refund amount
        if ($tournament_details->entry_type == "Cash" && $tournament_details->premium_category == null) {

            //return amounts
            if ($joined_player_row) {


                $transaction_data = [
                    "transaction_type" => "Credit",
                    "amount" => $tournament_details->entry_value,
                    "wallet_account_id" => $wallet_alccount_id,
                    "remark" => $tournament_details->ref_id . " - " . " Unjoin, amount refund",
                    "type" => "Tournament Unjoin",
                    "game_title" => "Tournament",
                    "game_sub_type" => "Tournament",
                    "games_id" => $tournaments_id
                ];
                //log_message("error", print_r($transaction_data, true));

                $this->wallet_transactions_model->create_transaction($transaction_data);

                $w_row = $this->wallet_model->get_wallet($wallet_alccount_id);
                $updated_withdrawl_chips = $w_row->real_chips_withdrawal + $joined_player_row->taken_from_withdrawal_balance;
                $this->wallet_model->set_updated_real_chips_withdrawl($updated_withdrawl_chips, $wallet_alccount_id);

                $updated_real_chips_deposit = $w_row->real_chips_deposit + $joined_player_row->taken_from_deposit_balance;
                $this->wallet_model->set_updated_real_chips_deposit($updated_real_chips_deposit, $wallet_alccount_id);
            }
        } else if (isset($tournament_details->premium_category)) {
            //do nothing
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }

        return true;
    }

    function is_joined_or_not($tournaments_id, $players_id) {
        $this->db->where("players_id", $players_id);
        $this->db->where("cloned_tournaments_id", $tournaments_id);
        $this->db->where("player_status", "Joined");
        $response = $this->db->get("joined_players_for_tournaments")->num_rows();
        //if it is cash tournament then refund amount
        if ($response) {
            return true;
        } else {
            return false;
        }
    }

}
