<?php

    class Faqs_model extends CI_Model {

        private $table_name = "faqs";

        function get_categories() {
            $this->db->where("status", 1);
            $data = $this->db->get('faq_categories')->result();
            return $data;
        }

        function get($id) {
            $this->db->where("faq_categories_id", $id);
            $this->db->where("status", 1);
            $data = $this->db->get('faqs')->result();
            return $data;
        }

    }
    