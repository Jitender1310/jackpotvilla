<?php

class Game_lobby_model extends CI_Model {

    private $table_name;

    public function __construct() {
        parent::__construct();
        $this->table_name = "games";
    }

    function get_game_section($players_id) {
        $data = [
            "tournaments" => $this->tournaments_model->get_game_lobby_tournaments($players_id),
            "cash" => $this->get_cash_game_categories(),
            "practice" => $this->get_practice_game_categories(),
            "cash_game_types" => $this->get_cash_game_types(),
            "practice_game_types" => $this->get_practice_game_types(),
            "tournament_types" => $this->get_practice_game_types()
        ];
        return $data;
    }

    function get_cash_game_types() {
        $arr = [
            ["key" => "101", "value" => "Rummy (101)", "type" => "pool"],
            ["key" => "201", "value" => "Rummy (201)", "type" => "pool"],
            ["key" => "301", "value" => "Rummy (301)", "type" => "pool"],
            ["key" => "Points", "value" => "Points Rummy", "type" => "points"],
            ["key" => "2", "value" => "Best of 2", "type" => "deals"],
            ["key" => "3", "value" => "Best of 3", "type" => "deals"],
            ["key" => "6", "value" => "Best of 6", "type" => "deals"],
            ["key" => "7", "value" => "Deal 7", "type" => "deals"],
        ];
        return $arr;
    }

    function get_practice_game_types() {
        return $this->get_cash_game_types();
    }

    function get_tournament_types() {
        
    }

    function get_cash_game_categories() {
        $arr = [
            "points" => $this->get_cash_games_by_categy_wise("points"),
            "pool" => $this->get_cash_games_by_categy_wise("pool"),
            "deals" => $this->get_cash_games_by_categy_wise("deals")
        ];
        return $arr;
    }
    
    function get_practice_game_categories(){
        $arr = [
            "points" => $this->get_practice_games_by_categy_wise("points"),
            "pool" => $this->get_practice_games_by_categy_wise("pool"),
            "deals" => $this->get_practice_games_by_categy_wise("deals")
        ];
        return $arr;
    }

    function get_cash_games_by_categy_wise($category_name) {

        if ($category_name == "points") {
            $this->db->order_by("point_value", "asc");
        }

        if ($category_name == "pool") {
            $this->db->order_by("entry_fee", "asc");
        }

        if ($category_name == "deals") {
            $this->db->order_by("entry_fee", "asc");
        }

        $this->db->select("id as games_id, _uuid, deals, entry_fee");
        $this->db->select("game_sub_type, game_title, game_type");
        $this->db->select("number_of_cards, number_of_deck");
        $this->db->select("pool_deal_prize, pool_game_type");
        $this->db->select("reward_points, point_value, seats");
        $this->db->select("token");
        $this->db->where("status", 1);
        $this->db->where("active", 1);
        $this->db->where("lower(game_sub_type)", strtolower($category_name));
        $this->db->where("game_type", "Cash");
        $result = $this->db->get($this->table_name)->result();
        foreach($result as $item){
            if($item->game_sub_type=="Deals" || $item->game_sub_type=="Pool"){
                $item->pool_deal_prize = $item->pool_deal_prize - ($item->pool_deal_prize * (ADMIN_COMMISSION_PERCENTAGE/100));
            }
            if ($item->number_of_deck != 2) {
                $this->db->set("number_of_deck", 2);
                $this->db->update($this->table_name);
                $item->number_of_deck = 2;
            }
        }
        return $result;
    }

    function get_practice_games_by_categy_wise($category_name) {

        if ($category_name == "points") {
            $this->db->order_by("point_value", "asc");
        }

        if ($category_name == "pool") {
            $this->db->order_by("entry_fee", "asc");
        }

        if ($category_name == "deals") {
            $this->db->order_by("entry_fee", "asc");
        }

        $this->db->select("id as games_id, _uuid, deals, entry_fee");
        $this->db->select("game_sub_type, game_title, game_type");
        $this->db->select("number_of_cards, number_of_deck");
        $this->db->select("pool_deal_prize, pool_game_type");
        $this->db->select("reward_points, point_value, seats");
        $this->db->select("token");
        $this->db->where("status", 1);
        $this->db->where("active", 1);
        $this->db->where("lower(game_sub_type)", strtolower($category_name));
        $this->db->where("game_type", "Practice");
        $result = $this->db->get($this->table_name)->result();
        foreach ($result as $item) {
            if ($item->number_of_deck != 2) {
                $this->db->set("number_of_deck", 2);
                $this->db->update($this->table_name);
                $item->number_of_deck = 2;
            }
        }
        return $result;
    }
            
}
