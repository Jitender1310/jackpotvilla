<?php

    class Player_profile_model extends CI_Model {

        public $table_name = "players";

        public function __construct() {
            parent::__construct();
            $this->table_name = "players";
        }

    function get_profile_information($access_token, $all_fields = false) {
        if ($all_fields == true) {
            $this->db->select("*");
        } else {
            $this->db->select("id, username, email, mobile, gender, my_referral_code, access_token");
            $this->db->select("firstname, lastname, date_of_birth");
            $this->db->select("address_line_1, address_line_2, pin_code, states_id, city");
            $this->db->select("email_verified, mobile_verified");
            $this->db->select("address_proof_type, address_proof, address_proof_back_side");
            $this->db->select("pan_card_number, pan_card");
            $this->db->select("address_proof_verified_at, pan_card_verified_at");
            $this->db->select("address_proof_rejected_at, pan_card_rejected_at");
            $this->db->select("address_proof_rejected_reason, pan_card_rejected_reason");
            $this->db->select("address_proof_status, pan_card_status, created_at");
            $this->db->select("wallet_account_id, expertise_level, play_token, profile_pic");
            $this->db->select("my_referral_code, bot_status, referred_by_code");
        }

            $player_details = $this->player_login_model->get_player_details($access_token);
            if (!$player_details->firstname && !$player_details->lastname) {
                $player_details->full_name = "";
            } else {
                $player_details->full_name = $player_details->firstname . " " . $player_details->lastname;
            }
            if (!$player_details->date_of_birth) {
                $player_details->display_date_of_birth = "";
            } else if ($player_details->date_of_birth && $player_details->date_of_birth != "0000-00-00") {
                $player_details->display_date_of_birth = date("d-M-Y", strtotime($player_details->date_of_birth));
                $player_details->date_of_birth = convert_date_to_display_format($player_details->date_of_birth);
            } else if ($player_details->date_of_birth == "0000-00-00") {
                $player_details->display_date_of_birth = "";
                $player_details->date_of_birth = "";
            }
            if ($player_details->mobile) {
                $player_details->display_mobile = "+91 " . $player_details->mobile;
            } else {
                $player_details->display_mobile = "Click edit to add Mobile number";
            }


            $player_details->state_name = $this->states_model->get_state_name($player_details->states_id);
            $player_details->display_address = $player_details->address_line_1 . " " . $player_details->address_line_2;
            $player_details->display_address .= " " . $player_details->city . " " . $player_details->state_name;



            $player_details->kyc_status = "Not Verified";
            $player_details->kyc_status_bootstrap_css = "text-danger";
            if ($player_details->pan_card_status == "Approved" && $player_details->address_proof_status == "Approved") {
                $player_details->kyc_status = "Verified";
                $player_details->kyc_status_bootstrap_css = "text-success";
            }

            if ($player_details->pan_card) {
                if (is_url_exist(KYC_ADMIN_ATTACHMENTS_UPLOAD_FOLDER_PATH . $player_details->pan_card)) {
                    $player_details->pan_card = KYC_ADMIN_ATTACHMENTS_UPLOAD_FOLDER_PATH . $player_details->pan_card;
                } else {
                    $player_details->pan_card = KYC_ATTACHMENTS_UPLOAD_FOLDER_PATH . $player_details->pan_card;
                }
            } else {
                $player_details->pan_card = "";
            }

        if ($player_details->address_proof) {
            if (is_url_exist(KYC_ADMIN_ATTACHMENTS_UPLOAD_FOLDER_PATH . $player_details->address_proof)) {
                $player_details->address_proof = KYC_ADMIN_ATTACHMENTS_UPLOAD_FOLDER_PATH . $player_details->address_proof;
            } else {
                $player_details->address_proof = KYC_ATTACHMENTS_UPLOAD_FOLDER_PATH . $player_details->address_proof;
            }
        } else {
            $player_details->address_proof = "";
        }

        if ($player_details->address_proof_back_side) {
            if (is_url_exist(KYC_ADMIN_ATTACHMENTS_UPLOAD_FOLDER_PATH . $player_details->address_proof_back_side)) {
                $player_details->address_proof_back_side = KYC_ADMIN_ATTACHMENTS_UPLOAD_FOLDER_PATH . $player_details->address_proof_back_side;
            } else {
                $player_details->address_proof_back_side = KYC_ATTACHMENTS_UPLOAD_FOLDER_PATH . $player_details->address_proof_back_side;
            }
        } else {
            $player_details->address_proof_back_side = "";
        }

            if ($player_details->pan_card_status == "Pending Approval") {
                $player_details->pan_card_status_bootstrap_css = "text-warning";
            }

            if ($player_details->address_proof_status == "Pending Approval") {
                $player_details->address_proof_status_bootstrap_css = "text-warning";
            }

            if ($player_details->address_proof_verified_at == "0000-00-00 00:00:00") {
                $player_details->address_proof_verified_at = "";
            }

            if ($player_details->pan_card_verified_at == "0000-00-00 00:00:00") {
                $player_details->pan_card_verified_at = "";
            }

            if ($player_details->address_proof_rejected_at == "0000-00-00 00:00:00") {
                $player_details->address_proof_rejected_at = "";
            }

            if ($player_details->pan_card_rejected_at == "0000-00-00 00:00:00") {
                $player_details->pan_card_rejected_at = "";
            }

            if ($player_details->states_id == 0) {
                $player_details->states_id = "";
            }

            $player_details->registered_at = date(DATE_FORMAT, $player_details->created_at);
            $player_details->wallet_account = $this->wallet_model->get_wallet($player_details->wallet_account_id);

            $player_details->expertise_level_id = $player_details->expertise_level;
            $player_details->expertise_level = $this->db->where("id", $player_details->expertise_level)->get("club_types")->row()->name;


            $player_details->expertise_level_image = $this->get_expertize_level_image($player_details->expertise_level);

            if ($player_details->profile_pic) {
                $player_details->profile_pic = DEFAULT_IMAGE_UPLOAD_FOLDER_PATH . $player_details->profile_pic;
            } else {
                $player_details->profile_pic = STATIC_IMAGES_PATH . "default_user_image.png";
            }

            unset($player_details->created_at);
            if ($all_fields == false) {
                unset($player_details->wallet_account_id);
                unset($player_details->wallet_account->id);
            }
            return $player_details;
        }

        function get_expertize_level_image($level) {
            $ext = ".png";
            $pic_name = ucfirst(strtolower($level)) . $ext;
            return STATIC_IMAGES_PATH . "level_images/" . $pic_name;

//        if($level=="JOKER"){
//            $image = STATIC_IMAGES_PATH."level_images/Joker".$ext;
//        }else if($level=="CLUB"){
//            $image = STATIC_IMAGES_PATH."level_images/Club".$ext;
//        }
        }

        function update_profile($data, $players_id) {
            $this->db->set($data);
            $this->db->where("id", $players_id);
            $this->db->update("players");
            if ($this->db->affected_rows() == 1) {
                $access_token = $this->player_login_model->get_access_token_by_id($players_id);
                ping_to_sfs_server(SFS_COMMAND_USER_PROFILE_UPDATED . $access_token);
            }
            return true;
        }

        function update_new_email($data, $players_id) {
            $email_verification_key = $this->player_login_model->generate_email_verification_key();
            $this->db->set($data);
            $this->db->set("email_verification_key", $email_verification_key);
            $this->db->where("id", $players_id);
            $this->db->update("players");
            $this->player_notifications_model->send_email_verification_link($players_id);
            return true;
        }

        function update_new_mobile($data, $players_id) {
            $this->db->set($data);
            $this->db->where("id", $players_id);
            $this->db->update("players");
            $this->player_notifications_model->send_otp($players_id);
            return true;
        }

        function mark_as_email_verified($email_verification_key, $username, $access_token) {
            $this->db->set("email_verified", 1);
            $this->db->set("email", "new_email", false);
            $this->db->where("access_token", $access_token);
            $this->db->where("username", $username);
            $this->db->where("email_verification_key", $email_verification_key);
            $this->db->update($this->table_name);
            if ($this->db->affected_rows() == 1) {

                $player_id = $this->player_login_model->get_player_id_by_access_token($access_token);
                $player_details = $this->get_profile_information($player_id, true);

                if ($this->db->get_where("temp_used_email_mobile", ["value" => $player_details->email])->num_rows() == 0) {
                    $this->db->set("players_id", $player_id);
                    $this->db->set("value", $player_details->email);
                    $this->db->set("created_date_time", CREATED_DATE_TIME);
                    $this->db->insert("temp_used_email_mobile");
                }

                //check email verified bonus applied or not
                if ($this->bonus_transactions_model->check_is_email_verified_bonus_applied($player_details->wallet_account_id) == false) {
                    $expiry_date = date("Y-m-d", strtotime("+" . EMAIL_VERIFIED_BONUS_VALIDITY_DAYS . " days"));
                    $bonus_data = [
                        "transaction_type" => "Credit",
                        "amount" => BONUS_ON_EMAIL_VERIFIED,
                        "wallet_account_id" => $player_details->wallet_account_id,
                        "expiry_date" => $expiry_date,
                        "remark" => "Email verified bonus",
                        "type" => "Email Verified"
                    ];
                    $this->bonus_transactions_model->create_bonus_transaction($bonus_data);
                    $this->wallet_model->add_email_verified_amount($player_details->wallet_account_id);
                }

                $this->player_login_model->create_login_session($player_details);
                return true;
            } else {
                return false;
            }
        }

        function verify_otp($otp, $players_id) {
            $this->db->where("otp", $otp);
            $this->db->where("otp IS NOT NULL");
            $this->db->where("id", $players_id);
            if ($this->db->get($this->table_name)->num_rows() == 1) {
                $this->db->set("mobile", "new_mobile", false);
                $this->db->set("updated_at", time());
                $this->db->set("otp", "");
                $this->db->where("id", $players_id);
                $this->db->update($this->table_name);
                return true;
            } else {
                return false;
            }
        }

        function mark_as_mobile_verified($players_id) {
            $this->db->set("mobile_verified", 1);
            $this->db->set("otp", "");
            $this->db->set("mobile", "new_mobile", false);
            $this->db->set("new_mobile", "");
            $this->db->where("id", $players_id);
            $this->db->update($this->table_name);
            if ($this->db->affected_rows() == 1) {

                $player_details = $this->get_profile_information($players_id, true);

            if ($this->db->get_where("temp_used_email_mobile", ["value" => $player_details->mobile])->num_rows() == 0) {
                $this->db->set("players_id", $players_id);
                $this->db->set("value", $player_details->mobile);
                $this->db->set("created_date_time", CREATED_DATE_TIME);
                $this->db->insert("temp_used_email_mobile");
            }

                //check mobile verified bonus applied or not
                if ($this->bonus_transactions_model->check_is_mobile_verified_bonus_applied($player_details->wallet_account_id) == false) {
                    $expiry_date = date("Y-m-d", strtotime("+" . MOBILE_VERIFIED_BONUS_VALIDITY_DAYS . " days"));
                    $bonus_data = [
                        "transaction_type" => "Credit",
                        "amount" => BONUS_ON_MOBILE_VERIFIED,
                        "wallet_account_id" => $player_details->wallet_account_id,
                        "expiry_date" => $expiry_date,
                        "remark" => "Mobile verified bonus",
                        "type" => "Mobile Verified"
                    ];
                    $this->bonus_transactions_model->create_bonus_transaction($bonus_data);
                    $this->wallet_model->add_mobile_verified_amount($player_details->wallet_account_id);
                }
                return true;
            } else {
                return false;
            }
        }

        function get_play_now_token($players_id) {
            $play_token = $this->generate_play_token();
            $this->db->set("play_token", $play_token);
            $this->db->set("updated_at", time());
            $this->db->where("id", $players_id);
            $this->db->update($this->table_name);
            return $play_token;
        }

        function generate_play_token() {
            $play_token = generateRandomString(50);
            if ($this->db->get_where($this->table_name, ["play_token" => $play_token])->num_rows() == 0) {
                return $play_token;
            } else {
                return $this->generate_play_token();
            }
        }

        function get_referral_code($access_token) {
            $query = $this->db->get_where($this->table_name, ["access_token" => $access_token]);
            if ($query->num_rows() > 0) {
                return $query->row_array()['my_referral_code'];
            } else {
                return false;
            }
        }

    function get_player_expertize_level_id($players_id) {
        $this->db->select("expertise_level");
        $this->db->where("(access_token = '$players_id' OR id= '$players_id')");
        return $this->db->get($this->table_name)->row()->expertise_level;
    }

}
