<?php

class Player_preferences_model extends CI_Model {

    private $table;

    public function __construct() {
        parent::__construct();
        $this->table = "players";
    }

    function get_preferences($players_id) {
        $this->db->select("contact_by_sms, contact_by_phone, contact_by_email");
        $this->db->select("speak_language, sms_subscriptions, newsletter_subscriptions, bot_status");
        $this->db->select("wallet_account_id");
        $player_details = $this->player_login_model->get_player_details($players_id);
        return $player_details;
    }

    function update_preferences($data, $players_id) {
        $this->db->set($data);
        $this->db->where("id", $players_id);
        $this->db->update($this->table);
        return true;
    }

}
