<?php

class Wallet_transactions_model extends CI_Model{

    public $table_name;
    public function __construct() {
        parent::__construct();
        $this->table_name = "wallet_transaction_history";
    }

    function create_transaction($data){
        $bt = debug_backtrace();
        $caller = array_shift($bt);
        log_message("error", "##ertert##$$ called from ".$caller['file']."---".$caller['line']);
        //log_message("error", "##347##$$ data ".var_dump($data));
        //log_message("error", var_dump($data));
        log_message('debug',print_r($data,TRUE));

        $data["ref_id"] = $this->generate_ref_id();
        $w_row = $this->wallet_model->get_wallet($data["wallet_account_id"]);
        $current_balance = $w_row->real_chips_deposit + $w_row->real_chips_withdrawal;

        #log_message("error", "Current Balance " . $current_balance);

        if ($data["transaction_type"] == "Credit") {
            $closing_balance = $current_balance + $data["amount"];
        }else if($data["transaction_type"] == "Debit"){
            $closing_balance = $current_balance - $data["amount"];
        }

        $this->db->set($data);
        $this->db->set("closing_balance", $closing_balance);
        $this->db->set("created_date_time", CREATED_DATE_TIME);
        $this->db->insert($this->table_name);
        $transaction_id = $this->db->insert_id();

        $this->db->select("access_token");
        $this->db->where("wallet_account_id", $data["wallet_account_id"]);
        $access_token = $this->db->get("players")->row()->access_token;

        ping_to_sfs_server(SFS_COMMAND_USER_CHIPS_UPDATED . $access_token);

        /*if($data['transaction_type'] == 'Debit' || $data['transaction_type'] == 'Credit')
        {
            $type = 'BET';
            if($data['transaction_type'] == 'Credit')
            {
                $type = 'WIN';
            }
            $url = 'http://slim.duoex.com/api/v1/transactions';
            $postData = array(
                'operator_transaction_id' => $transaction_id,
                'provider_transaction_id' => $transaction_id,
                'game_round_id' => $data["ref_id"],
                'game_id' => $data['games_id'],
                'player_id' => $data['wallet_account_id'],
                'amount' => $data["amount"],
                'type' => $type,
                'prev_balance' => $current_balance,
                'new_balance' => $closing_balance,
                'balance' => $closing_balance
            );
            $jsonData = json_encode($postData);
            $fh = fopen('/tmp/rummy.txt', "a");
            fwrite($fh, $jsonData);
            $headers = array('Content-Type: application/json');
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            fwrite($fh, $result);
            fclose($fh);
            curl_close($ch);
        }*/

        return true;
    }

    function generate_ref_id() {
        $ref_id = generateRandomNumber(10);
        if ($this->db->get_where($this->table_name, ["ref_id" => $ref_id])->num_rows() == 0) {
            return $ref_id;
        } else {
            return $this->generate_ref_id();
        }
    }

    function get_wallet_transactions_history($filters=[],$players_id){
        $only_cnt = false;
        if (!isset($filters["start"]) && !isset($filters["limit"])) {
            $only_cnt = true;
        }


        if ($filters["from_date"]) {
            $this->db->where("created_date_time >= ", $filters["from_date"]);
        }

        if ($filters["to_date"]) {
            $this->db->where("created_date_time <= ", $filters["to_date"]);
        }

        $this->db->select($this->table_name . '.*');
        $this->db->from($this->table_name);
        $this->db->join('players', 'players.wallet_account_id = '.$this->table_name.'.wallet_account_id', 'LEFT');
        $this->db->where('players.id', $players_id);
        $this->db->where('players.status', 1);

        if (isset($filters["txn_type"]) && $filters['txn_type'] == 'All') {

        } else if ($filters['txn_type'] == "Add Cash") {
            $this->db->where("(type ='Add Cash')");
        } else if ($filters['txn_type'] == "Games") {
            $this->db->where("(type ='Game Entry' "
                    . "OR type ='Game Won' "
                    . "OR type ='Entry' "
                    . "OR type ='Game Rejoin'"
                    . "OR type ='Game Lost'"
                    . ")");
        } else if ($filters['txn_type'] == "Tournament") {
            $this->db->where("(type ='Tournament Entry' "
                    . "OR type ='Tournament Unjoin' "
                    . "OR type ='Tournament Won' "
                    . ")");
        } else if ($filters['txn_type'] == "TDS") {
            $this->db->where("(type ='TDS')");
        } else if ($filters['txn_type'] == "RPS Redeemed") {
            $this->db->where("(type ='Rps Redeemed')");
        } else if ($filters['txn_type'] == "Game Bonuses") {
            $this->db->where("(type ='Bonus Converted On Game Lost')");
        } else if ($filters['txn_type'] == "Withdraw") {
            $this->db->where("(type ='Withdraw')");
        } else if ($filters['txn_type'] == "Winnings") {
            $this->db->where("(type ='Game Won' "
                    . "OR type ='Tournament Won' "
                    . ")");
        } else if ($filters['txn_type'] == "Cash Bonuses") {
            $this->db->where("(type ='Sign Up Bonus Cash' "
                    . "OR type ='Mobile Verified Bonus Cash' "
                    . "OR type ='Add Cash Bonus' "
                    . ")");
        }

        if($only_cnt){
            $cnt = $this->db->get()->num_rows();
            //echo $this->db->last_query(); die;
            return $cnt;
        }
        $this->db->limit($filters["limit"], $filters["start"]);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
        if($data->num_rows()==0){
            return [];
        }
        $data = $data->result();
        foreach($data as $item){
            if($item->transaction_type=="Credit"){
                $item->transaction_bootstrap_class = "text-success";
            }else{
                $item->transaction_bootstrap_class = "text-danger";
            }
            $item->created_date_time = date(DATE_TIME_FORMAT, strtotime($item->created_date_time));

            if ($item->type == "Game Entry" ||
                    $item->type == "Game Won" ||
                    $item->type == "Game Rejoin" ||
                    $item->type == "Game Lost" ||
                    $item->type == "Entry") {
                $item->type = $item->game_title . " - " . $item->type;
            }
        }
        return $data;
    }

    function check_is_it_first_transaction_to_apply_welcome_bonus($wallet_account_id){
        $this->db->where("wallet_account_id", $wallet_account_id);
        $this->db->where("type", "Add Cash");
        $this->db->limit(1);
        if($this->db->get($this->table_name)->num_rows()==0){
            return true;
        }else{
            return false;
        }
    }

    function check_atleast_one_cash_transaction_done_or_not($wallet_account_id){
        $this->db->where("wallet_account_id", $wallet_account_id);
        $this->db->where("type", "Add Cash");
        $this->db->limit(1);
        if($this->db->get($this->table_name)->num_rows()==0){
            return false;
        }else{
            return true;
        }
    }
}
