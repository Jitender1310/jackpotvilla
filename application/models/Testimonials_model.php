<?php

class Testimonials_model extends CI_Model{
    
    function get_testimonials(){
        $this->db->where("status", 1);
        $data = $this->db->get("testimonials")->result();
        foreach($data as $item){
            $item->image = BACK_END_SERVER_URL_FILE_UPLOAD_FOLDER_IMG_PATH."testimonials/".$item->image;
        }
        return $data;
    }
    
}