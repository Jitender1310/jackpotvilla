<?php

class Player_kyc_model extends CI_Model {

    private $table;

    public function __construct() {
        parent::__construct();
        $this->table = "players";
    }

    function update_kyc($data, $player_id) {

        $this->db->trans_begin();
        $player_details = $this->player_login_model->get_player_details($player_id);

        $prev_details = $player_details;

        if ($player_details->pan_card_status == "Approved" &&
                $player_details->address_proof_status == "Approved") {
            return false;
        }

        if ($player_details->pan_card_status != "Approved" && isset($data["pan_card_number"]) && isset($data["pan_card"])) {
            $this->db->set("pan_card", $data["pan_card"]);
            $this->db->set("pan_card_number", $data["pan_card_number"]);
            $this->db->set("pan_card_status", $data["pan_card_status"]);
        }

        if ($player_details->address_proof_status != "Approved" && isset($data["address_proof"])) {
            $this->db->set("address_proof", $data["address_proof"]);
            $this->db->set("address_proof_type", $data["address_proof_type"]);
            $this->db->set("address_proof_status", $data["address_proof_status"]);
        }

        if ($player_details->address_proof_status != "Approved" && isset($data["address_proof_back_side"])) {
            $this->db->set("address_proof_back_side", $data["address_proof_back_side"]);
            $this->db->set("address_proof_type", $data["address_proof_type"]);
            $this->db->set("address_proof_status", $data["address_proof_status"]);
        }

        $this->db->set("updated_at", time());
        $this->db->where("id", $player_id);
        $this->db->update($this->table);
        
        $new_player_details = $this->player_login_model->get_player_details($player_id);
        
        if($player_details->pan_card != $new_player_details->pan_card){
            $log_data = [
                "comment" => "Pan card image updated",
                "previous_status" => $player_details->pan_card,
                "current_status" => $new_player_details->pan_card,
                "players_id" => $player_id,
                "users_id" => 0
            ];
            $this->create_log($log_data);
        }
        
        if($player_details->pan_card_number != $new_player_details->pan_card_number){
            $log_data = [
                "comment" => "Pan card number updated",
                "previous_status" => $player_details->pan_card_number,
                "current_status" => $new_player_details->pan_card_number,
                "players_id" => $player_id,
                "users_id" => 0
            ];
            $this->create_log($log_data);
        }
        
        
        if($player_details->address_proof_type != $new_player_details->address_proof_type){
            $log_data = [
                "comment" => "Address proof type updated",
                "previous_status" => $player_details->address_proof_type,
                "current_status" => $new_player_details->address_proof_type,
                "players_id" => $player_id,
                "users_id" => 0
            ];
            $this->create_log($log_data);
        }
        
         if($player_details->address_proof != $new_player_details->address_proof){
            $log_data = [
                "comment" => "Address proof image updated ",
                "previous_status" => $player_details->address_proof,
                "current_status" => $new_player_details->address_proof,
                "players_id" => $player_id,
                "users_id" => 0
            ];
            $this->create_log($log_data);
        }
        
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            /* try {
              unlink(KYC_ATTACHMENTS_UPLOAD_FOLDER . $prev_details->address_proof);
              } catch (Exception $ex) {

              }

              try {
              unlink(KYC_ATTACHMENTS_UPLOAD_FOLDER . $prev_details->pan_card);
              } catch (Exception $ex) {
              unlink(KYC_ATTACHMENTS_UPLOAD_FOLDER . $prev_details->pan_card);
              } */

            return true;
        }
    }
    
    function create_log($data){
        $this->db->set($data);
        $this->db->set("created_at", CREATED_DATE_TIME);
        $this->db->insert("player_kyc_verification_log");
        return true;
    }

}
