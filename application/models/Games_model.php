<?php

class Games_model extends CI_Model
{
    public $table_name;

    public function __construct()
    {
        parent::__construct();
        $this->table_name = "games";
    }

    function get_game_row($games_id)
    {
        $this->db->limit(1);
        $this->db->where("id", $games_id);
        $row = $this->db->get($this->table_name)->row();
        return $row;
    }

    function check_player_played_at_least_one_cash_game($players_id)
    {
        $wallet_account_id = $this->wallet_model->get_wallet_account_id_by_players_id($players_id);

        $this->db->where("type", "Game Entry");
        $this->db->limit(1);
        $this->db->where("wallet_account_id", $wallet_account_id);
        if ($this->db->get("wallet_transaction_history")->num_rows() == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    function generate_error_response($method, $type)
    {
        $error_types = array(
            'TOKEN_EXPIRED' => array(
                'code' => 2000,
                'message' => 'Token is invalid or old.'
            ),
            'INSUFFICIENT_BALANCE' => array(
                'code' => 200,
                'message' => 'Not enough credits.'
            )
        );

        return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <message>
                    <result name="' . $method . '" success="0">
                        <returnset>
                            <error value="' . $error_types[$type]['message'] . '" />
                            <errorCode value="' . $error_types[$type]['code'] . '" />
                        </returnset>
                    </result>
                </message>';
    }

    function getPlayerInfo($game_token, $username, $currency, $balance)
    {
        return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <message>
                    <result name="getPlayerInfo" success="1">
                        <returnset>
                            <token value="' . $game_token . '"/>
                            <loginName value="' . $username . '" />
                            <currency value="' . $currency . '" />
                            <balance value="' . $balance . '" />
                        </returnset>
                    </result>
                </message>';
    }

    function getBalance($game_token, $balance)
    {
        return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <message>
                    <result name="getBalance" success="1">
                        <returnset>
                            <token value="' . $game_token . '"/>
                            <balance value="' . $balance . '" />
                        </returnset>
                    </result>
                </message>';
    }

    function bet($game_token, $transactionId, $balance)
    {
        return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <message>
                    <result name="bet" success="1">
                        <returnset>
                            <token value="' . $game_token . '"/>
                            <balance value="' . $balance . '" />
                            <transactionId value="' . $transactionId . '" />
                            <alreadyProcessed value="false" />
                        </returnset>
                    </result>
                </message>';
    }

    function win($game_token, $transactionId, $balance)
    {
        return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <message>
                    <result name="win" success="1">
                        <returnset>
                            <token value="' . $game_token . '"/>
                            <balance value="' . $balance . '" />
                            <transactionId value="' . $transactionId . '" />
                            <alreadyProcessed value="false" />
                        </returnset>
                    </result>
                </message>';
    }

    function authenticate($login, $password, $method)
    {
        $actualLogin = 'nikhil';
        $actualPassword = 'test';

        if($login != $actualLogin || $password != $actualPassword)
        {
            return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                    <message>
                        <result name="' . $method . '" success="0">
                            <returnset>
                                <error value="Credentials are wrong." />
                                <errorCode value="2000" />
                            </returnset>
                        </result>
                    </message>';
        }
        return 0;
    }

    function validate_token($token)
    {
        //TODO This part should be handled through Redis

        $this->db->from('casino_games_logs');
        $this->db->where('token', $token);
        $result = $this->db->get()->row();
    }

    function get_games_by_provider($providerId)
    {
        $this->db->from('casino_games');
        $this->db->where('provider_id', $providerId);
        $result = $this->db->get()->result();
        return $result;
    }

    function get_all_games()
    {
        $this->db->select('casino_games.*, games_providers.name AS game_provider_name');
        $this->db->from('casino_games');
        $this->db->join('games_providers', 'games_providers.id = casino_games.provider_id');
        $this->db->where('casino_games.active', 1);
        $result = $this->db->get()->result_array();
        return $result;
    }

    function get_game_by_id($game_id)
    {
        $this->db->from('casino_games');
        if(is_array($game_id))
        {
            $this->db->where_in('id', $game_id);
            $result = $this->db->get()->result_array();
        }
        else
        {
            $this->db->where('id', $game_id);
            $result = $this->db->get()->row();
        }
        return $result;
    }

    function get_game_by_reference($game_reference)
    {
        $this->db->from('casino_games');
        $this->db->where('reference_code', $game_reference);
        $result = $this->db->get()->row();
        return $result;
    }

    function get_player_by_access_token($access_token)
    {
        $this->db->from('players');
        $this->db->where('access_token', $access_token);
        return $this->db->get()->row();
    }

    function get_player_by_id($player_id)
    {
        $this->db->select('players.*, countries.currency, wallet_account.real_chips_deposit, wallet_account.loyalty_points, wallet_account.loyalty_slab_id AS slab_id, wallet_account.btc_address, wallet_account.btc_private_key, wallet_account.eth_address, wallet_account.eth_private_key');
        $this->db->from('players');
        $this->db->join('countries', 'countries.id = players.country_id');
        $this->db->join('wallet_account', 'wallet_account.id = players.wallet_account_id');
        $this->db->where('players.id', $player_id);
        return $this->db->get()->row();
    }

    function get_game_log_by_token($token)
    {
        $this->db->from('casino_games_logs');
        $this->db->where('token', $token);
        return $this->db->get()->row();
    }

    function add_game_log($log)
    {
        $game_id = $log['game_id'];
        $player_id = $log['player_id'];
        $checkWhere = ["game_id" => $game_id, "player_id" => $player_id];
        $token = $this->generate_game_token($player_id);
        $log['token'] = $token;
        if($this->check_game_log($checkWhere)){
            $this->update_game_log($log, $checkWhere);
        }
        else{
            $this->db->insert('casino_games_logs', $log);
        }
        return $token;
    }

    function check_game_log($where)
    {
        $this->db->from('casino_games_logs');
        $this->db->where($where);
        return $this->db->get()->row();
    }

    function update_game_log($log, $where)
    {
        $this->db->where($where);
        $this->db->update('casino_games_logs', $log);
    }

    function generate_game_token($player_id)
    {
        $token = $this->player_profile_model->get_play_now_token($player_id);
        if ($this->db->get_where('casino_games_logs', ["token" => $token])->num_rows() == 0) {
            return $token;
        } else {
            return $this->generate_game_token($player_id);
        }
    }

    function add_transaction($transaction_data)
    {
        $this->db->insert('casino_games_transactions', $transaction_data);
        return $this->db->insert_id();
    }

    function generate_ref_id()
    {
        $ref_id = generateRandomNumber(10);
        if ($this->db->get_where('wallet_transaction_history', ["ref_id" => $ref_id])->num_rows() == 0) {
            return $ref_id;
        } else {
            return $this->generate_ref_id();
        }
    }

    function add_wallet_transaction_history($history_data)
    {
        $history_data['ref_id'] = $this->generate_ref_id();
        $this->db->insert('wallet_transaction_history', $history_data);
        return $this->db->insert_id();
    }

    function update_loyalty($wallet_account_id, $loyalty_points, $slab_id)
    {
        $this->db->set("loyalty_points", $loyalty_points);
        $this->db->set("loyalty_slab_id", $slab_id);
        $this->db->where('id', $wallet_account_id);
        $this->db->update('wallet_account');
    }

    function get_slab_by_id($slab_id)
    {
        $this->db->from('loyalty_slabs');
        $this->db->where('id', $slab_id);
        return $this->db->get()->result_array();
    }

    function get_all_slabs()
    {
        $this->db->from('loyalty_slabs');
        return $this->db->get()->result_array();
    }

    function get_game_transaction_by_provider_transaction_id($providerTransactionId)
    {
        $this->db->from('casino_games_transactions');
        $this->db->where('provider_transaction_id', $providerTransactionId);
        return $this->db->get()->result_array();
    }

    function get_freespins_by_id($id)
    {
        $this->db->from('casino_games_freespins');
        $this->db->where('id', $id);
        return $this->db->get()->result_array();
    }

    function get_currency_value($currency_1, $currency_2)
    {
        $this->db->from('currency_conversion');
        $this->db->where('currency_1', $currency_1);
        $this->db->where('currency_2', $currency_2);
        return $this->db->get()->row();
    }
}
