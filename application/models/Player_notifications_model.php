<?php

class Player_notifications_model extends CI_Model {

    function registration_success($player_details) {

        $sms_message = "Hi " . $player_details->username.",
Congrats You Have Successfully registered to Rummy Maza.
You are almost there! Login And get your free welcome bonus. 
Enjoy the Free & Cash Games";

        send_message($sms_message, $player_details->mobile);


        /* $this->email->from(NO_REPLY_MAIL, SITE_TITLE);
          $this->email->to($data["email"]);
          if (BCC_EMAIL) {
          $this->email->bcc(BCC_EMAIL);
          }
          $this->email->subject("Welcome to " . SITE_TITLE . "!, Your registration successful ");
          $message = $this->load->view("mail_templates/registration_successful", '', true);
          $this->email->message($message);
          //$this->email->send(); */
    }

    function send_email_verification_link($players_id) {
        $player_details = $this->player_profile_model->get_profile_information($players_id, true);
        if (!$player_details->email_verification_key) {
            $data = [
                "new_email" => $player_details->email
            ];
            $this->update_new_email($data, $players_id);
            $player_details = $this->player_profile_model->get_profile_information($players_id, true);
        }
        $this->email->from(NO_REPLY_MAIL, SITE_TITLE);
        $this->email->to($player_details->email);
        if (BCC_EMAIL) {
            $this->email->bcc(BCC_EMAIL);
        }
        $this->email->subject($player_details->username . " activate your account to get your special Welcome Bonus");
        $data = array();
        $data['username'] = $player_details->username;

        if (SERVER_MODE == "development") {
            $data['email_verification_link'] = DEV_WEBSITE_URL . "verify_email?key=" . $player_details->email_verification_key . "&username=" . $player_details->username . "&access_token=" . $player_details->access_token;
        } else {
            $data['email_verification_link'] = base_url("verify_email?key=" . $player_details->email_verification_key . "&username=" . $player_details->username . "&access_token=" . $player_details->access_token);
        }
        $data['email_verification_link'] = base_url("verify_email?key=" . $player_details->email_verification_key . "&username=" . $player_details->username . "&access_token=" . $player_details->access_token);

        // $message = $this->load->view("mail_templates/send_user_verification_email", $player_details, true);
        $message = $this->load->view('mail_templates/player_registered_mail', $data, true);
        $this->email->message($message);
        $this->email->send();
        return true;
    }

    function send_otp($players_id) {
        $otp = generateFancyPassword();
        $player_details = $this->player_profile_model->get_profile_information($players_id, true);
        $this->db->set("otp", $otp);
        $this->db->where("id", $player_details->id);
        $this->db->update("players");

        $message = SITE_TITLE . " verification code " . $otp;
        send_message($message, $player_details->new_mobile);
        return true;
    }
    
    /*function send_password_resetted($new_password, $players_id){
        $player_details = $this->player_profile_model->get_profile_information($players_id, true);
        $message = SITE_TITLE . " account your new password is  " . $new_password;
        send_message($message, $player_details->new_mobile);
        return true;
    }*/
    
    function send_reset_password_link($players_id){
        $player_details = $this->player_profile_model->get_profile_information($players_id, true);
        $this->email->from(NO_REPLY_MAIL, SITE_TITLE);
        $this->email->to($player_details->email);
        if (BCC_EMAIL) {
            $this->email->bcc(BCC_EMAIL);
        }


        if (SERVER_MODE == "development") {
            $url = DEV_WEBSITE_URL . "reset_password?key=" . $player_details->forgot_password_verification_link . "&username=" . $player_details->username . "&access_token=" . $player_details->access_token;
        } else {
            $url = base_url("reset_password?key=" . $player_details->forgot_password_verification_link . "&username=" . $player_details->username . "&access_token=" . $player_details->access_token);
        }
        

        //$tiny_url = get_tiny_url($url);
         $tiny_url = $url; 
        $player_details->reset_password_link = $tiny_url;
        $this->email->subject("Reset Password");
        $message = $this->load->view("mail_templates/send_reset_password_email", $player_details, true);
        $this->email->message($message);
        $this->email->send();

        $message = "You recently requested to reset your password for your ".SITE_TITLE." account. Click the link below to reset it  " . $tiny_url;
        //send_message($message, $player_details->mobile);
        
        return true;
    }
    
    function send_payment_success_message($players_id, $recharge_credits, $latest_credits, $transaction_id){
        $player_details = $this->player_profile_model->get_profile_information($players_id, true);
        
        $this->email->from(NO_REPLY_MAIL, SITE_TITLE);
        $this->email->to($player_details->email);
        if (BCC_EMAIL) {
            $this->email->bcc(BCC_EMAIL);
        }
        $this->email->subject("Payment Success");
        $message = "Dear ".$player_details->username.", we have received Rs.".$recharge_credits." for Txn ID : ".$transaction_id.", Ledger Balance is Rs.".$latest_credits;
        $this->email->message($message);
        $this->email->send();
        
        $message = "Dear ".$player_details->username.", we have received Rs.".$recharge_credits." for Txn ID : ".$transaction_id.", Ledger Balance is Rs.".$latest_credits;
        send_message($message, $player_details->mobile);
    }

    function send_payment_failure_message($players_id, $recharge_credits, $latest_credits, $transaction_id){
        $player_details = $this->player_profile_model->get_profile_information($players_id, true);
        
        $this->email->from(NO_REPLY_MAIL, SITE_TITLE);
        $this->email->to($player_details->email);
        if (BCC_EMAIL) {
            $this->email->bcc(BCC_EMAIL);
        }
        $this->email->subject("Payment failed");
        $message = "Dear ".$player_details->username.", your transaction with Rs.".$recharge_credits." was failed, Txn ID : ".$transaction_id.". Your Ledger Balance is Rs.".$latest_credits;
        $this->email->message($message);
        $this->email->send();
        
        $message = "Dear ".$player_details->username.", your transaction with Rs.".$recharge_credits." was failed, Txn ID : ".$transaction_id.". Your Ledger Balance is Rs.".$latest_credits;
        send_message($message, $player_details->mobile);
    }

    function send_invitation_to_email($recipment_name, $to_email, $player_details_obj) {
        $name = $player_details_obj->firstname . " " . $player_details_obj->lastname;


        if (SERVER_MODE == "development") {
            $url = DEV_WEBSITE_URL . "?referral_code=" . $player_details_obj->my_referral_code;
        } else {
            $url = base_url() . "?referral_code=" . $player_details_obj->my_referral_code;
        }

        $url = get_tiny_url($url);
        $email_data = [
            "recipment_name"  =>$recipment_name,
            "from_name"=>$player_details_obj->username,
            "referral_link" => $url
        ];
        
        
        $this->email->from(INFO_EMAIL, SITE_TITLE);
        $this->email->to($to_email);
        if (BCC_EMAIL) {
            $this->email->bcc(BCC_EMAIL);
        }
        $this->email->subject("Your Friend ".$player_details_obj->username." has invited You to Play Rummy | Click to Accept");
        $message = $this->load->view("mail_templates/send_email_invitation", $email_data, true);
        $this->email->message($message);
        $this->email->send();
    }

    function send_invitation_to_mobile($recipment_name, $to_mobile, $player_details_obj) {
        $name = $player_details_obj->firstname . " " . $player_details_obj->lastname;

        if (SERVER_MODE == "development") {
            $url = DEV_WEBSITE_URL . "?referral_code=" . $player_details_obj->my_referral_code;
        } else {
            $url = base_url() . "?referral_code=" . $player_details_obj->my_referral_code;
        }

        $link = get_tiny_url($url);
        $message = "$recipment_name Love playing #CLover Rummy. Join me ".SITE_TITLE." to get your Rs.".WELCOME_BONUS." free 
            joining bonus by clicking this link:".$link;
        send_message($message, $to_mobile);
    }
    function send_update_password_message($player_details){
        $this->email->from(NO_REPLY_MAIL, SITE_TITLE);
        $this->email->to($player_details->email);
        if (BCC_EMAIL) {
            $this->email->bcc(BCC_EMAIL);
        }
        $this->email->subject("Change Password");
        //$message = "Dear ".$player_details->firstname." your CLover Rummy Passwords Change Successfully";
        $message = "Dear Rummy Maza player your Rummy Maza Passwords Change Successfully";
        $this->email->message($message);
        $this->email->send();

        //$message = "Dear ".$player_details->firstname." your CLover Rummy Passwords Change Successfully";
        $message = "Dear Rummy Maza player your Rummy Maza Passwords Change Successfully";
        send_message($message, $player_details->mobile);
        return true;
    }
}
