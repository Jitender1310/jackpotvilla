<?php

class Default_avatars_model extends CI_Model{
    
    function get_available_avatars(){
        $this->db->select("id, title, gender, image");
        $this->db->order_by("title", "asc");
        $this->db->where("status", 1);
        $data = $this->db->get("default_avatars")->result();
        foreach($data as $item){
            $item->image = DEFAULT_IMAGE_UPLOAD_FOLDER_PATH.$item->image;
        }
        return $data;
    }
    
    function set_avatar_to_player($avatar_id, $players_id){
         $this->db->select("image");
         $this->db->where("id", $avatar_id);
         $image = $this->db->get("default_avatars")->row()->image;
         
         $this->db->where("id", $players_id);
         $this->db->set("profile_pic", $image);
         $this->db->update("players");
         return true;
    }
}