<?php

    class Withdraw_model extends CI_Model {

        private $table_name = "withdraw_request";

    function add_withdraw_request($validate_response_object) {


        $this->db->trans_begin();

        $players_id = $this->player_login_model->get_logged_player_id();
        $player_details = $this->db->get_where('players', array('id' => $players_id))->row();
        $wallet_account_id = $player_details->wallet_account_id;
        $wallet_details = $this->db->get_where('wallet_account', array('id' => $wallet_account_id))->row();
        $withdrawal_id = $this->generate_withdrawal_id();

        $requested_amount = $validate_response_object["details"]["request_amount"];

        $this->db->set("withdrawal_id", $withdrawal_id);
        $this->db->set("players_id", $players_id);
        $this->db->set("request_status", "Pending");
        $this->db->set("transfer_type", $validate_response_object["details"]["transfer_type"]);
        $this->db->set("transfer_to", $validate_response_object["details"]["transfer_to"]);
        $this->db->set("request_amount", $requested_amount);

        $this->db->set("paytm_mobile_number", $validate_response_object["details"]["paytm_mobile_number"]);
        $this->db->set("paytm_person_name", $validate_response_object["details"]["paytm_person_name"]);

        $this->db->set("account_number", $validate_response_object["details"]["account_number"]);
        //$this->db->set("account_holder_name", $validate_response_object["details"]["account_holder_name"]);
        $this->db->set("account_holder_name", empty($validate_response_object["details"]["account_holder_name"]))?"":$validate_response_object["details"]["account_holder_name"];
        $this->db->set("ifsc_code", $validate_response_object["details"]["ifsc_code"]);
        $this->db->set("bank_name", $validate_response_object["details"]["bank_name"]);
        $this->db->set("branch_name", $validate_response_object["details"]["branch_name"]);

        $this->db->set("tds_percentage", $validate_response_object["details"]["tds_percentage"]);
        $this->db->set("tds_charge", $validate_response_object["details"]["tds_charge"]);
        $this->db->set("service_charge_percentage", $validate_response_object["details"]["service_charge_percentage"]);
        $this->db->set("service_charge", $validate_response_object["details"]["service_charge"]);
        $this->db->set("net_receivable", $validate_response_object["details"]["net_receivable"]);

        $this->db->set("expertize_level_id_at_withdraw_time", $player_details->expertise_level);
        $this->db->set("created_at", time());
        $this->db->set("request_date_time", CREATED_DATE_TIME);
        $this->db->insert('withdraw_request');


        $deduct_amount = $wallet_details->real_chips_withdrawal - $requested_amount;
        $add_amount = $wallet_details->on_hold_real_chips_withdrawal + $requested_amount;
        $this->db->set('real_chips_withdrawal', $deduct_amount);
        $this->db->set('on_hold_real_chips_withdrawal', $add_amount);
        $this->db->where("id", $wallet_account_id);
        $this->db->update("wallet_account");

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            //ping sfs
            ping_to_sfs_server(SFS_COMMAND_USER_CHIPS_UPDATED . $row->player_details->access_token);
            return $withdrawal_id;
        }
        return true;
    }

    function get($filters = [], $players_id) {
        $only_cnt = false;
        if (!isset($filters["start"]) && !isset($filters["limit"])) {
            $only_cnt = true;
        }
        $this->db->select("*");
        $this->db->where("players_id", $players_id);
        $this->db->from($this->table_name);
        if ($only_cnt) {
            $cnt = $this->db->get()->num_rows();
            //echo $this->db->last_query(); die;
            return $cnt;
        }
        $this->db->order_by("id", "desc");
        $this->db->limit($filters["limit"], $filters["start"]);
        $data = $this->db->get();
        if ($data->num_rows() == 0) {
            return [];
        }
        $data = $data->result();
        foreach ($data as $item) {
            if ($item->request_status == "Completed") {
                $item->transaction_bootstrap_class = "text-success";
            } else if ($item->request_status == "Pending") {
                $item->transaction_bootstrap_class = "text-warning";
            } else {
                $item->transaction_bootstrap_class = "text-danger";
            }

            $item->request_date_time = convert_date_time_to_display_format($item->request_date_time);

            $item->created_at = date(DATE_TIME_FORMAT, strtotime($item->created_at));
        }
        return $data;
    }

    function generate_withdrawal_id() {
        $withdrawal_id = date("YmdHis") . rand(100, 999);
        $this->db->where("withdrawal_id", $withdrawal_id);
        $data = $this->db->get($this->table_name)->num_rows();
        if ($data) {
            $this->generate_withdrawal_id();
        } else {
            return $withdrawal_id;
        }
        return [];
    }

    }
    