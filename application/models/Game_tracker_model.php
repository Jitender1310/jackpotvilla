<?php

class Game_tracker_model extends CI_Model {

    public $table_name;

    function __construct() {
        parent::__construct();
        $this->table_name = "game_played_tracker";
    }

    function add_played_record($bid_amount, $players_id, $game_ref_id, $pool_game_type) {

        $allowed_amounts = [25, 50, 100, 500];
        if (!in_array($bid_amount, $allowed_amounts)) {
            return false;
        }

        $remarks = $game_ref_id . " Game Played, Pool Type :" . $pool_game_type;
        $this->db->set("played_date", CREATED_DATE);
        $this->db->set("spend_game_amount", $bid_amount);
        $this->db->set("players_id", $players_id);
        $this->db->set("created_at", CREATED_AT);
        $this->db->set("remarks", $remarks);
        $this->db->insert($this->table_name);
        return true;
    }

    function get_headers() {
        $headers = [
            [
                "title" => "",
                "sub_title" => "",
                "info" => "",
            ],
            [
                "title" => "Bronze",
                "sub_title" => "(25)",
                "info" => "Mister Bronze - One can enter the bronze club on winning Rs.25 pool rummy games on Rummy Maza. The Rs.25 games that you win will be counted and you will be allowed to play the premium tournament. "
                . "Track your wins and stay updated on when you can play the premium tournament.",
                "image" => STATIC_IMAGES_PATH . "premium_levels/bronze.png"
            ],
            [
                "title" => "Silver",
                "sub_title" => "(50)",
                "info" => "Mister Silver - The user can enter the Silver Club by playing and winning Rs.50 pool rummy Games on Rummy Maza. "
                . "This is facilitate your chance in playing premium tournaments and in winning higher prizes.",
                "image" => STATIC_IMAGES_PATH . "premium_levels/silver.png"
            ],
            [
                "title" => "Gold",
                "sub_title" => "(100)",
                "info" => "Mister Gold - The Gold Club is for gamers who play and win Rs.100 pool rummy Games. Your wins will be calculated and you will be allowed to play premium tournaments for Mister Gold.",
                "image" => STATIC_IMAGES_PATH . "premium_levels/gold.png"
            ],
            [
                "title" => "Platinum",
                "sub_title" => "(500)",
                "info" => "Mister Platinum - One can enter the Platinum club by playing cash games worth Rs.500. pool rummy game. These will earn you higher and more exciting and rewarding prizes.",
                "image" => STATIC_IMAGES_PATH . "premium_levels/platinum.png"
            ]
        ];
        return $headers;
    }

    function get_my_game_tracker($players_id) {
        $bronze = $this->get_bronze_track($players_id);
        $silver = $this->get_silver_track($players_id);
        $gold = $this->get_gold_track($players_id);
        $platinum = $this->get_platinum_track($players_id);

        $data = [
            [
                "title" => "Hey Mister",
                "sub_title" => "(Daily)",
                "schedule" => "daily",
                "items" => []
            ],
            [
                "title" => "Weekend Striker",
                "sub_title" => "(Weekends)",
                "schedule" => "weekly",
                "items" => []
            ],
            [
                "title" => "Monthly Bonanza",
                "sub_title" => "(Monthly)",
                "schedule" => "monthly",
                "items" => []
            ]
        ];

        $categories = ["Bronze", "Silver", "Gold", "Platinum"];
        $max = (object) [
                    "bronze" => (object) [
                        "daily" => 5,
                        "weekly" => 25,
                        "monthly" => 100
                    ],
                    "silver" => (object) [
                        "daily" => 4,
                        "weekly" => 20,
                        "monthly" => 80
                    ],
                    "gold" => (object) [
                        "daily" => 3,
                        "weekly" => 15,
                        "monthly" => 60
                    ],
                    "platinum" => (object) [
                        "daily" => 2,
                        "weekly" => 10,
                        "monthly" => 40
                    ]
        ];



        $premimum_category_id = $this->db->get_where("tournament_categories", ["name" => "Premium", "status" => 1])->row()->id;


        for ($i = 0; $i < count($data); $i++) {
            for ($j = 0; $j < count($categories); $j++) {

                $category = $categories[$j];
                $schedule = $data[$i]["schedule"];

                //$category = $category == "Platinum" ? "Diamond" : $category;

                $data[$i]["items"][] = [
//                     "cateogory" => $data[$i]["title"] ." " . $data[$i]["schedule"] . " ",
//                     "schedule" => $data[$i]["schedule"],
                    "title" => $category,
                    "frequency" => $schedule,
                    "played" => ${strtolower($category)}->{$schedule},
                    "max" => $max->{strtolower($category)}->{$schedule},
                    "eligible" => ${strtolower($category)}->{$schedule} >= $max->{strtolower($category)}->{$schedule} ? 1 : 0,
                    "joined" => 0,
                    "premium_category_id" => $premimum_category_id,
                    "premium_tournaments_id" => $this->get_available_premium_tournament_id($schedule, $premimum_category_id, $category)
                ];
            }
        }

        return $data;
    }

    function get_available_premium_tournament_id($schedule, $premimum_category_id, $allowed_premium_category) {

        if ($schedule == "daily") {
            $from_time = strtotime("yesterday 6:00 PM");
            $to_time = strtotime("today 6:00 PM");
        } else if ($schedule == "weekends") {
            $from_time = strtotime("last saturday 6:00 PM");
            $to_time = strtotime("this saturday 6:00 PM");
        } else if ($schedule == "Fortnight") {
            
        } else if ($schedule == "monthly") {
            $from_time = strtotime("first day of this month 6:00 PM");
            $to_time = strtotime("last day of this month 6:00 PM");
        }


        $this->db->select("id");
        $this->db->where("status", 1);
        $this->db->where("tournament_status", "Registration_Start");
        $this->db->where("tournament_categories_id", $premimum_category_id);
        $this->db->where("premium_category", $allowed_premium_category);

        $this->db->order_by("id", "desc");
        $this->db->limit(1);
        $row = $this->db->get("cloned_tournaments")->row();
        $tournament_id = "";
        if ($row) {
            $tournament_id = $row->id;
        }
        return $tournament_id;
    }

    function get_bronze_track($players_id) {
        $arr = (object) [];
        $spend_amount = 25;
        $arr->daily = $this->get_played_count($spend_amount, $players_id, "Daily");
        $arr->weekly = $this->get_played_count($spend_amount, $players_id, "Weekends");
        $arr->monthly = $this->get_played_count($spend_amount, $players_id, "Monthly");
        return $arr;
    }

    function get_silver_track($players_id) {
        $arr = (object) [];
        $spend_amount = 50;
        $arr->daily = $this->get_played_count($spend_amount, $players_id, "Daily");
        $arr->weekly = $this->get_played_count($spend_amount, $players_id, "Weekends");
        $arr->monthly = $this->get_played_count($spend_amount, $players_id, "Monthly");
        return $arr;
    }

    function get_gold_track($players_id) {
        $arr = (object) [];
        $spend_amount = 100;
        $arr->daily = $this->get_played_count($spend_amount, $players_id, "Daily");
        $arr->weekly = $this->get_played_count($spend_amount, $players_id, "Weekends");
        $arr->monthly = $this->get_played_count($spend_amount, $players_id, "Monthly");
        return $arr;
    }

    function get_platinum_track($players_id) {
        $arr = (object) [];
        $spend_amount = 500;
        $arr->daily = $this->get_played_count($spend_amount, $players_id, "Daily");
        $arr->weekly = $this->get_played_count($spend_amount, $players_id, "Weekends");
        $arr->monthly = $this->get_played_count($spend_amount, $players_id, "Monthly");
        return $arr;
    }
    
    function get_diamond_track($players_id) {
        $arr = (object) [];
        $spend_amount = 500;
        $arr->daily = $this->get_played_count($spend_amount, $players_id, "Daily");
        $arr->weekly = $this->get_played_count($spend_amount, $players_id, "Weekends");
        $arr->monthly = $this->get_played_count($spend_amount, $players_id, "Monthly");
        return $arr;
    }

    function get_played_count($spend_amount, $players_id, $frequency) {


        if ($frequency == "Daily") {
            $from_time = strtotime("yesterday 6:00 PM");
            $to_time = strtotime("today 6:00 PM");
        } else if ($frequency == "Weekends") {
            $from_time = strtotime("last saturday 6:00 PM");
            $to_time = strtotime("this saturday 6:00 PM");
        } else if ($frequency == "Fortnight") {
            
        } else if ($frequency == "Monthly") {
            $from_time = strtotime("first day of this month 6:00 PM");
            $to_time = strtotime("last day of this month 6:00 PM");
        }

        $this->db->where("spend_game_amount", $spend_amount);
        $this->db->where("players_id", $players_id);

        $this->db->where("created_at > ", $from_time);
        $this->db->where("created_at < ", $to_time);
        $cnt = $this->db->get("game_played_tracker")->num_rows();
        if ($cnt) {
            return $cnt;
        }
        return 0;
    }

}
