<?php

class Tournament_started_model extends CI_Model {

    function unjoin_all_waitlist_persons($cloned_tournaments_id) {

        $tournament_details = $this->tournaments_model->get_tournament_row($cloned_tournaments_id, 0);

        $this->db->where("player_status", "Joined");
        $this->db->where("cloned_tournaments_id", $cloned_tournaments_id);
        $number_of_records = $this->db->count_all_results("joined_players_for_tournaments");


        $remain_players = $number_of_records - $tournament_details->max_players;

        if ($remain_players == 0) {
            return true;
        }


        //$this->db->limit($tournament_details->max_players, $number_of_records);
        $this->db->limit($remain_players, $tournament_details->max_players);
        $this->db->where("player_status", "Joined");
        $this->db->where("cloned_tournaments_id", $cloned_tournaments_id);
        $joined_players = $this->db->get("joined_players_for_tournaments")->result();

        foreach ($joined_players as $item) {
            $this->tournaments_joining_model->unjoin($cloned_tournaments_id, $item->players_id);
        }

        return true;
    }

}
