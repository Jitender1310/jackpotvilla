<?php

    class Tournament_progress_model extends CI_Model {

        function get_progress($cloned_tournaments_id) {
            $this->db->where("cloned_tournaments_id", $cloned_tournaments_id);
            $this->db->order_by("round_number", "asc");
            $result = $this->db->get("cloned_tournaments_round_wise_data")->result();
            foreach ($result as $item) {
                $item->players_info = json_decode($item->players_info);
                foreach ($item->players_info as $sub_item) {
                    foreach ($sub_item->players as $p_item) {
                        $p_item->player_name = "";
                        $p_item->player_name = $this->db->select("username")->where("id", $p_item->tournamentRegPlayer->players_id)->get("players")->row()->username;
                    }
                }
            }
            return $result;
        }

    }
    