<?php

class Uploaded_image_resize_model extends CI_Model {

    function resize_uploaded_image($uploaded_data) {
        try {
            $configer = array(
                'image_library' => 'gd2',
                'source_image' => $uploaded_data['upload_data']['full_path'],
                'maintain_ratio' => TRUE,
                'width' => 400,
                'height' => 650,
            );
            $this->image_lib->clear();
            $this->image_lib->initialize($configer);
            $this->image_lib->resize();
        } catch (Exception $ex) {
            
        }
    }

}
