<?php

class Player_login_model extends CI_Model {

    public $table_name = "players";

    public function __construct() {
        parent::__construct();
        $this->table_name = "players";
    }

    function get_player_details($access_token = null) {
        if ($this->session->userdata("p_access_token")) {
            $access_token = $this->session->userdata("p_access_token");
        }
        $this->db->where("(access_token = '$access_token' OR id= '$access_token')");
        $player_details = $this->db->get($this->table_name)->row();
        $player_details->active = 1;
        if($player_details->bot_status=="Inactive"){
            $player_details->active = 0;
        }
        
        $player_details->kyc_status = "Not Verified";
        $player_details->kyc_status_bootstrap_css = "text-danger";
        if (isset($player_details->pan_card_status)) {
            if ($player_details->pan_card_status == "Approved" && $player_details->address_proof_status == "Approved") {
                $player_details->kyc_status = "Verified";
                $player_details->kyc_status_bootstrap_css = "text-success";
            }
        }
        $player_details->wallet_info = $this->wallet_model->get_wallet($player_details->wallet_account_id);

        return $player_details;
    }

    function get_player_details_by_username($username) {
        $this->db->where("(username='$username' OR email='$username' OR mobile='$username')");
        $this->db->where("(username IS NOT NULL AND email IS NOT NULL AND mobile IS NOT NULL)");
        $data = $this->db->get($this->table_name);
        if ($data->num_rows() == 0) {
            return FALSE;
        } else {
            $row = $data->row();
            return $row;
        }
    }

    function create_login_session($player_details) {
        $this->session->set_userdata("p_access_token", $player_details->access_token);

        try {
            $ip = $_SERVER["REMOTE_ADDR"];
            if ($ip != "::1") {

                /*

                  $response = get_ip_location_details($ip);
                  if (isset($response->city) && isset($response->region_name) && isset($response->country_name)) {
                  $address = "City : <b>$response->city</b> <br/>
                  Region : <b>$response->region_name</b><br/>
                  Country : <b>$response->country_name</b><br/>
                  TimeZone : <b>$response->time_zone</b><br/>
                  ";

                  $insert_data = array(
                  "address" => $address,
                  "player_id" => $player_details->id,
                  "created_at_date" => CREATED_DATE,
                  "created_at_time" => CREATED_TIME,
                  "login_success" => 1
                  );
                  $this->db->insert("player_login_logs", $insert_data);
                  }
                 * */
            }
        } catch (Exception $exc) {
            log_message(1, "Login Location not found " . $exc);
        }

        return true;
    }

    function check_for_user_logged() {
        if ($this->session->userdata("p_access_token")) {
            return true;
        } else {
            return false;
        }
    }

    function get_logged_player_id() {
        if ($this->input->get_post("access_token")) {
            $access_token = $this->input->get_post("access_token");
            return $this->get_player_id_by_access_token($access_token);
        } else {
            return $this->get_player_id_by_access_token($this->session->userdata("p_access_token"));
        }
    }

    function get_access_token_by_id($players_id) {
        $this->db->select("access_token");
        $this->db->where("id", $players_id);
        $this->db->where("status", 1);
        $player = $this->db->get($this->table_name)->row();
        if ($player) {
            return $player->access_token;
        } else {
            show_forbidden();
        }
    }

    function get_player_id_by_access_token($access_token) {
        $this->db->select("id");
        $this->db->where("access_token", $access_token);
        $this->db->where("status", 1);
        $player = $this->db->get($this->table_name)->row();
        if ($player) {
            return $player->id;
        } else {
            show_forbidden();
        }
    }

    function verify_login($player_details, $password) {
        if ($player_details->password != md5($password . $player_details->salt)) {
            return false;
        } else {
            $this->create_login_session($player_details);
            $this->db->set("last_login", CREATED_DATE_TIME);
            $this->db->where("id", $player_details->id);
            $this->db->update("players");

            $this->player_profile_model->get_play_now_token($player_details->id);

            //check and update daily bonus
            /*
              if ($this->bonus_transactions_model->check_daily_bonus_added_or_not($player_details->wallet_account_id) == false) {
              $expiry_date = date("Y-m-d", strtotime("+" . DAILY_LOGIN_BONUS_VALIDITY_DAYS . " days"));

              $bonus_data = [
              "transaction_type" => "Credit",
              "amount" => DAILY_LOGIN_BONUS_POINTS,
              "wallet_account_id" => $player_details->wallet_account_id,
              "expiry_date" => $expiry_date,
              "remark" => "Daily Login bonus added",
              "type" => "Daily Bonus",
              "bonus_code" => "DAILY_LOGIN"
              ];
              $this->bonus_transactions_model->create_bonus_transaction($bonus_data);
              } */

            return $player_details->access_token;
        }
    }

    function mark_as_welcome_message_seen($access_token) {
        $this->db->where("access_token", $access_token);
        $this->db->set("welcome_message_seen", 1);
        $this->db->update($this->table_name);
    }

    function is_email_existed($email) {
        $this->db->where("email", $email);
        $this->db->select("access_token");
        $row = $this->db->get($this->table_name)->row();
        if ($row) {
            return $row->access_token;
        }
        return false;
    }

    function is_social_account_existed($account_id, $type) {
        $account_id = trim($account_id);
        if ($account_id == "") {
            return false;
        }

        if ($type == "google") {
            $this->db->where("google_account_id", $account_id);
        }

        if ($type == "facebook") {
            $this->db->where("facebook_account_id", $account_id);
        }

        //$this->db->where("facebook_account_id='$account_id' OR google_account_id='$account_id'");
        $this->db->select("access_token");
        $row = $this->db->get($this->table_name)->row();
        if ($row) {
            return $row->access_token;
        }
        return false;
    }

    function is_username_existed($username) {
        $this->db->where("username", $username);
        $this->db->select("access_token");
        $row = $this->db->get($this->table_name)->row();
        if ($row) {
            return $row->access_token;
        }
        return false;
    }

    function check_for_facebook_account_id($access_token, $received_account_id, $type = 'facebook') {
        if ($type === 'facebook') {
            log_message("error", "#@##@##@#1");  
            if ($this->get_player_details($access_token)->facebook_account_id == $received_account_id) {
            log_message("error", "#@##@##@#2");
                return true;
            //} else if (!$this->get_player_details($access_token)->facebook_account_id) {
            }else{
            log_message("error", "#@##@##@#3");
                $this->db->set("facebook_account_id", $received_account_id);
                $this->db->where("access_token", $access_token);
                $this->db->update($this->table_name);
                return true;
            }
        } else if ($type === 'google') {
            if ($this->get_player_details($access_token)->google_account_id == $received_account_id) {
                return true;
            } else if (!$this->get_player_details($access_token)->google_account_id) {
                $this->db->set("google_account_id", $received_account_id);
                $this->db->where("access_token", $access_token);
                $this->db->update($this->table_name);
                return true;
            }
        }

        return false;
    }

    function generate_email_verification_key() {
        $email_verification_key = generateRandomString(30);
        if ($this->db->get_where($this->table_name, ["email_verification_key" => $email_verification_key])->num_rows() == 0) {
            return $email_verification_key;
        } else {
            return $this->generate_email_verification_key();
        }
    }

    function generate_username($username) {
        $new_username = $username . generateRandomNumber(2);
        if ($this->db->get_where($this->table_name, ["username" => $new_username])->num_rows() == 0) {
            return $new_username;
        } else {
            return $this->generate_username($username);
        }
    }

}
