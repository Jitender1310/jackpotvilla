<?php

class Rps_expiry_cron_model extends CI_Model {

    function check_and_update_rps_points() {
        
        //$this->db->where("rps_level_expiry_date <> ''");
        $this->db->where("rps_level_expiry_date  < ", date("Y-m-d"));
        $result = $this->db->get("wallet_account")->result();
        foreach($result as $item){
            $this->db->trans_begin();
            $players_id = $this->wallet_model->get_players_id_by_wallet_account_id($item->id);
            
            if(!$players_id){
                continue;
            }
            
            $player_club_info = $this->club_types_model->get_current_club_info($players_id);
            
            if($player_club_info->current_club_level_id <= 1){
                continue;
            }
            $prev_level_id = $player_club_info->current_club_level_id - 1;
            
            $this->db->where("id", $prev_level_id);
            $this->db->limit(1);
            $club_info = $this->club_types_model->get_club_types();
            
            if($prev_level_id==1){
                $expiry_date = "";
                $updated_expertize_level_id = 1;
                $debitable_rps_points = $player_club_info->current_points - ($club_info[0]->from_points);
            } else {
                $expiry_date = date("Y-m-d", strtotime("+" . $club_info[0]->validity_in_days . " days"));
                $updated_expertize_level_id = $club_info[0]->id;
                $debitable_rps_points = $player_club_info->current_points - ($club_info[0]->from_points);
            }
            
            $transaction_data = [
                "transaction_type" => "Debit",
                "amount" => $debitable_rps_points,
                "wallet_account_id" => $item->wallet_alccount_id,
                "remark" => "Expertize level downgraded",
                "type" => "Expertize Level Downgrade"
            ];
            //log_message("error", print_r($transaction_data, true));

            $this->rps_transactions_model->create_transaction($transaction_data);

            $this->wallet_model->set_updated_rps_points($club_info[0]->to_points, $item->wallet_alccount_id, $players_id, $expiry_date, $updated_expertize_level_id);
            
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
        }
    }

}
