<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Player_register_model extends CI_Model {

    public $table_name = "players";

    public function __construct() {
        parent::__construct();
        $this->table_name = "players";
    }

    function register($data) {
        $this->db->trans_begin();
        $salt = generateRandomString(10);
        $password = $data['password'];
        $enc_password = md5($password . $salt);
        $data["password"] = $enc_password;
        $data["salt"] = $salt;
        $data["created_at"] = CREATED_AT;
        $data["email"] = trim($data["email"]);
        $data["new_email"] = $data["email"];
        $data["new_mobile"] = isset($data["mobile"])?$data["mobile"]:"";
        $data["my_referral_code"] = $this->player_register_model->generate_my_referral_code();
        $data["access_token"] = $this->player_register_model->generate_access_token();
        $data["wallet_account_id"] = $this->wallet_model->create_wallet("Player");
        $data["email_verification_key"] = $this->player_login_model->generate_email_verification_key();
        $data["date_of_birth"] = isset($data['date_of_birth']) ? $data['date_of_birth'] : date("Y-m-d H:i:00");
        $data["play_token"] = $this->player_profile_model->generate_play_token();

        $this->db->insert($this->table_name, $data);
        
        //Add signup bonus amount
        $this->wallet_model->add_signup_bonus_amount($data["wallet_account_id"]);
        
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            $player_details = $this->player_login_model->get_player_details($data["access_token"]);
            $this->player_login_model->create_login_session($player_details);
            $this->player_notifications_model->registration_success($player_details);
            $this->player_notifications_model->send_email_verification_link($player_details->access_token);
            return $data["access_token"];
        }
    }
    
    function generate_my_referral_code() {
        $my_referral_code = generateRandomString(6);
        if ($this->db->get_where($this->table_name, ["my_referral_code" => $my_referral_code])->num_rows() == 0) {
            return $my_referral_code;
        } else {
            return $this->generate_my_referral_code();
        }
    }

    function generate_access_token() {
        $access_token = generateRandomString(32);
        if ($this->db->get_where($this->table_name, ["access_token" => $access_token])->num_rows() == 0) {
            return $access_token;
        } else {
            return $this->generate_access_token();
        }
    }

    function validate_referral_code($referred_by_code) {
        $this->db->where("my_referral_code", $referred_by_code);
        if ($this->db->get($this->table_name)->num_rows()) {
            return true;
        }
        return false;
    }

    
}
