<?php
class Bring_a_friend_model extends CI_Model{
    
    
    public function __construct() {
        parent::__construct();
    }
    
    function send_invitations($data, $players_id){
        $player_details = $this->player_login_model->get_player_details($players_id);
        foreach($data as $item){
            if($item->email){
                $this->player_notifications_model->send_invitation_to_email($item->name, $item->email, $player_details);
            }
            if($item->mobile){
                $this->player_notifications_model->send_invitation_to_mobile($item->name, $item->mobile, $player_details);
            }
        }
        return true;
    }
    function send_invitations_moblie($data, $players_id){
        $player_details = $this->player_login_model->get_player_details($players_id);
        foreach($data as $item){
            if($item->value){
                $this->player_notifications_model->send_invitation_to_mobile("", $item->value, $player_details);
            }
        }
        return true;
    }
    function send_invitations_email($data, $players_id){
        $player_details = $this->player_login_model->get_player_details($players_id);
        foreach($data as $item){
            if($item->value){
                $this->player_notifications_model->send_invitation_to_email("", $item->value, $player_details);
            }
        }
        return true;
    }
}