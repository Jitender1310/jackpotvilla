<?php

class Bonus_transactions_model extends CI_Model{
    
    public $table_name;
    public function __construct() {
        parent::__construct();
        $this->table_name = "bonus_transaction_history";
    }
    
    function create_bonus_transaction($data){
        
        $data["ref_id"] = $this->generate_ref_id();
        $w_row = $this->wallet_model->get_wallet($data["wallet_account_id"]);
        $current_balance = $w_row->total_bonus;
        
        if($data["transaction_type"] == "Credit"){
            $closing_balance = $current_balance + $data["amount"];
        }else if($data["transaction_type"] == "Debit"){
            $closing_balance = $current_balance - $data["amount"];
        }
        
        $this->db->set($data);
        $this->db->set("closing_balance", $closing_balance);
        $this->db->set("created_date_time", CREATED_DATE_TIME);
        $this->db->insert($this->table_name);

        $bonus_transaction_id = $this->db->insert_id();

        //updating the total bonus
        $this->db->set("total_bonus", $closing_balance);
        $this->db->where("id", $data["wallet_account_id"]);
        $this->db->update("wallet_account");
        return $bonus_transaction_id;
    }
    
    function generate_ref_id() {
        $ref_id = generateRandomNumber(10);
        if ($this->db->get_where($this->table_name, ["ref_id" => $ref_id])->num_rows() == 0) {
            return $ref_id;
        } else {
            return $this->generate_ref_id();
        }
    }
    
    function get_bonus_transactions_history($filters=[],$players_id){
        $only_cnt = false;
        if (!isset($filters["start"]) && !isset($filters["limit"])) {
            $only_cnt = true;
        }
        $this->db->select($this->table_name.'.*');
        $this->db->from($this->table_name);
        $this->db->join('players', 'players.wallet_account_id = '.$this->table_name.'.wallet_account_id', 'LEFT');
        $this->db->where('players.id', $players_id);
        $this->db->where('players.status', 1);
        
        if(isset($filters["txn_type"]) && $filters['txn_type'] != 'All'){
            $this->db->where($this->table_name.'.transaction_type', $filters['txn_type']);
        }

        if($only_cnt){
            $cnt = $this->db->get()->num_rows();
            //echo $this->db->last_query(); die;
            return $cnt;
        }
        $this->db->limit($filters["limit"], $filters["start"]);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
        if($data->num_rows()==0){
            return [];
        }
        $data = $data->result();
        foreach($data as $item){
            if($item->transaction_type=="Credit"){
                $item->transaction_bootstrap_class = "text-success";
            }else{
                $item->transaction_bootstrap_class = "text-danger";
            }
            $item->created_date_time =  date(DATE_TIME_FORMAT, strtotime($item->created_date_time));
            if($item->expiry_date){
                $item->expiry_date = convert_date_to_display_format($item->expiry_date);
            }
            if($item->is_expired){
                $item->expiry_status = "Expired";
                $item->expiry_status_class = "text-danger";
            }else{
                $item->expiry_status = "Active";
                $item->expiry_status_class = "text-success";
            }
        }
        return $data;
    }
    
    function check_is_mobile_verified_bonus_applied($wallet_account_id){
        $this->db->where("wallet_account_id", $wallet_account_id);
        $this->db->where("type", "Mobile Verified");
        if($this->db->get($this->table_name)->num_rows()==0){
            return false;
        }else{
            return true;
        }
    }
    
    function validate_bonus_code($bonus_code, $credits_count, $players_id){
        $wallet_account_id= $this->player_profile_model->get_profile_information($players_id, true)->wallet_account_id;
        if($bonus_code==WELCOME_BONUS_CODE){
            //check for coupon 
            $this->db->where("wallet_account_id", $wallet_account_id);
            $this->db->where("bonus_code", $bonus_code);
            if($this->db->get("bonus_transaction_history")->num_rows()==0){
                //check for deposit first transaction or not
                if($this->wallet_transactions_model->check_is_it_first_transaction_to_apply_welcome_bonus($wallet_account_id) == true){
                    if($credits_count < WELCOME_BONUS){
                        return (int)$credits_count;
                    }else{
                        return (int)WELCOME_BONUS;
                    }
                }
                return "INVALID_BONUS_USAGE";
            }
        }else{
            //check for other coupon codes
            $response =  $this->check_other_coupon_codes_validation($bonus_code, $credits_count, $players_id);
            return $response;
        }
    }
    
    function check_other_coupon_codes_validation($bonus_code, $credits_count, $players_id){
        $player_information  = $this->player_profile_model->get_profile_information($players_id, true);        
        //print_r($player_information);
        if($player_information->date_of_birth == date("Y-m-d")){
            //Check birthday coupon code validation
            //No coupon code required on payment success just checked wheather any coupon used
            //for the transaction otherwise credit birthday bonus 
            //This logic already is already i wrote there
        }
        //check occassional bonus validation
        $oc_row = $this->get_occassional_bonus_row_by_bonus_code($bonus_code);
        
        //check deposit bonus validation
        $dp_row = $this->get_deposit_bonus_row_by_bonus_code($bonus_code);
        
        if(!$oc_row && !$dp_row){
            return "INVALID_BONUS_CODE";
        } else if ($oc_row) {
            if ($credits_count < $oc_row->minimum_deposit) {
                return "MIN_DEPOSIT_REQUIRED";
            }
            //row found in occasaion bonus code
            
            //checking number of times used here
            if ($oc_row->applicable_type == "Club Wise") {
                $oc_row->club_wise_config = json_decode($oc_row->club_wise_config);
                foreach ($oc_row->club_wise_config as $c_item) {
                    if ($c_item->id == $player_information->expertise_level_id) {
                        $oc_row->points_percentage = $c_item->points_percentage;
                        $oc_row->max_points = $c_item->max_points;
                        break;
                    }
                }
            }

            if ($oc_row->credit_to == "Bonus Wallet") {
                $this->db->where("wallet_account_id", $player_information->wallet_account_id);
                $this->db->where("bonus_code", $bonus_code);
                $usage_times = $this->db->get("bonus_transaction_history")->num_rows();
            } else if ($oc_row->credit_to == "Deposit Wallet") {
                $this->db->where("players_id", $players_id);
                $this->db->where("bonus_code", $bonus_code);
                $usage_times = $this->db->get("deposit_money_transactions")->num_rows();
            }


            if ($usage_times < $oc_row->usage_times) {
                $creditable_bonus_amount = (int) ($credits_count * ($oc_row->points_percentage / 100));
                if ($creditable_bonus_amount < $oc_row->max_points) {
                    return $creditable_bonus_amount;
                } else {
                    return (int) $oc_row->max_points;
                }
            }else {
                return "MAX_USED";
            }
        } else if ($dp_row) {
            if ($credits_count < $dp_row->minimum_deposit) {
                return "MIN_DEPOSIT_REQUIRED";
            }
            //row found in deposit bonus code
            //checking number of times used here
            if ($dp_row->applicable_type == "Club Wise") {
                $dp_row->club_wise_config = json_decode($dp_row->club_wise_config);
                foreach ($dp_row->club_wise_config as $c_item) {
                    if ($c_item->id == $player_information->expertise_level_id) {
                        $dp_row->points_percentage = $c_item->points_percentage;
                        $dp_row->max_points = $c_item->max_points;
                        break;
                    }
                }
            }

            if ($dp_row->credit_to == "Bonus Wallet") {
                $this->db->where("wallet_account_id", $player_information->wallet_account_id);
                $this->db->where("bonus_code", $bonus_code);
                $usage_times = $this->db->get("bonus_transaction_history")->num_rows();
            } else if ($dp_row->credit_to == "Deposit Wallet") {
                $this->db->where("players_id", $players_id);
                $this->db->where("bonus_code", $bonus_code);
                $usage_times = $this->db->get("deposit_money_transactions")->num_rows();
            }

            if ($usage_times < $dp_row->usage_times) {
                $creditable_bonus_amount = (int) ($credits_count * ($dp_row->points_percentage / 100));
                if ($creditable_bonus_amount < $dp_row->max_points) {
                    return $creditable_bonus_amount;
                } else {
                    return (int) $dp_row->max_points;
                }
            }else {
                return "MAX_USED";
            }
        }else{
            return false;
        }
    }
    
    function get_occassional_bonus_row_by_bonus_code($bonus_code){
        $this->db->where("lower(coupon_code)", strtolower($bonus_code));
        $this->db->where("('".CREATED_DATE_TIME."' >= from_date_time  AND '".CREATED_DATE_TIME."' <= to_date_time )");
        $this->db->where("bonus_type", "Occasional Bonus");
        $this->db->where("status", "1");
        $oc_row = $this->db->get("bonus_coupons")->row();
        return $oc_row;
    }
    
    function get_deposit_bonus_row_by_bonus_code($bonus_code){
        $this->db->where("lower(coupon_code)", strtolower($bonus_code));   
        $this->db->where("('".CREATED_DATE_TIME."' >= from_date_time  AND '".CREATED_DATE_TIME."' <= to_date_time )");
        $this->db->where("bonus_type", "Deposit Bonus");
        $this->db->where("status", "1");
        $dp_row = $this->db->get("bonus_coupons")->row();
        return $dp_row;
    }

    function get_minimum_deposit_amount_to_avail_bonus_code($bonus_code) {
        $oc_row = $this->get_occassional_bonus_row_by_bonus_code($bonus_code);
        $dp_row = $this->get_deposit_bonus_row_by_bonus_code($bonus_code);
        if (!$oc_row && !$dp_row) {
            return "INVALID_BONUS_CODE";
        } else if ($oc_row) {
            return $oc_row->minimum_deposit;
        } else if ($dp_row) {
            return $dp_row->minimum_deposit;
        }
    }

    function get_bonus_coupon_credit_to_type($bonus_code) {
        if (WELCOME_BONUS_CODE == $bonus_code) {
            return "Bonus Wallet";
        }
        $this->db->select("credit_to");
        $this->db->where("lower(coupon_code)", strtolower($bonus_code));
        $this->db->where("status", "1");
        $dp_row = $this->db->get("bonus_coupons")->row();
        return $dp_row->credit_to;
    }

    function check_is_email_verified_bonus_applied($wallet_account_id) {
        $this->db->where("wallet_account_id", $wallet_account_id);
        $this->db->where("type", "Email Verified");
        if ($this->db->get($this->table_name)->num_rows() == 0) {
            return false;
        } else {
            return true;
        }
    }

    function check_daily_bonus_added_or_not($wallet_account_id) {
        $this->db->where("wallet_account_id", $wallet_account_id);
        $this->db->where("type", "Daily Bonus");
        $this->db->where("created_date_time", CREATED_DATE_TIME);
        $this->db->where("expiry_date", date("Y-m-d"));
        if ($this->db->get($this->table_name)->num_rows() == 0) {
            return false;
        } else {
            return true;
        }
    }

}