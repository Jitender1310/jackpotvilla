<?php

    class Club_types_model extends CI_Model {

        function get_club_types() {
            $this->db->order_by("priority", "asc");
            $this->db->where("status", 1);
            $data = $this->db->get("club_types")->result();
            return $data;
        }

        function get_club_details($id) {
            $this->db->where("status", 1);
            $this->db->where("id", $id);
            $data = $this->db->get("club_types")->row();
            return $data;
        }

        function get_current_club_info($players_id) {
            $player_details = $this->player_profile_model->get_profile_information($players_id);

            $w_row = $this->wallet_model->get_wallet_details_by_players_id($players_id);

            $this->db->where("id >= ", $player_details->expertise_level_id);
            $this->db->limit(2);
            $this->db->order_by("priority", "asc");
            $data = $this->db->get("club_types")->result();

            $arr = (object) [
                        "min_points" => $data[0]->points_to_achieve + 1,
                        "max_points" => $data[1]->points_to_achieve,
                        "current_points" => $w_row->total_rps_points,
                        "current_level_expiry_date" => $w_row->rps_level_expiry_date,
                        "current_level_name" => $data[0]->name,
                        "next_level_name" => $data[1]->name,
                        "current_level_image" => $this->player_profile_model->get_expertize_level_image($data[0]->name),
                        "next_level_image" => $this->player_profile_model->get_expertize_level_image($data[1]->name),
                        "current_club_level_id" => $player_details->expertise_level_id,
                        "display_message" => "You need " . ($data[1]->points_to_achieve - $w_row->total_rps_points) . " for the next level"
            ];
            return $arr;
        }

}
