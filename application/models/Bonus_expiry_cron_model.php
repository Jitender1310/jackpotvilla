<?php

class Bonus_expiry_cron_model extends CI_Model {

    function check_and_update_bonus_amounts() {
        
        $this->db->where("is_expired",0);
        $this->db->where("transaction_type", "Credit");
        $this->db->where("expiry_date  < ", date("Y-m-d"));
        $result = $this->db->get("bonus_transaction_history")->result();
        foreach($result as $item){
            $this->db->trans_begin();
            
            $w_row = $this->wallet_model->get_wallet($item->wallet_account_id);
            $total_bonus = $w_row->total_bonus;
            $updated_bonus = $total_bonus - ($item->amount - $item->utilized_amount);
            
            //update bonus wallet
            $this->db->where("id", $item->wallet_account_id);
            $this->db->set("total_bonus", $updated_bonus);
            $this->db->update("wallet_account");
            
            //mark as expired bonus transaction row
            $this->db->set("is_expired", 1);
            $this->db->where("id", $item->id);
            $this->db->update("bonus_transaction_history");
            
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
        }
    }

}
