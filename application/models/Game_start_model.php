<?php

class Game_start_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function start_game($player_access_tokens, $rooms_ref_id, $games_token, $game_ref_id, $bid_amount, $is_rejoined) {

        $this->db->select("id, game_type, game_title, game_sub_type, point_value");
        $this->db->where("token", $games_token);
        $game_row = $this->db->get("games")->row();
        if (!$game_row) {
            return "INVALID_GAME";
        }

        $games_id = $game_row->id;
        $point_value = $game_row->point_value;
        $game_type = $game_row->game_type;
        $game_title = $game_row->game_title;
        $game_sub_type = $game_row->game_sub_type;

        $player_access_tokens_arr = explode(",", $player_access_tokens);
        //mapping player ids
        for ($i = 0; $i < count($player_access_tokens_arr); $i++) {
            //$p_row = explode("___", $player_access_tokens[$i]);
            $this->db->select("id, access_token, wallet_account_id");
            $this->db->where("access_token", $player_access_tokens_arr[$i]);
            $prow = $this->db->get("players")->row();
            $players_data[] = (object) [
                        "players_id" => $prow->id,
                        "access_token" => $player_access_tokens_arr[$i],
                        "wallet_account_id" => $prow->wallet_account_id
            ];
        }

//        log_message("error", "Game Type ". $game_type);
//        log_message("error", print_r($players_data, true));

        $number_of_players = count($players_data);

        //check room ref id 
        $this->db->select("id");
        $this->db->where("room_ref_id", $rooms_ref_id);
        $room_row = $this->db->get("rooms")->row();
        if ($room_row) {
            $rooms_id = $room_row->id;
        } else {
            $this->db->set("room_ref_id", $rooms_ref_id);
            $this->db->set("created_date_time", CREATED_DATE_TIME);
            $this->db->insert("rooms");
            $rooms_id = $this->db->insert_id();
        }

        /* $joined_rows = $this->game_join_model->get_joined_players_list($games_id, $rooms_id, $players_ids);
          if(count($joined_rows)<2){
          return "INSUFFICIENT_PLAYERS";
          } */


        if ($game_type == "Practice") {
            //loop the palyers
            $this->db->trans_begin();
            foreach ($players_data as $item) {
                $w_row = $this->wallet_model->get_wallet_details_by_players_id($item->players_id);
                $update_in_play_chips = $w_row->in_play_fun_chips + $bid_amount;
                $this->wallet_model->update_in_play_fun_chips($update_in_play_chips, $item->wallet_account_id);

                $updated_fun_chips = $w_row->fun_chips - $bid_amount;
                $this->wallet_model->set_updated_fun_chips($updated_fun_chips, $item->wallet_account_id);
            }
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                $players_wallets = [];
                $i = 0;
                foreach ($players_data as $item) {
                    $players_wallets[$i] = $this->wallet_model->get_wallet_details_by_players_id($item->players_id);
                    $players_wallets[$i]->access_token = $item->access_token;
                    $i++;
                }
                return $players_wallets;
            }
        }


        $this->db->trans_begin();
        $total_bid_amount = $bid_amount * $number_of_players;

        //Debit amount from players for as entry fee
        foreach ($players_data as $item) {

            //log_message("error", print_r(func_get_args(), true));
            $transaction_data = [
                "transaction_type" => "Debit",
                "amount" => $bid_amount,
                "wallet_account_id" => $item->wallet_account_id,
                "remark" => $game_ref_id . " - " . ($is_rejoined == 1 ? "Rejoin" : "Entry"),
                "type" => "Game " . ($is_rejoined == 1 ? "Rejoin" : "Entry"),
                "game_title" => $game_title,
                "game_sub_type" => $game_sub_type,
                "games_id" => $games_id
            ];
            //log_message("error", print_r($transaction_data, true));

            $this->wallet_transactions_model->create_transaction($transaction_data);

            $w_row = $this->wallet_model->get_wallet($item->wallet_account_id);

//            $update_in_play_real_chips = $w_row->in_play_real_chips + $bid_amount;
//            $this->wallet_model->update_in_play_real_chips($update_in_play_real_chips, $item->wallet_account_id);

            if ($this->wallet_model->check_sufficient_credits_has_or_not($game_type, $bid_amount, $item->players_id) == false) {
                return "INSUFFICIENT_FUNDS";
            }
            $this->wallet_model->convert_wallet_chips_to_inplay($game_type, $bid_amount, $item->players_id);



//            $updated_fun_chips = $w_row->fun_chips - $bid_amount;
//            $this->wallet_model->set_updated_fun_chips($updated_fun_chips, $item->wallet_account_id);
        }

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();

            $players_wallets = [];
            $i = 0;
            foreach ($players_data as $item) {
                $players_wallets[$i] = $this->wallet_model->get_wallet_details_by_players_id($item->players_id);
                $players_wallets[$i]->access_token = $item->access_token;
                $i++;
            }
            return $players_wallets;
        }
    }

    function generate_transaction_id() {
        $transaction_id = date("Ymdhis") . generateRandomNumber(3);
        if ($this->db->get_where("wallet_transaction", ["transaction_id" => $transaction_id])->num_rows() == 0) {
            return $transaction_id;
        } else {
            return $this->generate_transaction_id();
        }
    }

}
