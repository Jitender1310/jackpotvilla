<?php

class Player_change_password_model extends CI_Model {

    public $table_name = "players";

    public function __construct() {
        parent::__construct();
        $this->table_name = "players";
    }

    function update($data, $users_id) {
        $this->db->trans_begin();

        $password = $data["password"];
        $salt = generateRandomString(10);
        $enc_password = md5($password . $salt);

        $this->db->set("password", $enc_password);
        $this->db->set("salt", $salt);
        $this->db->where("id", $users_id);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        $this->player_reset_password_model->update_password_message($users_id);
//        if ($response) {
//            $this->log_model->create_log($this->table_name, __FUNCTION__, $agent_id);
//        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function check_for_current_password_is_correct($current_password, $player_id) {
        $password_row = $this->get_current_password($player_id);
        if ($password_row->password == md5($current_password . $password_row->salt)) {
            return true;
        }
        return false;
    }

    function get_current_password($player_id) {
        $this->db->select("id, password, salt");
        $row = $this->db->get_where($this->table_name, ['id' => $player_id])->row();
        return $row;
    }

}
