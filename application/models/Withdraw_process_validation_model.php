<?php

class Withdraw_process_validation_model extends CI_Model {

    function validate_and_process($withdraw_amount, $transfer_type, $transfer_to, $players_id, $posted_array) {

        $player_details = $this->player_profile_model->get_profile_information($players_id);
        //$club_details = $this->club_types_model->get_club_details($player_details->expertise_level_id);

        if ($this->get_number_of_withdraw_requests($players_id) > 1) {
            $arr = [
                "status" => "invalid",
                "debug_points" => "NUMBER_OF_TODAY_REQUESTS",
                "code" => "MAX_NUMBER_OF_WITHDRAW_REQUESTS",
                "title" => "Sorry, withdrawl cannot be processed",
                "message" => "You cannot place more than one withdrawal request per day"
            ];
            return $arr;
        }

        $error_str = "";
        if ($player_details->email_verified == 0) {
//            $arr = [
//                "status" => "invalid",
//                "debug_points" => "EMAIL_VERIFICATION_REQUIRED",
//                "code" => "EMAIL_VERIFICATION_REQUIRED",
//                "title" => "Sorry, Email verification required",
//                "message" => "Please verify you email to place withdrawl request"
//            ];
//            return $arr;
            $error_str .=" email";
        }

        if ($player_details->mobile_verified == 0) {
//            $arr = [
//                "status" => "invalid",
//                "debug_points" => "MOBILE_VERIFICATION_REQUIRED",
//                "code" => "MOBILE_VERIFICATION_REQUIRED",
//                "title" => "Sorry, Mobile verification required",
//                "message" => "Please verify you mobile number to place withdrawl request"
//            ];
//            return $arr 
            $error_str .=($error_str !="" ? ",":""). " mobile number";
        }


        $w_config = $this->db->get("withdraw_system_settings")->row();

        $tds_amount = $this->get_tds_amount($withdraw_amount, $w_config);

        //check number of withdrawal requests today
        //check kyc verified or not
        if ($player_details->kyc_status != "Verified") {
//            $arr = [
//                "status" => "invalid",
//                "debug_points" => "KYC_VERIFICATION_PENDING",
//                "code" => "KYC_VERIFICATION_PENDING",
//                "title" => "Sorry, Kyc verification required",
//                "message" => "Please verify you kyc details from My Account > Account Details to place withdrawl request"
//            ];
//            return $arr;
            $error_str .=($error_str !="" ? ",":"")." kyc details from My Account > Account Details";
        }
        if($error_str!=""){
            $arr = [
                "status" => "invalid",
                "debug_points" => "KYC_VERIFICATION_PENDING",
                "code" => "KYC_VERIFICATION_PENDING",
                "title" => "Sorry, Some verification required",
                "message" => "Please verify you ".$error_str." to place withdrawl request."
            ];
            return $arr;
        }


        if ($transfer_type == "IMPS") {
            if ($transfer_to == "PAYTM") {

                if ($w_config->paytm_withdraw_min_amount <= $withdraw_amount &&
                        $withdraw_amount <= $w_config->paytm_withdraw_max_amount) {


                    if ($withdraw_amount <= $w_config->paytm_withdrawal_service_charge_for_less_than_amount) {
                        $service_charge_percentage = $w_config->paytm_withdrawal_service_charge_on_less_than;
                    }

                    if ($withdraw_amount >= $w_config->paytm_withdrawal_service_charge_for_greater_than_amount) {
                        $service_charge_percentage = $w_config->paytm_withdrawal_service_charge_on_greater_than;
                    }

                    $service_charge_amount = round($withdraw_amount * ($service_charge_percentage / 100), 2);

                    $arr = [
                        "status" => "valid",
                        "code" => "VALID_TRANSACTION",
                        "details" => [
                            "request_amount" => $withdraw_amount,
                            "transfer_type" => $transfer_type,
                            "transfer_to" => $transfer_to,
                            "service_charge_percentage" => $service_charge_percentage,
                            "service_charge" => $service_charge_amount,
                            "paytm_mobile_number" => $posted_array["paytm_mobile_number"],
                            "paytm_person_name" => $posted_array["paytm_person_name"],
                            "account_number" => $posted_array["account_number"],
                            "account_holder_name" => $posted_array["account_holder_name"],
                            "ifsc_code" => $posted_array["ifsc_code"],
                            "bank_name" => $posted_array["bank_name"],
                            "branch_name" => $posted_array["branch_name"],
                            "tds_percentage" => $w_config->tds_percentage,
                            "tds_charge" => $tds_amount,
                            "net_receivable" => $withdraw_amount - $tds_amount - $service_charge_amount
                        ]
                    ];

                    return $arr;
                } else {
                    $arr = [
                        "status" => "invalid",
                        "debug_points" => "IMPS_PAYTM",
                        "code" => "INVALID_AMOUNT",
                        "title" => "Sorry, withdrawl cannot be processed",
                        "message" => "You can able to withdraw Min: Rs." . $w_config->paytm_withdraw_min_amount . " to Max: Rs." . $w_config->paytm_withdraw_max_amount
                    ];
                    return $arr;
                }
            } else if ($transfer_to == "BANK TRANSFER") {
                if ($w_config->instant_withdraw_min_amount <= $withdraw_amount &&
                        $withdraw_amount <= $w_config->instant_withdraw_max_amount) {
                    if ($withdraw_amount <= $w_config->instant_withdrawal_service_charge_for_less_than_amount) {
                        $service_charge_percentage = $w_config->instant_withdrawal_service_charge_on_less_than;
                    }

                    if ($withdraw_amount >= $w_config->instant_withdrawal_service_charge_for_greater_than_amount) {
                        $service_charge_percentage = $w_config->instant_withdrawal_service_charge_on_greater_than;
                    }

                    $service_charge_amount = round($withdraw_amount * ($service_charge_percentage / 100), 2);

                    $arr = [
                        "status" => "valid",
                        "code" => "VALID_TRANSACTION",
                        "details" => [
                            "request_amount" => $withdraw_amount,
                            "transfer_type" => $transfer_type,
                            "transfer_to" => $transfer_to,
                            "service_charge_percentage" => $service_charge_percentage,
                            "service_charge" => $service_charge_amount,
                            "paytm_mobile_number" => $posted_array["paytm_mobile_number"],
                            "paytm_person_name" => $posted_array["paytm_person_name"],
                            "account_number" => $posted_array["account_number"],
                            "account_holder_name" => $posted_array["account_holder_name"],
                            "ifsc_code" => $posted_array["ifsc_code"],
                            "bank_name" => $posted_array["bank_name"],
                            "branch_name" => $posted_array["branch_name"],
                            "tds_percentage" => $w_config->tds_percentage,
                            "tds_charge" => $tds_amount,
                            "net_receivable" => $withdraw_amount - $tds_amount - $service_charge_amount
                        ]
                    ];
                    return $arr;
                } else {
                    $arr = [
                        "status" => "invalid",
                        "debug_points" => "IMPS_BANK_TRANSFER",
                        "code" => "INVALID_AMOUNT",
                        "title" => "Sorry, withdrawl cannot be processed",
                        "message" => "You can able to withdraw Min: Rs." . $w_config->instant_withdraw_min_amount . " to Max: Rs." . $w_config->instant_withdraw_max_amount
                    ];
                    return $arr;
                }
            }
        } else if ($transfer_type == "NEFT") {
            if ($w_config->neft_withdraw_min_amount <= $withdraw_amount &&
                    $withdraw_amount <= $w_config->neft_withdraw_max_amount) {
                //Get expertize level this month number of withdrawals
                $completed_number_of_transactions = $this->get_current_month_number_of_withdrawls($players_id, $player_details->expertise_level_id);
                $service_charge_percentage = $this->get_service_charge_as_per_player_expertise_level($player_details->expertise_level_id, $completed_number_of_transactions);

                $service_charge_amount = round($withdraw_amount * ($service_charge_percentage / 100), 2);

                $arr = [
                    "status" => "valid",
                    "code" => "VALID_TRANSACTION",
                    "details" => [
                        "request_amount" => $withdraw_amount,
                        "transfer_type" => $transfer_type,
                        "transfer_to" => $transfer_to,
                        "service_charge_percentage" => $service_charge_percentage,
                        "service_charge" => $service_charge_amount,
                        "paytm_mobile_number" => $posted_array["paytm_mobile_number"],
                        "paytm_person_name" => $posted_array["paytm_person_name"],
                        "account_number" => $posted_array["account_number"],
                        "account_holder_name" => $posted_array["account_holder_name"],
                        "ifsc_code" => $posted_array["ifsc_code"],
                        "bank_name" => $posted_array["bank_name"],
                        "branch_name" => $posted_array["branch_name"],
                        "tds_percentage" => $w_config->tds_percentage,
                        "tds_charge" => $tds_amount,
                        "net_receivable" => $withdraw_amount - $tds_amount - $service_charge_amount
                    ]
                ];
                return $arr;
            } else {
                $arr = [
                    "status" => "invalid",
                    "debug_points" => "IMPS_BANK_TRANSFER",
                    "code" => "INVALID_AMOUNT",
                    "title" => "Sorry, withdrawl cannot be processed",
                    "message" => "You can able to withdraw Min: Rs." . $w_config->neft_withdraw_min_amount . " to Max: Rs." . $w_config->neft_withdraw_max_amount
                ];
                return $arr;
            }
        }
    }

    function get_current_month_number_of_withdrawls($players_id, $player_expertise_level_id) {
        $this_month_start_date = date("Y-m-01");
        $this_month_current_date = date("Y-m-d");

        $this->db->where("request_date_time >= ", $this_month_start_date);
        $this->db->where("request_date_time <= ", $this_month_current_date);
        $this->db->where("request_status != ", "Rejected");
        $this->db->where("players_id", $players_id);
        $this->db->where("transfer_type", "NEFT");
        $this->db->where("expertize_level_id_at_withdraw_time", $player_expertise_level_id);
        $number_of_withdrawals = $this->db->get("withdraw_request")->num_rows();

        return $number_of_withdrawals > 0 ? $number_of_withdrawals : 0;
    }

    function get_service_charge_as_per_player_expertise_level($player_expertise_level_id, $completed_number_of_transactions) {

        $this->db->select("number_of_free_withdrawals, withdraw_fees_in_percentage");
        $this->db->where("id", $player_expertise_level_id);
        $row = $this->db->get("club_types")->row();

        $service_charge_percentage = 0;
        if ($completed_number_of_transactions > $row->number_of_free_withdrawals) {
            $service_charge_percentage = $row->withdraw_fees_in_percentage;
        }

        return $service_charge_percentage;
    }

    function get_tds_amount($withdraw_amount, $w_config_row) {
        $tds_amount = 0;
        if ($withdraw_amount >= $w_config_row->apply_tds_on_greater_than) {
            $tds_amount = round($withdraw_amount * ($w_config_row->tds_percentage * 100), 2);
        }
        return $tds_amount;
    }

    function get_number_of_withdraw_requests($players_id) {

        $this_month_start_date = date("Y-m-d");
        $this_month_current_date = date("Y-m-d");

        $this->db->where("request_date_time >= ", $this_month_start_date);
        $this->db->where("request_date_time <= ", $this_month_current_date);
        $this->db->where("request_status != ", "Rejected");
        $this->db->where("players_id", $players_id);
        $number_of_withdrawals = $this->db->get("withdraw_request")->num_rows();

        return $number_of_withdrawals;
    }

    function cancel_withdraw_request($withdraw_id, $players_id) {
        $this->db->where("withdrawal_id", $withdraw_id);
        $this->db->where("players_id", $players_id);
        $withdraw_row = $this->db->get("withdraw_request")->row();

        if (!$withdraw_row) {
            return $arr = [
                "status" => "invalid",
                "debug_points" => "REQUEST_NOT_FOUND",
                "code" => "REQUEST_NOT_FOUND",
                "title" => "Sorry, request id not found",
                "message" => "This withdraw record not found",
            ];
        }

        if ($withdraw_row->request_status == "Cancelled") {
            return $arr = [
                "status" => "invalid",
                "debug_points" => "REQUEST_ALREADY_CANCELLED",
                "code" => "REQUEST_ALREADY_CANCELLED",
                "title" => "Oops",
                "message" => "Sorry, This request is already cancelled",
            ];
        } else if ($withdraw_row->request_status == "Pending") {

            if (isset($withdraw_row->pg_ref_id) && isset($withdraw_row->transfer_utr_ref_id)) {
                return $arr = [
                    "status" => "invalid",
                    "debug_points" => "REQUEST_IN_PROCESS",
                    "code" => "REQUEST_IN_PROCESS",
                    "title" => "Oops",
                    "message" => "Sorry, This request is cannot be cancelled. The status is already in process",
                ];
            } else {
                $this->db->set("response_date_time", date("Y-m-d H:i:s"));
                $this->db->set("request_status", "Cancelled");
                $this->db->set("request_processed_users_id", "");
                $this->db->where("withdrawal_id", $withdraw_row->withdrawal_id);
                $response = $this->db->update("withdraw_request");

                if ($response) {
                    $wallet_account_id = $this->db->select("wallet_account_id")->where("id", $withdraw_row->players_id)->get("players")->row()->wallet_account_id;
                    $transaction_data = [
                        "transaction_type" => "Credit",
                        "amount" => $withdraw_row->request_amount,
                        "wallet_account_id" => $wallet_account_id,
                        "remark" => "Withdraw Request #" . $withdraw_row->withdrawal_id . " Cancelled by yourself manullay",
                        "type" => "Withdraw",
                    ];
                    $this->wallet_transactions_model->create_transaction($transaction_data);

                    $this->db->where("id", $wallet_account_id);
                    $wa_row = $this->db->get("wallet_account")->row();

                    $wa_row->real_chips_withdrawal = ($wa_row->real_chips_withdrawal + $withdraw_row->request_amount);
                    $wa_row->on_hold_real_chips_withdrawal = $wa_row->on_hold_real_chips_withdrawal - $withdraw_row->request_amount;

                    $this->db->set("real_chips_withdrawal", $wa_row->real_chips_withdrawal);
                    $this->db->set("on_hold_real_chips_withdrawal", $wa_row->on_hold_real_chips_withdrawal);
                    $this->db->where("id", $wallet_account_id);
                    $this->db->update("wallet_account");


                    $players_id = $withdraw_row->players_id;
                    $player_details = $this->player_login_model->get_player_details($players_id);

                    //send sms/email
                    $msg = "Dear " . $player_details->username . ", Your withdrawl request #" . $withdraw_row->withdrawal_id . " has been cancelled by your self";
                    send_message($player_details->mobile, $msg);
                }
                return $arr = [
                    "status" => "valid",
                    "debug_points" => "REQUEST_CANCELLED",
                    "code" => "REQUEST_CANCELLED",
                    "title" => "Success",
                    "message" => "Your withdraw request has been cancelled succesfully, and wallet balance adjusted",
                ];
            }
        } else if ($withdraw_row->request_status == "Rejected") {
            return $arr = [
                "status" => "invalid",
                "debug_points" => "REQUEST_ALREADY_REJECTED",
                "code" => "REQUEST_ALREADY_REJECTED",
                "title" => "Oops",
                "message" => "Your trying this request is already in rejected mode, Reason : " . $withdraw_row->remark
            ];
        } else if ($withdraw_row->request_status == "Completed") {
            return $arr = [
                "status" => "invalid",
                "debug_points" => "REQUEST_ALREADY_COMPLETED",
                "code" => "REQUEST_ALREADY_COMPLETED",
                "title" => "Oops",
                "message" => "Your trying this request is already completed @ " . convert_date_time_to_display_format($withdraw_row->response_date_time)
            ];
        } else if ($withdraw_row->request_status == "Processing") {
            return $arr = [
                "status" => "invalid",
                "debug_points" => "REQUEST_ALREADY_IN_PROCESS",
                "code" => "REQUEST_ALREADY_IN_PROCESS",
                "title" => "Oops",
                "message" => "Your withdrawl request #" . $withdraw_row->withdrawal_id . " already in process, request cannot be allowed to cancel"
            ];
        } else {
            return $arr = [
                "status" => "invalid",
                "debug_points" => "REQUESST_ERROR",
                "code" => "REQUESST_ERROR",
                "title" => "Oops",
                "message" => "Your cannot be cancelled, Please contact our support to know more details"
            ];
        }
    }

}
