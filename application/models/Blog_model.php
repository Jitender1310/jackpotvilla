<?php

class Blog_model extends CI_Model {

    private $table_name = "blog_pages";

    function blog_categories() {
        $this->db->order_by("category_name", "asc");
        $this->db->where("status", 1);
        $categories = $this->db->get("blog_categories")->result();
        foreach ($categories as $item) {
            $this->db->select("count(id) as pages_cont");
            $this->db->where(" (FIND_IN_SET(" . $item->id . ",blog_pages.blog_categories_ids) )");
            $item->pages_count = $this->db->get("blog_pages")->row()->pages_cont;
        }
        return $categories;
    }

    function get_page($id) {
        $this->db->where("id", $id);
        $this->db->where("status", 1);
        $data = $this->db->get($this->table_name)->row();
        return $data;
    }

    function get_pages($filters = []) {
        $only_cnt = false;
        if (!isset($filters["start"]) && !isset($filters["limit"])) {
            $only_cnt = true;
        }

        if (isset($filters["category_id"])) {
            $category_id = $filters["category_id"];
            $this->db->where(" (FIND_IN_SET($category_id,blog_pages.blog_categories_ids) )");
        }

        $this->db->select($this->table_name . '.*');
        $this->db->from($this->table_name);

        if ($only_cnt) {
            $cnt = $this->db->get()->num_rows();
            //echo $this->db->last_query(); die;
            return $cnt;
        }
        $this->db->limit($filters["limit"], $filters["start"]);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();

        if ($data->num_rows() == 0) {
            return [];
        }
        $data = $data->result();

        foreach ($data as $item) {
            if (isset($item->thumbnail_image)) {
                $item->thumbnail_image = BLOG_IMAGES_UPLOAD_FOLDER_PATH . $item->thumbnail_image;
            } else {
                $item->thumbnail_image = "";
            }
            if (isset($item->banner_image)) {
                $item->banner_image = BLOG_IMAGES_UPLOAD_FOLDER_PATH . $item->banner_image;
            } else {
                $item->banner_image = "";
            }

            $item->created_date_time = date(DATE_TIME_FORMAT, $item->created_at);
        }
        return $data;
    }

}
