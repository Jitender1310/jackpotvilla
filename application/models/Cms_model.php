<?php

class Cms_model extends CI_Model {

    private $table_name = "cms_pages";

    function get_page($id) {
        $this->db->where("id", $id);
        $this->db->where("status", 1);
        $data = $this->db->get($this->table_name)->row();
        return $data;
    }
    
    function get_sliders(){
        $data = $this->db->where("display_in", "Desktop Website")->get("homepage_sliders")->result();
        foreach($data as $item){
            $item->image = BACK_END_SERVER_URL_FILE_UPLOAD_FOLDER_IMG_PATH.$item->image;
        }
        return $data;
    }

}
