<?php

class Offer_notifications_model extends CI_Model{
    
    function get_available_notifications(){
        $this->db->select("id, title, image, web_link");
        $this->db->order_by("title", "asc");
        $this->db->where("status", 1);
        $data = $this->db->get("offer_notifications")->result();
        foreach($data as $item){
            $item->image = BACK_END_SERVER_URL_FILE_UPLOAD_FOLDER_IMG_PATH.$item->image;
            $item->web_link = base_url("mobile_app/offer_notifications/index/".$item->id);
        }
        return $data;
    }
    
    function set_avatar_to_player($avatar_id, $players_id){
         $this->db->select("image");
         $this->db->where("id", $avatar_id);
         $image = $this->db->get("default_avatars")->row()->image;
         
         $this->db->where("id", $players_id);
         $this->db->set("profile_pic", $image);
         $this->db->update("players");
         return true;
    }
}