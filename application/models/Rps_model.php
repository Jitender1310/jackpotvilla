<?php

class Rps_model extends CI_Model {

    function add_rps_points_game_played($spend_amount, $players_id, $game_ref_id, $wallet_alccount_id) {
        $rps_points = floor($spend_amount / RPS_POINT_PER_SPEND_CASH);
        $player_club_info = $this->club_types_model->get_current_club_info($players_id);
        $previous_rps_points = $player_club_info->current_points;
        $updated_rps_points = $previous_rps_points + $rps_points;

        $club_types = $this->club_types_model->get_club_types();
        //if level is upgraded then expiry date will change
        $expiry_date = null;
        $updated_expertize_level_id = null;

        if ($updated_rps_points > $player_club_info->max_points) {
            foreach ($club_types as $item) {
                if ($item->name == $player_club_info->next_level_name) {
                    $expiry_date = date("Y-m-d", strtotime("+" . $item->validity_in_days . " days"));
                    $updated_expertize_level_id = $item->id;
                    break;
                }
            }
        }

        $transaction_data = [
            "transaction_type" => "Credit",
            "amount" => $rps_points,
            "wallet_account_id" => $wallet_alccount_id,
            "remark" => $game_ref_id . " - Played",
            "type" => "Cash Game Played"
        ];
        //log_message("error", print_r($transaction_data, true));

        $this->rps_transactions_model->create_transaction($transaction_data);

        $this->wallet_model->set_updated_rps_points($updated_rps_points, $wallet_alccount_id, $players_id, $expiry_date, $updated_expertize_level_id);
        return true;
    }

    function get_my_rps_points($players_id) {
        $player_club_info = $this->club_types_model->get_current_club_info($players_id);
        return $player_club_info->current_points;
    }

    function has_eligible_rps_points_to_reedem($redeem_points, $players_id) {
        $current_points = $this->get_my_rps_points($players_id);
        if ($redeem_points <= $current_points) {
            return true;
        } else {
            return false;
        }
    }

    function redeem_points($redeem_points,$players_id) {
        
        if (!$this->has_eligible_rps_points_to_reedem($redeem_points,$players_id)) {
            return "INSUFFICIENT_FUNDS";
        } else {

            //check is valid readable points
            $found = FALSE;
            $allowed_rps_values = $this->get_rps_images();
            for ($i = 0; $i < count($allowed_rps_values); $i++) {
                if ($allowed_rps_values[$i]->value == $redeem_points) {
                    $found = true;
                    break;
                }
            }
            
            if ($found == FALSE) {
                return "INVALID_RPS_REEDEM_VALUE";
            }
            
            $this->db->trans_begin();
            $current_points = $this->get_my_rps_points($players_id);
            $wallet_alccount_id = $this->player_login_model->get_player_details($players_id)->wallet_account_id;
            $converted_to_cash = $redeem_points / 10;

            $transaction_data = [
                "transaction_type" => "Debit",
                "amount" => $redeem_points,
                "wallet_account_id" => $wallet_alccount_id,
                "remark" => $redeem_points . " Rps Redemed and credited to deposit wallet Rs." . $converted_to_cash . "/-",
                "type" => "Rps Redemed"
            ];
            
            $this->rps_transactions_model->create_transaction($transaction_data);
            $created_txn_ref_number = $this->rps_transactions_model->get_txn_ref_number();
            
            $updated_rps_points = $current_points - $redeem_points;

            $club_types_asc = $this->club_types_model->get_club_types();

            $club_types = array_reverse($club_types_asc);
            $expiry_date = null;
            $updated_expertize_level_id = null;

            //Possible downgrading player expertize level
            foreach ($club_types as $item) {
                if ($item->from_points <= $updated_rps_points && $updated_rps_points <= $item->to_points) {
                    //echo $item->from_points ."<=". $updated_rps_points ."&&". $updated_rps_points ."<=". $item->to_points;
                    $expiry_date = date("Y-m-d", strtotime("+" . $item->validity_in_days . " days"));
                    $updated_expertize_level_id = $item->id;
                    break;
                }
            }
//            echo $expiry_date;
//            echo "\n";
//            echo $updated_expertize_level_id;
//            echo "\n";
//            print_r($converted_to_cash);
//            die;

            $this->wallet_model->set_updated_rps_points($updated_rps_points, $wallet_alccount_id, $players_id, $expiry_date, $updated_expertize_level_id);


            $convert_cash_transaction_data = [
                "transaction_type" => "Credit",
                "amount" => $converted_to_cash,
                "wallet_account_id" => $wallet_alccount_id,
                "remark" => "Rps Redeemed, " . $redeem_points . " converted to cash Rps Ref id #".$created_txn_ref_number,
                "type" => "Rps Redeemed",
            ];
            $this->wallet_transactions_model->create_transaction($convert_cash_transaction_data);

            $w_row = $this->wallet_model->get_wallet($wallet_alccount_id);
            $updated_real_deposit_chips = $w_row->real_chips_deposit + $converted_to_cash;
            $this->wallet_model->set_updated_real_chips_deposit($updated_real_deposit_chips, $wallet_alccount_id);

            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        }
    }

    function get_rps_images() {
        $arr = [
            ["key" => "10", "value" => 100, "display_text" => "100"],
            ["key" => "25", "value" => 250, "display_text" => "250"],
            ["key" => "50", "value" => 500, "display_text" => "500"],
            ["key" => "100", "value" => 1000, "display_text" => "1,000"],
            ["key" => "250", "value" => 2500, "display_text" => "2,500"],
            ["key" => "500", "value" => 5000, "display_text" => "5,000"],
            ["key" => "1k", "value" => 10000, "display_text" => "10,000"],
            ["key" => "5k", "value" => 50000, "display_text" => "50,000"],
            ["key" => "10k", "value" => 100000, "display_text" => "100,000"],
            ["key" => "50k", "value" => 500000, "display_text" => "500,000"],
            ["key" => "1l", "value" => 1000000, "display_text" => "1,000,000"]
        ];
        $images = [];
        for ($i = 0; $i < count($arr); $i++) {
            $images[] = (object) [
                        "key" => STATIC_IMAGES_PATH . "rps/" . $arr[$i]["key"] . ".png",
                        "value" => $arr[$i]["value"],
                        "display_text" => $arr[$i]["display_text"] . " Rps"
            ];
        }
        return $images;
    }

}
