<?php

/**
  @  Helper functions
  @ By Manikanta
 */
defined('BASEPATH') OR exit('No direct script access allowed');

function load_instance() {
    $CI = get_instance();
    $CI->load->model('site/Site_model');
    return $CI->Site_model;
}

function is_logged_in() {
    $CI = get_instance();
    return $CI->player_login_model->check_for_user_logged();
}

function get_last_login() {
    $m = load_instance();
    return $m->get_last_login();
}

function my_header_location($uri) {
    echo "<script>location.href='" . site_url($uri) . "'</script>";
    redirect($uri);
}

function convert_date_to_db_format($datetime, $from_date_format = null) {
    if (!strpos($datetime, "-")) {
        return;
    }
    try {
        if ($from_date_format) {
            $date_obj = date_create_from_format($from_date_format, $datetime);
        } else {
            $date_obj = date_create_from_format(DATE_FORMAT, $datetime);
        }
        $db_date = date_format($date_obj, 'Y-m-d');
        return $db_date;
    } catch (Exception $ex) {
        
    }
}

function convert_date_to_display_format($datetime, $to_date_format = null) {
    if ($to_date_format) {
        $date_obj = date_create_from_format($to_date_format, $datetime);
    } else {
        $date_obj = date_create_from_format("Y-m-d", $datetime);
    }
    if (isset($date_obj) && !is_bool($date_obj)) {
        $display_date = date_format($date_obj, DATE_FORMAT);
        return $display_date;
    }
    return "";
}

function convert_date_time_to_display_format($datetime, $to_date_format = null) {
    if ($to_date_format) {
        $date_obj = date_create_from_format($to_date_format, $datetime);
    } else {
        $date_obj = date_create_from_format("Y-m-d H:i:s", $datetime);
    }
    $display_date = date_format($date_obj, DATE_TIME_HUMAN_READABLE_FORMAT);
    return $display_date;
}

//For time prefix
function ordinal($number) {
    $ends = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
    if ((($number % 100) >= 11) && (($number % 100) <= 13))
        return $number . 'th';
    else
        return $number . $ends[$number % 10];
}

function calculate_rounds($sturcture_obj, $round_number, $number_of_players) {
    $round_qualify_info = $sturcture_obj;
    //print_r($round_qualify_info);
    $number_of_tables = ceil($number_of_players / 6);
    $qualify = 1;
    $round_number++;
    foreach ($round_qualify_info as $item) {
        if ($item->round_number == $round_number) {
            $qualify = $item->qualify;
            break;
        }
    }

    $qualified_persons = $qualify * $number_of_tables;
    if ($qualified_persons == 0) {
        return [];
    }

    $response = (object) [
                "round_number" => $round_number,
                "qualified" => $qualified_persons,
                "players_text" => $number_of_players . " Players on " . $number_of_tables . " Table(s)",
                "qualifiction_text" => $qualify . " player(s) per table",
                "deals_text" => "1 x players on table"
    ];

//    print_r($response);

    return $response;
}

function get_round_titles($rounds_arr) {
    $r = 0;
    for ($i = count($rounds_arr) - 1, $l = 0; $i >= $l; $i--) {
        if ($r == 0) {
            $rounds_arr[$i]->round_title = "Final";
        } else if ($r == 1) {
            $rounds_arr[$i]->round_title = "Semi Final";
        } else if ($r == 2) {
            $rounds_arr[$i]->round_title = "Quarter  Final";
        } else {
            $rounds_arr[$i]->round_title = "";
        }
        $r++;
    }
    return array_reverse($rounds_arr);
}

function calculate_prize_positions($max_players, $entry_value, $registered_players, $actual_prizes_obj, $entry_type) {
    $expected_prize = $max_players * $entry_value;
    $prize_calculation_registered_expected_prize = $registered_players * $entry_value;

    if ($entry_type == "Cash") {
        $joined_percentage = 100 - ((($expected_prize - $prize_calculation_registered_expected_prize) / $expected_prize) * 100);
        foreach ($actual_prizes_obj as $item) {
            $item->prize_value = round($item->prize_value - ($joined_percentage / 100) * $item->prize_value, 2);
        }
    } else if ($entry_type == "Free") {
        $number_of_prizes = count($actual_prizes_obj);
        $joined_percentage = 100 - ((($expected_prize - $prize_calculation_registered_expected_prize) / $expected_prize) * 100);
        $joined_players_count = ceil(100 - ((($number_of_prizes - $registered_players) / $number_of_prizes) * 100));
        $actual_prizes_obj = array_slice($actual_prizes_obj, $joined_players_count);
        foreach ($actual_prizes_obj as $item) {
            $item->prize_value = round($item->prize_value - ($joined_percentage / 100) * $item->prize_value, 2);
        }
    }
    return $actual_prizes_obj;
}
