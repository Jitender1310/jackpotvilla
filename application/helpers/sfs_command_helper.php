<?php 
function ping_to_sfs_server($COMMAND) {
    $ch = curl_init();
    
    $options = array(
        CURLOPT_URL => $COMMAND,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HEADER => true,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_ENCODING => "",
        CURLOPT_AUTOREFERER => true,
        CURLOPT_CONNECTTIMEOUT => 120,
        CURLOPT_TIMEOUT => 120,
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_NOBODY => true,
        CURLOPT_SSL_VERIFYPEER => false
    );
    curl_setopt_array($ch, $options);
    $response = curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    
    return $code;
}