<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class KYC_Policy extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data["page_active"] = "kyc_policy";
        $this->front_view("kyc_policy");
    }

}