<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Premium_tournaments extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data["page_active"] = "premium_tournaments";
        $this->front_view( "cms/premium_tournaments");
    }

}
