<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class How_to_play extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data["page_active"] = "how_to_play";
        $this->front_view("cms/how_to_play");
    }

    function rummy_variants() {
        $this->data["page_active"] = "rummy_variants";
        $this->front_view("cms/rummy_variants");
    }
    function the_rummy_dictionary() {
        $this->data["page_active"] = "the_rummy_dictionary";
        $this->front_view("cms/the_rummy_dictionary");
    }
    function tips_and_tricks() {
        $this->data["page_active"] = "tips_and_tricks";
        $this->front_view("cms/tips_and_tricks");
    }
    function mobile_verification() {
        $this->data["page_active"] = "mobile_verification";
        $this->front_view("cms/mobile_verification");
    }
    function refund_and_cancellation_policy() {
        $this->data["page_active"] = "refund_and_cancellation_policy";
        $this->front_view("cms/refund_and_cancellation_policy");
    }
    function split() {
        $this->data["page_active"] = "split";
        $this->front_view("cms/split");
    }
    function download_app() {
        $this->data["page_active"] = "download_app";
        $this->front_view("cms/download_app");
    }
    function technical_requirements() {
        $this->data["page_active"] = "technical_requirements";
        $this->front_view("cms/technical_requirements");
    }
    function practice_rummy() {
        $this->data["page_active"] = "practice_rummy";
        $this->front_view("cms/practice_rummy");
    }
    function faq() {
        $this->data["page_active"] = "faq";
        $this->front_view("cms/faq");
    }

}
