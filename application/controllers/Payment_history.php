<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_history extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->login_required();
    }

    public function index() {
        $this->data["page_active"] = "payment_history";
        $this->front_view("payment_history");
    }

}
