<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Deposit extends MY_Controller {

        function __construct() {
            parent::__construct();
            $this->login_required();
        }

        public function index() {
            $this->data["page_active"] = "deposit";

            $this->data['payment_options'] = [
                ["key" => "Paytm", "image" => "paytm.png"],
                ["key" => "Credit/Debit Card", "image" => "creditdebiticon.png"],
                ["key" => "UPI", "image" => "upi.png"],
                ["key" => "Net Banking", "image" => "ntebanking.png"]
                    //["key" => "Lazy Pay", "image" => "lazyppay.png"],
                    //["key" => "Google Pay", "image" => "google_pay.png"],
                    //["key" => "Debit Card", "image" => "debit_card.png"],
                    //["key" => "Wallet", "image" => "wallet.png"],
            ];
            
        $this->load->view("mobile_app/deposit", $this->data);
        }

    }
    