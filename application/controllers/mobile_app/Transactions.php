<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Transactions extends MY_Controller {

        function __construct() {
            parent::__construct();
            $this->login_required();
        }

        public function index() {
            $this->data["page_active"] = "transactions";
            $this->load->view("mobile_app/transactions");
        }

    }
    