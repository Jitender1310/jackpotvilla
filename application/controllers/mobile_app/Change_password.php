<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Change_password extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->login_required();
    }

    public function index() {
        $this->data["page_active"] = "change_password";
        $this->load->view("mobile_app/change_password", $this->data);
    }

}
