<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class offer_notifications extends MY_Controller {
        private $table_name = "offer_notifications";

        function __construct() {
            parent::__construct();
            //$this->login_required();
        }

        public function index($id) {
            $this->data["page_active"] = "offer_notifications";
            $this->data["offer_notifications"] = $this->db->where("id", $id)->get($this->table_name)->row();
            $this->load->view("mobile_app/offer_notifications");
        }

    }
    