<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class How_to_play extends MY_Controller {

        function __construct() {
            parent::__construct();
            $this->login_required();
        }

        public function index() {
            $this->data["page_active"] = "how_to_play";
            $this->load->view("mobile_app/how_to_play");
        }

    }
    