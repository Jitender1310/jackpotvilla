<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Account_overview extends MY_Controller {

        function __construct() {
            parent::__construct();
            $this->login_required();
        }

        public function index() {
            $this->data["page_active"] = "account_overview";
            $this->load->view("mobile_app/account-overview");
        }

    }
    