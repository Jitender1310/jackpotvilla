<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Bring_a_friend extends MY_Controller {

        function __construct() {
            parent::__construct();
            $this->login_required();
        }

        public function index() {
            $this->load->model('player_profile_model');

            $referral_code = $this->player_profile_model->get_referral_code(
                    $this->session->userdata('p_access_token')
            );
            
            $data['referral_link'] = base_url() . '?referral_code=' . $referral_code;
            $this->data["page_active"] = "bring_a_friend";
            $this->load->view("mobile_app/bring-a-friend",$data);
        }

    }
    