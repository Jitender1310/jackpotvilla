<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tips_and_tricks extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->front_view("tips_and_tricks");
    }

}
