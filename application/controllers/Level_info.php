<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Level_info extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->login_required();
    }

    public function index() {
        $this->data["page_active"] = "level_info";
        $this->front_view("level_info");
    }

}
