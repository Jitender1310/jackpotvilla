<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tournaments extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data["page_active"] = "tournaments";
        $this->front_view("cms/tournaments");
    }

}
