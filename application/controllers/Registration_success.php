<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Registration_success extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->login_required();
    }

    public function index() {
//        if ($this->data["player_details"]->welcome_message_seen) {
//            redirect("account");
//        }
        $this->player_login_model->mark_as_welcome_message_seen($this->data["player_details"]->access_token);
        $this->front_view("registration_success");
    }

}
