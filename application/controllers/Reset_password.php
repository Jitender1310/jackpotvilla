<?php

class Reset_password extends MY_Controller {

    function __construct() {
        parent::__construct("Reset_password");
    }
	
    function _remap(){
        $this->index();
    }

    function index() {
	$this->data["key"]= $this->input->get_post("key");
	$this->data["username"]= $this->input->get_post("username");
	$this->data["access_token"]= $this->input->get_post("access_token");
	
        $this->db->where("forgot_password_verification_link", $this->data["key"]);
        $this->db->where("access_token", $this->data["access_token"]);
        $this->db->where("username", $this->data["username"]);
        $this->db->select("id");
        $row =  $this->db->get("players")->row();
       
        if(!$row){
            die('<meta http-equiv="refresh" content="2;url='.base_url().'" />	This link has been expired');
        }
        $this->front_view("reset_password"); 	
	
    }

}