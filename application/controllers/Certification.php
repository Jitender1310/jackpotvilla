<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Certification extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data["page_active"] = "certification";
        $this->front_view("cms/certification");
    }

}
