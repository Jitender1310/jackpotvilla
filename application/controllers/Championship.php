<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Championship extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data["page_active"] = "championship";
        $this->front_view("championship");
    }

}
