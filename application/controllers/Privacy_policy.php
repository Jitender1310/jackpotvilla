<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Privacy_policy extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data["page_active"] = "privacy_policy";
        
        $this->data["content"] = $this->cms_model->get_page(5);
        $this->front_view("cms/privacy_policy");
    }

}
