<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->front_view("sitemap");
    }

}
