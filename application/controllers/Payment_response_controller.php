<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'libraries/Cash_free.php';

class Payment_response_controller extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        //echo '<pre>';var_dump($_REQUEST);die();
        //extract($_POST); // Changes done for demo
	extract($_GET);
        if (isset($orderId)) {//if cashfee gateway
            $orderId = $orderId;
        }
        if (isset($ORDERID)) {//if paytm gateway
            $orderId = $ORDERID;
        }
        if (isset($shopping_order_id)) {//if razor pay
            $orderId = $shopping_order_id;
        }
        if (isset($txnid)) {//if pay u mony pay
            $orderId = $txnid;
        }

        $row = $this->deposit_model->get_depositing_row($orderId);
        //echo"<pre>";var_dump($row);die();


        if ($row->payment_gateway == "Paytm") {
            $this->paytm_response_handler($orderId);
        } else if ($row->payment_gateway == "Pay U Money") {
            $this->Pay_U_Money_response_handler($orderId);
        } else if ($row->payment_gateway == "Razor Pay") {
            $this->razo_response_handler($row);
        } else {
            $this->cashfree_response_handler($orderId);
        }
    }

    function cashfree_response_handler($orderId) {
        //extract($_POST); // Changes done for demo
	extract($_GET);
        $row = $this->deposit_model->get_depositing_row($orderId);
        if ($this->input->get_post("txStatus") == "SUCCESS") {
            $this->deposit_model->mark_as_transaction_successful($orderId, $referenceId, json_encode($_POST));
            if ($row->source == 'website') {
                echo "<script>alert('Payment successful');"
                . "location.href='" . base_url("account") . "'</script>";
            } else if ($row->source == 'unity_mobile_app') {
                echo "<script>alert('Payment successful');"
                . "location.href='" . base_url("mobile_app/deposit") . "'</script>";
            }
        } else if ($this->input->get_post("txStatus") == "FAILED") {
            $this->deposit_model->mark_as_transaction_faliure($orderId, $referenceId, json_encode($_POST));
            if ($row->source == 'website') {
                echo "<script>alert('Payment failure');"
                . "location.href='" . base_url("account") . "'</script>";
            } else if ($row->source == 'unity_mobile_app') {
                echo "<script>alert('Payment failure');"
                . "location.href='" . base_url("mobile_app/deposit") . "'</script>";
            }
        }
    }

    function paytm_response_handler($orderId) {
        extract($_POST);
        //        print_r($_POST);
        //        DIE;
        $row = $this->deposit_model->get_depositing_row($orderId);
        if ($this->input->get_post("STATUS") == "TXN_SUCCESS") {

            $this->deposit_model->mark_as_transaction_successful($orderId, $TXNID, json_encode($_POST));
            if ($row->source == 'website') {
                echo "<script>alert('Payment successful');"
                . "location.href='" . base_url("account") . "'</script>";
            } else if ($row->source == 'unity_mobile_app') {
                echo "<script>alert('Payment successful');"
                . "location.href='" . base_url("mobile_app/deposit") . "'</script>";
            }
        } else if ($this->input->get_post("STATUS") == "TXN_FAILURE") {
            $this->deposit_model->mark_as_transaction_faliure($orderId, $TXNID, json_encode($_POST));

            if ($row->source == 'website') {
                echo "<script>alert('Payment failure');"
                . "location.href='" . base_url("account") . "'</script>";
            } else if ($row->source == 'unity_mobile_app') {
                echo "<script>alert('Payment failure');"
                . "location.href='" . base_url("mobile_app/deposit") . "'</script>";
            }
        }
    }

    function Pay_U_Money_response_handler($orderId) {
        extract($_POST);
        //        print_r($_POST);
        //        DIE;
        $row = $this->deposit_model->get_depositing_row($orderId);
        if ($_POST['status']== "success") {

            $this->deposit_model->mark_as_transaction_successful($orderId, $TXNID, json_encode($_POST));
            if ($row->source == 'website') {
                echo "<script>alert('Payment successful');"
                . "location.href='" . base_url("account") . "'</script>";
            } else if ($row->source == 'unity_mobile_app') {
                echo "<script>alert('Payment successful');"
                . "location.href='" . base_url("mobile_app/deposit") . "'</script>";
            }
        } else if ($_POST['status'] == "failure") {
            $this->deposit_model->mark_as_transaction_faliure($orderId, $TXNID, json_encode($_POST));

            if ($row->source == 'website') {
                echo "<script>alert('Payment failure');"
                . "location.href='" . base_url("account") . "'</script>";
            } else if ($row->source == 'unity_mobile_app') {
                echo "<script>alert('Payment failure');"
                . "location.href='" . base_url("mobile_app/deposit") . "'</script>";
            }
        }
    }
    function razo_response_handler($order_data) {
        
        extract($_POST);
        $orderId = $order_data->transaction_id;
        $setData = array(
            'shopping_order_id' =>$shopping_order_id,
            'razorpay_payment_id' =>$razorpay_payment_id,
            'razorpay_order_id' =>$razorpay_order_id,
            'razorpay_signature' =>$razorpay_signature,
            'currency' =>"INR"
        );
        require_once APPPATH . 'libraries/Razor_pay_fetch.php';
        //echo"<pre>";var_dump($order_razor_data);die();

        $TXNID = $order_razor_data->id;
        $row = $this->deposit_model->get_depositing_row($orderId);
        //echo"<pre>";var_dump($order_razor_data->id);die();


        if ($order_razor_data->status == "paid") {
            $this->deposit_model->mark_as_transaction_successful($orderId, $TXNID, json_encode($_POST));
            if ($row->source == 'website') {
                echo "<script>alert('Payment successful');"
                . "location.href='" . base_url("account") . "'</script>";
            } else if ($row->source == 'unity_mobile_app') {
                echo "<script>alert('Payment successful');"
                . "location.href='" . base_url("mobile_app/deposit") . "'</script>";
            }
        } else if ($this->input->get_post("STATUS") == "TXN_FAILURE") {
            $this->deposit_model->mark_as_transaction_faliure($orderId, $TXNID, json_encode($_POST));

            if ($row->source == 'website') {
                echo "<script>alert('Payment failure');"
                . "location.href='" . base_url("account") . "'</script>";
            } else if ($row->source == 'unity_mobile_app') {
                echo "<script>alert('Payment failure');"
                . "location.href='" . base_url("mobile_app/deposit") . "'</script>";
            }
        }
    }

}
