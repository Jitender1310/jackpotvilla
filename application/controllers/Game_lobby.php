<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Game_lobby extends MY_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->is_logged_in()) {
            redirect("home?show_login_alert=true");
        }
        $this->login_required();
    }

    public function index() {
        $this->data["page_active"] = "game_lobby";
        $this->front_view("game-lobby");
    }

}
