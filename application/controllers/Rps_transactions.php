<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Rps_transactions extends MY_Controller {

        function __construct() {
            parent::__construct();
            $this->login_required();
        }

        public function index() {
            $current_page = $this->input->get('page');
            if ($current_page && !empty($current_page) && $current_page > 0) {
                
            } else {
                $current_page = 1;
            }

            $this->data['current_page'] = $current_page;
            $this->data["page_active"] = "rps_transactions";
            $this->front_view("rps_transactions");
        }

    }
    