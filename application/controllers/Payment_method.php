<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_method extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data["page_active"] = "payment_method";
        $this->front_view("payment_method");
    }

}