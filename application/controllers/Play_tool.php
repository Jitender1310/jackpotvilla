<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Play_tool extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->login_required();
    }

    public function index() {
        $this->data["page_active"] = "play_tool";
        //$this->data["token"] = $this->input->get_post("token");
        $this->data["token"] = $this->session->userdata("p_access_token");
        //$this->load->view("play_tool", $this->data);
        $this->front_view("play_tool");
    }

}
