<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Disclaimer extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data["page_active"] = "disclaimer";
        $this->front_view("cms/disclaimer");
    }

}
