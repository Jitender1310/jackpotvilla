<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Game_tracker extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->login_required();
    }

    public function index() {
        $this->data["page_active"] = "game_tracker";
        $this->front_view("game-tracker");
    }

}
