<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Withdraw_transactions extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->login_required();
    }

    public function index() {
        $this->data["page_active"] = "withdraw_transactions";
        $this->front_view("withdraw_transactions");
    }

}
