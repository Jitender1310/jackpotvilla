<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("blog_model");
        $this->data["categories"] = $this->blog_model->blog_categories();
    }

    function _remap() {
        $this->index();
    }

    public function index() {
        $filters = [];
        if ($this->uri->segment(2) == "view") {
            return $this->view();
        }
        if ($this->uri->segment(2)) {
            $filters["category_id"] = $this->uri->segment(2);
            $row = $this->db->get_where("blog_categories", ["seo_slug" => $filters["category_id"]])->row();
            if (!$row) {
                echo "Category not found";
            } else {
                $filters["category_id"] = $row->id;
            }
        }

        $total_results = $this->blog_model->get_pages($filters);

        $limit = $this->input->get_post("limit") ? $this->input->get_post("limit") : ORDERS_PER_PAGE;
        $_GET['page'] = $this->input->get_post("page");
        $start = $this->input->get_post("page") > 1 ? ($this->input->get_post("page") - 1) * $limit : 0;
        $filters["limit"] = $limit;
        $filters["start"] = $start;

        $result["total_results_found"] = $total_results;
        $result["total_pages"] = ceil($total_results / $limit);

        $pagination = my_pagination("blog", $limit, $total_results, true);
        $pagination['pagination'] = str_replace("&amp;", "&", $pagination['pagination']);
        $cur_page = ($pagination['pagination_helper']->cur_page != 0) ? ($pagination['pagination_helper']->cur_page - 1) : $pagination['pagination_helper']->cur_page;
        $pagination['initial_id'] = (($cur_page) * $pagination['pagination_helper']->per_page) + 1;


        $this->data["pages"] = $this->blog_model->get_pages($filters);
        $this->data["pagination"] = $pagination;

        $filters["start"] = 0;
        $filters["limit"] = 5;
        $this->data["related_pages"] = $this->blog_model->get_pages($filters);


        $this->front_view("cms/blog");
    }

    function view() {
        $filters["seo_slug"] = $this->uri->segment(3);
        $filters["start"] = 0;
        $filters["limit"] = 1;
        $this->data["row"] = $this->blog_model->get_pages($filters);
        $this->data["row"] = $this->data["row"][0];


        $filters["start"] = 0;
        $filters["limit"] = 5;
        $this->data["related_pages"] = $this->blog_model->get_pages($filters);
        $this->front_view("cms/blog_view");
    }

}
