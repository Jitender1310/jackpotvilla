<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Deposit extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->login_required();
    }

    public function index() {
        // OLD Code Starts Here
        /*$this->data["page_active"] = "deposit";

        $this->data['payment_options'] = [
            ["key" => "Paytm", "image" => "paytm.png"],
            ["key" => "Credit/Debit Card", "image" => "creditdebiticon.png"],
            ["key" => "UPI", "image" => "upi.png"],
            ["key" => "Net Banking", "image" => "ntebanking.png"]
                //["key" => "Lazy Pay", "image" => "lazyppay.png"],
                //["key" => "Google Pay", "image" => "google_pay.png"],
                //["key" => "Debit Card", "image" => "debit_card.png"],
                //["key" => "Wallet", "image" => "wallet.png"],
        ];

        $this->front_view("deposit");*/
        // OLD Code Ends Here

        $player_id = $this->player_login_model->get_logged_player_id();
        $player_detail = $this->games_model->get_player_by_id($player_id);
        $this->front_view("deposit");
    }

    function submit(){
        $player_id = $this->player_login_model->get_logged_player_id();
        $player_detail = $this->games_model->get_player_by_id($player_id);

        $type = $this->input->post('type');
        switch($type){
            case 'eth':
                $address = $player_detail->eth_address;
                break;
            case 'btc':
                $address = $player_detail->btc_address;
                break;
        }

        $callback = urlencode(base_url() . "deposit/crypto");

        redirect('/deposit/temp?type=' . $type . '&address=' . $address . '&token=' . time() . '&timestamp=' . time() . '&callback=' . $callback);
    }

    function temp(){
        $type = $this->input->get('type');
        $receiver = $this->input->get('address');
        $token = $this->input->get('token');
        $timestamp = $this->input->get('timestamp');
        $callback = $this->input->get('callback');
        $amount = 1;
        $sender = 'abcd';
        $tid = 'xyz';

        $final_url = $callback . '?type=' . $type . '&receiver=' . $receiver . '&sender=' . $sender . '&token=' . $token . '&timestamp=' . $timestamp . '&amount=' . $amount . '&tid=' . $tid;
        redirect($final_url);
    }

    function crypto(){
        $player_id = $this->player_login_model->get_logged_player_id();
        $player_detail = $this->games_model->get_player_by_id($player_id);
        $type = $this->input->get('type');
        $receiver = $this->input->get('receiver');
        $token = $this->input->get('token');
        $timestamp = $this->input->get('timestamp');
        $callback = $this->input->get('callback');
        $amount = $this->input->get('amount');
        $sender = $this->input->get('sender');
        $tid = $this->input->get('tid');

        $currency_1 = $type;
        $currency_2 = $player_detail->currency;
        $currency_conversion = $this->games_model->get_currency_value($currency_1, $currency_2);
        $deposit_amount = $currency_conversion->value * $amount;

        $data = [
            "players_id" => $player_id,
            "credits_count" => $deposit_amount,
            "source" => "website",
            "version_code" => "",
            "payment_gateway" => strtoupper($type)
        ];

        $response = $this->deposit_model->create_request($data);
        $transactionId = $response;

        $redirect_url = "/payment_response_controller?orderId=" . $transactionId . "&txStatus=SUCCESS&referenceId=crypto_" . $transactionId;
        redirect($redirect_url);
    }

}
