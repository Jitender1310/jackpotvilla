<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Forgot_password extends MY_Controller {

    function __construct() {
        parent::__construct();
       if ($this->is_logged_in()) {
            redirect("account");
        }
    }

    public function index() {
        $this->data["page_active"] = "forgot_password";
        $this->front_view("forgot_password");
    }

}
