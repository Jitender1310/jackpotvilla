<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blog_view extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->front_view( "cms/blog-view");
    }

}
