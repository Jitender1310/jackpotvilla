<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Mail_template extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function view($page_name) {
        $this->load->view("mail_templates/" . $page_name);
    }

}
