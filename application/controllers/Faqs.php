<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Faqs extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data["page_active"] = "faqs";
        
        $this->data["content"] = $this->cms_model->get_page(4);
        $this->front_view("cms/faqs");
    }

}
