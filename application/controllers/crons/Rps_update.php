<?php

class Rps_update extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    function index() {
        $this->load->model("rps_expiry_cron_model");
        $this->rps_expiry_cron_model->check_and_update_rps_points();
    }

}
