<?php

class Bonus_update extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    function index() {
        $this->bonus_expiry_cron_model->check_and_update_bonus_amounts();
    }

}
