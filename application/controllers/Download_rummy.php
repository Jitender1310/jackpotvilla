<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Download_rummy extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data["page_active"] = "download_rummy";
        $this->front_view("cms/download_rummy");
    }

}
