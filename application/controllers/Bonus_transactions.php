<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bonus_transactions extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->login_required();
    }

    public function index() {
        $this->data["page_active"] = "bonus_transactions";
        $this->front_view("bonus-transactions");
    }

}
