<?php

class Verify_email extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        extract($_REQUEST);
        $response = $this->player_profile_model->mark_as_email_verified($key, $username, $access_token);
        if ($this->session->userdata("p_access_token")) {
             $this->data["player_details"] = $this->player_login_model->get_player_details();
        }
        $this->data["page_active"] = "account";
        if ($response) {
            $this->front_view("email_verified");
        } else {
            //if already verified redirect to home for testing i showing same page instead of redirecting
            $this->front_view("email_verified");
            //redirect("home");
        }
    }

}
