<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bring_a_buddy extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data["page_active"] = "bring_a_buddy";
        $this->front_view("cms/bring_a_buddy");
    }

}
