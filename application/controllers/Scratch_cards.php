<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Scratch_cards extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }


    public function index() {
        if( $this->input->get('referral_code') ){
            $referral_code = $this->input->get('referral_code');
        } else {
            $referral_code = '';
        }

        $is_logged_in = $this->is_logged_in();
        $play_token = '';

        if($is_logged_in){
            $player_id = $this->player_login_model->get_logged_player_id();
            $player_details = $this->player_profile_model->get_profile_information($player_id);
            $access_token = $player_details->access_token;
        }

        $this->data = ['referral_code' => $referral_code, "is_logged_in" => $is_logged_in, "access_token" => $access_token];
        $this->data["page_active"] = "scratch_cards";
        $this->front_view("scratch_cards");
    }


}
