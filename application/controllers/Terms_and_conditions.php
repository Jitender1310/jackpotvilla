<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Terms_and_conditions extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data["page_active"] = "terms_and_conditions";
        
        $this->data["content"] = $this->cms_model->get_page(2);
        $this->front_view("cms/terms_and_conditions");
    }

}
