<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Online_offline extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data["page_active"] = "online_offline";
        $this->front_view("cms/online_offline");
    }

}
