<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Withdraw_charges extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data["page_active"] = "withdraw_charges";
        $this->data["content"] = $this->cms_model->get_page(7);
        $this->front_view("withdraw_charges");
    }

}
