<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Legality extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data["page_active"] = "legality";
        
        $this->data["content"] = $this->cms_model->get_page(6);
        $this->front_view("cms/legality");
    }

}
