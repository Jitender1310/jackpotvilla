<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Responsible_gaming extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data["page_active"] = "responsible_gaming";
        $this->front_view("cms/responsible_gaming");
    }

}
