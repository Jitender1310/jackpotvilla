<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_controller extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->login_required();
        if(!empty($_POST)){
            redirect("deposit");
        }
    }

    public function index() {
        $this->data["page_active"] = "deposit";
        $transaction_id = $this->input->get_post("transaction_id");
        $order_details = $this->deposit_model->get_depositing_row($transaction_id);
        if (!$order_details) {
            redirect("deposit");
        }
        //echo "<pre>";var_dump($order_details);die();
        if ($order_details->payment_gateway == "Paytm") {
            $this->paytm_payment_connect($order_details);
        } elseif($order_details->payment_gateway == "Pay U Money") {
            $this->Pay_U_Money_connect($order_details);
        }elseif($order_details->payment_gateway == "Razor Pay") {
            $this->Razor_Pay_Money_connect($order_details);
        } else {
            $this->cash_free_payment_connect($order_details);
        }
    }

    function paytm_payment_connect($order_details) {
        require_once APPPATH . 'libraries/Paytm.php';
        $obj = new Paytm();
        //$obj->setServerMode('live');
        $obj->setTransactionid($order_details->transaction_id);
        $obj->setAmount($order_details->credits_count);
        $obj->setConsumerName($order_details->player_details->username);
        $obj->setPhoneNumber($order_details->player_details->mobile);
        $obj->setEmail($order_details->player_details->email);
        $obj->setCustomerId($order_details->players_id);
        $productInfo = "Wallet recharge " . SITE_TITLE;
        $obj->setProductInfo($productInfo);
        $obj->setWebsiteName(make_seo_name(SITE_TITLE));
        $obj->setServerMode(PAYMENT_GATEWAY_MODE);
        $obj->setIndustryTypeId("Retail");
        $obj->setChannelId("WEB");
        $obj->setNotify_url(base_url() . "payment_notify_controller");
        $obj->setReturn_url(base_url() . "payment_response_controller");
        $obj->generateHash();

        $this->deposit_model->update_with_signature($obj->hash, $order_details->transaction_id);

        //while connecting to payment gateway remove session of cart key
        $obj->goto_collect_money();
        //$this->front_view("payment_form");
    }

    function cash_free_payment_connect($order_details) {
        require_once APPPATH . 'libraries/Cash_free.php';
        $obj = new Cash_free();
        $obj->setTransactionid($order_details->transaction_id);
        $obj->setAmount($order_details->credits_count);
        $obj->setConsumerName($order_details->player_details->username);
        $obj->setPhoneNumber($order_details->player_details->mobile);
        $obj->setEmail($order_details->player_details->email);
        $productInfo = "Wallet recharge " . SITE_TITLE;
        $obj->setProductInfo($productInfo);
        $obj->setOrderCurrency("INR");
        $obj->setServerMode(PAYMENT_GATEWAY_MODE);
        //$obj->setServerMode('live');
        $obj->setNotify_url(base_url() . "payment_notify_controller");
        $obj->setReturn_url(base_url() . "payment_response_controller");
        $obj->generateHash();

        $this->deposit_model->update_with_signature($obj->hash, $order_details->transaction_id);

        //while connecting to payment gateway remove session of cart key
        $obj->goto_collect_money();
        //$this->front_view("payment_form");
    }

    function Pay_U_Money_connect($order_details) {
        require_once APPPATH . 'libraries/Pay_u_money.php';
        $obj = new Pay_u_money();
        
        $obj->setUrl(base_url("Payment_response_controller"));
        $obj->setTransactionid($order_details->transaction_id);
        $obj->setAmount($order_details->credits_count);
        $obj->setConsumerName($order_details->player_details->username);
        $obj->setPhoneNumber($order_details->player_details->mobile);
        $obj->setEmail($order_details->player_details->email);
        $productInfo = "Wallet recharge " . SITE_TITLE;
        $obj->setProductInfo($productInfo);
        ///$obj->setOrderCurrency("INR");
        $obj->setServerMode(PAYMENT_GATEWAY_MODE);
        //$obj->setServerMode('live');
        //$obj->setNotify_url(base_url() . "payment_notify_controller");
        //$obj->setReturn_url(base_url() . "payment_response_controller");
        $obj->generateHashString();

        $this->deposit_model->update_with_signature($obj->hash, $order_details->transaction_id);

        //while connecting to payment gateway remove session of cart key
        $obj->goto_collect_money();
        //$this->front_view("payment_form");
    }

    function Razor_Pay_Money_connect($order_details) {
        // $obj = new Razor_pay();
        $transaction_id = $this->input->cookie('transaction_id',true);
        if(empty($transaction_id) || (!empty($transaction_id) && $transaction_id!=$_GET["transaction_id"] ) ){
            $setData = array(
                'receipt' =>$order_details->transaction_id,
                'amount' =>$order_details->credits_count,
                'currency' =>"INR",
                'action' =>base_url("Payment_response_controller"),
                'payment_capture' =>1,
                'name' =>SITE_TITLE,
                'description' =>"Wallet recharge " . SITE_TITLE,
                'image' =>"https://s29.postimg.org/r6dj1g85z/daft_punk.jpg",
                'prefill_name' =>$order_details->player_details->username,
                'prefill_email' =>$order_details->player_details->email,
                'prefill_contact' =>$order_details->player_details->mobile,
                'prefill_address' =>"",
                'theme_color' =>"#F37254"
            );
            require_once APPPATH . 'libraries/Razor_pay.php';
            
            $cookie= array(
               'name'   => 'transaction_id',
               'value'  => $_GET["transaction_id"],
               'expire' => '3600',
           );
           $this->input->set_cookie($cookie);
       }
        //orderData_initialize($setData);
        //order_create();
        //check_displayCurrency();
        //set_data_and_json_encode($setData);
        
        $row = $this->deposit_model->get_depositing_row($transaction_id);
        if ($row->source == 'website') {
            //echo "<pre style='color:#000'>".$row->source;var_dump($row->source === 'website');die();
            $this->data["page_active"] == "deposit";
            $this->front_view("deposit");
        } else if ($row->source == 'unity_mobile_app') {
            $this->load->view("mobile_app/deposit");
        }
    }

}
