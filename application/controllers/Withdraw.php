<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Withdraw extends MY_Controller {

        function __construct() {
            parent::__construct();
            $this->login_required();
        }

        public function index() {
            $this->data["page_active"] = "withdraw";
            $this->front_view("withdraw");
        }

    }
    