<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Contact_us
 *
 * @author KrishnaPadam
 */
class Contact_us extends MY_Controller {
    public function __construct(){
        parent::__construct();
    }
    
    public function index(){
        $this->data["page_active"] = "contact_us";
        $this->front_view("contact_us");
    }
}
