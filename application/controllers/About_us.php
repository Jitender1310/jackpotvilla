<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class About_us extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data["page_active"] = "about_us";
        $this->data["content"] = $this->cms_model->get_page(1);
        $this->front_view("cms/about");
    }

}
