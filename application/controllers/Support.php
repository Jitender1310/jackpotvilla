<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Support extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data["page_active"] = "support";
        $this->front_view("support");
    }

}
