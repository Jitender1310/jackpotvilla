<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Help extends MY_Controller {

        function __construct() {
            parent::__construct();
        }

        public function index() {
            $this->data["page_active"] = "help";
            $this->front_view("help");
        }

    }
    