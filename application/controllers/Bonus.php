<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bonus extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data["page_active"] = "bonus";
        $this->front_view("cms/bonus");
    }

}
