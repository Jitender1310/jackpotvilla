<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Games extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->login_required();
    }

    public function index() {
        $this->data["page_active"] = "games";
        $this->front_view("my_games");
    }

}
