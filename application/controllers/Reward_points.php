<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reward_points extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data["page_active"] = "reward_points";
        $this->front_view("cms/reward_points");
    }

}
