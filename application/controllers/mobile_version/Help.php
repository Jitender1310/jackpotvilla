<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Help extends MY_Controller {

        function __construct() {
            parent::__construct();
            $this->login_required();
        }

        public function index() {
            $this->data["page_active"] = "how_to_play";
            $this->load->view("mobile_app/help");
        }

    }
    