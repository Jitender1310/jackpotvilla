<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Bring_a_friend extends MY_Controller {

        function __construct() {
            parent::__construct();
            $this->login_required();
        }

        public function index() {
            $this->data["page_active"] = "bring_a_friend";
            $this->load->view("mobile_app/bring-a-friend");
        }

    }
    