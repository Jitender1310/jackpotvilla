<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Promotions extends MY_Controller {

        function __construct() {
            parent::__construct();
            $this->login_required();
         $this->load->model('tournament_categories_model');
        }

        public function index() {
        $this->data["tournament_categories_data"] = $this->tournament_categories_model->get();
            $this->data["page_active"] = "promotions";
            $this->load->view("mobile_app/promotions");
        }

    }
    