<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Responsible_Play extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data["page_active"] = "responsible_play";
        $this->front_view("responsible_play");
    }

}