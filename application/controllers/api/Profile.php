<?php

header('Content-type: application/json');

class Profile extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("uploaded_image_resize_model");
    }

    function index_get() {
        $player_id = $this->player_login_model->get_logged_player_id();
        $player_details = $this->player_profile_model->get_profile_information($player_id);
        $arr = [
            "status" => "valid",
            "data" => $player_details
        ];
        $arr = json_encode($arr);
        $arr = json_decode(str_replace('null', '""', $arr));
        $this->response($arr);
    }

    function index_post() {
        return $this->index_get();
    }

    function update_post() {
        $player_id = $this->player_login_model->get_logged_player_id();
        $this->form_validation->set_rules("firstname", "Firstname", "required", array(
            'required' => 'Firstname cannot be empty'
        ));

        $this->form_validation->set_rules("lastname", "Lastname", "required", array(
            'required' => 'Lastname cannot be empty'
        ));

        $this->form_validation->set_rules("gender", "Gender", "required", array(
            'required' => 'Please choose your gender'
        ));

        $this->form_validation->set_rules("date_of_birth", "Date of Birth", "required", array(
            'required' => 'Date of Birth cannot be empty'
        ));

        $this->form_validation->set_rules("address_line_1", "Address", "required", array(
            'required' => 'Please enter your address'
        ));

        $this->form_validation->set_rules("pin_code", "Pin code", "required", array(
            'required' => 'Pin code cann\'t be empty'
        ));

        $this->form_validation->set_rules("states_id", "State", "required", array(
            'required' => 'Please choose your state'
        ));

        $this->form_validation->set_rules("city", "City", "required", array(
            'required' => 'Please enter your city name'
        ));

        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "firstname" => form_error("firstname"),
                "lastname" => form_error("lastname"),
                "gender" => form_error("gender"),
                "date_of_birth" => form_error("date_of_birth"),
                "address_line_1" => form_error("address_line_1"),
                "pin_code" => form_error("pin_code"),
                "city" => form_error("city"),
                "states_id" => form_error("states_id")
            ];
            $arr = [
                "status" => "invalid_form",
                "title" => "Required",
                "message" => "Please fill all mandatory fields",
                "data" => $errors
            ];
            $this->response($arr);
        } else {
            $data = [
                "firstname" => $this->post("firstname"),
                "lastname" => $this->post("lastname"),
                "gender" => $this->post("gender"),
                "date_of_birth" => convert_date_to_db_format($this->post("date_of_birth")),
                "address_line_1" => $this->post("address_line_1"),
                "address_line_2" => $this->post("address_line_2"),
                "city" => $this->post("city"),
                "pin_code" => $this->post("pin_code"),
                "states_id" => $this->post("states_id")
            ];
            $response = $this->player_profile_model->update_profile($data, $player_id);
            if ($response) {
                $arr = array(
                    'status' => "valid",
                    "title" => "Profile Updated",
                    "message" => "Profile updated successfully"
                );
                $this->response($arr);
            } else {
                $arr = [
                    "status" => "invalid",
                    "title" => "Profile not updated",
                    "message" => "Profile not updated",
                    "data" => ""
                ];
                $this->response($arr);
            }
        }
    }

    function update_email_post() {
        $player_id = $this->player_login_model->get_logged_player_id();
        $this->form_validation->set_rules("email", "Email", "required", array(
            'required' => 'Please enter a valid email address.'
        ));
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "email" => form_error("email")
            ];
            $arr = [
                "status" => "invalid_form",
                "data" => $errors
            ];
            $this->response($arr);
        } else {
            $player_details = $this->player_profile_model->get_profile_information($player_id);
            if ($player_details->email == $this->post("email")) {
                $errors = [
                    "email" => "The new email is same as old email."
                ];
                $arr = [
                    "status" => "invalid_form",
                    "data" => $errors
                ];
                $this->response($arr);
            } else {
                $data = [
                    "new_email" => $this->post("email")
                ];
                $response = $this->player_profile_model->update_new_email($data, $player_id);
                if ($response) {
                    $arr = [
                        "status" => "valid",
                        "title" => "Verification Email send",
                        "message" => "A confirmation link has been sent to your email id. "
                        . "Please check and click on the link to verify.",
                        "data" => ""
                    ];
                    $this->response($arr);
                } else {
                    $arr = [
                        "status" => "invalid",
                        "title" => "Error",
                        "message" => "Email not updated",
                        "data" => ""
                    ];
                    $this->response($arr);
                }
            }
        }
    }

    function update_mobile_post() {
        $player_id = $this->player_login_model->get_logged_player_id();
        $this->form_validation->set_rules("mobile", "Mobile", "required|min_length[10]|max_length[10]", array(
            'required' => 'Please enter a valid mobile number.'
        ));
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "email" => form_error("email")
            ];
            $arr = [
                "status" => "invalid_form",
                "data" => $errors
            ];
            $this->response($arr);
        } else {
            $player_details = $this->player_profile_model->get_profile_information($player_id);
            if ($player_details->mobile == $this->post("mobile")) {
                $errors = [
                    "mobile" => "The new mobile number is same as old number."
                ];
                $arr = [
                    "status" => "invalid_form",
                    "data" => $errors
                ];
                $this->response($arr);
            } else {
                $data = [
                    "new_mobile" => $this->post("mobile")
                ];
                $response = $this->player_profile_model->update_new_mobile($data, $player_id);
                if ($response) {
                    $arr = [
                        "status" => "valid",
                        "title" => "Verification Code Sent",
                        "message" => "A verification code has been sent to your mobile number. "
                        . "Please check and verify with OTP.",
                        "data" => ""
                    ];
                    $this->response($arr);
                } else {
                    $arr = [
                        "status" => "invalid",
                        "title" => "Error",
                        "message" => "Mobile not updated",
                        "data" => ""
                    ];
                    $this->response($arr);
                }
            }
        }
    }

    function send_email_verification_code_get() {
        $player_id = $this->player_login_model->get_logged_player_id();
        $player_details = $this->player_profile_model->get_profile_information($player_id);
        if ($player_details->email_verified == 1) {
            $arr = [
                "status" => "valid",
                "title" => "Already verified",
                "message" => "Your email already verified",
                "data" => ""
            ];
            $this->response($arr);
        } else {
            $this->player_notifications_model->send_email_verification_link($player_id);
            $arr = [
                "status" => "valid",
                "title" => "Verification Email send",
                "message" => "A confirmation link has been sent to your email id. "
                . "Please check and click on the link to verify.",
                "data" => ""
            ];
            $this->response($arr);
        }
    }

    function verify_otp_post() {
        $player_id = $this->player_login_model->get_logged_player_id();
        $this->form_validation->set_rules("otp", "Mobile", "required|min_length[4]|max_length[4]", array(
            'required' => 'Invalid verification code'
        ));
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "otp" => form_error("otp")
            ];
            $arr = [
                "status" => "invalid_form",
                "data" => $errors
            ];
            $this->response($arr);
        } else {
            $player_details = $this->player_profile_model->get_profile_information($player_id, true);
            if ($player_details->otp == $this->post("otp")) {
                $response = $this->player_profile_model->mark_as_mobile_verified($player_details->id);
                if ($response) {
                    $arr = [
                        "status" => "valid",
                        "title" => "Verified",
                        "message" => "Mobile verified successfully.",
                        "data" => ""
                    ];
                    $this->response($arr);
                } else {
                    $arr = [
                        "status" => "invalid",
                        "title" => "Error",
                        "message" => "Some error occured",
                        "data" => ""
                    ];
                    $this->response($arr);
                }
            } else {
                $errors = [
                    "otp" => "Invalid verification code"
                ];
                $arr = [
                    "status" => "invalid_form",
                    "data" => $errors
                ];
                $this->response($arr);
            }
        }
    }

    function resend_otp_get() {
        $player_id = $this->player_login_model->get_logged_player_id();
        $player_details = $this->player_profile_model->get_profile_information($player_id, true);
        if (!$player_details->new_mobile && $player_details->mobile_verified == 1) {
            $errors = [
                "mobile" => "This mobile number already verified"
            ];
            $arr = [
                "status" => "invalid_form",
                "data" => $errors
            ];
            $this->response($arr);
        } else {
            $response = $this->player_notifications_model->send_otp($player_id);
            if ($response) {
                $arr = [
                    "status" => "valid",
                    "title" => "Verification Code Sent",
                    "message" => "A verification code has been sent to your mobile number. "
                    . "Please check and verify with OTP.",
                    "data" => ""
                ];
                $this->response($arr);
            } else {
                $arr = [
                    "status" => "invalid",
                    "title" => "Error",
                    "message" => "Some error occured",
                    "data" => ""
                ];
                $this->response($arr);
            }
        }
    }

    function update_kyc_post() {
        $player_id = $this->player_login_model->get_logged_player_id();
        $this->form_validation->set_rules("address_proof_type", "Address Proof Type", "required", array(
            'required' => 'Please choose address proof type'
        ));
        $this->form_validation->set_rules("pan_card_number", "PAN Card Number", "required", array(
            'required' => 'PAN Card Number cannot be empty'
        ));
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "address_proof_type" => form_error("address_proof_type"),
                "pan_card_number" => form_error("pan_card_number")
            ];
            $arr = [
                "status" => "invalid_form",
                "data" => $errors
            ];
            $this->response($arr);
        } else {

            if (@$_FILES["address_proof"]["name"]) {
                $config['upload_path'] = KYC_ATTACHMENTS_UPLOAD_FOLDER;
                $config['allowed_types'] = 'jpg|jpeg|pdf';
                $config['max_size'] = 5000;
                $config['overwrite'] = FALSE;
                //$config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;

                $extension = pathinfo(@$_FILES['address_proof']['name'], PATHINFO_EXTENSION);
                $file_name = "address_proof" . time() . generateRandomNumber(30) . '.' . $extension;
                $config["file_name"] = $file_name;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('address_proof')) {
                    if ($this->upload->display_errors() == "The file you are attempting to upload is larger than the permitted size.") {
                        $this->data['error'] = array('error' => "The file you are attempting to upload is larger than the permitted size 2MB");
                    } else {
                        $this->data['error'] = array('error' => $this->upload->display_errors());
                    }

                    $errors = [
                        "address_proof_type" => form_error("address_proof_type"),
                        "pan_card_number" => form_error("pan_card_number"),
                        "address_proof" => strip_tags($this->data['error']["error"]),
                    ];
                    $arr = [
                        "status" => "invalid_form",
                        "data" => $errors
                    ];
                    $this->response($arr);
                    die;
                } else {
                    $uploaded_data = array('upload_data' => $this->upload->data());

                    $this->uploaded_image_resize_model->resize_uploaded_image($uploaded_data);

                    $file_name = $uploaded_data["upload_data"]["file_name"];
                    $data["address_proof"] = $file_name;
                    $data["address_proof_status"] = "Pending Approval";
                }
            }

            if (@$_FILES["address_proof_back_side"]["name"]) {
                $config['upload_path'] = KYC_ATTACHMENTS_UPLOAD_FOLDER;
                $config['allowed_types'] = 'jpg|jpeg|pdf';
                $config['max_size'] = 5000;
                $config['overwrite'] = FALSE;
                //$config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;

                $extension = pathinfo(@$_FILES['address_proof_back_side']['name'], PATHINFO_EXTENSION);
                $file_name = "address_proof_back_side" . time() . generateRandomNumber(30) . '.' . $extension;
                $config["file_name"] = $file_name;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('address_proof_back_side')) {
                    if ($this->upload->display_errors() == "The file you are attempting to upload is larger than the permitted size.") {
                        $this->data['error'] = array('error' => "The file you are attempting to upload is larger than the permitted size 3MB");
                    } else {
                        $this->data['error'] = array('error' => $this->upload->display_errors());
                    }

                    $errors = [
                        "address_proof_type" => form_error("address_proof_type"),
                        "pan_card_number" => form_error("pan_card_number"),
                        "address_proof" => "",
                        "address_proof_back_side" => strip_tags($this->data['error']["error"]),
                    ];
                    $arr = [
                        "status" => "invalid_form",
                        "data" => $errors
                    ];
                    $this->response($arr);
                    die;
                } else {
                    $uploaded_data = array('upload_data' => $this->upload->data());
                    $this->uploaded_image_resize_model->resize_uploaded_image($uploaded_data);
                    $file_name = $uploaded_data["upload_data"]["file_name"];
                    $data["address_proof_back_side"] = $file_name;
                    $data["address_proof_status"] = "Pending Approval";
                }
            }

            if (@$_FILES["pan_card"]["name"]) {
                $config['upload_path'] = KYC_ATTACHMENTS_UPLOAD_FOLDER;
                $config['allowed_types'] = 'jpg|jpeg|pdf';
                $config['max_size'] = 5000;
                $config['overwrite'] = FALSE;
                //$config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;

                $extension = pathinfo(@$_FILES['pan_card']['name'], PATHINFO_EXTENSION);
                $file_name = "pan_card" . time() . generateRandomNumber(30) . '.' . $extension;
                $config["file_name"] = $file_name;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('pan_card')) {
                    if ($this->upload->display_errors() == "The file you are attempting to upload is larger than the permitted size.") {
                        $this->data['error'] = array('error' => "The file you are attempting to upload is larger than the permitted size 2MB");
                    } else {
                        $this->data['error'] = array('error' => $this->upload->display_errors());
                    }

                    $errors = [
                        "address_proof_type" => form_error("address_proof_type"),
                        "pan_card_number" => form_error("pan_card_number"),
                        "address_proof" => "",
                        "pan_card" => strip_tags($this->data['error']["error"]),
                    ];
                    $arr = [
                        "status" => "invalid_form",
                        "data" => $errors
                    ];
                    $this->response($arr);
                    die;
                } else {
                    $uploaded_data = array('upload_data' => $this->upload->data());
                    $this->uploaded_image_resize_model->resize_uploaded_image($uploaded_data);
                    $file_name = $uploaded_data["upload_data"]["file_name"];
                    $data["pan_card"] = $file_name;
                    $data["pan_card_status"] = "Pending Approval";
                }
            }

            $data["address_proof_type"] = $this->post("address_proof_type");
            $data["pan_card_number"] = $this->post("pan_card_number");


            $response = $this->player_kyc_model->update_kyc($data, $player_id);
            if ($response) {
                $arr = [
                    "status" => "valid",
                    "title" => "KYC Details submitted",
                    "message" => "Kyc details has been submitted",
                    "data" => ""
                ];
                $this->response($arr);
            } else {
                $arr = [
                    "status" => "invalid",
                    "title" => "Error",
                    "message" => "Some error occured",
                    "data" => ""
                ];
                $this->response($arr);
            }
        }
    }

    function update_avatar_post() {
        $player_id = $this->player_login_model->get_logged_player_id();
        $this->form_validation->set_rules("avatar_id", "Avatar", "required", array(
            'required' => 'Please select avatar'
        ));
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "avatar_id" => form_error("avatar_id")
            ];
            $arr = [
                "status" => "invalid_form",
                "data" => $errors
            ];
            $this->response($arr);
        } else {
            $avatar_id = $this->post("avatar_id");
            $response = $this->default_avatars_model->set_avatar_to_player($avatar_id, $player_id);
            if ($response) {
                $arr = [
                    "status" => "valid",
                    "title" => "Profile updated",
                    "message" => "Profile avatar updated",
                    "data" => []
                ];
                $this->response($arr);
            } else {
                $arr = [
                    "status" => "invalid",
                    "title" => "Error",
                    "message" => "Some error occured",
                    "data" => []
                ];
                $this->response($arr);
            }
        }
    }

}
