<?php

header('Content-type: application/json');

class Bring_a_friend extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function index_post() {
        $player_id = $this->player_login_model->get_logged_player_id();
        
        
    }
    
    function send_invitations_post(){
        $player_id = $this->player_login_model->get_logged_player_id();
        $json = file_get_contents("php://input");
        $received_data = json_decode($json);
        $data = [];
        foreach( $received_data as $item ){
            if($item->name !="" && ($item->email!="" || $item->mobile!="")){
                $data[] = $item;
            }
        }
        $response = $this->bring_a_friend_model->send_invitations($data, $player_id);
        if($response){
            $arr = [
                "status" => "valid",
                "title"=>"Invitation Sent",
                "message"=>"Inviation sent successfully",
                "data" => [
                    "message"=>"Inviation sent"
                ]
            ];
        }else{
            $arr = [
                "status" => "invalid",
                "title"=>"Invitation not Sent",
                "message"=>"Some error occured",
                "data" => [
                    "message"=>"Some error occured"
                ]
            ];
        }
        $this->response($arr);
    }
    function send_invitations_multiple_get(){
        $player_id = $this->player_login_model->get_logged_player_id();
        $phones = json_decode($_GET['phones'],false);
        if(count($phones)>0){
            $data = [];
            foreach( $phones as $item ){
                if(trim( $item->value) !=""){
                    $data[] = $item;
                }
            }
            $response = $this->bring_a_friend_model->send_invitations_moblie($data, $player_id);
        }
        $emails = json_decode($_GET['emails'],false);
        if(count($emails)>0){
            $data = [];
            foreach( $emails as $item ){
                if(trim( $item->value) !=""){
                    $data[] = $item;
                }
            }
            $response = $this->bring_a_friend_model->send_invitations_email($data, $player_id);
        }
        if($response){
            $arr = [
                "status" => "valid",
                "title"=>"Invitation Sent",
                "message"=>"Inviation sent successfully",
                "data" => [
                    "message"=>"Inviation sent"
                ]
            ];
        }else{
            $arr = [
                "status" => "invalid",
                "title"=>"Invitation not Sent",
                "message"=>"Some error occured",
                "data" => [
                    "message"=>"Some error occured"
                ]
            ];
        }
        $this->response($arr);
    }

}
