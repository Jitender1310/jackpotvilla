<?php

header('Content-type: application/json');

class Tournaments extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("tournaments_model");
        $this->load->model("club_types_model");
    }

    function categories_get() {
        $response = $this->tournaments_model->get_tournament_categories();

        if ($response) {
            $arr = [
                "status" => "valid",
                "title" => "Tournament Categories",
                "message" => "Tournament Categories List",
                "data" => [
                    "tournament_categories" => $response
                ]
            ];
        } else {
            $arr = [
                "status" => "invalid",
                "title" => "Error",
                "message" => "Some error occured",
                "data" => [
                    "message" => "Some error occured"
                ]
            ];
        }
        $this->response($arr);
    }

    function categories_post() {
        $this->categories_get();
    }

    function index_get() {
        $category_id = $this->input->get_post("category_id");
        $players_id = $this->player_login_model->get_logged_player_id();
        $response = $this->tournaments_model->get_available_tournaments_by_category_id($category_id, $players_id);

        $arr = [
            "status" => "valid",
            "title" => "Tournaments for category",
            "message" => "Tournament  for category",
            "data" => [
                "tournaments" => $response
            ]
        ];
        $this->response($arr);
    }

    function index_post() {
        $this->index_get();
    }

    function info_get() {
        $this->info_post();
    }

    function info_post() {
        $tournament_id = $this->input->get_post("tournament_id");
        $players_id = $this->player_login_model->get_logged_player_id();
        $response = $this->tournaments_model->get_tournament_row($tournament_id, $players_id);

        $arr = [
            "status" => "valid",
            "title" => "Tournaments details",
            "message" => "Tournament  details",
            "data" => [
                "tournament_details" => $response
            ]
        ];
        $this->response($arr);
    }

    function join_post() {
        $players_id = $this->player_login_model->get_logged_player_id();
        $this->form_validation->set_rules("tournament_id", "Tournaments", "required", array(
            'required' => 'Tournaments cannot be empty'
        ));

        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "tournament_id" => form_error("tournament_id")
            ];
            $arr = [
                "status" => "invalid_form",
                "title" => "Required",
                "message" => "Please select tournament",
                "data" => $errors
            ];
            $this->response($arr);
            die;
        } else {
            $tournaments_id = $this->post("tournament_id");
            $response = $this->tournaments_joining_model->join($tournaments_id, $players_id);

            if ($response === "CASH_TRANSACTION_REQUIRED") {
                $arr = [
                    "status" => "invalid",
                    "title" => "Sorry!",
                    "message" => "We are sorry, you are not eligible to play tournaments. Required to do atleast one cash transaction to your wallet",
                    "data" => [
                        "message" => "We are sorry, you are not eligible to play tournaments. Required to do atleast one cash transaction to your wallet"
                    ],
                    "debug_point" => $response
                ];
            } else if ($response === "CASH_GAME_PLAY_REQUIRED") {
                $arr = [
                    "status" => "invalid",
                    "title" => "Sorry!",
                    "message" => "We are sorry, you are not eligible to play tournaments. Required to play atleast one cash game",
                    "data" => [
                        "message" => "We are sorry, you are not eligible to play tournaments. Required to play atleast one cash game"
                    ],
                    "debug_point" => $response
                ];
            }/* else if ($response === "KYC_VERIFICATION_PENDING") {
              $arr = [
              "status" => "invalid",
              "title" => "Sorry!",
              "message" => "We are sorry, you are not eligible to play tournaments. Please verify your KYC Details from Home>My Account>Account Details",
              "data" => [
              "message" => "We are sorry, you are not eligible to play tournaments. Please verify your KYC Details from Home>My Account>Account Details"
              ],
              "debug_point" => $response
              ];
              } *//* else if ($response === "EMAIL_VERIFICATION_REQUIRED") {
              $arr = [
              "status" => "invalid",
              "title" => "Sorry!",
              "message" => "We are sorry, you are not eligible to play tournaments. Please verify your Email id from Home>My Account>Account Details",
              "data" => [
              "message" => "We are sorry, you are not eligible to play tournaments. Please verify your Email id from Home>My Account>Account Details"
              ],
              "debug_point" => $response
              ];
              } */ else if ($response === "TOURNAMENT_REJECTED") {
                $arr = [
                    "status" => "invalid",
                    "title" => "Sorry!",
                    "message" => "We are sorry, Tournament Rejected",
                    "data" => [
                        "message" => "We are sorry, Tournament Rejected"
                    ],
                    "debug_point" => $response
                ];
            } else if ($response === "TOURNAMENT_REGISTRATION_CLOSED") {
                $arr = [
                    "status" => "invalid",
                    "title" => "Sorry!",
                    "message" => "We are sorry, Tournament registration closed",
                    "data" => [
                        "message" => "We are sorry, Tournament registration closed"
                    ],
                    "debug_point" => $response
                ];
            } else if ($response === "TOURNAMENT_COMPLETED") {
                $arr = [
                    "status" => "invalid",
                    "title" => "Sorry!",
                    "message" => "We are sorry, Tournament already completed",
                    "data" => [
                        "message" => "We are sorry, Tournament already completed"
                    ],
                    "debug_point" => $response
                ];
            } else if ($response === "REGISTRATION_NOT_STARTED") {
                $arr = [
                    "status" => "invalid",
                    "title" => "Sorry!",
                    "message" => "We are sorry, Tournament not started",
                    "data" => [
                        "message" => "We are sorry, Tournament not started"
                    ],
                    "debug_point" => $response
                ];
            } else if ($response === "ONLY_FOR_EXPERTS") {
                $allowed_types = $this->db->get_where('cloned_tournaments', array('id' => $this->post('tournament_id')))->row()->allowed_club_types;
                $selected_club_types = explode(",", $allowed_types);
                $allowed_club_types = [];
                for ($i = 0; $i < count($selected_club_types); $i++) {
                    $this->db->where("id", $selected_club_types[$i]);
                    $club_types = $this->club_types_model->get_club_types();
                    foreach ($club_types as $c_item) {
                        $allowed_club_types[] = $c_item->name;
                    }
                }
                $allowed_club_types_text = implode(", ", $allowed_club_types);
                $arr = [
                    "status" => "invalid",
                    "title" => "Sorry!",
                    "message" => "We are sorry, This tournament is for only for " . $allowed_club_types_text . " level players",
                    "data" => [
                        "message" => "We are sorry, This tournament is for only for " . $allowed_club_types_text . " level players"
                    ],
                    "debug_point" => $response
                ];
            } else if ($response === "ALREADY_JOINED") {
                $arr = [
                    "status" => "invalid",
                    "title" => "Oops!",
                    "message" => "Your already joined for this tournament",
                    "data" => [
                        "message" => "Your already joined for this tournament"
                    ],
                    "debug_point" => $response
                ];
            } else if ($response === "TOURNAMENT_RUNNING") {
                $arr = array(
                    'status' => "invalid",
                    "title" => "Tournament already started",
                    "message" => "Tournament is already in running state, no joings are allowed",
                    "data" => [
                        "message" => "Tournament is already in running state, no joings are allowed"
                    ],
                    "debug_point" => $response
                );
            } else if ($response === "NOT_ELIGIBLE_FOR_PREMIUM_TOURNAMENTS") {
                $arr = array(
                    'status' => "invalid",
                    "title" => "Not Eligible for Premium tournaments",
                    "message" => "Sorry, Your are not eligible to join this premium tournament",
                    "data" => [
                        "message" => "Sorry, Your are not eligible to join this premium tournamen"
                    ],
                    "debug_point" => $response
                );
            } else if ($response === "INSUFFICIENT_FUNDS") {
                $arr = array(
                    'status' => "invalid",
                    "title" => "Not Eligible for tournaments",
                    "message" => "Sorry, Your are not eligible to join this premium tournament",
                    "data" => [
                        "message" => "Sorry, Your are not eligible to join this premium tournamen"
                    ],
                    "debug_point" => $response
                );
            } else if ($response) {
                $arr = array(
                    'status' => "valid",
                    "title" => "Joined successfully",
                    "message" => "You are successfully joined to this tournament",
                    "data" => [
                        "message" => "You are successfully joined to this tournament"
                    ],
                    "debug_point" => $response
                );
            } else {
                $arr = [
                    "status" => "invalid",
                    "title" => "Not joined",
                    "message" => "Not joined, please try again",
                    "data" => [
                        "message" => "Not joined, please try again"
                    ],
                    "debug_point" => $response
                ];
            }
            $this->response($arr);
        }
    }

    function unjoin_post() {
//            log_message("error", print_r($_POST, true));
        $players_id = $this->player_login_model->get_logged_player_id();
        $this->form_validation->set_rules("tournament_id", "Tournaments", "required", array(
            'required' => 'Tournaments cannot be empty'
        ));

        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "tournament_id" => form_error("tournament_id")
            ];
            $arr = [
                "status" => "invalid_form",
                "title" => "Required",
                "message" => "Please select tournament",
                "data" => $errors
            ];
            $this->response($arr);
            die;
        } else {
            $tournaments_id = $this->post("tournament_id");
            $response = $this->tournaments_joining_model->unjoin($tournaments_id, $players_id);

            if ($response === "TOURNAMENT_REJECTED") {
                $arr = [
                    "status" => "invalid",
                    "title" => "Sorry!",
                    "message" => "We are sorry, Tournament Rejected, money has been refund if you paid",
                    "data" => [
                        "message" => "We are sorry, Tournament Rejected, money has been refund if you paid"
                    ],
                    "debug_point" => $response
                ];
            } else if ($response === "TOURNAMENT_REGISTRATION_CLOSED") {
                $arr = [
                    "status" => "invalid",
                    "title" => "Sorry!",
                    "message" => "We are sorry, Tournament registration closed unjoined not allowed",
                    "data" => [
                        "message" => "We are sorry, Tournament registration closed unjoined not allowed"
                    ],
                    "debug_point" => $response
                ];
            } else if ($response === "TOURNAMENT_RUNNING") {
                $arr = [
                    "status" => "invalid",
                    "title" => "Sorry!",
                    "message" => "We are sorry, Tournament already running unjoined not allowed",
                    "data" => [
                        "message" => "We are sorry, Tournament already running unjoined not allowed"
                    ],
                    "debug_point" => $response
                ];
            } else if ($response === "TOURNAMENT_COMPLETED") {
                $arr = [
                    "status" => "invalid",
                    "title" => "Sorry!",
                    "message" => "We are sorry, Tournament already completed",
                    "data" => [
                        "message" => "We are sorry, Tournament already completed"
                    ],
                    "debug_point" => $response
                ];
            } else if ($response === "ALREADY_UNJOINED") {
                $arr = [
                    "status" => "invalid",
                    "title" => "Oops!",
                    "message" => "Your already unjoined from this tournament",
                    "data" => [
                        "message" => "Your already unjoined from this tournament"
                    ],
                    "debug_point" => $response
                ];
            } else if ($response) {
                $arr = array(
                    'status' => "valid",
                    "title" => "Unregistered successfully",
                    "message" => "You are unregistered from this tournament"
                );
                $this->response($arr);
            } else {
                $arr = [
                    "status" => "invalid",
                    "title" => "Not joined",
                    "message" => "Not joined, please try again",
                    "data" => ""
                ];
                $this->response($arr);
            }
        }
    }

}
