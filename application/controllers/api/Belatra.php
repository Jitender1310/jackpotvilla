<?php
class Belatra extends REST_Controller
{
    private $_casino_id = 'test_blockmatrix';
    private $_auth_key = '7119711a3f1d4f8d';
    private $_base_url = 'https://test.belatragames.com:34443';

    public function __construct()
    {
        parent::__construct();
    }

    function index_get()
    {
        $arr = array(
            "success" => true
        );
        $this->response($arr);
    }

    function launch_get()
    {
        $game_token = $_GET['token'];
        $casino = $_GET['pn'];
        $locale = $_GET['lang'];
        $game_external_id = $_GET['game'];
        $type = $_GET['type'];

        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';

        $home_url = "https://test.a2zbetting.com/";
        $deposit_url = "https://test.a2zbetting.com/deposit";
        $urls = array(
            'deposit_url' => $deposit_url,
            'return_url' => $home_url
        );

        $player_id = $this->player_login_model->get_logged_player_id();
        $player_detail = $this->games_model->get_player_by_id($player_id);

        $user_array = array(
            'id' => $player_detail->id,
            'email' => $player_detail->email,
            'nickname' => $player_detail->username
        );
        $currency = $player_detail->currency;

        $session_array = array(
            'casino_id' => $this->_casino_id,
            'game' => $game_external_id,
            'currency' => $currency,
            'locale' => $locale,
            'ip' => $ipaddress,
            'urls' => $urls,
            'user' => $user_array
        );

        $session_obj = json_encode($session_array);

        $hash_string = hash_hmac("sha256", $session_obj, $this->_auth_key);

        $headers = array(
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "X-REQUEST-SIGN: $hash_string"
        );

        $curlData['url'] = $this->_base_url . '/sessions';
        $curlData['postData'] = $session_obj;
        $curlData['headers'] = $headers;

        $response = curlPost($curlData);
        $decode_response = json_decode($response, true);

        $game_url = $decode_response['launch_options']['game_url'];
        $session_id = $decode_response['session_id'];

        redirect($game_url);
    }

    function play_post()
    {
        /*$filePath = APPPATH . '/../uploads/wac.txt';
        $fh = fopen($filePath, 'a');
        fwrite($fh, "Hi Ai am called play");
        //fwrite($fh, json_encode($this->post()));
        fwrite($fh, json_encode($this->head()));
        fclose($fh);*/

        $signature = $this->head('X-REQUEST-SIGN');
        $post_json = json_encode($this->post());
        $hash_string = hash_hmac("sha256", $post_json, $this->_auth_key);
        $player_id = $this->post('user_id');
        $gameReference = $this->post('game');

        $player_detail = $this->games_model->get_player_by_id($player_id);
        //$matched = 1;
        if($hash_string != $signature)
        {
            $response = array(
                'code' => 403,
                'message' => "Forbidden. (Request sign doesn't match)"
            );
            if($player_detail)
            {
                $balance = $player_detail->real_chips_deposit * 100;
                $response['balance'] = $balance;
            }
            $this->response($response);
        }
        if(!$player_detail)
        {
            $response = array(
                'code' => 101,
                'message' => "Player is invalid"
            );
            $this->response($response);
        }
        $balance = $player_detail->real_chips_deposit * 100;

        /*$temp_array = array(
            'sent' => $signature,
            'generated' => $hash_string,
            'matched' => $matched
        );

        $fh = fopen($filePath, 'a');
        fwrite($fh, "Hi Ai am comparing");
        fwrite($fh, json_encode($temp_array));
        fclose($fh);*/

        if($this->post('game_id'))
        {
            $response['game_id'] = $this->post('game_id');
        }

        $player_id = $this->post('user_id');
        $gameReference = $this->post('game');

        $player_detail = $this->games_model->get_player_by_id($player_id);
        $balance = $player_detail->real_chips_deposit * 100;

        $game_detail = $this->games_model->get_game_by_reference($gameReference);
        $game_id = $game_detail->id;

        $log['game_id'] = $game_id;
        $log['player_id'] = $player_id;
        $game_token = $this->games_model->add_game_log($log);

        $actions = $this->post('actions');
        if($actions)
        {
            $response_transactions = array();
            foreach($actions as $action)
            {
                $amount = $action['amount'];
                $transactionId = $action['action_id'];
                $roundId = $this->post('game_id') ? $this->post('game_id') : $transactionId;
                $wallet_account_id = $player_detail->wallet_account_id;
                $prev_balance = $balance / 100;
                $current_balance = $balance;
                $transaction = array();

                switch($action['action'])
                {
                    case 'bet':
                        $type = 'WITHDRAWAL';
                        if($balance - $amount < 0)
                        {
                            $response = array(
                                'code' => 100,
                                'message' => "Player has not enough funds to process an action",
                                'balance' => $balance
                            );
                            $this->response($response);
                        }
                        $current_balance = ($balance - $amount) / 100;
                        $amount = $amount / 100;

                        $transaction_data = array(
                            'player_id' => $player_id,
                            'game_id' => $game_id,
                            'wallet_account_id' => $wallet_account_id,
                            'type' => $type,
                            'prev_balance' => $prev_balance,
                            'current_balance' => $current_balance,
                            'amount' => $amount,
                            'date_added' => date('Y-m-d H:i:s'),
                            'provider_transaction_id' => $transactionId,
                            'round_id' => $roundId
                        );

                        $game_transaction_id = $this->games_model->add_transaction($transaction_data);
                        $history_data = array(
                            'transaction_type' => 'Bet',
                            'amount' => $amount,
                            'wallet_account_id' => $wallet_account_id,
                            'remark' => $gameReference . ' Entry',
                            'type' => 'Game Entry',
                            'game_title' => $gameReference,
                            'game_sub_type' => $gameReference,
                            'games_id' => $game_id,
                            'closing_balance' => $current_balance,
                            'created_date_time' => date('Y-m-d H:i:s')
                        );
                        $this->games_model->add_wallet_transaction_history($history_data);
                        $this->wallet_model->set_updated_real_chips_deposit($current_balance, $wallet_account_id);

                        $current_balance = $current_balance * 100;

                        $slab_id = $player_detail->slab_id;
                        $slabs = $this->games_model->get_all_slabs();
                        $min_points = 0;
                        $max_points = 0;
                        $wager_factor = 0;

                        foreach($slabs as $slab)
                        {
                            if($slab['id'] == $slab_id)
                            {
                                $min_points = $slab['min_points'];
                                $max_points = $slab['max_points'];
                                $wager_factor = $slab['wager_factor'];
                            }
                        }

                        $current_loyalty_points = $player_detail->loyalty_points;
                        $new_loyalty_points = $current_loyalty_points + $wager_factor * $amount;

                        if($new_loyalty_points > $max_points)
                        {
                            foreach($slabs as $slab)
                            {
                                if($slab['min_points'] <= $new_loyalty_points && $slab['max_points'] > $new_loyalty_points)
                                {
                                    $slab_id = $slab['id'];
                                }
                            }
                        }
                        $this->games_model->update_loyalty($wallet_account_id, $new_loyalty_points, $slab_id);
                        break;
                    case 'win':
                        $type = 'DEPOSIT';
                        $current_balance = ($balance + $amount) / 100;
                        $amount = $amount / 100;

                        $transaction_data = array(
                            'player_id' => $player_id,
                            'game_id' => $game_id,
                            'wallet_account_id' => $wallet_account_id,
                            'type' => $type,
                            'prev_balance' => $prev_balance,
                            'current_balance' => $current_balance,
                            'amount' => $amount,
                            'date_added' => date('Y-m-d H:i:s'),
                            'provider_transaction_id' => $transactionId,
                            'round_id' => $roundId
                        );

                        $game_transaction_id = $this->games_model->add_transaction($transaction_data);
                        $history_data = array(
                            'transaction_type' => 'Win',
                            'amount' => $amount,
                            'wallet_account_id' => $wallet_account_id,
                            'remark' => $gameReference . ' End',
                            'type' => 'Game End',
                            'game_title' => $gameReference,
                            'game_sub_type' => $gameReference,
                            'games_id' => $game_id,
                            'closing_balance' => $current_balance,
                            'created_date_time' => date('Y-m-d H:i:s')
                        );
                        $this->games_model->add_wallet_transaction_history($history_data);
                        $this->wallet_model->set_updated_real_chips_deposit($current_balance, $wallet_account_id);

                        $current_balance = $current_balance * 100;
                        break;
                }
                $transaction['action_id'] = $action['action_id'];
                $transaction['tx_id'] = $game_transaction_id;
                $response_transactions[] = $transaction;
                $balance = $current_balance;
            }

            $response['balance'] = $balance;
            $response['transactions'] = $response_transactions;

            /*$fh = fopen($filePath, 'a');
            fwrite($fh, "Hi Ai am called transaction response");
            fwrite($fh, json_encode($response));
            fclose($fh);*/
        }
        else
        {
            $response = array(
                'balance' => $balance
            );
        }

        $this->response($response);
    }

    function rollback_post()
    {
        /*$filePath = APPPATH . '/../uploads/wac.txt';
        $fh = fopen($filePath, 'a');
        fwrite($fh, "Hi Ai am called rollback");
        fwrite($fh, $_POST);
        fwrite($fh, $_GET);
        fwrite($fh, json_encode($this->post()));
        fclose($fh);*/

        $signature = $this->head('X-REQUEST-SIGN');
        $post_json = json_encode($this->post());
        $hash_string = hash_hmac("sha256", $post_json, $this->_auth_key);

        $player_id = $this->post('user_id');
        $gameReference = $this->post('game');

        $player_detail = $this->games_model->get_player_by_id($player_id);
        if($hash_string != $signature)
        {
            $response = array(
                'code' => 403,
                'message' => "Forbidden. (Request sign doesn't match)"
            );
            if($player_detail)
            {
                $balance = $player_detail->real_chips_deposit * 100;
                $response['balance'] = $balance;
            }
            $this->response($response);
        }
        if(!$player_detail)
        {
            $response = array(
                'code' => 101,
                'message' => "Player is invalid"
            );
            $this->response($response);
        }
        $balance = $player_detail->real_chips_deposit * 100;

        $game_detail = $this->games_model->get_game_by_reference($gameReference);
        $game_id = $game_detail->id;

        $log['game_id'] = $game_id;
        $log['player_id'] = $player_id;
        $game_token = $this->games_model->add_game_log($log);

        $response['game_id'] = $this->post('game_id');

        $actions = $this->post('actions');
        if($actions)
        {
            $response_transactions = array();
            foreach($actions as $action)
            {
                $transactionId = $action['action_id'];
                $roundId = $this->post('game_id') ? $this->post('game_id') : $transactionId;
                $wallet_account_id = $player_detail->wallet_account_id;
                $transaction = array();

                $providerTransactionId = $action['original_action_id'];
                $game_transaction = $this->games_model->get_game_transaction_by_provider_transaction_id($providerTransactionId);
                if($game_transaction)
                {
                    $game_transaction = $game_transaction[0];
                    $amount = $game_transaction['amount'] * 100;
                    $prev_balance = $balance / 100;
                    $current_balance = $balance;
                    switch($game_transaction['type'])
                    {
                        case 'DEPOSIT':
                            $type = 'DEPOSIT_ROLLBACK';
                            $current_balance = ($balance - $amount) / 100;
                            $amount = $amount / 100;

                            $transaction_data = array(
                                'player_id' => $player_id,
                                'game_id' => $game_id,
                                'wallet_account_id' => $wallet_account_id,
                                'type' => $type,
                                'prev_balance' => $prev_balance,
                                'current_balance' => $current_balance,
                                'amount' => $amount,
                                'date_added' => date('Y-m-d H:i:s'),
                                'provider_transaction_id' => $transactionId,
                                'round_id' => $roundId
                            );

                            $game_transaction_id = $this->games_model->add_transaction($transaction_data);
                            $history_data = array(
                                'transaction_type' => 'Win Rollback',
                                'amount' => $amount,
                                'wallet_account_id' => $wallet_account_id,
                                'remark' => $gameReference . ' Rollback',
                                'type' => 'Game Rollback',
                                'game_title' => $gameReference,
                                'game_sub_type' => $gameReference,
                                'games_id' => $game_id,
                                'closing_balance' => $current_balance,
                                'created_date_time' => date('Y-m-d H:i:s')
                            );
                            $this->games_model->add_wallet_transaction_history($history_data);
                            $this->wallet_model->set_updated_real_chips_deposit($current_balance, $wallet_account_id);

                            $current_balance = $current_balance * 100;
                            break;
                        case 'WITHDRAWAL':
                            $type = 'WITHDRAWAL_ROLLBACK';
                            $current_balance = ($balance + $amount) / 100;
                            $amount = $amount / 100;

                            $transaction_data = array(
                                'player_id' => $player_id,
                                'game_id' => $game_id,
                                'wallet_account_id' => $wallet_account_id,
                                'type' => $type,
                                'prev_balance' => $prev_balance,
                                'current_balance' => $current_balance,
                                'amount' => $amount,
                                'date_added' => date('Y-m-d H:i:s'),
                                'provider_transaction_id' => $transactionId,
                                'round_id' => $roundId
                            );

                            $game_transaction_id = $this->games_model->add_transaction($transaction_data);
                            $history_data = array(
                                'transaction_type' => 'Bet Rollback',
                                'amount' => $amount,
                                'wallet_account_id' => $wallet_account_id,
                                'remark' => $gameReference . ' Rollback',
                                'type' => 'Game Rollback',
                                'game_title' => $gameReference,
                                'game_sub_type' => $gameReference,
                                'games_id' => $game_id,
                                'closing_balance' => $current_balance,
                                'created_date_time' => date('Y-m-d H:i:s')
                            );
                            $this->games_model->add_wallet_transaction_history($history_data);
                            $this->wallet_model->set_updated_real_chips_deposit($current_balance, $wallet_account_id);

                            $current_balance = $current_balance * 100;
                            break;
                    }
                    $transaction['action_id'] = $action['action_id'];
                    $transaction['tx_id'] = $game_transaction_id;
                    $response_transactions[] = $transaction;
                    $balance = $current_balance;
                }
                else
                {
                    $transaction['action_id'] = $action['action_id'];
                    $transaction['tx_id'] = "";
                    $response_transactions[] = $transaction;
                }
            }
            $response['balance'] = $balance;
            $response['transactions'] = $response_transactions;

            /*$fh = fopen($filePath, 'a');
            fwrite($fh, "Hi Ai am called rollback transaction response");
            fwrite($fh, json_encode($response));
            fclose($fh);*/
        }
        else
        {
            $response['balance'] = $balance;
        }
        $this->response($response);
    }

    function freespins_post()
    {
        $filePath = APPPATH . '/../uploads/wac.txt';
        $fh = fopen($filePath, 'a');
        fwrite($fh, "Hi Ai am called freespins");
        fwrite($fh, json_encode($this->post()));
        fclose($fh);

        $signature = $this->head('X-REQUEST-SIGN');
        $post_json = json_encode($this->post());
        $hash_string = hash_hmac("sha256", $post_json, $this->_auth_key);

        $issue_id = $this->post('issue_id');
        $status = $this->post('status');
        $amount = $this->post('total_amount');

        $freespins_detail = $this->games_model->get_freespins_by_id($issue_id);
        $freespins_detail = $freespins_detail[0];
        $player_id = $freespins_detail['player_id'];

        $player_detail = $this->games_model->get_player_by_id($player_id);
        if($hash_string != $signature)
        {
            $response = array(
                'code' => 403,
                'message' => "Forbidden. (Request sign doesn't match)"
            );
            if($player_detail)
            {
                $balance = $player_detail->real_chips_deposit * 100;
                $response['balance'] = $balance;
            }
            $this->response($response);
        }
        if(!$player_detail)
        {
            $response = array(
                'code' => 101,
                'message' => "Player is invalid"
            );
            $this->response($response);
        }
        $balance = $player_detail->real_chips_deposit * 100;

        $prev_balance = $balance / 100;
        $current_balance = $balance;
        $type = 'DEPOSIT';
        $current_balance = ($balance + $amount) / 100;
        $amount = $amount / 100;
        $wallet_account_id = $player_detail->wallet_account_id;

        $transaction_data = array(
            'player_id' => $player_id,
            'game_id' => '',
            'wallet_account_id' => $wallet_account_id,
            'type' => $type,
            'prev_balance' => $prev_balance,
            'current_balance' => $current_balance,
            'amount' => $amount,
            'date_added' => date('Y-m-d H:i:s'),
            'provider_transaction_id' => '',
            'round_id' => $issue_id
        );

        $game_transaction_id = $this->games_model->add_transaction($transaction_data);
        $history_data = array(
            'transaction_type' => 'Win',
            'amount' => $amount,
            'wallet_account_id' => $wallet_account_id,
            'remark' => 'Freespins',
            'type' => 'Freespins',
            'game_title' => '',
            'game_sub_type' => '',
            'games_id' => '',
            'closing_balance' => $current_balance,
            'created_date_time' => date('Y-m-d H:i:s')
        );
        $this->games_model->add_wallet_transaction_history($history_data);
        $this->wallet_model->set_updated_real_chips_deposit($current_balance, $wallet_account_id);

        $current_balance = $current_balance * 100;
    }

    function freespins_get()
    {
        $issue_id = $this->get('id');
        $freespins_detail = $this->games_model->get_freespins_by_id($issue_id);
        $freespins_detail = $freespins_detail[0];

        $freespins_quantity = $freespins_detail['freespins_quantity'];
        $valid_until = $freespins_detail['to_time'];
        $bet_level = 1;
        $games_list = json_decode($freespins_detail['games_ids'], true);
        $all_games = $this->games_model->get_game_by_id($games_list);
        foreach($all_games as $game_detail)
        {
            $post_games[] = $game_detail['reference_code'];
        }

        $player_id = $this->player_login_model->get_logged_player_id();
        $player_detail = $this->games_model->get_player_by_id($player_id);

        $user_array = array(
            'id' => $player_detail->id,
            'email' => $player_detail->email,
            'nickname' => $player_detail->username
        );
        $currency = $player_detail->currency;

        $session_array = array(
            'casino_id' => $this->_casino_id,
            'issue_id' => $issue_id,
            'currency' => $currency,
            'games' => $post_games,
            'freespins_quantity' => $freespins_quantity,
            'bet_level' => $bet_level,
            'valid_until' => $valid_until,
            'user' => $user_array
        );

        $session_obj = json_encode($session_array);

        $hash_string = hash_hmac("sha256", $session_obj, $this->_auth_key);

        $headers = array(
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "X-REQUEST-SIGN: $hash_string"
        );

        $curlData['url'] = $this->_base_url . '/freespins/issue';
        $curlData['postData'] = $session_obj;
        $curlData['headers'] = $headers;

        $response = curlPost($curlData);
        echo "response - " . $response;
        /*$decode_response = json_decode($response, true);
        echo '<pre>';
        print_r($decode_response);*/
    }
}
