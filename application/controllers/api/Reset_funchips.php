<?php

header('Content-type: application/json');

class Reset_funchips extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function index_post() {
        $players_id = $this->player_login_model->get_logged_player_id();
        $response = $this->wallet_model->reset_funchips($players_id);
        $player_details = $this->player_login_model->get_player_details($players_id);
        $wallet_info = $this->wallet_model->get_wallet($player_details->wallet_account_id);
        if ($response) {
            $arr = [
                "status" => "valid",
                "title"=>"Funchips updated",
                "message"=>"Funchips updated successfully",
                "data" => [
                    "fun_chips"=>$wallet_info->fun_chips
                ]
            ];
        } else {
            $arr = [
                "status" => "invalid",
                "title"=>"Oops",
                "message"=>"You cannot reload beyond ".MAX_FUN_CHIPS." practice chips",
                "data" =>  [
                    "fun_chips"=>$wallet_info->fun_chips
                ]
            ];
        }
        $this->response($arr);
    }
}