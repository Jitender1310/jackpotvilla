<?php

    class Rps_transactions extends REST_Controller {

        private $title = 'RPS Transaction History';
        private $model_name = 'rps_transactions_model';

        function __construct() {
            parent::__construct();
        }

        function index_get() {
            $player_id = $this->player_login_model->get_logged_player_id();

            $filters = [];

            if ($this->input->get_post("txn_type") && !empty($this->input->get_post("txn_type"))) {
                $filters["txn_type"] = $this->input->get_post("txn_type");
            }

            /*
              if($this->input->get_post("from_date")){
              $filters["from_date"] = $this->input->get_post("from_date");
              }else{
              $filters["from_date"] = "";
              }

              if($this->input->get_post("to_date")){
              $filters["to_date"] = $this->input->get_post("to_date");
              }else{
              $filters["to_date"] = "";
              }
             */

            $total_results = $this->{$this->model_name}->get_rps_transactions_history($filters, $player_id);

            $limit = $this->input->get_post("limit") ? $this->input->get_post("limit") : ORDERS_PER_PAGE;
            $_GET['page'] = $this->input->get_post("page");
            $start = $this->input->get_post("page") > 1 ? ($this->input->get_post("page") - 1) * $limit : 0;
            $filters["limit"] = $limit;
            $filters["start"] = $start;

            $result["data"] = $this->{$this->model_name}->get_rps_transactions_history($filters, $player_id);
            $result["total_results_found"] = $total_results;

            $result["total_pages"] = ceil($total_results / $limit);

            $pagination = my_pagination("rps_transactions", $limit, $total_results, true);
            $pagination['pagination'] = str_replace("&amp;", "&", $pagination['pagination']);
            $cur_page = ($pagination['pagination_helper']->cur_page != 0) ? ($pagination['pagination_helper']->cur_page - 1) : $pagination['pagination_helper']->cur_page;
            $pagination['initial_id'] = (($cur_page) * $pagination['pagination_helper']->per_page) + 1;

            $arr = [
                "status" => "valid",
                "message" => $this->title,
                "data" => $result,
                "pagination" => $pagination
            ];
            $arr = json_encode($arr);
            $arr = json_decode(str_replace('null', '""', $arr));
            $this->response($arr);
        }

    }
    