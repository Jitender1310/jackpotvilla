<?php

header('Content-type: application/json');

class Reset_password extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function index_post() {

        $this->form_validation->set_rules("password", "Password", "required", array(
            'required' => 'Password cannot be empty'
        ));
        $this->form_validation->set_rules("cnf_password", "Confirm Password", "required", array(
            'required' => 'Password cannot be empty'
        ));
        $this->form_validation->set_rules("forgot_password_verfication_code", "Confirm Password", "required", array(
            'required' => 'Verfication code is required'
        ));

        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "password" => form_error("password"),
                "cnf_password" => form_error("cnf_password"),
                "forgot_password_verfication_code" => form_error("forgot_password_verfication_code")
            ];

            $single_line_message = "";

            if (form_error("password")) {
                $single_line_message = form_error("password");
            }
            if (form_error("cnf_password") && $single_line_message == "") {
                $single_line_message = form_error("cnf_password");
            }
            if (form_error("forgot_password_verfication_code") && $single_line_message == "") {
                $single_line_message = form_error("forgot_password_verfication_code");
            }

            $arr = [
                "status" => "invalid_form",
                "title" => "Please fill all mandatory fields",
                "message" => $single_line_message,
                "data" => $errors
            ];
            $this->response($arr);
            die;
        } else {

            $players_id = $this->player_reset_password_model->get_players_id_by_forgot_password_code($this->post('forgot_password_verfication_code'));

            if (!$players_id) {
                $errors = [
                    "username" => "User does not exist!"
                ];
                $arr = [
                    "status" => "invalid",
                    "title" => "Oops",
                    "message" => "Sorry, This user does not exist!",
                    "data" => $errors
                ];
                $this->response($arr);
                die;
            }

            $response = $this->player_reset_password_model->reset_password_by_forgotten_password_verification_link($this->post('forgot_password_verfication_code'), $this->post("password"), $players_id);

            if ($response) {
                $arr = [
                    "status" => "valid",
                    "title" => "Success",
                    "message" => "Password updated successfully",
                    "data" => [
                        "message" => "Password updated successfully"
                    ]
                ];
            } else {
                $arr = [
                    "status" => "invalid",
                    "title" => "Sorry",
                    "message" => "Invalid Username",
                    "data" => [
                        "message" => "Invalid Username"
                    ]
                ];
            }
        }
        $this->response($arr);
    }

}
