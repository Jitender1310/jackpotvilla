<?php

header('Content-type: application/json');

class Redeem_rps extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function index_post() {
        $player_id = $this->player_login_model->get_logged_player_id();
        
        $this->form_validation->set_rules("redeem_points", "Redeem Points", "required", array(
            'required' => 'Please select redeem points'
        ));

        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "redeem_points" => form_error("redeem_points")
            ];
            $arr = [
                "status" => "invalid_form",
                "title"=>"Required",
                "message" => "Please select redeem points",
                "data" => $errors
            ];
            $this->response($arr);
            die;
        } else {
            $redeem_points = (int)$this->post("redeem_points");
            $response = $this->rps_model->redeem_points($redeem_points, $player_id);
            if($response==="INSUFFICIENT_FUNDS"){
                $arr = [
                    "status" => "invalid",
                    "title"=>"Insufficient rps points",
                    "message" => "Sorry, you don't have sufficient rps points to redeem",
                    "data" => [
                        "message" => "Sorry, you don't have sufficient rps points to redeem"
                    ],
                    "debug_point"=>1
                ];
            }else if($response==="INVALID_RPS_REEDEM_VALUE"){
                $arr = [
                    "status" => "invalid",
                    "title"=>"Invalid Rps Redemtion value",
                    "message" => "Sorry, Invalid Rps Redemtion value",
                    "data" => [
                        "message" => "Sorry, Invalid Rps Redemtion value"
                    ],
                    "debug_point"=>2
                ];
            }else if($response){
                $arr = [
                    "status" => "valid",
                    "title"=>"Rps redeemed",
                    "message" => "Success, Rps redeemed and credited to deposit wallet",
                    "data" => [
                        "message" => "Success, Rps redeemed and credited to deposit wallet"
                    ],
                    "debug_point"=>3
                ];
            }else{
               $arr = [
                    "status" => "invalid",
                    "title"=>"Insufficient rps points",
                    "message" => "Sorry, you don't have sufficient rps points to redeem",
                    "data" => [
                        "message" => "Sorry, you don't have sufficient rps points to redeem"
                    ],
                    "debug_point"=>4
                ];
            }
        }
        
        $arr = json_encode($arr);
        $arr = json_decode(str_replace('null', '""', $arr));
        $this->response($arr);
    }

}
