<?php

header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');

class Time_streamer extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    function index() {
        $str = "";
        
         if ($this->input->get("c_s") == "Created") {
            $arr = [
                "action_affecting_at" => "Reg Start Timer",
                "date_time" => count_down_timer($this->input->get("r_s_d_t"))
            ];
            $str = json_encode($arr);
            
        }else if ($this->input->get("c_s") == "Registration_Start") {
            $arr = [
                "action_affecting_at" => "Reg Close Timer",
                "date_time" => count_down_timer($this->input->get("r_c_d_t"))
            ];
            $str = json_encode($arr);
            
        }
        echo "data: {$str}\n\n";
        flush();
    }

}
