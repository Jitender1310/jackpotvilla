<?php

header('Content-type: application/json');

class Offer_notifications extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("offer_notifications_model");
    }

    function index_get() {
        $response = $this->offer_notifications_model->get_available_notifications();
        if ($response) {
            $arr = [
                "status" => "valid",
                "title"=>"Available notifications List",
                "message" => "Available notifications List",
                "data" => [
                    "list" => $response
                ]
            ];
        } else {
            $arr = [
                "status" => "invalid",
                "title"=>"Error",
                "message" => "Some error occured",
                "data" => [
                    "message" => "Some error occured"
                ]
            ];
        }
        $this->response($arr);
    }
    
    function index_post(){
        $this->index_get();
    }

}
