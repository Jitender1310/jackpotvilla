<?php

    header('Content-type: application/json');

    class Faqs extends REST_Controller {

        function __construct() {
            parent::__construct();
            $this->load->model("faqs_model");
        }

        function index_get() {
            $response = $this->faqs_model->get_categories();
            foreach ($response as $item) {
                $item->icon = BACK_END_SERVER_URL . 'uploads/faq_icons/' . $item->icon;
                $item->faq = $this->faqs_model->get($item->id);
            }
            if ($response) {
                $arr = [
                    "status" => "valid",
                    "title" => "Faqs List",
                    "message" => "Faqs List",
                    "data" => $response
                ];
            } else {
                $arr = [
                    "status" => "invalid",
                    "title" => "Error",
                    "message" => "Some error occured",
                    "data" => [
                        "message" => "Some error occured"
                    ]
                ];
            }
            $this->response($arr);
        }

        function index_post() {
            $this->index_get();
        }

    }
    