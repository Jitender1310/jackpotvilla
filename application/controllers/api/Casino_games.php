<?php

header('Content-type: application/json');

class Casino_games extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function all_get() {
        $all_games = $this->games_model->get_all_games();
        $final_list = [];
        foreach($all_games as $game)
        {
            $final_list[$game['type']][] = $game;
        }
        $arr = [
            "status" => "valid",
            "message" => "Countries Successful",
            "data" => $final_list
        ];
        $arr = json_encode($arr);
        $arr = json_decode(str_replace('null', '""', $arr));
        $this->response($arr);
    }

    function gameUrl_get(){
        $game_id = $this->get('id');
        $access_token = $this->head('token');
        $player_detail = $this->games_model->get_player_by_access_token($access_token);
        $game_url = '';
        if($player_detail){
            $player_id = $player_detail->id;
            $log['game_id'] = $game_id;
            $log['player_id'] = $player_id;
            $game_token = $this->games_model->add_game_log($log);

            $game_detail = $this->games_model->get_game_by_id($game_id);
            $game_url = $game_detail->url;
            $external_id = $game_detail->reference_code;
            $game_url = str_replace('<TOKEN>', $game_token, $game_url);
            $game_url = str_replace('<LANG>', 'en', $game_url);
            $game_url = str_replace('<EXTERNAL_ID>', $external_id, $game_url);
        }
        $arr = [
            "status" => "valid",
            "message" => "Countries Successful",
            "data" => array(
                'game_url' => $game_url
            )
        ];
        $arr = json_encode($arr);
        $arr = json_decode(str_replace('null', '""', $arr));
        $this->response($arr);
    }
}
