<?php

header('Content-type: application/json');

class Preferences extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function index_get() {
        $player_id = $this->player_login_model->get_logged_player_id();
        $player_preferences = $this->player_preferences_model->get_preferences($player_id);
        $arr = [
            "status" => "valid",
            "data" => $player_preferences
        ];
        $arr = json_encode($arr);
        $arr = json_decode(str_replace('null', '""', $arr));
        $this->response($arr);
    }

    function update_post() {
        $player_id = $this->player_login_model->get_logged_player_id();
        $data = [
            "contact_by_sms" => $this->post("contact_by_sms") ? 1 : 0,
            "contact_by_phone" => $this->post("contact_by_phone") ? 1 : 0,
            "contact_by_email" => $this->post("contact_by_email") ? 1 : 0,
            "speak_language" => $this->post("speak_language") ? $this->post("speak_language") : "",
            "sms_subscriptions" => $this->post("sms_subscriptions") ? 1 : 0,
            "newsletter_subscriptions" => $this->post("newsletter_subscriptions") ? 1 : 0
        ];
        $response = $this->player_preferences_model->update_preferences($data, $player_id);
        if ($response) {
            $arr = array(
                'status' => "valid",
                "title" => "Preferences Updated",
                "message" => "Preferences updated successfully"
            );
            $this->response($arr);
        } else {
            $arr = [
                "status" => "invalid",
                "title" => "Preferences not updated",
                "message" => "Preferences not updated",
                "data" => ""
            ];
            $this->response($arr);
        }
    }

}
