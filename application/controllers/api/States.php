<?php

header('Content-type: application/json');

class States extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function index_get() {
        $states = $this->states_model->get_states();
        $arr = [
            "status" => "valid",
            "message"=>"States List",
            "data" => [
                "states"=>$states
            ]
        ];
        $this->response($arr);
    }

}
