<?php

header('Content-type: application/json');

class Default_avatars extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("default_avatars_model");
    }

    function index_get() {
        $response = $this->default_avatars_model->get_available_avatars();
        if ($response) {
            $arr = [
                "status" => "valid",
                "title"=>"Available avatars List",
                "message" => "Available avatars List",
                "data" => [
                    "list" => $response
                ]
            ];
        } else {
            $arr = [
                "status" => "invalid",
                "title"=>"Error",
                "message" => "Some error occured",
                "data" => [
                    "message" => "Some error occured"
                ]
            ];
        }
        $this->response($arr);
    }

}
