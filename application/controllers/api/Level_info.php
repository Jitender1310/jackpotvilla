<?php

header('Content-type: application/json');

class Level_info extends REST_Controller {

    function __construct() {
        parent::__construct();
    }
    
    function index_post(){
        $this->index_get();
    }

    function index_get() {
        $player_id = $this->player_login_model->get_logged_player_id();
        $level_details = $this->club_types_model->get_current_club_info($player_id);
        $arr = [
            "status" => "valid",
            "title" => "Expertise Level Info details",
            "message" => "Expertise Level Info details",
            "data" => [
                "level_info" =>$level_details
            ]
        ];
        $arr = json_encode($arr);
        $arr = json_decode(str_replace('null', '""', $arr));
        $this->response($arr);
    }
    
}