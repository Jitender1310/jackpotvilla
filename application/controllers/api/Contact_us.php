<?php

class Contact_us extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->model_name = 'Contact_us_model';
        $this->load->model($this->model_name);
        $this->load->library('simple_captcha');
    }

    function captcha_get(){
        $this->simple_captcha->draw_captcha('contact_us');
        die;
    }
    
    function index_post(){
        //$player_id = $this->player_login_model->get_logged_player_id();
        
       //print_r($this->input->post()); die;
        
        $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email', array(
            'required' => 'Email Address is required field.',
            'valid_email' => 'Email Address is not valid.'
        ));
        
        $this->form_validation->set_rules('query', 'Query', 'required', array(
            'required' => 'Query is a required field.'
        ));
        
        /*$this->form_validation->set_rules('captcha', 'Captcha Code','required|callback_captcha_val', array(
            'required'=>'Captcha Code is required.',
            'captcha_val'=>'Captcha Code is Invalid.'
        ));*/
        //using to google reCAPTCHA
        $google_captcha_flage = TRUE;
        $google_captcha_message = "";
        $Response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LffjKoUAAAAAHaviecmnV711Yyo6us4QzKB3KAF&response={$this->input->post('captcha')}");
        $Return = json_decode($Response);
        if(!($Return->success)){
            $google_captcha_flage = FALSE; 
            $google_captcha_message = "Captcha Address is not valid";
        }
        
        $this->form_validation->set_rules('message', 'Message', 'required', array(
            'required' => 'Message is a required field.'
        ));
        
        if( $this->form_validation->run() ==  FALSE || $google_captcha_flage == FALSE) {
            $arr = [
                "status" => "invalid_form",
                "title" => "Please fill mandatory fields",
                "data" => [
                    'email' => form_error('email'),
                    'query' => form_error('query'),
                    'message' => form_error('message'),
                    'username' => form_error('username'),
                    'captcha' => $google_captcha_message
                ]
            ];
            $this->response($arr);
        } else {
            $data = array(
                'email' => $this->input->post('email'),
                'query' => $this->input->post('query'),
                'message' => $this->input->post('message'),
                'username' => $this->input->post('username')
            );
            
            if( $this->{$this->model_name}->save($data) ) {
                $arr = [
                    "status" => "valid",
                    "title"=>"Contact List",
                    "message" => "Submitted Successfully",
                    "data" => [
                        "message" => 'Contact form has been Submitted Successfully.'
                    ]
                ];
                
                $this->{$this->model_name}->send_notification_email($data);
            } else {
                $arr = [
                    "status" => "invalid",
                    "title"=>"Error",
                    "message" => "Some error occured",
                    "data" => [
                        "message" => "Some error occured"
                    ]
                ];
            }
            
             $this->response($arr);
        }
    }

    function captcha_val() {
        $captcha_input = $this->input->post('captcha', TRUE);
        $captcha_text = $this->simple_captcha->get_captcha_text('contact_us');

        if ($captcha_input == $captcha_text) {
            return true;
        } else {
            $this->form_validation->set_message('captcha', 'Invalid Captcha code');
            return false;
        }
    }
}
