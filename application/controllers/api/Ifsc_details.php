<?php

header('Content-type: application/json');

class Ifsc_details extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function index_post() {
        $this->index_get();
    }

    function index_get() {

        $ifsc_code = $this->input->get_post("ifsc_code");

        if (strlen(trim($ifsc_code)) != 11) {
            $arr = [
                "status" => "invalid",
                "title" => "Ifsc details not found",
                "message" => "Ifsc details not found",
                "data" => [
                    "ifsc_detais" => []
                ]
            ];
            $this->response($arr);
            die;
        }


        $URL = "https://ifsc.razorpay.com/" . $ifsc_code;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $URL);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($code == 404) {
            $arr = [
                "status" => "invalid",
                "title" => "Ifsc details not found",
                "message" => "Ifsc details not found",
                "data" => [
                    "ifsc_detais" => []
                ]
            ];
        } else if ($code == 200) {
            if ($response != "") {
                $arr = [
                    "status" => "valid",
                    "title" => "Ifsc details",
                    "message" => "Ifsc details",
                    "data" => [
                        "ifsc_detais" => json_decode($response)
                    ]
                ];
            }
        } else {
            $arr = [
                "status" => "invalid",
                "title" => "Ifsc details not found",
                "message" => "Ifsc details not found",
                "data" => [
                    "ifsc_detais" => []
                ]
            ];
        }
        $this->response($arr);
    }

}
