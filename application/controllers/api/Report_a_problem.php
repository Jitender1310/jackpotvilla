<?php

header('Content-type: application/json');

class Report_a_problem extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("report_problem_model");
    }

    function index_post() {
        $player_id = $this->player_login_model->get_logged_player_id();
        $this->form_validation->set_rules("name", "Name", "required", array(
            'required' => 'Name cannot be empty'
        ));

        $this->form_validation->set_rules("email", "Email", "required", array(
            'required' => 'Email cannot be empty'
        ));

        $this->form_validation->set_rules("type_of_issue", "Type of Issue", "required", array(
            'required' => 'Please choose issue type'
        ));

        $this->form_validation->set_rules("message", "Message", "required", array(
            'required' => 'Please enter little desciption'
        ));
        
        $this->form_validation->set_rules("version_code", "Version", "required", array(
            'required' => 'Version Code is missing'
        ));

        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "name" => form_error("name"),
                "email" => form_error("email"),
                "type_of_issue" => form_error("type_of_issue"),
                "message" => form_error("message"),
                "version_code" => form_error("version_code")
            ];
            
            $single_line_message = "";
            
            if(form_error("name")){
                $single_line_message = form_error("name");
            }
            if(form_error("email") && $single_line_message==""){
                $single_line_message = form_error("email");
            }
            if(form_error("type_of_issue") && $single_line_message==""){
                $single_line_message = form_error("type_of_issue");
            }
            if(form_error("message") && $single_line_message==""){
                $single_line_message = form_error("message");
            }
            if(form_error("version_code") && $single_line_message==""){
                $single_line_message = form_error("version_code");
            }
            
            $arr = [
                "status" => "invalid_form",
                "message"=>  $single_line_message,
                "data" => $errors
            ];
            $this->response($arr);
        } else {
            $data = [
                "name" => $this->post("name"),
                "email" => $this->post("email"),
                "type_of_issue" => $this->post("type_of_issue"),
                "message" => $this->post("message"),
                "version_code" => $this->post("version_code")
            ];
            $response = $this->report_problem_model->create($data, $player_id);
            if ($response) {
                $arr = array(
                    'status' => "valid",
                    "title" => "Submitted",
                    "message" => "Thanks for your feedback, we will look into it",
                    "data" =>  [
                        "message"=> "Thanks for your feedback, we will look into it"
                    ]
                );
                $this->response($arr);
            } else {
                $arr = [
                    "status" => "invalid",
                    "title" => "Not submitted",
                    "message" => "Submission error",
                    "data" =>  [
                        "message"=> "Submission error please try again"
                    ]
                ];
                $this->response($arr);
            }
        }
    }
}