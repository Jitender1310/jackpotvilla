<?php

class Social_login extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function index_post() {
        log_message("error", print_r($_POST, true));
        $email = $this->input->get_post("email");

        /* if ($email == "") {
          $arr = array(
          'status' => "invalid",
          "title" => "Invalid login",
          "message" => "You have logged in successfully",
          "data" => [
          "invalid_login" => "Invalid Login"
          ]
          );
          $this->response($arr);
          die;
          } */

        if ($this->input->get_post("facebook_account_id")) {
            $type = 'facebook';
            $account_id = $this->input->get_post("facebook_account_id");
        } else if ($this->input->get_post("google_account_id")) {
            $type = 'google';
            $account_id = $this->input->get_post("google_account_id");
        }

        $access_token = $this->player_login_model->is_email_existed($email);
        $social_based_access_token = $this->player_login_model->is_social_account_existed($account_id, $type);


        if ($access_token) {
            log_message("error", "##@1@##");  
            if ($this->player_login_model->check_for_facebook_account_id($access_token, $account_id, $type)) {
                log_message("error", "##@@@@1@@@@##");
                $player_details = $this->player_login_model->get_player_details($access_token);
                $this->player_login_model->create_login_session($player_details);
                $arr = array(
                    'status' => "valid",
                    "title" => "Login successful",
                    "message" => "You have logged in successfully",
                    "data" => $this->player_profile_model->get_profile_information($access_token)
                );
                log_message("error", "Queryy::L ".$this->db->last_query());
                $this->response($arr);
                die;
            }
        } else if ($social_based_access_token) {
            log_message("error", "##@2@##"); 
            if ($this->player_login_model->check_for_facebook_account_id($access_token, $account_id, $type)) {
                $player_details = $this->player_login_model->get_player_details($access_token);
                $this->player_login_model->create_login_session($player_details);
                $arr = array(
                    'status' => "valid",
                    "title" => "Login successful",
                    "message" => "You have logged in successfully",
                    "data" => $this->player_profile_model->get_profile_information($access_token)
                );
                $this->response($arr);
                die;
            }
        } else if ($account_id) {
            log_message("error", "##@3@##"); 
            $username = explode("@", $this->post("email"));
            $access_token = $this->player_login_model->is_username_existed($username[0]);

            if ($access_token) {
                $new_username = $this->player_login_model->generate_username($username[0]);
                $username[0] = $new_username;
            }

            $data = array(
                "username" => trim($username[0]),
                "email" => trim($this->post("email")),
                "password" => generateRandomString(6),
                "firstname" => $this->post("firstname"),
                "lastname" => $this->post("lastname"),
                "device_type" => $this->post("device_type") ? $this->post("device_type") : "",
                "device_token" => $this->post("device_token") ? $this->post("device_token") : "",
                "referred_by_code" => $this->post("referred_by_code") ? $this->post("referred_by_code") : "",
                "mobile" => $this->post("mobile") ?: ''
            );

            if ($type === 'facebook') {
                $data["facebook_account_id"] = $this->post("facebook_account_id");
            } else if ($type === 'google') {
                $data["google_account_id"] = $this->post("google_account_id");
            }
            $response = $this->player_register_model->register($data);
            if ($response) {
                $arr = array(
                    'status' => "valid",
                    "title" => "Registration successful",
                    "message" => "Please check your mail for details",
                    "data" => $this->player_profile_model->get_profile_information($response)
                );
                $this->response($arr);
            }
        }else{
            log_message("error", "##@4@##"); 
        }
            log_message("error", "##@5@##"); 
    }

}
