<?php

header('Content-type: application/json');

class End_game extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function index_post() {

        $this->form_validation->set_rules("games_id", "Game Id", "required", array(
            'required' => 'Game id is required'
        ));

        $this->form_validation->set_rules("rooms_ref_id", "Room Ref id", "required", array(
            'required' => 'Room id is required'
        ));

        $this->form_validation->set_rules("player_access_tokens", "Player access tokens", "required", array(
            'required' => 'Player access tokens is required'
        ));

        $this->form_validation->set_rules("game_ref_id", "Game Ref id", "required", array(
            'required' => 'Game ref id is required'
        ));

        $this->form_validation->set_rules("player_access_tokens_with_points_joinings", "Player Points", "required", array(
            'required' => 'Player points is required'
        ));

        $this->form_validation->set_rules("bid_amount", "Bid amount", "required", array(
            'required' => 'Bid amount is required'
        ));

        $this->form_validation->set_rules("split_type", "Split Type", "required", array(
            'required' => 'Split Type is required'
        ));

        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "games_id" => form_error("games_id"),
                "rooms_ref_id" => form_error("rooms_ref_id"),
                "player_access_tokens" => form_error("player_access_tokens"),
                "player_access_tokens_with_points_joinings" => form_error("player_access_tokens_with_points_joinings"),
                "game_ref_id" => form_error("game_ref_id"),
                "bid_amount" => form_error("bid_amount"),
                "split_type" => form_error("split_type"),
            ];

            $single_line_message = "";

            if (form_error("games_id")) {
                $single_line_message = form_error("games_id");
            }
            if (form_error("rooms_ref_id") && $single_line_message == "") {
                $single_line_message = form_error("rooms_ref_id");
            }
            if (form_error("player_access_tokens") && $single_line_message == "") {
                $single_line_message = form_error("player_access_tokens");
            }
            if (form_error("game_ref_id") && $single_line_message == "") {
                $single_line_message = form_error("game_ref_id");
            }
            if (form_error("player_access_tokens_with_points_joinings") && $single_line_message == "") {
                $single_line_message = form_error("player_access_tokens_with_points_joinings");
            }

            if (form_error("bid_amount") && $single_line_message == "") {
                $single_line_message = form_error("bid_amount");
            }

            if (form_error("split_type") && $single_line_message == "") {
                $single_line_message = form_error("split_type");
            }

            $arr = [
                "status" => "invalid_form",
                "message" => $single_line_message,
                "data" => $errors
            ];
            $this->response($arr);
            die;
        } else {
            $player_access_tokens = $this->post("player_access_tokens");
            $rooms_ref_id = $this->post("rooms_ref_id");
            $games_id = $this->post("games_id");
            $game_ref_id = $this->post("game_ref_id");
            $player_access_tokens_with_points_joinings = $this->post("player_access_tokens_with_points_joinings");
            $bid_amount = $this->post("bid_amount");
            $split_type = $this->post("split_type"); //Manual, Auto, None
            log_message("error", print_r($_POST, true));

            $response = $this->game_end_model->finish_game($player_access_tokens, $rooms_ref_id, $games_id, $game_ref_id, $player_access_tokens_with_points_joinings, $bid_amount, $split_type);

            if ($response === "INVALID_ROOM") {
                $arr = [
                    "status" => "invalid",
                    "title" => "Room Not found",
                    "message" => "Room not found",
                    "data" => [
                        "message" => "Your not yet joined"
                    ],
                    "debug_point" => 1
                ];
            } else if ($response === "INSUFFICIENT_PLAYERS") {
                $arr = [
                    "status" => "invalid",
                    "title" => "Insufficient Players",
                    "message" => "Minimum 2 players are required to start game",
                    "data" => [
                        "message" => "Minimum 2 players are required to start game"
                    ],
                    "debug_point" => 2
                ];
            } else if ($response) {
                $arr = [
                    "status" => "valid",
                    "title" => "Completed",
                    "message" => "Game completed",
                    "data" => [
                        "report" => $response
                    ]
                ];
            } else {
                $arr = [
                    "status" => "invalid",
                    "title" => "Error",
                    "message" => "Some error occured",
                    "data" => [
                        "message" => "Some error occured"
                    ],
                    "debug_point" => 3,
                ];
            }
        }
        $this->response($arr);
    }

}
