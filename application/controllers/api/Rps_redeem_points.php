<?php

    header('Content-type: application/json');

    class Rps_redeem_points extends REST_Controller {

        function __construct() {
            parent::__construct();
            $this->load->model('club_types_model');
        }

        function index_post() {
            $this->index_get();
        }

        function index_get() {
            $images = $this->rps_model->get_rps_images();
            $player_id = $this->player_login_model->get_logged_player_id();
            $level_details = $this->club_types_model->get_current_club_info($player_id);
            $arr = [
                "status" => "valid",
                "message" => "Rps Redeemable points List",
                "data" => [
                    "states" => $images,
                    "level_info" => $level_details,
                ]
            ];
            $this->response($arr);
        }

    }
    