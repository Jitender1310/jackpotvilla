<?php

header('Content-type: application/json');

class Playnow_token extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function index_get() {
        $player_id = $this->player_login_model->get_logged_player_id();

        $player_details = $this->player_profile_model->get_profile_information($player_id);
        $play_token = $this->player_profile_model->get_play_now_token($player_id);

        $arr = [
            "status" => "valid",
            "data" => [
                "play_token" =>$play_token,
                "access_token" =>$player_details->access_token,
                "rand_string" => time().  generateFancyPassword()
            ]
        ];

        $this->response($arr);
    }

}
