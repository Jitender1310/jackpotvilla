<?php

header('Content-type: application/json');

class Bonus_coupons extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("bonus_model");
    }

    function index_get() {
        $player_id = $this->player_login_model->get_logged_player_id();

        $response = $this->bonus_model->get_available_coupons();
        $welcome_coupon_used = $this->player_login_model->get_player_details()->welcome_coupon_used;

        $welcome_coupon_array = array(
            "id" => "0",
            "coupon_code" => WELCOME_BONUS_CODE,
            "bonus_type" => "Welcome Bonus",
            "title" => WELCOME_BONUS_CODE,
            "description" => "Get Welcome Bonus on your first transaction",
            "from_date_time" => "",
            "to_date_time" => "",
            "terms_conditions" => ""
        );

        if ($response) {

            if ($welcome_coupon_used == 'NO') {
                array_unshift($response, $welcome_coupon_array);
            }

            $arr = [
                "status" => "valid",
                "title" => "Bots List",
                "message" => "Available Bots List",
                "data" => [
                    "list" => $response
                ]
            ];
        } else {
            $arr = [
                "status" => "invalid",
                "title" => "Not available",
                "message" => "No coupons are available",
                "data" => [
                    "message" => "No coupons are available"
                ]
            ];
        }
        $this->response($arr);
    }

}
