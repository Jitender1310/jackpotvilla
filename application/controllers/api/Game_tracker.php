<?php

header('Content-type: application/json');

class Game_tracker extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function index_post() {
        $this->index_get();
    }

    function index_get() {
        $player_id = $this->player_login_model->get_logged_player_id();
        //$game_tracker_info = $this->game_tracker_model->get_my_game_tracker($player_id);
        $game_tracker_info = $this->game_tracker_model->get_my_game_tracker($player_id);
        $headers = $this->game_tracker_model->get_headers();
        $arr = [
            "status" => "valid",
            "title" => "My Grame Tracker info",
            "message" => "My Grame Tracker info",
            "data" => [
                "game_tracker_info" =>[
                    "headers" => $headers,
                    "grid_info" => $game_tracker_info
                ]
            ]
        ];
        $arr = json_encode($arr);
        $arr = json_decode(str_replace('null', '""', $arr));
        $this->response($arr);
    }

}
