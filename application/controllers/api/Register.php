<?php

class Register extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function index_post() {

        log_message("error", print_r($_POST, true));
        $this->form_validation->set_rules("username", "Username", "required|min_length[5]|max_length[12]|is_unique[players.username]", array(
            'required' => 'Username cannot be empty',
            'is_unique' => 'This %s already exists.'
        ));
        $this->form_validation->set_rules("password", "Password", "required|min_length[5]", array(
            'required' => 'Password cannot be empty',
            "min_length" => "Password should have at least 5 characters"
        ));
        $this->form_validation->set_rules("email", "Email", "required|valid_email|is_unique[players.email]", array(
            'required' => 'Email Id cannot be empty',
            'is_unique' => 'Email already exists.'
        ));

        $this->form_validation->set_rules("mobile", "Mobile", "required|min_length[10]|max_length[10]|is_natural|is_unique[players.mobile]", array(
            'required' => 'Mobile number cannot be empty',
            'is_unique' => 'Mobile already exists.',
            "is_natural" => "Please enter valid mobile number"
        ));

        $this->form_validation->set_rules("date_of_birth", "Date of Birth", "required|callback_date_of_birth_check", array(
            'required' => 'Age Verification is Required'
        ));

        $this->form_validation->set_rules("country", "Country", "required", array(
            'required' => 'Country cannot be empty'
        ));

        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "username" => form_error("username"),
                "password" => form_error("password"),
                "email" => form_error("email"),
                "mobile" => form_error("mobile"),
                "referred_by_code" => "",
                "device_type" => "",
                "device_token" => "",
                "date_of_birth" => form_error("date_of_birth"),
                "country" => form_error("country")
            ];

            $single_line_message = "";

            if (form_error("username")) {
                $single_line_message = form_error("username");
            }
            if (form_error("password") && $single_line_message == "") {
                $single_line_message = form_error("password");
            }
            if (form_error("email") && $single_line_message == "") {
                $single_line_message = form_error("email");
            }
            if (form_error("mobile") && $single_line_message == "") {
                $single_line_message = form_error("mobile");
            }
            if (form_error("date_of_birth") && $single_line_message == "") {
                $single_line_message = form_error("date_of_birth");
            }
            if (form_error("country") && $single_line_message == "") {
                $single_line_message = form_error("country");
            }

            $arr = [
                "status" => "invalid_form",
                "message" => $single_line_message,
                "data" => $errors
            ];
            $this->response($arr);
        } else {

            if ($this->input->post("referred_by_code") != "") {
                $referral_code_response = $this->player_register_model->validate_referral_code($this->post("referred_by_code"));
                if ($referral_code_response == false) {
                    $errors = [
                        "username" => "",
                        "password" => "",
                        "email" => "",
                        "mobile" => "",
                        "referred_by_code" => "This referral Code is invalid"
                    ];
                    $arr = [
                        "status" => "invalid_form",
                        "message" => "This referral Code is invalid",
                        "data" => $errors
                    ];
                    $this->response($arr);
                    die;
                }
            }

            $data = array(
                "username" => $this->post("username"),
                "email" => $this->post("email"),
                "password" => $this->post("password"),
                "device_type" => $this->post("device_type") ? $this->post("device_type") : "",
                "device_token" => $this->post("device_token") ? $this->post("device_token") : "",
                "referred_by_code" => $this->post("referred_by_code") ? $this->post("referred_by_code") : "",
                "mobile" => $this->post("mobile"),
                "date_of_birth" => convert_date_to_db_format($this->post("date_of_birth")),
                "country_id" => $this->post("country")
            );
            $response = $this->player_register_model->register($data);
            if ($response) {
                $arr = array(
                    'status' => "valid",
                    "title" => "Registration successful",
                    "message" => "Registration successful",
                    "data" => $this->player_profile_model->get_profile_information($response)
                );
                $arr = json_encode($arr);
                $arr = json_decode(str_replace('null', '""', $arr));
                $this->response($arr);
            }
        }
    }

    function date_of_birth_check($str) {
        //date of birth format should be d-m-Y
        if (validate_age($str, 18) == false) {
            $this->form_validation->set_message('date_of_birth_check', 'You are Not eligible. Should be atleast 18 years old');
            return false;
        } else {
            return true;
        }
    }

}
