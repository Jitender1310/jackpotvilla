<?php

header('Content-type: application/json');

class Call_off_tournament extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("tournament_call_off_model");
    }

    function index_get() {
        $this->index_post();
    }

    function index_post() {

        $cloned_tournaments_id = $this->input->get_post("cloned_tournaments_id");
        $this->tournament_call_off_model->unjoin_all_joined_persons($cloned_tournaments_id);

        $response = "Response";
        if ($response) {
            $arr = [
                "status" => "valid",
                "title" => "Tournament Marked as calloff",
                "message" => "Tournament Marked as calloff",
                "data" => [
                    "call_off_tournament" => ""
                ]
            ];
        } else {
            $arr = [
                "status" => "invalid",
                "title" => "Error",
                "message" => "Some error occured",
                "data" => [
                    "message" => "Some error occured"
                ]
            ];
        }
        $this->response($arr);
    }

}
