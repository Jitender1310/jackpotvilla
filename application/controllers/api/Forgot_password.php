<?php

header('Content-type: application/json');

class Forgot_password extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function index_post() {
        $this->form_validation->set_rules("username", "Username", "required", array(
            'required' => 'Username cannot be empty'
        ));

        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "username" => form_error("username")
            ];
            $arr = [
                "status" => "invalid_form",
                "message" => "Please provide your username",
                "data" => $errors
            ];
            $this->response($arr);
            die;
        } else {
            $username = $this->post("username");
            $player_details = $this->player_login_model->get_player_details_by_username($username);
            if (!$player_details) {
                $errors = [
                    "username" => "User does not exist!"
                ];
                $arr = [
                    "status" => "invalid_form",
                    "title"=>"Oops",
                    "message" => "Sorry, User does not exist!",
                    "data" => $errors
                ];
                $this->response($arr);
                die;
            }
            $response = $this->player_reset_password_model->update_reset_password_link($player_details->access_token);
            if ($response) {
                $arr = [
                    "status" => "valid",
                    "title"=>"Updated",
                    "message" => "Password instructions has been sent to your email/mobile",
                    "data" => [
                        "message" => "Password instruction has been sent to your email/mobile"
                    ]
                ];
            } else {
                $arr = [
                    "status" => "invalid",
                    "title"=>"Oops",
                    "message" => "Invalid Username",
                    "data" => "Connection error"
                ];
            }
        }
        $this->response($arr);
    }

}
