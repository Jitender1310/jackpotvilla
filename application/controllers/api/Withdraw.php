<?php

header('Content-type: application/json');

class Withdraw extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('withdraw_model');
        $this->load->model('withdraw_process_validation_model');
    }

    function index_post() {
        $player_id = $this->player_login_model->get_logged_player_id();
        $filters = [];

        $total_results = $this->withdraw_model->get($filters, $player_id);

        $limit = $this->input->get_post("limit") ? $this->input->get_post("limit") : ORDERS_PER_PAGE;
        $_GET['page'] = $this->input->get_post("page");
        $start = $this->input->get_post("page") > 1 ? ($this->input->get_post("page") - 1) * $limit : 0;
        $filters["limit"] = $limit;
        $filters["start"] = $start;

        $result["data"] = $this->withdraw_model->get($filters, $player_id);
        $result["total_results_found"] = $total_results;

        $result["total_pages"] = ceil($total_results / $limit);

        $pagination = my_pagination("withdraw_status", $limit, $total_results, true);
        $pagination['pagination'] = str_replace("&amp;", "&", $pagination['pagination']);
        $cur_page = ($pagination['pagination_helper']->cur_page != 0) ? ($pagination['pagination_helper']->cur_page - 1) : $pagination['pagination_helper']->cur_page;
        $pagination['initial_id'] = (($cur_page) * $pagination['pagination_helper']->per_page) + 1;

        $arr = [
            "status" => "valid",
            "message" => "Withdraw Requests",
            "data" => $result,
            "pagination" => $pagination
        ];
        $arr = json_encode($arr);
        $arr = json_decode(str_replace('null', '""', $arr));
        $this->response($arr);
    }
    function send_withdrawl_request_post_old() {
        $players_id = $this->player_login_model->get_logged_player_id();
        $wallet_account_id = $this->player_login_model->get_player_details()->wallet_account_id;
        $real_chips_amount = $this->db->get_where('wallet_account', array('id' => $wallet_account_id))->row()->real_chips_withdrawal;
        $this->form_validation->set_rules("account_number", "Account Number", "required", array(
            'required' => 'Please enter Account number.'
        ));

        $this->form_validation->set_rules("ifsc_code", "IFSC code", "required", array(
            'required' => 'Please enter ifsc code.'
        ));

        $this->form_validation->set_rules("bank_name", "Bank Name", "required", array(
            'required' => 'Please enter bank name.'
        ));

        $this->form_validation->set_rules("branch_name", "Branch Name", "required", array(
            'required' => 'Please enter branch name.'
        ));
        $this->form_validation->set_rules("request_amount", "Amount", "required", array(
            'required' => 'Please enter Amount.'
        ));
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "request_amount" => form_error("request_amount"),
                "ifsc_code" => form_error("ifsc_code"),
                "account_number" => form_error("account_number"),
                "bank_name" => form_error("bank_name"),
                "branch_name" => form_error("branch_name"),
            ];
            $arr = [
                "status" => "invalid_form",
                "title" => "Required",
                "message" => "Please fill all mandatory fields",
                "data" => $errors
            ];
            $this->response($arr);
            die;
        }

        /*
          if ($this->post('request_amount') > MAXIMUM_AMOUNT_FOR_WITHDRAW_MONEY) {
          $arr = [
          "status" => "invalid",
          "title" => "Error",
          "message" => "You cannot withdraw more than " . MAXIMUM_AMOUNT_FOR_WITHDRAW_MONEY . " Rs/- ",
          ];
          $this->response($arr);
          die;
          }
          if ($this->post('request_amount') < MINIMUM_AMOUNT_FOR_WITHDRAW_MONEY) {
          $arr = [
          "status" => "invalid",
          "title" => "Error",
          "message" => "Minimum amount to withdraw is " . MAXIMUM_AMOUNT_FOR_WITHDRAW_MONEY . " Rs/- ",
          ];
          $this->response($arr);
          die;
          }
         * */


        if ($this->post('request_amount') > $real_chips_withdrawl_amount) {
            $arr = [
                "status" => "invalid",
                "title" => "Insufficient",
                "message" => "You cannot withdraw more than you have",
                "data" => [
                    "message" => "You cannot withdraw more than you have"
                ]
            ];
            $this->response($arr);
            die;
        }

        $data = [
            "request_amount" => $this->post("request_amount"),
            "account_number" => $this->post("account_number"),
            "ifsc_code" => $this->post("ifsc_code"),
            "bank_name" => $this->post("bank_name"),
            "branch_name" => $this->post("branch_name"),
            "request_status" => "Pending",
            "players_id" => $players_id,
            "request_date_time" => date('Y-m-d H:i:s A'),
        ];
        $response = $this->withdraw_model->add_withdraw_request($data);
        if ($response) {
            $arr = array(
                'status' => "valid",
                "title" => "Success",
                "message" => "Withdraw request sent successfully"
            );
            die;
        } else {
            $arr = [
                "status" => "in_valid",
                "title" => "Failed",
                "message" => "Unable To send withdraw request",
            ];
        }
        $arr = json_decode(str_replace('null', '""', json_encode($arr)));
        $this->response($arr);
    }

    function check_field_validations() {
        $players_id = $this->player_login_model->get_logged_player_id();
        $wallet_account_id = $this->player_login_model->get_player_details($players_id)->wallet_account_id;
        $real_chips_withdrawl_amount = $this->db->get_where('wallet_account', array('id' => $wallet_account_id))->row()->real_chips_withdrawal;


        $this->form_validation->set_rules("request_amount", "Amount", "required", array(
            'required' => 'Please enter Amount.'
        ));
        $this->form_validation->set_rules("transfer_type", "Transfer type", "required", array(
            'required' => 'Please select Payment Transaction type'
        ));

        if ($this->input->get_post("transfer_type") == "IMPS") {
            $this->form_validation->set_rules("transfer_to", "Transfer to", "required", array(
                'required' => 'Please select withdrawal type.'
            ));
        }


        if ($this->post('transfer_to') == 'PAYTM') {
            $this->form_validation->set_rules("paytm_mobile_number", "Paytm Mobile Number", "required", array(
                'required' => 'Please enter Paytm mobile number.'
            ));
            $this->form_validation->set_rules("paytm_person_name", "Person name", "required", array(
                'required' => 'Please enter Name.'
            ));
        } else {
            $this->form_validation->set_rules("account_number", "Account Number", "required", array(
                'required' => 'Please enter Account number.'
            ));
            $this->form_validation->set_rules("account_holder_name", "Account Holder name", "required", array(
                'required' => 'Please enter Account holder name.'
            ));
            $this->form_validation->set_rules("ifsc_code", "IFSC code", "required", array(
                'required' => 'Please enter ifsc code.'
            ));
        }

        if ($this->form_validation->run() == FALSE) {
            if ($this->post('transfer_type') == "IMPS" && $this->post('transfer_to') == 'PAYTM') {
                $errors = [
                    "paytm_mobile_number" => form_error("paytm_mobile_number"),
                    "paytm_person_name" => form_error("paytm_person_name"),
                ];
            } else {
                $errors = [
                    "ifsc_code" => form_error("ifsc_code"),
                    "account_number" => form_error("account_number"),
                    "account_holder_name" => form_error("account_holder_name"),
                    "bank_name" => form_error("bank_name"),
                    "branch_name" => form_error("branch_name"),
                ];
            }

            $errors['request_amount'] = form_error("request_amount");
            $errors['transfer_type'] = form_error("transfer_type");
            $errors['transfer_to'] = form_error("transfer_to");

            $arr = [
                "status" => "invalid_form",
                "title" => "Required",
                "message" => "Please fill all mandatory fields",
                "data" => $errors
            ];
            $this->response($arr);
            die;
        }

        /*
          if ($this->post('request_amount') > MAXIMUM_AMOUNT_FOR_WITHDRAW_MONEY) {
          $arr = [
          "status" => "invalid",
          "title" => "Error",
          "message" => "You cannot withdraw more than " . MAXIMUM_AMOUNT_FOR_WITHDRAW_MONEY . " Rs/- ",
          ];
          $this->response($arr);
          die;
          }
          if ($this->post('request_amount') < MINIMUM_AMOUNT_FOR_WITHDRAW_MONEY) {
          $arr = [
          "status" => "invalid",
          "title" => "Error",
          "message" => "Minimum amount to withdraw is " . MAXIMUM_AMOUNT_FOR_WITHDRAW_MONEY . " Rs/- ",
          ];
          $this->response($arr);
          die;
          }
         * */


        if ($this->post('request_amount') > $real_chips_withdrawl_amount) {
            $arr = [
                "status" => "invalid",
                "title" => "Insufficient",
                "message" => "You cannot withdraw more than you have",
                "data" => [
                    "message" => "You cannot withdraw more than you have"
                ]
            ];
            $this->response($arr);
            die;
        }
    }

    function send_withdrawl_request_post() {
        $players_id = $this->player_login_model->get_logged_player_id();
        $this->check_field_validations();

        $transfer_type = $this->post("transfer_type");
        $transfer_to = $this->post("transfer_to");
        if ($transfer_type == "NEFT") {
            $transfer_to = "BANK TRANSFER";
        }

        $data = [
            "request_amount" => $this->post("request_amount"),
            "transfer_type" => $transfer_type, //IMPS, NEFT
            "transfer_to" => $transfer_to, //PAYTM, BANK TRANSFER
            "request_status" => "Pending",
            "players_id" => $players_id,
            "request_date_time" => date('Y-m-d H:i:s A'),
        ];


        //if ($this->post('transfer_to') == 'PAYTM') {
        $data['paytm_mobile_number'] = $this->post('paytm_mobile_number');
        $data['paytm_person_name'] = $this->post('paytm_person_name');
        //} else {
        $data['account_number'] = $this->post('account_number');
        $data['account_holder_name'] = $this->post('account_holder_name');
        $data['ifsc_code'] = strtoupper($this->post('ifsc_code'));
        $data['bank_name'] = $this->post('bank_name');
        $data['branch_name'] = $this->post('branch_name');
        //}

        $validation_response = $this->withdraw_process_validation_model->validate_and_process($data["request_amount"], $data["transfer_type"], $data["transfer_to"], $players_id, $data);

        if ($validation_response["status"] == "invalid") {
            $this->response($validation_response);
        } else {
            $response = $this->withdraw_model->add_withdraw_request($validation_response);


            if ($response) {
                $arr = array(
                    'status' => "valid",
                    "title" => "Success",
                    "message" => "Your withdraw request has been accepted, it will be in process shortly Ref Id:#" . $response
                );
            } else {
                $arr = [
                    "status" => "invalid",
                    "title" => "Failed",
                    "message" => "Unable To send withdraw request",
                ];
            }
            $arr = json_decode(str_replace('null', '""', json_encode($arr)));
            $this->response($arr);
        }
    }

    function check_joker_status_get() {
        $players_id = $this->player_login_model->get_logged_player_id();
        $player_details = $this->player_profile_model->get_profile_information($players_id);
        $club_details = $this->club_types_model->get_club_details($player_details->expertise_level_id);
        if ($club_details) {
            $arr = array(
                'status' => "valid",
                "title" => "Success",
                "message" => "Club details found",
                "data" => $club_details
            );
        } else {
            $arr = [
                "status" => "invalid_form",
                "title" => "Failed",
                "message" => "Club details not found",
            ];
        }
        $arr = json_decode(str_replace('null', '""', json_encode($arr)));
        $this->response($arr);
    }

    function validate_request_post() {
        $players_id = $this->player_login_model->get_logged_player_id();

        $this->check_field_validations();


        $transfer_type = $this->post("transfer_type");
        $transfer_to = $this->post("transfer_to");
        if ($transfer_type == "NEFT") {
            $transfer_to = "BANK TRANSFER";
        }
        $data = [
            "request_amount" => $this->post("request_amount"),
            "transfer_type" => $transfer_type, //IMPS, NEFT
            "transfer_to" => $transfer_to, //PAYTM, BANK TRANSFER
            "request_status" => "Pending",
            "players_id" => $players_id,
            "request_date_time" => date('Y-m-d H:i:s A')
        ];

        //if ($this->post('transfer_to') == 'PAYTM') {
        $data['paytm_mobile_number'] = $this->post('paytm_mobile_number');
        $data['paytm_person_name'] = $this->post('paytm_person_name');
        //} else {
        $data['account_number'] = $this->post('account_number');
        $data['account_holder_name'] = $this->post('account_holder_name');
        $data['ifsc_code'] = strtoupper($this->post('ifsc_code'));
        $data['bank_name'] = $this->post('bank_name');
        $data['branch_name'] = $this->post('branch_name');
        //}
        $response = $this->withdraw_process_validation_model->validate_and_process($data["request_amount"], $data["transfer_type"], $data["transfer_to"], $players_id, $data);
        $this->response($response);
    }

    function cancel_request_post() {
        $players_id = $this->player_login_model->get_logged_player_id();
        $this->form_validation->set_rules("withdrawal_id", "Withdraw Request id", "required", array(
            'required' => 'Please select an item.'
        ));
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "withdrawal_id" => form_error("withdrawal_id")
            ];
            $arr = [
                "status" => "invalid_form",
                "title" => "Required",
                "message" => "Plese select an item to cancel",
                "data" => $errors
            ];
            $this->response($arr);
            die;
        }
        $data = [
            "withdrawal_id" => $this->post("withdrawal_id")
        ];

        $response = $this->withdraw_process_validation_model->cancel_withdraw_request($data["withdrawal_id"], $players_id);
        $this->response($response);
    }

}
