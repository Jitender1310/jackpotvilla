<?php

    header('Content-type: application/json');

    class Game_lobby extends REST_Controller {

        function __construct() {
            parent::__construct();
            $this->load->model('tournaments_joining_model');
            $this->load->model('player_login_model');
            $this->load->model('player_profile_model');
        }

        function index_get() {
            $this->index_post();
        }

        function index_post() {
            $player_id = $this->player_login_model->get_logged_player_id();
            $response = $this->game_lobby_model->get_game_section($player_id);

            if ($response) {
                $arr = array(
                    'status' => "valid",
                    "title" => "Game Lobby",
                    "message" => "Game Lobby",
                    "data" => $response
                );
                $arr = json_encode($arr);
                $arr = json_decode(str_replace('null', '""', $arr));
            } else {
                $arr = [
                    "status" => "invalid",
                    "title" => "No Games",
                    "message" => "Game section is empty",
                    "data" => ""
                ];
            }
            $this->response($arr);
        }

//
//        function join_tournament_post() {
//            $cloned_tournaments_id = $this->post('cloned_tournaments_id');
//            $player_id = $this->player_login_model->get_player_id_by_access_token($this->session->userdata('p_access_token'));
//            //$player_details = $this->player_profile_model->get_profile_information($this->session->userdata('p_access_token'));
//            $response = $this->tournaments_joining_model->join($cloned_tournaments_id, $player_id);
//
//            if ($response == 'JOINED') {
//                $arr = array(
//                    'status' => "valid",
//                    "title" => "Success",
//                    "message" => "Registered Successfully",
//                    "data" => $response
//                );
//                $arr = json_encode($arr);
//                $arr = json_decode(str_replace('null', '""', $arr));
//            } else {
//                $arr = [
//                    "status" => "invalid",
//                    "title" => "Sorry",
//                    "message" => str_replace('_', ' ', ucfirst(strtolower($response))),
//                    "data" => str_replace('_', ' ', ucfirst(strtolower($response)))
//                ];
//                $arr = json_encode($arr);
//                $arr = json_decode(str_replace('null', '""', $arr));
//            }
//
//            $this->response($arr);
//        }
    }
    