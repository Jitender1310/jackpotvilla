<?php
class Games extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        /*$filePath = APPPATH . '/../uploads/wac.txt';
        $fh = fopen($filePath, 'a');
        fwrite($fh, "Hi Ai am called");
        fwrite($fh, $_POST);
        fwrite($fh, $_GET);
        fwrite($fh, file_get_contents('php://input'));
        fclose($fh);*/
        //echo "Done";

        $xml = simplexml_load_string(file_get_contents('php://input')) or die("Error: Cannot create object");
        $method = $xml->method;
        $credentials = $method->credentials;
        $params = $method->params;

        $methodName = $method['name'];
        $login = $credentials->login;
        $password = $credentials->password;
        $token = $params->token['value'];
        $gameReference = $params->gameReference['value'];

        $game_log = $this->games_model->get_game_log_by_token($token);
        if(!$game_log){
            echo $this->games_model->generate_error_response($methodName, 'TOKEN_EXPIRED');
            exit;
        }

        $player_id = $game_log->player_id;

        $player_detail = $this->games_model->get_player_by_id($player_id);
        $username = $player_detail->username;
        $currency = $player_detail->currency;
        $balance = $player_detail->real_chips_deposit * 100;

        $game_detail = $this->games_model->get_game_by_reference($gameReference);
        $game_id = $game_detail->id;

        $log['game_id'] = $game_id;
        $log['player_id'] = $player_id;
        $game_token = $this->games_model->add_game_log($log);

        switch($methodName)
        {
            case 'getPlayerInfo':
                $result = $this->games_model->getPlayerInfo($game_token, $username, $currency, $balance);
                break;
            case 'getBalance':
                $result = $this->games_model->getBalance($game_token, $balance);
                break;
            case 'bet':
                $transactionId = $params->transactionId['value'];
                $roundId = $params->roundId['value'];
                $amount = $params->amount['value'];

                if($amount > $balance){
                    echo $this->games_model->generate_error_response($methodName, 'INSUFFICIENT_BALANCE');
                    exit;
                }

                $wallet_account_id = $player_detail->wallet_account_id;
                $type = 'WITHDRAWAL';
                $prev_balance = $balance / 100;
                $current_balance = ($balance - $amount) / 100;
                $amount = $amount / 100;

                $transaction_data = array(
                    'player_id' => $player_id,
                    'game_id' => $game_id,
                    'wallet_account_id' => $wallet_account_id,
                    'type' => $type,
                    'prev_balance' => $prev_balance,
                    'current_balance' => $current_balance,
                    'amount' => $amount,
                    'date_added' => date('Y-m-d H:i:s'),
                    'provider_transaction_id' => $transactionId,
                    'round_id' => $roundId
                );

                $game_transaction_id = $this->games_model->add_transaction($transaction_data);
                $history_data = array(
                    'transaction_type' => 'Bet',
                    'amount' => $amount,
                    'wallet_account_id' => $wallet_account_id,
                    'remark' => $gameReference . ' Entry',
                    'type' => 'Game Entry',
                    'game_title' => $gameReference,
                    'game_sub_type' => $gameReference,
                    'games_id' => $game_id,
                    'closing_balance' => $current_balance,
                    'created_date_time' => date('Y-m-d H:i:s')
                );
                $this->games_model->add_wallet_transaction_history($history_data);
                $this->wallet_model->set_updated_real_chips_deposit($current_balance, $wallet_account_id);

                $current_balance = $current_balance * 100;
                $result = $this->games_model->bet($game_token, $game_transaction_id, $current_balance);

                $slab_id = $player_detail->slab_id;
                $slabs = $this->games_model->get_all_slabs();
                $min_points = 0;
                $max_points = 0;
                $wager_factor = 0;

                foreach($slabs as $slab)
                {
                    if($slab['id'] == $slab_id)
                    {
                        $min_points = $slab['min_points'];
                        $max_points = $slab['max_points'];
                        $wager_factor = $slab['wager_factor'];
                    }
                }

                $current_loyalty_points = $player_detail->loyalty_points;
                $new_loyalty_points = $current_loyalty_points + $wager_factor * $amount;

                if($new_loyalty_points > $max_points)
                {
                    foreach($slabs as $slab)
                    {
                        if($slab['min_points'] <= $new_loyalty_points && $slab['max_points'] > $new_loyalty_points)
                        {
                            $slab_id = $slab['id'];
                        }
                    }
                }
                $this->games_model->update_loyalty($wallet_account_id, $new_loyalty_points, $slab_id);
                break;
            case 'win':
                $transactionId = $params->transactionId['value'];
                $roundId = $params->roundId['value'];
                $amount = $params->amount['value'];

                $wallet_account_id = $player_detail->wallet_account_id;
                $type = 'DEPOSIT';
                $prev_balance = $balance / 100;
                $current_balance = ($balance + $amount) / 100;
                $amount = $amount / 100;

                $transaction_data = array(
                    'player_id' => $player_id,
                    'game_id' => $game_id,
                    'wallet_account_id' => $wallet_account_id,
                    'type' => $type,
                    'prev_balance' => $prev_balance,
                    'current_balance' => $current_balance,
                    'amount' => $amount,
                    'date_added' => date('Y-m-d H:i:s'),
                    'provider_transaction_id' => $transactionId,
                    'round_id' => $roundId
                );

                $game_transaction_id = $this->games_model->add_transaction($transaction_data);
                $history_data = array(
                    'transaction_type' => 'Win',
                    'amount' => $amount,
                    'wallet_account_id' => $wallet_account_id,
                    'remark' => $gameReference . ' End',
                    'type' => 'Game End',
                    'game_title' => $gameReference,
                    'game_sub_type' => $gameReference,
                    'games_id' => $game_id,
                    'closing_balance' => $current_balance,
                    'created_date_time' => date('Y-m-d H:i:s')
                );
                $this->games_model->add_wallet_transaction_history($history_data);
                $this->wallet_model->set_updated_real_chips_deposit($current_balance, $wallet_account_id);

                $current_balance = $current_balance * 100;
                $result = $this->games_model->win($game_token, $game_transaction_id, $current_balance);
                break;
        }

        echo $result;
    }
}
