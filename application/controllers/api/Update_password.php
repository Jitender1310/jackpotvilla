<?php

class Update_password extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function index_post() {
        $this->form_validation->set_rules("current_password", "Current Password", "trim|required|min_length[6]|max_length[32]", array(
            'required' => 'Current Password cannot be empty'
        ));
        $this->form_validation->set_rules("new_password", "New Password", "trim|required|min_length[6]|max_length[32]", array(
            'required' => 'New Password cannot be empty'
        ));
        $this->form_validation->set_rules("confirm_new_password", "Confirm New Password", "trim|required|matches[new_password]", array(
            'matches' => 'Confirm Password should match with New password'
        ));
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "current_password" => form_error("current_password"),
                "new_password" => form_error("new_password"),
                "confirm_new_password" => form_error("confirm_new_password")
            ];
            $arr = [
                "status" => "invalid_form",
                "data" => $errors
            ];
            $this->response($arr);
            die;
        }

        $current_password = $this->post('current_password');
        $player_id = $this->player_login_model->get_logged_player_id();

        $valid = $this->player_change_password_model->check_for_current_password_is_correct($current_password, $player_id);
        if (!$valid) {
            $errors = [
                "current_password" => "Please enter valid current password",
                "new_password" => "",
                "confirm_new_password" => ""
            ];
            $arr = [
                "status" => "invalid_form",
                "data" => $errors
            ];
            $this->response($arr);
            die;
        }

        $password = $this->post('new_password');
        $data = [
            "password" => $password
        ];
        $response = $this->player_change_password_model->update($data, $player_id);

        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "Success",
                "message" => "Password updated successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "Success",
                "message" => "Password not updated, please try again"
            ];
        }
        $this->response($arr);
    }

}
