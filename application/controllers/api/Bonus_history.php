<?php

header('Content-type: application/json');

class Bonus_history extends REST_Controller {

    function __construct() {
        parent::__construct();
    }
    
    function index_get(){
        $this->index_post();
    }

    function index_post() {
        $player_id = $this->player_login_model->get_logged_player_id();
        $filters =[];
        
        if($this->input->get_post("from_date")){
            $filters["from_date"] = $this->input->get_post("from_date");
        }else{
            $filters["from_date"] = "";
        }

        if($this->input->get_post("to_date")){
            $filters["to_date"] = $this->input->get_post("to_date");
        }else{
            $filters["to_date"] = "";
        }
        
        $total_results = $this->bonus_transactions_model->get_bonus_transactions_history($filters, $player_id);
        
        $limit = $this->input->get_post("limit") ? $this->input->get_post("limit") : ORDERS_PER_PAGE;
        $_GET['page'] = $this->input->get_post("page");
        $start = $this->input->get_post("page") > 1 ? ($this->input->get_post("page") - 1) * $limit : 0;
        $filters["limit"] = $limit;
        $filters["start"] = $start;
        
        $result["data"] = $this->bonus_transactions_model->get_bonus_transactions_history($filters, $player_id);
        $result["total_results_found"] = $total_results;
        
        $result["total_pages"] = ceil($total_results/$limit);
        
        $pagination = my_pagination("bonus_transactions", $limit, $total_results, true);
        $pagination['pagination'] = str_replace("&amp;", "&", $pagination['pagination']);
        $cur_page = ($pagination['pagination_helper']->cur_page != 0) ? ($pagination['pagination_helper']->cur_page - 1) : $pagination['pagination_helper']->cur_page;
        $pagination['initial_id'] = (($cur_page) * $pagination['pagination_helper']->per_page) + 1;
        
        $arr = [
            "status" => "valid",
            "message" => "Bonus History",
            "data" => $result,
            "pagination" => $pagination
        ];
        $arr = json_encode($arr);
        $arr = json_decode(str_replace('null', '""', $arr));
        $this->response($arr);
    }
}