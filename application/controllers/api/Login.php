<?php

header('Content-type: application/json');

class Login extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function index_post() {
        //log_message("error", print_r($_POST, true));
        $this->form_validation->set_rules("username", "Username", "required", array(
            'required' => 'Username cannot be empty'
        ));
        $this->form_validation->set_rules("password", "Password", "required");
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "username" => form_error("username"),
                "password" => form_error("password")
            ];
            $arr = [
                "status" => "invalid_form",
                "data" => $errors
            ];
            $this->response($arr);
            die;
        } else {
            $username = $this->post("username");
            $password = $this->post("password");
            $player_details = $this->player_login_model->get_player_details_by_username($username);

            if (!$player_details) {
                $errors = [
                    "username" => "Invalid Userid",
                    "password" => ""
                ];
                $arr = [
                    "status" => "invalid_form",
                    "title" => "Invalid Credentials",
                    "message" => "Invalid Credentials",
                    "data" => $errors
                ];
            } else if ($player_details->status == 0) {
                $arr = [
                    "status" => "invalid",
                    "title" => "Account Successpended",
                    "message" => "Sorry, you account has been suspended",
                    "data" => [
                        "message" => "Sorry, you account has been suspended"
                    ]
                ];
            } else {
                $response = $this->player_login_model->verify_login($player_details, $password);
                if ($response) {
                    $arr = [
                        "status" => "valid",
                        "message" => "Login Successful",
                        "data" => $this->player_profile_model->get_profile_information($response)
                    ];
                } else {
                    $errors = [
                        "username" => "",
                        "password" => "Invalid Password"
                    ];
                    $arr = [
                        "status" => "invalid_form",
                        "message" => "Invalid Password",
                        "data" => $errors
                    ];
                }
            }
        }
        $arr = json_encode($arr);
        $arr = json_decode(str_replace('null', '""', $arr));
        $this->response($arr);
    }

}
