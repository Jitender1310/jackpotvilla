<?php
header('Content-type: application/json');
class Check_location extends CI_Controller {

    public $states;

    public function __construct() {
        parent::__construct();
    }

    function index() {

        $arr = array(       
            'status' => "valid",        
            "title" => "Eligible to play game ",        
            "message" => "Eligible to play game",       
            "data" => [     
                "location_check" => "Eligible to play game"     
            ]       
        );

        //echo json_encode($arr, JSON_PRETTY_PRINT);
        //die;
        log_message("last_query__k", "testing"); 

        //$map_key = "AIzaSyDvVuhFabi4FRFEMLlUiVma53uuRl6Iup8";
        //$map_key = "AIzaSyB-z5qVNl8zVEomkoJFxQKg3JA8xDsvw2I";
        $map_key = "AIzaSyBHqEzPkt5dOZUJWMa2KD2rS3rJ8WSLbhA";
        $lat = $this->input->get_post("lat");
        $lng = $this->input->get_post("lon");

        $url = "https://maps.google.com/maps/api/geocode/json?latlng=" . $lat . "," . $lng . "&key=" . $map_key;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = json_decode(curl_exec($ch));
        curl_close($ch);

        $location_name = "";

        if ($data->status == "OK") {
            for ($i = 0; i < count($data->results[0]->address_components); $i++) {
                if ($data->results[0]->address_components[$i]->types[0] == "administrative_area_level_1") {
                    $location_name = $data->results[0]->address_components[$i]->long_name;
                    break;
                }
            }
        }

        $this->db->where("play_permission", 1);
        $this->db->where("status", 1);
        $this->db->where("LOWER(state_name)", strtolower($location_name));
        if ($this->db->get("states")->num_rows()) {
            $arr = array(
                'status' => "valid",
                "title" => "Eligible to play game ",
                "message" => "Eligible to play game",
                "data" => [
                    "location_check" => "Eligible to play game"
                ]
            );
        } else {
            $arr = array(
                'status' => "invalid",
                "title" => "Oops",
                "message" => "All Telangana region players are not allowed to play any Real Money Gaming as per  Amendment to the Ordinance. ",
                "data" => [
                    "location_check" => "All Telangana region players are not allowed to play any Real Money Gaming as per  Amendment to the Ordinance. "
                ]
            );
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }

}
