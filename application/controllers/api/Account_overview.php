<?php

header('Content-type: application/json');

class Account_overview extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function index_get() {
        $player_id = $this->player_login_model->get_logged_player_id();
        $account_overview = $this->player_account_overview_model->get_account_view_details($player_id);
        $arr = [
            "status" => "valid",
            "data" => $account_overview
        ];
        $this->response($arr);
    }

}
