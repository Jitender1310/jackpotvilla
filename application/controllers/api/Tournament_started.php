<?php

class Tournament_started extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("tournament_started_model");
    }

    function index_post() {
        $cloned_tournaments_id = $this->input->get_post("cloned_tournaments_id");
        $response = $this->tournament_started_model->unjoin_all_waitlist_persons($cloned_tournaments_id);
        if ($response) {
            $arr = [
                "status" => "valid",
                "title" => "Tournament running info",
                "message" => "Tournament running info",
                "data" => [
                    "tournament_started"=> ""
                ]
            ];
        } else {
            $arr = [
                "status" => "invalid",
                "title" => "Game Progress",
                "message" => "No progress recorded",
                "data" => [
                    "message" => "Some error occured"
                ]
            ];
        }
        $this->response($arr);
    }

    function index_get() {
        $this->index_post();
    }

}
