<?php

header('Content-type: application/json');

class Mark_as_tournament_completed extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("tournament_completed_distribute_prize_model");
    }

    function index_get() {
        $this->index_post();
    }

    function index_post() {

        $cloned_tournaments_id = $this->input->get_post("cloned_tournaments_id");
        $response = $this->tournament_completed_distribute_prize_model->distribute_prizes($cloned_tournaments_id);

        if ($response) {
            $arr = [
                "status" => "valid",
                "title" => "Tournament Prize Distribution",
                "message" => "Tournament Prize Distribution",
                "data" => [
                    "mark_as_tournament_completed" => ""
                ]
            ];
        } else {
            $arr = [
                "status" => "invalid",
                "title" => "Error",
                "message" => "Some error occured",
                "data" => [
                    "message" => "Some error occured"
                ]
            ];
        }
        $this->response($arr);
    }

}
