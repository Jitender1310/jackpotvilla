<?php

header('Content-type: application/json');

class Send_download_rummy_link extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->model_name = 'download_rummy_link_model';
        $this->load->model($this->model_name);
        $this->load->library('simple_captcha');
    }

    function captcha_get() {
        $this->simple_captcha->draw_captcha('contact_us');
        die;
    }

    function index_post() {
        //$player_id = $this->player_login_model->get_logged_player_id();
        // print_r($this->input->post()); die;

        $this->form_validation->set_rules('mobile', 'Mobile', 'required', array(
            'required' => 'Mobile is required field.'
        ));


        $this->form_validation->set_rules('captcha', 'Captcha Code', 'required|callback_captcha_val', array(
            'required' => 'Captcha Code is required.',
            'captcha_val' => 'Captcha Code is Invalid.'
        ));


        if ($this->form_validation->run() == FALSE) {
            $arr = [
                "status" => "invalid_form",
                "title" => "Please fill mandatory fields",
                "data" => [
                    'mobile' => form_error('mobile'),
                    "captcha" => form_error("captcha")
                ]
            ];
            $this->response($arr);
        } else {
            $data = array(
                'mobile' => $this->input->post('mobile')
            );

            $this->{$this->model_name}->save($data);

            $response = $this->{$this->model_name}->send_download_link_to_mobile($data);
            if ($response) {
                $arr = [
                    "status" => "valid",
                    "title" => "Download link sent",
                    "message" => "Download link sent",
                    "data" => [
                        "message" => 'Download link sent to mobile number'
                    ]
                ];
            } else {
                $arr = [
                    "status" => "invalid",
                    "title" => "Error",
                    "message" => "Some error occured",
                    "data" => [
                        "message" => "Some error occured"
                    ]
                ];
            }

            $this->response($arr);
        }
    }

    function captcha_val() {
        $captcha_input = $this->input->post('captcha', TRUE);
        $captcha_text = $this->simple_captcha->get_captcha_text('contact_us');

        if ($captcha_input == $captcha_text) {
            return true;
        } else {
            $this->form_validation->set_message('captcha', 'Invalid Captcha code');
            return false;
        }
    }

}
