<?php

header('Content-type: application/json');

class Subscribe_to_newsletters extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->model_name = 'newsletter_subscribe_model';
        $this->load->model($this->model_name);
        $this->load->library('simple_captcha');
    }

    function captcha_get() {
        $this->simple_captcha->draw_captcha('contact_us');
        die;
    }

    function index_post() {
        //$player_id = $this->player_login_model->get_logged_player_id();
        // print_r($this->input->post()); die;

        $this->form_validation->set_rules('newsletter_email', 'Email', 'required', array(
            'required' => 'Email is required field.'
        ));


        $this->form_validation->set_rules('captcha', 'Captcha Code', 'required|callback_captcha_val', array(
            'required' => 'Captcha Code is required.',
            'captcha_val' => 'Captcha Code is Invalid.'
        ));


        if ($this->form_validation->run() == FALSE) {
            $arr = [
                "status" => "invalid_form",
                "title" => "Please fill mandatory fields",
                "data" => [
                    'newsletter_email' => form_error('newsletter_email'),
                    "captcha" => form_error("captcha")
                ]
            ];
            $this->response($arr);
        } else {
            $data = array(
                'email' => $this->input->post('newsletter_email')
            );

            $this->{$this->model_name}->save($data);

            //$response = $this->{$this->model_name}->send_download_link_to_mobile($data);
            if ($response) {
                $arr = [
                    "status" => "valid",
                    "title" => "Download link sent",
                    "message" => "Download link sent",
                    "data" => [
                        "message" => 'You are successfully subscribed to our news letter'
                    ]
                ];
            } else {
                $arr = [
                    "status" => "invalid",
                    "title" => "Error",
                    "message" => "Some error occured",
                    "data" => [
                        "message" => "Some error occured"
                    ]
                ];
            }

            $this->response($arr);
        }
    }

    function captcha_val() {
        $captcha_input = $this->input->post('captcha', TRUE);
        $captcha_text = $this->simple_captcha->get_captcha_text('contact_us');

        if ($captcha_input == $captcha_text) {
            return true;
        } else {
            $this->form_validation->set_message('captcha', 'Invalid Captcha code');
            return false;
        }
    }

}
