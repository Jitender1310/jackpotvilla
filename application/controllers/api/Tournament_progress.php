<?php

    class Tournament_progress extends REST_Controller {

        public function __construct() {
            parent::__construct();
            $this->load->model("tournament_progress_model");
        }

        function index_post() {
            $cloned_tournaments_id = $this->input->get_post("cloned_tournaments_id");
            $response = $this->tournament_progress_model->get_progress($cloned_tournaments_id);
            if ($response) {
                $arr = [
                    "status" => "valid",
                    "title" => "Tournament running info",
                    "message" => "Tournament running info",
                    "data" => [
                        "round_info" => $response
                    ]
                ];
            } else {
                $arr = [
                    "status" => "invalid",
                    "title" => "Game Progress",
                    "message" => "No progress recorded",
                    "data" => [
                        "message" => "Some error occured"
                    ]
                ];
            }
            $this->response($arr);
        }

        function index_get() {
            $this->index_post();
        }

    }
    