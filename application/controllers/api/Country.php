<?php

header('Content-type: application/json');

class Country extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function index_get() {
        $arr = [
            "status" => "valid",
            "message" => "Countries Successful",
            "data" => $this->country_model->get_all()
        ];
        $arr = json_encode($arr);
        $arr = json_decode(str_replace('null', '""', $arr));
        $this->response($arr);
    }

}
