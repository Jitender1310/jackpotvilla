<?php

header('Content-type: application/json');

class Deposit_money_request extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function create_post() {
        $players_id = $this->player_login_model->get_logged_player_id();
        
        $this->form_validation->set_rules("credits_count", "Amount", "required", array(
            'required' => 'Amount cannot be empty'
        ));
        
        /*$this->form_validation->set_rules("payment_gateway", "Payment Mode", "required", array(
            'required' => 'Please choose payment mode'
        ));*/
        
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "credits_count" => form_error("credits_count"),
                "bonus_code" => form_error("bonus_code"),
                "payment_gateway" => form_error("payment_gateway")
            ];
            $arr = [
                "status" => "invalid_form",
                "data" => $errors
            ];
            $this->response($arr);
            die;
        } else {
            $data = [
                "players_id" => $players_id,
                "credits_count" => $this->post("credits_count"),
                "source" => $this->post('source') ? $this->post('source') : "website",
                "version_code" => $this->post("version_code")? $this->post("version_code") : "",
                "payment_gateway" => $this->post("payment_gateway") ? $this->post("payment_gateway") : 'CASH'
            ];
            
            if($this->post("bonus_code")){
                $bonus_code = $this->post("bonus_code");
                $credits_count = $this->post("credits_count");
                $coupon_response = $this->bonus_transactions_model->validate_bonus_code($bonus_code, $credits_count, $players_id);
                if($coupon_response === "INVALID_BONUS_USAGE"){
                    $arr = [
                        "status" => "invalid",
                        "title"=>"Invalid Coupon Code",
                        "message" => "Sorry, already this bonus code updated",
                        "data" => [
                            "bonus_code" => $bonus_code
                        ]
                    ];
                    $this->response($arr);
                    die;
                }else if($coupon_response === "INVALID_BONUS_CODE"){
                    $arr = [
                        "status" => "invalid",
                        "title"=>"Invalid Coupon Code",
                        "message" => "Sorry, this bonus code does't existed",
                        "data" => [
                            "bonus_code" => $bonus_code
                        ]
                    ];
                    $this->response($arr);
                    die;
                }else if($coupon_response === "MAX_USED"){
                    $arr = [
                        "status" => "invalid",
                        "title"=>"Maximum times used",
                        "message" => "Sorry, You have Exceeded Maximum Number Of usages",
                        "data" => [
                            "bonus_code" => $bonus_code
                        ]
                    ];
                    $this->response($arr);
                    die;
                } else if ($coupon_response == "MIN_DEPOSIT_REQUIRED") {
                    $minimum_amount = $this->bonus_transactions_model->get_minimum_deposit_amount_to_avail_bonus_code($bonus_code);
                    $arr = [
                        "status" => "invalid",
                        "title" => "Minimum amount required",
                        "message" => "To avail " . $bonus_code . ", The minimum amount should be Rs." . $minimum_amount,
                        "data" => [
                            "bonus_code" => $bonus_code
                        ]
                    ];
                    $this->response($arr);
                    die;
                }else if(is_numeric($coupon_response)){
                    $data["bonus_code"] = strtoupper($this->post("bonus_code"));
                    $data["bonus_credits"] = $coupon_response;
                }else{
                    $arr = [
                        "status" => "invalid",
                        "title"=>"Invalid Bonus Code",
                        "message" => "Sorry, this bonus code is invalid",
                        "data" => [
                            "bonus_code" => $bonus_code
                        ]
                    ];
                    $this->response($arr);
                    die;
                }
            }
            $response = $this->deposit_model->create_request($data);
            if ($response) {
                $arr = [
                    "status" => "valid",
                    "title"=>"Submitted",
                    "message" => "Deposit request created",
                    "data" => [
                        "transaction_id" => $response
                    ]
                ];
            } else {
                $arr = [
                    "status" => "invalid",
                    "title"=>"Error",
                    "message" => "Some error occured",
                    "data" => [
                        "message" => "Some error occured"
                    ]
                ];
            }
        }
        $this->response($arr);
    }
    
    function validate_bonus_code_post(){
        $players_id = $this->player_login_model->get_logged_player_id();
        $this->form_validation->set_rules("credits_count", "Amount", "required", array(
            'required' => 'Amount cannot be empty'
        ));
        $this->form_validation->set_rules("bonus_code", "Bonus code", "required", array(
            'required' => 'Bonus Code be empty'
        ));
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "credits_count" => form_error("credits_count"),
                "bonus_code" => form_error("bonus_code")
            ];
            $arr = [
                "status" => "invalid_form",
                "data" => $errors
            ];
            $this->response($arr);
            die;
        } else {
            $bonus_code = $this->post("bonus_code");
            $data = [
                "players_id" => $players_id,
                "credits_count" => $this->post("credits_count"),
                "bonus_code" => $bonus_code
            ];
            $credits_count = $this->post("credits_count");
            $coupon_response = $this->bonus_transactions_model->validate_bonus_code($bonus_code, $credits_count, $players_id);
            
            if($coupon_response === "INVALID_BONUS_USAGE"){
                $arr = [
                    "status" => "invalid",
                    "title"=>"Invalid Coupon Code",
                    "message" => "Sorry, already this bonus code used",
                    "data" => [
                        "bonus_code" => $bonus_code
                    ]
                ];
            }else if($coupon_response ==="INVALID_BONUS_CODE"){
                $arr = [
                    "status" => "invalid",
                    "title"=>"Invalid Coupon Code",
                    "message" => "Sorry, this bonus code does't existed",
                    "data" => [
                        "bonus_code" => $bonus_code
                    ]
                ];
            }else if($coupon_response === "MAX_USED"){
                $arr = [
                    "status" => "invalid",
                    "title"=>"Maximum times used",
                    "message" => "Sorry, You have Exceeded Maximum Number Of usages",
                    "data" => [
                        "bonus_code" => $bonus_code
                    ]
                ];
                $this->response($arr);
                die;
            } else if ($coupon_response == "MIN_DEPOSIT_REQUIRED") {
                $minimum_amount = $this->bonus_transactions_model->get_minimum_deposit_amount_to_avail_bonus_code($bonus_code);
                $arr = [
                    "status" => "invalid",
                    "title" => "Minimum amount required",
                    "message" => "To avail " . $bonus_code . ", The minimum amount should be Rs." . $minimum_amount,
                    "data" => [
                        "bonus_code" => $bonus_code
                    ]
                ];
                $this->response($arr);
                die;
            } else if(is_numeric($coupon_response)){
                $credit_to = $this->bonus_transactions_model->get_bonus_coupon_credit_to_type($bonus_code);
                if($credit_to == "Deposit Wallet"){
                    $arr = [
                        "status" => "valid",
                        "title"=>"Success",
                        "message" => "Promo code applied successfully",
                        "data" => [
                            "message" => "Rs.".$coupon_response." will be added to your wallet on successful transaction"
                        ]
                    ];
                }else if($credit_to == "Bonus Wallet"){
                    $arr = [
                        "status" => "valid",
                        "title"=>"Success",
                        "message" => "Bonus code applied successfully",
                        "data" => [
                            "message" => $coupon_response . " bonus points applied on this "
                        ]
                    ];
                }
            }else{
                $arr = [
                    "status" => "invalid",
                    "title"=>"Invalid Bonus Code",
                    "message" => "Sorry, this bonus code is invalid",
                    "data" => [
                        "bonus_code" => $bonus_code
                    ]
                ];
            }   
        }
        $this->response($arr);
    }
    
}
