<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>tableGamesCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>popularGamesService.js?r=<?= time() ?>"></script>

<?php if (is_logged_in()) { ?>
    <?php $this->load->view('includes/menu-dashboard') ?>
<?php } ?>

<?php if (!is_logged_in()) { ?>
    <?php $this->load->view('menu') ?>
<?php } ?>
<section class="innerpages" ng-controller="tableGamesCtrl">
    <div class="container py-2" style="background: #fff;">
    <div class="card card-gold text-center">
    <div class="card-header card-header2 text-white">
        <h3>Bet Games</h3>
    </div>
    </div>
<!-- <div class="row my-2" ng-style="">
    <div class="col-sm-6 col-xs-6">
    <div class='card border-0'>
        <button type="button" class="btn btn-block btn-light mx-auto" ng-click="page ='first'" ng-class="{'btnactive':page === 'first'}">Tab 1</button>
    </div>
    </div>
    <div class="col-sm-6 col-xs-6">
    <div class='card border-0'>
        <button type="button" class="btn btn-block btn-light mx-auto" ng-click="page ='second'" ng-class="{'btnactive':page === 'second'}">Tab 2</button>
    </div>
    
    </div>
    <div ng-init="page='first'"></div>
    <div class="container text-center py-4" ng-show="page === 'first'">
    <div class="card-header w-100 text-balck text-center">
            <strong> Tab 1 Games</strong>
        </div>
       <div class="row text-center">
    
        <div class="card my-1 img_icon" ng-repeat="(index, game) in games" style="border: 0; margin: auto; align-items: center; justify-content: flex-start">
        <div class="card-img">
        <img src="{{game.logo}}" class="image logged_in_img" alt="" ng-mouseover="open = true" ng-mouseleave="open = false">
            <div class="middle">
                <?php
                if(!$is_logged_in)
                {
                    ?>
                    <a href="#">
                        <div class="text"> Login</div>
                     </a>
                    <?php
                }
                else
                {
                    ?>
                    <a href="javascript::void(0);" ng-click="loadGame(game.id, '<?=$access_token?>')">
                        <div class="text"> Play</div>
                     </a>
                    <?php
                }
                ?>
            </div>
        </div>
        <div class="card-body w-100 card-header2 text-white text-center">
            <strong>{{game.display_name}}</strong>
        </div>
    </div>
    </div>
    </div>
    <div class="container text-center py-4" ng-show="page === 'second'">
        <div class="row">
    <div class="card w-100 text-balck text-center">
        <div class="card-header">
            <strong> Tab 2 Games</strong>
            </div>
        </div>
        </div>
       <div class="row text-center">
        <div class="card my-1 img_icon" ng-repeat="(index, game) in games" style="border: 0; margin: auto; align-items: center; justify-content: flex-start">
        <div class="card-img">
        <img src="{{game.logo}}" class="image logged_in_img" alt="" ng-mouseover="open = true" ng-mouseleave="open = false">
            <div class="middle">
                <?php
                if(!$is_logged_in)
                {
                    ?>
                    <a href="#">
                        <div class="text"> Login</div>
                     </a>
                    <?php
                }
                else
                {
                    ?>
                    <a href="javascript::void(0);" ng-click="loadGame(game.id, '<?=$access_token?>')">
                        <div class="text"> Play</div>
                     </a>
                    <?php
                }
                ?>
            </div>
        </div>
        <div class="card-body w-100 card-header2 text-white text-center">
            <strong>{{game.display_name}}</strong>
        </div>
    </div>
    </div>
    </div>
</div> -->
</div>

</section>

<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>registerCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>registerService.js?r=<?= time() ?>"></script>   