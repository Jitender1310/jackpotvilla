<?php $this->load->view("includes/header"); ?>
<?php include'menu.php'; ?>
<section class="innerpages py-5">
    <div class="container">
        <div class="row" style=" background: #fff; padding: 20px 0;">
            <div class="col-3">
<?php include'includes/cms-sidebar.php'; ?>
         
            </div>
            <div class="col-9 text-white">
<div class="editor">
    <h5 class=" text-black1" style=" margin-bottom: 0;">TIPS AND TRICKS</h5>

<ul>
    <li>Making a pure sequence must be your primary objective.</li>
    <li>Discard cards of higher value. They can get you in trouble.</li>
    <li>Observe your opponents and the discard pile carefully and retrieve any card that is of value to you.</li>
    <li>Remember than a run can consist of more than 3 cards.</li>
    <li>Do not retain cards in hope that it will be of use to you in the future. Keep discarding  cards of no current value to you.</li>
    <li>Retain neutral numbers such as 6. You can make a 4,5,6 or 5,6,7 or a 6,7,8. Neutral cards have more probability to be turned into a sequence.</li>
    <li>You can  confuse your opponents by discarding a card that you have two of.</li>
    <li>Make the best use of your joker.</li>
</ul></div>


            
            
            </div>
        </div>
    </div>
</section>