<style>
.card_new {
    background-color: #2f0060;
    border-radius: 6px;
    margin: 0;
    padding:0;
}

.btn-light {
    background-image: linear-gradient(to bottom, #04db04, #006300);
    /* width: 200px;
    text-align: center; */
}

.btn-light:focus, .btn-light.focus,
.btn-light:not(:disabled):not(.disabled):active:focus, .btn-light:not(:disabled):not(.disabled).active:focus, .show>.btn-light.dropdown-toggle:focus {
    box-shadow: none
}
</style>

<?php $this->load->view('includes/menu-dashboard') ?>
<section class="innerpages" ng-controller="gameLobbyCtrl">
    <?php $this->load->view('includes/my_joined_list_game_popup') ?>
    <?php $this->load->view('includes/tournament_details_popup') ?>
    <div class="container">
        <div class="inner-frame"> 
      
            <?php include'chips.php'; ?>
            <div class="row px-3">
            <div class="col-lg-12 col-sm-4 col-xs-12">
            <div class="btn-group-special row no-gutters justify-content-center">
                <div class="col-lg-4 col-sm-4 col-xs-12 p-2">
                <div class="card card_new text-center">
                    <div class="card-img my-4">
                    <div class="circle">
                    <img src="../../assets/images/coins.svg" height="90" alt="">
                    </div>
                    </div>
                    <button type="button" ng-click="setGameType('cash')" class="btn btn-block btn-light mx-auto {{selectedGameType=='cash'?'btn-active':''}}">CASH
                    </button>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-4 col-xs-12 p-2">
                <div class="card card_new text-center">
                <div class="card-img my-4">
                <div class="circle">
                <img src="../../assets/images/practice.svg" height="90" alt="">
                </div>
                </div>
                    <button type="button" ng-click="setGameType('practice')" class="btn btn-block btn-light mx-auto {{selectedGameType=='practice'?'btn-active':''}}">PRACTICE</button>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-4 col-xs-12 p-2">
                <div class="card card_new text-center">
                <div class="card-img my-4">
                    <div class="circle">
                    <img src="../../assets/images/podium.svg" height="90" alt="">
                    </div>
                    </div>
                    <button type="button" ng-click="setGameType('tournaments')" class="btn btn-block btn-light mx-auto {{selectedGameType=='tournaments'?'btn-active':''}}" onclick="tournaments()">TOURNAMENTS</button>
                    </div>
                </div>

                <!--<div class="col-3-width">
                     <button type="button" ng-click="showMyJoinedGamePopup()" class="btn btn-block btn-light">MY GAMES</button> 
                </div>-->
            </div>
            </div>
            <div class="col-12">
                <div class="card card-table mt-3" style="border: none;">
                <div class="card-body" ng-if="selectedGameType == 'cash' || selectedGameType == 'practice' || selectrdGameType == 'tournaments'">
                    <div class="card card-table-menu shadow">
                        <div class="card-body py-2">
                            <div class="row no-gutters">
                                <div class="col-lg-4 col-sm-4 col-xs-12 p-2 text-center" ng-click="setSubGameType('pool')">
                                    <a class="special-tag has-info {{selectedSubGameType=='pool'?'active':''}}" href="javascript:void(0);">
                                         Pool Rummy
                                    </a>
                                </div>

                                <div class="col-lg-4 col-sm-4 col-xs-12 p-2 text-center" ng-click="setSubGameType('points')">
                                    <a class="special-tag has-info {{selectedSubGameType=='points'?'active':''}}"  href="javascript:void(0);">
                                        Points Rummy
                                    </a>
                                </div>

                                <div class="col-lg-4 col-sm-4 col-xs-12 p-2 text-center" ng-click="setSubGameType('deals')">
                                    <a class="special-tag has-info {{selectedSubGameType=='deals'?'active':''}}" href="javascript:void(0);">
                                        Deal Rummy
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                
                <?php $this->load->view("includes/tournaments_area_div") ?>
                <div class="tabel-outer">
                <table class="table table-custom table-striped table-hover bg-white" ng-if="selectedSubGameType == 'points' && selectedGameType != 'tournaments'">
                    <thead class="bg-primary-dark text-white">   <tr>
                            <th class="border-top-0">Name</th>
                            <th class="border-top-0">Decks</th>
                            <th class="border-top-0">Max Players</th>
                            <th class="border-top-0">Point Value</th>
                            <th class="border-top-0">Min- Entry</th>
                            <th class="border-top-0">Active Players</th>
                            <th class="border-top-0">Registering</th>
                            <th class="border-top-0">Action</th>
                        </tr>
                    </thead>
                    <tr ng-repeat="item in gameListTable track by $index">
                        <td>{{item.game_sub_type}} Rummy</td>
                        <td>{{item.number_of_deck}}</td>
                        <td>{{item.seats}}</td>
                        <td>{{item.point_value}}</td>
                        <td>{{selectedGameType == 'cash' ? "Rs."+item.entry_fee : item.entry_fee}}</td>
                        <td>{{item.active_players|| 0}}</td>
                        <td>{{item.registering|| 0}}/{{item.seats|| 0}}</td>
                        <td class="text-right">
                            <button type="button" ng-click="openPlayNowWindow(item.token, '<?= $player_details->access_token ?>', item.game_type)" class="btn rounded btn-success btn-sm">PLAY NOW</button>
                        </td>
                    </tr>
                </table>


                <table class="table table-custom table-striped table-hover bg-white" ng-if="selectedSubGameType == 'pool' && selectedGameType != 'tournaments'">
                    <thead class="bg-primary-dark text-white">   
                        <tr>
                            <th class="border-top-0">Name</th>
                            <th class="border-top-0">Type</th>
                            <th class="border-top-0">Max Players</th>
                            <th class="border-top-0">Entry Fee</th>
                            <th class="border-top-0">Prize</th>
                            <th class="border-top-0">Active Players</th>
                            <th class="border-top-0">Registering</th>
                            <th class="border-top-0">Action</th>
                        </tr>
                    </thead>
                    <tr ng-repeat="item in gameListTable track by $index">
                        <td>{{item.game_sub_type}} Rummy</td>
                        <td>{{item.pool_game_type}}</td>
                        <td>{{item.seats}}</td>
                        <td>{{selectedGameType == 'cash' ? "Rs."+item.entry_fee : item.entry_fee}}</td>
                        <td>{{selectedGameType == 'cash' ? "Rs."+item.pool_deal_prize : item.pool_deal_prize}}</td>
                        <td>{{item.active_players|| 0}}</td>
                        <td>{{item.regPlayers|| 0}}</td>
                        <td>
                            <button type="button" ng-click="openPlayNowWindow(item.token, '<?= $player_details->access_token ?>', item.game_type)" class="btn rounded btn-success btn-sm">PLAY NOW</button>
                        </td>
                    </tr>
                </table>

                <table class="table table-custom table-striped table-hover bg-white" ng-if="selectedSubGameType == 'deals' && selectedGameType != 'tournaments'">
                    <thead class="bg-primary-dark text-white">   
                        <tr>
                            <th class="border-top-0">Name</th>
                            <th class="border-top-0">Deals</th>
                            <th class="border-top-0">Max Players</th>
                            <th class="border-top-0">Entry Fee</th>
                            <th class="border-top-0">Prize</th>
                            <th class="border-top-0">Active Players</th>
                            <th class="border-top-0">Registering</th>
                            <th class="border-top-0">Action</th>
                        </tr>
                    </thead>
                    <tr ng-repeat="item in gameListTable track by $index">
                        <td>{{item.game_sub_type}} Rummy</td>
                        <td>{{item.deals}}</td>
                        <td>{{item.seats}}</td>
                        <td>{{selectedGameType == 'cash' ? "Rs."+item.entry_fee : item.entry_fee}}</td>
                        <td>{{selectedGameType == 'cash' ? "Rs."+item.pool_deal_prize : item.pool_deal_prize}}</td>
                        <td>{{item.active_players|| 0}}</td>
                        <td>{{item.registering|| 0}}/{{item.seats|| 0}}</td>
                        <td class="text-right">
                            <button type="button" ng-click="openPlayNowWindow(item.token, '<?= $player_details->access_token ?>', item.game_type)" class="btn rounded btn-success btn-sm">PLAY NOW</button>
                        </td>
                    </tr>
                </table>
                </div>
            </div>
            </div>  

           
        </div>    
    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>gameLobbyCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>gameLobbyService.js?r=<?= time() ?>"></script>
