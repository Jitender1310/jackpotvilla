<?php include'menu-dashboard.php'; ?>
<section class="innerpages py-5">
    <div class="container">
        <?php include'chips.php'; ?>
        <div class="row mt-4">
            <?php include'dashboard-sidebar.php'; ?>
            <div class="col-9">
                <div class="card card-gold">
                    <div class="card-header">UPLOADED DOCUMENTS</div>


                    <table class="table table-custom table-striped table-hover  mb-0">
                        <thead class="bg-primary-dark text-white">
                            <tr>
                                <th>Verification for</th>
                                <th>Document Type</th>
                                <th>Date & Time</th>
                                <th>File Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tr>
                            <td>ID</td>
                            <td>Pan Card</td>
                            <td>21-12-2018 03:17 PM</td>
                            <td>1.Jpg</td>
                            <td><a href="#" class="btn btn-success btn-sm">Download</a></td>
                        </tr>
                        <tr>
                            <td>ID</td>
                            <td>Pan Card</td>
                            <td>21-12-2018 03:17 PM</td>
                            <td>1.Jpg</td>
                            <td><a href="#" class="btn btn-success btn-sm">Download</a></td>
                        </tr>
                        <tr>
                            <td>ID</td>
                            <td>Pan Card</td>
                            <td>21-12-2018 03:17 PM</td>
                            <td>1.Jpg</td>
                            <td><a href="#" class="btn btn-success btn-sm">Download</a></td>
                        </tr>
                        <tr>
                            <td>ID</td>
                            <td>Pan Card</td>
                            <td>21-12-2018 03:17 PM</td>
                            <td>1.Jpg</td>
                            <td><a href="#" class="btn btn-success btn-sm">Download</a></td>
                        </tr>
                        <tr>
                            <td>ID</td>
                            <td>Pan Card</td>
                            <td>21-12-2018 03:17 PM</td>
                            <td>1.Jpg</td>
                            <td><a href="#" class="btn btn-success btn-sm">Download</a></td>
                        </tr>
                        <tr>
                            <td>ID</td>
                            <td>Pan Card</td>
                            <td>21-12-2018 03:17 PM</td>
                            <td>1.Jpg</td>
                            <td><a href="#" class="btn btn-success btn-sm">Download</a></td>
                        </tr>
                        <tr>
                            <td>ID</td>
                            <td>Pan Card</td>
                            <td>21-12-2018 03:17 PM</td>
                            <td>1.Jpg</td>
                            <td><a href="#" class="btn btn-success btn-sm">Download</a></td>
                        </tr>
                        <tr>
                            <td>ID</td>
                            <td>Pan Card</td>
                            <td>21-12-2018 03:17 PM</td>
                            <td>1.Jpg</td>
                            <td><a href="#" class="btn btn-success btn-sm">Download</a></td>
                        </tr>


                    </table>




                </div>
            </div>
        </div>
    </div>
</section>