<?php $this->load->view('includes/menu-dashboard') ?>
<section class="innerpages py-5">
    <div class="container">
        <div class="row mt-4 px-0">
            <div class="col-lg-12 col-xs-12">
                <div class="card card-gold">
                    <div class="card-header">Welcome <?= $player_details->username ?></div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <div class="card bg-light h-100">
                                    <div class="card-body text-center">
                                        <div class="mb-3"><svg height="100px" viewBox="0 0 188.148 188.148"><use xlink:href="#dp" /></svg></div>
                                        <h3>I WON RS 17,000 FOR INVITING FRIENDS</h3>
                                        <h6 class="text-muted"> </h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-8 col-sm-6 col-xs-12 text-center">

                                <div class="row align-items-center justify-content-center my-5">
                                    <div class="col-auto"><svg height="80px" viewBox="0 0 512 512"><use xlink:href="#enevelope" /></svg></div>
                                    <div class="col-auto"><i class="fal fa-plus fa-2x"></i></div>
                                    <div class="col-auto"><svg height="80px" viewBox="0 0 512 512"><use xlink:href="#group" /></svg></div>
                                    <div class="col-auto"><i class="fal fa-equals fa-2x"></i></div>
                                    <div class="col-auto"><svg height="80px" viewBox="0 0 512 512"><use xlink:href="#rupee-coin" /></svg></div>
                                </div>
                                <h2>Getting started is easy!</h2>
                                <!--<p class="lead">Earn Upto 1000 in bonus for each friend that joins</p>-->

                                <div class="card mt-5 bg-light">
                                    <div class="card-body">
                                        <div class="row align-items-center">
                                            <div class="col ">
                                                <a  href="<?= base_url() ?>account" class="btn btn-block btn-success">Go to My Account</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>