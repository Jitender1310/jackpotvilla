<section class="slider">
    <div class="swiper-container home-slider">
        <div class="swiper-wrapper">
            <div class="swiper-slide item" style="background-image: url(<?= STATIC_IMAGES_PATH ?>forgot-slider.png);"></div>
            <div class="swiper-slide item" style="background-image: url(<?= STATIC_IMAGES_PATH ?>forgot-slider2.png);"></div>
        </div>
    </div>
    <div class="slider__button-prev fal fa-chevron-left shadow"></div>
    <div class="slider__button-next fal fa-chevron-right shadow"></div>
    <div class="form">
        <div class="container">
            <div class="row">
                <div class="col"></div>
                <div class="col-auto" ng-controller="forgotPasswordCtrl" ng-submit="submitRequest()">
                    <div class="registerbox shadow-lg" style=" background: #0006;">
                        <div class="registerbox__head" style="font-size: 20px">
                            Forgot Password
                        </div>                        
                        <h6 class="text-white text-center text-uppercase mb-3 mt-3" style="margin: 30px 0 20px 0 !important;">New User Bonus upto <?php echo WELCOME_BONUS ?> points</h6>
                        <form class="px-4" id="forgotPasswordForm" ng-submit="doResetPassword()" ng-keyup="fp_error = {}">
                            <div class="form-group">
                                <div class="iconbox">
                                    <div class="iconbox--icon"><img src="<?= STATIC_IMAGES_PATH ?>/Forgot-user.png" class="img-fluid " style="width: 60%;"></div>
                                    <input type="text" name="username" class="form-control form-control-sm border-0 shadow iconbox--input" 
                                           placeholder="Username/Email" ng-model="fp.username">
                                </div>
                                <span>{{fp_error.username}}</span>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" ng-disabled="forgotPasswordBtn" class="btn btn-theme shadow">
                                    SUBMIT
                                </button>
                                
                            </div>
                        </form>
                        <p class="text-center text-white mt-2">Don't have an account, 
                            <a href="<?=  base_url()?>home" class="text-warning" style="color: #00f300 !important;">Sign up now</a>.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>registerCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>registerService.js?r=<?= time() ?>"></script>   
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>forgotPasswordCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>forgotPasswordService.js?r=<?= time() ?>"></script> 