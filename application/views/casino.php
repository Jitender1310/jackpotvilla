<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>casinoGamesCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>casinoGamesService.js?r=<?= time() ?>"></script>

<?php if (is_logged_in()) { ?>
    <?php $this->load->view('includes/menu-dashboard') ?>
<?php } ?>

<?php if (!is_logged_in()) { ?>
    <?php $this->load->view('menu') ?>
<?php } ?>
<section class="innerpages" ng-controller="casinoGamesCtrl">
    <div class="container py-4" style="background: #fff;">
    <div class="card card-gold text-center">
    <div class="card-header card-header2 text-white">
        <h3>Casino Games</h3>
    </div>
    </div>
    <div class="row py-4 d-flex">

    <div class="card my-1 img_icon" ng-repeat="(index, game) in games" style="border: 0; margin: auto; align-items: center; justify-content: flex-start !important">
        <div class="card-img">
        <img src="{{game.logo}}" class="image logged_in_img" alt="" ng-mouseover="open = true" ng-mouseleave="open = false">
            <div class="middle">
                <?php
                if(!$is_logged_in)
                {
                    ?>
                    <a href="#">
                        <div class="text"> Login</div>
                     </a>
                    <?php
                }
                else
                {
                    ?>
                    <a href="javascript::void(0);" ng-click="loadGame(game.id, '<?=$access_token?>')">
                        <div class="text"> Play</div>
                     </a>
                    <?php
                }
                ?>
            </div>
        </div>
        <div class="card-body w-100 card-header2 text-white text-center">
            <strong>{{game.display_name}}</strong>
        </div>
    </div>
    </div>
</div>
</section>

<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>registerCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>registerService.js?r=<?= time() ?>"></script>   