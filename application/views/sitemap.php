<?php if (is_logged_in()) { ?>
    <?php $this->load->view('includes/menu-dashboard') ?>
<?php } ?>

<?php if (!is_logged_in()) { ?>
    <?php $this->load->view("menu"); ?>
<?php } ?>
<section class="section py-5">
    <div class="container" style=" background: #fff; padding: 10px 20px;">
        <h4 class=" text-black1">Site Map</h4>
    <div class="row">
      <div class="col-lg-4 col-sm-6 col-xs-12 py-3">
          <div class="card h-100  border-custom mb-3" style="box-shadow: 0px 6px 5px 3px #e3e3e3; border: none;">
          <div class="card-header card-header-bg  border-custom h6 text-custom">Getting Started</div>
          <div class="">
            <ul class="nav flex-column">
              
              <li class="nav-item">
                <a href="" class="  nav-link text-custom a-bg-colar">Registration</a>
              </li>
              <li class="nav-item">
                <a href="" class="  nav-link text-custom a-bg-colar">Login</a>
              </li>
              <?php /*<li class="nav-item">
                <a href="" class="  nav-link text-custom a-bg-colar">Download Rummy</a>
              </li>*/?>
              <li class="nav-item">
                <a href="" class="  nav-link text-custom a-bg-colar">Download Mobile App</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 py-3">
          <div class="card h-100  border-custom mb-3" style="box-shadow: 0px 6px 5px 3px #e3e3e3; border: none;">
          <div class="card-header card-header-bg  border-custom h6 text-custom">A2Z Betting</div>
          <div class="">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a href="" class=" a-bg-colar nav-link text-custom">About us</a>
              </li>
              <li class="nav-item">
                <a href="" class="a-bg-colar nav-link text-custom">Legality</a>
              </li>
              <?php /*<li class="nav-item">
                <a href="" class="a-bg-colar nav-link text-custom">Security</a>
              </li>*/?>
              <li class="nav-item">
                <a href="" class=" a-bg-colar nav-link text-custom">Terms and conditions</a>
              </li>
              <li class="nav-item">
                <a href="" class="a-bg-colar nav-link text-custom">Privacy policy</a>
              </li>
              <?php /*<li class="nav-item">
                <a href="" class=" a-bg-colar nav-link text-custom">Fair play policy</a>
              </li>*/?>
            
            </ul>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 py-3">
          <div class="card h-100  border-custom mb-3" style="box-shadow: 0px 6px 5px 3px #e3e3e3; border: none;">
          <div class="card-header card-header-bg  border-custom h6 text-custom">Contact </div>
          <div class="">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a href="" class=" a-bg-colar nav-link text-custom">Contact us</a>
              </li>
              <li class="nav-item">
                <a href="" class=" a-bg-colar nav-link text-custom">FAQ’s</a>
              </li>
               <?php /*<li class="nav-item">
                <a href="" class="a-bg-colar nav-link text-custom">Blog</a>
              </li>*/?>
              <li class="nav-item">
                <a href="" class=" a-bg-colar nav-link text-custom">Forgot password</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <!-- <div class="col-4 py-3">
          <div class="card h-100 border-custom mb-3" style="box-shadow: 0px 6px 5px 3px #e3e3e3; border: none;">
          <div class="card-header card-header-bg  border-custom h6 text-custom">Play rummy</div>
          <div class="">
            <ul class="nav flex-column">
               <?php /*<li class="nav-item">
                <a href="" class="a-bg-colar nav-link text-custom">Rummy rules</a>
              </li>*/?>
              <li class="nav-item">
                <a href="" class=" a-bg-colar nav-link text-custom">Rummy tips & tricks</a>
              </li>
                 <?php /*<li class="nav-item">
                <a href="" class=" nav-link text-custom a-bg-color">Online vs offline rummy</a>
              </li>*/?>
                 <?php /*<li class="nav-item">
                <a href="" class="nav-link text-custom a-bg-color">Rummy dictionary</a>
              </li>*/?>
              <li class="nav-item">
                <a href="" class=" a-bg-colar nav-link text-custom">How to play rummy</a>
              </li>
              <?php /*<li class="nav-item">
                <a href="" class=" a-bg-colar nav-link text-custom">Demo videos</a>
              </li>*/?>
             
              <li class="nav-item">
                <a href="" class=" a-bg-colar nav-link text-custom">Split</a>
              </li>
              <?php /*<li class="nav-item">
                <a href="" class=" a-bg-colar nav-link text-custom">Rummy history</a>
              </li>*/?>
            </ul>
          </div>
        </div>
      </div> -->
      <div class="col-lg-4 col-sm-6 col-xs-12 py-3">
          <div class="card h-100  border-custom mb-3" style="box-shadow: 0px 6px 5px 3px #e3e3e3; border: none;">
          <div class="card-header card-header-bg  border-custom h6 text-custom">Tournaments</div>
          <div class="">
            <ul class="nav flex-column">
              <?php /* <li class="nav-item">
                <a href="" class="nav-link text-custom a-bg-colar">Premium tournament</a>
              </li>*/?>
              <li class="nav-item">
                <a href="" class="nav-link text-custom a-bg-colar">Level tournament</a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link text-custom a-bg-colar">Special Tournaments</a>
              </li>
              
              <li class="nav-item">
                <a href="" class="nav-link text-custom a-bg-colar">Cash tournament</a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link text-custom a-bg-colar">Free tournament</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 py-3">
        <div class="card h-100  border-custom mb-3"  style="box-shadow: 0px 6px 5px 3px #e3e3e3; border: none;">
          <div class="card-header card-header-bg  border-custom h6 text-custom">Promotions</div>
          <div class="">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a href="" class="nav-link text-custom a-bg-colar"></a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link text-custom a-bg-colar">Bonus</a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link text-custom a-bg-colar">Welcome bonus</a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link text-custom a-bg-colar">Deposit bonus</a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link text-custom a-bg-colar">Bring-a-friend</a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link text-custom a-bg-colar">Reward points</a>
              </li>
               <?php /*<li class="nav-item">
                <a href="" class="nav-link text-custom a-bg-colar">Birthday bonus</a>
              </li>*/?>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 py-3">
        <div class="card h-100  border-custom mb-3"  style="box-shadow: 0px 6px 5px 3px #e3e3e3; border: none;">
          <div class="card-header card-header-bg  border-custom h6 text-custom">Social media</div>
          <div class="">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a href="" class="nav-link text-custom a-bg-colar">Facebook</a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link text-custom a-bg-colar">Instagram</a>
              </li>
              
              <li class="nav-item">
                <a href="" class="nav-link text-custom a-bg-colar">Youtube</a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link text-custom a-bg-colar">Pintrest</a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link text-custom a-bg-colar">Twitter</a>
              </li>
           
              
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
 <!--li.nav-item*>a.nav-link.text-custom-->  
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>registerCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>registerService.js?r=<?= time() ?>"></script>    