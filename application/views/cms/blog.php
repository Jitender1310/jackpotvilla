<?php if (is_logged_in()) { ?>
    <?php $this->load->view('includes/menu-dashboard') ?>
<?php } ?>

<?php if (!is_logged_in()) { ?>
    <?php $this->load->view("includes/header"); ?>
    <?php $this->load->view('menu') ?>
<?php } ?>
<section class="innerpages py-5">
    <div class="container editor" style=" background: #fff; padding: 10px 20px;">

        <h5 class=" text-black1" style=" ">Blog</h5>


        <?php for ($i = 0; $i < 10; $i++) { ?>
            <div class="p-5 shadow mb-3 bg-white">
                <div class="row">
                    <div class="col-4">
                  
                        <img src="<?= STATIC_IMAGES_PATH ?>blog-post.jpg" class="img-fluid mb-5">
                    </div>
                    <div class="col-8">

                        <h5 class="mb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. </h5>
                        <p class="mb-2 text-black-50">03/04/2019 2:30 PM</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur accusantium, quas in quam impedit, eaque quae reprehenderit, ab rem modi distinctio voluptates a aperiam reiciendis? Quasi, dolor! Laborum, ex. Fugit. adipisicing elit. Tenetur accusantium. adipisicing elit. Tenetur accusantium adipisicing elit. Tenetur accusantium.
                        </p>

                        <a href="<?= base_url() ?>blog_view" class="btn btn-outline-primary">Read More</a>

                    </div>
                </div>
            </div>

        <?php  } ?>



    </div>
</section>