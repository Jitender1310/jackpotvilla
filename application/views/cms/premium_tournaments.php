<?php if (is_logged_in()) { ?>
    <?php $this->load->view('includes/menu-dashboard') ?>
<?php } ?>

<?php if (!is_logged_in()) { ?>
    <?php $this->load->view("includes/header"); ?>
    <?php $this->load->view('menu') ?>
<?php } ?>
<section class="innerpages py-5">
    <div class="container">
        <div class="row">
            <div class="col-3">
                <ul class="nav nav-stabs flex-column" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" id="t1" data-toggle="tab" href="#h1" role="tab" aria-controls="h1" aria-selected="true">Daily Tournaments</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="t2" data-toggle="tab" href="#h2" role="tab" aria-controls="h2" aria-selected="false">Weekly Tournaments</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="t3" data-toggle="tab" href="#h3" role="tab" aria-controls="h3" aria-selected="false">Monthly Tournaments</a>
                    </li>

                </ul>
            </div>
            <div class="col-9 text-white">
                <div class="tab-content  stab-content editor">
                    <div class="tab-pane fade show active" id="h0" role="tabpanel" aria-labelledby="t0">


                        <p>
                            Rummy Maza introduces, for the very first time in Rummy history, not only in India, but also in the online rummy community across the globe, Premium Tournaments! Premium tournaments spike up the fun quotient of rummy game-play and make sure to send every player back with something in hand.
                        </p>

                        <p>
                            Rummy Maza is the only online Rummy Website that allows you to gain profit from every game you play through its premium tournaments scheme. In premium tournaments, your every win is tracked and recognized to give you better results and better rewards. On winning cash games, you will not only receive the money the table offers, but also, you will be allowed to play premium tournaments which will allow you to further earn rewards. We will help you understand the concept of Premium tournaments better.
                        </p>
                        <p>Premium tournaments are divided into Four clubs:</p>

                        <ul>
                            <li>Mr. Bronze - One can enter the bronze club on winning Rs.25 pool rummy games on Rummy Maza. The Rs.25 games that you win will be counted and you will be allowed to play the premium tournament. Track your wins and stay updated on when you can play the premium tournament.</li>
                            <li>Mr. Silver - The user can enter the Silver Club by playing and winning Rs.50 pool rummy Games on Rummy Maza. This is facilitate your chance in playing premium tournaments and in winning higher prizes.</li>
                            <li>Mr. Gold - The Gold Club is for gamers who play and win Rs.100 pool rummy Games. Your wins will be calculated and you will be allowed to play premium tournaments for Mr. Gold.</li>
                            <li>Mr. Diamond - One can enter the Diamond club by playing cash games worth Rs.500. pool rummy game. These will earn you higher and more exciting and rewarding prizes.</li>
                        </ul>



                        <p>
                            To summarize, the club you are in depends on the amount for which you play cash games. The greater the amount, the greater are the results plus benefits. It is important to remember that the only way you can go to the higher club is by playing cash games for higher amounts. You cannot level up with certain points or wins.
                        </p>

                        <p>
                            To see how many cash games you have won and to see when you will be eligible to partake in premium tournaments, log into “My Game Tracker” from your account. It allows you to check your game count and what premium club you belong to based on the cash games you have played. If you meet the criteria and if you eligible for the premium tournament, you will be allowed to play them.
                        </p>






                    </div>
                    <div class="tab-pane fade" id="h1" role="tabpanel" aria-labelledby="t1">

                        <p>Daily Tournaments/ Hey Mister - The Hey Mister Tournaments are held everyday from 3:00 pm to 3:00 pm. Participate if you are eligible by winning at least 5 games
                        </p>

                        <h5>Eligibility:</h5>

                        <ul>
                        	<li>If you are in the Bronze club, you need a minimum of five wins to play the daily tournament.</li>
                        	<li>If you are in the Silver club, you require a minimum of four wins to participate in the daily tournament.</li>
                        	<li>If you are in the Gold Club, you require a minimum of three wins to participate in Hey Mister</li>
                        	<li>If you are in the Diamond Club, you will need a minimum of two wins to play Hey Mister.</li>
                        </ul>


                    </div>
                    <div class="tab-pane fade" id="h2" role="tabpanel" aria-labelledby="t2">
                    </div>
                    <div class="tab-pane fade" id="h3" role="tabpanel" aria-labelledby="t3">

                    </div>
                    <div class="tab-pane fade" id="h4" role="tabpanel" aria-labelledby="t4">

                    </div>
                    <div class="tab-pane fade" id="h5" role="tabpanel" aria-labelledby="t5">

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>