<?php if (is_logged_in()) { ?>
    <?php $this->load->view('includes/menu-dashboard') ?>
<?php } ?>

<?php if (!is_logged_in()) { ?>
    <?php $this->load->view("includes/header"); ?>
    <?php $this->load->view('menu') ?>
<?php } ?>

<section class="innerpages py-5">
    <div class="container">
        <div class="row" style="background: #fff;
             padding: 20px 0;">
            <div class="col-3">
                <ul class="nav nav-stabs flex-column"role="tablist" style="border: 1px solid #cecece;">
                    <li class="nav-item">
                        <a class="nav-link active" id="t1" data-toggle="tab" href="#h1" role="tab" aria-controls="h1" aria-selected="true">What is a Sequence?</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="t2" data-toggle="tab" href="#h2" role="tab" aria-controls="h2" aria-selected="false">How to form a set?</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="t3" data-toggle="tab" href="#h3" role="tab" aria-controls="h3" aria-selected="false">Points Rummy</a>
                    </li>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="t4" data-toggle="tab" href="#h4" role="tab" aria-controls="h4" aria-selected="false">Deal Rummy</a>
                    </li>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="t5" data-toggle="tab" href="#h5" role="tab" aria-controls="h5" aria-selected="false">Pool Rummy</a>
                    </li>
                </ul>
            </div>
            <div class="col-9 text-white">
                <div class="tab-content  stab-content editor">
                    <div class="tab-pane fade show active" id="h0" role="tabpanel" aria-labelledby="t0" style="background: #fff;">
                        <img src="<?= STATIC_IMAGES_PATH ?>how-to-play.png" class="img-fluid ">
                        <ul>
                            <li>The number of cards distributed depends on the number of players. 2-6 individuals can play the game.</li>
                            <li>After evenly distributing the cards, the remaining deck is placed in the middle with the top card flipped up. This card starts the discard pile.</li>
                            <li>The player must aim at playing all of his cards into melds. A meld is a set of three or four cards of the same face value or three or four cards in the same suit.</li>
                            <li>The player must retrieve a card from the deck or from the discard pile.</li>
                            <li>Try to form a meld with the cards you own and discard any unnecessary card.</li>
                            <li>Each player’s turn ends discarding one card.</li>
                            <li>During the player’s turn, he/she can lay down any possible melds on the table.</li>
                            <li>The next player can add to the meld that the previous player has started.</li>
                            <li>This process continues until a player has discarded all his cards.</li>
                            <li>The winner is allotted points by summing up the face value of all the other players cards.</li>
                            <li>If a player that has not laid down any cards on the table is able to lay them off in a meld, all at once, it is known as GOING RUMMY.</li>
                            <li>When a player goes rummy, he is awarded double the points in the other players hands.</li>
                        </ul>
                        <div class="embed-responsive embed-responsive-21by9 mt-5">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Gij_jOGHDIo"></iframe>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="h1" role="tabpanel" aria-labelledby="t1" style=" background: #fff;">
                        <h5 id="1" class=" text-black1" style=" margin-bottom: 0;">What is a Sequence?</h5>
                        <p>
                            With the cards that the player has been dealt with, he/she must create a sequence, i.e., he/she must arrange the cards into sets of 3 or 4 consecutive cards of the same suit.
                        </p>
                        <p>
                            A sequence can be pure or impure. A pure sequence is a group of three or four consecutive cards of the same suit. A player cannot use a joker or a wild card while creating a pure sequence.
                        </p>
                        <p>2<span class="fas fa-spade text-warning mx-1"></span>3<span class="fas fa-spade text-warning mx-1"></span>4<span class="fas fa-spade text-warning mx-1"></span>5<span class="fas fa-spade text-warning mx-1"></span> is a pure sequence of four cards.</p>
                        <p>3<span class="fas fa-heart text-danger mx-1"></span>4<span class="fas fa-heart text-danger mx-1"></span>5<span class="fas fa-heart text-danger mx-1"></span>  is a pure sequence of three cards.</p>
                        <p>An impure sequence is a group of three or four cards while using one or more jokers.</p>
                        <p>3<span class="fas fa-diamond text-info mx-1"></span>4<span class="fas fa-diamond text-info mx-1"></span>Q<span class="fas fa-diamond text-info mx-1"></span>6<span class="fas fa-diamond text-info mx-1"></span> is an impure sequence of four cards where Q<span class="fas fa-diamond text-info mx-1"></span> is used as a wild</p>
                        <p>joker for 5<span class="fas fa-diamond text-info mx-1"></span></p>
                        <p>2<span class="fas fa-heart text-danger mx-1"></span>3<span class="fas fa-heart text-danger mx-1"></span>Q<span class="fas fa-heart text-danger mx-1"></span> is an impure sequence of three cards where Q<span class="fas fa-heart text-danger mx-1"></span>  is used as a wild joker for</p>
                        <p>4<span class="fas fa-heart text-danger mx-1"></span></p>
                    </div>
                    <div class="tab-pane fade" id="h2" role="tabpanel" aria-labelledby="t2" style=" background: #fff;">
                        <h5 id="2" class=" text-black1" style="margin-bottom: 0;">How to form a set?</h5>
                        <p>A set is a group of three or four cards of the same value but from different suits. A set
                            cannot be formed if the same numbered card belongs to the same suit.</p>
                        <p>2<span class="fas fa-heart text-danger mx-1"></span>2<span class="fas fa-spade text-warning mx-1"></span>2<span class="fas fa-diamond text-info mx-1"></span>2<span class="fas fa-club text-success mx-1"></span> is a set of 2’s from four different suits.</p>
                        <p>3<span class="fas fa-spade text-warning mx-1"></span>3<span class="fas fa-diamond text-info mx-1"></span>3<span class="fas fa-heart text-danger mx-1"></span> is a set of 3’s from three different suits.</p>
                        <p>6<span class="fas fa-heart text-danger mx-1"></span>6<span class="fas fa-spade text-warning mx-1"></span>6<span class="fas fa-diamond text-info mx-1"></span>Q<span class="fas fa-club text-success mx-1"></span> is a set of 6’s, with Q<span class="fas fa-club text-success mx-1"></span>  being the wild card to replace 6<span class="fas fa-club text-success mx-1"></span></p>
                        <p>8<span class="fas fa-heart text-danger mx-1"></span>8<span class="fas fa-spade text-warning mx-1"></span>Q<span class="fas fa-club text-success mx-1"></span>  is a set of 8’s with Q<span class="fas fa-club text-success mx-1"></span>  the wild card to replace 8<span class="fas fa-club text-success mx-1"></span></p>
                    </div>
                    <div class="tab-pane fade" id="h3" role="tabpanel" aria-labelledby="t3" style="background: #fff;">
                        <h5 id="3" class=" text-black1  " style="margin-bottom: 0;">POINTS RUMMY</h5>
                        <p>In points rummy, 2 to 6 players can play the game. Typically, it only lasts for about 2 or 3 minutes. The decks of cards used are dependant on the type of table chosen.
                        </p>
                        <p>The game lasts only for one deal. The points have a predetermined value. In order to win, the player must lay down a pure sequence, an impure sequence and the rest into melds. The first person to discard all of his cards wins all the cash brought to the table.</p>
                        <p>Calculation of points:</p>
                        <ul>
                            <li>2,3,4,5,6,7,8 and 9 of any suit carry their face value.</li>
                            <li>Ace, Jack, Queen and King of any suit carry 10 points.</li>
                            <li>The winner gains the sum of the value of the remaining cards of the other players. This is the reason why the players must hold onto cards with the lowest face value.</li>
                        </ul>
                    </div>
                    <div class="tab-pane fade" id="h4" role="tabpanel" aria-labelledby="t4" style=" background: #fff;">
                        <h5 id="4" class=" text-black1" style=" margin-bottom: 0;">DEAL RUMMY</h5>
                        <p>In deal rummy, the players decide to play on a fixed number of deals. Each player is allotted a certain number of chips. The game play is similar to that of points rummy. The number of decks of cards used depends on the table. The winner gets the chips lost by the other players. At the end of each deal, the winner gets the chips according to the value of the cards his opponent owns.
                        </p>
                        <p>
                            Calculation of points:
                        </p>
                        <ul>
                            <li>Face cards carry their face value.</li>
                            <li>Ace, Jack, Queen and King of any suit carry 10 points.</li>
                        </ul>
                    </div>
                    <div class="tab-pane fade" id="h5" role="tabpanel" aria-labelledby="t5" style=" background: #fff;">
                        <h5 id="5" class=" text-black1" style=" margin-bottom: 0;">POOL RUMMY</h5>
                        <p>Pool Rummy has two variations, 101 pool rummy and 201 pool rummy. 101 and 201 points are the predetermined points of elimination. The player reaching the points of 101 and 201 in the respective games must leave the game and the player who remains at the table with the least amount of points at the end is the winner. The winner is credited with the entry fee paid by all the players.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>registerCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>registerService.js?r=<?= time() ?>"></script> 