<?php if (is_logged_in()) { ?>
    <?php $this->load->view('includes/menu-dashboard') ?>
<?php } ?>

<?php if (!is_logged_in()) { ?>
    <?php $this->load->view("includes/header"); ?>
    <?php $this->load->view('menu') ?>
<?php } ?>

<section class="innerpages py-5">
    <div class="container">
        <div class="row" style=" background: #fff; padding: 10px 0;">
            <div class="col-12 text-white">
                <div class="editor">
                    <h5 class=" text-black1" style=" margin-bottom: 0;">Withdraw Charges Info</h5>
                    <?=$content->description?>
                </div>
            </div>
        </div>
    </div>
</section>