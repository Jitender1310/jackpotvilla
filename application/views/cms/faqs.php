<?php if (is_logged_in()) { ?>
    <?php $this->load->view('includes/menu-dashboard') ?>
<?php } ?>

<?php if (!is_logged_in()) { ?>
    <?php $this->load->view("includes/header"); ?>
    <?php $this->load->view('menu') ?>
<?php } ?>

<section class="py-4" ng-controller="faqsCtrl">
    <div class="container" style=" background: #fff; padding: 10px 20px;" >
        <h3 class="mb-4">FAQ's</h3>
        <div class="row">
            <div class="col-12">
                <div  id="groups">
                    <div class="card bg-primary border-0 mb-3" ng-repeat="item in faqs track by $index" ng-init="groupindex = $index">
                        <div class="card-header p-0" id="h-{{groupindex}}">

                        <div ng-if="groupindex == 0">
                        <button class="btn btn-l1 btn-link btn-block text-left text-white h5 m-0" type="button" data-toggle="collapse" aria-expanded="true" data-target="#g-{{groupindex}}" aria-controls="c-{{groupindex}}">
                                <img src="{{item.icon}}" width="30" class="mr-2" >  {{item.category}}
                            </button>
                        </div>



                        <div ng-if="groupindex != 0">
                            <button class="btn btn-l1 btn-link btn-block text-left text-white h5 m-0" type="button" data-toggle="collapse" aria-expanded="false" data-target="#g-{{groupindex}}" aria-controls="c-{{groupindex}}">
                                    <img src="{{item.icon}}" width="30" class="mr-2" >  {{item.category}}
                            </button>
                        </div>


                        </div>
                        <div id="g-{{groupindex}}" class="collapse" ng-class="{'show':groupindex == 0}" aria-labelledby="h-{{groupindex}}"  data-parent="#groups">
                            <div class="card-body bg-white">
                                <div  id="faq-{{groupindex}}">
                                    <div class="card bg-white" ng-repeat="item in item.faq track by $index" ng-init="qindex = $index">
                                        <div class="card-header bg-white p-0" id="hq-{{qindex}}">

                                        <div ng-if="qindex == 0">


                                        <button class="btn btn-l2 btn-link btn-block text-left text-dark h5 m-0" type="button"  aria-expanded="true"  data-toggle="collapse" data-target="#q-{{qindex}}" aria-controls="cq-{{qindex}}">
                                            <span style="color: black;"> {{item.title}}</span>
                                        </button>
                                        </div>
                                        <div ng-if="qindex != 0">

                                        <button class="btn btn-l2 btn-link btn-block text-left text-dark h5 m-0" type="button"  aria-expanded="false"  data-toggle="collapse" data-target="#q-{{qindex}}" aria-controls="cq-{{qindex}}">
                                            <span style="color: black;"> {{item.title}}</span>
                                        </button>

                                        </div>


                                        </div>
                                        <div id="q-{{qindex}}" class="collapse" ng-class="{'show':qindex == 0}" aria-labelledby="hq-{{qindex}}" data-parent="#faq-{{groupindex}}">
                                            <div class="card-body">
                                                {{item.description}}
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

       <div class="text-center mt-4">
       <img src="<?= STATIC_IMAGES_PATH ?>/enq001.png"  height="40" class="mb-2" >
       <strong class="d-block text-muted">For any queries you can contact us on : <?= WEBSITE_CONTACT_NUMBER ?><br/>
Or you can write us on : <?= CONTACT_EMAIL ?></strong>
       </div>

        
    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>faqsCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>faqsService.js?r=<?= time() ?>"></script>