<?php if (is_logged_in()) { ?>
    <?php $this->load->view('includes/menu-dashboard') ?>
<?php } ?>

<?php if (!is_logged_in()) { ?>
    <?php $this->load->view("includes/header"); ?>
    <?php $this->load->view('menu') ?>
<?php } ?>
<section class="innerpages py-5">
    <div class="container">
        <div class="row" style=" background: #fff;">
            <div class="col-12 text-white">
                <?=$content->description?>
            </div>
        </div>
    </div>
</section>