<?php if (is_logged_in()) { ?>
    <?php $this->load->view('includes/menu-dashboard') ?>
<?php } ?>

<?php if (!is_logged_in()) { ?>
    <?php $this->load->view('menu') ?>
<?php } ?>

<section class="innerpages py-5">
    <div class="container">
        <div class="row" style=" background: #fff; padding: 10px 0;">
            <div class="col-12 text-white ">
            <h2  class="text-center pt-4" style="color: #c0392b"> <b>Terms and Conditions</b></h2>
                <div class="editor p-5">
                <div class="container" style="border: 1px solid #ccc; border-radius: 8px">
                    <?=$content->description?>
                <div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>registerCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>registerService.js?r=<?= time() ?>"></script>