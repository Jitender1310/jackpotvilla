<?php if (is_logged_in()) { ?>
    <?php $this->load->view('includes/menu-dashboard') ?>
<?php } ?>

<?php if (!is_logged_in()) { ?>
    <?php $this->load->view('menu') ?>
<?php } ?>



<section class="innerpages py-5">
    <div class="container">
        <div class="row" style="background: #fff;
             padding: 20px 0;">
            <div class="col-lg-3 col-md-3 col-xs-12">
                <ul class="nav nav-stabs flex-column" role="tablist" style="border: 1px solid #cecece;">
                    <li class="nav-item">
                        <a class="nav-link active show" id="t1" data-toggle="tab" href="#h1" role="tab" aria-controls="h1" aria-selected="true">PROMOTIONS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="t2" data-toggle="tab" href="#h2" role="tab" aria-controls="h2" aria-selected="false">THE RUMMY DICTIONARY:</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="t3" data-toggle="tab" href="#h3" role="tab" aria-controls="h3" aria-selected="false">MOBILE VERIFICATION</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="t4" data-toggle="tab" href="#h4" role="tab" aria-controls="h4" aria-selected="false">Tournaments</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="t5" data-toggle="tab" href="#h5" role="tab" aria-controls="h5" aria-selected="false">BONUS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="t6" data-toggle="tab" href="#h6" role="tab" aria-controls="h6" aria-selected="false">SPLIT</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="t7" data-toggle="tab" href="#h7" role="tab" aria-controls="h7" aria-selected="false">Technical Requirements:</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="t8" data-toggle="tab" href="#h8" role="tab" aria-controls="h8" aria-selected="false">Mobile Experience:</a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-9 col-md-9 col-xs-12 text-white">
                <div class="tab-content  stab-content editor">

                    <div class="tab-pane fade active show" id="h1" role="tabpanel" aria-labelledby="t1" style=" background: #fff;">

                        <div class="card  card-gold">
                            <div class="card-header card-header2 text-white">PROMOTIONS</div>

                            <div class="p-4">
                                <div class="swiper-container promotions-slider">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <div class="item rounded" style="background-image: url(<?= STATIC_IMAGES_PATH ?>promo.png);"></div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="item rounded" style="background-image: url(<?= STATIC_IMAGES_PATH ?>promo1.png);"></div>
                                        </div>
                                    </div>
                                    <div class="promotions-slider-pagination text-center"></div>
                                </div>
                            </div>
                            <h5  style="color: #000;">PROMOTIONS</h5>

                            <p>All the details of all our promotions are available in the “Promotions�? section on the Website. Each promotion has its own Terms and Conditions, which are clearly explained on the respective promotion pages. All the cash bonuses and deposit bonuses have additional terms and conditions, which may differ from offer to offer in accordance with the applicable bonus plans. Any Bonus offer may not be claimed in conjunction with any other bonus offers offered by A2Z Betting at the time. Once you activate the bonus code, any bonus codes and offers being utilized by you previously will become invalid and inactive automatically and you will stop receiving their benefits. You will only receive disbursements for the active bonus code.</p>
                            <p>All the games, contests, bonuses and cashback offers in the “Promotions�? section may be discontinued or cancelled without any prior notice. A2Z Betting is not responsible or liable for any such cancellations and will not pay the Users any compensation whatsoever except refunding the applicable entry fee. The decision of the A2Z Betting Management shall be final and binding in case of any dispute</p>


                        </div>

                    </div>
                    <div class="tab-pane fade" id="h2" role="tabpanel" aria-labelledby="t2" style=" background: #fff;">
                        <h5 style="color: #000;">THE RUMMY DICTIONARY: </h5>
                        <p>For our rummy novices, we have developed a dictionary that will help you understand the rummy lingo better. These terms are frequently used on A2Z Betting.</p>


                        <ul>
                            <li>Rummy- Rummy is a card melding game of 2-6 members.</li>
                            <li>13 cards rummy- It is a popular type of rummy in which each player is dealt 13 cards.</li>
                            <li>101 pool rummy- It is a type of Pool rummy, in which a player gets eliminated on reaching 101 points.</li>
                            <li>201 pool rummy- It is a type of Pool rummy in which a player gets eliminated on reaching 201 points.</li>
                            <li>Ace- Ace is the card that is marked A. It carried 10 points and can make runs of A, 2, 3 and King, Queen, Ace.</li>
                            <li>Cash game- A cash game requires you to deposit a certain amount to play with it.</li>
                            <li>Points- Each card holds certain points. For example, the face cards, Jack, King, Queen and Ace carry 10 points each.</li>
                            <li>Chips- Chips are virtual money. You can use chips to play practice games until you get a hold of the game to play cash rummy.</li>
                            <li>Sequence- It is a group of three consecutive cards of the same suit. 2<span class="fas fa-spade text-warning mx-1"></span>3<span class="fas fa-spade text-warning mx-1"></span>4<span class="fas fa-spade text-warning mx-1"></span>5<span class="fas fa-spade text-warning mx-1"></span> is an example of a sequence.</li>
                            <li>Set- A set is a group of three or four cards with the same number but of different groups.</li>
                            <li>Pure sequence- A pure sequence is a sequence made without using a joker.</li>
                            <li>Impure sequence- A sequence made from using a joker.</li>
                            <li>Best of two- It is a type of Deals rummy in which the players play two rounds of the game. The player with the highest number of chips at the end is the winner.</li>
                            <li>Best of three- It is a type of Deals rummy in which the players play three rounds of the game. The player with the highest number of chips at the end is the winner.</li>
                            <li>Buy in- It is the entry fee or the cash amount the player has to pay before the game begins.</li>
                            <li>Open deck- This is the discard pile. The player can draw a card from either the open deck or the closed deck.</li>
                            <li>Closed deck- This is the pile of cards left unflipped after dealing the cards. The player can draw a card from either the open deck or the closed deck.</li>
                            <li>Deal- It refers to the distribution of cards to the players at the beginning.</li>
                            <li>Dealer- this is the person who deals the cards. He is seen in a green circle and will be the last one to get his turn.</li>
                            <li>Deals rummy- it is a type of rummy in which the game is predetermined to be played for 2 or 3 rounds. The player with the highest amount of chips at the end will be the winner.</li>
                            <li>Face cards- They refer to Jack, King, Queen and Ace cards. They carry 10 points each.</li>
                            <li>Hand- Refers to the cards the player gets in the beginning of the game.</li>
                            <li>Meld- a meld is any possible combination, i.e., a set or a run.</li>
                            <li>Melding- The act of forming a meld.</li>
                            <li>Winner- the player who melds all of his cards first.</li>
                            <li>Csh tournaments- The player deposits money to play these tournaments and win money.</li>
                            <li>Free tournaments- the player does not have to pay any entry fee. The first deposit makes him eligible to play these tournaments.</li>
                            <li>Declare- the act of melding all the cards and laying them down.</li>
                            <li>Draw- The player must take a card from the open or closed piles. This action is called drawing.</li>
                            <li>Drop- This is the option that allows the player to quit at any time during the game.</li>
                            <li>Joker- It is the card that allows you to replace any card and make an impure sequence. It can be a wild card or a printed joker.</li>
                            <li>Show- When a player has successfully melded two sequences, with at least one of them being a pure sequence, he can make a show of his cards.</li>
                            <li>Unmatched cards- the cards that do not form a sequence. When a player declares, the loss of the other players is calculated by summing the value of the unmatched cards.</li>
                            <li>Wild card joker- Any card selected randomly can be used as a joker. If the selected cards is a 6, all 6’s can be used as wild card jokers.</li>
                            <li>Printed joker- the card that is available in the deck which can be used to make an impure sequence.</li>
                            <li>Shuffle- the act of mixing the cards after every game is known as shuffling.</li>
                        </ul>
                    </div>
                    <div class="tab-pane fade" id="h3" role="tabpanel" aria-labelledby="t3" style="background: #fff;">
                        <h5  style="color: #000;">MOBILE VERIFICATION</h5>

                        <p> In order to play practice or cash games, the user must provide and validate his/her mobile number. This measure is to make sure that fraud of any kind is prohibited at the grass root level. This information is kept confidential from third parties and it is absolutely mandatory to avail cash game services.
                        </p>


                        <p>If this information is deemed absolutely necessary by the law or an authoritative body, A2Z Betting might have to disclose this to comply with law. You will be informed prior to the disclosement. A2Z Betting has a tight security system that denies access to our users’ information that is under our protection.
                        </p>

                        <p>If a user has reason to believe that the information that is under our protection is compromised or is being misused, it is the user’s duty to immediately notify the authority, i.e., the customer service of A2Z Betting, who will take immediate action against privacy breach. You are advised to keep the information under your control safe, failing of which, A2Z Betting shall not be held responsible
                        </p>

                        <p>One can also change the information that they have provided once they register with our services. You can go to account settings and change or add details.
                        </p>

                        <p>The privacy policy and terms and conditions of A2Z Betting are complementary. You are advised to read the terms and conditions before you register with us. These policies are governed by the laws of india.
                        </p>
                    </div>
                    <div class="tab-pane fade" id="h4" role="tabpanel" aria-labelledby="t4" style=" background: #fff;">


                        <div class="py-4">
                            <h1 class="text-warning text-center m-0"style="color: #f00 !important;">Tournaments</h1>
                        </div>
                        <div class="py-4">
                            <div class="row">
                                <?php foreach ($tournament_categories_data as $row){ ?>
                                <div class="col-4 text-center my-3">
                                    <img src="<?php echo $row->image ?>"style=" width: 100%;">
                                    <h6 style=" color: #000; font-size: 21px; padding: 0;"><?php echo $row->name ?></h6>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="embed-responsive embed-responsive-21by9 mt-4">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Gij_jOGHDIo"></iframe>
                        </div>

                    </div>
                    <div class="tab-pane fade" id="h5" role="tabpanel" aria-labelledby="t5" style=" background: #fff;">
                        <h5 style="color: #000;">BONUS</h5>

                        <ul>
                            <li></li>
                            <li>Refer a friend bonus:The refer a friend bonus of A2Z Betting is very generous. Read more about Refer a Friend bonus on Bonuses.</li>
<!--                            <li>Register Bonus:A2Z Betting also encompasses the Register Bonus. On registering with A2Z Betting, you can now receive a bonus of 100 Rupees. Once you register, sign up to receive Rs.50. Furthermore, you can verify your registration using email and SMS, for which your user account will be credited with Rs.25 each, credititing you with a total of Rs.100.</li>
                            <li>Birthday Bonus:You can now receive a Birthday Bonus. You will receive Rs.100 in your cash account on your birthday. Failing to use this amount within a week, the bonus will expire and the same amount will be debited from your account.</li>-->
                            <li>Welcome Bonus:When a user registers with A2Z Betting and makes an initial deposit, his/her bonus account will be credited with the same amount as the welcome bonus, unless the deposit amount is higher that Rs.2000. In such cases, the user will be credited with Rs.2000 even if the deposit amount is higher.</li>
                        </ul>

                        <p>For example, if a player initially deposits Rs.100, he/she will receive a welcome bonus of Rs.100 in their bonus account.
                        </p>
                        <p>Also, if a player’s initial deposit is Rs.3000, his/her bonus account will be credited with Rs.2000 only. </p>
                        <p>When a user wins a game, 5% of the amount from their bonus account will be transferred from his/her bonus account to their cash account. This amount can be used to play more cash games.</p>

                    </div>
                    <div class="tab-pane fade" id="h6" role="tabpanel" aria-labelledby="t6" style=" background: #fff;">
                        <h5 style="color: #000;">SPLIT</h5>
                        <h6>How does the split option work in Pool Rummy? </h6>
                        <p>In rummy, the split option is used when there are only two or three players left at a table when the game started with more than three players. There are two types of split options in a rummy game:</p>


                        <ul>
                            <li>Auto Split - Auto split is again divided into two-player and three-player auto splits.
                                <ul>
                                    <li>Two player auto split - This option is applicable to 101 and 201 pool tables when the game started with two or more people.


                                        <ul>
                                            <li>101 pool table - the Auto Split option works when only two players are left at the table, and each of them has a game count of 80 or higher. When the auto split is used, the prize is automatically split between the two players. For example, if Player A has 90 points and Player B has 85 points, the prize is equally distributed between Players A and B.</li>
                                            <li>201 pool table - the Auto Split option works only when two players are left at the table, each of them holding a game count of 175 or higher. When the auto split is used, the prize is automatically split between the two players. For example, Player A has a game count of 185 and Player B has a count of 190, the prize will be equally distributed between Players A and B</li>
                                        </ul>



                                    </li>
                                    <li>Three player Auto Split- this is applicable to 101 and 201 pool tables which have started off with more than three players.

                                        <ul>
                                            <li>101 pool tables - The auto split option works only when three players are left at the table and each of them have a count of 80 or higher. When the auto split is used, the prize is automatically split between the two players. For example, Player A has a game count of 90, Player two has a count of 85 and Player C has a count of 100 points, the prize will be equally distributed between the three players.</li>
                                            <li>201 pool tables - This is applicable when there are three players left at the table with each player holding a game count of more than 175 points. In such a case, the prize will be equally distributed among the players. For example, if Players A, B and C hold game points of 180, 190 and 200 respectively, the prize will be distributed equally among the players.</li>

                                        </ul>

                                    </li>
                                </ul>

                            </li>
                            <li>Manual Split: There are different sets of rules for two-player and three-player manual splits.
                                <ul>
                                    <li>Two player manual split - this is applicable to 101 and 201 pool tables where the game started off with more than two players</li>
                                    <li>Three player manual split - this is applicable to 101 and 201 pool tables where the game started off with more than three players</li>
                                </ul>


                            </li>
                        </ul>

                        <p>In a 101 pool game, the manual split option can be used when the total score of the players is greater than 60. In a 201 pool game, the manual split option can be used when the total score of the players is greater than 150. It is applicable if the difference in the number of drops possible for any two or three players is not greater than 2 during a game.
                        </p>

                        <h6>How is the prize split processed?</h6>

                        <p>Each player’s remaining drop will be equated to the player’s who has the least number of remaining drops. The excess drops of each player will be paid off from the prize pool as follows:</p>

                        <h6>Number of excess drops x Entry fee</h6>
                        <p>For example, Players A, B, C and D join a 101 pool table but only A, B and C remain as D drops out. If all the three players agree to split the prize, the pool prize will be distributed according to each player’s remaining number of drops.</p>
                        <p>If player A has 2 drops left, player B has 1 drop left and player C has 0 drops left, the calculation of the prize pool will be as follows:
                        </p>

                        <ul>
                            <li>Prize pool - Rs.300</li>
                            <li>1 drop - Rs.50</li>
                            <li>Player A - 2 Drops</li>
                            <li>Player B - 1 drop</li>
                            <li>Player C - 0 drops</li>
                        </ul>

                        <p>Since Players A and B have more number of drops than C does, they get a bigger portion of the prize pool.</p>

                        <ul>
                            <li>Player A- 2 x 50 = Rs.100</li>
                            <li>Player B - 1 x 50 = Rs.50</li>
                            <li>Player C - 0 x 50 = 0\</li>
                            <li>Remaining prize pool = 300 - 100 - 50 = Rs. 150</li>
                            <li>Remaining prize pool distribution = 150/3 = Rs. 50 each.</li>
                            <li>Player A = 150 + 50 = Rs. 200</li>
                            <li>Player B = 50 + 50 = Rs.100</li>
                            <li>Player C = Rs.50</li>
                        </ul>


                        </h5>

                        <p>Firstly, install Windows EXE file extension on your personal computer or your laptop to run the A2Z Betting app. You can always install the A2Z Betting app on your iOs or android mobile to play the game. Choose the platform of your choice and enter the world of rummy enthusiasts.</p>

                        <p>On Windows: One can easily install the A2Z Betting app on their PC or laptop. You will need a windows powered device with the latest software, good processor and a good screen resolution for an out of the world gaming experience.
                        </p>

                        <h6>The App on iOS, Android and Windows:</h6>

                        <p>After the declaration of Rummy being a skill based game by Supreme Court, the popularity and marketability of the game has skyrocketed. People desired a platform that is easily accessible and feasible. A2Z Betting works to satisfy this desire of the players to avail the best gaming experience there is. You can now install the A2Z Betting app and play the game you love on the move.
                        </p>

                        <p>The new app will allow you to play any variant of the rummy game- Pool Rummy, Points Rummy and deals rummy along with various tournaments. Now available on your smartphones.
                        </p>
                        <p>Try out the A2Z Betting app now!
                        </p>

                        <p>How to download the A2Z Betting app on Android:</p>
                        <p>You can choose the most convenient method to download the app:</p>

                        <ul>
                            <li>Direct Download - Click on the ‘Download Now’ option to download the app instantly.</li>
                            <li>SMS Download - Enter your mobile number on the top banner. You will receive an SMS from A2Z Betting. Open the link and download the app.</li>
                            <li>QR Code Download - Scan the QR code on the website. You will be redirected to the play store. Download the add.</li>
                            <li>Missed Call download- Give a missed call to ___________. You will receive a link to your mobile number. Open the link and download the app.</li>
                        </ul>

                        <p>How to download A2Z Betting on iOS:</p>
                        <p>You can choose the most convenient method to download the app:</p>

                        <ul>
                            <li>App store Download - go to the A2Z Betting app store. Click on the app and download the app.</li>
                            <li>SMS download - Enter your mobile number on the top banner. You will receive an SMS from A2Z Betting. Open the link and download the app.</li>
                            <li>QR code Download - Scan the QR code given on the website. You will be redirected to the app store. Download the app.</li>
                            <li>Missed call Download - Give a missed call to __________. You will receive a link to your mobile number. Open it and download the app.</li>
                        </ul>
                    </div>
                    <div class="tab-pane fade" id="h7" role="tabpanel" aria-labelledby="t7" style=" background: #fff;">
                        <h5  style="color: #000;"><b> Technical Requirements:</b>
                        </h5>

                        <table class="table table-bordered text-center" style=" color: #000;">
                            <thead>
                                <th>Configuration</th>

                                <th>Minimum requirements
                                </th>

                            </thead>
                            <tr>
                                <td>Android</td>
                                <td>6 and above</td>
                            </tr>
                            <tr>
                                <td>iOS</td>
                                <td>9 and above</td>
                            </tr>
                            <tr>
                                <td>Processor Speed</td>
                                <td>1200 MHz</td>
                            </tr>
                            <tr>
                                <td>Screen Resolution</td>
                                <td>1280 × 1024</td>
                            </tr>



                        </table>

                    </div>
                    <div class="tab-pane fade" id="h8" role="tabpanel" aria-labelledby="t8" style=" background: #fff;">
                        <h5  style="color: #000;">Mobile Experience:</h5>

                        <p>The availability of A2Z Betting on various versions of Android and iOS mobiles makes it greatly convenient and accessible to the players on the move. You can now play your favourite game while travelling, waiting or in your hour of boredom. It is only one click away.
                        </p>
                        <p>
                            You do not need companions, cards or a place to play. Neither do you need a computer system or a laptop. All you need is your smartphone with good internet speed. Play practice or cash games. If you wish to play cash games, pay using PayTm, debit or credit cards. Withdrawal is equally easy. Here’s why you must experience the A2Z Betting mobile app:
                        </p>

                        <ul>
                            <li>It is faster and easier to access.</li>
                            <li>You can play free of cost or make the most out of cash games</li>
                            <li>You will play with real players from across the country</li>
                            <li>Choose and play the rummy variant of your choice</li>
                            <li>Play on upto 3 tables at once.</li>
                            <li>Win real money from the app</li>
                            <li>Enjoy an advertisement free experience</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>registerCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>registerService.js?r=<?= time() ?>"></script>