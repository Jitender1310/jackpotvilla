<?php if (is_logged_in()) { ?>
    <?php $this->load->view('includes/menu-dashboard') ?>
<?php } ?>

<?php if (!is_logged_in()) { ?>
    <?php $this->load->view("includes/header"); ?>
    <?php $this->load->view('menu') ?>
<?php } ?>

<section class="innerpages py-4">
    <div class="container">
        <div class="row">
            <div class="col-12 text-white" style="background: #fff; padding: 10px 10px;">

                <div class="editor">
                    <div class=" row">
                    <div class=" col-md-4">
                                            <h5 style=" color: #000;font-size: 28px; margin-bottom: 4px;">Refund policy</h5>

                    <img src="<?= STATIC_IMAGES_PATH ?>about-us-banner.png" class="img-fluid mb-4">
                    </div>
                        <div class=" col-md-8" style="padding-top: 43px;">
                            <?=$content->description?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>registerCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>registerService.js?r=<?= time() ?>"></script>   