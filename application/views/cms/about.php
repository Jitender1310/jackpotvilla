<?php if (is_logged_in()) { ?>
    <?php $this->load->view('includes/menu-dashboard') ?>
<?php } ?>

<?php if (!is_logged_in()) { ?>
    <?php $this->load->view('menu') ?>
<?php } ?>

<section class="innerpages py-4">
    <div class="container">
        <div class="row">
            <div class="col-12 text-white" style="background: #fff; padding: 10px 10px;">

                <div class="editor px-5 pb-4">
                    <h2  class="text-center pt-4">About us</h2>

                    <div class="container" style="border: 1px solid #ccc; border-radius: 8px">
                    <div class=" row">
                    <!--<div class=" col-md-4">

                     <img src="<?= STATIC_IMAGES_PATH ?>about-us-banner.png" class="img-fluid mb-4"> 
                    </div>-->
                        <div class=" col-md-12">
                            <!-- <?=$content->description?> -->
                            <!-- <div class=" col-md-8" style="padding-top: 43px;"> -->
                    <!-- <p style="color:#000;">About Us: Here at Rummy Desk, we provide a vivid gaming experience to our fellow Rummy enthusiasts. Come join our arena to play against the best, in a fair and safe play and win exciting rewards and cash prizes! We sell to you the best stage for us rummy lovers and potential rummy geniuses. Rummy Desk brings to you, a shift from conventional gaming sites, to a much more thrilling and sophisticated card game experience.
                        Love for rummy is only showing an upward trend in India and we, at Rummy Desk hope to help you realize your passion for the game we love. With only access to the Internet, you can now permanently say goodbye to boredom and play at any time of the say. All you have to do is register now, play tournaments and win money!
                        Not only can you win spectacular prizes and cash money, you can now communicate with your fellow rummy lovers and draw ticks and tips from them.</p>
                    <p style="color: #000;">Our friendly customer service ensures top class service to our loyal users. Financial transactions are transparent and well accounted for. Learn more about us in our Support and Security.</p> -->

                    <ul>
                        <li>Sky Infotech Limited is one of the upcoming providers for online gaming entertainment across Sports Betting, Online and Live Casino operating in the emerging and the regulated markets.</li>
                        <li>We aim to utilize the latest technologies to provide innovative and interactive gaming experiences in a secure environment.</li>
                        <li><b>We have dedicated ourselves to offering our customers a seamless and thrilling gaming experience while you are on the go. We aim to provide an exceptional and fully customizable online betting experience.</b></li>
                        <li><b>We are innovative, ambitious and passionate about what we do. We do it in a credible and responsible way, always aiming for the top.</b></li>
                        <li>We only operate in regulated markets where we hold the appropriate licenses. We take our responsibilities to customers and our other stakeholders seriously and place great emphasis on working to a ‘compliance first’ model across the business.</li>
                        <li><b>Dedicated Customer Service Team: </b>We are here for you every step of the way with dedicated customer service managers standing by to provide you with a 24/7 top notch customer care service, handling any issues quickly and efficiently.</li>
                        <li>When customers bet on our site they can rest assured that they are getting a wide variety of betting options, up to date information and the best odds available.</li>
                        <li>
                        Our customers also have peace of mind, knowing that when it’s time to collect, they are betting with a well-known reputable company.
                        </li>
                        <li>We have integrated best and secured payment methods on our site and a transaction process that is quick, easy enabling our players to cash out their winnings quickly and securely.</li>
                    </ul>

                    </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>registerCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>registerService.js?r=<?= time() ?>"></script>   