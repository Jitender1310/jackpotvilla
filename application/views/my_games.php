<?php $this->load->view('includes/menu-dashboard') ?>
<section class="innerpages py-5">
    <div class="container">
        <?php include'chips.php'; ?>
        <div class="row mt-4">
           <?php $this->load->view('includes/dashboard-sidebar') ?>
            <div class="col-9">
                <div class="card card-gold h-100">
                    <div class="card-header">MY Joined GAMES</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <div class="card card-gold bg-grey h-100 shadow-lg">
                                    <div class="card-header">

                                        <div class="row align-items-center">
                                            <div class="col">ACTIVE GAMES</div>
                                            <div class="col-auto">
                                                <svg height="20px" viewBox="0 0 38 38"><use xlink:href="#info" /></svg>

                                            </div>
                                        </div>


                                    </div>
                                    <div class="card-body text-white">
                                        <div class="row align-items-center my-3">
                                            <div class="col">
                                                <strong class="d-block text-warning">Pool Rummy</strong>
                                                <small>(2p-101-Rs. 25)</small>
                                            </div>
                                            <div class="col-auto">
                                                <button class="btn btn-success shadow">Join Now</button>
                                            </div>
                                        </div>
                                        <div class="row align-items-center my-3">
                                            <div class="col">
                                                <strong class="d-block text-warning">Points Rummy </strong>
                                                <small>(6p-2pts)</small>
                                            </div>
                                            <div class="col-auto">
                                                <button class="btn btn-success shadow">Join Now</button>
                                            </div>
                                        </div>
                                        <div class="row align-items-center my-3">
                                            <div class="col">
                                                <strong class="d-block text-warning">Deal Rummy</strong>
                                                <small>(2p-Best of 3-Rs. 200)</small>
                                            </div>
                                            <div class="col-auto">
                                                <button class="btn btn-success shadow">Join Now</button>
                                            </div>
                                        </div>





                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="card card-gold bg-grey h-100 shadow-lg">
                                    <div class="card-header">
                                        <div class="row align-items-center">
                                            <div class="col">TOURNAMENTS</div>
                                            <div class="col-auto">
                                                <svg height="20px" viewBox="0 0 38 38"><use xlink:href="#info" /></svg>

                                            </div>
                                        </div></div>
                                    <div class="card-body text-white">
                                        <div class="row align-items-center my-3">
                                            <div class="col">
                                                <strong class="d-block text-warning">Tournament Name 1 </strong>

                                            </div>
                                            <div class="col-auto">
                                                <button class="btn btn-success shadow">Join Now</button>
                                            </div>
                                        </div>  <div class="row align-items-center my-3">
                                            <div class="col">
                                                <strong class="d-block text-warning">Tournament Name 2 </strong>

                                            </div>
                                            <div class="col-auto">
                                                <button class="btn btn-success shadow">Join Now</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>


                </div>
            </div>
        </div>
    </div>
</section>