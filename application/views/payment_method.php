<style>
    .button_deposit {
	background-color: green;
	color: #fff;
	border: 0;
	border-radius: 4px;
    padding: 6px 12px !important;
    color: #f1f1f1 !important;
}

.button_deposit a {
    color: #f1f1f1 !important;
}

.button_deposit:hover {
	background-color: #2d982d;
}

.table th, .table td {
	vertical-align: middle !important;
	padding: 15px; 
}
.step_box_main {
display: -webkit-flex;
display: flex;
-webkit-justify-content: center;
justify-content: center;
}
.step_box, .step-box{
background: #f0f0f0;
width: 100%;
border-radius: 10px;
padding: 15px;
margin: 10px;
position: relative;
}
.steps_count1 {
text-align: center;
font-size: 2em;
color: #333;
width: 50px;
height: 50px;
display: -webkit-flex;
display: flex;
-webkit-align-items: center;
align-items: center;
-webkit-justify-content: center;
justify-content: center;
margin: auto;
/* background: #ae0001; */
border-radius: 5px;
margin-bottom: 8px;
}
.steps_txt {
text-align: center;
}

.step_box:after {
content: '\2192';
font: normal normal normal 14px/1 FontAwesome;
font-weight: 800;
position: absolute;
right: -17px;
top: 0;
bottom: 0;
display: flex;
align-items: center;
justify-content: center;
}


@media screen and (max-width: 767px){
.step_box_main {
-webkit-flex-wrap: wrap;
flex-wrap: wrap;
}
.step_box:after {
content: "\2193";
font: normal normal normal 14px/1 FontAwesome;
position: absolute;
right: 0;
left: 0;
top: initial;
bottom: -18px;
}
.table-responsive {
		width: 100% !important;
		box-sizing: border-box !important;
		overflow: scroll !important;
	}
}
</style>

<?php if (is_logged_in()) { ?>
    <?php $this->load->view('includes/menu-dashboard') ?>
<?php } ?>

<?php if (!is_logged_in()) { ?>
    <?php $this->load->view('menu') ?>
<?php } ?>

<section class="innerpages py-5">
    <div class="container">
        <div class="row" style="background: #fff;padding: 25px;">
        <h4 class="d-block mx-auto" style="color: #c0392b; text-align: center"><b>Payment Method</b></h4>

        <div class="container-fluid">
        At A2Z Betting, you can choose from among today’s most popular payment methods to make a deposit and start playing our real-money games.

        We have several Payment options, all secure in their own way. Please review the list of options below to get started. Whichever method you select, you can rest assured that we only partner with the most globally trusted names in payment processing.<br>

        <!-- <button class="button_deposit d-block mb-4 mx-auto">Deposit now</button> -->

        <?php if (is_logged_in()) { ?>
        <button class="button_deposit d-block mb-4 mx-auto">
        <a class="nav-link  <?= $page_active == "deposit" ? "active" : "" ?>" href="<?= base_url() ?>deposit">Deposit now</a></button>
        <?php } ?>
        </div>
        <div class="table-responsive text-center">
            <table class="table table-bordered">
            <thead class="thead-dark">
                <tr>
                <th scope="col" colspan="2">Payment Method</th>
                <th scope="col">Min</th>
                <th scope="col">max</th>
                <th scope="col">Credit</th>
                <th scope="col">Free of Charge</th>
                </tr>
            </thead>
            <tbody>
            <tr>
                <th rowspan="2" style="background-color: #ccc"><img src="<?= STATIC_IMAGES_PATH ?>/astropay-card.png" style="width: 125px" alt=""></th>
                <td>Deposit</td>
                <td>500</td>
                <td>No Limit</td>
                <td>Instant</td>
                <td><i class="fa fa-check-circle fa-lg" aria-hidden="true" style="heigth: 40px !important; color:#2f0060"></i></td>
            </tr>
            <tr>
                <td>Withdrawal</td>
                <td>1000</td>
                <td>1,00,000</td>
                <td>Instant</td>
                <td><i class="fa fa-check-circle fa-lg" aria-hidden="true" style="heigth: 40px !important; color:#2f0060"></i></td>
            </tr>
            <tr>
                <th rowspan="2" style="background-color: #333"><img src="<?= STATIC_IMAGES_PATH ?>/cash_icon.svg" style="height: 80px" alt=""></th>
                <td>Deposit</td>
                <td>500</td>
                <td>No Limit</td>
                <td>Instant</td>
                <td><i class="fa fa-check-circle fa-lg" aria-hidden="true" style="heigth: 40px !important; color:#2f0060"></i></td>
            </tr>
            <tr>
                <td>Withdrawal</td>
                <td>1000</td>
                <td>1,00,000</td>
                <td>Instant</td>
                <td><i class="fa fa-check-circle fa-lg" aria-hidden="true" style="heigth: 40px !important; color:#2f0060"></i></td>
            </tr>
            <tr>
                <th rowspan="2" style="background-color: #333"><img src="<?= STATIC_IMAGES_PATH ?>/net_banking.svg" style="width: 125px" alt=""></th>
                <td>Deposit</td>
                <td>500</td>
                <td>No Limit</td>
                <td>Instant</td>
                <td><i class="fa fa-check-circle fa-lg" aria-hidden="true" style="heigth: 40px !important; color:#2f0060"></i></td>
            </tr>
            <tr>
                <td>Withdrawal</td>
                <td>1000</td>
                <td>1,00,000</td>
                <td>Instant</td>
                <td><i class="fa fa-check-circle fa-lg" aria-hidden="true" style="heigth: 40px !important; color:#2f0060"></i></td>
            </tr>
            <tr>
                <th rowspan="2" style="background-color: #333"><img src="<?= STATIC_IMAGES_PATH ?>/skrill_logo_white.png" style="width: 100px" alt=""></th>
                <td>Deposit</td>
                <td>500</td>
                <td>No Limit</td>
                <td>Instant</td>
                <td><i class="fa fa-check-circle fa-lg" aria-hidden="true" style="heigth: 40px !important; color:#2f0060"></i></td>
            </tr>
            <tr>
                <td>Withdrawal</td>
                <td>1000</td>
                <td>1,00,000</td>
                <td>Instant</td>
                <td><i class="fa fa-check-circle fa-lg" aria-hidden="true" style="heigth: 40px !important; color:#2f0060"></i></td>
            </tr>
            <tr>
                <th rowspan="2" style="background-color: #ccc"><img src="<?= STATIC_IMAGES_PATH ?>/neteller-vector-logo.svg" style="width: 125px" alt=""></th>
                <td>Deposit</td>
                <td>500</td>
                <td>No Limit</td>
                <td>Instant</td>
                <td><i class="fa fa-check-circle fa-lg" aria-hidden="true" style="heigth: 40px !important; color:#2f0060"></i></td>
            </tr>
            <tr>
                <td>Withdrawal</td>
                <td>1000</td>
                <td>1,00,000</td>
                <td>Instant</td>
                <td><i class="fa fa-check-circle fa-lg" aria-hidden="true" style="heigth: 40px !important; color:#2f0060"></i></td>
            </tr>
            <tr>
                <th rowspan="2" style="background-color: #ccc"><img src="<?= STATIC_IMAGES_PATH ?>/INOVAPAY-logo.png" style="width: 125px" alt=""></th>
                <td>Deposit</td>
                <td>500</td>
                <td>No Limit</td>
                <td>Instant</td>
                <td><i class="fa fa-check-circle fa-lg" aria-hidden="true" style="heigth: 40px !important; color:#2f0060"></i></td>
            </tr>
            <tr>
                <td>Withdrawal</td>
                <td>1000</td>
                <td>1,00,000</td>
                <td>Instant</td>
                <td><i class="fa fa-check-circle fa-lg" aria-hidden="true" style="heigth: 40px !important; color:#2f0060"></i></td>
            </tr>
            </tbody>
            </table>
        </div>
    
    
    
            <h2 class="d-block mx-auto text-center mt-4">Informations</h2>

            <div class="step_box_main">
                <div class="step_box">
                <p class="steps_count1">1</p>
                <div class="steps_txt">Create a Real Money account where you can select your preferred currency</div>
                </div>
                <div class="step_box">
                <p class="steps_count1">2</p>
                <div class="steps_txt">Log in to your account and click on the Banking button to make your deposit transaction</div>
                </div>
                <div class="step_box">
                <p class="steps_count1">3</p>
                <div class="steps_txt">Withdrawalal processing time vary according to payment method</div>
                </div>
                <div class="step-box">
                <p class="steps_count1">4</p>
                <div class="steps_txt">For more details and help regarding payment options, you may contact our 24/7 Live Chat Support</div>
                </div>
                </div>
            </div>
</section>

<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>registerCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>registerService.js?r=<?= time() ?>"></script>

