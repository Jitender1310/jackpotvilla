<?php $this->load->view('includes/menu-dashboard') ?>

<section class="innerpages py-5" ng-controller="depositCtrl">
    <div class="container">
        <div class="overview-outer">
        <div class="row mt-4">
            <?php $this->load->view('includes/dashboard-sidebar') ?>
            <?php $this->load->view('includes/bonus_coupons_popup') ?>
            <div class="col-lg-9 col-xs-12 p-0 mt-2 mb-4 pl-2">
                <div class="card h-100 card-gold">
                    <div class="card-header card-header2 text-white">DEPOSIT CASH</div>
                    <form name="deposit-form" method="post" action="/deposit/submit">
                        <div class="card-body">
                            <div class="bordered-frame">
                                <div class="bf-body">
                                    <div class="row my-3">
                                        <div class="col"><strong>BTC</strong></div>
                                        <div class="col-auto">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="1" name="type" ng-model="preferencesObj.contact_by_sms" value="btc" class="custom-control-input">
                                                <label class="custom-control-label" for="1"></label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row my-3">
                                        <div class="col"><strong>ETH</strong></div>
                                        <div class="col-auto">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="3" name="type" ng-model="preferencesObj.contact_by_phone" value="eth" class="custom-control-input">
                                                <label class="custom-control-label" for="3"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                        /*
                        <form ng-submit="createDepositRequest()" id="depositForm">
                            <div class="row no-gutters align-items-center mb-3">
                                <div class="col-auto"><div class="text-dark h6 m-0">Step 1 of 2</div></div>
                                <div class="col pl-2"><div class="border-top border-dark"></div></div>
                            </div>
                            <div class="row justify-content-center mb-4">
                                <div class="col-auto"><a href="javascript:void(0);" class="btn btn-outline-primary" ng-click="deposit.credits_count = 100">Rs 100</a></div>
                                <div class="col-auto"><a href="javascript:void(0);" class="btn btn-outline-primary" ng-click="deposit.credits_count = 250">Rs 250</a></div>
                                <div class="col-auto"><a href="javascript:void(0);" class="btn btn-outline-primary" ng-click="deposit.credits_count = 500">Rs 500</a></div>
                            </div>
                            <div class="row my-4 p-0 justify-content-center">
                                <div class="col-lg-3 col-xs-12">
                                    <div class="form-group">
                                        <label>Enter Amount</label>
                                        <input type="number"  name="amount" class="number form-control" ng-model="deposit.credits_count" ng-blur="validateBonusCode()">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-12">
                                    <div class="form-group">
                                        <label>Bonus Code</label>
                                        <input ng-readonly="deposit.credits_count == false" type="text" name="bonus_code"  ng-model="deposit.bonus_code" class="form-control" ng-blur="validateBonusCode()">
                                    </div>
                                </div>
                                <div class="col-auto align-self-end">
                                    <div class="form-group">
                                        <span class="btn bg-transparent px-0 text-muted">(or)</span>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-12 align-self-end">
                                    <div class="form-group">
                                        <button ng-disabled="deposit.credits_count == false" type="button" class="btn btn-block btn-theme" data-toggle="modal" data-target="#bonuses">
                                            <i class="fa fa-plus-hexagon mr-2"></i>Apply Bonus
                                        </button>
                                    </div>
                                </div>
                            </div>


                            <div class="row no-gutters align-items-center mb-3">
                                <div class="col-auto"><div class="text-dark h6 m-0">Step 2 of 2</div></div>
                                <div class="col pl-2"><div class="border-top border-dark"></div></div>
                            </div>


                            <?php
                            $payment_options = [
                            ];
                            ?>


                            <div class="row my-4 justify-content-center">
                                <?php foreach ($payment_options as $item) { ?>
                                    <div class="col-3">
                                        <label>
                                            <input type="radio" name="payment_gateway" ng-model="deposit.payment_gateway" value="<?= $item['key'] ?>">
                                            <img src="<?= STATIC_IMAGES_PATH . "payment_options/" . $item["image"] ?>" class="img-thumbnail" style="width:130px; height: 50px">
                                        </label>
                                    </div>
                                <?php } ?>
                            </div>



                            <div class="text-center" ng-if="couponValidating">
                                <img src="<?= base_url() ?>assets/images/loading.gif"/>
                            </div>

                            <div class="row my-4 justify-content-center">
                                <p class="{{bonusApplyNotifiction.css}}" ng-if="bonusApplyNotifiction.css">{{bonusApplyNotifiction.message}}</p>
                            </div>

                            <div class="text-center">
                                <button class="btn btn-gold card-header-btn text-white">Add Cash {{deposit.credits_count ? "Rs "+deposit.credits_count :""}}</button>
                            </div>

                            <!--
                            <div class="row no-gutters align-items-center my-3">
                                <div class="col-auto"><div class="text-dark h6 m-0">Step 2 of 2</div></div>
                                <div class="col pl-2"><div class="border-top border-dark"></div></div>
                            </div>
                            <div class="bordered-frame">
                                <div class="bf-label"><span>Payment Method</span></div>
                                <div class="bf-body">

                                    <div class="row align-items-center">
                                        <div class="col-auto my-2"><svg height="40px" viewBox="0 0 287.989 90.344"><use xlink:href="#paytm" /></svg></div>
                                        <div class="col-auto my-2"><svg height="80px" viewBox="0 0 504 504"><use xlink:href="#mastercard" /></svg></div>
                                        <div class="col-auto my-2"><svg height="80px" viewBox="0 0 504 504"><use xlink:href="#maestro" /></svg></div>
                                        <div class="col-auto my-2"><svg height="80px" viewBox="0 0 291.764 291.764"><use xlink:href="#visa" /></svg></div>
                                    </div>
                                </div>
                            </div>
                            -->
                        </form>
                        */
                        ?>
                        </div>
                        <div class="card-footer">
                            <input type="submit" class="btn btn-primary" value="Submit">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>depositCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>depositService.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>bonusAvailableService.js?r=<?= time() ?>"></script>
