<?php $this->load->view('includes/header') ?>
<?php $this->load->view('includes/menu-dashboard') ?>
<section class="innerpages py-5" ng-controller="bonusAvailableCtrl">
    <div class="container">
        <?php include'chips.php'; ?>
        <div class="row mt-4">
            <?php $this->load->view('includes/dashboard-sidebar') ?>
            <div class="col-9">
                <div class="card h-100 card-gold">
                    <div class="card-header">BONUS AVAILABLE</div>
                    <div class="card-body bonus-table bonus-table-white" >
                        <div class="bonus-table-row" ng-repeat="coupon in coupons"> 
                            <div class="row align-items-center">
                                <div class="col">
                                    <h6>{{coupon.bonus_type}}</h6>
                                    <p class="m-0">({{coupon.description}})</p>
                                </div>
                                <div class="col text-center"><h6>#{{coupon.coupon_code}}</h6></div>
                                <div class="col-auto"><button class="btn btn-theme" data-dismiss="modal">Apply Code</button></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>bonusAvailableCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>bonusAvailableService.js?r=<?= time() ?>"></script>