<?php $this->load->view('includes/menu-dashboard') ?>
<section class="innerpages py-5" ng-controller="withdrawCtrl">
    <div class="container">
        <div class="withdraw-outer">
            <div class="row mt-4">
                <?php $this->load->view('includes/dashboard-sidebar') ?>
                <div class="col-lg-9 col-xs-12 p-0">
                    <div class="card card-gold my-2 p-0 px-2">
                        <div class="card-header py-2 card-header2 text-white">
                            <div class="row align-items-center">
                                <div class="col">WITHDRAW CASH</div>
                                <div class="col-auto">
                                    <a class="btn btn-light" href="<?= base_url() ?>withdraw_transactions">Withdraw history</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="card mb-3">
                                <div class="card-body">
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <h5>Available Withdrawable Balance</h5>
                                            <p class="text-danger m-0">*Minimum amount to withdraw: Rs <?= MINIMUM_AMOUNT_FOR_WITHDRAW_MONEY ?></p>
                                        </div>
                                        <div class="col-auto">
                                            <button class="btn btn-warning font-weight-bold">Rs {{chipsObj.wallet_account.real_chips_withdrawal}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bordered-frame">
                                <div class="bf-body">
                                    <?php $this->load->view("common_files/withdraw_form") ?>
                                </div>
                            </div>




                            <!--
                                                    <div class="bordered-frame">
                                                        <div class="bf-label"><span>E-wallet</span></div>
                                                        <div class="bf-body"><svg height="40px" viewBox="0 0 287.989 90.344"><use xlink:href="#paytm" /></svg></div>
                                                    </div>-->
                            <p>To get information on processing fees and withdrawing cash from your account, <a href="<?= base_url() ?>withdraw_charges" target="_blank">click here</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
<style>
 .textblack{ color: #000; }   



</style>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>withdrawCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>withdrawService.js?r=<?= time() ?>"></script>