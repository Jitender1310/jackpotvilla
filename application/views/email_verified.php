<?php $this->load->view('includes/menu-dashboard') ?>
<section class="innerpages py-5">
    <div class="container">
        
        <?php if(isset($player_details)){
            include'chips.php'; 
        } ?>
        <div class="row mt-4">
            <div class="col-12">
                <div class="card card-gold">
                    <div class="card-header">BRING-A-FRIEND</div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-12 text-center">

                                <div class="row align-items-center justify-content-center my-5">
                                    <div class="col-auto">
                                        <img src="<?= STATIC_IMAGES_PATH ?>verified-symbol.png" title="Email Verififed" class="img-fluid" style="width:100px;">
                                    </div>
                                </div>
                                <h2>Email Verified</h2>
<!--                                <p class="lead">Earn Upto 1000 in bonus for each friend that joins</p>-->

                                <div class="card mt-5 bg-light">
                                    <div class="card-body">
                                        <div class="row align-items-center">
                                            <div class="col ">
                                                <a  href="<?= base_url() ?>account" class="btn btn-block btn-success">Go to My Account</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>