<?php include'menu.php'; ?>
<section class="innerpages py-5">
    <div class="container">

        <div class="card border-0">
            <div class="card-header bg-yello font-weight-bold h3">Premium Tournament</div>
            <div class="card-body">

                <ul id="collapze">
                    <li><a href="" data-toggle="collapse" data-target="#item01" aria-expanded="true">What is Premium Tournament?</a>
                        <div id="item01" class="collapse show"  data-parent="#collapze">
                            <div class="py-4"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                <img src="<?= STATIC_IMAGES_PATH ?>screen2.png" class="img-fluid"></div>
                        </div>
                    </li>
                    <li><a href="" data-toggle="collapse" data-target="#item03">How do I Register for Premium Tournaments?</a>
                        <div id="item03" class="collapse"  data-parent="#collapze">
                            <div class="py-4"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></div>
                        </div>
                    </li><li><a href="" data-toggle="collapse" data-target="#item04">How do I Register for Premium Tournaments?</a>
                        <div id="item04" class="collapse"  data-parent="#collapze">
                            <div class="py-4"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></div>
                        </div>
                    </li><li><a href="" data-toggle="collapse" data-target="#item05">How do I Register for Premium Tournaments?</a>
                        <div id="item05" class="collapse"  data-parent="#collapze">
                            <div class="py-4"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></div>
                        </div>
                    </li>


                </ul>

            </div>
        </div>
    </div>
</section>