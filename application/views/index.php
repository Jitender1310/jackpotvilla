<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>casinoGamesCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>casinoGamesService.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_JS_PATH ?>swiper.min.js"></script>

<style>
 .flx_boxes::-webkit-scrollbar {
  width: 10px !important;
}

/* Track */
.flx_boxes::-webkit-scrollbar-track {
  box-shadow: inset 0 0 5px grey;
  border-radius: 5px;
}

/* Handle */
.flx_boxes::-webkit-scrollbar-thumb {
  background: red;
  border-radius: 5px;
}

/* Handle on hover */
.flx_boxes::-webkit-scrollbar-thumb:hover {
  background: #b30000;
}
</style>



<section class="innerpages">
<div class="container p-0">
<?php
if (!$is_logged_in) {
    include 'menu.php';
?>
   
<?php
} else {
    $this->load->view('includes/menu-dashboard');
}
?>
    <div class="container" style="background: #fff; padding: 0;">
        <section class="game_slider" style="background-color: #fff; padding-bottom: 2em;">
            <div ng-controller="casinoGamesCtrl">
            <div class="row m-0">

<div class="col-xs-6 col-lg-2 card  p-2 m-0" style="border:0" ng-repeat="game in jackpot | filter:searchText"><!-- Games of 30 per page -->
    <div class="card-img card_img">
    <span class="badge badge-secondary d-block">{{game.type}}</span>
    <img class="image" bn-lazy-src="{{game.logo}}" alt="">
    <div class="middle">
        <?php
        if (!$is_logged_in) {
        ?>
            <a href="#">
                <div class="text"> Login</div>
            </a>
        <?php
        } else {
        ?>
            <a href="javascript::void(0);" ng-click="loadGame(game.id, '<?= $access_token ?>')">
                <div class="text"> Play</div>
            </a>
        <?php
        }
        ?>
    </div>
    </div>
    <!-- <div class="card-body w-100 card-header2 text-white text-center">
    <strong>{{game.display_name}}</strong>
    </div> -->
</div>

</div>
<!-- <dir-pagination-controls class="pagging_ctrl"></dir-pagination-controls> -->
</div>
</div>
                <!-- <div class="container-fluid pt-2" ng-repeat="(type,list) in games"> -->
                    <!-- <div class="row m-0 py-2">
                        <div class="col-6">
                            <h3 style="color: #333">{{type}}</h3>
                        </div>
                        <div class="col-6 m-0 p-0 text-right">
                        <a href="{{type}}"> <h5><button class="btn">View More</button></h5> </a>
                        </div>
                    </div> -->
                    
                    <!-- Slider main container -->
                    <!-- <div class="swiper-container flx_boxes"> -->
                        <!-- Additional required wrapper -->
                        <!-- <div class="swiper-wrapper" swiper>
                            <div class="swiper-slide swiper_{{index}}" ng-repeat="(index, game) in list" ng-if="index!='null'" is-loaded>
                                <span class="badge badge-secondary d-block" ng-if="index!='null'">{{game.game_provider_name}}</span>
                                <img src="{{game.logo}}" class="image" alt="">
                                <div class="middle" ng-if="index!='null'">
                                    <?php
                                    if (!$is_logged_in) {
                                    ?>
                                        <a href="#">
                                            <div class="text"> Login</div>
                                        </a>
                                    <?php
                                    } else {
                                    ?>
                                        <a href="javascript::void(0);" ng-click="loadGame(game.id, '<?= $access_token ?>')">
                                            <div class="text"> Play</div>
                                        </a>
                                    <?php
                                    }
                                    ?>
                                </div>                               
                            </div>
                        </div> -->

                        <!-- If we need navigation buttons -->
                        <!-- <div class="swiper-button-prev" ng-click="goPrev()"></div>
                        <div class="swiper-button-next" ng-click="goNext()"></div> -->
                    </div>
                </div>
            </div>

            <div class="container-fluid" style="display: none">

                <h3 class="pt-4" style="color: #333">Popular Games</h3>
                <!-- Slider main container -->
                <div class="swiper-container flx_boxes">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <div class="swiper-slide slide_1">
                            <img src="/assets/images/casino_games/boongo/goldbg.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_2">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/Baby-Bloomers.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_3">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/Bombs-Away.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_4">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/Booming_Bars.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_5">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/Booming-Bananas.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>

                        <div class="swiper-slide slide_1">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/chicago-gangster.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_2">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/midas treasure.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_3">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/devils_heat.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_4">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/03565.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_5">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/lava_loca.jpg" class="image" alt="">
                            <div class="middle">
                                <?php
                                if (!$is_logged_in) {
                                ?>
                                    <a href="#">
                                        <div class="text"> Login</div>
                                    </a>
                                <?php
                                } else {
                                ?>
                                    <a href="https://pi-test.njoybingo.com/game.do?token=cd7fe0d7-718e-4c70-b79e-d53ab30f1b7a-1582541092981&pn=a2zbetting&lang=en&game=BOOONGO-12&type=CHARGED" target="_blank">
                                        <div class="text"> Play</div>
                                    </a>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div class="swiper-slide slide_2">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/Baby-Bloomers.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_3">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/Bombs-Away.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_4">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/Booming_Bars.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_5">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/lava_loca.jpg" class="image" alt="">
                            <div class="middle">
                                <?php
                                if (!$is_logged_in) {
                                ?>
                                    <a href="#">
                                        <div class="text"> Login</div>
                                    </a>
                                <?php
                                } else {
                                ?>
                                    <a href="https://pi-test.njoybingo.com/game.do?token=cd7fe0d7-718e-4c70-b79e-d53ab30f1b7a-1582541092981&pn=a2zbetting&lang=en&game=BOOONGO-12&type=CHARGED" target="_blank">
                                        <div class="text"> Play</div>
                                    </a>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                    <!-- If we need navigation buttons -->
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            </div>

            <div class="container-fluid" style="display: none">

                <h3 style="color: #333">New Games</h3>
                <!-- Slider main container -->
                <div class="swiper-container flx_boxes">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">

                        <div class="swiper-slide slide_2">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/midas treasure.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_3">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/Bombs-Away.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_4">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/Booming_Bars.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_5">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/Booming-Bananas.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_3">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/devils_heat.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_1">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/chicago-gangster.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>

                        <div class="swiper-slide slide_2">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/Baby-Bloomers.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>

                        <div class="swiper-slide slide_4">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/03565.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_1">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/asia-wins.png" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_5">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/lava_loca.jpg" class="image" alt="">
                            <div class="middle">
                                <?php
                                if (!$is_logged_in) {
                                ?>
                                    <a href="#">
                                        <div class="text"> Login</div>
                                    </a>
                                <?php
                                } else {
                                ?>
                                    <a href="https://pi-test.njoybingo.com/game.do?token=cd7fe0d7-718e-4c70-b79e-d53ab30f1b7a-1582541092981&pn=a2zbetting&lang=en&game=BOOONGO-12&type=CHARGED" target="_blank">
                                        <div class="text"> Play</div>
                                    </a>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div class="swiper-slide slide_4">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/Booming_Bars.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_5">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/Booming-Bananas.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_3">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/devils_heat.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_2">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/Baby-Bloomers.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <!-- <div class="swiper-slide slide_6">
                <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/jumbostampede.jpg" class="image" alt="">
                <div class="middle">
                <a href="#">
                    <div class="text"> Login</div>
                 </a>
                </div>
            </div> -->
                    </div>

                    <!-- If we need navigation buttons -->
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            </div>


            <div class="container-fluid" style="display: none">

                <h3 class="" style="color: #333">Jackpot Games</h3>
                <!-- Slider main container -->
                <div class="swiper-container jackpot_slot">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <div class="swiper-slide slide_1">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/asia-wins.png" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_2">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/Baby-Bloomers.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_3">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/Bombs-Away.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_4">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/Booming_Bars.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_5">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/Booming-Bananas.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>

                        <div class="swiper-slide slide_1">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/chicago-gangster.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_2">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/midas treasure.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_3">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/devils_heat.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_4">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/03565.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_5">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/lava_loca.jpg" class="image" alt="">
                            <div class="middle">
                                <?php
                                if (!$is_logged_in) {
                                ?>
                                    <a href="#">
                                        <div class="text"> Login</div>
                                    </a>
                                <?php
                                } else {
                                ?>
                                    <a href="https://pi-test.njoybingo.com/game.do?token=cd7fe0d7-718e-4c70-b79e-d53ab30f1b7a-1582541092981&pn=a2zbetting&lang=en&game=BOOONGO-12&type=CHARGED" target="_blank">
                                        <div class="text"> Play</div>
                                    </a>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                        <!-- <div class="swiper-slide slide_6">
                <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/jumbostampede.jpg" class="image" alt="">
                <div class="middle">
                <a href="#">
                    <div class="text"> Login</div>
                 </a>
                </div>
            </div> -->
                    </div>

                    <!-- If we need navigation buttons -->
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            </div>

            <div class="container-fluid">

                <h3 class="" style="color: #333">Slots</h3>
                <!-- Slider main container -->
                <div class="swiper-container jackpot_slot">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <div class="swiper-slide slide_1">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/mexico-wins.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_2">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/Romeo.png" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_3">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/RuffledUp.png" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_4">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/CoyoteCrash.png" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_5">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/Skys the Limit-ico.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>

                        <div class="swiper-slide slide_1">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/WinnersCup.png" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <!-- <div class="swiper-slide slide_6">
                <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/jumbostampede.jpg" class="image" alt="">
                <div class="middle">
                <a href="#">
                    <div class="text"> Login</div>
                 </a>
                </div>
            </div> -->
                    </div>

                    <!-- If we need navigation buttons -->
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            </div>

            <div class="container-fluid">

                <h3 class="" style="color: #333">Table Games</h3>
                <!-- Slider main container -->
                <div class="swiper-container jackpot_slot">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <div class="swiper-slide slide_1">
                            <img src="<?= STATIC_IMAGES_PATH ?>/atlantic_treasures_img.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>

                        <div class="swiper-slide slide_3">
                            <img src="<?= STATIC_IMAGES_PATH ?>/egyptian_rebirth_img.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_2">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/Romeo.png" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_1">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/WinnersCup.png" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_5">
                            <img src="<?= STATIC_IMAGES_PATH ?>/zodiac_img.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>


                        <div class="swiper-slide slide_4">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/CoyoteCrash.png" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <!-- <div class="swiper-slide slide_6">
                <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/jumbostampede.jpg" class="image" alt="">
                <div class="middle">
                <a href="#">
                    <div class="text"> Login</div>
                 </a>
                </div>
            </div> -->
                    </div>

                    <!-- If we need navigation buttons -->
                    <!-- <div class="swiper-pagination"></div> -->
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            </div>

            <div class="container-fluid">

                <h3 class="" style="color: #333">Classic Slots</h3>
                <!-- Slider main container -->
                <div class="swiper-container jackpot_slot">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <div class="swiper-slide ">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/WinnersCup.png" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_2">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/Romeo.png" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>

                        <div class="swiper-slide slide_4">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/CoyoteCrash.png" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide slide_5">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/Skys the Limit-ico.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>

                        <div class="swiper-slide slide_3">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/RuffledUp.png" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>

                        <div class="swiper-slide slide_1">
                            <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/mexico-wins.jpg" class="image" alt="">
                            <div class="middle">
                                <a href="#">
                                    <div class="text"> Login</div>
                                </a>
                            </div>
                        </div>
                        <!-- <div class="swiper-slide slide_6">
                <img src="<?= STATIC_IMAGES_PATH ?>/casino_web/jumbostampede.jpg" class="image" alt="">
                <div class="middle">
                <a href="#">
                    <div class="text"> Login</div>
                 </a>
                </div>
            </div> -->
                    </div>

                    <!-- If we need navigation buttons -->
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            </div>
            <!-- <div class="container-fluid">

    <h3 class="pt-4" style="color: #333" >Casino Games</h3>
    <!-- Slider main container -->
            <!-- <div class="swiper-container flx_boxes">
        <!-- Additional required wrapper -->
            <!--<div class="swiper-wrapper" ng-controller="casinoGamesCtrl">
            <div class="swiper-slide slide_{{index}}" ng-repeat="(index, game) in games" >
                <img src="{{game.logo}}" class="image" alt="" ng-mouseover="open = true" ng-mouseleave="open = false">
                <div class="middle">
                    <?php
                    if (!$is_logged_in) {
                    ?>
                        <a href="#">
                            <div class="text"> Login</div>
                         </a>
                        <?php
                    } else {
                        ?>
                        <a href="javascript::void(0);" ng-click="loadGame(game.id, '<?= $access_token ?>')">
                            <div class="text"> Play</div>
                         </a>
                        <?php
                    }
                        ?>
                </div>
            </div>
        </div>

        <!-- If we need navigation buttons -->
            <!-- <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
</div> -->

        </section>


        <section class="py-5 bg-gradeint2" style="display:none;">
            <div class="container">
                <div class="row">
                    <div class=" col-md-4">
                        <div class="dr">
                            <a href="#" class="btn-hover"><span class="img-box"><img src="<?= STATIC_IMAGES_PATH ?>/cards2.png" class="img-fluid "></span><span style=" display: block;">TIPS & TRICKS</span></a>
                        </div>
                    </div>
                    <div class=" col-md-4">
                        <div class="dr">
                            <a href="#" class="btn-hover"><span class="img-box"><img src="<?= STATIC_IMAGES_PATH ?>/refer-and-eran.png" class="img-fluid "></span><span style=" display: block;">REFER & EARN</span></a>
                        </div>
                    </div>
                    <div class=" col-md-4">
                        <div class="dr">
                            <a href="#" class="btn-hover"><span class="img-box"><img src="<?= STATIC_IMAGES_PATH ?>/prize.png" class="img-fluid "></span><span style=" display: block;">NEW USER BONUS</span></a>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- <section class="bg-red py-5">
    <div class="container">
        <div class="row align-items-center mb-5">
            <div class="col text-right">
                <img src="<?= STATIC_IMAGES_PATH ?>testimonial-shape - Copy.png" class="img-fluid ">

            </div>
            <div class="col">
                <h1  style="color: #000 !important;" class="text-warning text-center m-0">Testimonial</h1>

            </div>
            <div class="col text-left">
                <img src="<?= STATIC_IMAGES_PATH ?>testimonial-shape - Copy.png" class="img-fluid ">
            </div>
        </div>
        <div class="swiper-container testimonial-slider">

            <div class="swiper-wrapper">
                <?php
                $testimonials = $this->testimonials_model->get_testimonials();
                foreach ($testimonials as $item) {
                ?>
                    <div class="swiper-slide">
                        <div class="row">
                            <div class=" col-md-6">
                                <div class="text-center text-white  mx-auto" style="display: flex; border: 1px solid #000; border-radius: 20px;
                                     padding: 18px 0 0 0;">
                                    <div class=" col-md-3">
                                        <img src="<?= $item->image ?>" height="100px" width="100px" style=" border: 1px solid #000; border-radius: 5px;" >
                                            <p class="phara" style=" color: #000"><?= $item->message ?></p>
                                    </div>
                                    <div class=" col-md-9">
                                        <h4 class="m-0" style="text-align: left; padding: 0 !important; font-size: 16px; color: #000"><?= $item->name_and_address ?></h4>
                                        <h4 style="text-align: left; font-size: 16px; margin-top: 20px; font-weight: bold; color: #000"><?= $item->prize_title ?></h4>
                                    </div>
                                </div>
                            </div>
                            <div class=" col-md-6">
                                <div class="text-center text-white mx-auto" style="display: flex; border: 1px solid #000;border-radius: 20px;
                                     padding: 18px 0 0 0;">
                                    <div class=" col-md-3">
                                        <img src="<?= $item->image ?>" height="100px" width="100px" style=" border: 1px solid #000; border-radius: 5px;">
                                            <p class="phara" style=" color: #000"><?= $item->message ?></p>
                                    </div>
                                    <div class=" col-md-9">

                                        <h4 class="m-0" style="text-align: left; padding: 0 !important; font-size: 16px; color: #000"><?= $item->name_and_address ?></h4>
                                        <h4 style="text-align: left; font-size: 16px; margin-top: 20px; font-weight: bold; color: #000"><?= $item->prize_title ?></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>


            </div>
            <div class="swiper-pagination"></div>
            <div class="testimonial-slider-left"></div>
            <div class="testimonial-slider-right"></div>
        </div>
    </div>
</section> -->

        <section class="download py-5" style="display: none">
            <div class="container">

                <div class="row justify-content-center">
                    <div class="col-6">

                        <div class="img-box1">

                            <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">

                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <img src="<?= STATIC_IMAGES_PATH ?>game-lobby1_new.png" class="img-fluid">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="<?= STATIC_IMAGES_PATH ?>game-lobby2_new.png" class="img-fluid">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="<?= STATIC_IMAGES_PATH ?>lobby_2.png" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-6">
                        <div class="row download-outer">
                            <div class=" col-12">
                                <h1 class=" text-white " style="font-size: 33px;  margin-bottom: 15px; ">A2Z Betting for Mobile</h1>
                                <p style=" color: #fff;">Play Rummy on the Go and experience fun and excitement with the A2Z Betting App! Or you can simply visit www.A2Z Betting.com on your mobile.</p>
                                <p style=" color: #fff;">Download the A2Z Betting mobile app on your Android Smartphones/Tablets and enjoy the game anywhere you like.</p>

                            </div>
                            <div class="col-4">

                                <a href="<?php echo base_url("clover_rummy.apk") ?>"><img src="<?= STATIC_IMAGES_PATH ?>download-apk.png" class="img-fluid"></a>
                            </div>
                            <div class="col-4">

                                <a href="<?= DOWNLOAD_IOS_APP_LINK ?>"><img src="<?= STATIC_IMAGES_PATH ?>app-store.png" class="img-fluid"></a>
                            </div>
                            <div class="col-4">
                                <a href="<?= DOWNLOAD_ANDROID_APP_LINK ?>"><img src="<?= STATIC_IMAGES_PATH ?>play-store.png" class="img-fluid"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="py-5 text-black" style=" display: none; text-align: center;margin-top: 50px;">
            <div class="container">
                <div class="row ">
                    <div class="col-4">
                        <div class="rummykhel-box">
                            <span class=" img-bg2"><img src="<?= STATIC_IMAGES_PATH ?>logo.svg" style=" width: 40%;" class="img-fluid mb-3"></span>
                            <h4 style="color : #000 !important; margin-top: 50px;"> A2Z Betting Online</h4>
                            <p style="padding: 0 31px;">A2Z Betting is the first game launched by Rummy games. Rediscover new standards of gaming. A2Z Betting is meticulously designed and crafted to suit your taste. If you love rummy and can’t get your friends to play it any time of the day, you’ve come to the right place. A2Z Betting provides a 24x7 service to our users. A2Z Betting solely focuses on providing the best gaming experience there is, to minimize the line between online and offline rummy. Play with your computerized partner and earn real money. Register now!
                            </p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="rummykhel-box">
                            <span class=" img-bg2"><img src="<?= STATIC_IMAGES_PATH ?>logo.svg" style="width: 40%;" class="img-fluid mb-3"></span>
                            <h4 style="color : #000 !important;margin-top: 50px;">Tournaments</h4>
                            <p style="padding: 0 31px;">If you’re new to rummy, it is important to know what tournaments are. You deal with real cash in the pool tables. A2Z Betting offers to you two types of Rummy tournaments. Cash and Free tournaments. If you aren’t sure of your rummy skills yet, you can always avail our free tournaments. It’s a source of practice for beginners and a warm up for regulars. Later, you can move on to cash tournaments and earn fame and fortune. Rummy tournaments take you up on a rollercoaster ride until the very end of the game.
                            </p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="rummykhel-box">
                            <span class=" img-bg2"><img src="<?= STATIC_IMAGES_PATH ?>logo.svg" style="width: 40%;" class="img-fluid mb-3"></span>
                            <h4 style="color : #000 !important;margin-top: 50px;">Safe Play</h4>
                            <p style="padding: 0 31px;">A2Z Betting ensured a safe play to its users. The information that you entrust with us is closely protected and is not used for any purpose not mentioned in the privacy policy. Do not be afraid of fraudulent practices and play the game. The safety of your personal data is guaranteed. The information you provide will only help us maximize your satisfaction.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>registerCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>registerService.js?r=<?= time() ?>"></script>
<script src="https://unpkg.com/swiper/js/swiper.min.js"></script>
<script>
    function alphaOnly(event) {
        var key = event.keyCode;
        return ((key >= 65 && key <= 90) || key == 8);
    };

    $('#date').datepicker({
        autoclose: true,
        todayHighlight: true
    });




    $('.form_datetime').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1
    });

    $('#example1').calendar({
        type: 'date'
    });
    $('#example2').calendar({
        type: 'time'
    });
    $('#example3').calendar();

    $('#rangestart').calendar({
        type: 'date',
        endCalendar: $('#rangeend')
    });
    $('#rangeend').calendar({
        type: 'date',
        startCalendar: $('#rangestart')
    });

    $(function() {
        $('#datetimepicker1').datetimepicker();
    });
</script>
<script>
    $(function() {
        $("#date_of_birth").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
        });
    });
</script>
