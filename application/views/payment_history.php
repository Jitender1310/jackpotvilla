<?php $this->load->view('includes/menu-dashboard') ?>
<section class="innerpages py-5" ng-controller="paymentHistoryCtrl">
    <div class="container">
        <div class="payment-outer">
        <div class="row mt-4">
            <?php $this->load->view('includes/dashboard-sidebar') ?>
            <div class="col-lg-9 col-xs-12 my-2 p-0 pl-2">
                <div class="card card-gold">
                    <div class="card-header py-2 card-header2 text-white">
                        <div class="row align-items-center">
                            <div class="col-lg-9 col-xs-12">Payment History</div>
                            <div class="col-lg-3 col-xs-12 d-none">
                                <select class="custom-select border-0">
                                    <option>All</option>
                                    <option>Deposit</option>
                                    <option>Withdraw</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive table_container">
                    <table class="table table-striped table-hover table-custom mb-0">
                        <thead class="bg-secondary text-white">
                            <tr>
                                <th>S.No</th>
                                <th>Txn ID</th>
                                <th>Date & Time</th>
                                <th class="text-right">Transaction Amount</th>
                                <th class="text-right">Status</th>
                            </tr>
                        </thead>
                        <tr ng-if="paymentHistoryList.length==0">
                            <td colspan="4" class="text-center">No data found</td>
                        </tr>
                        <tr ng-repeat="item in paymentHistoryList track by $index">
                            <td>{{paymentHistoryPagination.initial_id+$index}}</td>
                            <td>{{item.transaction_id}}</td>
                            <td>{{item.created_at}}</td>
                            <td class="text-right">{{item.credits_count}}</td>
                            <td class="text-right {{item.transaction_bootstrap_class}}">{{item.transaction_status}}</td>
                        </tr>
                    </table>
                    </div>
                    <div class="custom-pagination" ng-bind-html="paymentHistoryPagination.pagination"></div>
                </div>
            </div>
            
            
        </div>
        </div>
    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>paymentHistoryCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>paymentHistoryService.js?r=<?= time() ?>"></script>