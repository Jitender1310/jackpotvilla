<?php $this->load->view('includes/header') ?>
<?php $this->load->view('includes/menu-dashboard') ?>
<section class="innerpages py-5" ng-controller="levelInfoCtrl">
    <div class="container">
        <div class="levelinfo-outer">
        <div class="row mt-4">
            <?php $this->load->view('includes/dashboard-sidebar') ?>
            <div class="col-lg-9 col-xs-12 my-2 p-0 px-2">
                <div class="card h-100 card-gold">
                    <div class="card-header card-header2 text-white">CURRENT LEVEL INFO</div>
                    <div class="card-body">

                        <div class="row align-items-center">
                            <div class="col-auto">
                                <img src="<?= STATIC_IMAGES_PATH ?>logo_face.svg" height="150" class="mb-2" style="height: 50px;">
                                <strong class="d-block text-center mt-2">{{expertizeLevelObj.level_info.current_level_name}}</strong>
                            </div>
                            <div class="col">

                                <div>
                                    <b>{{expertizeLevelObj.level_info.current_points}}</b>
                                    <b class="float-right">{{expertizeLevelObj.level_info.max_points}}</b>
                                </div>


                                <div class="progress progress-level">
  <div class="progress-bar" role="progressbar" style="width: {{ expertizeLevelObj.level_info.current_points * 100 / expertizeLevelObj.level_info.max_points}}%;"></div>
</div>

<strong class="d-block text-center mt-2">You need {{expertizeLevelObj.level_info.max_points-expertizeLevelObj.level_info.current_points}} for the next level</strong>

</div>
                             <div class="col-auto">         <img src="<?= STATIC_IMAGES_PATH ?>heart-level.png" height="150" class="mb-2" style="height: 50px;">
                                <strong class="d-block text-center mt-2">{{expertizeLevelObj.level_info.next_level_name}}</strong></div>
                        </div>

<hr>

<p>* Reward Points are calculated at the end of each game</p>
<p>* Reward Level of the player may get downgraded based on player Inactivity on the website</p>

                   
                      
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>levelInfoCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>levelInfoService.js?r=<?= time() ?>"></script>