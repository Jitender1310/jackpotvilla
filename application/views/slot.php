
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>casinoGamesCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>casinoGamesService.js?r=<?= time() ?>"></script>
<?php if (is_logged_in()) { ?>
    <?php $this->load->view('includes/menu-dashboard') ?>
<?php } ?>

<?php if (!is_logged_in()) { ?>
    <?php $this->load->view('menu') ?>
<?php } ?>
<section class="innerpages">
    <div class="container py-2" style="background: #fff;">
    <div class="card card-gold text-center">
    <div class="card-header card-header2 text-white">
        <h3>Slot Games</h3>
    </div>
    <div ng-controller="casinoGamesCtrl">
    <div class="row m-0 mt-4 p-0">
    <div class="col-lg-4 col-xs-0 m-0 p-0"></div>
    <div class="col-lg-4 col-xs-0 m-0 p-0"></div>
    <div class="col-lg-4 col-xs-12 m-0 p-0">
        <div class="input-group mb-3">
        <input type="text" class="form-control" placeholder="Search Game" ng-model="searchText" aria-label="Recipient's username" aria-describedby="basic-addon2">
        <div class="input-group-append">
            <span class="input-group-text" id="basic-addon2"><i class="fa fa-search" aria-hidden="true"></i></span>
        </div>
    </div>
    </div>

    </div>
                <div class="row p-2" ng-repeat="(type,list) in games" ng-if="type == 'Slot'">

                            <div class="col-xs-6 col-lg-2 card  p-2 m-0" style="border:0" ng-repeat="game in list | filter:searchText"><!-- Games of 30 per page -->
                                <div class="card-img card_img">
                                <span class="badge badge-secondary d-block">{{game.game_provider_name}}</span>
                                <img class="image" bn-lazy-src="{{game.logo}}" alt="">
                                <div class="middle">
                                    <?php
                                    if (!$is_logged_in) {
                                    ?>
                                        <a href="#">
                                            <div class="text"> Login</div>
                                        </a>
                                    <?php
                                    } else {
                                    ?>
                                        <a href="javascript::void(0);" ng-click="loadGame(game.id, '<?= $access_token ?>')">
                                            <div class="text"> Play</div>
                                        </a>
                                    <?php
                                    }
                                    ?>
                                </div>
                                </div>
                                <div class="card-body w-100 card-header2 text-white text-center">
                                <strong>{{game.display_name}}</strong>
                                </div>
                            </div>
                            
                            </div>
                            <!-- <dir-pagination-controls class="pagging_ctrl"></dir-pagination-controls> -->
                    </div>
                </div>
            </div>

    </div>

</div>

</section>

<script type="text/javascript">

const myLazyLoad = new myLazyLoad({
        elements_selector:".lazyload"
    })
</script>

<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>registerCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>registerService.js?r=<?= time() ?>"></script>   