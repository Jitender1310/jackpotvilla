<?php $this->load->view('includes/menu-dashboard') ?>
<section class="innerpages py-5" ng-controller="transactionsCtrl" ng-init="filterObj.page = <?php echo $current_page; ?>;
                getTxnHistory()">
    <div class="container">
        <div class="view-transaction-outer">
        <div class="row mt-4">
            <?php $this->load->view('includes/dashboard-sidebar') ?>
            <div class="col-lg-9 col-xs-12 my-2 p-0 pl-2">
                <div class="card card-gold">
                    <div class="card-header py-2 card-header2 text-white">
                        <div class="row align-items-center">
                            <div class="col-lg-9 col-xs-12">Transactions</div>
                            <div class="col-lg-3 col-xs-12">
                                <select class="custom-select border-0" ng-model="filter.txn_type" ng-change="getTxnHistory()">
                                    <option value="All">All</option>
                                    <option value="Credit">Credit</option>
                                    <option value="Debit">Debit</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive table_container">
                    <table class="table table-striped table-hover table-custom mb-0">
                        <thead class="bg-secondary text-white">
                            <tr>
                                <th>S.No</th>
                                <th>Type</th>
                                <th>Ref. ID</th>
                                <th>On</th>
                                <th>Amount</th>
                                <th class="text-right">Balance</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="transaction in transactions">
                                <td>{{transactionHistoryPagination.initial_id+$index}}</td>
                                <td><span class="{{transaction.transaction_bootstrap_class}}">{{transaction.transaction_type}}</span> / {{transaction.type}}</td>
                                <td>{{transaction.ref_id}}</td>
                                <td>{{transaction.created_date_time}}</td>
                                <td>{{transaction.amount}}</td>
                                <td class="text-right">{{transaction.closing_balance}}</td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    <div ng-if="transactions.length == 0">
                        <h4 class="text-center" style="padding:20px;">No data found</h4>
                    </div>
                    <div class="custom-pagination" ng-bind-html="transactionHistoryPagination.pagination"></div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>transactionsCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>transactionsService.js?r=<?= time() ?>"></script>