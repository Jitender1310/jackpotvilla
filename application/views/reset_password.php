<section class="content-section pt-3 pb-5" ng-controller="resetPasswordCtrl">
    <br/>
    <br/>
    <div class="container">
        <div class="justify-content-md-center row">
            <div class="col-lg-6 justify">
                <h3>Reset Password</h3>
                <form method="POST" role="form" id="reset_password_form" class="row" ng-keyup="rp_error = {}" ng-submit="doResetPasswordWithCode('<?php echo $key ?>')">
                        <!-- <input type="hidden" class="form-control"  id="forgot_password_verfication_code" name="forgot_password_verfication_code" ng-model="myObj.forgot_password_verfication_code"> -->
                    <div class="form-group col-md-12">
                        <label for="">New Password</label>
                        <input type="password" class="form-control" id="restPasswordId" required="" name="password" placeholder="Enter New Password" ng-model="myObj.password">
                        <span>{{rp_error.password}}</span>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="">Confirm Password</label>
                        <input type="password" class="form-control" id="cnf_password" required="" name="cnf_password" placeholder="Re-enter New Password" ng-model="myObj.cnf_password">
                        <span>{{rp_error.cnf_password}}</span>
                    </div>
                    <div class="col-md-12"><button type="submit" class="btn btn-primary btn-block radius text-uppercase">Update New Password</button></div>
                </form>
                <br/>
                <h5 class="text-center m-t-2"><a href="<?= base_url() ?>home" class="text-primary"><u>Cancel</u></a></h5>
            </div>
        </div>
    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>resetPasswordCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>forgotPasswordService.js?r=<?= time() ?>"></script>

<script>
                            $(function () {

                                function check_and_redirect() {
                                    if (window.screen.width <= 765) {
                                        location.href = "<?= MOBILE_WEBSITE_URL ?>/reset_password?key=<?= $this->input->get_post("key") ?>&username=<?= $this->input->get_post("username") ?>&access_token=<?= $this->input->get_post("access_token") ?>&no_redirect=true";
                                    } else {
                                        location.href = "<?= WEBSITE_URL ?>/reset_password?key=<?= $this->input->get_post("key") ?>&username=<?= $this->input->get_post("username") ?>&access_token=<?= $this->input->get_post("access_token") ?>&no_redirect=true";
                                    }
                                }

<?php if (!$this->input->get_post("no_redirect")) { ?>
                                    setTimeout(check_and_redirect, 1000);
<?php } ?>


                                $(window).resize(function () {
                                    check_and_redirect();
                                });
                            });
</script>