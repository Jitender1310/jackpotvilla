<?php $this->load->view('includes/menu-dashboard') ?>
<section class="innerpages py-5" ng-controller="withdrawCtrl">
    <div class="container" style="background: #fff;

padding-bottom: 10px;">
        <?php include'chips.php'; ?>
        <div class="row mt-4">
            <?php $this->load->view('includes/dashboard-sidebar') ?>
            <div class="col-9">
                <div class="card card-gold">
                    <div class="card-header py-2" style="border: 1px solid #cfcfcf;">
                        <div class="row align-items-center">
                            <div class="col">VIEW WITHDRAWAL REQUESTS</div>
                            <div class="col-auto">
                                <a class="btn btn-red-light" href="<?= base_url() ?>withdraw">WITHDRAW CASH</a>
                            </div>
                        </div>
                    </div>
                    <table class="table  mb-0">
                        <thead class="bg-primary text-white">
                            <tr>
                                <th>Si.No</th>
                                <th>Req.Date</th>
                                <th>Amount</th>
                                <th>Ref-id</th>
                                <th>Ac.No</th>
                                <th>IFSC Code</th>
                                <th>Bank</th>
                                <th>Branch</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tr ng-repeat="item in requestsList track by $index">
                            <td>{{requestsListPagination.initial_id + $index}}</td>
                            <td>{{item.request_date_time}}</td>
                            <td>{{item.request_amount}}</td>
                            <td>{{item.withdrawal_id}}</td>
                            <td>{{item.account_number}}</td>
                            <td>{{item.ifsc_code}}</td>
                            <td>{{item.bank_name}}</td>
                            <td>{{item.branch_name}}</td>
                            <td><span class="{{item.transaction_bootstrap_class}}">{{item.request_status}}</span></td>
                        </tr>

                    </table>
                    <div ng-if="requestsList.length == 0">
                        <h4 class="text-center" style="padding:20px;">No data found</h4>
                    </div>
                    <div class="custom-pagination" ng-bind-html="requestsListPagination.pagination"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>withdrawCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>withdrawService.js?r=<?= time() ?>"></script>