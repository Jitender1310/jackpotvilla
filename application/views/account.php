<?php $this->load->view('includes/menu-dashboard') ?>
<section class="innerpages py-5" ng-controller="profileCtrl">
    <div class="container">
        <div class="account-outer">
        <div class="row mt-4">
            <?php $this->load->view('includes/dashboard-sidebar') ?>
            <div class="col-lg-9 col-md-9 col-xs-12 mt-2 p-0 px-2">
                <div class="card card-gold">
                    <div class="card-header card-header2 text-white">ACCOUNT DETAILS</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-xs-12 pr-4 border-right">
                                <div class="card card-rounded border-dark mb-3">
                                    <div class="card-body" style="padding: 15px 20px !important;">
                                        <div class="row">
                                            <div class="col"><strong>User Name:</strong> <?= $player_details->username ?></div>
                                            <div class="col-auto"><a href="<?= base_url() ?>change_password" class="text-danger">Change Password</a></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row no-gutters align-items-center mb-3">
                                    <div class="col-auto"><div class="text-primary h6 m-0">Personal Information</div></div>
                                    <div class="col pl-2"><div class="border-top border-dark"></div></div>
                                </div>

                                <div class="list-group mb-3">
                                    <div class="list-group-item"><strong>Name</strong> <span class="float-right">{{profileObj.full_name}}</span></div>
                                    <div class="list-group-item"><strong>Date of Birth</strong> <span class="float-right">{{profileObj.display_date_of_birth}}</span></div>
                                    <div class="list-group-item"><strong>Address</strong> <span class="float-right">{{profileObj.address_line_1 + " " + profileObj.address_line_2}}</span></div>
                                    <div class="list-group-item"><strong>City</strong> <span class="float-right">{{profileObj.city}}</span></div>
                                    <div class="list-group-item"><strong>State</strong> <span class="float-right">{{profileObj.state_name}}</span></div>
                                    <div class="list-group-item"><strong>Pin Code</strong> <span class="float-right">{{profileObj.pin_code}}</span></div>
                                    <div class="list-group-item"><strong></strong> 
                                        <span class="float-right"><a href="javascript:void(0);" ng-click="showEditProfilePopup()"><span class="btn btn-sm fal fa-pen"></span>Edit Profile</a></span>
                                    </div>
                                </div>

                                <?php $this->load->view("includes/edit_profile_popup") ?>
                                <?php $this->load->view("includes/edit_email_popup") ?>
                                <?php $this->load->view("includes/edit_mobile_popup") ?>
                                <?php $this->load->view("includes/kyc_popup") ?>
                                <?php $this->load->view("includes/choose_avatar_popup") ?>


                                <div class="card card-rounded mb-3">
                                    <div class="card-body">
                                        <div class="row align-items-center">
                                            <div class="col-auto border-right"><svg viewBox="0 0 512 512" width="40px"><use xlink:href="#enevelope"/></svg></div>
                                            <div class="col">
                                                <div>
                                                    <strong>E-Mail ID</strong>
                                                    <!-- <i class="fal fa-exclamation-triangle ml-1 text-danger" 
                                                       title="Email Verification Pending, click to resend" style="cursor: pointer" 
                                                       ng-click="resendVerificationKey()" ng-if="profileObj.email_verified == 0"></i>
                                                    <i class="fal fa-check ml-1 text-success" ng-if="profileObj.email_verified == 1"></i> -->
                                                </div>
                                                <div>{{profileObj.email}}
                                                </div>
                                                
                                            </div>
                                            <!-- <div class="col-auto"><button class="btn btn-link btn-sm fal fa-pen" ng-click="showEditEmailPopup()"></button></div> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="card card-rounded mb-3">
                                    <div class="card-body">
                                        <div class="row align-items-center">
                                            <div class="col-auto border-right"><svg viewBox="0 0 58.981 58.981" width="40px"><use xlink:href="#smartphone"/></svg></div>
                                            <div class="col">
                                                <div>
                                                    <strong>Mobile</strong>
                                                    <!-- <i class="fal fa-exclamation-triangle ml-1 text-danger" 
                                                       title="Mobile Verification Pending, click to resend" style="cursor: pointer" 
                                                       ng-click="resendOTP()" ng-if="profileObj.mobile_verified == 0"></i>
                                                    <i class="fal fa-check ml-1 text-success" ng-if="profileObj.mobile_verified == 1"></i> -->
                                                </div>
                                                <div> {{profileObj.display_mobile}}</div>
                                            </div>
                                            <!-- <div class="col-auto"><button class="btn btn-link btn-sm fal fa-pen" ng-click="showEditMobilePopup()"></button></div> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="row no-gutters align-items-center mb-3">
                                    <div class="col-auto"><div class="text-primary h6 m-0">KYC verification</div></div>
                                    <div class="col pl-2"><div class="border-top border-dark"></div></div>
                                </div>

                                <div class="list-group mb-3">
                                    <div class="list-group-item" title="{{profileObj.address_proof_status}}">
                                        <strong>Address Proof</strong> 
                                        <span class="float-right {{profileObj.address_proof_status_bootstrap_css}}">{{profileObj.address_proof_type}}
                                            <i class="fal fa-exclamation-triangle ml-1 text-danger" ng-if="profileObj.address_proof_status != 'Approved'"></i>
                                            <i class="fal fa-check ml-1 text-success" ng-if="profileObj.address_proof_status == 'Approved'"></i>
                                        </span>
                                    </div>
                                    <div class="list-group-item" title="{{profileObj.pan_card_status}}">
                                        <strong>PAN Card</strong> 
                                        <span class="float-right {{profileObj.pan_card_status}}">{{profileObj.pan_card_number}}
                                            <i class="fal fa-exclamation-triangle ml-1 text-danger" ng-if="profileObj.address_proof_status != 'Approved'"></i>
                                            <i class="fal fa-check ml-1 text-success" ng-if="profileObj.address_proof_status == 'Approved'"></i>
                                        </span>
                                    </div>
                                    <div class="list-group-item" title="{{profileObj.pan_card_status}}">
                                        <button class="btn btn-success" type="button" ng-click="showKycPopup()">UPLOAD NOW</button>
                                        <span class="float-right {{profileObj.pan_card_status}}" style="    margin-top: 7px;">{{profileObj.pan_card_number}}
                                            <i class="fal fa-exclamation-triangle ml-1 text-danger" ng-if="profileObj.address_proof_status != 'Approved'"></i>
                                            <i class="fal fa-check ml-1 text-success" ng-if="profileObj.address_proof_status == 'Approved'"></i>
                                        </span>
                                    </div>
                                </div>
                                <?php /*<div class="card bg-warning" ng-if="profileObj.kyc_status == 'Not Verified'">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col">
                                                <button class="btn btn-success" type="button" ng-click="showKycPopup()">UPLOAD NOW</button>
                                            </div>
                                            <div class="col-auto"><svg viewBox="0 0 473.931 473.931" height="80px"><use xlink:href="#error"/></svg></div>
                                        </div>
                                    </div>
                                </div> */?>
                            </div>
                            <div class="col-lg-4 col-md-4 col-xs-12 text-center pl-4">
                                <h6> <b> MY AVATAR</b></h6>
                                <div class="my-2">
                                    <div class="row no-gutters d-flex" style="margin:auto; border: 1px solid #ccc; border-radius: 8px;">
                                        <div class="col-12">
                                            <img ng-src="{{profileObj.profile_pic}}" height="150px"/>
                                            <!--<svg height="100px" viewBox="0 0 256 256"><use xlink:href="#dp" /></svg>-->
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-default" ng-click="showChooseAvatarPopup()"> UPDATE MY AVATAR</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>