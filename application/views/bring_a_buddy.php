<?php $this->load->view("includes/header"); ?>
<?php include'menu.php'; ?>
<section class="innerpages py-5">
    <div class="container">
        <div class="row" style="background: #fff; padding: 20px 0;">
            <div class="col-3">
<?php include'includes/cms-sidebar.php'; ?>
         
            </div>
            <div class="col-9 text-white editor">
          
                <h5 class="text-black1" style="margin-bottom: 0;">REFER A EARN</h5>
<p>You can now receive bonus from inviting your friends to join A2Z Betting family. That simple! The refer a friend bonus works as follows:
</p>

<ul>
    <li>Invite a friend to install and register in the A2Z Betting app.</li>
    <li>Once they register and deposit a certain amount, your bonus account will be credited with the same amount.</li>
    <li>Every time they play for Rs.500, your deposit account will be credited with Rs.100.</li>
    <li>You will continue to receive refer a friend bonus until you have accumulated Rs.2000 in your deposit account.This Rs.2000 is divided into two parts. Firstly, when the friend deposits Rs.1000 or more for the first time, you will receive Rs.1000. Next, when he/she plays for Rs.500, you will be credited with Rs.100, until it accumulates to Rs.1000.</li>
    <li>You can continue receiving more bonus by inviting more friends.</li>
</ul>

<p style="margin-top: 15px;"><strong>For example</strong>, if you refer a friend who initially deposits Rs. 1000 in his account, you will be credited with the same amount in your bonus account. If your friend continues to deposit Rs. 1000 after the primary deposit, your deposit account will be credited with Rs. 100. You will receive the amount of Rs. 2000 in total through this bonus program. You can refer more friends to earn more bonuses.</p>
            


            <h6>Terms and Conditions:</h6>

            <ul>
                <li>The refer a friend bonus plan is effective only if the said friend is eligible to play the game.</li>
                <li>It is the person’s responsibility to read through the instructions and accept to our terms and conditions.</li>
                <li>Sending the refer a friend link to unknown numbers will be considered as spam and the bonus will not be availed by the user.</li>
                <li>The bonus is valid only if the friend uses the referral link sent by the player.</li>
                <li>A player shall not use multiple emails to avail more bonuses.</li>
            </ul>

            
            
            </div>
        </div>
    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>registerCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>registerService.js?r=<?= time() ?>"></script>   