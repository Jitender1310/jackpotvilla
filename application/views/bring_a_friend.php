<?php $this->load->view('includes/menu-dashboard') ?>

<link rel="stylesheet" href="<?= STATIC_ANGULAR_CTRLS_PATH ?>Tiny-Text/dist/tagify.css">
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>Tiny-Text/dist/tagify.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>Tiny-Text/dist/jQuery.tagify.min.js"></script>

<script>
    var isIE = !document.currentScript;

    function renderPRE(currentScript, codeScriptName) {
        if (isIE)
            return;
        setTimeout(function () {
            var jsCode = document.querySelector("[data-name='" + codeScriptName + "']").innerHTML;

            // cleanup closure wraper
            jsCode = jsCode.replace("(function(){", "").replace("})()", "").trim();
            // escape angled brackets between two _ESCAPE_START_ and _ESCAPE_END_ comments
            let textsToEscape = jsCode.match(new RegExp("// _ESCAPE_START_([^]*?)// _ESCAPE_END_", 'mg'));
            if (textsToEscape) {
                textsToEscape.forEach(function (textToEscape) {
                    jsCode = jsCode.replace(textToEscape, textToEscape.replace(/</g, "&lt")
                            .replace(/>/g, "&gt")
                            .replace("// _ESCAPE_START_", "")
                            .replace("// _ESCAPE_END_", "")
                            .trim());
                });
            }
            currentScript.insertAdjacentHTML('afterend', "<pre class='language-js'><code>" + jsCode + "</code></pre>");
        }, 500);
    }

    // detect modern browser and enhance console logs with colors

</script>

<section class="innerpages py-5" ng-controller="bringAFriendCtrl">
    <div class="container" style=" background: #fff;">
        <div class="row mt-4">
            <div class="col-12 py-4">
                <div class="card card-gold" style="border: 1px solid #cecbcb;
                     margin-bottom: 10px;">
                    <div class="card-header text-center"><h2>BRING A FRIEND</h2></div>
                    <div class="card-body">

                        <h4 class="mb-5 text-center text-black1">EARN UPTO RS. 2000 BY INVITING YOUR FRIEND</h4>

                        <div class="row text-center no-gutters">
                            <div class="col p-2">
                                <div class="card bg-light h-100">
                                    <div class="card-body">
                                        <img src="<?= STATIC_IMAGES_PATH ?>mail-invite.svg" height="150" class="mb-2">
                                        <h5>
                                            INVITE
                                        </h5>


                                        Share the invitation link via e-mail or social media.
                                    </div>
                                </div>
                            </div>
                            <div class="col-auto align-self-center px-2"><i class="fas fa-2x text-black-50 fa-ellipsis-h"></i></div>
                            <div class="col p-2">
                                <div class="card bg-light h-100">
                                    <div class="card-body">
                                        <img src="<?= STATIC_IMAGES_PATH ?>registration_img.svg" height="150" class="mb-2">
                                        <h5>
                                            REGISTER
                                        </h5>

                                        Your buddy has to register in A2Z Betting using your referral link
                                    </div>
                                </div>
                            </div>
                            <div class="col-auto align-self-center px-2"><i class="fas fa-2x text-black-50 fa-ellipsis-h"></i></div>
                            <div class="col p-2">
                                <div class="card bg-light h-100">
                                    <div class="card-body">
                                        <img src="<?= STATIC_IMAGES_PATH ?>reward-earn.svg" height="150" class="mb-2">
                                        <h5>
                                            EARN
                                        </h5>

                                        You will earn 100% deposit bonus made by your friend and you will earn rs 100 from every rs 500 cash game played by your friend.
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card bg-light my-4">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col">
                                        <h4 class="m-0 text-black1">UPTO Rs. 1000 DEPOSIT BONUS</h4>
                                    </div>
                                    <div class="col-auto text-center"><i class="fal fa-plus fa-2x"></i></div>
                                    <div class="col">

                                        <b>YOU WILL EARN Rs. 100 REAL CASH FOR EVERY 500RS
                                            CASH GAME THAT YOUR FRIEND PLAYS (UPTO Rs. 1000/ BUDDY)</b>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row align-items-center">
                            <div class="col-auto">
                                <!-- <img src="<?= STATIC_IMAGES_PATH ?>e1.png" height="150"> -->
                            </div>
                            <div class="col">
                                <div class="card bg-light ">
                                    <div class="card-body">
                                        <div class="row align-items-center">
                                            <div class="col">

                                                <!--<div class="form-group">
                                                    <select class="custom-select">
                                                        <option value="">Email/SMS</option>
                                                    </select>
                                                </div>-->

                                                <div class="form-group bring_a_friend_by_contact">
                                                    <section id='section-basic'>
                                                        <aside class='rightSide'>
                                                            
                                                        </aside>
                                                    </section>
                                                </div>


                                                <div class="form-group bring_a_friend_by_email">
                                                    <section id='section-basic_email'>
                                                        <aside class='rightSide'>
                                                            
                                                        </aside>
                                                    </section>
                                                </div>
                                                <a href="javascript:void(0)" ng-click="openContactModal()" class="invite_friends btn btn-block btn-success" data-url="<?php echo base_url("api/bring_a_friend/send_invitations_multiple"); ?>">Invite Friends</a>


                                            </div>
                                            <div class="col-auto"><svg height="60px" viewBox="0 0 14 56.664">
                                                <use xlink:href="#or-v" /></svg></div>
                                            <div class="col text-center">
                                                <p>Share Your Link</p>

                                                <ul class="nav nav-socials justify-content-center">
                                                    <li class="nav-item"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode($referral_link); ?>" class="nav-link rounded fb fab fa-facebook-f"></a></li>
                                                    <li class="nav-item"><a target="_blank" href="https://twitter.com/intent/tweet?text=<?php echo urlencode("I love playing #Rummy. Join me @A2Z Betting to get your Rs." . WELCOME_BONUS . " free joining bonus by clicking this link:"); ?>&url=<?php echo urlencode($referral_link); ?>" class="nav-link rounded tw fab fa-twitter"></a></li>
                                                    <li class="nav-item"><a target="_blank" href="https://api.whatsapp.com/send?phone=&text=<?php echo "I love playing #Rummy. Join me @@A2Z Betting to get your Rs." . WELCOME_BONUS . " free joining bonus by clicking this link: " . urlencode($referral_link); ?>" class="nav-link rounded tw fab fa-whatsapp"></a></li>
                                                    <li class="nav-item"><a style="cursor:pointer;" ng-click="copyToClipboard('<?php echo $referral_link; ?>')" class="nav-link rounded fas fa-link"></a><input type="text" style="display:none;" value="" id="referral_link"></li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-auto">
                               <!--  <img src="<?= STATIC_IMAGES_PATH ?>e2.png" height="150"> -->
                            </div>
                        </div>
                    </div>

                </div>




            </div>
        </div>
    </div>
</div>
</div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>bringAFriendCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>bringAFriendService.js?r=<?= time() ?>"></script>

<script data-name="basic">
                                                        (function () {

                                                            var input = document.querySelector('input[name=phones]'),
                                                                    // init Tagify script on the above inputs
                                                                    tagify = new Tagify(input, {
                                                                        whitelist: ["A# .NET", "A# (Axiom)", "A-0 System", "A+", "A++", "ABAP", "ABC", "ABC ALGOL", "ABSET", "ABSYS", "ACC", "Accent", "Ace DASL", "ACL2", "Avicsoft", "ACT-III", "Action!", "ActionScript", "Ada", "Adenine", "Agda", "Agilent VEE", "Agora", "AIMMS", "Alef", "ALF", "ALGOL 58", "ALGOL 60", "ALGOL 68", "ALGOL W", "Alice", "Alma-0", "AmbientTalk", "Amiga E", "AMOS", "AMPL", "Apex (Salesforce.com)", "APL", "AppleScript", "Arc", "ARexx", "Argus", "AspectJ", "Assembly language", "ATS", "Ateji PX", "AutoHotkey", "Autocoder", "AutoIt", "AutoLISP / Visual LISP", "Averest", "AWK", "Axum", "Active Server Pages", "ASP.NET", "B", "Babbage", "Bash", "BASIC", "bc", "BCPL", "BeanShell", "Batch (Windows/Dos)", "Bertrand", "BETA", "Bigwig", "Bistro", "BitC", "BLISS", "Blockly", "BlooP", "Blue", "Boo", "Boomerang", "Bourne shell (including bash and ksh)", "BREW", "BPEL", "B", "C--", "C++ � ISO/IEC 14882", "C# � ISO/IEC 23270", "C/AL", "Cach� ObjectScript", "C Shell", "Caml", "Cayenne", "CDuce", "Cecil", "Cesil", "C�u", "Ceylon", "CFEngine", "CFML", "Cg", "Ch", "Chapel", "Charity", "Charm", "Chef", "CHILL", "CHIP-8", "chomski", "ChucK", "CICS", "Cilk", "Citrine (programming language)", "CL (IBM)", "Claire", "Clarion", "Clean", "Clipper", "CLIPS", "CLIST", "Clojure", "CLU", "CMS-2", "COBOL � ISO/IEC 1989", "CobolScript � COBOL Scripting language", "Cobra", "CODE", "CoffeeScript", "ColdFusion", "COMAL", "Combined Programming Language (CPL)", "COMIT", "Common Intermediate Language (CIL)", "Common Lisp (also known as CL)", "COMPASS", "Component Pascal", "Constraint Handling Rules (CHR)", "COMTRAN", "Converge", "Cool", "Coq", "Coral 66", "Corn", "CorVision", "COWSEL", "CPL", "CPL", "Cryptol", "csh", "Csound", "CSP", "CUDA", "Curl", "Curry", "Cybil", "Cyclone", "Cython", "Java", "Javascript", "M2001", "M4", "M#", "Machine code", "MAD (Michigan Algorithm Decoder)", "MAD/I", "Magik", "Magma", "make", "Maple", "MAPPER now part of BIS", "MARK-IV now VISION:BUILDER", "Mary", "MASM Microsoft Assembly x86", "MATH-MATIC", "Mathematica", "MATLAB", "Maxima (see also Macsyma)", "Max (Max Msp � Graphical Programming Environment)", "Maya (MEL)", "MDL", "Mercury", "Mesa", "Metafont", "Microcode", "MicroScript", "MIIS", "Milk (programming language)", "MIMIC", "Mirah", "Miranda", "MIVA Script", "ML", "Model 204", "Modelica", "Modula", "Modula-2", "Modula-3", "Mohol", "MOO", "Mortran", "Mouse", "MPD", "Mathcad", "MSIL � deprecated name for CIL", "MSL", "MUMPS", "Mystic Programming L"],
                                                                        blacklist: [".NET", "PHP"], // <-- passed as an attribute in this demo
                                                                    });


// "remove all tags" button event listener
//document.querySelector('.tags--removeAllBtn')
//    .addEventListener('click', tagify.removeAllTags.bind(tagify))

// Chainable event listeners
                                                            tagify.on('add', onAddTag)
                                                                    .on('remove', onRemoveTag)
                                                                    .on('input', onInput)
                                                                    .on('edit', onTagEdit)
                                                                    .on('invalid', onInvalidTag)
                                                                    .on('click', onTagClick)
                                                                    .on('dropdown:show', onDropdownShow)
                                                                    .on('dropdown:hide', onDropdownHide);

// tag added callback
                                                            function onAddTag(e) {
                                                                console.log("onAddTag: ", e.detail);
                                                                console.log("original input value: ", input.value)
                                                                tagify.off('add', onAddTag) // exmaple of removing a custom Tagify event
                                                            }

// tag remvoed callback
                                                            function onRemoveTag(e) {
                                                                console.log(e.detail);
                                                                console.log("tagify instance value:", tagify.value)
                                                            }

// on character(s) added/removed (user is typing/deleting)
                                                            function onInput(e) {
                                                                console.log(e.detail);
                                                                console.log("onInput: ", e.detail);
                                                            }

                                                            function onTagEdit(e) {
                                                                console.log("onTagEdit: ", e.detail);
                                                            }

// invalid tag added callback
                                                            function onInvalidTag(e) {
                                                                console.log("onInvalidTag: ", e.detail);
                                                            }

// invalid tag added callback
                                                            function onTagClick(e) {
                                                                console.log(e.detail);
                                                                console.log("onTagClick: ", e.detail);
                                                            }

                                                            function onDropdownShow(e) {
                                                                console.log("onDropdownShow: ", e.detail)
                                                            }

                                                            function onDropdownHide(e) {
                                                                console.log("onDropdownHide: ", e.detail)
                                                            }

                                                        })()
</script>
<script data-name="basic">
            (function () {

                var input = document.querySelector('input[name=emails]'),
                        // init Tagify script on the above inputs
                        tagify = new Tagify(input, {
                            whitelist: ["A# .NET", "A# (Axiom)", "A-0 System", "A+", "A++", "ABAP", "ABC", "ABC ALGOL", "ABSET", "ABSYS", "ACC", "Accent", "Ace DASL", "ACL2", "Avicsoft", "ACT-III", "Action!", "ActionScript", "Ada", "Adenine", "Agda", "Agilent VEE", "Agora", "AIMMS", "Alef", "ALF", "ALGOL 58", "ALGOL 60", "ALGOL 68", "ALGOL W", "Alice", "Alma-0", "AmbientTalk", "Amiga E", "AMOS", "AMPL", "Apex (Salesforce.com)", "APL", "AppleScript", "Arc", "ARexx", "Argus", "AspectJ", "Assembly language", "ATS", "Ateji PX", "AutoHotkey", "Autocoder", "AutoIt", "AutoLISP / Visual LISP", "Averest", "AWK", "Axum", "Active Server Pages", "ASP.NET", "B", "Babbage", "Bash", "BASIC", "bc", "BCPL", "BeanShell", "Batch (Windows/Dos)", "Bertrand", "BETA", "Bigwig", "Bistro", "BitC", "BLISS", "Blockly", "BlooP", "Blue", "Boo", "Boomerang", "Bourne shell (including bash and ksh)", "BREW", "BPEL", "B", "C--", "C++ � ISO/IEC 14882", "C# � ISO/IEC 23270", "C/AL", "Cach� ObjectScript", "C Shell", "Caml", "Cayenne", "CDuce", "Cecil", "Cesil", "C�u", "Ceylon", "CFEngine", "CFML", "Cg", "Ch", "Chapel", "Charity", "Charm", "Chef", "CHILL", "CHIP-8", "chomski", "ChucK", "CICS", "Cilk", "Citrine (programming language)", "CL (IBM)", "Claire", "Clarion", "Clean", "Clipper", "CLIPS", "CLIST", "Clojure", "CLU", "CMS-2", "COBOL � ISO/IEC 1989", "CobolScript � COBOL Scripting language", "Cobra", "CODE", "CoffeeScript", "ColdFusion", "COMAL", "Combined Programming Language (CPL)", "COMIT", "Common Intermediate Language (CIL)", "Common Lisp (also known as CL)", "COMPASS", "Component Pascal", "Constraint Handling Rules (CHR)", "COMTRAN", "Converge", "Cool", "Coq", "Coral 66", "Corn", "CorVision", "COWSEL", "CPL", "CPL", "Cryptol", "csh", "Csound", "CSP", "CUDA", "Curl", "Curry", "Cybil", "Cyclone", "Cython", "Java", "Javascript", "M2001", "M4", "M#", "Machine code", "MAD (Michigan Algorithm Decoder)", "MAD/I", "Magik", "Magma", "make", "Maple", "MAPPER now part of BIS", "MARK-IV now VISION:BUILDER", "Mary", "MASM Microsoft Assembly x86", "MATH-MATIC", "Mathematica", "MATLAB", "Maxima (see also Macsyma)", "Max (Max Msp � Graphical Programming Environment)", "Maya (MEL)", "MDL", "Mercury", "Mesa", "Metafont", "Microcode", "MicroScript", "MIIS", "Milk (programming language)", "MIMIC", "Mirah", "Miranda", "MIVA Script", "ML", "Model 204", "Modelica", "Modula", "Modula-2", "Modula-3", "Mohol", "MOO", "Mortran", "Mouse", "MPD", "Mathcad", "MSIL � deprecated name for CIL", "MSL", "MUMPS", "Mystic Programming L"],
                            blacklist: [".NET", "PHP"], // <-- passed as an attribute in this demo
                        });


// "remove all tags" button event listener
//document.querySelector('.tags--removeAllBtn')
//    .addEventListener('click', tagify.removeAllTags.bind(tagify))

// Chainable event listeners
                tagify.on('add', onAddTag)
                        .on('remove', onRemoveTag)
                        .on('input', onInput)
                        .on('edit', onTagEdit)
                        .on('invalid', onInvalidTag)
                        .on('click', onTagClick)
                        .on('dropdown:show', onDropdownShow)
                        .on('dropdown:hide', onDropdownHide);

// tag added callback
                function onAddTag(e) {
                    console.log("onAddTag: ", e.detail);
                    console.log("original input value: ", input.value)
                    tagify.off('add', onAddTag) // exmaple of removing a custom Tagify event
                }

// tag remvoed callback
                function onRemoveTag(e) {
                    console.log(e.detail);
                    console.log("tagify instance value:", tagify.value)
                }

// on character(s) added/removed (user is typing/deleting)
                function onInput(e) {
                    console.log(e.detail);
                    console.log("onInput: ", e.detail);
                }

                function onTagEdit(e) {
                    console.log("onTagEdit: ", e.detail);
                }

// invalid tag added callback
                function onInvalidTag(e) {
                    console.log("onInvalidTag: ", e.detail);
                }

// invalid tag added callback
                function onTagClick(e) {
                    console.log(e.detail);
                    console.log("onTagClick: ", e.detail);
                }

                function onDropdownShow(e) {
                    console.log("onDropdownShow: ", e.detail)
                }

                function onDropdownHide(e) {
                    console.log("onDropdownHide: ", e.detail)
                }

            })()
</script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.15.0/themes/prism.min.css">
<script src='https://cdnjs.cloudflare.com/ajax/libs/prism/1.15.0/prism.min.js'></script>
