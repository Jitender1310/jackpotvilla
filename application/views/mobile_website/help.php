<section class="workspace">
    <div class="page-title">Help</div>
    <div class="container-fluid py-3">
        <p>We are constantly trying to ensure that you have a very smooth and enjoyable Rummy experience. As a result, we
            have tried to address most questions that may arise while you are on King Of Rummy
        </p>
        <h6 class="text-primary">How to Play Rummy?</h6>
        <div class="custom-control custom-radio">
            <input type="radio" id="q1" name="ans" class="custom-control-input">
            <label class="custom-control-label" for="q1">Demos</label>
        </div>
        <div class="custom-control custom-radio">
            <input type="radio" id="q2" name="ans" class="custom-control-input">
            <label class="custom-control-label" for="q2">Rules</label>
        </div>
        <div class="custom-control custom-radio">
            <input type="radio" id="q3" name="ans" class="custom-control-input">
            <label class="custom-control-label" for="q3">Tips and </label>
        </div>
        <div class="custom-control custom-radio">
            <input type="radio" id="q4" name="ans" class="custom-control-input">
            <label class="custom-control-label" for="q4">Tricks </label>
        </div>
        <div class="custom-control custom-radio">
            <input type="radio" id="q5" name="ans" class="custom-control-input">
            <label class="custom-control-label" for="q5">Learn Rummy </label>
        </div>
    </div>
</section>