<!-- Edit email Popup Starts -->
<div class="modal" id="editMobileModal" data-backdrop='static' data-keyboard='false'>
    <div class="modal-dialog">
        <form ng-submit="updateNewMobile()" id="editEmailForm" ng-keyup="ep_error = {}">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Update Mobile Number</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Enter your Valid Mobile Number: <span class="text-danger">*</span></label>
                            <input type="tel"  class="form-control" placeholder="" ng-readonly="show_otp_field"  name="firstname" ng-model="editProfileObj.mobile">
                            <span class="text-danger">{{ep_error.mobile}}</span>
                        </div>
                    </div>
                    <div class="form-row" ng-if="show_otp_field">
                        <div class="form-group col-md-12">
                            <label>OTP: <span class="text-danger">*</span></label>
                            <input type="tel"  class="form-control" placeholder="Verification Code" name="otp" ng-model="editProfileObj.otp">
                            <span class="text-danger">{{ep_error.otp}}</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary text-white"  ng-if="!show_otp_field" type="submit"><b>Update</b></button>
                    <button class="btn btn-info text-white"  ng-if="show_otp_field" type="button" ng-click="resendOTP()"><b>Resend</b></button>
                    <button class="btn btn-primary text-white"  ng-if="show_otp_field" type="button" ng-click="verifyOTP()"><b>Verify</b></button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Edit Email POPUP Ends -->