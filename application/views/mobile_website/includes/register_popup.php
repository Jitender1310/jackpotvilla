<div ng-controller="registerCtrl" ng-init="reg_user.referred_by_code = '<?php echo $referral_code; ?>'" class="modal fade" id="modal-register" tabindex="-1" role="dialog" aria-labelledby="registerModal" aria-hidden="true">
    <form class="px-4" id="registerForm" ng-submit="doRegister()" ng-keyup="reg_error = {}">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Register</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Username /Email/Mobile</label>
                        <input type="text" placeholder="Username/Email/Mobile" name="username" class="form-control" ng-model="reg_user.username">
                        <span class="text-danger">{{reg_error.username}}</span>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="text" name="password" class="form-control" placeholder="Password" ng-model="reg_user.password">
                        <span class="text-danger">{{reg_error.password}}</span>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email" placeholder="Email Id"  ng-model="reg_user.email">
                        <span class="text-danger">{{reg_error.email}}</span>
                    </div>
                    <div class="form-group">
                        <label>Mobile</label>
                        <input type="text" name="mobile" class="form-control" placeholder="Mobile Number"  ng-model="reg_user.mobile">
                        <span class="text-danger">{{reg_error.mobile}}</span>
                    </div>
                </div>
                <div class="modal-footer">
                    <a data-toggle="modal" href="#modal-login" class="btn btn-block rounded-0 text-uppercase btn1" data-dismiss="modal"><small><b>Already have account Login ?</b></small></a>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Register</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>registerCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>registerService.js?r=<?= time() ?>"></script>