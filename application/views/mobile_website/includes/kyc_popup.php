<!-- Edit kyc Popup Starts -->
<div class="modal" id="editKycModal" data-backdrop='static' data-keyboard='false'>
    <div class="modal-dialog modal-lg" style="max-width: 100%">
        <form ng-submit="updateKycDetails()" ng-click="kyc_error = {}">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Your KYC Status</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-custom table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Information</th>
                                        <th>Details</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Address Proof
                                        </td>
                                        <td>
                                            <select class="form-control"  ng-readonly="editProfileObj.pan_card_status == 'Approved'" id="address_proof_type" required name="address_proof_type" ng-model="editProfileObj.address_proof_type">
                                                <option value="">--Select Proof--</option>
                                                <option value="Aadhaar Card">Aadhaar Card</option>
                                                <option value="Voter ID">Voter ID</option>
                                                <option value="Driving License">Driving License</option>
                                                <option value="Passport">Passport</option>
                                            </select>
                                            <label class="error" ng-if="kyc_error.address_proof_type">{{kyc_error.address_proof_type}}</label>
                                        </td>
                                        <td  class='{{editProfileObj.address_proof_status_bootstrap_css ? editProfileObj.address_proof_status_bootstrap_css :"text-danger" }}'>
                                            {{editProfileObj.address_proof_status==""?"Not Submitted":editProfileObj.address_proof_status}}
                                        </td>
                                        <td>
                                            <input type="file" name="address_proof" id="address_proof" required ng-if="editProfileObj.address_proof !== 'Approved'">
                                            <label class="error" ng-if="kyc_error.address_proof">{{kyc_error.address_proof}}</label>
                                            <a target="_blank" href="{{editProfileObj.address_proof}}" ng-if="editProfileObj.address_proof !== ''">View file</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            PAN Card
                                        </td>
                                        <td>
                                            <input type="text" ng-readonly="editProfileObj.pan_card_status == 'Approved'" class="form-control" required name="pan_card_number" placeholder="Enter pan card number" ng-model="editProfileObj.pan_card_number">
                                            <label class="error" ng-if="kyc_error.pan_card_number">{{kyc_error.pan_card_number}}</label>
                                        </td>
                                        <td class='{{editProfileObj.pan_card_status_bootstrap_css ? editProfileObj.pan_card_status_bootstrap_css :"text-danger" }}'>
                                            {{editProfileObj.pan_card_status==""?"Not Submitted":editProfileObj.pan_card_status}}
                                        </td>
                                        <td>
                                            <input type="file" name="pan_card" id="pan_card" required ng-if="editProfileObj.pan_card !== 'Approved'">
                                            <label class="error" ng-if="kyc_error.pan_card">{{kyc_error.pan_card}}</label>
                                            <a target="_blank" href="{{editProfileObj.pan_card}}" ng-if="editProfileObj.pan_card !== ''">View file</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <p><span class="text-danger">Note:</span> Max allowed size is : 2 MB, Allowed file formats JPEG, JPG, and PDF</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary text-white" type="submit"><b>Update</b></button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Edit Kyc POPUP Ends -->