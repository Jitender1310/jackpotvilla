<!-- Edit profile Popup Starts -->
<div class="modal" id="editProfileModal" data-backdrop='static' data-keyboard='false'>
    <div class="modal-dialog modal-lg" style="width:98%">
        <form ng-submit="updateProfile()" id="profileForm" ng-keyup="ep_error = {}">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Edit Profile</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Firstname <span class="text-danger">*</span></label>
                            <input type="text"  class="form-control" placeholder=""  name="firstname" ng-model="editProfileObj.firstname">
                            <span class="text-danger">{{ep_error.firstname}}</span>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Lastname <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" placeholder="" name="lastname" ng-model="editProfileObj.lastname">
                            <span class="text-danger">{{ep_error.lastname}}</span>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Date of Birth <span class="text-danger">*</span></label>
                            <input type="text" class="form-control datepicker" autocomplete="off" placeholder="Date of Birth (DD-MM-YYYY)" name="date_of_birth" ng-model="editProfileObj.date_of_birth">
                            <span class="text-danger">{{ep_error.lastname}}</span>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Gender <span class="text-danger">*</span></label>
                            <br/>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline1" class="custom-control-input" name="gender" value="Male" ng-model="editProfileObj.gender">
                                <label class="custom-control-label" for="customRadioInline1">Male</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline2" class="custom-control-input"  name="gender" value="Female" ng-model="editProfileObj.gender">
                                <label class="custom-control-label" for="customRadioInline2">Female</label>
                            </div>
                            <span class="text-danger">{{ep_error.gender}}</span>
                        </div>

                        <div class="form-group col-md-12">
                            <label>Address Line 1 <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" placeholder="" name="address_line_1" ng-model="editProfileObj.address_line_1">
                            <span class="text-danger">{{ep_error.address_line_1}}</span>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Address Line 2 <span class="text-danger"></span></label>
                            <input type="text" class="form-control" placeholder="" name="address_line_2" ng-model="editProfileObj.address_line_2">
                            <span class="text-danger">{{ep_error.address_line_2}}</span>
                        </div>
                        <div class="form-group col-md-4">
                            <label>State <span class="text-danger">*</span></label>
                            <select class="form-control" name="states_id" ng-model="editProfileObj.states_id">
                                <option value="">--Choose State--</option>
                                <option ng-repeat="state in statesList.states" ng-value="state.id">{{state.state_name}}</option>
                            </select>
                            <span class="text-danger">{{ep_error.states_id}}</span>
                        </div>
                        <div class="form-group col-md-4">
                            <label>City <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" placeholder="" name="city" ng-model="editProfileObj.city">
                            <span class="text-danger">{{ep_error.city}}</span>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Pincode <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" placeholder="" name="pin_code" ng-model="editProfileObj.pin_code">
                            <span class="text-danger">{{ep_error.pin_code}}</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary text-white" type="submit"><b>Update</b></button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Edit profile Popup Ends -->