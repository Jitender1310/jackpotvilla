<div class="modal" id="tournamentDetails" data-backdrop='static' data-keyboard='false'>
    <div class="modal-dialog modal-lg" style="max-width: 75%">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tournament Details</h4>

                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <style>
                .actve {
                    color: white!important;
                    background-color: grey!important;
                }
            </style>
            <div class="modal-body p-0">
                <div class="row no-gutters">
                    <div class="col-md-9">
                        <div class="h-100 border-right" >
                            <div class="p-4" style="min-height:400px;">
                                <div class="container nopad">
                                    <ul class="nav nav-tabs mb-4" role="tablist">
                                        <li class="nav-item" style="cursor:pointer;">
                                            <a class="nav-link"  data-toggle="tab" ng-click="tab_id = 1" ng-class="tab_id === 1 ? 'actve' : ''">Tournament Summary</a>
                                        </li>
                                        <li class="nav-item" style="cursor:pointer;">
                                            <a class="nav-link" data-toggle="tab" ng-click="tab_id = 2" ng-class="tab_id === 2 ? 'actve' : ''">Tournament Structure</a>
                                        </li>
                                        <li class="nav-item" style="cursor:pointer;">
                                            <a class="nav-link" data-toggle="tab" ng-click="tab_id = 3" ng-class="tab_id === 3 ? 'actve' : ''">Tournament Prizes</a>
                                        </li>
                                        <li class="nav-item" style="cursor:pointer;" ng-click="GetGameProgress(tournamentDetailsObj.cloned_tournaments_id);">
                                            <a class="nav-link" data-toggle="tab" ng-click="tab_id = 4" ng-class="tab_id === 4 ? 'actve' : ''">View Progress</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-content">
                                    <table class="table table-bordered" ng-show="tab_id === 1">

                                        <tr class="bg-light">
                                            <th >Name</th>
                                            <th>Duration</th>
                                            <th>Format</th>
                                            <th>Frequency</th>
                                            <th>Entry</th>
                                            <th>M.Players/Table</th>
                                        </tr>
                                        <tr>
                                            <td>{{tournamentDetailsObj.tournament_title}} (#{{tournamentDetailsObj.ref_id}})</td>
                                            <td>{{tournamentDetailsObj.duration_in_minutes}}</td>
                                            <td>{{tournamentDetailsObj.tournament_format}}</td>
                                            <td>{{tournamentDetailsObj.frequency}}</td>
                                            <td>{{tournamentDetailsObj.entry_value}}</td>
                                            <td>{{tournamentDetailsObj.players_per_table}}</td>
                                        </tr>
                                    </table>
                                    <table class="table table-bordered text-center" ng-show="tab_id === 1">
                                        <tr>
                                            <td colspan="2" class="h4 bg-light">Expected Prize:  <span style="color:green;">{{tournamentDetailsObj.expected_prize_amount}} Rs /-</span></td>
                                        </tr>
                                        <tr>
                                            <td class="h4">Total Prizes :  <span style="color:green;">{{tournamentDetailsObj.prize_distributions.length}}</span></td>
                                            <td class="h4">Total Rounds :  <span style="color:green;">{{tournamentDetailsObj.tournament_structure.length}}</span></td>
<!--                                                <td colspan="2"><h5>Prizes Start from Round 1</h5></td>-->
                                        </tr>
                                    </table>



                                    <table class="table table-bordered" ng-show="tab_id === 2">

                                        <tr class="bg-light">
                                            <th >Round</th>
                                            <th>Players</th>
                                            <th>Qualification</th>
                                            <th>Deals</th>
                                        </tr>


                                        <tr ng-repeat="item in tournamentDetailsObj.tournament_structure">
                                            <td>Round {{item.round_number}} ({{item.round_title}})</td>
                                            <td>{{item.players_text}}</td>
                                            <td>{{item.qualifiction_text}}</td>
                                            <td>{{item.deals_text}}</td>
                                        </tr>

                                    </table>

                                    <table class="table table-bordered" ng-show="tab_id === 3">

                                        <tr class="bg-light">
                                            <th >Position</th>
                                            <th>Players</th>
                                            <th>Prize</th>
                                            <th>Given in Round</th>
                                        </tr>


                                        <tr ng-repeat="item in tournamentDetailsObj.prize_distributions">
                                            <td>{{item.display_position}}</td>
                                            <td>{{item.no_of_players}}</td>
                                            <td>{{item.prize_value}}</td>
                                            <td>Round 2 (Final)</td>
                                        </tr>

                                    </table>
                                    <style>
                                        .nopad {
                                            padding-left:0px;
                                            padding-right:0px;
                                        }
                                        .clr {
                                            background-color: #ffc107;
                                        }
                                    </style>

                                    <div class="col-md-12">
                                        <div class="row "  ng-show="tab_id === 4">
                                            <div class="col-sm-3 nopad" >
                                                <table class="table table-bordered" >
                                                    <tr class="bg-light" style="width: 100%;">
                                                        <th >Round</th>
                                                    </tr>
                                                    <tr ng-repeat="item in gameProgress track by $index" style="cursor:pointer;" ng-click="GetTablesInfo(item);" ng-class="item.round_number === roundno ? 'clr' : '';">
                                                        <td >Round {{item.round_number}}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-sm-3 nopad" >
                                                <table class="table table-bordered" >
                                                    <tr class="bg-light">
                                                        <th >Table</th>
                                                        <th >Deal</th>
                                                    </tr>
                                                    <tr ng-repeat="item1 in tablesInfo" style="cursor:pointer;" ng-if="tablesInfo.length > 0" ng-click="GetPlayers(item1);" ng-class="item1.tournamentTableId === tableid ? 'clr' : '';">
                                                        <td>{{item1.tournamentTableId}}</td>
                                                        <td>{{item1.players.length}} / 6</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-sm-6 nopad" >
                                                <table class="table table-bordered" >
                                                    <tr class="bg-light">
                                                        <th >Players</th>
                                                        <th >Chips</th>
                                                        <th >Status</th>
                                                    </tr>
                                                    <tr ng-repeat="item2 in playersInfo" ng-if="playersInfo.length > 0" >
                                                        <td>{{item2.player_name}}</td>
                                                        <td>{{item2.chips}}</td>
                                                        <td ng-if="item2.isQualified">Qualified</td>
                                                        <td ng-if="!item2.isQualified">Not Qualified</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="alert alert-warning" ng-if="tournamentDetailsObj.allowed != 'All'">
                                        <i class="fal fa-exclamation-triangle ml-1"></i> This tournament is open for {{tournamentDetailsObj.allowed_club_types_text}} level club players only.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="bg-light h-100">
                            <div class="p-4">

                                <div ng-if="tournamentDetailsObj.tournament_status == 'Created'" 
                                     class="{{tournamentDetailsObj.tournament_status=='Created' ? 'bg-danger' : ''}}">
                                    <p>Registration Starts</p>
                                    <h5>{{tournamentDetailsObj.display_registration_start_date_time}}</h5>
                                    <p style="color:white; font-weight: bold;"> {{tournamentDetailsObj.starting_timer}}</p>
                                </div>


                                <div ng-if="tournamentDetailsObj.tournament_status != 'Created'" >
                                    <p>Joined Players</p>
                                    <button type="button" ng-click="showJoinedPlayersPopup()" class="btn-sm">View Players</button>
                                </div>


                                <hr/>
                                <div class="{{tournamentDetailsObj.tournament_status=='Registration_Start' ? 'bg-danger' : ''}}">
                                    <p>Registration Closes</p>
                                    <h5>{{tournamentDetailsObj.display_registration_close_date_time}}</h5>

                                    <p style="color:white; font-weight: bold;"> {{tournamentDetailsObj.closing_timer}}</p>

                                </div>
                                <hr/>
                                <div class="{{tournamentDetailsObj.tournament_status=='Registration_Closed' ? 'bg-danger' : ''}}">
                                    <p>Tournament Starts</p>
                                    <h5>{{tournamentDetailsObj.display_tournament_date_time}} </h5>
                                    <p style="color:white; font-weight: bold;"> {{tournamentDetailsObj.tournament_start_timer}}</p>
                                </div>


                                <div ng-if="tournamentDetailsObj.tournament_status === 'Running'" class="{{tournamentDetailsObj.tournament_status=='Running' ? 'bg-danger' : ''}}">
                                    <p>Tournament Duration</p>
                                    <h5>{{tournamentDetailsObj.tournament_duration}} </h5>
                                </div>

                                <div ng-if="tournamentDetailsObj.tournament_status === 'Completed'">
                                    <p>Tournament Status</p>
                                    <h4>Completed</h4>
                                </div>

                                <div ng-if="tournamentDetailsObj.tournament_status === 'Registration_Start'">
                                    <hr />
                                    <div>
                                        <button ng-if="tournamentDetailsObj.is_joined != 1" type="button" class="btn btn-info btn-block" ng-click="joinTournament(tournamentDetailsObj.cloned_tournaments_id);">REGISTER NOW</button>
                                        <button ng-if="tournamentDetailsObj.is_joined === 1" type="button" class="btn btn-info btn-block" ng-click="unJoinTournament(tournamentDetailsObj.cloned_tournaments_id);">UN REGISTER</button>
                                    </div>
                                </div>

                                <div ng-if="tournamentDetailsObj.tournament_status === 'Running' && tournamentDetailsObj.is_joined == 1">
                                    <hr />
                                    <div>
                                        <button  type="button" class="btn btn-info btn-block" ng-click="openPlayNowTournamentWindow(tournamentDetailsObj.cloned_tournaments_id, '<?= $player_details->access_token ?>', 'Tournament');">Take Seat</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    $this->load->view("includes/tournament_joined_players_popup")?>