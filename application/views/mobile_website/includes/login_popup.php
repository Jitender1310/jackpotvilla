<div ng-controller="loginCtrl" class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="loginModel" aria-hidden="true">
    <form id="loginForm"  ng-submit="doLogin()" ng-keyup="login_error = {}" autocomplete="off">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Username /Email/Mobile</label>
                        <input type="text" placeholder="Username" class="form-control" name="username"  ng-model="user.username">
                        <span class="text-danger">{{login_error.username}}</span>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" placeholder="Password" class="form-control" name="password" ng-model="user.password">
                        <span class="text-danger">{{login_error.password}}</span>
                    </div>

                </div>
                <div class="modal-footer">

                    <a href="<?= base_url() ?>forgot_password">Forgot Password ? </a>

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Login</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>loginCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>loginService.js?r=<?= time() ?>"></script>