</div>


<?php $this->load->view("mobile_website/includes/login_popup") ?>
<?php $this->load->view("mobile_website/includes/register_popup") ?>


<script type="text/javascript" src="<?= STATIC_MOBILE_JS_PATH ?>bootstrap.min.js"></script>
<script type="text/javascript" src="<?= STATIC_MOBILE_JS_PATH ?>popper.min.js"></script>
<script src="<?= STATIC_JS_PATH ?>additional-methods.js"></script>
<script src="<?= STATIC_JS_PATH ?>sweetalert.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script type="text/javascript" src="<?= STATIC_MOBILE_JS_PATH ?>angular-slick.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="//stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>
<script src="<?= STATIC_JS_PATH ?>jquery.datetimepicker.full.min.js"></script>
<script src="<?= STATIC_JS_PATH ?>loadingoverlay.min.js"></script>
<script src="//apis.google.com/js/platform.js" async defer></script>

<script type="text/javascript" src="<?= STATIC_MOBILE_JS_PATH ?>custom.js"></script>



<?php if (is_logged_in()) { ?>
    <script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>profileCtrl.js?r=<?= time() ?>"></script>
    <script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>profileService.js?r=<?= time() ?>"></script>
    <script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>bonusAvailableCtrl.js?r=<?= time() ?>"></script>
    <script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>bonusAvailableService.js?r=<?= time() ?>"></script>


    <script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>gameTrackerCtrl.js?r=<?= time() ?>"></script>
    <script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>gameTrackerService.js?r=<?= time() ?>"></script>
<?php } ?>

<script>
    theme.icons('<?= STATIC_IMAGES_PATH ?>/icons.svg');
</script>

</body>

</html>