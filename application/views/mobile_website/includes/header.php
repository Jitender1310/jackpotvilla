<!doctype html>
<html lang="en" ng-app="rummyApp" ng-cloak="" ng-init="GAME_SERVER_ID = '<?= GAME_SERVER_IP ?>'">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="<?= STATIC_MOBILE_CSS_PATH ?>scss/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?= STATIC_MOBILE_CSS_PATH ?>f5.css">
    <link rel="stylesheet" href="<?= STATIC_CSS_PATH ?>jquery.datetimepicker.min.css">
    <!--<meta name="google-signin-client_id" content="1016768204382-42tuasfoh21s0vdpoucs94mraqra6npl.apps.googleusercontent.com">-->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= STATIC_MOBILE_CSS_PATH ?>custom.css">
    <script src="<?= STATIC_JS_PATH ?>jquery.min.js"></script>
    <script src="<?= STATIC_JS_PATH ?>angular.min.js"></script>
    <script src="<?= STATIC_JS_PATH ?>jquery.validate.min.js"></script>
    <script src="<?= STATIC_ANGULAR_PATH ?>app.js?r=<?= time() ?>"></script>
    <script src="<?= STATIC_JS_PATH ?>app_urls.js?r=<?= time() ?>"></script>
    <script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>commonCtrl.js?r=<?= time() ?>"></script>
    <script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>chipsService.js?r=<?= time() ?>"></script>
    <script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>statesService.js?r=<?= time() ?>"></script>
    <script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>playNowService.js?r=<?= time() ?>"></script>
    <script src="<?= STATIC_JS_PATH ?>sfs2x-api-1.7.11.js?r=<?= time() ?>"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/purl/2.3.1/purl.min.js"></script>
    <script>
        var sfs;
    </script>
    <title><?= SITE_TITLE ?></title>
    <style type="text/css">
        .custom-pagination {
            text-align: center
        }

        .custom-pagination>.pagination {
            display: inline-block;
            padding-left: 0;
            margin: 20px 0;
            border-radius: 4px
        }

        .custom-pagination>.pagination>li {
            display: inline
        }

        .custom-pagination>.pagination>li:first-child>a {
            margin-left: 0;
            border-top-left-radius: 4px;
            border-bottom-left-radius: 4px
        }

        .custom-pagination>.pagination>li:last-child>a {
            border-top-right-radius: 4px;
            border-bottom-right-radius: 4px
        }

        .custom-pagination>.pagination>li>a:focus,
        .custom-pagination>.pagination>li>a:hover {
            z-index: 2;
            color: #23527c;
            background-color: #eee;
            border-color: #ddd
        }

        .custom-pagination>.pagination>li>a {
            position: relative;
            float: left;
            padding: 6px 12px;
            margin-left: -1px;
            line-height: 1.42857143;
            color: #337ab7;
            text-decoration: none;
            background-color: #fff;
            border: 1px solid #ddd
        }

        .custom-pagination>.pagination a {
            text-decoration: none !important;
            z-index: 0 !important
        }

        .custom-pagination>.pagination>.active>a,
        .custom-pagination>.pagination>.active>a:focus,
        .custom-pagination>.pagination>.active>a:hover {
            z-index: 3;
            color: #fff;
            cursor: default;
            background-color: #337ab7;
            border-color: #337ab7
        }
    </style>
</head>

<body ng-controller="commonCtrl">

    <style>
        em.invalid {
            color: red;
        }
    </style>
    <div class="loadicons d-none"></div>
    <div class="wrapper">
        <?php if (!is_logged_in()) { ?>
            <section class="top-actions workspace-offset">
                <div class="row no-gutters">
                    <div class="col"><a data-toggle="modal" href="#modal-login" class="btn btn-block rounded-0 text-uppercase btn1"><small><b>Login</b></small></a></div>
                    <div class="col"><a data-toggle="modal" href="#modal-register" class="btn btn-block rounded-0 text-uppercase btn2"><small><b>Register</b></small></a></div>
                </div>
            </section>
        <?php } ?>
        <header class="workspace-offset">
            <div class="container-fluid">
                <div class="row align-items-center no-gutters">
                    <div class="col"><a href="<?= base_url() ?>" class="logo" style="background-image: url(<?= STATIC_IMAGES_PATH ?>logo.png);"></a></div>
                    <div class="col-auto">
                        <div class="row no-gutters align-items-center">
                            <div class="col-auto pr-1">
                                <svg height="25px" width="25px" viewBox="0 0 1000.111 1000.907">
                                    <use xlink:href="#group" />
                                </svg>
                            </div>
                            <div class="col">
                                <small class="d-block"><i class="fas fa-circle text-success mr-1"></i>PLAYERS</small>
                                <strong class="text-primary">73947</strong>
                            </div>
                        </div>
                    </div>
                    <?php if (is_logged_in()) { ?>
                        <div class="col-auto pl-3">
                            <a href="#pts" data-toggle="modal"><i class="fas fa-2x fa-crown"></i></a>
                        </div>
                    <?php } ?>
                        <div class="col-auto"><button class="fal fa-bars toggle sidebar-action"></button></div>
                    </div>
                </div>
            </header>
            <section class="sidebar-overlay sidebar-action"></section>
            <section class="sidebar">
                <button class="fal fa-arrow-left toggle sidebar-action"></button>
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a href="<?= base_url() ?>" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url() ?>promotions" class="nav-link">Promotions</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url() ?>bring_a_friend" class="nav-link">Bring a Friend</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url() ?>how_to_play" class="nav-link">Learn Rummy</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url() ?>help" class="nav-link">Help</a>
                    </li>
                    <!--                    <li class="nav-item">
                                                <a class="nav-link">&nbsp;</a>
                                            </li>-->
                    <li class="nav-item">
                        <a href="<?= base_url() ?>game_lobby" class="nav-link">Gamelobby</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url() ?>account" class="nav-link">Account</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url() ?>account_overview" class="nav-link">Account Overview</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url() ?>deposit" class="nav-link">Deposit</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url() ?>payment_history" class="nav-link">Deposit History</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url() ?>transactions" class="nav-link">View Transaction</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url() ?>bonus_transactions" class="nav-link">Bonus Transaction</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url() ?>rps_transactions" class="nav-link">Rps Transaction</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url() ?>withdraw" class="nav-link">Withdraw Cash</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url() ?>preferences" class="nav-link">Preferences</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url() ?>game_tracker" class="nav-link">My Game Tracker</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url() ?>level_info" class="nav-link">Level Info</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url() ?>logout" class="nav-link">Logout</a>
                    </li>
                </ul>
            </section>



            <?php if (is_logged_in()) { ?>


                <div class="modal fade" id="pts" tabindex="-1" role="dialog" aria-labelledby="ptl" aria-hidden="true" ng-controller="gameTrackerCtrl">
                    <div class="modal-dialog m-0 modal-dialog-centered" role="document">
                        <div class="modal-content border-0 rounded-0">
                            <div class="modal-header">
                                <h6 class="modal-title" id="ptl">PREMIUM TOURNAMENTS</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body p-0">


                                <table class="table table-dark table-bordered text-center m-0">
                                    <thead>
                                        <tr>
                                            <td width="20%" ng-repeat="hd in gameTrackerObj.headers track by $index">
                                                <strong class="d-block">{{hd.title}}</strong><small>{{hd.sub_title}}
                                                    <i class="fas fa-info-circle ml-1 text-success" ng-hide="$index == 0" data-toggle="popover" data-content="{{hd.info}}"></i>
                                                </small>
                                            </td>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr ng-repeat="gt in gameTrackerObj.grid_info track by $index">
                                            <td>
                                                <div class="h6 m-0">{{gt.title}}</div>
                                                <div class="m-0">{{gt.sub_title}}</div>
                                            </td>
                                            <td ng-repeat="item in gt.items track by $index">{{item.played}}/{{item.max}}</td>
                                        </tr>

                                    </tbody>
                                </table>

                            </div>

                        </div>
                    </div>
                </div>


            <?php } ?>