<section class="workspace" ng-controller="changePasswordCtrl">
    <div class="container-fluid py-3"> 
        <div class="row justify-content-center">
            <div class="col-md-12"> 
                <div class="card col-md-12 mx-auto">
                    <div class="card-body">

                        <h2>{{error_message}}</h2>
                        <form ng-submit="UpdatePassword()" id="updatePasswordForm" ng-keyup="cp_error = {}">
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label>Current Password <span class="text-danger">*</span></label>
                                    <input type="password"  class="form-control" placeholder=""  name="current_password" ng-model="passwordObj.current_password">
                                    <span class="text-danger">{{cp_error.current_password}}</span>
                                </div>

                                <div class="form-group col-md-12">
                                    <label>New Password <span class="text-danger">*</span></label>
                                    <input type="password" class="form-control" placeholder="" id="uppassword" name="new_password" ng-model="passwordObj.new_password">
                                    <span class="text-danger">{{cp_error.new_password}}</span>
                                </div>

                                <div class="form-group col-md-12">
                                    <label>Confirm Password <span class="text-danger">*</span></label>
                                    <input type="password" class="form-control" placeholder="" name="confirm_new_password" ng-model="passwordObj.confirm_new_password">
                                    <span class="text-danger">{{cp_error.confirm_new_password}}</span>
                                </div>
                                <div class="form-group col-md-12 text-right">
                                    <button class="btn btn-primary text-white" type="submit"><b>Update</b></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>changePasswordCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>changePasswordService.js?r=<?= time() ?>"></script>