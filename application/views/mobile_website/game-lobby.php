<div ng-controller="gameLobbyCtrl">
    <div class="workspace-offset">
        <div class="page-title">Game Lobby</div>
        <div class="bg-white">
            <div class="row no-gutters">
                <div class="col-4 p-2 border-right border-bottom">
                    <div class="row no-gutters align-items-center">
                        <div class="col-auto pr-2">
                            <svg height="30px" width="30px" viewBox="0 0 464.001 464.001">
                                <use xlink:href="#user" />
                            </svg>
                        </div>
                        <div class="col">
                            <strong class="d-block">Username</strong>
                            <strong class="m-0 text-primary">manikanta</strong>
                        </div>
                    </div>
                </div>
                <div class="col-4 p-2 border-bottom border-right">
                    <div class="row no-gutters align-items-center">
                        <div class="col-auto pr-2">
                            <svg height="30px" width="30px" viewBox="0 0 512 512">
                                <use xlink:href="#rupee-coin" />
                            </svg>
                        </div>
                        <div class="col">
                            <strong class="d-block">Cash</strong>
                            <strong class="m-0 text-primary">137877.25</strong>
                        </div>
                    </div>
                </div>
                <div class="col-4 p-2 border-right border-bottom">
                    <div class="row no-gutters align-items-center">
                        <div class="col-auto pr-2">
                            <svg height="30px" width="30px" viewBox="0 0 512 512">
                                <use xlink:href="#bonus" />
                            </svg>
                        </div>
                        <div class="col">
                            <strong class="d-block">Bonus</strong>
                            <strong class="m-0 text-primary">50</strong>
                        </div>
                    </div>
                </div>
                <div class="col-4 p-2 border-right">
                    <div class="row no-gutters align-items-center">
                        <div class="col-auto pr-2">
                            <svg height="30px" width="30px" viewBox="0 0 1000.111 1000.907">
                                <use xlink:href="#chips" />
                            </svg>
                        </div>
                        <div class="col">
                            <strong class="d-block">Fun Chips</strong>
                            <strong class="m-0 text-primary">10000</strong>
                        </div>
                    </div>
                </div>
                <div class="col-4 p-2 border-right">
                    <div class="row no-gutters align-items-center">
                        <div class="col-auto pr-2"> <img ng-src="{{chipsObj.expertise_level_image}}" height="30"></div>
                        <div class="col">
                            <strong class="d-block">Level</strong>
                            <strong class="m-0 text-primary">DIAMOND</strong>
                        </div>
                    </div>
                </div>
                <div class="col-4 p-2">
                    <div class="row no-gutters align-items-center">
                        <div class="col-auto pr-2">
                            <svg height="30px" width="30px" viewBox="0 0 512 512">
                                <use xlink:href="#rupee-coin" />
                            </svg>
                        </div>
                        <div class="col">
                            <strong class="d-block">Rps</strong>
                            <h6 class="m-0 text-primary">3805</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('mobile_website/includes/my_joined_list_game_popup') ?>
        <?php $this->load->view('mobile_website/includes/tournament_details_popup') ?>
        <ul class="nav justify-content-center  nav-one">
            <li class="nav-item" ng-click="setGameType('cash')"><a class="nav-link {{selectedGameType=='cash'?'active':''}}">Cash</a></li>
            <li class="nav-item" ng-click="setGameType('tournaments')"><a class="nav-link {{selectedGameType=='tournaments'?'active':''}}">Tournaments</a></li>
            <li class="nav-item" ng-click="setGameType('practice')"><a class="nav-link {{selectedGameType=='practice'?'active':''}}">Practice</a></li>
            <li class="nav-item" ng-click="showMyJoinedGamePopup()"><a class="nav-link">My Joined</a></li>
        </ul>
        <ul class="nav justify-content-center nav-two" ng-if="selectedGameType == 'cash' || selectedGameType == 'practice'">
            <li class="nav-item" ng-click="setSubGameType('pool')"><a class="nav-link {{selectedSubGameType=='pool'?'active':''}}">Pool Rummy</a></li>
            <li class="nav-item" ng-click="setSubGameType('points')"><a class="nav-link {{selectedSubGameType=='points'?'active':''}}">Points Rummy</a></li>
            <li class="nav-item" ng-click="setSubGameType('deals')"><a class="nav-link {{selectedSubGameType=='deals'?'active':''}}">Deal Rummy</a></li>
        </ul>
        <ul class="nav justify-content-center nav-two" ng-if="selectedGameType == 'tournaments'">
            <li class="nav-item" ng-click="setSubGameType(tc.name)" ng-repeat="tc in gameLobbyObj.tournaments track by $index"><a class="nav-link {{selectedSubGameType==tc.name?'active':''}}" href="javascript:void(0);"> {{tc.display_menu_name}}</a></li>
        </ul>
    </div>
    <section class="workspace">
        <div class="container" ng-if="selectedGameType == 'tournaments'">
            <div class="card my-3" ng-repeat="item in gameListTable track by $index">
                <div>
                    <table class="table">
                        <tr>
                            <th>Title</th>
                            <td class="text-right">{{item.tournament_title}}</td>
                        </tr>
                        <tr>
                            <th>Entry</th>
                            <td class="text-right">{{item.entry_type=='Cash' ? "Rs."+item.entry_value : item.entry_type}}</td>
                        </tr>
                        <tr>
                            <th>Prize</th>
                            <td class="text-right">{{item.expected_prize_amount}}</td>
                        </tr>
                        <tr>
                            <th>Joined</th>
                            <td class="text-right">{{item.number_of_registered_users}}/{{item.max_players}}</td>
                        </tr>
                        <tr ng-if="item.is_past == 1">
                            <th>Date</th>
                            <td class="text-right">N/a</td>
                        </tr>
                        <tr ng-if="item.is_past == 1">
                            <th>Date</th>
                            <td class="text-right">{{item.display_tournament_date_time}}</td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td class="text-right"> <span ng-if="item.tournament_status != 'Created'">{{item.tournament_status}}</span><button type="button" ng-click="showTournamentDeatilsPopup(item)" ng-if="item.tournament_status == 'Created'" class="btn rounded btn-default btn-sm">Join</button></td>
                        </tr>
                        <tr>
                            <th>Action</th>
                            <td class="text-right">
                                <button type="button" ng-if="item.tournament_status == 'Running' && item.is_joined == 1" class="btn rounded btn-info btn-sm" ng-click="openPlayNowTournamentWindow(item.cloned_tournaments_id, '<?= $player_details->access_token ?>', 'Tournament')">Take Seat</button>
                                <button type="button" ng-if="item.tournament_status == 'Completed'" ng-click="showTournamentDeatilsPopup(item)" class="btn rounded btn-default btn-sm">Completed</button>
                                <button type="button" ng-click="showTournamentDeatilsPopup(item)" ng-if="item.is_past === 0" class="btn rounded btn-success btn-sm">Details</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="container" ng-if="selectedSubGameType == 'points' && selectedGameType != 'tournaments'">
            <div class="card my-3" ng-repeat="item in gameListTable track by $index">
                <div>
                    <table class="table">
                        <tr>
                            <th>Name</th>
                            <td class="text-right">{{item.game_sub_type}} Rummy</td>
                        </tr>
                        <tr>
                            <th>Type</th>
                            <td class="text-right">{{item.number_of_deck}}</td>
                        </tr>
                        <tr>
                            <th>Max Players</th>
                            <td class="text-right">{{item.seats}}</td>
                        </tr>
                        <tr>
                            <th>Entry Fee</th>
                            <td class="text-right">{{item.point_value}}</td>
                        </tr>
                        <tr>
                            <th>Prize</th>
                            <td class="text-right">{{selectedGameType == 'cash' ? "Rs."+item.entry_fee : item.entry_fee}}</td>
                        </tr>
                        <tr>
                            <th>Active Players</th>
                            <td class="text-right">{{item.active_players|| 0}}</td>
                        </tr>
                        <tr>
                            <th>Registering</th>
                            <td class="text-right">{{item.registering|| 0}}/{{item.seats|| 0}}</td>
                        </tr>
                        <tr>
                            <th>Action</th>
                            <td class="text-right"><button ng-click="openPlayNowWindow(item.token, '<?= $player_details->access_token ?>', item.game_type)" type="button" class="btn rounded btn-success btn-sm">PLAY NOW</button></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="container" ng-if="selectedSubGameType == 'pool' && selectedGameType != 'tournaments'">
            <div class="card my-3" ng-repeat="item in gameListTable track by $index">
                <div>
                    <table class="table">
                        <tr>
                            <th>Name</th>
                            <td class="text-right">{{item.game_sub_type}} Rummy</td>
                        </tr>
                        <tr>
                            <th>Type</th>
                            <td class="text-right">{{item.number_of_deck}}</td>
                        </tr>
                        <tr>
                            <th>Max Players</th>
                            <td class="text-right">{{item.seats}}</td>
                        </tr>
                        <tr>
                            <th>Entry Fee</th>
                            <td class="text-right">{{selectedGameType == 'cash' ? "Rs."+item.entry_fee : item.entry_fee}}</td>
                        </tr>
                        <tr>
                            <th>Prize</th>
                            <td class="text-right">{{selectedGameType == 'cash' ? "Rs."+item.pool_deal_prize : item.pool_deal_prize}}</td>
                        </tr>
                        <tr>
                            <th>Active Players</th>
                            <td class="text-right">{{item.active_players|| 0}}</td>
                        </tr>
                        <tr>
                            <th>Registering</th>
                            <td class="text-right">{{item.regPlayers|| 0}}</td>
                        </tr>
                        <tr>
                            <th>Action</th>
                            <td class="text-right"><button ng-click="openPlayNowWindow(item.token, '<?= $player_details->access_token ?>', item.game_type)" type="button" class="btn rounded btn-success btn-sm">PLAY NOW</button></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="container" ng-if="selectedSubGameType == 'deals' && selectedGameType != 'tournaments'">
            <div class="card my-3" ng-repeat="item in gameListTable track by $index">
                <div>
                    <table class="table">
                        <tr>
                            <th>Name</th>
                            <td class="text-right">{{item.game_sub_type}} Rummy</td>
                        </tr>
                        <tr>
                            <th>Type</th>
                            <td class="text-right">{{item.deals}}</td>
                        </tr>
                        <tr>
                            <th>Max Players</th>
                            <td class="text-right">{{item.seats}}</td>
                        </tr>
                        <tr>
                            <th>Entry Fee</th>
                            <td class="text-right">{{selectedGameType == 'cash' ? "Rs."+item.entry_fee : item.entry_fee}}</td>
                        </tr>
                        <tr>
                            <th>Prize</th>
                            <td class="text-right">{{selectedGameType == 'cash' ? "Rs."+item.pool_deal_prize : item.pool_deal_prize}}</td>
                        </tr>
                        <tr>
                            <th>Active Players</th>
                            <td class="text-right">{{item.active_players|| 0}}</td>
                        </tr>
                        <tr>
                            <th>Registering</th>
                            <td class="text-right">{{item.registering|| 0}}/{{item.seats|| 0}}</td>
                        </tr>
                        <tr>
                            <th>Action</th>
                            <td class="text-right"><button ng-click="openPlayNowWindow(item.token, '<?= $player_details->access_token ?>', item.game_type)" type="button" class="btn rounded btn-success btn-sm">PLAY NOW</button></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>gameLobbyCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>gameLobbyService.js?r=<?= time() ?>"></script>