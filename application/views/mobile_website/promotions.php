<section class="workspace">
    <div class="promotions">
        <div class="carousel">
            <div>
                <div class="item rounded" style="background-image: url(<?= STATIC_IMAGES_PATH ?>promo.png);"></div>
            </div>
            <div>
                <div class="item rounded" style="background-image: url(<?= STATIC_IMAGES_PATH ?>promo.png);"></div>
            </div>
        </div>
    </div>
    <div class="text-center my-4 h6">TOURNAMENTS</div>
    <div class="container-fluid">
        <div class="row mb-3">
            <div class="col-4 text-center my-3"><img src="<?= STATIC_IMAGES_PATH ?>gua.png" class="mb-2 img-fluid">
                <div>THE RUMMY</div>
            </div>
            <div class="col-4 text-center my-3"><img src="<?= STATIC_IMAGES_PATH ?>gua.png" class="mb-2 img-fluid">
                <div>BONUS</div>
            </div>
            <div class="col-4 text-center my-3"><img src="<?= STATIC_IMAGES_PATH ?>gua.png" class="mb-2 img-fluid">
                <div>FREE TOURNAMENTS</div>
            </div>
            <div class="col-4 text-center my-3"><img src="<?= STATIC_IMAGES_PATH ?>gua.png" class="mb-2 img-fluid">
                <div>PREMIUM TOURNAMENTS</div>
            </div>
            <div class="col-4 text-center my-3"><img src="<?= STATIC_IMAGES_PATH ?>gua.png" class="mb-2 img-fluid">
                <div>BRING A FRIEND</div>
            </div>
            <div class="col-4 text-center my-3"><img src="<?= STATIC_IMAGES_PATH ?>gua.png" class="mb-2 img-fluid">
                <div>RUMMY LEAGUE</div>
            </div>
        </div>
    </div>
    <div class="testimonials">
        <div class="carousel">
            <?php
            $testimonials = $this->testimonials_model->get_testimonials();
            foreach ($testimonials as $item) {
                ?>
                <div class="item">
                    <div class="text-center text-white">
                        <div class="profile mx-auto mb-3" style="background-image:url(<?= $item->image ?>);"></div>
                        <p><?= $item->message ?></p>
                        <strong><?= $item->name_and_address ?></strong>
                        <div class="small mt-2"><?= $item->prize_title ?></div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="embed-responsive embed-responsive-21by9">
        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Gij_jOGHDIo"></iframe>
    </div>
</section>