<section class="workspace" ng-controller="forgotPasswordCtrl">
    <div class="container-fluid py-3"> 
        <div class="row justify-content-center">
            <div class="col-md-12"> 
                <div class="card col-md-12 mx-auto">
                    <div class="card-body">

                        <h2>{{error_message}}</h2>
                        <form id="forgotPasswordForm" ng-submit="submitRequest()" ng-keyup="fp_error = {}">
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label>Username/Email <span class="text-danger">*</span></label>
                                    <input type="password"  class="form-control" placeholder=""  name="username" ng-model="fp.username">
                                    <span class="text-danger">{{fp_error.username}}</span>
                                </div>

                                <div class="form-group col-md-12 text-right">
                                    <button class="btn btn-primary text-white" type="submit"><b>Update</b></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>forgotPasswordCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>forgotPasswordService.js?r=<?= time() ?>"></script>