<section class="workspace" ng-controller="bringAFriendCtrl">
    <div class="page-title">BRING A FRIEND</div>
    <div class="container-fluid py-3">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body text-center">
                        <div class="mb-3"><svg height="100px" viewBox="0 0 188.148 188.148">
                                <use xlink:href="#dp" /></svg></div>
                        <div class="text-dark h6">I WON RS 17,000 FOR INVITING FRIENDS</div>
                        <div class="text-primary"> </div>
                    </div>
                </div>
            </div>
            <div class="col-12 text-center">

                <div class="row align-items-center justify-content-center my-5">
                    <div class="col-auto"><svg height="30px" viewBox="0 0 512 512">
                            <use xlink:href="#enevelope"></use>
                        </svg></div>
                    <div class="col-auto"><i class="fal fa-plus fa-2x"></i></div>
                    <div class="col-auto"><svg height="30px" viewBox="0 0 512 512">
                            <use xlink:href="#group"></use>
                        </svg></div>
                    <div class="col-auto"><i class="fal fa-equals fa-2x"></i></div>
                    <div class="col-auto"><svg height="30px" viewBox="0 0 512 512">
                            <use xlink:href="#rupee-coin"></use>
                        </svg></div>
                </div>
                <div class="h6">Send Invites to your friends</div>
                <p>Earn Upto <?= BONUS_ON_REFER_FRIEND ?> in bonus for each friend that joins</p>

                <div class="card mt-5">
                    <div class="card-body">

                        <div class="row align-items-center">
                            <div class="col-12">

                                <div class="form-group"><select class="custom-select">
                                        <option>Email/SMS</option>
                                    </select></div>
                                <a href="javascript:void(0)" ng-click="openContactModal()" class="btn btn-block btn-success">Invite Friends</a>


                            </div>

                            <div class="col-12">
                                <p class="text-dark mt-3">Share Your Link</p>

                                <ul class="nav nav-socials justify-content-center">
                                    <li class="nav-item"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode($referral_link); ?>" class="nav-link rounded fb fab fa-facebook-f"></a></li>
                                    <li class="nav-item"><a target="_blank" href="https://twitter.com/intent/tweet?text=<?php echo urlencode("I love playing #Rummy. Join me @@King Of Rummy to get your Rs." . WELCOME_BONUS . " free joining bonus by clicking this link:"); ?>&url=<?php echo urlencode($referral_link); ?>" class="nav-link rounded tw fab fa-twitter"></a></li>
                                    <li class="nav-item"><a target="_blank" href="https://api.whatsapp.com/send?phone=&text=<?php echo urlencode("I love playing #Rummy. Join me @@King Of Rummy to get your Rs." . WELCOME_BONUS . " free joining bonus by clicking this link: " . $referral_link); ?>" class="nav-link rounded tw fab fa-whatsapp"></a></li>
                                    <li class="nav-item"><a style="cursor:pointer;" ng-click="copyToClipboard('<?php echo $referral_link; ?>')" class="nav-link rounded fas fa-link"></a><input type="text" style="display:none;" value="" id="referral_link"></li>
                                    <!--                                    <li class="nav-item"><a href="#" class="nav-link rounded fb fab fa-facebook-f"></a></li>
                                                                        <li class="nav-item"><a href="#" class="nav-link rounded tw fab fa-twitter"></a></li>
                                                                        <li class="nav-item"><a href="#" class="nav-link rounded gp fab fa-google-plus-g"></a></li>
                                                                        <li class="nav-item"><a href="#" class="nav-link rounded in fab fa-linkedin-in"></a></li>
                                                                        <li class="nav-item"><a href="#" class="nav-link rounded fas fa-link"></a></li>-->
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal" id="invide_contact_selection_modal" tabindex="-1" role="dialog">

                    <div class="modal-dialog modal-lg" style="max-width:100%" role="document">
                        <form ng-submit="submitInviteFriends()">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Invite Friends</h5>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col">Name</th>
                                                <th scope="col">Email</th>
                                                <th scope="col">Mobile</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="contact in contacts track by $index">
                                                <td><input type="text" ng-keyup="addNewContactRow($index)" placeholder="Name" ng-model="contact.name" class="form-control"></td>
                                                <td><input type="email" ng-keyup="addNewContactRow($index)" ng-required="{{contact.mobile == ''}}" placeholder="Email" ng-model="contact.email" class="form-control"></td>
                                                <td><input type="tel" ng-keyup="addNewContactRow($index)" ng-required="{{contact.email == ''}}" placeholder="Mobile" ng-model="contact.mobile" class="form-control"></td>
                                                <td><button type="button" ng-if="$index > 0" ng-click="removeThisContact($index)" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>bringAFriendCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>bringAFriendService.js?r=<?= time() ?>"></script>