<section class="workspace" ng-controller="preferencesCtrl">
    <div class="page-title">preferences</div>
    <div class="container-fluid">
        <form id="preferencesForm" ng-submit="updatePreferences()">
            <div class="bordered-frame">
                <div class="bf-label"><span>Contact</span></div>
                <div class="bf-body">
                    <div class="row my-3">
                        <div class="col"><strong>SMS</strong></div>
                        <div class="col-auto">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="1" name="contact_by_sms" ng-model="preferencesObj.contact_by_sms" value="1" class="custom-control-input">
                                <label class="custom-control-label" for="1">Yes</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="2" name="contact_by_sms" ng-model="preferencesObj.contact_by_sms" value="0" class="custom-control-input">
                                <label class="custom-control-label" for="2">No</label>
                            </div>
                        </div>
                    </div>

                    <div class="row my-3">
                        <div class="col"><strong>Phone</strong></div>
                        <div class="col-auto">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="3" name="contact_by_phone" ng-model="preferencesObj.contact_by_phone" value="1" class="custom-control-input">
                                <label class="custom-control-label" for="3">Yes</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="4" name="contact_by_phone" ng-model="preferencesObj.contact_by_phone" value="0" class="custom-control-input">
                                <label class="custom-control-label" for="4">No</label>
                            </div>
                        </div>
                    </div>

                    <div class="row my-3">
                        <div class="col"><strong>Mail</strong></div>
                        <div class="col-auto">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="5" name="contact_by_email" ng-model="preferencesObj.contact_by_email" value="1" class="custom-control-input">
                                <label class="custom-control-label" for="5">Yes</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="6" name="contact_by_email" ng-model="preferencesObj.contact_by_email" value="0" class="custom-control-input">
                                <label class="custom-control-label" for="6">No</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="bordered-frame">
                <div class="bf-label"><span>Language</span></div>
                <div class="bf-body">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="7" name="speak_language" ng-model="preferencesObj.speak_language" value="English" class="custom-control-input">
                        <label class="custom-control-label" for="7">English</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="8" name="speak_language" ng-model="preferencesObj.speak_language" value="Telugu" class="custom-control-input">
                        <label class="custom-control-label" for="8">Telugu</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="9" name="speak_language" ng-model="preferencesObj.speak_language" value="Hindi" class="custom-control-input">
                        <label class="custom-control-label" for="9">Hindi</label>
                    </div>
                </div>
            </div>

            <div class="bordered-frame">
                <div class="bf-label"><span>Subscriptions</span></div>
                <div class="bf-body">
                    <div class="custom-control custom-radio custom-control">
                        <input type="checkbox" id="10" class="custom-control-input" name="sms_subscriptions" ng-model="preferencesObj.sms_subscriptions" ng-true-value="'1'" ng-false-value="'0'">
                        <label class="custom-control-label" for="10">I want to receive Promotional SMS.</label>
                    </div>
                    <br />
                    <div class="custom-control custom-radio custom-control">
                        <input type="checkbox" id="11" name="5" class="custom-control-input" name="newsletter_subscriptions" ng-model="preferencesObj.newsletter_subscriptions" ng-true-value="'1'" ng-false-value="'0'">
                        <label class="custom-control-label" for="11">I want to receive Promotional Emails.</label>
                    </div>
                </div>
            </div>
            <button type="button" class="btn btn-primary btn-block" ng-click="updatePreferences()">Save</button>
        </form>
    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>preferencesCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>preferencesService.js?r=<?= time() ?>"></script>