<section class="workspace">
    <div class="page-title">Account Details</div>
    <div class="container-fluid py-3" ng-controller="profileCtrl">
        <div class="row no-gutters">
            <div class="col-sm-12 text-center mb-3">
                <h6 class="mb-3 pt-3 pt-md-0">MY AVATAR</h6>
                <div class="row no-gutters mb-3">
                    <div class="col-12">
                        <div class="profile mx-auto" style="background-image:url({{profileObj.profile_pic}})"></div>
                    </div>
                </div>
                <button class="btn btn-primary btn-block" ng-click="showChooseAvatarPopup()">Change Avtar</button>
            </div>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card-bordered mb-3">
                            <div class="row align-items-center no-gutters">
                                <div class="col-auto pr-1">User Name</div>
                                <div class="col"><span class="text-primary"><?= $player_details->username ?></span></div>
                                <div class="col-auto">
                                    <a href="<?= base_url() ?>change_password"><button class="btn btn-sm bg-transparent text-dark">Change Password <i class="fal fa-pen ml-1"></i></button></a>
                                </div>
                            </div>
                        </div>
                        <div class="title-bordered mb-3">
                            <div class="row align-items-center">
                                <div class="col-auto">
                                    <strong>Personal Information</strong>
                                </div>
                                <div class="col">
                                    <div class="border-bottom"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row align-items-center mb-3">
                            <div class="col-4 font-weight-bold">Name:</div>
                            <div class="col">{{profileObj.full_name}}</div>
                        </div>
                        <div class="row align-items-center mb-3">
                            <div class="col-4 font-weight-bold">DOB:</div>
                            <div class="col">{{profileObj.display_date_of_birth}}</div>
                        </div>
                        <div class="row align-items-center mb-3">
                            <div class="col-4 font-weight-bold">Address:</div>
                            <div class="col">{{profileObj.address_line_1 + " " + profileObj.address_line_2}}</div>
                        </div>
                        <div class="row align-items-center mb-3">
                            <div class="col-4 font-weight-bold">City:</div>
                            <div class="col">{{profileObj.city}}</div>
                        </div>
                        <div class="row align-items-center mb-3">
                            <div class="col-4 font-weight-bold">State:</div>
                            <div class="col">{{profileObj.state_name}}</div>
                        </div>
                        <div class="row align-items-center mb-3">
                            <div class="col-4 font-weight-bold">Pin Code:</div>
                            <div class="col">{{profileObj.pin_code}}</div>
                        </div>
                        <div class="row align-items-right mb-3">
                            <div class="col">
                                <span class="float-right"><a href="javascript:void(0);" ng-click="showEditProfilePopup()"><span class="btn btn-sm fal fa-pen"></span>Edit Profile</a></span>
                            </div>
                        </div>
                    </div>

                    <?php $this->load->view("mobile_website/includes/edit_profile_popup") ?>
                    <?php $this->load->view("mobile_website/includes/edit_email_popup") ?>
                    <?php $this->load->view("mobile_website/includes/edit_mobile_popup") ?>
                    <?php $this->load->view("mobile_website/includes/kyc_popup") ?>
                    <?php $this->load->view("mobile_website/includes/choose_avatar_popup") ?>
                    <div class="col-12">
                        <div class="card-bordered mb-3">
                            <div class="row align-items-center no-gutters">
                                <div class="col-auto pr-3">
                                    <svg height="40px" width="40px">
                                        <use xlink:href="#enevelope" />
                                    </svg>
                                </div>
                                <div class="col">
                                    <div>
                                        <strong class="text-dark-50">Email Id:</strong> <i ng-if="profileObj.email_verified == 0" class="fal fa-exclamation-triangle text-primary"></i><i ng-if="profileObj.email_verified == 1" class="fas fa-check-circle text-success"></i>
                                        <div>{{profileObj.email}}</div>
                                    </div>
                                </div>
                                <div class="col-auto text-right">
                                    <div class="small text-dark-50" ng-if="profileObj.email_verified == 0">Not Verified</div>
                                    <div class="small text-dark-50" ng-if="profileObj.email_verified == 1">Verified</div>
                                    <button class="btn btn-sm bg-transparent fal fa-pen text-dark" ng-click="showEditEmailPopup()"></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="card-bordered mb-3">
                            <div class="row align-items-center no-gutters">
                                <div class="col-auto pr-3">
                                    <svg height="40px" width="40px">
                                        <use xlink:href="#smartphone" />
                                    </svg>
                                </div>
                                <div class="col">
                                    <div>
                                        <strong class="text-dark-50">Mobile:</strong> <i ng-if="profileObj.mobile_verified == 0" class="fal fa-exclamation-triangle text-primary"></i><i ng-if="profileObj.mobile_verified == 1" class="fas fa-check-circle text-success"></i>
                                        <div>{{profileObj.display_mobile}}</div>
                                    </div>
                                </div>
                                <div class="col-auto text-right">
                                    <div class="small text-dark-50" ng-if="profileObj.mobile_verified == 0">Not Verified</div>
                                    <div class="small text-dark-50" ng-if="profileObj.mobile_verified == 1">Verified</div>
                                    <button class="btn btn-sm bg-transparent fal fa-pen text-dark" ng-click="showEditMobilePopup()"></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="title-bordered mb-3">
                            <div class="row align-items-center">
                                <div class="col-auto">
                                    <strong>KYC Verification</strong>
                                </div>
                                <div class="col">
                                    <div class="border-bottom"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row align-items-center no-gutters mb-3">
                            <div class="col-auto pr-1"><i class="fas fa-home text-info mr-2"></i></div>
                            <div class="col font-weight-bold">Address Proof:</div>
                            <div class="col-auto">
                                <button class="btn btn-sm bg-transparent text-dark">{{profileObj.address_proof_type}}
                                    <i class="fas fa-check-circle text-success" ng-if="profileObj.address_proof_status == 'Approved'"></i>
                                    <i class="fal fa-exclamation-triangle text-primary" ng-if="profileObj.address_proof_status != 'Approved'"></i>
                                </button>
                            </div>
                        </div>
                        <div class="row align-items-center no-gutters mb-3">
                            <div class="col-auto pr-1"><i class="fas fa-user text-info mr-2"></i></div>
                            <div class="col font-weight-bold">PAN Card:</div>
                            <div class="col-auto"><button class="btn btn-sm bg-transparent text-dark">{{profileObj.pan_card_number}}
                                    <i class="fas fa-check-circle text-success" ng-if="profileObj.address_proof_status == 'Approved'"></i>
                                    <i class="fal fa-exclamation-triangle text-primary" ng-if="profileObj.address_proof_status != 'Approved'"></i>
                                </button>
                            </div>
                        </div>

                        <div class="card bg-mattegreen" ng-if="profileObj.kyc_status != 'Not Verified'">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col">
                                        <h6>Get KYC Verified & Experience</h6>
                                        <p>Higher add Cash limits
                                            Smoother withdrawal
                                        </p>
                                        <button class="btn btn-success" ng-click="showKycPopup()"><small>UPLOAD NOW</small></button>
                                    </div>
                                    <div class="col-auto">
                                        <svg height="60px" width="60px">
                                            <use xlink:href="#error" />
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>