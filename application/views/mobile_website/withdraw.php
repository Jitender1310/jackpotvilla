<section class="workspace" ng-controller="withdrawCtrl">
    <div class="page-title">Withdraw Cash</div>
    <div class="container-fluid py-3">


        <div class="row align-items-center mb-4">
            <div class="col">
                <h5>Available Balance</h5>
                <p class="text-danger m-0">*Minimum amount to withdraw: Rs <?= MINIMUM_AMOUNT_FOR_WITHDRAW_MONEY ?></p>
            </div>
            <div class="col-auto">
                <button class="btn btn-warning font-weight-bold">Rs {{chipsObj.wallet_account.real_chips_withdrawal}}</button>
            </div>
        </div>

        <a class="btn btn-primary btn-block" href="<?= base_url() ?>withdraw_status">WITHDRAW STATUS</a>

        <div class="bordered-frame">
            <div class="bf-label"><span>SUBMIT WITHDRAW REQUEST</span></div>
            <div class="bf-body">
                <form id="withdrawl_form" ng-submit="RequestWithdrawl();" ng-keyup="ep_error = {}">
                    <div class="form-group">
                        <label>Enter Amount to Withdraw</label>
                        <input type="text" name="request_amount" min="<?= MINIMUM_AMOUNT_FOR_WITHDRAW_MONEY ?>" max="<?= MAXIMUM_AMOUNT_FOR_WITHDRAW_MONEY ?>" ng-model="withdrawObj.request_amount" class="form-control number">
                        <span class="text-danger">{{ep_error.request_amount}}</span>
                    </div>
                    <div class="form-group">
                        <label>Enter account number</label>
                        <input type="text" ng-model="withdrawObj.account_number" id="account_number" name="account_number" class="form-control number">
                        <span class="text-danger">{{ep_error.account_number}}</span>
                    </div>
                    <div class="form-group">
                        <label>Re-enter account number </label>
                        <input type="text" ng-model="withdrawObj.confirm_account_number" name="confirm_account_number" class="form-control number">

                    </div>
                    <div class="form-group">
                        <label>IFSC Code </label>
                        <input type="text" ng-model="withdrawObj.ifsc_code" name="ifsc_code" class="form-control">
                        <span class="text-danger">{{ep_error.ifsc_code}}</span>
                    </div>
                    <div class="form-group">
                        <label>Bank name</label>
                        <input type="text" ng-model="withdrawObj.bank_name" name="bank_name" class="form-control">
                        <span class="text-danger">{{ep_error.bank_name}}</span>
                    </div>
                    <div class="form-group">
                        <label>Branch name</label>
                        <input type="text" ng-model="withdrawObj.branch_name" name="branch_name" class="form-control">
                        <span class="text-danger">{{ep_error.branch_name}}</span>
                    </div>
                    <div class="col-auto" align="center">
                        <button type="submit" class="btn btn-warning font-weight-bold">Send Request</button>
                    </div>
                </form>
            </div>
        </div>

        <p>To get information on processing fees and withdrawing cash from your account, <a href="#">click here</a></p>
    </div>





</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>withdrawCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>withdrawService.js?r=<?= time() ?>"></script>