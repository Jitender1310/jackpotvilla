<section class="workspace">
    <section class="slider">
        <div class="carousel">
            <?php
            $sliders = $this->cms_model->get_sliders();
            foreach ($sliders as $item) {
                if ($item->display_in != "Desktop Website") {
                    continue;
                }
                ?>
                <div class="item">
                    <div class="slider_block" style="background-image: url(<?= $item->image ?>);"></div>
                </div>
            <?php } ?>
        </div>
    </section>
    <section class="services d-none">
        <div class="carousel">
            <div class="item">
                <div class="services_block ">
                    <div><svg height="30px" viewBox="0 0 50.188 52.689">
                            <use xlink:href="#default" /></svg>
                        <div class="h6">Lorem Ipsum is simply Ipsum</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="services_block ">
                    <div><svg height="30px" viewBox="0 0 50.188 52.689">
                            <use xlink:href="#default" /></svg>
                        <div class="h6">Lorem Ipsum is simply Ipsum</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="services_block">
                    <div><svg height="30px" viewBox="0 0 50.188 52.689">
                            <use xlink:href="#default" /></svg>
                        <div class="h6">Lorem Ipsum is simply Ipsum</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="services_block ">
                    <div><svg height="30px" viewBox="0 0 50.188 52.689">
                            <use xlink:href="#default" /></svg>
                        <div class="h6">Lorem Ipsum is simply Ipsum</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="services_block ">
                    <div><svg height="30px" viewBox="0 0 50.188 52.689">
                            <use xlink:href="#default" /></svg>
                        <div class="h6">Lorem Ipsum is simply Ipsum</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="about">
        <div class="container-fluid">
            <div class="row">
                <!--   <div class="col-12 mb-3"><img src="<?= STATIC_MOBILE_IMAGES_PATH ?>pic01.jpg" class="img-fluid" ></div> -->
                <div class="col-12">
                    <div class="h6"> Lorem Ipsum is simply dummy text of the printing</div>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled galley of type and scrambled it to make a type specimen book.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="points">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col">
                    <img src="<?= STATIC_IMAGES_PATH ?>mrs1.png" class="img-fluid">
                </div>
                <div class="col">
                    <img src="<?= STATIC_IMAGES_PATH ?>mrs2.png" class="img-fluid">
                </div>
                <div class="col">
                    <img src="<?= STATIC_IMAGES_PATH ?>mrs3.png" class="img-fluid">
                </div>
            </div>
        </div>
    </section>
    <section class="services">
        <div class="carousel">
            <div class="item">
                <div class="services_block ">
                    <div> <svg height="40px" viewBox="0 0 498.4 498.4">
                            <use xlink:href="#legal"></use>
                        </svg>
                        <div class="h6">100% Legal</div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="services_block ">
                    <div><svg height="40px" viewBox="0 0 130 138">
                            <use xlink:href="#shield"></use>
                        </svg>
                        <div class="h6">Responsible Play</div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="services_block">
                    <div><svg height="40px" viewBox="0 0 501.551 501.551">
                            <use xlink:href="#support"></use>
                        </svg>
                        <div class="h6">Support</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="screens">
        <div class="h6">TAKE A LOOK AT OUR SCREENSHOTS</div>
        <div class="carousel">
            <div class="item">
                <div class="screen_block" style="background-image: url(<?= STATIC_IMAGES_PATH ?>screen1.png);"></div>
            </div>
            <div class="item">
                <div class="screen_block" style="background-image: url(<?= STATIC_IMAGES_PATH ?>screen.png);"></div>
            </div>
            <div class="item">
                <div class="screen_block" style="background-image: url(<?= STATIC_IMAGES_PATH ?>screen1.png);"></div>
            </div>
        </div>
    </section>
    <div class="testimonials">
        <div class="carousel">
            <?php
            $testimonials = $this->testimonials_model->get_testimonials();
            foreach ($testimonials as $item) {
                ?>
                <div class="item">
                    <div class="text-center text-white">
                        <div class="profile mx-auto mb-3" style="background-image:url(<?= $item->image ?>);"></div>
                        <p><?= $item->message ?></p>
                        <strong><?= $item->name_and_address ?></strong>
                        <div class="small mt-2"><?= $item->prize_title ?></div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <section class="downloads">
        <div class="container-fluid">
            <div class="h6">Download App Now</div>
            <div class="row justify-content-center no-gutters">
                <div class="col-4 pr-1" title="Download <?= SITE_TITLE ?> Android App">
                    <a href="<?= DOWNLOAD_ANDROID_APP_LINK ?>"><img src="<?= STATIC_IMAGES_PATH ?>android.png" class="img-fluid"></a>
                </div>
                <div class="col-4" title="Download <?= SITE_TITLE ?> iOS App">
                    <a href="<?= DOWNLOAD_IOS_APP_LINK ?>"><img src="<?= STATIC_IMAGES_PATH ?>apple.png" class="img-fluid"></a>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <ul class="nav justify-content-center">
            <li class="nav-item">
                <a href="<?= FACEBOOK_PAGE_LINK ?>" target="_blank" class="nav-link p-0 pr-2"><img height="40" src="<?= STATIC_IMAGES_PATH ?>fb.png"></a>
            </li>
            <li class="nav-item">
                <a href="<?= TWITTER_PAGE_LINK ?>" target="_blank" class="nav-link p-0 pr-2"><img height="40" src="<?= STATIC_IMAGES_PATH ?>tw.png"></a>
            </li>
            <li class="nav-item">
                <a href="<?= GOOGLE_PLUS_PAGE_LINK ?>" target="_blank" class="nav-link p-0 pr-2"><img height="40" src="<?= STATIC_IMAGES_PATH ?>gp.png"></a>
            </li>
            <li class="nav-item">
                <a href="<?= INSTAGRAM_PAGE_LINK ?>" target="_blank" class="nav-link p-0 pr-2"><img height="40" src="<?= STATIC_IMAGES_PATH ?>in.png"></a>
            </li>
        </ul>
        <div class="text-center mt-2">All rights reserved 2019<?= date("Y") > 2019 ? "-" . date("Y") : "" ?></div>
    </footer>
</section>