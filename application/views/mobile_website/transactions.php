<div class="workspace" ng-controller="transactionsCtrl" ng-init="filterObj.page = <?php echo $current_page; ?>;
                getTxnHistory()">
    <div class="page-title">Transactions</div>
    <div class="container-fluid2">

        <div class="table-responsive">
            <table class="table table-striped table-hover table-custom text-nowrap mb-0">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Type</th>
                        <th>Ref. ID</th>
                        <th>On</th>
                        <th>Amount</th>
                        <th class="text-right">Balance</th>
                    </tr>
                </thead>
                <tr ng-repeat="transaction in transactions">
                    <td>{{transactionHistoryPagination.initial_id + $index}}</td>
                    <td><span class="{{transaction.transaction_bootstrap_class}}">{{transaction.transaction_type}}</span> / {{transaction.type}}</td>
                    <td>{{transaction.ref_id}}</td>
                    <td>{{transaction.created_date_time}}</td>
                    <td>{{transaction.amount}}</td>
                    <td class="text-right">{{transaction.closing_balance}}</td>
                </tr>
            </table>
        </div>
    </div>
</div>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>transactionsCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>transactionsService.js?r=<?= time() ?>"></script>