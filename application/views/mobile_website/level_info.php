<section class="workspace" ng-controller="levelInfoCtrl">
    <div class="page-title">
        CURRENT LEVEL INFO
    </div>
    <div class="container-fluid py-3">
        <div class="row align-items-center">
            <div class="col-auto">
                <img ng-src="{{expertizeLevelObj.level_info.current_level_image}}" height="50">
                <strong class="d-block text-center mt-2">{{expertizeLevelObj.level_info.current_level_name}}</strong>
            </div>
            <div class="col">

                <div>
                    <b>{{expertizeLevelObj.level_info.current_points}}</b>
                    <b class="float-right">{{expertizeLevelObj.level_info.max_points}}</b>
                </div>


                <div class="progress bg-white">
                    <div class="progress-bar" role="progressbar" style="width: {{ expertizeLevelObj.level_info.current_points * 100 / expertizeLevelObj.level_info.max_points}}%;"></div>
                </div>

                <strong class="d-block text-center mt-2">You need {{expertizeLevelObj.level_info.max_points - expertizeLevelObj.level_info.current_points}} for the next level</strong>

            </div>
            <div class="col-auto"><img ng-src="{{expertizeLevelObj.level_info.next_level_image}}" height="50" class="" ng-class="{'level-inactive':expertizeLevelObj.level_info.current_points != expertizeLevelObj.level_info.max_points }">
                <strong class="d-block text-center mt-2">{{expertizeLevelObj.level_info.next_level_name}}</strong></div>
        </div>

        <hr>

        <p>* Reward Points are calculated at the end of each game</p>
        <p>* Reward Level of the player may get downgraded based on player Inactivity on the website</p>



    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>levelInfoCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>levelInfoService.js?r=<?= time() ?>"></script>