
<?php if (is_logged_in()) { ?>
    <?php $this->load->view('includes/menu-dashboard') ?>
<?php } ?>

<?php if (!is_logged_in()) { ?>
    <?php $this->load->view('menu') ?>
<?php } ?>

<section class="innerpages py-5">
    <div class="container" style="background: #fff;

padding: 1px 0 20px 0;">
        <div class="row mt-4">
            <?php //  $this->load->view('includes/dashboard-sidebar') ?>
            <div class="col-lg-9 col-xs-12 my-4 p-0 pl-2">
                <div class="card h-100 card-gold" style="border: 1px solid #ced4da;">
                    <div class="card-header" style="background: #2f0060; color: #fff;">CONTACT US</div>
                    <div class="card-body bonus-table bonus-table-white">
                        <p>We encouraged our players to interact with us by way of comments, reviews, feedback, questions, concerns or complaints of any issues they may face on A2Z Betting. Every interaction is examined and due responses are provided by our team. We assure you that our team is committed to providing you with exceptional service and we will get back to you as soon as possible.</p>
                        <form ng-submit='submitContact()' id="contact_us_form" ng-controller="contactUsCtrl" ng-keyup="cp_error = {}">
                            <input type="hidden" id="g-recaptcha-response" name="captcha" class="form-control bg-light shadow-sm ng-valid ng-not-empty ng-dirty ng-valid-parse ng-touched" ng-model="contactObj.captcha" autocomplete="off" />
                            <div class='row'>
                                <div class="col-md-12">
                                    <div class='form-group'>
                                        <label>Email Address <small>*</small></label>
                                        <input type="email" name="email" class="form-control" ng-model="contactObj.email" required>
                                    </div>
                                   
                                </div>
                                <div class="col-md-12">
                                    <div class='form-group'>
                                        <label>Username</label>
                                        <input type="text" name="username" class="form-control" ng-model="contactObj.username">
                                    </div>
                                    
                                </div>
                                <a href="contact.php"></a>
                                <div class="col-md-12">
                                 
                                        <div class='form-group'>
                                            <label>Query <small>*</small></label>
                                            <select class="form-control" name="query" ng-model="contactObj.query" required>
                                                <option value="">Select the category</option>
                                                <option>Registering/Creating an Account</option>
                                                <option>Unable to login</option>
                                            </select>
                                        </div>
                                        
                                   
                                    </div>
                                <div class="col-md-12">
                                    <div class='form-group'>
                                        <label>Message <small>*</small></label>
                                        <textarea name="message" style="height: 120px;" required class="form-control" ng-model="contactObj.message"></textarea>
                                    </div>
                                    
                                </div>
                                <div class=" col-md-12">
                                         <div class="form-group">
                                                <?php /* <label>Captcha</label> */ ?>
                                                <div class="row align-items-center no-gutters">
                                                    <?php /*<div class="col-md-auto " style="padding-left: 0;">
                                                        <img style="height:38px; width: 130px; padding-left: 0;" ng-src="https://dev.misterrummy.com/api/contact_us/captcha?rand=8653125224811002" class="border" src="https://dev.misterrummy.com/api/contact_us/captcha?rand=8653125224811002">

                                                    </div>
                                                    <div class="col-md-auto pr-2"><button ng-click="get_captcha()" type="button" class="btn btn-primary py-2 fal fa-sync"></button></div>
                                                    <div class="col-md-4">
                                                        <input type="text" name="captcha" class="form-control bg-light shadow-sm ng-valid ng-not-empty ng-dirty ng-valid-parse ng-touched" ng-model="contactObj.captcha" autocomplete="off" style="width: 131px;">
                                                    </div>*/ ?>
                                                     <button type="submit" class="btn btn-primary" style="float: left;

width: auto; padding: 8px 16px">Submit</button>
                                                    <span class="ng-binding"></span>
                                                </div>
                                   </div>
                                    </div>
                                
                                <div class="col-md-12 text-center">
                                   
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class=" col-md-3 col-xs-12 my-2 p-0 pl-2">
             <div class=" text-center ">
                 <div class="col " style="border: 1px solid #ced4da;
padding: 5px 0;">
                                 <img src="<?= STATIC_IMAGES_PATH ?>at-the-rate.png" class="img-fluid">
                                <h6 class="mt-2">Mail us at</h6>
                                <p style=" text-align: center;"><?= CONTACT_EMAIL ?></p>
                            </div>
                 <div class="col " style="border: 1px solid #ced4da;
padding: 5px 0; margin: 40px 0;">
                   <img src="<?= STATIC_IMAGES_PATH ?>phone-call.png" class="img-fluid">
                                <h6 class="mt-2">Call</h6>
                                <p style=" text-align: center;"><?= WEBSITE_CONTACT_NUMBER ?></p>
                            </div>
                 <div class="col" style="border: 1px solid #ced4da;
padding: 5px 8px;">
                                   <img src="<?= STATIC_IMAGES_PATH ?>email.png" class="img-fluid">
                                <h6 class="mt-2">Address</h6>
                                <p style=" text-align: center;"><?= WEBSITE_CONTACT_ADDRESS ?></p>
                            </div>
                        </div>   
                
                
                
            </div>
            
        </div>
    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>contactUsCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>contactUsService.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>registerCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>registerService.js?r=<?= time() ?>"></script>
<!-- <script src="https://www.google.com/recaptcha/api.js?render=6LffjKoUAAAAALS0ZXNe1BxJg0_6LNor_KdZizsW"></script> -->
<script>
    grecaptcha.ready(function() {
        grecaptcha.execute('6LffjKoUAAAAALS0ZXNe1BxJg0_6LNor_KdZizsW', {action: 'homepage'}).then(function(token) {
             document.getElementById('g-recaptcha-response').value=token;
        });
    });
</script>