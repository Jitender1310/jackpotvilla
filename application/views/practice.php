
<?php include'menu-dashboard.php'; ?>
<section class="innerpages">
    <div class="container">

        <div class="inner-frame"> <?php include'chips.php'; ?>

            <div class="btn-group-special row justify-content-center mt-4 pb-5">
                <div class="col-3">
                    <a href="#" class="btn btn-block btn-light">CASH</a>
                </div>
                <div class="col-3">
                    <a href="#" class="btn btn-block btn-light">TOURNAMENTS</a>
                </div>
                <div class="col-3">
                    <a href="#" class="btn btn-block btn-light btn-active">PRACTICE</a>
                </div>
            </div>

            <div class="card card-table py-4 border-0">


                <div class="card-body">
                    <div class="card card-table-menu shadow">
                        <div class="card-body py-2">
                            <div class="row no-gutters">
                                <div class="col-4 pr-2"><a class="special-tag has-info active" href=""><span><small>13</small> <strong>Card</strong></span>Points Rummy</a></div>
                                <div class="col-4 pr-2"><a class="special-tag has-info" href=""><span><small>13</small> <strong>Card</strong></span>Pool Rummy</a></div>
                                <div class="col-4"><a class="special-tag has-info" href=""><span><small>13</small> <strong>Card</strong></span>Deal Rummy</a></div>
                            </div>
                        </div>
                    </div>
                </div>




                <table class="table table-custom table-striped table-hover bg-white">
                    <thead class="bg-primary-dark text-white">   <tr>
                            <th class="border-top-0">Name</th>
                            <th class="border-top-0">Decks</th>
                            <th class="border-top-0">Max Players</th>
                            <th class="border-top-0">Point Value</th>
                            <th class="border-top-0">Min- Entry</th>
                            <th class="border-top-0">Registering</th>
                            <th class="border-top-0 text-right">Action</th>
                        </tr></thead>

                    <tr>
                        <td>Points Rummy - 6 Players</td>
                        <td>2</td>
                        <td>6</td>
                        <td>₹0.05</td>
                        <td>₹40</td>
                        <td>5/6</td>
                        <td class="text-right"><a href="#" class="btn rounded btn-success btn-sm">PLAY NOW</a></td>
                    </tr>
                    <tr>
                        <td>Points Rummy - 6 Players</td>
                        <td>2</td>
                        <td>6</td>
                        <td>₹0.05</td>
                        <td>₹40</td>
                        <td>5/6</td>
                        <td class="text-right"><a href="#" class="btn rounded btn-success btn-sm">PLAY NOW</a></td>
                    </tr>
                    <tr>
                        <td>Points Rummy - 6 Players</td>
                        <td>2</td>
                        <td>6</td>
                        <td>₹0.05</td>
                        <td>₹40</td>
                        <td>5/6</td>
                        <td class="text-right"><a href="#" class="btn rounded btn-success btn-sm">PLAY NOW</a></td>
                    </tr>
                    <tr>
                        <td>Points Rummy - 6 Players</td>
                        <td>2</td>
                        <td>6</td>
                        <td>₹0.05</td>
                        <td>₹40</td>
                        <td>5/6</td>
                        <td class="text-right"><a href="#" class="btn rounded btn-success btn-sm">PLAY NOW</a></td>
                    </tr>
                    <tr>
                        <td>Points Rummy - 6 Players</td>
                        <td>2</td>
                        <td>6</td>
                        <td>₹0.05</td>
                        <td>₹40</td>
                        <td>5/6</td>
                        <td class="text-right"><a href="#" class="btn rounded btn-success btn-sm">PLAY NOW</a></td>
                    </tr>
                    <tr>
                        <td>Points Rummy - 6 Players</td>
                        <td>2</td>
                        <td>6</td>
                        <td>₹0.05</td>
                        <td>₹40</td>
                        <td>5/6</td>
                        <td class="text-right"><a href="#" class="btn rounded btn-success btn-sm">PLAY NOW</a></td>
                    </tr>
                    <tr>
                        <td>Points Rummy - 6 Players</td>
                        <td>2</td>
                        <td>6</td>
                        <td>₹0.05</td>
                        <td>₹40</td>
                        <td>5/6</td>
                        <td class="text-right"><a href="#" class="btn rounded btn-success btn-sm">PLAY NOW</a></td>
                    </tr>
                    <tr>
                        <td>Points Rummy - 6 Players</td>
                        <td>2</td>
                        <td>6</td>
                        <td>₹0.05</td>
                        <td>₹40</td>
                        <td>5/6</td>
                        <td class="text-right"><a href="#" class="btn rounded btn-success btn-sm">PLAY NOW</a></td>
                    </tr>
                    <tr>
                        <td>Points Rummy - 6 Players</td>
                        <td>2</td>
                        <td>6</td>
                        <td>₹0.05</td>
                        <td>₹40</td>
                        <td>5/6</td>
                        <td class="text-right"><a href="#" class="btn rounded btn-success btn-sm">PLAY NOW</a></td>
                    </tr>
                    <tr>
                        <td>Points Rummy - 6 Players</td>
                        <td>2</td>
                        <td>6</td>
                        <td>₹0.05</td>
                        <td>₹40</td>
                        <td>5/6</td>
                        <td class="text-right"><a href="#" class="btn rounded btn-success btn-sm">PLAY NOW</a></td>
                    </tr>
                </table>
            </div>






        </div>
    </div>
</section>