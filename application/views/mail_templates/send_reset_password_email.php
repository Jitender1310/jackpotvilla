<h1>Reset Password</h1>

Hi <?=$username?>, 
<br/>
You recently requested to reset your password for your <?=SITE_TITLE?> account. Click the link below to reset it.
<br/>
<br/>
<a href="<?= $reset_password_link ?>">Reset your password</a>
<br/>

If you did not request a password reset, please ignore this email.
<br/>
<br/>
Cheers,<br/>
The <?=SITE_TITLE?>