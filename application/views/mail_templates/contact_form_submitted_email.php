<style type="text/css"> *{margin: 0px;} </style>
<div class="container">
    <table  cellpadding="0" cellspacing="0" border="0" style="background:#fff;  width: 100%; padding: 20px; font-family:sans-serif; font-size: 13px; color: #676767;">
        <tr>
            <td valign="top">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered" style="background: #fff; border: 1px solid #e5e5e5; border-radius: 10px; margin: 0 auto; padding: 30px;" >

                    <tr class="hide">
                        <td>
                            <div style="height: 1px; background: #e5e5e5; margin: 15px 0px; overflow: hidden;"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h3 style="margin: 0px; margin-bottom: 12px; ">Dear <span style="color: #0496d0">Sir/Madam</span>,</h3>
                            <h4 style="margin:0px; margin-bottom: 12px; font-weight: normal; line-height: 25px;">One of the users just submitted the contact form.</h4>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h4 style="text-align:center">Provided Details</h4>
                            <table style="width:100%;" border="1" cellpadding="10">
                                <tbody>
                                    <tr><th style="width:30%">Username</th> <td><?= $username ?></td></tr>
                                    <tr><th>Email</th> <td><?= $email ?></td></tr>
                                    <tr><th>Query</th> <td><?= $query ?></td></tr>
                                    <tr><th>Message</th> <td><?= $message ?></td></tr>
                                    <tr><th>Submitted On:</th> <td><?= date('d-m-Y', time()); ?></td></tr>
                                </tbody>
                            </table>
                            <p colspan="5" style="text-align: center"><small>*Terms and conditions will apply</small></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div style="height: 1px; background: #e5e5e5; margin: 15px 0px; overflow: hidden;"></div>
                        </td>
                    </tr>
                    <tr >
                        <td>
                            <p style="margin-bottom: 10px;"> You can also contact our helpdesk at for immediate assistance: <strong><?php echo HELPDESK_NUMBER; ?></strong></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>