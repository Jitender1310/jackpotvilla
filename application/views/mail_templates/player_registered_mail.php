<?php error_reporting(0)?>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
    </head>
    <body>
        <table width="100%" cellpadding="0" cellspacing="0" style="font-family: sans-serif; font-size: 12px; line-height: 20px; color: #676767;">
            <tr>
                <td style="background: #eee; padding:50px 20px;">
                    <table style="width:600px; margin: 0px auto;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="background: #1e2931; color: white; padding: 10px 20px; text-align:center;">

                                <strong style="display: inline-block; height: 30px; line-height:30px; vertical-align: top; padding-right: 10px;">Download App Now </strong>

                                <a href="<?=DOWNLOAD_ANDROID_APP_LINK?>"><div style="display: inline-block; vertical-align: top; height: 30px; width: 30px;background-image: url(<?= STATIC_IMAGES_PATH ?>android_icon.png); background-position: center; background-repeat: no-repeat; background-size: 20px; background-color:transparent;">
                                </div></a>
                                <a href="<?=DOWNLOAD_IOS_APP_LINK?>"><div style="display: inline-block; vertical-align: top; height: 30px; width: 30px;background-image: url(<?= STATIC_IMAGES_PATH ?>apple_icon.png); background-position: center; background-repeat: no-repeat; background-size: 20px; background-color:transparent;">
                                    </div></a>
                            </td>
                        </tr> <tr>
                            <td style="background: #db1c25; padding:10px 20px;">
                                <img src="<?= SITE_LOGO ?>" style="height: 80px; display: block; margin: 0px auto;"> 
                            </td>
                        </tr>
                        <tr>
                            <td style="background: #fff; padding: 20px 40px;  text-align:center;">
                                <strong style="display: inline-block; height: 32px; line-height: 32px; padding-left:45px; font-size: 20px; font-weight: bold; background-image: url(<?= STATIC_IMAGES_PATH ?>checked_icon.png); background-size: 32px auto; background-position: center left; background-repeat: no-repeat; margin-bottom: 10px;">Congratulations <?= $username ?>!</strong>
                                <p style="font-size: 15px; line-height:25px; margin: 0px;">Welcome to India's Most Trusted Rummy Site, where millions of players get together to enjoy the thrill of playing rummy and winning real cash prizes everyday. Verify your email to activate your account and get a Welcome Bonus worth <strong style="color: #db1c25;">Rs.<?=WELCOME_BONUS?></strong>.</p>
                                <a href="<?= $email_verification_link ?>" style="display: inline-block; height: 50px; line-height: 50px; padding: 0px 20px; background: #049b27; color: white; text-decoration: none; font-weight: bold; text-transform: uppercase;margin-top: 20px; border-radius: 5px;">Activate Now </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="background: #fff; padding: 0px 40px 20px 40px;">
                                <table cellpadding="0" cellspacing="0" width="100%" style="background: #f2f2f2; padding-bottom: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;">

                                    <tr>
                                        <td style="background: #1e2931; color: white; padding: 10px 20px; font-weight: bold; border-top-left-radius: 10px; ">Special benefits for you</td>
                                        <td style="background: #1e2931; color: white; padding: 10px 20px; font-weight: bold; width: 25%; border-top-right-radius: 10px;">Value</td>
                                    </tr>

                                    <tr>
                                        <td style="background: #f2f2f2; padding: 0px 20px;">
                                            <div style="background-image: url(<?= STATIC_IMAGES_PATH ?>money1.png); background-size:  80px auto; background-position: center left; background-repeat: no-repeat; padding: 20px; padding-left: 90px;">   <strong style="color: #db1c25;">Welcome Bonus</strong>
                                                <p style="margin: 0px;">Get 100% Welcome Bonus</div></p>
                                        </td>
                                        <td style="background: #f2f2f2; padding: 10px 20px; width: 25%;">
                                            <p style="margin: 0px;">Bonus Value</p>
                                            <strong style="color: #db1c25;">Rs.<?=WELCOME_BONUS?></strong>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="background: #f2f2f2; padding: 0px 20px;">
                                            <div style="background-image: url(<?=STATIC_IMAGES_PATH?>money2.png); background-size:  80px auto; background-position: center left; background-repeat: no-repeat;padding: 20px;  padding-left: 90px;"> 
                                                <strong style="color: #db1c25;"><?=  number_format(MAX_FUN_CHIPS)?> Practice Chips</strong>
                                                <p style="margin: 0px;">Lifetime Free Re-Charge of practice chips enabled</p>
                                            </div>
                                        </td>
                                        <td style="background: #f2f2f2; padding: 10px 20px; width: 25%;">
                                            <p style="margin: 0px;">Virtual Chips</p>
                                            <strong style="color: #db1c25;"><?=  number_format(MAX_FUN_CHIPS)?></strong>
                                        </td>
                                    </tr>

                                </table>
                                <div style="text-align: center;"><a href="#" style="display: inline-block; height: 50px; line-height: 50px; padding: 0px 20px; background: #049b27; color: white; text-decoration: none; font-weight: bold; text-transform: uppercase;margin-top: 20px;border-radius: 5px;">Activate Now </a></div>
                            </td>
                        </tr>
                        <tr>
                            <td style="background: #1e2931; color: white; padding: 20px; ">
                                <strong>See you at the rummy tables!</strong>
                                <p style="margin: 0px; margin-top: 5px;">Team <b><?=SITE_TITLE?></b> India's Most Trusted Rummy Site</p>
                            </td>
                        </tr>
            </tr>
        </table>
        <p style="text-align: center; color: #676767;margin-bottom: 5px;">To ensure delivery to your inbox, add <?=NO_REPLAY_MAIL?> to your address book. </p>

        <p style="text-align: center; color: #676767; margin: 0px;">&copy; <?=SITE_TITLE?>. All rights reserved.</p>
    </td>
</tr>
</table>
</body>
</html>