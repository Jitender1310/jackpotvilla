<div class="navigation-wrapper border-bottom border-white blue_nav sticky-top">
<div class="row nav_row">
<section class="navigation sticky-top">
    <div class="container-fluid">
<nav class="navbar navbar-expand-lg navbar-light menu_nav">
  <!-- <a class="navbar-brand" href="#">Navbar</a> -->
  <button class="navbar-toggler ml-auto mr-2 mb-2 bg-light" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
  <ul class="nav menu" style="margin:auto"> 
  <!--  style=" float: left;" -->
        <li class="nav-item <?= $page_active == "home" ? "active" : "" ?>"><a class="nav-link" href="<?= base_url() ?>index.php" style="border-left: 0">Home</a></li>

        <li class="nav-item <?= $page_active == "game_lobby" ? "active" : "" ?>"><a class="nav-link" href="<?= base_url() ?>game_lobby" style="border-left: 0">Rummy</a></li>

        <li class="nav-item <?= $page_active == "casino" ? "active" : "" ?>"><a class="nav-link" href="<?= base_url() ?>slot" style="border-left: 0">Slot</a></li>
            <li class="nav-item <?= $page_active == "live_casino" ? "active" : "" ?>"><a class="nav-link" href="<?= base_url() ?>roulette">Roulette</a></li>
            <li class="nav-item <?= $page_active == "bingo" ? "active" : "" ?>"><a class="nav-link" href="<?= base_url() ?>bingo">Bingo</a></li>
            <li class="nav-item <?= $page_active == "virtuals" ? "active" : "" ?>"><a class="nav-link" href="<?= base_url() ?>virtuals">Virtuals</a></li>
            <li class="nav-item <?= $page_active == "bet_games" ? "active" : "" ?>"><a class="nav-link" href="<?= base_url() ?>bet_games">Bet Games</a></li>
            <li class="nav-item <?= $page_active == "scratch_cards" ? "active" : "" ?>"><a class="nav-link" href="<?= base_url() ?>scratch_cards">Scratch Cards</a></li>
            <!-- <li class="nav-item <?= $page_active == "all_games" ? "active" : "" ?>"><a class="nav-link" href="<?= base_url() ?>all_games">All Games</a></li> -->
            <li class="nav-item <?= $page_active == "referral_programs" ? "active" : "" ?>"><a class="nav-link" href="<?= base_url() ?>referral_programs">Referral Program</a></li>
            <li class="nav-item <?= $page_active == "loyalty_programs" ? "active" : "" ?>"><a class="nav-link" href="<?= base_url() ?>loyalty_programs">Loyalty Program</a></li>
           <li class="nav-item <?= $page_active == "promotions" ? "active" : "" ?>"><a class="nav-link" href="<?= base_url() ?>promotions" style="border-right: 0">Promotions</a></li>
    </ul>
  </div>
</nav>
</div>
</section>
</div>

</div>