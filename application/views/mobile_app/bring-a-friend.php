<?php
//var_dump($referral_link);die();
include 'header.php';
?>
<link rel="stylesheet" href="<?= STATIC_ANGULAR_CTRLS_PATH ?>Tiny-Text/dist/tagify.css">
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>Tiny-Text/dist/tagify.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>Tiny-Text/dist/jQuery.tagify.min.js"></script>
<div class="wrapper"  ng-controller="bringAFriendCtrl" style="    padding-bottom:170px;">
    <div class="container-fluid">
        <div class="row">

            <div class="col-12 text-center">
                <div class="row align-items-center justify-content-center my-5">
                    <div class="col-auto"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                               height="80px" viewBox="0 0 485.211 485.211" enable-background="new 0 0 485.211 485.211"
                                               xml:space="preserve">
                            <g>
                                <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="0" y1="242.6047" x2="485.2109" y2="242.6047">
                                    <stop  offset="0" style="stop-color:#FAA950"/>
                                    <stop  offset="1" style="stop-color:#E56704"/>
                                </linearGradient>
                                <path fill="url(#SVGID_1_)" d="M485.211,363.906c0,10.638-2.992,20.498-7.785,29.174L324.225,221.67l151.54-132.584
                                      c5.896,9.354,9.446,20.344,9.446,32.219V363.906L485.211,363.906z M242.606,252.793l210.862-184.5
                                      c-8.652-4.737-18.396-7.643-28.907-7.643H60.651c-10.524,0-20.271,2.905-28.889,7.643L242.606,252.793z M301.393,241.631
                                      l-48.809,42.733c-2.854,2.487-6.41,3.729-9.978,3.729c-3.57,0-7.125-1.242-9.98-3.729l-48.82-42.735L28.667,415.23
                                      c9.299,5.834,20.197,9.329,31.983,9.329h363.911c11.783,0,22.687-3.495,31.982-9.329L301.393,241.631z M9.448,89.085
                                      C3.554,98.44,0,109.429,0,121.305v242.602c0,10.638,2.978,20.498,7.789,29.174l153.183-171.439L9.448,89.085z"/>
                            </g>
                        </svg></div>
                    <div class="col-auto"><i class="fal fa-plus fa-2x"></i></div>
                    <div class="col-auto"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve"  height="80px" class=""><g><g>
                                    <g>
                                        <path style="fill:#EF934B" d="M53.566,44.783l-9.552-4.776C42.78,39.39,42,38.128,42,36.748V33    c0.268-0.305,0.576-0.698,0.904-1.162c1.302-1.838,2.286-3.861,2.969-5.984C47.098,25.477,48,24.345,48,23v-4    c0-0.88-0.391-1.667-1-2.217V11c0,0,1.187-9-11-9c-12.188,0-11,9-11,9v5.783c-0.609,0.55-1,1.337-1,2.217v4    c0,1.054,0.554,1.981,1.383,2.517C26.382,29.869,29,33,29,33v3.655c0,1.333-0.728,2.56-1.899,3.198L18.18,44.72    C15.603,46.125,14,48.826,14,51.761V55h44v-3.043C58,48.919,56.283,46.142,53.566,44.783z" data-original="#8697CB" class="" data-old_color="#8697CB"/>
                                        <path style="fill:#EF7515" d="M18.18,44.72l5.946-3.243c-0.034-0.033-0.005-0.043,0.065-0.036l2.91-1.587    C28.272,39.215,29,37.989,29,36.655V33c0,0-1.062-1.275-2.092-3.323h0c0-0.001-0.001-0.002-0.001-0.003    c-0.135-0.268-0.268-0.551-0.399-0.844c-0.018-0.041-0.036-0.08-0.054-0.121c-0.133-0.303-0.263-0.616-0.386-0.944    c-0.008-0.021-0.015-0.044-0.023-0.065c-0.108-0.29-0.209-0.589-0.306-0.896c-0.026-0.084-0.052-0.167-0.077-0.251    c-0.101-0.338-0.196-0.682-0.278-1.038C24.554,24.981,24,24.054,24,23v-4c0-0.88,0.391-1.667,1-2.217v-5.648    C23.587,10.039,21.397,9,18,9C8.437,9,8,17,8,17v4.995c-0.526,0.475-1,1.154-1,1.914v3.455c0,0.911,0.479,1.711,1.194,2.174    C9.057,33.296,11.955,36,11.955,36v3.157c0,1.151-0.629,2.211-1.64,2.762L3.61,46.122C1.385,47.336,0,49.668,0,52.203V55h14    v-3.239C14,48.826,15.603,46.125,18.18,44.72z" data-original="#556080" class="active-path" data-old_color="#556080"/>
                                    </g>
                                    <g>
                                        <circle style="fill:#2567E1" cx="48" cy="46" r="12" data-original="#26B999" class="" data-old_color="#26B999"/>
                                        <path style="fill:#FFFFFF" d="M54.571,40.179c-0.455-0.316-1.077-0.204-1.392,0.25l-5.596,8.04l-3.949-3.242    c-0.426-0.351-1.057-0.288-1.407,0.139c-0.351,0.427-0.289,1.057,0.139,1.407l4.786,3.929c0.18,0.147,0.404,0.227,0.634,0.227    c0.045,0,0.091-0.003,0.137-0.009c0.276-0.039,0.524-0.19,0.684-0.419l6.214-8.929C55.136,41.118,55.024,40.495,54.571,40.179z" data-original="#FFFFFF" class="" data-old_color="#FFFFFF"/>
                                    </g>
                                </g></g> </svg>
                    </div>
                    <div class="col-auto"><i class="fal fa-equals fa-2x"></i></div>
                    <div class="col-auto"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                               height="80px" viewBox="0 0 512 512" enable-background="new 0 0 512 512" xml:space="preserve">
                            <g>
                                <g>
                                    <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="0" y1="255.9998" x2="512" y2="255.9998">
                                        <stop  offset="0" style="stop-color:#FAA950"/>
                                        <stop  offset="1" style="stop-color:#E56704"/>
                                    </linearGradient>
                                    <path fill="url(#SVGID_1_)" d="M256,0C114.624,0,0,114.624,0,256c0,141.377,114.624,256,256,256s256-114.623,256-256
                                          C512,114.624,397.376,0,256,0z M318.368,192H336c8.832,0,16,7.169,16,16c0,8.832-7.168,16-16,16h-17.632
                                          c-7.424,36.48-39.744,64-78.368,64h-25.376l100.672,100.673c6.24,6.239,6.24,16.384,0,22.624c-3.104,3.136-7.2,4.703-11.296,4.703
                                          s-8.192-1.567-11.328-4.672l-128-128c-4.576-4.607-5.952-11.456-3.456-17.439C163.68,259.904,169.536,256,176,256h64
                                          c20.832,0,38.432-13.407,45.056-32H176c-8.832,0-16-7.168-16-16c0-8.831,7.168-16,16-16h109.056
                                          c-6.624-18.592-24.224-32-45.056-32h-64c-8.832,0-16-7.168-16-16c0-8.831,7.168-16,16-16h64h96c8.832,0,16,7.169,16,16
                                          c0,8.832-7.168,16-16,16h-32.416C310.656,169.344,315.968,180.096,318.368,192z"/>
                                </g>
                            </g>
                        </svg>
                    </div>
                </div>


                <div class="card mt-5">
                    <div class="card-body">

                        <div class="row align-items-center">
                            <div class="col">

                                  <div class="form-group bring_a_friend_by_contact">
                                                    <section id='section-basic'>
                                                        <aside class='rightSide'>
                                                        </aside>
                                                    </section>
                                                </div>


                                                <div class="form-group bring_a_friend_by_email">
                                                    <section id='section-basic_email'>
                                                        <aside class='rightSide'>
                                                        </aside>
                                                    </section>
                                                </div>
                                                <div class="row">
                                                <div class=" col-5">
                                                </div>

                            <div class="col-2"><svg height="60px" viewBox="0 0 14 56.664"><use xlink:href="#or-v"></use></svg></div>
                            <div class="col-5">
                                <p class="text-balck-50" style=" display: none;">Share Your Link</p>

                                <ul class="nav nav-socials justify-content-center">
                                    <li class="nav-item"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode($referral_link); ?>" class="nav-link rounded fb fab fa-facebook-f"></a></li>
                                    <li class="nav-item"><a target="_blank" href="https://twitter.com/intent/tweet?text=<?php echo urlencode("I love playing #Rummy. Join me @@King Of Rummy to get your Rs." . WELCOME_BONUS . " free joining bonus by clicking this link:"); ?>&url=<?php echo urlencode($referral_link); ?>" class="nav-link rounded tw fab fa-twitter"></a></li>
                                    <li class="nav-item"><a target="_blank" href="https://api.whatsapp.com/send?phone=&text=<?php echo "I love playing #Rummy. Join me @@King Of Rummy to get your Rs." . WELCOME_BONUS . " free joining bonus by clicking this link: " . urlencode($referral_link); ?>" class="nav-link rounded tw fab fa-whatsapp"></a></li>
                                    <li class="nav-item"><a style="cursor:pointer;" ng-click="copyToClipboard('<?php echo $referral_link; ?>')" class="nav-link rounded fas fa-link"></a><input type="text" style="display:none;" value="" id="referral_link"></li>
                                </ul>
                            </div>
                            </div>
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>bringAFriendCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>bringAFriendService.js?r=<?= time() ?>"></script>

<script data-name="basic">
                                                        (function () {

                                                            var input = document.querySelector('input[name=phones]'),
                                                                    // init Tagify script on the above inputs
                                                                    tagify = new Tagify(input, {
                                                                        whitelist: ["A# .NET", "A# (Axiom)", "A-0 System", "A+", "A++", "ABAP", "ABC", "ABC ALGOL", "ABSET", "ABSYS", "ACC", "Accent", "Ace DASL", "ACL2", "Avicsoft", "ACT-III", "Action!", "ActionScript", "Ada", "Adenine", "Agda", "Agilent VEE", "Agora", "AIMMS", "Alef", "ALF", "ALGOL 58", "ALGOL 60", "ALGOL 68", "ALGOL W", "Alice", "Alma-0", "AmbientTalk", "Amiga E", "AMOS", "AMPL", "Apex (Salesforce.com)", "APL", "AppleScript", "Arc", "ARexx", "Argus", "AspectJ", "Assembly language", "ATS", "Ateji PX", "AutoHotkey", "Autocoder", "AutoIt", "AutoLISP / Visual LISP", "Averest", "AWK", "Axum", "Active Server Pages", "ASP.NET", "B", "Babbage", "Bash", "BASIC", "bc", "BCPL", "BeanShell", "Batch (Windows/Dos)", "Bertrand", "BETA", "Bigwig", "Bistro", "BitC", "BLISS", "Blockly", "BlooP", "Blue", "Boo", "Boomerang", "Bourne shell (including bash and ksh)", "BREW", "BPEL", "B", "C--", "C++ � ISO/IEC 14882", "C# � ISO/IEC 23270", "C/AL", "Cach� ObjectScript", "C Shell", "Caml", "Cayenne", "CDuce", "Cecil", "Cesil", "C�u", "Ceylon", "CFEngine", "CFML", "Cg", "Ch", "Chapel", "Charity", "Charm", "Chef", "CHILL", "CHIP-8", "chomski", "ChucK", "CICS", "Cilk", "Citrine (programming language)", "CL (IBM)", "Claire", "Clarion", "Clean", "Clipper", "CLIPS", "CLIST", "Clojure", "CLU", "CMS-2", "COBOL � ISO/IEC 1989", "CobolScript � COBOL Scripting language", "Cobra", "CODE", "CoffeeScript", "ColdFusion", "COMAL", "Combined Programming Language (CPL)", "COMIT", "Common Intermediate Language (CIL)", "Common Lisp (also known as CL)", "COMPASS", "Component Pascal", "Constraint Handling Rules (CHR)", "COMTRAN", "Converge", "Cool", "Coq", "Coral 66", "Corn", "CorVision", "COWSEL", "CPL", "CPL", "Cryptol", "csh", "Csound", "CSP", "CUDA", "Curl", "Curry", "Cybil", "Cyclone", "Cython", "Java", "Javascript", "M2001", "M4", "M#", "Machine code", "MAD (Michigan Algorithm Decoder)", "MAD/I", "Magik", "Magma", "make", "Maple", "MAPPER now part of BIS", "MARK-IV now VISION:BUILDER", "Mary", "MASM Microsoft Assembly x86", "MATH-MATIC", "Mathematica", "MATLAB", "Maxima (see also Macsyma)", "Max (Max Msp � Graphical Programming Environment)", "Maya (MEL)", "MDL", "Mercury", "Mesa", "Metafont", "Microcode", "MicroScript", "MIIS", "Milk (programming language)", "MIMIC", "Mirah", "Miranda", "MIVA Script", "ML", "Model 204", "Modelica", "Modula", "Modula-2", "Modula-3", "Mohol", "MOO", "Mortran", "Mouse", "MPD", "Mathcad", "MSIL � deprecated name for CIL", "MSL", "MUMPS", "Mystic Programming L"],
                                                                        blacklist: [".NET", "PHP"], // <-- passed as an attribute in this demo
                                                                    });


// "remove all tags" button event listener
//document.querySelector('.tags--removeAllBtn')
//    .addEventListener('click', tagify.removeAllTags.bind(tagify))

// Chainable event listeners
                                                            tagify.on('add', onAddTag)
                                                                    .on('remove', onRemoveTag)
                                                                    .on('input', onInput)
                                                                    .on('edit', onTagEdit)
                                                                    .on('invalid', onInvalidTag)
                                                                    .on('click', onTagClick)
                                                                    .on('dropdown:show', onDropdownShow)
                                                                    .on('dropdown:hide', onDropdownHide);

// tag added callback
                                                            function onAddTag(e) {
                                                                console.log("onAddTag: ", e.detail);
                                                                console.log("original input value: ", input.value)
                                                                tagify.off('add', onAddTag) // exmaple of removing a custom Tagify event
                                                            }

// tag remvoed callback
                                                            function onRemoveTag(e) {
                                                                console.log(e.detail);
                                                                console.log("tagify instance value:", tagify.value)
                                                            }

// on character(s) added/removed (user is typing/deleting)
                                                            function onInput(e) {
                                                                console.log(e.detail);
                                                                console.log("onInput: ", e.detail);
                                                            }

                                                            function onTagEdit(e) {
                                                                console.log("onTagEdit: ", e.detail);
                                                            }

// invalid tag added callback
                                                            function onInvalidTag(e) {
                                                                console.log("onInvalidTag: ", e.detail);
                                                            }

// invalid tag added callback
                                                            function onTagClick(e) {
                                                                console.log(e.detail);
                                                                console.log("onTagClick: ", e.detail);
                                                            }

                                                            function onDropdownShow(e) {
                                                                console.log("onDropdownShow: ", e.detail)
                                                            }

                                                            function onDropdownHide(e) {
                                                                console.log("onDropdownHide: ", e.detail)
                                                            }

                                                        })()
</script>
<script data-name="basic">
            (function () {

                var input = document.querySelector('input[name=emails]'),
                        // init Tagify script on the above inputs
                        tagify = new Tagify(input, {
                            whitelist: ["A# .NET", "A# (Axiom)", "A-0 System", "A+", "A++", "ABAP", "ABC", "ABC ALGOL", "ABSET", "ABSYS", "ACC", "Accent", "Ace DASL", "ACL2", "Avicsoft", "ACT-III", "Action!", "ActionScript", "Ada", "Adenine", "Agda", "Agilent VEE", "Agora", "AIMMS", "Alef", "ALF", "ALGOL 58", "ALGOL 60", "ALGOL 68", "ALGOL W", "Alice", "Alma-0", "AmbientTalk", "Amiga E", "AMOS", "AMPL", "Apex (Salesforce.com)", "APL", "AppleScript", "Arc", "ARexx", "Argus", "AspectJ", "Assembly language", "ATS", "Ateji PX", "AutoHotkey", "Autocoder", "AutoIt", "AutoLISP / Visual LISP", "Averest", "AWK", "Axum", "Active Server Pages", "ASP.NET", "B", "Babbage", "Bash", "BASIC", "bc", "BCPL", "BeanShell", "Batch (Windows/Dos)", "Bertrand", "BETA", "Bigwig", "Bistro", "BitC", "BLISS", "Blockly", "BlooP", "Blue", "Boo", "Boomerang", "Bourne shell (including bash and ksh)", "BREW", "BPEL", "B", "C--", "C++ � ISO/IEC 14882", "C# � ISO/IEC 23270", "C/AL", "Cach� ObjectScript", "C Shell", "Caml", "Cayenne", "CDuce", "Cecil", "Cesil", "C�u", "Ceylon", "CFEngine", "CFML", "Cg", "Ch", "Chapel", "Charity", "Charm", "Chef", "CHILL", "CHIP-8", "chomski", "ChucK", "CICS", "Cilk", "Citrine (programming language)", "CL (IBM)", "Claire", "Clarion", "Clean", "Clipper", "CLIPS", "CLIST", "Clojure", "CLU", "CMS-2", "COBOL � ISO/IEC 1989", "CobolScript � COBOL Scripting language", "Cobra", "CODE", "CoffeeScript", "ColdFusion", "COMAL", "Combined Programming Language (CPL)", "COMIT", "Common Intermediate Language (CIL)", "Common Lisp (also known as CL)", "COMPASS", "Component Pascal", "Constraint Handling Rules (CHR)", "COMTRAN", "Converge", "Cool", "Coq", "Coral 66", "Corn", "CorVision", "COWSEL", "CPL", "CPL", "Cryptol", "csh", "Csound", "CSP", "CUDA", "Curl", "Curry", "Cybil", "Cyclone", "Cython", "Java", "Javascript", "M2001", "M4", "M#", "Machine code", "MAD (Michigan Algorithm Decoder)", "MAD/I", "Magik", "Magma", "make", "Maple", "MAPPER now part of BIS", "MARK-IV now VISION:BUILDER", "Mary", "MASM Microsoft Assembly x86", "MATH-MATIC", "Mathematica", "MATLAB", "Maxima (see also Macsyma)", "Max (Max Msp � Graphical Programming Environment)", "Maya (MEL)", "MDL", "Mercury", "Mesa", "Metafont", "Microcode", "MicroScript", "MIIS", "Milk (programming language)", "MIMIC", "Mirah", "Miranda", "MIVA Script", "ML", "Model 204", "Modelica", "Modula", "Modula-2", "Modula-3", "Mohol", "MOO", "Mortran", "Mouse", "MPD", "Mathcad", "MSIL � deprecated name for CIL", "MSL", "MUMPS", "Mystic Programming L"],
                            blacklist: [".NET", "PHP"], // <-- passed as an attribute in this demo
                        });


// "remove all tags" button event listener
//document.querySelector('.tags--removeAllBtn')
//    .addEventListener('click', tagify.removeAllTags.bind(tagify))

// Chainable event listeners
                tagify.on('add', onAddTag)
                        .on('remove', onRemoveTag)
                        .on('input', onInput)
                        .on('edit', onTagEdit)
                        .on('invalid', onInvalidTag)
                        .on('click', onTagClick)
                        .on('dropdown:show', onDropdownShow)
                        .on('dropdown:hide', onDropdownHide);

// tag added callback
                function onAddTag(e) {
                    console.log("onAddTag: ", e.detail);
                    console.log("original input value: ", input.value)
                    tagify.off('add', onAddTag) // exmaple of removing a custom Tagify event
                }

// tag remvoed callback
                function onRemoveTag(e) {
                    console.log(e.detail);
                    console.log("tagify instance value:", tagify.value)
                }

// on character(s) added/removed (user is typing/deleting)
                function onInput(e) {
                    console.log(e.detail);
                    console.log("onInput: ", e.detail);
                }

                function onTagEdit(e) {
                    console.log("onTagEdit: ", e.detail);
                }

// invalid tag added callback
                function onInvalidTag(e) {
                    console.log("onInvalidTag: ", e.detail);
                }

// invalid tag added callback
                function onTagClick(e) {
                    console.log(e.detail);
                    console.log("onTagClick: ", e.detail);
                }

                function onDropdownShow(e) {
                    console.log("onDropdownShow: ", e.detail)
                }

                function onDropdownHide(e) {
                    console.log("onDropdownHide: ", e.detail)
                }

            })()
</script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.15.0/themes/prism.min.css">
<script src='https://cdnjs.cloudflare.com/ajax/libs/prism/1.15.0/prism.min.js'></script>
