<?php include 'header.php'; ?>
<div class="wrapper" ng-controller="paymentHistoryCtrl" >
    <div class="container-fluid2">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-custom text-nowrap mb-0">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Txn ID</th>
                        <th>Date & Time</th>
                        <th class="text-right">Transaction Amount</th>
                        <th class="text-right">Status</th>
                    </tr>
                </thead>
                <tr ng-if="paymentHistoryList.length == 0">
                    <td colspan="4" class="text-center">No data found</td>
                </tr>
                <tr ng-repeat="item in paymentHistoryList track by $index">
                    <td>{{paymentHistoryPagination.initial_id + $index}}</td>
                    <td>{{item.transaction_id}}</td>
                    <td>{{item.created_at}}</td>
                    <td class="text-right">{{item.credits_count}}</td>
                    <td class="text-right {{item.transaction_bootstrap_class}}">{{item.transaction_status}}</td>
                </tr>
            </table>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>paymentHistoryCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>paymentHistoryService.js?r=<?= time() ?>"></script>