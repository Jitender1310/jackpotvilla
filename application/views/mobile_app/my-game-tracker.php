<?php include 'header.php'; ?>
<div class="wrapper">
   <div class="container-fluid2">
 <table class="table table-bordered table-striped text-center">
                            <tbody><tr>
                                <td width="20%"></td>
                                <td width="20%"><strong class="d-block">Bronze</strong><small>(25)<i class="fal fa-info-circle ml-1"></i></small></td>
                                <td width="20%"><strong class="d-block">Silver</strong><small>(50)<i class="fal fa-info-circle ml-1"></i></small></td>
                                <td width="20%"><strong class="d-block">Gold</strong><small>(100)<i class="fal fa-info-circle ml-1"></i></small></td>
                                <td width="20%"><strong class="d-block">Diamond</strong> <small>Any of the Game<i class="fal fa-info-circle ml-1"></i></small></td>
                            </tr>
                            <tr>
                                <td><div class="h6 m-0">Hey MR</div><div class="m-0">(Daily)</div></td>
                                <td>4/4</td>
                                <td>3/4</td>
                                <td>2/2</td>
                                <td>6/6</td>
                            </tr>
                            <tr>
                                <td><div class="h6 m-0">Weekend Jackpot</div><div class="m-0">(Weekends)</div></td>
                                <td>4/4</td>
                                <td>3/4</td>
                                <td>2/2</td>
                                <td>6/6</td>
                            </tr>
                            <tr>
                                <td><div class="h6 m-0">Fortnight Fortunes</div><div class="m-0">(Fortnight)</div></td>
                                <td>4/4</td>
                                <td>3/4</td>
                                <td>2/2</td>
                                <td>6/6</td>
                            </tr>
                            <tr><td><div class="h6 m-0">Monthly Masters </div><div class="m-0">(Monthly)</div></td>
                            <td>4/4</td>
                            <td>3/4</td>
                            <td>2/2</td>
                            <td>6/6</td>
                            </tr>
                        </tbody></table>
   </div>
</div>
<?php include 'footer.php'; ?>