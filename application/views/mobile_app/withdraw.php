<?php include 'header.php'; ?>
<style>
.invalid{
	color: red;
}
</style>
<div class="wrapper" ng-controller="withdrawCtrl" style="padding-bottom: 150px;">
    <div class="container-fluid">
        <div class="row align-items-center mb-4">
            <div class="col">
                <h5>Available Balance</h5>
                <p class="text-danger m-0">*Minimum amount to withdraw: Rs <?= MINIMUM_AMOUNT_FOR_WITHDRAW_MONEY ?></p>
            </div>
            <div class="col-auto">
                <button class="btn btn-warning font-weight-bold">Rs {{chipsObj.wallet_account.real_chips_withdrawal}}</button>
            </div>
        </div>
        <?php $this->load->view("common_files/withdraw_form") ?>

        <!--        <div class="bordered-frame">
                    <div class="bf-label"><span>E-wallet</span></div>
                    <div class="bf-body">
                        <svg height="40px" viewBox="0 0 287.989 90.344">
                        <use xlink:href="#paytm" />
                        </svg>
                    </div>
                </div>-->
        <br/>
        <p class="text-white">To get information on processing fees and withdrawing cash from your account, <a href="<?= base_url() ?>withdraw_charges"  target="_blank" style=" color: #fff;">click here</a></p>
    </div>
</div>
<?php include 'footer.php'; ?>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>withdrawCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>withdrawService.js?r=<?= time() ?>"></script>