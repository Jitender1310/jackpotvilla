<?php include 'header.php'; ?>
<div class="wrapper" ng-controller="depositCtrl">
    <div class="container-fluid">
        <div class="text-dark">
            <?php $this->load->view('includes/bonus_coupons_popup') ?>
        </div>
        <form ng-submit="createDepositRequest()" id="depositForm">
            <div class="row no-gutters align-items-center mb-3">
                <div class="col-auto">
                    <div class="h6 m-0">Step 1 of 2</div>
                </div>
                <div class="col pl-2">
                    <div class="border-top border-alpha"></div>
                </div>
            </div>
            <div class="row justify-content-center mb-4">
                <div class="col-auto"><a href="javascript:void(0);" class="btn btn-outline-primary text-white-50" ng-click="deposit.credits_count = 100">Rs 100</a></div>
                <div class="col-auto"><a href="javascript:void(0);" class="btn btn-outline-primary text-white-50" ng-click="deposit.credits_count = 250">Rs 250</a></div>
                <div class="col-auto"><a href="javascript:void(0);" class="btn btn-outline-primary text-white-50" ng-click="deposit.credits_count = 500">Rs 400</a></div>
            </div>
            <div class="row my-4 justify-content-center">
                <div class=" col-8 justify-content-center">
                <div class="col-md">
                    <div class="form-group">
                        <label class="text-white-50">Enter Amount</label>
                        <input type="number"  name="amount" class="number form-control" ng-model="deposit.credits_count" ng-blur="validateBonusCode()">
                    </div>
                </div>
                <div class="col-md">
                    <div class="form-group">
                        <label class="text-white-50">Bonus Code</label>
                        <input ng-readonly="deposit.credits_count == false" type="text" name="bonus_code"  ng-model="deposit.bonus_code" class="form-control" ng-blur="validateBonusCode()">
                    </div>
                </div>
                <div class="col-md-auto align-self-end text-center">
                    <div class="form-group">
                        <span class="btn bg-transparent px-0 text-muted">(or)</span>
                    </div>
                </div>
                <div class="col-md align-self-end">
                    <div class="form-group">
                        <button ng-disabled="deposit.credits_count == false" type="button" class="btn btn-block btn-theme" data-toggle="modal" data-target="#bonuses"><i class="fa fa-plus-hexagon mr-2"></i>Apply Bonus</button>

                    </div>
                </div>
                    
                    <?php
                    $payment_options = [
                    ];
                    ?>


                    <div class="row  justify-content-center">
                        <?php foreach ($payment_options as $item) { ?>
                            <div class="col-4">
                                <label>
                                    <input type="radio" name="payment_gateway" ng-model="deposit.payment_gateway" value="<?= $item['key'] ?>">
                                    <img src="<?= STATIC_IMAGES_PATH . "payment_options/" . $item["image"] ?>" class="img-thumbnail" style="width:75px; height: 40px">
                                </label>
                            </div>
                        <?php } ?>
                    </div>

            </div>
          </div>

            <div class="text-center" ng-if="couponValidating">
                <img src="<?= base_url() ?>assets/images/loading.gif"/>
            </div>

            <div class="row  justify-content-center">
                <p class="{{bonusApplyNotifiction.css}}" ng-if="bonusApplyNotifiction.css">{{bonusApplyNotifiction.message}}</p>
            </div>

            <div class="text-center">
                <button class="btn btn-gold">Add Cash {{deposit.credits_count ? "Rs "+deposit.credits_count :""}}</button>
            </div>

            <!--            <div class="row no-gutters align-items-center my-3">
                            <div class="col-auto">
                                <div class="h6 m-0">Step 2 of 2</div>
                            </div>
                            <div class="col pl-2">
                                <div class="border-top border-alpha"></div>
                            </div>
                        </div>
                        <div class="bordered-frame mb-0">
                            <div class="bf-label"><span>Payment Method</span></div>
                            <div class="bf-body">
                                <div class="row align-items-center">
                                    <div class="col-auto my-2">
                                        <svg height="40px" viewBox="0 0 287.989 90.344">
                                        <use xlink:href="#paytm" />
                                        </svg>
                                    </div>
                                    <div class="col-auto my-2">
                                        <svg height="80px" viewBox="0 0 504 504">
                                        <use xlink:href="#mastercard" />
                                        </svg>
                                    </div>
                                    <div class="col-auto my-2">
                                        <svg height="80px" viewBox="0 0 504 504">
                                        <use xlink:href="#maestro" />
                                        </svg>
                                    </div>
                                    <div class="col-auto my-2">
                                        <svg height="80px" viewBox="0 0 291.764 291.764">
                                        <use xlink:href="#visa" />
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>-->
        </form>
    </div>
</div>
<?php include 'footer.php'; ?>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>depositCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>depositService.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>bonusAvailableService.js?r=<?= time() ?>"></script>