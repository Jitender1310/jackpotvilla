<!doctype html>
<html lang="en" ng-app="rummyApp" ng-cloak="" ng-init="GAME_SERVER_ID = '<?= GAME_SERVER_IP ?>'">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" type="text/css" href="<?= STATIC_CSS_PATH ?>bootstrap.css">
        <link rel="stylesheet" type="text/css" href="<?= STATIC_CSS_PATH ?>f5.css">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/css/swiper.min.css">
        <link rel="stylesheet" type="text/css" href="<?= STATIC_CSS_PATH ?>custom.css">
         <link rel="stylesheet" type="text/css" href="<?= STATIC_CSS_PATH ?>responsive.css">
        <link rel="stylesheet" href="<?= STATIC_CSS_PATH ?>jquery.datetimepicker.min.css">
        <link rel="stylesheet" href="<?= STATIC_CSS_PATH ?>custome_sweetalert.css">

        <script src="<?= STATIC_JS_PATH ?>jquery.min.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/popper.min.js"></script>

        <script src="<?= STATIC_JS_PATH ?>angular.min.js"></script>
        <script src="<?= STATIC_JS_PATH ?>jquery.datetimepicker.full.min.js"></script>

        <script src="<?= STATIC_ANGULAR_PATH ?>app.js?r=<?= time() ?>"></script>
        <script src="<?= STATIC_JS_PATH ?>app_urls.js?r=<?= time() ?>"></script>

        <script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>loginCtrl.js?r=<?= time() ?>"></script>
        <script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>commonCtrl.js?r=<?= time() ?>"></script>
        <script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>chipsService.js?r=<?= time() ?>"></script>
        <script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>loginService.js?r=<?= time() ?>"></script>
        <script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>statesService.js?r=<?= time() ?>"></script>
        <script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>playNowService.js?r=<?= time() ?>"></script>
        <script src="<?= STATIC_JS_PATH ?>sfs2x-api-1.7.11.js?r=<?= time() ?>"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/purl/2.3.1/purl.min.js"></script>
        <title><?=SITE_TITLE?></title>
        <script>
            var SITE_TITLE = '<?=SITE_TITLE?>' ;
            var WELCOME_BONUS = '<?=WELCOME_BONUS?>' ;
            </script>
    </head>
    <body ng-controller="commonCtrl" style=" background: transparent !important; color:#fff;">
        <div class="loadicons d-none"></div>

