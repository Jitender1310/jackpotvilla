<?php include 'header.php'; ?>
<div class="wrapper" ng-controller="profileCtrl">
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-sm py-sm-4 px-sm-3 p-0">
                <div class="row">
                    <div class="col-12">
                        <div class="card-bordered mb-3">
                            <div class="row align-items-center no-gutters">
                                <div class="col-auto pr-1" style="padding-left: 0;">User Name</div>
                                <div class="col"><span class="text-primary text-white-50"> {{profileObj.username}}</span></div>
                                <div class="col-auto">
<!--                                    <button class="btn btn-sm bg-transparent text-white">Change Password <i class="fal fa-pen ml-1"></i></button>-->
                                </div>
                            </div>
                        </div>
                        <div class="title-bordered mb-3">
                            <div class="row align-items-center">
                                <div class="col-auto">
                                    <strong>Personal Information</strong>
                                </div>
                                <div class="col">
                                    <div class="border-bottom"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class=" col-6 align-items-center mb-3" style=" padding-left: 0;">
                                <div class="col-4 font-weight-bold">Name:</div>
                                <div class="col">{{profileObj.firstname}} {{profileObj.lastname}}</div>

                            </div>
                            <div class=" col-6 align-items-center mb-3" style=" padding-left: 0">
                                <div class="col-4 font-weight-bold">Address:</div>
                                <div class="col">{{profileObj.city}}</div>

                            </div>
                            <div class=" col-12 align-items-center mb-3">
                                <div class="row">
                                    <div class="col-6 font-weight-bold">DOB:</div>
                                    <div class="col-6">{{profileObj.date_of_birth}}</div>

                                </div>
                            </div>


                            <div class=" col-12 align-items-center mb-3" style="text-align: center;">
                                <button ng-click="showEditProfilePopup()" class="btn btn-sm bg-transparent fal fa-pen text-white">&nbsp;Edit Profile</button>

                            </div>
                        </div>
                    </div>

                    <div class="text-dark">
                        <?php $this->load->view("includes/edit_profile_popup") ?>
                        <?php $this->load->view("includes/edit_email_popup") ?>
                        <?php $this->load->view("includes/edit_mobile_popup") ?>
                        <?php $this->load->view("includes/kyc_popup") ?>
                        <?php $this->load->view("includes/choose_avatar_popup") ?>
                    </div>
                    <div class="col-6 width-991">
                        <div class="card-bordered mb-3">
                            <div class="row align-items-center no-gutters">
                                <div class="col-auto pr-3">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="30px" height="32px" viewBox="0 0 485.211 485.211" style="enable-background:new 0 0 485.211 485.211;" xml:space="preserve" class=""><g><g>
                                    <path d="M485.211,363.906c0,10.637-2.992,20.498-7.785,29.174L324.225,221.67l151.54-132.584   c5.895,9.355,9.446,20.344,9.446,32.219V363.906z M242.606,252.793l210.863-184.5c-8.653-4.737-18.397-7.642-28.908-7.642H60.651   c-10.524,0-20.271,2.905-28.889,7.642L242.606,252.793z M301.393,241.631l-48.809,42.734c-2.855,2.487-6.41,3.729-9.978,3.729   c-3.57,0-7.125-1.242-9.98-3.729l-48.82-42.736L28.667,415.23c9.299,5.834,20.197,9.329,31.983,9.329h363.911   c11.784,0,22.687-3.495,31.983-9.329L301.393,241.631z M9.448,89.085C3.554,98.44,0,109.429,0,121.305v242.602   c0,10.637,2.978,20.498,7.789,29.174l153.183-171.44L9.448,89.085z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#E56704"/>
                                    </g></g> </svg>
                                </div>
                                <div class="col">
                                    <div>
                                        <strong class="text-white-50">Email Id:</strong> 
                                        <div>{{profileObj.email}}</div>
                                    </div>
                                </div>
                                <div class="col-auto text-right">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 width-991">
                        <div class="card-bordered mb-3">
                            <div class="row align-items-center no-gutters">
                                <div class="col-auto pr-3">



                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 58.981 58.981" style="enable-background:new 0 0 58.981 58.981;" xml:space="preserve" width="40px" height="40px" class=""><g><g>
                                    <g>
                                    <path style="fill:#E56704" d="M35.89,58H10.701c-1.881,0-3.405-1.525-3.405-3.405V3.405C7.296,1.525,8.821,0,10.701,0H35.89    c1.881,0,3.405,1.525,3.405,3.405v51.189C39.296,56.475,37.771,58,35.89,58z" data-original="#545E73" class="" data-old_color="#545E73"/>
                                    <rect x="7.296" y="6" style="fill:#363942" width="32" height="40" data-original="#8697CB" class="" data-old_color="#8697CB"/>
                                    <circle style="fill:#3E434A" cx="23.296" cy="52" r="3" data-original="#323A45" class="" data-old_color="#323A45"/>
                                    <path style="fill:#3E434A" d="M23.296,4h-4c-0.553,0-1-0.447-1-1s0.447-1,1-1h4c0.553,0,1,0.447,1,1S23.849,4,23.296,4z" data-original="#323A45" class="" data-old_color="#323A45"/>
                                    <path style="fill:#3E434A" d="M27.296,4h-1c-0.553,0-1-0.447-1-1s0.447-1,1-1h1c0.553,0,1,0.447,1,1S27.849,4,27.296,4z" data-original="#323A45" class="" data-old_color="#323A45"/>
                                    </g>
                                    <g>
                                    <circle style="fill:#3481D1" cx="39.686" cy="46.981" r="12" data-original="#26B999" class="active-path" data-old_color="#26B999"/>
                                    <path style="fill:#FFFFFF" d="M46.257,41.161c-0.455-0.316-1.077-0.204-1.392,0.25l-5.596,8.04l-3.949-3.242    c-0.426-0.351-1.057-0.288-1.407,0.139c-0.351,0.427-0.289,1.057,0.139,1.407l4.786,3.929c0.18,0.147,0.404,0.227,0.634,0.227    c0.045,0,0.091-0.003,0.137-0.009c0.276-0.039,0.524-0.19,0.684-0.419l6.214-8.929C46.822,42.1,46.71,41.476,46.257,41.161z" data-original="#FFFFFF" class="" data-old_color="#FFFFFF"/>
                                    </g>
                                    </g></g> </svg>
                                </div>
                                <div class="col">
                                    <div>
                                        <strong class="text-white-50">Mobile:</strong> 
                                        <div>{{profileObj.display_mobile}}</div>
                                    </div>
                                </div>
                                <div class="col-auto text-right">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="title-bordered mb-3">
                            <div class="row align-items-center">
                                <div class="col-auto">
                                    <strong>KYC Verification</strong>
                                </div>
                                <div class="col">
                                    <div class="border-bottom"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6 width-991">
                                <div class="row align-items-center no-gutters mb-3">
                                    <div class="col-auto pr-1"><i class="fas fa-user text-info mr-2"></i></div>
                                    <div class="col font-weight-bold">PAN Card:</div>
                                    <div class="col-auto"><button class="btn btn-sm bg-transparent text-balck-50" style=" background: #fff !important;">{{profileObj.pan_card_number}}
                                            <i class="fal fa-check-circle text-success" ng-if="profileObj.address_proof_status == 'Approved'"></i>
                                            <i class="fal fa-exclamation-triangle text-danger" ng-if="profileObj.address_proof_status != 'Approved'"></i>
                                        </button></div>
                                </div>
                            </div>
                            <div class="col-6 width-991">
                                <div class="row align-items-center no-gutters mb-3">
                                    <div class="col-auto pr-1"><i class="fas fa-home text-info mr-2"></i></div>
                                    <div class="col font-weight-bold">Address Proof:</div>
                                    <div class="col-auto"><button class="btn btn-sm bg-transparent text-balck-50" style=" background: #fff !important;">{{profileObj.address_proof_type}} 
                                            <i class="fal fa-check-circle text-success" ng-if="profileObj.address_proof_status == 'Approved'"></i>
                                            <i class="fal fa-exclamation-triangle text-danger" ng-if="profileObj.address_proof_status != 'Approved'"></i>
                                        </button></div>
                                </div>
                            </div>
                        </div>
                        <div class="card bg-mattegreen" ng-if="profileObj.kyc_status == 'Not Verified'" style="    text-align: center; border: none; background: transparent;">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col-12">


                                        <button class="btn btn-success" ng-click="showKycPopup()"><small>UPLOAD NOW</small></button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 py-sm-4 px-sm-3 p-0 text-center border-left border-alpha border-alpha-xs-none">
                <h6 class="mb-3 pt-3 pt-md-0">MY AVATAR</h6>
                <div class="row no-gutters mb-3">
                    <div class="col-12">
<!--                        <svg height="60px" viewBox="0 0 256 256">
                        <use xlink:href="#dp" />
                        </svg>-->
                        <img ng-src="{{profileObj.profile_pic}}" height="100px"/>
                    </div>
                </div>
                <button class="btn btn-gold btn-block" ng-click="showChooseAvatarPopup()">Change Avtar</button>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>