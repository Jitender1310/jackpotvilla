
<script src="<?= STATIC_JS_PATH ?>swiper.min.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/js/swiper.min.js"></script>-->
<script type="text/javascript" src="assets/js/custom.js"></script>
<script src="<?= STATIC_JS_PATH ?>jquery.validate.min.js"></script>
<script src="<?= STATIC_JS_PATH ?>additional-methods.js"></script>
<script src="<?= STATIC_JS_PATH ?>sweetalert.min.js"></script>
<script src="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>

<script src="<?= STATIC_JS_PATH ?>loadingoverlay.min.js"></script>
<script src="//apis.google.com/js/platform.js" async defer></script>
<?php if (is_logged_in()) { ?>
        <script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>profileCtrl.js?r=<?= time() ?>"></script>
        <script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>profileService.js?r=<?= time() ?>"></script>
        <script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>bonusAvailableCtrl.js?r=<?= time() ?>"></script>
        <script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>bonusAvailableService.js?r=<?= time() ?>"></script>
    <?php } ?>
</body>
</html>