<?php include 'header.php'; ?>
<div class="wrapper" ng-controller="bonusHistoryCtrl">
    <div class="container-fluid2">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-custom text-nowrap mb-0">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Txn ID</th>
                        <th>Type</th>
                        <th>Date & Time</th>
                        <th>Remark</th>
                        <th class="text-right">Amount</th>
                        <th>Expiry Date</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tr ng-if="paymentHistoryList.length == 0">
                    <td colspan="4" class="text-center">No data found</td>
                </tr>
                <tr ng-repeat="item in bonusHistoryList track by $index">
                    <td>{{bonusHistoryPagination.initial_id + $index}}</td>
                    <td>{{item.ref_id}}</td>
                    <td class="{{item.transaction_bootstrap_class}}">{{item.transaction_type}}</td>
                    <td>{{item.created_date_time}}</td>
                    <td>{{item.remark}}</td>
                    <td class="text-right">{{item.amount}}</td>
                    <td class="text-right">{{item.expiry_date}}</td>
                    <td class="text-right {{item.expiry_status_class}}">{{item.expiry_status}}</td>
                </tr>
            </table>
            <style>
                .pagination li a {
                    padding: 8px!important;
                }
                .pagination li a hover{
                    padding: 8px!important;
                }
            </style>
            <div class="custom-pagination" ng-bind-html="bonusHistoryPagination.pagination"></div>

        </div>
    </div>
</div>
<?php include 'footer.php'; ?>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>bonusHistoryCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>bonusHistoryService.js?r=<?= time() ?>"></script>