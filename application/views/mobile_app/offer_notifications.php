<?php include 'header.php'; ?>
<div class="wrapper">
    <div class="container-fluid">
        <div class="row" style="background: transparent;
             padding: 20px 0;">
            <div class="col-2"></div>
            <div class="col-8 text-white">
                <div class="tab-content editor">

                    <div class="tab-pane fade active show tab" id="h1" role="tabpanel" aria-labelledby="t1" style=" background: transparent;">

                        <div class="card  card-gold" style=" background:  transparent;">
                            <div class="card-header card-header2 text-white">
                                <?php echo $this->data["offer_notifications"]->title ?>
                            </div>
                            <div class="img-box12">
                                <img  style="width: 100%; height:200px;" src="<?php echo base_url("/admin/uploads/".$this->data["offer_notifications"]->image) ?>">
                            </div>
                            <div>
                                <?php echo $this->data["offer_notifications"]->description ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
        </div>
        <?php include 'footer.php'; ?>