<?php include 'header.php'; ?>
<div class="wrapper" ng-controller="accountOverViewCtrl">
    <div class="container-fluid" ng-controller="profileCtrl">
        <div class="row font-weight-normal mb-4">
            <div class="col"><strong>User Name:</strong> {{profileObj.username}}</div>
            <div class="col-auto">
                <span ng-if="profileObj.kyc_status == 'Verified'">KYC Verified<i class="fal fa-check ml-1 text-success"></i></span>
                <span ng-if="profileObj.kyc_status != 'Verified'">KYC Verification<i class="fal fa-exclamation-triangle ml-1 text-danger"></i></span>
            </div>
        </div>
        <div class="bordered-frame">
            <div class="bf-label"><span>Cash Account</span></div>
            <div class="bf-body">
                <div class="row">
                    <div class="col-md-6 pr-md-5 my-3">
                        <div class="row align-items-end">
                            <div class="col-6 col-md">Deposit Balance</div>
                            <div class="col-6 col-md-4">
                                <div class="digits">{{acObj.deposit_balance}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 pl-md-5 my-3">
                        <div class="row align-items-end">
                            <div class="col-6 col-md">Reward Points</div>
                            <div class="col-6 col-md-4">
                                <div class="digits">{{acObj.reward_points}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 pr-md-5 my-3">
                        <div class="row align-items-end">
                            <div class="col-6 col-md">Withdrawal Balance</div>
                            <div class="col-6 col-md-4">
                                <div class="digits">{{acObj.withdrawal_balance}}</div>
                            </div>
                        </div>
                    </div>
                    <!--                    <div class="col-md-6 pl-md-5 my-3">
                                            <div class="row align-items-end">
                                                <div class="col-6 col-md">Level 1 Status</div>
                                                <div class="col-6 col-md-4">
                                                    <div class="digits">Joker</div>
                                                </div>
                                            </div>
                                        </div>-->
                    <div class="col-md-6 pr-md-5 my-3">
                        <div class="row align-items-end">
                            <div class="col-6 col-md">Total Cash Balance</div>
                            <div class="col-6 col-md-4">
                                <div class="digits">{{acObj.total_cash_balance}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bordered-frame">
            <div class="bf-label"><span>Practice account</span></div>
            <div class="bf-body">
                <div class="row">
                    <div class="col-md-6 pr-5 my-3">
                        <div class="row align-items-end">
                            <div class="col">Practice Chips: {{acObj.practice_account_chips}} chips</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bordered-frame mb-1">
            <div class="bf-label"><span>Practice account</span></div>
            <div class="bf-body">
                <div class="row">
                    <div class="col-md-6 pr-md-5 my-3">
                        <div class="row align-items-end">
                            <div class="col-6 col-md">Release Bonus </div>
                            <div class="col-6 col-md-4">
                                <div class="digits">{{acObj.release_bonus}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 pl-md-5 my-3">
                        <div class="row align-items-end">
                            <div class="col-6 col-md">Pending Bonu</div>
                            <div class="col-6 col-md-4">
                                <div class="digits">{{acObj.pending_bonus}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 pr-md-5 my-3">
                        <div class="row align-items-end">
                            <div class="col-6 col-md">Active Bonus</div>
                            <div class="col-6 col-md-4">
                                <div class="digits">{{acObj.active_bonus}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>accountOverViewCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>accountOverViewService.js?r=<?= time() ?>"></script>