<style>
    #divLoading
    {
        display : none;
    }
    #divLoading.show
    {
        display : block;
        position : fixed;
        z-index: 100;
        opacity : 0.4;
        background-repeat : no-repeat;
        background-position : center;
        left : 0;
        bottom : 0;
        right : 0;
        top : 0;
    }
    #loadinggif.show
    {
        left : 50%;
        top : 50%;
        position : absolute;
        z-index : 101;
        width : 32px;
        height : 32px;
        margin-left : -16px;
        margin-top : -16px;
    }
</style>

<div class="text-center show" id="loadinggif" >
    <img src="<?= base_url() ?>assets/images/loading.gif"/>
</div>