<?php include 'header.php'; ?>
<div class="wrapper" ng-controller="transactionsCtrl" ng-init="filterObj.page = '1';
                getTxnHistory()">
    <div class="container-fluid2">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-custom text-nowrap mb-0">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Type</th>
                        <th>Ref. ID</th>
                        <th>On</th>
                        <th>Amount</th>
                        <th class="text-right">Balance</th>
                    </tr>
                </thead>
                <tr ng-repeat="transaction in transactions">
                    <td>{{transactionHistoryPagination.initial_id + $index}}</td>
                    <td><span class="{{transaction.transaction_bootstrap_class}}">{{transaction.transaction_type}}</span> / {{transaction.type}}</td>
                    <td>{{transaction.ref_id}}</td>
                    <td>{{transaction.created_date_time}}</td>
                    <td>{{transaction.amount}}</td>
                    <td class="text-right">{{transaction.closing_balance}}</td>
                </tr>
            </table>
            <div ng-if="transactions.length == 0">
                <h4 class="text-center" style="padding:20px;">No data found</h4>
            </div>
            <style>
                .pagination li a {
                    padding: 8px!important;
                     color: #fff;
                }
                .pagination li a hover{
                    padding: 8px!important;
                }
            </style>
            <div class="custom-pagination" ng-bind-html="transactionHistoryPagination.pagination" style="float:right;"></div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>transactionsCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>transactionsService.js?r=<?= time() ?>"></script>