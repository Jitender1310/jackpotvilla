<?php if (is_logged_in()) { ?>
    <?php $this->load->view('includes/menu-dashboard') ?>
<?php } ?>

<?php if (!is_logged_in()) { ?>
    <?php $this->load->view('menu') ?>
<?php } ?>

<section class="innerpages py-4">
    <div class="container">
        <div class="row">
            <div class="col-12 text-white" style="background: #fff; padding: 10px 10px;">

                <div class="editor">
                    <div class=" row">
                    <div class=" col-md-4">
                                            <h5 style=" color: #c0392b;font-size: 28px; margin-bottom: 4px;">About us</h5>

                    <!-- <img src="<?= STATIC_IMAGES_PATH ?>about-us-banner.png" style="display: none" class="img-fluid mb-4"> -->
                    </div>
                    <div class="container" style="border: 1px solid #ccc; border-radius: 8px">
                    <p style="color:#000;">About Us: Here at Rummy Desk, we provide a vivid gaming experience to our fellow Rummy enthusiasts. Come join our arena to play against the best, in a fair and safe play and win exciting rewards and cash prizes! We sell to you the best stage for us rummy lovers and potential rummy geniuses. Rummy Desk brings to you, a shift from conventional gaming sites, to a much more thrilling and sophisticated card game experience.
                        Love for rummy is only showing an upward trend in India and we, at Rummy Desk hope to help you realize your passion for the game we love. With only access to the Internet, you can now permanently say goodbye to boredom and play at any time of the say. All you have to do is register now, play tournaments and win money!
                        Not only can you win spectacular prizes and cash money, you can now communicate with your fellow rummy lovers and draw ticks and tips from them.</p>
                    <p style="color: #000;">Our friendly customer service ensures top class service to our loyal users. Financial transactions are transparent and well accounted for. Learn more about us in our Support and Security.</p>
                  
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>registerCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>registerService.js?r=<?= time() ?>"></script>