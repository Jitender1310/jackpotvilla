<?php $this->load->view('includes/menu-dashboard') ?>
<section class="innerpages py-5" ng-controller="rpsTransactionsCtrl" ng-init="filterObj.page = <?php echo $current_page; ?>;
                getTxnHistory()">
    <div class="container">
        <div class="rps-outer">
        <?php $this->load->view('chips') ?>
        <div class="row mt-4">
            <?php $this->load->view('includes/dashboard-sidebar') ?>
            <div class="col-9">
                <div class="card card-gold">
                    <div class="card-header py-2 card-header2 text-white">
                        <div class="row align-items-center">
                            <div class="col-9">Rps Transactions</div>
                            <div class="col-3">
                                <select class="custom-select border-0" ng-model="filter.txn_type" ng-change="getTxnHistory()">
                                    <option value="All">All</option>
                                    <option value="Credit">Credit</option>
                                    <option value="Debit">Debit</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-hover table-custom mb-0">
                        <thead class="bg-secondary text-white">
                            <tr>
                                <th>S.No</th>
                                <th>Type</th>
                                <th>Ref. ID</th>
                                <th>On</th>
                                <th>Amount</th>
                                <th class="text-right">Balance</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="rpsTransaction in rpsTransactions">
                                <td>{{rpsTransactionHistoryPagination.initial_id + $index}}</td>
                                <td><span class="{{rpsTransaction.rpsTransaction_bootstrap_class}}">{{rpsTransaction.transaction_type}}</span> / {{rpsTransaction.type}}</td>
                                <td>{{rpsTransaction.ref_id}}</td>
                                <td>{{rpsTransaction.created_date_time}}</td>
                                <td>{{rpsTransaction.amount}}</td>
                                <td class="text-right">{{rpsTransaction.closing_balance}}</td>
                            </tr>
                        </tbody>
                    </table>
                    <div ng-if="rpsTransactions.length == 0">
                        <h4 class="text-center" style="padding:20px;">No data found</h4>
                    </div>
                    <div class="custom-pagination" ng-bind-html="rpsTransactionHistoryPagination.pagination"></div>
                </div>
            </div>
        </div>
        </div>
    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>rpsTransactionsCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>rpsTransactionsService.js?r=<?= time() ?>"></script>