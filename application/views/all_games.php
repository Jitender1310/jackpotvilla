<?php if (is_logged_in()) { ?>
    <?php $this->load->view('includes/menu-dashboard') ?>
<?php } ?>

<?php if (!is_logged_in()) { ?>
    <?php $this->load->view('menu') ?>
<?php } ?>
<section class="innerpages">
    <div class="container py-2" style="background: #fff;">
    <div class="card card-gold text-center">
    <div class="card-header card-header2 text-white">
        <h3>All Games</h3>
    </div>
    </div>
    </div>
</section>

<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>registerCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>registerService.js?r=<?= time() ?>"></script>   