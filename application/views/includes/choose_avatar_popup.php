<!-- Edit email Popup Starts -->
<div class="modal" id="editAvatarModal" data-backdrop='static' data-keyboard='false'>
    <div class="modal-dialog modal-lg">
        <form ng-submit="updateNewEmail()" id="editEmailForm" ng-keyup="ep_error = {}">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Change Avatar</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-3 m-2 text-center" ng-repeat="item in avatars.list track by $index">
                            <img ng-src="{{item.image}}" class="img-thumbnail" height="100px"/>
                            <button type="button" class="m-1 btn btn-primary btn-sm" ng-click="setAvatar(item.id)">Set this</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Edit Email POPUP Ends -->