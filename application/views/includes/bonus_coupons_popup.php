<div class="modal fade" id="bonuses">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content bg-red">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">BONUS AVAILABLE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span></span>
                </button>
            </div>
            <div class="modal-body bonus-table" ng-controller="bonusAvailableCtrl">

                <div class="bonus-table-row" ng-repeat="coupon in coupons">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6>{{coupon.bonus_type}}</h6>
                            <p class="m-0 text-white-50">({{coupon.description}})</p>
                        </div>
                        <div class="col text-center"><h6>#{{coupon.coupon_code}}</h6></div>
                        <div class="col-auto"><button ng-click="applyBonusCodeHandler(coupon.coupon_code)" class="btn btn-theme" data-dismiss="modal">Apply Code</button></div>
                    </div>
                </div>
                
            </div>

        </div>
    </div>
</div>