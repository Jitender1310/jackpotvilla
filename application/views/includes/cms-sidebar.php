<div class="card border-0 justify-content-center" style="border: 1px solid #c0c0c0 !important;">
                    <div class="list-group list-group-flush">
                        <a href="<?= base_url() ?>about" class="list-group-item font-weight-bold active">About Us</a>
                        <a href="<?= base_url() ?>how_to_play" class="list-group-item font-weight-bold  text-black1">How to Play</a>
                        <a href="<?= base_url() ?>promotions" class="list-group-item font-weight-bold  text-black1">Promotions</a>
                        <a href="<?= base_url() ?>bring_a_buddy" class="list-group-item font-weight-bold text-black1">Bring a Buddy</a>
                        <a href="<?= base_url() ?>tips_and_tricks" class="list-group-item font-weight-bold text-black1">Tips and Tricks</a>
                    </div>
                </div>