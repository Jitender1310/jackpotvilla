<div class="modal fade" id="modal-rps">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content bg-grey">
            <div class="modal-header" style="border-bottom: 1px solid #c0c0c0;">
                <h5 class="modal-title" id="exampleModalLabel">RPS</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span></span>
                </button>
            </div>
            <div class="modal-body px-4" style="background: #fff;">
                
                <?php if($player_details->wallet_info->total_rps_points){?>
                    <h4 class="text-white">Your reward points balance : <?=$player_details->wallet_info->total_rps_points?></h4>
                <?php }else{?>
                    <h4 class=" text-black1">You have no Reward Points in your account. Add cash to your account to start earning Loyalty and Reward Points.</h4>
                <?php }?>
                
                
                
                <div class="row">
                    <?php $images = $this->rps_model->get_rps_images();?>
                    <?php for ($i = 0; $i < count($images); $i++) { ?>
                        <div class="col-3 my-3">
                            <div class="card card-rps">
                                <div class="card-body">
                                    <div class="mb-3">
                                        <img height="100" src="<?=$images[$i]->key?>">
                                    </div>
                                    <span class="btn btn-warning rounded font-weight-bold text-primary" ng-click="redeemRps(<?=$images[$i]->value?>)"><?=$images[$i]->display_text?></span>
                                    <p class="mt-3 text-white small">Required to redeem.</p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>            
                </div>        
            </div>
        </div>
    </div>
</div>