<div class="modal fade" id="withdrawDetailsModalForm">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content bg-grey">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Confirm Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span></span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-bordered-alpha text-white text-center m-0">
                    <tr>
                        <th>Withdraw Type</th>
                        <td>{{withdraw_validted_obj.transfer_type}}</td>
                    </tr>
                    <tr>
                        <th>Withdraw To</th>
                        <td>{{withdraw_validted_obj.transfer_to}}</td>
                    </tr>
                    <tr ng-if="withdraw_validted_obj.transfer_to == 'PAYTM'">
                        <th>Paytm Number</th>
                        <td>{{withdraw_validted_obj.paytm_mobile_number}}</td>
                    </tr>
                    <tr ng-if="withdraw_validted_obj.transfer_to == 'PAYTM'">
                        <th>Paytm Person Name</th>
                        <td>{{withdraw_validted_obj.paytm_person_name}}</td>
                    </tr>


                    <tr ng-if="withdraw_validted_obj.transfer_to == 'BANK TRANSFER' || withdraw_validted_obj.transfer_type == 'NEFT'">
                        <th>Account Holder Name</th>
                        <td>{{withdraw_validted_obj.account_holder_name}}</td>
                    </tr>
                    <tr ng-if="withdraw_validted_obj.transfer_to == 'BANK TRANSFER' || withdraw_validted_obj.transfer_type == 'NEFT'">
                        <th>Account Number</th>
                        <td>{{withdraw_validted_obj.account_number}}</td>
                    </tr>
                    <tr ng-if="withdraw_validted_obj.transfer_to == 'BANK TRANSFER' || withdraw_validted_obj.transfer_type == 'NEFT'">
                        <th>IFSC Code</th>
                        <td>{{withdraw_validted_obj.ifsc_code}}</td>
                    </tr>
                    <tr ng-if="withdraw_validted_obj.transfer_to == 'BANK TRANSFER' || withdraw_validted_obj.transfer_type == 'NEFT'">
                        <th>Bank Name</th>
                        <td>{{withdraw_validted_obj.bank_name}}</td>
                    </tr>
                    <tr ng-if="withdraw_validted_obj.transfer_to == 'BANK TRANSFER' || withdraw_validted_obj.transfer_type == 'NEFT'">
                        <th>Branch Name</th>
                        <td>{{withdraw_validted_obj.branch_name}}</td>
                    </tr>


                    <tr>
                        <th>Withdraw Amount</th>
                        <td>Rs.{{withdraw_validted_obj.request_amount}}</td>
                    </tr>
                    <tr>
                        <th>Service Charge ({{withdraw_validted_obj.service_charge_percentage}}%)</th>
                        <td>Rs.{{withdraw_validted_obj.service_charge}}</td>
                    </tr>
                    <tr>
                        <th>TDS ({{withdraw_validted_obj.tds_percentage}}%)</th>
                        <td>Rs.{{withdraw_validted_obj.tds_charge}}</td>
                    </tr>

                    <tr>
                        <th>Net Receivable</th>
                        <th>Rs.{{withdraw_validted_obj.net_receivable}}</th>
                    </tr>
                </table>

                <br/>
                <div class="col-auto" align="center">
                    <button type="button" ng-click="RequestWithdrawl()" class="btn btn-warning font-weight-bold">Proceed To Withdraw</button>
                </div>

            </div>
        </div>
    </div>
</div>