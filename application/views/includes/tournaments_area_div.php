<div class="card-body" ng-if="selectedGameType == 'tournaments'">
    <div class="card card-table-menu shadow">
        <div class="card-body py-2">
            <div class="row no-gutters">
                <div class="col pr-2" ng-click="setSubGameType(tc.name)" ng-repeat="tc in gameLobbyObj.tournaments track by $index">
                    <a class="special-tag text-center {{selectedSubGameType==tc.name?'active':''}}" href="javascript:void(0);">
                        {{tc.display_menu_name}}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<table class="table table-custom table-striped table-hover bg-white" ng-if="selectedGameType == 'tournaments'">
    <thead class="bg-primary-dark text-white">   
        <tr>
            <th class="border-top-0">Title</th>
            <th class="border-top-0">Entry</th>
            <th class="border-top-0">Prize</th>
            <th class="border-top-0">Joined</th>
            <th class="border-top-0">Date</th>
            <th class="border-top-0 text-right">Action</th>
            <th class="border-top-0"></th>
        </tr>
    </thead>
    <tr ng-repeat="item in gameListTable track by $index">
        <td>{{item.tournament_title}}</td>
        <td>{{item.entry_type=='Cash' ? "Rs."+item.entry_value : item.entry_type}}</td>
        <td>{{item.expected_prize_amount}}</td>
        <td>{{item.number_of_registered_users}}/{{item.max_players}}</td>

        <td ng-if="item.is_past == 1">N/a</td>
        <td ng-if="item.is_past == 0">{{item.display_tournament_date_time}}</td>
        <td>

            <span ng-if="item.tournament_status != 'Created'">{{item.tournament_status}}</span>

            <button type="button" ng-click="showTournamentDeatilsPopup(item)" ng-if="item.tournament_status == 'Created'" class="btn rounded btn-default btn-sm">Join</button>

        </td>
        <td class="text-right">

            <button type="button" ng-if="item.tournament_status == 'Running' && item.is_joined == 1" class="btn rounded btn-info btn-sm" 
                    ng-click="openPlayNowTournamentWindow(item.cloned_tournaments_id, '<?= $player_details->access_token ?>', 'Tournament')">Take Seat</button>

            <button type="button" ng-if="item.tournament_status == 'Completed'" ng-click="showTournamentDeatilsPopup(item)" class="btn rounded btn-default btn-sm">Completed</button>
            <button type="button" ng-click="showTournamentDeatilsPopup(item)" ng-if="item.is_past === 0" class="btn rounded btn-success btn-sm">Details</button>
        </td>
    </tr>
    <tr ng-if="gameListTable.length == 0">
        <td colspan="7"><h4 class="text-center">No tournaments found</h4></td>
    </tr>
</table>