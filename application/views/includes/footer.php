<style>
.img_slider .swiper-container {
        width: 100%;
        height: 100%;
    }
    .img_slider .swiper-container  .swiper-slide {
        margin: 0; 
        padding: 0;
        box-sizing: border-box;
        text-align: center;
        font-size: 18px;
        background: #fff;
        height: 200px;
        /* Center slide text vertically */
        display: -webkit-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        -webkit-justify-content: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        align-items: center;
        border: 1px solid #dbb300;
        color: #000; 
    }
    .img_slider .swiper-container  .swiper-slide img {
        margin: 0;
        padding:0;
        height: 200px; 
        width: 100%;
    }
.img_slider .swiper-container {
  position: relative;
  background-color: #000
}

.image {
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}

.middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}

.slide_1:hover .image,
.slide_2:hover .image,
.slide_3:hover .image,
.slide_4:hover .image,
.slide_5:hover .image {
  opacity: 0.3;
}

.slide_1:hover .middle,
.slide_2:hover .middle, 
.slide_3:hover .middle,
.slide_4:hover .middle,
.slide_5:hover .middle{
  opacity: 1;
}

.text {
  background-color: #0f7579;
  color: white;
  font-size: 14px;
  padding: 12px 24px;
  border-radius: 6px;
}




</style>
<footer class="footer py-5">
<!-- <section class="img_slider mb-4">
<div class="container-fluid">   
    <!-- Slider main container -->
   <!-- <div class="swiper-container flex_boxes">
        <!-- Additional required wrapper -->
       <!-- <div class="swiper-wrapper">
            <div class="swiper-slide slide_1" data-swiper-autoplay="2000">
                <img src="<?= STATIC_IMAGES_PATH ?>/lucky_charm.jpg" class="image" alt="">
                <div class="middle">
                <a href="#">
                    <div class="text"> Login</div>
                 </a>
                </div>
            </div>
            <div class="swiper-slide slide_2" data-swiper-autoplay="2000">
                <img src="<?= STATIC_IMAGES_PATH ?>/atlantic_treasures_img.jpg" class="image" alt="">
                <div class="middle">
                <a href="#">
                    <div class="text"> Login</div>
                 </a>
                </div>
            </div>
            <div class="swiper-slide slide_3" data-swiper-autoplay="2000">
                <img src="<?= STATIC_IMAGES_PATH ?>/egyptian_rebirth_img.jpg" class="image" alt="">
                <div class="middle">
                <a href="#">
                    <div class="text"> Login</div>
                 </a>
                </div>
            </div>
            <div class="swiper-slide slide_4" data-swiper-autoplay="2000">
                <img src="<?= STATIC_IMAGES_PATH ?>/pirates_plunder_thumb_img.jpg" class="image" alt="">
                <div class="middle">
                <a href="#">
                    <div class="text"> Login</div>
                 </a>
                </div>
            </div>
            <div class="swiper-slide slide_5" data-swiper-autoplay="2000">
                <img src="<?= STATIC_IMAGES_PATH ?>/zodiac_img.jpg" class="image" alt="">
                <div class="middle">
                <a href="#">
                    <div class="text"> Login</div>
                 </a>
                </div>
            </div>
        </div>

        <!-- If we need navigation buttons -->
        <!--<div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
</div>   
</section> -->
    <div class="container">
        <div class="row">
        <div class="col-lg-12 col-sm-12">
                <h5 class="text-white-50" style="margin-left: 12px;">Quick Links</h5>
                <div class="row text-center">
                    <div class=" col-md-12 col-sm-12"> 
                        <ul class="nav footer_nav">
                            <!-- <li class="nav-item"><a href="<?= base_url("home") ?>" class="nav-link text-white-50">Home</a></li> -->
                            <!-- <li class="nav-item"><a href="https://CLover Rummy.blogspot.com" class="nav-link text-white-50">Blogs</a></li> -->
                            <li class="nav-item"><a href="<?= base_url() ?>about" class="nav-link text-white-50">About us</a></li>
                            <!-- <li class="nav-item"><a href="<?= base_url() ?>how_to_play" class="nav-link text-white-50">How to Play</a></li> -->
                            <!-- <li class="nav-item"><a href="<?= base_url() ?>promotions" class="nav-link text-white-50">Promotions</a></li> -->
                            <li class="nav-item"><a href="<?= base_url() ?>contact_us" class="nav-link text-white-50">Contact Us</a></li>
                            <!-- <li class="nav-item"><a href="<?= base_url() ?>withdraw_charges_info" class="nav-link text-white-50">Withdraw Charges</a></li> -->
                            <li class="nav-item"><a href="<?= base_url() ?>terms_of_service" class="nav-link text-white-50">Terms & Conditions</a></li>
                            <li class="nav-item"><a href="<?= base_url() ?>responsible_play" class="nav-link text-white-50">Responsible Play</a></li>
                            <li class="nav-item"><a href="<?= base_url() ?>privacy_policy" class="nav-link text-white-50">Privacy Policy</a></li>
                            <!-- <li class="nav-item"><a href="<?= base_url() ?>payment_method" class="nav-link text-white-50">Payment Method</a></li> -->
                            <!-- <li class="nav-item"><a href="<?= base_url() ?>refund_policy" class="nav-link text-white-50">Refund Policy</a></li> -->
                            <!-- <li class="nav-item"><a href="<?= base_url() ?>support" class="nav-link text-white-50">Support</a></li> -->
                            <li class="nav-item"><a href="<?= base_url('payment_method') ?>" class="nav-link text-white-50">Payment Method</a></li>
                            <li class="nav-item"><a href="<?= base_url("kyc_policy") ?>" class="nav-link text-white-50">KYC Policy</a></li>
                            <!-- <li class="nav-item"><a href="<?= base_url("faqs") ?>" class="nav-link text-white-50">FAQs </a></li> -->
                            <li class="nav-item"><a href="<?= base_url("sitemap") ?>" class="nav-link text-white-50">Sitemap</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
<hr style="background-color: white">
<div class="container my-5">
<div class="row footer_section">

        <div class="col-lg-4 col-xs-12 header_center">
            <b>Game Provider</b>
            <ul class="payment_part_foot">
                                <!-- <li class="banklogo1 lazy nl-loaded" style="background-image: url('<?php echo base_url("assets/images/razorpay.png") ?>');"></li> -->
                                <!-- <li><img src="<?= STATIC_IMAGES_PATH ?>/simpleLogo.png" style="width: 100px" alt=""></li> -->
                                <li><img src="<?= STATIC_IMAGES_PATH ?>/we_are_casino_black.png" class="we_are_casino" style="width: 200px;" alt=""></li>
    
            </ul>
        </div>
        <div class="col-lg-4 col-xs-12 header_center">
           <b>Certification</b> 
           <ul class="payment_part_foot">
                                <!-- <li class="banklogo1 lazy nl-loaded" style="background-image: url('<?php echo base_url("assets/images/razorpay.png") ?>');"></li> -->
                                <li><img src="<?= STATIC_IMAGES_PATH ?>/simpleLogo.png" style="width: 100px" alt=""></li>
                                <li class="ml-5"><img src="<?= STATIC_IMAGES_PATH ?>/ssl_cert.png" class="cert" style="height:40px" alt=""></li>
    
            </ul>
        </div>
        <div class="col-lg-4 col-xs-12 header_center">
            <b>Payment Methods</b> 
            <div class="row">
            <div class="col-12">
            <ul class="payment_part_foot payment_offers">
                                <!-- <li class="banklogo1 lazy nl-loaded" style="background-image: url('<?php echo base_url("assets/images/razorpay.png") ?>');"></li> -->
                                <li><img src="<?= STATIC_IMAGES_PATH ?>/astropay-card.png" style="width: 100px" alt=""></li>
                                <li class="ml-2"><img src="<?= STATIC_IMAGES_PATH ?>/skrill_logo_white.png" style="width: 100px" alt=""></li>
                                <li class="ml-2"><img src="<?= STATIC_IMAGES_PATH ?>/neteller-vector-logo.svg" style="width: 125px" alt=""></li>
            </ul>
            </div>
            </div>
            <div class="row">
            <div class="col-12">
            <ul class="payment_part_foot">
                                <!-- <li class="banklogo1 lazy nl-loaded" style="background-image: url('<?php echo base_url("assets/images/razorpay.png") ?>');"></li> -->
                                <li><img src="<?= STATIC_IMAGES_PATH ?>/cash_icon.svg" style="height: 60px" alt=""></li>
                                <li class="ml-2"><img src="<?= STATIC_IMAGES_PATH ?>/net_banking.svg" style="height: 50px; width: 125px" alt=""></li>
                                <li class="ml-2"><img src="<?= STATIC_IMAGES_PATH ?>/INOVAPAY-logo.png" style="width: 125px" alt=""></li>
            </ul>
            </div>
            </div>
        </div>
    </div>
</div>


        <div class="row">
            <div class="col-lg-12 col-sm-12 pr-5">
                <div class="row">
                    <div class="col-lg-3 col-xs-12">
                <a class="d-inline-block img-fluid"><img src="<?= STATIC_IMAGES_PATH ?>a2z_new.svg" class="img-responsive" height="100"></a></div>
                <div class="col-lg-9 col-xs-12">
                <p class="text-white-50 p-4 pt-4">Here at A2Z Betting, we provide a vivid gaming experience to our fellow wagerer enthusiasts. Come join our arena to play against the best, in a fair and safe play and win exciting rewards and cash prizes.
                </p>        </div>  </div>  </div>
            <!-- <div class="col-lg-4 col-sm-12">
                <h5 class="text-white-50" style="margin-left: 12px;">Quick Links</h5>
                <div class=" row">
                    <div class=" col-md-6 col-sm-6"> 
                        <ul class="nav">
                            <li class="nav-item"><a href="<?= base_url("home") ?>" class="nav-link text-white-50">Home</a></li>
                            <li class="nav-item"><a href="https://CLover Rummy.blogspot.com" class="nav-link text-white-50">Blogs</a></li>
                            <li class="nav-item"><a href="<?= base_url() ?>about" class="nav-link text-white-50">About us</a></li>
                            <li class="nav-item"><a href="<?= base_url() ?>how_to_play" class="nav-link text-white-50">How to Play</a></li>
                            <li class="nav-item"><a href="<?= base_url() ?>promotions" class="nav-link text-white-50">Promotions</a></li>
                            <li class="nav-item"><a href="<?= base_url() ?>contact_us" class="nav-link text-white-50">Contact Us</a></li>
                            <li class="nav-item"><a href="<?= base_url() ?>withdraw_charges_info" class="nav-link text-white-50">Withdraw Charges</a></li>
                        </ul>
                    </div>
                    <div class=" col-md-6 col-sm-6">
                        <ul class="nav">
                            <li class="nav-item"><a href="<?= base_url() ?>terms_of_service" class="nav-link text-white-50">Terms & Conditions</a></li>
                            <li class="nav-item"><a href="<?= base_url() ?>privacy_policy" class="nav-link text-white-50">Privacy Policy</a></li>
                            <li class="nav-item"><a href="<?= base_url() ?>refund_policy" class="nav-link text-white-50">Refund Policy</a></li>
                            <!--<li class="nav-item"><a href="<?= base_url() ?>support" class="nav-link text-balck-50">Support</a></li>-->
                            <!--<li class="nav-item"><a href="<?= base_url("legality") ?>" class="nav-link text-white-50">Legality</a></li>
                            <li class="nav-item"><a href="<?= base_url("faqs") ?>" class="nav-link text-white-50">FAQs </a></li>
                            <li class="nav-item"><a href="<?= base_url("sitemap") ?>" class="nav-link text-white-50">Sitemap</a></li>
                        </ul>
                    </div>
                </div>

            </div> -->


            <div class="col-lg-4">
                <div class="col-12 col-sm-12">
                    <div class="col_home3">
                        <!-- <h6 class="footer_head">Security &amp; Game Integrity</h6>
                        <div class="scrollBar">
                            <ul class="payment_part_foot">
                                <li><img class="forground-lazy" width="74" height="38" alt="Fraud Control" src="https://rcmg.in/rc/acquisition/landing-page-responsive/Fraud-Control.png" style="display: flex;">
                                </li>

                                <li class="last"><img class="forground-lazy" width="74" height="38" alt="18+" src="https://rcmg.in/rc/acquisition/landing-page-responsive/18.png" style="display: inline-block;">
                                </li>


                                <li><img class="forground-lazy" width="74" height="38" alt="100% Legal &amp; Secure" src="https://rcmg.in/rc/acquisition/landing-page-responsive/legal-Secure.png" style="display: inline-block;">
                                </li>

                            </ul>
                        </div> -->

                        <!-- <h6 class="footer_head">Our Payment Partners</h6> -->
                        <div class="scrollBar">
                            <!-- <ul class="payment_part_foot">
                                <!-- <li class="banklogo1 lazy nl-loaded" style="background-image: url('<?php echo base_url("assets/images/razorpay.png") ?>');"></li> -->
                               <!-- <li><img src="<?= STATIC_IMAGES_PATH ?>/astropay-card.png" style="width: 125px" alt=""></li>
                                <li class="ml-5"><img src="<?= STATIC_IMAGES_PATH ?>/skrill_logo_white.png" style="width: 125px" alt=""></li>
                                <li class="ml-5"><img src="<?= STATIC_IMAGES_PATH ?>/neteller-vector-logo.svg" style="" alt=""></li> -->


<!--                                <li class="banklogo4 lazy nl-loaded" style="background-image: url(&quot;https://rcmg.in/rc/acquisition/landingpage-icon-css-sprite-7.jpg&quot;);"></li>-->


                                <!--<li class="banklogo1 lazy nl-loaded" style="background-image: url(&quot;https://rcmg.in/rc/acquisition/landingpage-icon-css-sprite-7.jpg&quot;);"></li>
                                <li class="banklogo2 lazy nl-loaded" style="background-image: url(&quot;https://rcmg.in/rc/acquisition/landingpage-icon-css-sprite-7.jpg&quot;);"></li>
                                <li class="last banklogo3 lazy nl-loaded" style="background-image: url(&quot;https://rcmg.in/rc/acquisition/landingpage-icon-css-sprite-7.jpg&quot;);"></li>
                                <li class="banklogo4 lazy nl-loaded" style="background-image: url(&quot;https://rcmg.in/rc/acquisition/landingpage-icon-css-sprite-7.jpg&quot;);"></li>
                                <li class="last banklogo5 lazy nl-loaded" style="background-image: url(&quot;https://rcmg.in/rc/acquisition/landingpage-icon-css-sprite-7.jpg&quot;);"></li>-->
                            </ul>
                        </div>
                    </div>
                </div>

            </div>



        </div>
    </div>
</footer>
<section class="copyright">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-auto">
                <div class="h5 m-0">Follow Us On</div>
            </div>
            <div class="col">
                <ul class="nav nav-socials2">
                    <li class="nav-item">
                        <a href="https://www.facebook.com/A2Z Betting/" target="_blank" class="nav-link p-0 pr-2"><img height="40" src="<?= STATIC_IMAGES_PATH ?>/fb.png"></a>
                    </li>
                    <li class="nav-item">
                        <a href="https://twitter.com/A2Z Betting" target="_blank" class="nav-link p-0 pr-2"><img height="40" src="<?= STATIC_IMAGES_PATH ?>/tw.png"></a>
                    </li>

                    <li class="nav-item">
                        <a href="https://www.instagram.com/" target="_blank" class="nav-link p-0 pr-2"><img height="40" src="<?= STATIC_IMAGES_PATH ?>/in.png"></a>
                    </li>
                    <li class="nav-item">
                        <a href="https://www.youtube.com/channel/" target="_blank" class="nav-link p-0 pr-2"><img height="40" src="<?= STATIC_IMAGES_PATH ?>/ytb.png"></a>
                    </li>
                    <?php /*                    <li class="nav-item">
                      <a href="<?= PINTRESET_PAGE_LINK ?>" target="_blank" class="nav-link p-0 pr-2"><img height="40" src="<?= STATIC_IMAGES_PATH ?>/pin.png"></a>
                      </li> */ ?>

                </ul>
            </div>



            <div class="row align-items-center">
                <div class="col text-center">
                    <div class=" card-option ">
                        <div class="card-body" style="display: flex;">
                            <a href="<?php echo base_url("")?>terms_of_service" style="display: inline-flex;

                               color: #fff;">
                                <img src="<?= STATIC_IMAGES_PATH ?>/legal.svg" height="46" alt="">
                                <h3 class="m-0 mt-3"style=" font-size: 14px;"> &nbsp; 100% Legal</h3></a>
                        </div> 
                    </div>
                </div>
                <div class="col text-center">
                    <div class=" card-option ">
                        <div class="card-body" style="display: flex;">
                            <a href="<?php echo base_url("")?>responsible_play" style="display: inline-flex; color: #fff;">
                                <svg height="50" id="svg36" version="1.1" viewBox="0 0 467.39801 467.38" width="50" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:svg="http://www.w3.org/2000/svg"><defs id="defs40"/><g id="Lcd_With_Tablet_and_mobile_icon" transform="translate(-22.301,-22.31)"><g id="g32"><path d="m 256.001,22.31 c 129.121,0 233.698,104.911 233.698,233.69 0,129.113 -104.577,233.69 -233.698,233.69 -129.118,0 -233.7,-104.577 -233.7,-233.69 0,-128.779 104.582,-233.69 233.7,-233.69 z" id="path4" style="clip-rule:evenodd;fill:#fd8469;fill-rule:evenodd"/><path d="m 472.55,168.238 c 11.099,27.237 17.149,56.822 17.149,87.762 0,129.113 -104.577,233.69 -233.698,233.69 -4.366,0 -8.408,0 -12.774,-0.324 L 95.275,375.035 c 0,-37.658 -0.334,-80.031 -0.334,-117.689 0,-5.039 4.027,-9.076 8.742,-9.076 5.712,0 11.094,0 16.472,0 0,-43.05 0,-86.416 0,-129.8 l 287.843,-3.025 c 1.68,3.024 63.206,52.793 64.552,52.793 z" id="path6" style="clip-rule:evenodd;fill:#e4775f;fill-rule:evenodd"/><path d="m 319.893,355.862 h -115.34 c -4.366,0 -7.735,-3.35 -7.735,-7.386 v 0 c 0,-4.381 3.369,-7.73 7.735,-7.73 h 115.34 c 4.037,0 7.73,3.35 7.73,7.73 v 0 c 0,4.037 -3.693,7.386 -7.73,7.386 z" id="path8" style="clip-rule:evenodd;fill:#324a5e;fill-rule:evenodd"/><rect height="40.025002" id="rect10" style="clip-rule:evenodd;fill:#21313e;fill-rule:evenodd" width="61.873001" x="231.117" y="300.72101"/><rect height="201.08" id="rect12" style="clip-rule:evenodd;fill:#324a5e;fill-rule:evenodd" width="291.54099" x="116.457" y="115.444"/><rect height="160.71201" id="rect14" style="clip-rule:evenodd;fill:#ffffff;fill-rule:evenodd" width="251.18201" x="136.632" y="135.638"/><path d="m 415.041,259.369 h -63.885 v 100.53 h 63.885 z M 387.479,367.63 c 0,-2.682 -2.014,-4.695 -4.371,-4.695 -2.357,0 -4.371,2.014 -4.371,4.695 0,2.357 2.014,4.371 4.371,4.371 2.357,0 4.371,-2.014 4.371,-4.371 z M 372.687,252.984 c 0,0.668 0.334,1.002 1.002,1.002 h 18.829 c 0.335,0 1.003,-0.334 1.003,-1.002 0,-0.678 -0.668,-1.012 -1.003,-1.012 h -18.829 c -0.668,0.001 -1.002,0.335 -1.002,1.012 z m 48.414,1.336 v 113.31 c 0,4.714 -4.037,8.751 -8.741,8.751 h -58.502 c -4.715,0 -8.742,-4.037 -8.742,-8.751 V 254.32 c 0,-4.705 4.027,-8.732 8.742,-8.732 h 58.502 c 4.703,0 8.741,4.027 8.741,8.732 z" id="path16" style="clip-rule:evenodd;fill:#253645;fill-rule:evenodd"/><rect height="102.888" id="rect18" style="clip-rule:evenodd;fill:#ffffff;fill-rule:evenodd" width="65.238998" x="350.479" y="258.35699"/><path d="m 383.108,362.935 c -2.357,0 -4.371,2.014 -4.371,4.695 0,2.357 2.014,4.371 4.371,4.371 2.357,0 4.371,-2.014 4.371,-4.371 0,-2.682 -2.013,-4.695 -4.371,-4.695 z" id="path20" style="clip-rule:evenodd;fill:#ffffff;fill-rule:evenodd"/><path d="m 373.688,253.986 h 18.829 c 0.335,0 1.003,-0.334 1.003,-1.002 0,-0.678 -0.668,-1.012 -1.003,-1.012 h -18.829 c -0.668,0 -1.002,0.334 -1.002,1.012 0.001,0.668 0.335,1.002 1.002,1.002 z" id="path22" style="clip-rule:evenodd;fill:#ffffff;fill-rule:evenodd"/><path d="m 181.019,259.369 h -83.73 v 100.53 h 83.729 v -100.53 z m -62.205,-6.385 c 0,0.668 0.339,1.002 1.007,1.002 h 38.67 c 0.678,0 1.341,-0.334 1.341,-1.002 0,-0.678 -0.663,-1.012 -1.341,-1.012 h -38.67 c -0.668,0.001 -1.007,0.335 -1.007,1.012 z m 68.589,1.336 v 113.31 c 0,4.714 -4.023,8.751 -9.071,8.751 H 99.976 c -4.7,0 -8.737,-4.037 -8.737,-8.751 V 254.32 c 0,-4.705 4.037,-8.732 8.737,-8.732 h 78.356 c 5.048,0 9.071,4.027 9.071,8.732 z" id="path24" style="clip-rule:evenodd;fill:#253645;fill-rule:evenodd"/><path d="m 139.328,362.935 c -2.696,0 -4.371,2.014 -4.371,4.695 0,2.357 1.675,4.371 4.371,4.371 2.353,0 4.366,-2.014 4.366,-4.371 0,-2.682 -2.013,-4.695 -4.366,-4.695 z" id="path26" style="clip-rule:evenodd;fill:#ffffff;fill-rule:evenodd"/><path d="m 119.821,253.986 h 38.67 c 0.678,0 1.341,-0.334 1.341,-1.002 0,-0.678 -0.663,-1.012 -1.341,-1.012 h -38.67 c -0.668,0 -1.007,0.334 -1.007,1.012 0,0.668 0.339,1.002 1.007,1.002 z" id="path28" style="clip-rule:evenodd;fill:#ffffff;fill-rule:evenodd"/><rect height="100.53" id="rect30" style="clip-rule:evenodd;fill:#ffffff;fill-rule:evenodd" width="83.728996" x="97.289001" y="259.36899"/></g></g><g id="Layer_1" transform="translate(-22.301,-22.31)"/></svg>
                                <h3 class="m-0 mt-3"style=" font-size: 14px;">Responsible Play</h3></a>
                        </div>
                    </div>
                </div>
                <div class="col text-center">
                    <div class=" card-option">
                        <div class="card-body" style="display: flex;">
                            <a href="<?php echo base_url("")?>contact_us" style="display: inline-flex;

                               color: #fff;"><svg enable-background="new -27 24 100 100" height="50px" width="50" id="supportmale" version="1.1" viewBox="-27 24 100 100" width="100px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns" xmlns:xlink="http://www.w3.org/1999/xlink"><g><g><circle cx="23" cy="74" fill="#F5EEE5" r="50"/><g><defs><circle cx="23" cy="74" id="SVGID_1_" r="50"/></defs><clipPath id="SVGID_2_"><use overflow="visible" xlink:href="#SVGID_1_"/></clipPath><path clip-path="url(#SVGID_2_)" d="M38,99.9l27.9,7.7c3.2,1.1,5.7,3.5,7.1,6.6v9.8H-27v-9.8      c1.3-3.1,3.9-5.5,7.1-6.6L8,99.9V85h30V99.9z" fill="#E6C19C"/><g clip-path="url(#SVGID_2_)"><defs><path d="M38,99.9l27.9,7.7c3.2,1.1,5.7,3.5,7.1,6.6v9.8H-27v-9.8c1.3-3.1,3.9-5.5,7.1-6.6L8,99.9V85h30V99.9z" id="SVGID_3_"/></defs><clipPath id="SVGID_4_"><use overflow="visible" xlink:href="#SVGID_3_"/></clipPath><path clip-path="url(#SVGID_4_)" d="M23,102c-1.7,0-3.9-0.4-5.4-1.1c-1.7-0.9-8-6.1-10.2-8.3       c-2.8-3-4.2-6.8-4.6-13.3c-0.4-6.5-2.1-29.7-2.1-35c0-7.5,5.7-19.2,22.1-19.2l0.1,0l0,0l0,0l0.1,0       c16.5,0.1,22.1,11.7,22.1,19.2c0,5.3-1.7,28.5-2.1,35c-0.4,6.5-1.8,10.2-4.6,13.3c-2.1,2.3-8.4,7.4-10.2,8.3       C26.9,101.6,24.7,102,23,102L23,102z" fill="#D4B08C"/><path clip-path="url(#SVGID_4_)" d="M-27,82H73v42H-27V82z M23,112c11,0,20-6.3,20-14s-9-14-20-14S3,90.3,3,98       S12,112,23,112z" fill="#249CF2"/></g></g><path d="M23,98c-1.5,0-3.5-0.3-4.8-0.9c-1.6-0.7-7.2-4.6-9.1-6.3c-2.5-2.3-3.8-5.1-4.2-10S3,58.5,3,54.5     C3,48.8,8.1,40,23,40l0,0l0,0l0,0l0,0C37.9,40,43,48.8,43,54.5c0,4-1.5,21.5-1.9,26.4s-1.6,7.7-4.2,10c-1.9,1.7-7.6,5.6-9.1,6.3     C26.5,97.7,24.5,98,23,98L23,98z" fill="#F2CEA5"/><path d="M41,65.5c0,0,1.6,0.4,4.5-0.6c0.4-1.8,1.5-7,1.5-9.9s-0.3-5.7-1.9-8.1c-1.8-2.6-5.6-4.1-7.6-4.1     c-2.3,1.4-7.7,4.6-9.4,6.5c-0.9,1,0.4,1.8,0.4,1.8s1.2-0.5,1.7-0.6c2.5-0.7,6.3-0.3,8.4,1.6c2.4,2.2,2.4,7.4,2.5,8.8     C41,62.2,41,65.5,41,65.5z" fill="#D98C21"/><path d="M0.5,65c2.9,1,4.5,3.4,4.5,3.4s0-5.2,0-6.3c0-1.6,0-6.5,2.4-9.7c3.2-4.3,8.4-5.3,11.1-3.5     c1.4,0.9,6.1,5.5,11.1,1.7c3-2.3,8.5-7.5,8.5-7.5s-2.9-8.9-16.1-7.9c-5.6,0.5-11.8-0.9-11.8-0.9s-0.1,2.5,0.9,3.8     C2.8,40.4,0.1,46.4-0.7,51c-0.2,0.9-0.3,1.8-0.3,2.7c0,0.5,0,1,0,1.4C-1,58,0.1,63.1,0.5,65z" fill="#E6A422"/><path d="M30,84l-14,0c-0.4,0-0.6,0.4-0.4,0.8c1.3,2.3,3.7,3.5,6.7,3.7c3.1,0.2,6.5-1.3,8.1-3.7     C30.6,84.5,30.4,84,30,84z" fill="#A3705F"/><circle cx="32" cy="69" fill="#262626" r="2"/><circle cx="14" cy="69" fill="#262626" r="2"/><path d="M8,66c0,0,1.1-3,6.1-3c3.4,0,5.4,1.5,6.4,3" fill="none" stroke="#CC9872" stroke-width="2"/><path d="M38.1,66c0,0-1.1-3-6.1-3c-4.8,0-7,3-7,5c0,1.9,0,9,0,9" fill="none" stroke="#BB8660" stroke-width="2"/><path d="M46,61c0-12.7-10.3-23-23-23C10.3,38,0,48.3,0,61v8h3v-8c-0.1-11,8.9-20.1,20-20c11.1-0.1,20.1,9,20,20v9     h3V61z" fill="#6B363E"/><path d="M44,79.2C43.9,92.3,31,93,31,93" fill="none" stroke="#6B363E" stroke-width="2"/><path d="M25.3,91.9l5.8-1.6c1.1-0.3,2.2,0.3,2.4,1.4l0.5,1.9c0.3,1.1-0.3,2.2-1.4,2.4l-5.8,1.6     c-1.1,0.3-2.2-0.3-2.4-1.4l-0.5-1.9C23.6,93.3,24.3,92.2,25.3,91.9z" fill="#452228"/><path d="M5,82c-5.1-0.5-9-4.8-9-10s3.9-9.4,9-10V82z" fill="#6B363E"/><g><defs><path d="M5,82c-5.1-0.5-9-4.8-9-10s3.9-9.4,9-10V82z" id="SVGID_5_"/></defs><clipPath id="SVGID_6_"><use overflow="visible" xlink:href="#SVGID_5_"/></clipPath><rect clip-path="url(#SVGID_6_)" fill="#452228" height="20" width="7" x="-4" y="62"/></g><path d="M41,82c5.1-0.5,9-4.8,9-10s-3.9-9.4-9-10V82z" fill="#6B363E"/><g><defs><path d="M41,82c5.1-0.5,9-4.8,9-10s-3.9-9.4-9-10V82z" id="SVGID_7_"/></defs><clipPath id="SVGID_8_"><use overflow="visible" xlink:href="#SVGID_7_"/></clipPath><rect clip-path="url(#SVGID_8_)" fill="#452228" height="20" width="8" x="43" y="62"/></g></g></g></svg>
                                <h3 class="m-0 mt-3" style=" font-size: 14px;"> &nbsp; 24X7 Support</h3></a>
                        </div>
                    </div>
                </div>
            </div>




        </div>
        <div class="col-auto" style="text-align: center;
             border-top: 1px solid #000000;
             margin-top: 10px;">
            <div class="text-white-50" style="padding-top: 15px;"> A2Z Betting All rights reserved 2019<?= date("Y") > 2019 ? "-" . date("Y") : "" ?></div>
            <!-- <div  style="bottom: 0px; right: 0px; position: absolute;">
                developed by <span><a href="https://www.blocmatrix.com" target="_blank" style=" color: #00ff00; font-weight: bold;">Blocmatrix Pvt Ltd</a></span>
            </div> -->
        </div>
    </div>
</section>

<?php $this->load->view("includes/rps_model_popup"); ?>
<?php $this->load->view("includes/premimum_tournaments_popup"); ?>

</div>
<script src="<?= STATIC_JS_PATH ?>jquery.validate.min.js"></script>
<script src="<?= STATIC_JS_PATH ?>additional-methods.js"></script>
<script src="<?= STATIC_JS_PATH ?>sweetalert.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="//stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>
<script src="<?= STATIC_JS_PATH ?>jquery.datetimepicker.full.min.js"></script>
<script src="<?= STATIC_JS_PATH ?>swiper.min.js"></script>
<script src="<?= STATIC_JS_PATH ?>dirPagination.js"></script>
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/js/swiper.min.js"></script> -->
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/js/swiper.min.js"></script> -->
<script src="<?= STATIC_JS_PATH ?>loadingoverlay.min.js"></script>
<script type="text/javascript" src="<?= STATIC_JS_PATH ?>custom.js"></script>
<script src="//apis.google.com/js/platform.js" async defer></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<div id="comm100-button-1763"></div>
<?php if (is_logged_in()) { ?>
    <script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>profileCtrl.js?r=<?= time() ?>"></script>
    <script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>profileService.js?r=<?= time() ?>"></script>
    <script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>bonusAvailableCtrl.js?r=<?= time() ?>"></script>
    <script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>bonusAvailableService.js?r=<?= time() ?>"></script>


    <script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>gameTrackerCtrl.js?r=<?= time() ?>"></script>
    <script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>gameTrackerService.js?r=<?= time() ?>"></script>
<?php } ?>

<script>
    
  $(document).ready(function() {
// Swiper: Slider
    new Swiper('.swiper-container.flex_boxes', {
        loop: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 5,
        autoplay: true,
        paginationClickable: true,
        // spaceBetween: 20,
        breakpoints: {
            1920: {
                slidesPerView: 5,
                // spaceBetween: 30
            },
            1028: {
                slidesPerView: 3,
                // spaceBetween: 30
            }
        }
    });
});
</script>
</body>

</html>
