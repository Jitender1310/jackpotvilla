<!-- Edit email Popup Starts -->
<div class="modal" id="joinedGameList" data-backdrop='static' data-keyboard='false'>
    <div class="modal-dialog" style="max-width: 60%">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">My Joined Game List</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

                <div class="card mb-3" ng-repeat="item in joinedListTable track by $index" ng-if="joinedListTable.length > 0">
                    <div class="card-body">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <th colspan="2">Table id : {{item.tableId}}  {{item.responsePacket.gameTitle}}</th>
                                </tr>
                                <tr >
                                    <td colspan="2" ng-if="item.responsePacket.gameSubType == 'Points'">
                                        {{item.responsePacket.seats}} Players
                                        <br/>
                                        Point value: {{item.responsePacket.pointValue}} Chips
                                    </td>
                                    <td colspan="2" ng-if="item.responsePacket.gameSubType == 'Pool'">
                                        {{item.responsePacket.seats}} Players
                                        <br/>
                                        Pool Type: {{item.responsePacket.poolGameType}} 
                                        <br/>
                                        Prize: {{item.responsePacket.prizeAfterDeduction}} 
                                    </td>
                                    <td colspan="2" ng-if="item.responsePacket.gameSubType == 'Deals'">
                                        {{item.responsePacket.seats}} Players
                                        <br/>
                                        Deals: {{item.responsePacket.deals}} 
                                        <br/>
                                        Prize: {{item.responsePacket.prizeAfterDeduction}} 
                                    </td>
                                    <td>
                                        <button ng-click="takeSeat(item)" class="btn btn-success" type="button">TAKE SEAT</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card mb-3" ng-repeat="item in joinedListTournaments track by $index" ng-if="joinedListTournaments.length > 0">
                    <div class="card-body">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <th colspan="2">Tournament id : {{item.tableId}}  {{item.responsePacket.gameTitle}}</th>
                                </tr>
                                <tr ng-if="item.gameSubType == 'Points'">
                                    <td colspan="2">
                                        {{item.responsePacket.seats}} Players
                                        <br/>
                                        Point value: {{item.responsePacket.pointValue}} Chips
                                    </td>
                                    <td>
                                        <button ng-click="takeSeat(item)" class="btn btn-success" type="button">TAKE SEAT</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div ng-if="joinedListTable.length == 0 && joinedListTournaments.length == 0">
                    <div class="card-body">
                        <h3 class="text-center">You are not part of any ongoing games at this moment.</h3>
                    </div>
                </div>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
<!-- Edit Email POPUP Ends -->