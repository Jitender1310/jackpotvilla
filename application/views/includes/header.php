<!doctype html>
<html lang="en" ng-app="rummyApp" ng-cloak="" ng-init="GAME_SERVER_ID = '<?= GAME_SERVER_IP ?>'">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="<?php echo base_url("assets/images/a2z_fav.png") ?>">
        <link rel="stylesheet" href="<?= STATIC_CSS_PATH ?>bootstrap.css">
        <link rel="stylesheet" href="<?= STATIC_CSS_PATH ?>f5.css">
        <link rel="stylesheet" href="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/css/swiper.min.css">

        <link rel="stylesheet" href="<?= STATIC_CSS_PATH ?>animate.css">
        <link rel="stylesheet" href="<?= STATIC_CSS_PATH ?>custom.css?v=0.01">
        <link rel="stylesheet" href="<?= STATIC_CSS_PATH ?>jquery.datetimepicker.min.css">


        <!--For New testimonials css-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.transitions.css">
        <link rel="stylesheet" href="httpS://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="<?= STATIC_CSS_PATH ?>custome_sweetalert.css">

        <script src="<?= STATIC_JS_PATH ?>jquery.min.js"></script>
        <script src="<?= STATIC_JS_PATH ?>jquery.lazy.min.js"></script>
        <script src="<?= STATIC_JS_PATH ?>angular.min.js"></script>
        <script src="<?= STATIC_ANGULAR_PATH ?>app.js?r=<?= time() ?>"></script>
        <script src="<?= STATIC_JS_PATH ?>app_urls.js?r=<?= time() ?>"></script>

<!--<script src="<?= STATIC_JS_PATH ?>social_login.js?r=<?= time() ?>"></script>-->
        <script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>loginCtrl.js?r=<?= time() ?>"></script>
        <script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>commonCtrl.js?r=<?= time() ?>"></script>
        <script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>chipsService.js?r=<?= time() ?>"></script>
        <script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>loginService.js?r=<?= time() ?>"></script>
        <script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>statesService.js?r=<?= time() ?>"></script>
        <script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>playNowService.js?r=<?= time() ?>"></script>
        <script src="<?= STATIC_JS_PATH ?>sfs2x-api-1.7.11.js?r=<?= time() ?>"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/purl/2.3.1/purl.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
    var sfs;
        </script>

        <meta name="google-signin-client_id" content="820754122829-fbiqghtkddpe9tufs6r33l5bvb98c29n.apps.googleusercontent.com">


        <title><?= SITE_TITLE ?></title>
        <style>
            #loginForm span, em.invalid{
                color:red;
            }
            #registerForm em.invalid,  #registerForm span, #forgotPasswordForm em.invalid,  #forgotPasswordForm span{
                color:white;
            }
            #registerForm a, #forgotPasswordForm a{
                color: whitesmoke;
            }
        </style>
        <style type="text/css">
            .custom-pagination{text-align:center}.custom-pagination>.pagination{display:inline-block;padding-left:0;margin:20px 0;border-radius:4px}.custom-pagination>.pagination>li{display:inline}.custom-pagination>.pagination>li:first-child>a{margin-left:0;border-top-left-radius:4px;border-bottom-left-radius:4px}.custom-pagination>.pagination>li:last-child>a{border-top-right-radius:4px;border-bottom-right-radius:4px}.custom-pagination>.pagination>li>a:focus,.custom-pagination>.pagination>li>a:hover{z-index:2;color:#23527c;background-color:#eee;border-color:#ddd}.custom-pagination>.pagination>li>a{position:relative;float:left;padding:6px 12px;margin-left:-1px;line-height:1.42857143;color:#337ab7;text-decoration:none;background-color:#fff;border:1px solid #ddd}.custom-pagination>.pagination a{text-decoration:none!important;z-index:0!important}.custom-pagination>.pagination>.active>a,.custom-pagination>.pagination>.active>a:focus,.custom-pagination>.pagination>.active>a:hover{z-index:3;color:#fff;cursor:default;background-color:#337ab7;border-color:#337ab7}
        </style>
    </head>
    <body ng-controller="commonCtrl">

        <?php if ($this->uri->segment(1) == "game_lobby") { ?>
            <div class="onplrs">
                <div class="row no-gutters align-items-center">
                    <div class="col-auto"><i class="fas fa-users"></i></div>
                    <div class="col pl-2">
                        <small>Online players</small> <strong>{{onlinePlayers}}</strong>
                    </div>
                </div>
            </div>
        <?php } ?>

        <div class="mobile">
            <div class="d-flex h-100 align-items-center justify-content-center">
                <div class="text-center text-white p-4">
                    <img src="<?= STATIC_IMAGES_PATH ?>logo.svg" height="60">

                    <div class="my-4 h6">Download the <?= SITE_TITLE ?> mobile app on your Android Smartphones/Tablets and enjoy the game anywhere you like.</div>

                    <div class="row justify-content-center no-gutters">
                        <div class="col-4 col-sm-2 pr-1">
                        <!-- <a href="<?php echo base_url("clover_rummy.apk") ?>"> -->
                        <img class="img-fluid" src="<?= STATIC_IMAGES_PATH ?>APK-btn.png"></a></div>
                        <div class="col-4 col-sm-2 pr-1"><a href="<?= DOWNLOAD_ANDROID_APP_LINK ?>"><img class="img-fluid" src="<?= STATIC_IMAGES_PATH ?>android.png"></a></div>
                        <div class="col-4 col-sm-2 pl-1"><a href="<?= DOWNLOAD_IOS_APP_LINK ?>"><img class="img-fluid" src="<?= STATIC_IMAGES_PATH ?>apple.png"></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="desktop">
            <?php $this->load->view("includes/icons") ?>
            <div class="header-parent"><div class="header">
                    <div class="container" ng-controller="loginCtrl">
                        <form id="loginForm" ng-submit="doLogin()" ng-keyup="login_error = {}" autocomplete="off">
                            <div class="row align-items-center">
                                <div class=" col-md-3 col-xs-12 logo-section">
                                    <a href="<?= base_url() ?>home">
                                        <img src="<?= STATIC_IMAGES_PATH ?>a2z_new.svg" height="75">
                                    </a>
                                </div>

                                <div class="col-lg-6 col-xs-12 p-0">
                                    <div class="container mx-2 d-block mx-auto">
                                    <div id="carouselExampleInterval" class="carousel p-0 slide vertical mx-auto px-4" data-ride="carousel">
                                        <div class="carousel-inner d-block mx-auto">
                                        <div class="carousel-item active" data-interval="2000">
                                            <img src="<?= STATIC_IMAGES_PATH ?>/slide_1.jpg" class="img-responsive d-block w-100" alt="...">
                                        </div>
                                        <div class="carousel-item" data-interval="2000">
                                            <img src="<?= STATIC_IMAGES_PATH ?>/slide_2.jpg" class="img-responsive d-block w-100" alt="...">
                                        </div>
                                        <div class="carousel-item" data-interval="2000">
                                            <img src="<?= STATIC_IMAGES_PATH ?>/slide_3.jpg" class="img-responsive d-block w-100" alt="...">
                                        </div>
                                        <div class="carousel-item" data-interval="2000">
                                            <img src="<?= STATIC_IMAGES_PATH ?>/slide_4.jpg" class="img-responsive d-block w-100" alt="...">
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>

                                <div style="display: none">
                                    <div class="iconbox "style="margin-right:10px;" >
                                        <div class="iconbox--icon" style="top: 25%;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 350 350" style="enable-background:new 0 0 350 350;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
                                            <path d="M175,171.173c38.914,0,70.463-38.318,70.463-85.586C245.463,38.318,235.105,0,175,0s-70.465,38.318-70.465,85.587   C104.535,132.855,136.084,171.173,175,171.173z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#750bfe"/>
                                            <path d="M41.909,301.853C41.897,298.971,41.885,301.041,41.909,301.853L41.909,301.853z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#750bfe"/>
                                            <path d="M308.085,304.104C308.123,303.315,308.098,298.63,308.085,304.104L308.085,304.104z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#750bfe"/>
                                            <path d="M307.935,298.397c-1.305-82.342-12.059-105.805-94.352-120.657c0,0-11.584,14.761-38.584,14.761   s-38.586-14.761-38.586-14.761c-81.395,14.69-92.803,37.805-94.303,117.982c-0.123,6.547-0.18,6.891-0.202,6.131   c0.005,1.424,0.011,4.058,0.011,8.651c0,0,19.592,39.496,133.08,39.496c113.486,0,133.08-39.496,133.08-39.496   c0-2.951,0.002-5.003,0.005-6.399C308.062,304.575,308.018,303.664,307.935,298.397z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#750bfe"/>
                                            </g></g> </svg>
                                        </div>
                                        <input type="text" class="form-control form-control-sm border-0 shadow iconbox--input" placeholder="Username/Email/Mobile" name="username" ng-model="user.username">
                                        <span>{{login_error.username}}</span>
                                    </div>
                                    <div class="iconbox">
                                        <div class="iconbox--icon" style="top: 25%;"><svg height="512pt" viewBox="0 0 512 512.0005" width="512pt" xmlns="http://www.w3.org/2000/svg"><path d="m472.523438 81.902344-21.214844-21.210938-40.65625 12.375-12.375 40.660156 21.210937 21.210938c23.394531 23.394531 23.394531 61.460938 0 84.851562-19.742187 19.746094-49.941406 22.824219-72.945312 9.242188 16.253906-17.65625 15.828125-45.148438-1.300781-62.273438l-76.566407 28.285157-255.515625 303.796875c17.546875 17.546875 46.09375 17.546875 63.636719 0l29.984375-29.980469 42.425781 42.425781 63.636719-63.636718-42.425781-42.425782 21.214843-21.214844 42.425782 42.425782 63.640625-63.640625-42.425781-42.425781 37.53125-37.53125c52.953124 42.371093 130.667968 39.039062 179.71875-10.011719 52.636718-52.636719 52.636718-138.285157 0-190.921875zm0 0" fill="#4f00b6"/><path d="m430.097656 39.476562c-52.636718-52.636718-138.285156-52.636718-190.921875 0-49.050781 49.050782-52.382812 126.765626-10.011719 179.722657l-216.003906 216.003906c-17.546875 17.542969-17.546875 46.09375 0 63.636719l268.359375-268.359375s63.726563-63.722657 63.726563-63.722657c-17.128906-17.125-44.621094-17.554687-62.273438-1.300781-13.585937-23.003906-10.507812-53.199219 9.238282-72.945312 23.394531-23.394531 61.460937-23.394531 84.851562 0l21.214844 21.214843 53.03125-53.035156zm0 0" fill="#4f00b6"/></svg></div>
                                        <input type="password" class="form-control form-control-sm border-0 shadow iconbox--input" placeholder="Password"  name="password" ng-model="user.password">
                                        <span>{{login_error.password}}</span>
                                    </div>
                                </div>
                                <div class=" col-md-3 col-xs-12 ">
                                <button type="submit" ng-disabled="loginBtn" class="btn btn-theme btn-block shadow" style="margin-bottom: 5px; padding: 2px 0; margin-top: 5px;">Register</button>
                                    <button type="submit" ng-disabled="loginBtn" class="btn btn-theme btn-block shadow" style="margin-bottom: 5px; padding: 2px 0; margin-top: 5px;">Login</button>
                                    <a href="<?= base_url() ?>forgot_password" class="d-block text-white-50 font-weight-bold" style="font-weight: lighter !important; text-align:center">Forgot Password ?</a>
                                </div>
                            </div>
                        </form>

                    </div>
                    </div>
                    </div>
