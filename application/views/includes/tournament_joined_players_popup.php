<div class="modal" id="tournamentJoinedPlayersPopup" data-backdrop='static' data-keyboard='false'>
    <div class="modal-dialog modal-sm" style="max-width: 50%">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tournament Joined Players</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Player name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="jp in tournamentDetailsObj.joined_players_list track by $index">
                                        <td>{{$index+1}}</td>
                                        <td>{{jp.player_name}}</td>
                                    </tr>
                                    
                                    <tr ng-if="tournamentDetailsObj.joined_players_list.length==0">
                                        <td colspan="2">
                                            <h5 class="text-center">No players joined, Be the first one Join in this tournament
                                                <br/>
                                                <button type="button" ng-click="joinTournament(tournamentDetailsObj.cloned_tournaments_id)" class="btn btn-success">Join Now</button>
                                            </h5>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>