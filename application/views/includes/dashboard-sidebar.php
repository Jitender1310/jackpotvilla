<div class="col-lg-3 col-md-3 col-xs-12 p-0 my-2">
    <div class="card">
        <ul class="nav flex-column d-menu">
            <li class="nav-item"><a class="nav-link <?= $page_active == "account" ? "active" : "" ?>" href="<?= base_url() ?>account">ACCOUNT DETAILS</a></li>
            <li class="nav-item"><a class="nav-link <?= $page_active == "account_overview" ? "active" : "" ?>" href="<?= base_url() ?>account_overview">ACCOUNT OVERVIEW</a></li>
            <li class="nav-item"><a class="nav-link <?= $page_active == "deposit" ? "active" : "" ?>" href="<?= base_url() ?>deposit">DEPOSIT CASH</a></li>
            <li class="nav-item"><a class="nav-link <?= $page_active == "payment_history" ? "active" : "" ?>" href="<?= base_url() ?>payment_history">DEPOSIT HISTORY</a></li>
            <li class="nav-item"><a class="nav-link <?= $page_active == "transactions" ? "active" : "" ?>" href="<?= base_url() ?>transactions">VIEW TRANSACTION</a></li>
            <li class="nav-item"><a class="nav-link <?= $page_active == "bonus_transactions" ? "active" : "" ?>" href="<?= base_url() ?>bonus_transactions">BONUS TRANSACTION</a></li>
            <!-- <li class="nav-item"><a class="nav-link <?= $page_active == "rps_transactions" ? "active" : "" ?>" href="<?= base_url() ?>rps_transactions">RPS TRANSACTION</a></li> -->
            <li class="nav-item"><a class="nav-link <?= $page_active == "withdraw" ? "active" : "" ?>" href="<?= base_url() ?>withdraw">WITHDRAW CASH</a></li>
            <li class="nav-item"><a class="nav-link <?= $page_active == "preferences" ? "active" : "" ?>" href="<?= base_url() ?>preferences">PREFERENCES</a></li>
            <!--<li class="nav-item"><a class="nav-link" href="uploaded-docs.php">UPLOADED DOCUMENTS</a></li>-->
            <?php /* <li class="nav-item"><a class="nav-link <?= $page_active == "game_tracker" ? "active" : "" ?>" href="<?= base_url() ?>game_tracker">MY GAME TRACKER</a></li> */?>
            <!--<li class="nav-item"><a class="nav-link <?= $page_active == "bonus_available" ? "active" : "" ?>" href="<?= base_url() ?>bonus_available">BONUS AVAILABLE</a></li>-->
            <li class="nav-item"><a class="nav-link <?= $page_active == "level_info" ? "active" : "" ?>" href="<?= base_url() ?>level_info">LEVEL INFO</a></li>
        </ul>
    </div>
</div> 