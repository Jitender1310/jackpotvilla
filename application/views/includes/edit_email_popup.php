<!-- Edit email Popup Starts -->
<div class="modal" id="editEmailModal" data-backdrop='static' data-keyboard='false'>
    <div class="modal-dialog">
        <form ng-submit="updateNewEmail()" id="editEmailForm" ng-keyup="ep_error = {}">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title"style="color: #000;">Change Email Id</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Enter your Email Id: <span class="text-danger">*</span></label>
                            <input type="text"  class="form-control" placeholder=""  name="firstname" ng-model="editProfileObj.email">
                            <span class="text-danger">{{ep_error.email}}</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary text-white" type="submit"><b>Update</b></button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Edit Email POPUP Ends -->