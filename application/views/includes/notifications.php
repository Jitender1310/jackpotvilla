<!-- Edit email Popup Starts -->
<div class="modal" id="offerNotifications" data-backdrop='static' data-keyboard='false'>
    <div class="modal-dialog" style="max-width: 60%">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-balck-50">Offer Notifications</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" style="height: 600px;overflow-y:scroll">
                <div class="card mb-3" ng-repeat="item in notificationsList track by $index" ng-if="notificationsList.length > 0">
                    <div class="card-body">
                        <h5 class="card-title">{{item.title}}</h5>
                        <a href="{{item.web_link}}"><img class="card-img-top" src="{{item.image}}" alt="Notification"></a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
<!-- Edit Email POPUP Ends -->