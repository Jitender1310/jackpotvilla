<?php $this->load->view('includes/menu-dashboard') ?>
<section class="innerpages py-5">
    <div class="container">
		<?php $this->load->view('chips') ?>
        <div class="row mt-4">
            <div class="col-3">
                <div class="card h-100  ">
                    <ul id="submenu">
                        <li><a href="">CHAMPIONSHIP</a></li>
                        <li><a href="">THE RUMMY FEST</a></li>
                        <li><a href="">KYC VERIFICATION</a></li>
                        <li><a href="">FREE TOURNAMENTS</a></li>
                        <li><a href="">ENTRY FEE TOURNAMENTS</a></li>
                        <li><a href="">BONUSES</a></li>
                        <li><a href="">MOBILE APP</a></li>
                        <li><a href="">BRING A FRIEND</a></li>
                        <li><a href="">SUPPORT</a></li>

                    </ul>
                </div>
            </div>
            <div class="col-9">
                <div class="card h-100 card-gold">
                    <div class="card-header">CHAMPIONSHIP</div>
                    <div class="card-body">
                        <img src="<?= STATIC_IMAGES_PATH ?>champ.png" width="100%" class="mb-3">
                        <h6>Rummy</h6>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                            has been the industry's standard dummy text ever since the 1500s, when an unknown printer
                            took a galley of type and scrambled it to make a type specimen book. It has survived not
                            only five centuries, but also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset sheets containing
                            Lorem Ipsum passages, and more recently with desktop publishing software like Aldus
                            PageMaker including versions of Lorem Ipsum.</p>
                        <h6>Terms and Conditions</h6>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                            has been the industry's standard dummy text ever since the 1500s, when an unknown printer
                            took a galley of type and scrambled it to make a type specimen book. It has survived not
                            only five centuries, but also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset sheets containing
                            Lorem Ipsum passages, and more recently with desktop publishing software like Aldus
                            PageMaker including versions of Lorem Ipsum.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                            has been the industry's standard dummy text ever since the 1500s, when an unknown printer
                            took a galley of type and scrambled it to make a type specimen book. It has survived not
                            only five centuries, but also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset sheets containing
                            Lorem Ipsum passages, and more recently with desktop publishing software like Aldus
                            PageMaker including versions of Lorem Ipsum.</p>
                        <h6>Terms and Conditions</h6>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                            has been the industry's standard dummy text ever since the 1500s, when an unknown printer
                            took a galley of type and scrambled it to make a type specimen book. It has survived not
                            only five centuries, but also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset sheets containing
                            Lorem Ipsum passages, and more recently with desktop publishing software like Aldus
                            PageMaker including versions of Lorem Ipsum.</p>





                    </div>


                    <div class="embed-responsive embed-responsive-21by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Gij_jOGHDIo"></iframe>
                    </div>




                </div>
            </div>
        </div>
    </div>
</section>
