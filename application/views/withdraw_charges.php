<?php if (is_logged_in()) { ?>
    <?php $this->load->view('includes/menu-dashboard') ?>
<?php } ?>

<?php if (!is_logged_in()) { ?>
    <?php $this->load->view('menu') ?>
<?php } ?>
<section class="innerpages py-4">
    <div class="container">
        <div class="row">
            <div class="col-12 text-white">

                <div class="editor">
                    <h5>Withdraw Charges</h5>
                    <?= $content ?>
                </div>
            </div>
        </div>
    </div>
</section>