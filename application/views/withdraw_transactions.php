<?php $this->load->view('includes/menu-dashboard') ?>
<section class="innerpages py-5 " ng-controller="withdrawCtrl">
    <div class="container">
        <div class="withdraw-outer">
        <div class="row mt-4">
            <?php $this->load->view('includes/dashboard-sidebar') ?>
            <div class="col-lg-9 col-xs-12 my-2 p-0 pl-2">
                <div class="card card-gold">
                    <div class="card-header py-2 card-header2 text-white">
                        <div class="row align-items-center">
                            <div class="col-6">VIEW WITHDRAWAL REQUESTS</div>
                            <div class="col-auto">
                                <a class="btn btn-light" href="<?= base_url() ?>withdraw">WITHDRAW CASH</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive table_container">
                    <table class="table  mb-0">
                        <thead class="bg-secondary text-white">
                            <tr>
                                <th>S.No</th>
                                <th>Ref ID</th>
                                <th>Date</th>
                                <th>Amount</th>
                                <th>Details</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tr ng-repeat="item in requestsList track by $index">
                            <td>{{requestsListPagination.initial_id + $index}}</td>
                            <td>{{item.withdrawal_id}}</td>
                            <td>{{item.request_date_time}}</td>
                            <td>{{item.request_amount}}</td>
                            <td>
                                {{item.transfer_type}},
                                {{item.transfer_to}}
                                <span ng-if="item.transfer_to == 'PAYTM'">
                                    <br/>
                                    {{item.paytm_mobile_number}},
                                    <br/>
                                    {{item.paytm_person_name}}
                                </span>

                                <span ng-if="item.transfer_to != 'PAYTM'">
                                    <br/>
                                    {{item.account_number}},
                                    <br/>
                                    {{item.account_holder_name}},
                                    <br/>
                                    {{item.ifsc_code}}
                                </span>
                            </td>
                            <td>
                                <span class="{{item.transaction_bootstrap_class}}">{{item.request_status}}</span>
                                <br ng-if="item.request_status == 'Pending'"/>
                                <button class="btn btn-danger btn-sm" ng-if="item.request_status == 'Pending'" type="button" ng-click="CancleWithdrawRequest(item)">Cancel</button>
                            </td>
                        </tr>

                    </table>
                    </div>
                    <div ng-if="requestsList.length == 0">
                        <h4 class="text-center" style="padding:20px;">No data found</h4>
                    </div>
                    <div class="custom-pagination" ng-bind-html="requestsListPagination.pagination"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>withdrawCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>withdrawService.js?r=<?= time() ?>"></script>