<?php $this->load->view('includes/menu-dashboard') ?>
<section class="innerpages py-5" ng-controller="accountOverViewCtrl">
    <div class="container">
        <div class="overview-outer">
        <div class="row mt-4">
            <?php $this->load->view('includes/dashboard-sidebar') ?>
            <div class="col-lg-9 col-md-9 col-xs-12 my-2 p-0 px-2"> 
                <div class="card card-gold p-0">
                    <div class="card-header card-header2 text-white">ACCOUNT DETAILS</div>
                    <div class="card-body">
                        <div class="row h5 font-weight-normal mb-5">
                            <div class="col col-xs-12"><strong>User Name:</strong> <?= $player_details->username ?></div>
                            <div class="col-lg-auto col-xs-12">
                                <span> 
                                    <?php if($player_details->kyc_status=="Verified"){?>
                                        KYC verified 
                                        <i class="fal fa-check ml-1 text-success"></i>
                                    <?php }else{?>
                                        KYC verification 
                                        <i class="fal fa-exclamation-triangle ml-1 text-danger"></i>
                                    <?php }?>
                                </span>
                            </div>
                        </div>
                        <div class="bordered-frame">
                           <?php /* <div class="bf-label"><span>Cash Account</span></div> */?>
                            <div class="bf-body">
                                <div class="row">
                                    <div class="col-lg-6 col-xs-12 right_side my-3 p-0">
                                        <div class="row align-items-end">
                                            <div class="col">Deposit Balance</div>
                                            <div class="col-lg-4 col-xs-12">
                                                <div class="digits">{{acObj.deposit_balance}}</div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-xs-12 left_side my-3">
                                        <div class="row align-items-end">
                                            <div class="col">Reward Points</div>
                                            <div class="col-lg-4 col-xs-12">
                                                <div class="digits">{{acObj.reward_points}}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-xs-12 right_side my-3">
                                        <div class="row align-items-end">
                                            <div class="col">Withdrawal Balance</div>
                                            <div class="col-lg-4 col-xs-12">
                                                <div class="digits">{{acObj.withdrawal_balance}}</div>
                                            </div>

                                        </div>
                                    </div>
<!--                                    <div class="col-6 pl-5 my-3">
                                        <div class="row align-items-end">
                                            <div class="col">Level 1 Status</div>
                                            <div class="col-4">
                                                <div class="digits">{{acObj.withdrawal_balance}}</div>
                                            </div>
                                        </div>
                                    </div>-->
                                    <div class="col-lg-6 col-xs-12 right_side my-3">
                                        <div class="row align-items-end">
                                            <div class="col">Total Cash Balance</div>
                                            <div class="col-lg-4 col-xs-12">
                                                <div class="digits">{{acObj.total_cash_balance}}</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="bordered-frame">
                            <?php /*<div class="bf-label"><span>Practice account</span></div> */?>
                            <div class="bf-body">
                                <div class="row">
                                    <div class="col-lg-6 col-xs-12 right_side my-3">
                                        <div class="row align-items-end">
                                            <div class="col">Practice Chips: {{acObj.practice_account_chips}} chips</div>                         
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="bordered-frame">
                            <?php /*<div class="bf-label"><span>Bonus account</span></div>*/?>
                            <div class="bf-body">
                                <div class="row">
                                    <div class="col-lg-6 col-xs-12">
                                        <div class="row align-items-end">
                                            <div class="col">Released Bonus </div>
                                            <div class="col-lg-4 col-xs-12">
                                                <div class="digits">{{acObj.release_bonus}}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-xs-12">
                                        <div class="row align-items-end">
                                            <div class="col">Pending Bonus</div>
                                            <div class="col-lg-4 col-xs-12">
                                                <div class="digits">{{acObj.pending_bonus}}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-xs-12">
                                        <div class="row align-items-end">
                                            <div class="col">Active Bonus</div>
                                            <div class="col-lg-4 col-xs-12">
                                                <div class="digits">{{acObj.active_bonus}}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<script src="<?= STATIC_ANGULAR_CTRLS_PATH ?>accountOverViewCtrl.js?r=<?= time() ?>"></script>
<script src="<?= STATIC_ANGULAR_SERVICES_PATH ?>accountOverViewService.js?r=<?= time() ?>"></script>