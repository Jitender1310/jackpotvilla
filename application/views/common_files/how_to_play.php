<div class="editor">
<div class="pagebanner mb-4" style="background:url(<?= STATIC_IMAGES_PATH ?>HOWTOPLAY-01.png)"></div>


<ul>
<li>The game can be played between two players or there can be 2-6 players in one single game of rummy online.</li>
<li>The distribution of the cards will be more depended on the number of players taking part in the game.  </li>
<li>The cards will be evenly distributed between / among the players. </li>
<li>The remaining deck will be kept in the middle.</li>
<li>The top card will be flipped up (which will set the discard pile). </li>
<li>Players need to play all of the cards into melds (A meld is a set of 3-4 cards which will be of the same face value).</li>
<li>The players will have to retrieve the cards from the deck / discard pile.</li>
<li>The players will have to try at forming the meld with the available cards and try to discard unimportant cards.</li>
<li>Each player will have the turn to discard one card.</li>
<li>At the time of discarding the card, the players can also have the option to lay down any possible meld on the table. </li>
<li>The other players can have the option to add to the meld (previously laid down by the player). </li>
<li>The game will be continued until the players have discarded their cards.</li>
<li>The winner will be given points on the basis of the face value of the cards of the other players.</li>
<li>If the player has not laid down the card on the table but is able to lay off in a meld all together then it is known as GOING RUMMY.</li>
</ul>
<p>When a player goes rummy, he is awarded double the points in the other players’ hands.</p>
<h5 id="1">What is a Sequence?</h5>
<p>
Rummy players will have to create the sequence.  It is needed to arrange the cards into sets of 3 or 4 consecutive cards of the same suit.
</p>
<p>
Players will be creating a pure sequence / impure sequence. 
</p>
<p>A pure sequence is a set of 3-4 consecutive cards of the same suit. A player cannot use a joker or a wild card while creating a pure sequence.</p>
<p>2<span class="fas fa-spade text-warning mx-1"></span>3<span class="fas fa-spade text-warning mx-1"></span>4<span class="fas fa-spade text-warning mx-1"></span>5<span class="fas fa-spade text-warning mx-1"></span> is a pure sequence of four cards.</p>
<p>3<span class="fas fa-heart text-danger mx-1"></span>4<span class="fas fa-heart text-danger mx-1"></span>5<span class="fas fa-heart text-danger mx-1"></span>  is a pure sequence of three cards.</p>
<p>An impure sequence is a group of 3-4 cards while using one or more jokers.</p>
<p>3<span class="fas fa-diamond text-info mx-1"></span>4<span class="fas fa-diamond text-info mx-1"></span>Q<span class="fas fa-diamond text-info mx-1"></span>6<span class="fas fa-diamond text-info mx-1"></span> is an impure sequence of four cards where Q<span class="fas fa-diamond text-info mx-1"></span> is used as a wild</p>
<p>joker for 5<span class="fas fa-diamond text-info mx-1"></span></p>
<p>2<span class="fas fa-heart text-danger mx-1"></span>3<span class="fas fa-heart text-danger mx-1"></span>Q<span class="fas fa-heart text-danger mx-1"></span> is an impure sequence of three cards where Q<span class="fas fa-heart text-danger mx-1"></span>  is used as a wild joker for</p>
<p>4<span class="fas fa-heart text-danger mx-1"></span></p>

<h5 id="2">How to form a set?</h5>
<p>A set is a group of 3-4 cards of the same value but from different suits. A set
cannot be formed if the same numbered card belongs to the same suit.</p>
<p>2<span class="fas fa-heart text-danger mx-1"></span>2<span class="fas fa-spade text-warning mx-1"></span>2<span class="fas fa-diamond text-info mx-1"></span>2<span class="fas fa-club text-success mx-1"></span> is a set of 2’s from four different suits.</p>
<p>3<span class="fas fa-spade text-warning mx-1"></span>3<span class="fas fa-diamond text-info mx-1"></span>3<span class="fas fa-heart text-danger mx-1"></span> is a set of 3’s from three different suits.</p>
<p>6<span class="fas fa-heart text-danger mx-1"></span>6<span class="fas fa-spade text-warning mx-1"></span>6<span class="fas fa-diamond text-info mx-1"></span>Q<span class="fas fa-club text-success mx-1"></span> is a set of 6’s, with Q<span class="fas fa-club text-success mx-1"></span>  being the wild card to replace 6<span class="fas fa-club text-success mx-1"></span></p>
<p>8<span class="fas fa-heart text-danger mx-1"></span>8<span class="fas fa-spade text-warning mx-1"></span>Q<span class="fas fa-club text-success mx-1"></span>  is a set of 8’s with Q<span class="fas fa-club text-success mx-1"></span>  the wild card to replace 8<span class="fas fa-club text-success mx-1"></span></p>


<!-- <h5 id="3">POINTS RUMMY</h5>
<p>In points rummy, 2 to 6 players can play the game. Typically, it only lasts for about 2 or 3 minutes. The decks of cards used are dependant on the type of table chosen.
</p>
<p>The game lasts only for one deal. The points have a predetermined value. In order to win, the player must lay down a pure sequence, an impure sequence and the rest into melds. The first person to discard all of his cards wins all the cash brought to the table.</p>
<h6>Calculation of points:</h6>
<ul>
<li>2,3,4,5,6,7,8 and 9 of any suit carry their face value.</li>
<li>Ace, Jack, Queen and King of any suit carry 10 points.</li>
<li>The winner gains the sum of the value of the remaining cards of the other players. This is the reason why the players must hold onto cards with the lowest face value.</li>
</ul>
<h5 id="4">DEAL RUMMY</h5>
<p>In deal rummy, the players decide to play on a fixed number of deals. Each player is allotted a certain number of chips. The game play is similar to that of points rummy. The number of decks of cards used depends on the table. The winner gets the chips lost by the other players. At the end of each deal, the winner gets the chips according to the value of the cards his opponent owns.
</p>
<h6>
Calculation of points:
</h6>
<ul>
<li>Face cards carry their face value.</li>
<li>Ace, Jack, Queen and King of any suit carry 10 points.</li>
</ul>
<h5 id="5">POOL RUMMY</h5>
<p>Pool Rummy has two variations, 101 pool rummy and 201 pool rummy. 101 and 201 points are the predetermined points of elimination. The player reaching the points of 101 and 201 in the respective games must leave the game and the player who remains at the table with the least amount of points at the end is the winner. The winner is credited with the entry fee paid by all the players.
</p> -->

</div>