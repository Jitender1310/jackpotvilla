<form id="withdrawl_form" ng-submit="ValidateWithdrawlRequest();" ng-keyup="ep_error = {}">
    <!-- <div class="my-4">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" id="neft" name="customRadioInline1" class="custom-control-input" checked>
            <label class="custom-control-label" for="neft"><img src="<?= STATIC_IMAGES_PATH ?>neft.png" height="50" class="rounded"></label>
        </div>
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" id="instant" name="customRadioInline1" class="custom-control-input">
            <label class="custom-control-label" for="instant"><img src="<?= STATIC_IMAGES_PATH ?>instant.png" height="50" class="rounded"></label>
        </div>
    </div> -->
    <div class="form-group">
        <label class="text-white-50">Withdrawal Type <span class="text-danger">*</span></label>
        <select id="transfer_type" name="transfer_type" ng-model="withdrawObj.transfer_type" required class="form-control text-balck-50">
            <option value="IMPS">IMPS or Paytm</option>
            <option>NEFT</option>
        </select>
        <span class="text-danger">{{ep_error.transfer_type}}</span>
    </div>

    <div class="form-group" ng-if="withdrawObj.transfer_type == 'IMPS'">
        <label class="text-white-50">Transfer To <span class="text-danger">*</span></label>
        <select id="instant_withdrawal_type" name="transfer_to" ng-model="withdrawObj.transfer_to" required class="form-control text-balck-50">
            <option value="">--Choose--</option>
            <option>PAYTM</option>
            <option>BANK TRANSFER</option>
        </select>
        <span class="text-danger">{{ep_error.transfer_to}}</span>
    </div>

    <div ng-if="withdrawObj.transfer_to === 'PAYTM' && withdrawObj.transfer_type === 'IMPS'">
        <div class="form-group">
            <label class="text-white-50">Paytm mobile number <span class="text-danger">*</span></label>
            <input type="number" ng-model="withdrawObj.paytm_mobile_number" id="paytm_mobile_number" name="paytm_mobile_number" class="form-control number text-balck-50">
            <span class="text-danger">{{ep_error.paytm_mobile_number}}</span>
        </div>
        <div class="form-group">
            <label class="text-white-50">Paytm person name <span class="text-danger">*</span></label>
            <input type="text" ng-model="withdrawObj.paytm_person_name" name="paytm_person_name" class="form-control text-balck-50">
            <span class="text-danger">{{ep_error.paytm_person_name}}</span>
        </div>
    </div>
    <div ng-if="withdrawObj.transfer_to === 'BANK TRANSFER' || withdrawObj.transfer_type == 'NEFT'">
        <div class="form-group">
            <label class="text-white-50">Enter account number <span class="text-danger">*</span></label>
            <input type="password" ng-model="withdrawObj.account_number" id="account_number" name="account_number" class="form-control number text-balck-50">
            <span class="text-danger">{{ep_error.account_number}}</span>
        </div>
        <div class="form-group">
            <label class="text-white-50">Re-enter account number  <span class="text-danger">*</span></label>
            <input type="text" ng-model="withdrawObj.confirm_account_number" name="confirm_account_number" class="form-control number text-balck-50">
        </div>
        <div class="form-group">
            <label class="text-white-50">Account Holder Name <span class="text-danger">*</span></label>
            <input type="text" ng-model="withdrawObj.account_holder_name" name="account_holder_name" class="form-control text-balck-50">
        </div>
        <div class="form-group">
            <label class="text-white-50">IFSC Code  <span class="text-danger">*</span></label>
            <input type="text" ng-model="withdrawObj.ifsc_code" name="ifsc_code" class="form-control text-balck-50" ng-blur="getIfscDetails()" style="text-transform: uppercase" minlength="11" maxlength="11">
            <span class="text-danger"><p style="color: red;">{{ep_error.ifsc_code}}</p></span>

            <span class="text-balck-50">{{withdrawObj.branch_name}} {{withdrawObj.bank_name}}</span>
        </div>
    </div>
    <div class="form-group">
        <label class="text-white-50">Enter Amount to Withdraw <span class="text-danger">*</span></label>
        <input type="number" name="request_amount" ng-model="withdrawObj.request_amount" class="form-control number text-balck-50 numberOnly">
        <span class="text-danger">{{ep_error.request_amount}}</span>
    </div>
    <div class="col-auto" align="center">
        <button type="submit" class="btn btn-warning font-weight-bold">Send Request</button>
    </div>
</form>

<?php $this->load->view("includes/withdrawal_modal_popup") ?>