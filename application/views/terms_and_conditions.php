<?php if (is_logged_in()) { ?>
    <?php $this->load->view('/includes/menu-dashboard') ?>
<?php } ?>

<?php if (!is_logged_in()) { ?>
    <?php $this->load->view("/includes/header"); ?>
    <?php include'menu.php'; ?>
<?php } ?>

<section class="innerpages py-5">
    <div class="container">
        <div class="row" style=" background: #fff; padding: 10px 0;">
            <div class="col-12 text-white ">
                <div class="editor">
                    <h5 class="text-black1" style=" margin-bottom: 0;">TERMS AND CONDITIONS: </h5>

                    <p>The Terms of Service refer to rules and regulations of A2Z Betting that the user must abide by in order to avail our services. The user is expected to read all the terms and conditions put down by A2Z Betting before registering with us. Our terms of service are in adherence with the government of India. If you believe that one or more aspects of the terms of service are unlawful or invalid, the enforceability of the rest of the terms remain valid. A user unsure of the terms or one who isn’t in compliance with our terms may stop using the features of the website.
                    </p>

                    <h6>Applicability:</h6> <p>Before registering with us, please read and review the terms provided by A2Z Betting carefully, which are applicable to all services provided on the website And also the product- specific conditions and rules applicable to specific offerings.
                    </p>

                    <h6>Eligibility:</h6> <p>A person who is of age 18 years or more is eligible to play rummy on our site.  We expect you to understand that we do not provide our services in the states of Assam, Telangana, Odisha and Nagaland to adhere by law.
                    </p>

                    <h6>Game offerings:</h6> <p>All tournaments, practice and cash games provided by A2Z Betting are collectively called as Games. All the rules applicable to each type of games is provided on the website. 
                        Cash games require the user to have certain amount of cash balance in their user account. The rest of the games are considered as non-cash games.A2Z Betting charges on Cash games based on the type of game. The non-cash games are free of charge and require no cash balance in your account.
                    </p>

                    <h6>User representations:</h6>
                    <ul>
                        <li>Any information provided to us by you, either at the time of registration or during any point subsequently, should be truthful and complete.</li>
                        <li>Prior to adding cash to your user account or participating in Cash Games, you shall be responsible to satisfy yourself about the legality of playing Cash Games in the jurisdiction from where you are accessing Cash Games. If you are not legally competent to individually enter into Indian Rupee transactions through banking channels in India and/or are not accessing the Website from a permitted jurisdiction, you are prohibited from participating in Cash Games on the website. In the event of such violation, your participation in Cash Games will be deemed to be in breach of the Terms and you will not be entitled to receive any prize that you might win in such Cash Games.</li>
                        <li>The user must be at least 18 years old to indulge in cash games. You will be held responsible for the breach of this term, including your financial loss.</li>
                        <li>You represent that you possess the knowledge and skill required to partake in activities of the website. A2Z Betting will not be held responsible for any financial loss you might incur.</li>
                        <li>You understand that your active participation in the games of the website does not create any obligation on us to grant you prizes. Your skill is solely responsible for your winnings and losses.</li>
                        <li>You understand and agree that you are solely responsible for all content posted, transmitted, uploaded or otherwise made available on the Websites by you. All content posted by you must be legally owned by or licensed to you. By publishing any content on the Website, you agree to grant us a royalty-free, world-wide, non-exclusive, perpetual and assignable right to use, copy, reproduce, modify, adapt, publish, edit, translate, create derivative works from, transmit, distribute, publicly display, and publicly perform your content and to use such content in any related marketing materials produced by us or our affiliates. Such content may include, without limitation, your name, username, location, messages, gender or pictures. You also understand that you do not obtain any rights, legal or equitable, in any material incorporating your content.</li>
                        <li>You understand that the funds you have in your user account in A2Z Betting will carry no interest or return.</li>
                        <li>You shall not hold A2Z Betting responsible for not being able to play a game that you are eligible for. This includes situations where you are not able to access your user account due to breach of terms.</li>
                        <li>You understand and accept that by viewing or using the Websites or availing of any Services, or using communication features on the Website, you may be exposed to content posted by other users which you may find offensive, objectionable or indecent. You may bring such content posted by other users to our notice that you may find offensive, objectionable or indecent and we reserve the right to act upon it as we may deem fit. The decision taken by us on this regard shall be final and binding on you.</li>
                    </ul>

                    <h6>User account creation and Operation</h6>


                    <ul>
                        <li>To avail our services, you will first register at our portal.</li>
                        <li>After completing the online registration process, you will read and accept the terms and conditions.</li>
                        <li>with registering with A2Z Betting, you will receive a promotional message related to tournaments and bonuses.</li>
                        <li>During the registration process, you will be required to choose a login ID and password followed by personal information such as your name, email ID, postal address, etc</li>
                        <li>You acknowledge that we may, at any point,wish to verify the information you have given us, failing of which, we have the right to suspend your registration at A2Z Betting.</li>
                        <li>Any information you give us must be complete and truthful. We have no obligation to verify the information you give us and we will not be held responsible for any consequence of misinformation or concealment of information.</li>
                        <li>You understand that it is your responsibility to protect the information you give us on our website including but not limited to username, password, email and mobile number</li>
                        <li>A2Z Betting  will not ask for your user account login password which is only to be entered at the time of login. At no other time should you provide your user account information to any user logged in on the Websites or elsewhere. You undertake that you will not allow / login and then allow, any other person to play from your user account using your username. You specifically understand and agree that we will not incur any liability for information provided by you to anyone which may result in your user account on the Websites being exposed or misused by any other person.</li>
                        <li>You agree to use A2Z Betting user account solely for the purpose of playing on the Websites and for transactions which you may have to carry out in connection with availing the Services on the Websites. Use or attempted use of your user account for any reason other than what is stated in the Terms may result in immediate termination of your user account and forfeiture of any prize, bonus or balance in the user account.</li>
                        <li>You understand and agree that you cannot transfer any sum from your user account with us to the account of another registered user on the website except as may be permitted by A2Z Betting and subject to restrictions and conditions as may be prescribed.</li>
                        <li>You also understand and agree that deposits in your user account maintained with us are purely for the purpose of participation in Cash Games made available on the Website.</li>
                        <li>We are legally obliged to deduct tax at source(TDS) of 30% on winning above 10,000 Rs in a single game. In such cases, your PAN ID must be provided. The amount after deduction of TDS will be directly credited to your withdrawal account. You can only withdraw this amount after correctly providing your PAN number</li>
                        <li>We reserve the right to verify your PAN number from time to time and cancel the winning if it is seen to be inconsistent.</li>
                    </ul>


                    <h6>USER ACCOUNT VALIDATION: 
                    </h6>
                    <ul>

                        <li>A2Z Betting might verify user’s account from time to time. This might be done via phone or email. Failing to reach the user, A2Z Betting will make further attempts to contact the user. If the information provided by you is incorrect, we are not responsible for the discontinuation of services after not being able to get in contact with you.</li>
                        <li>If we are unable to reach you or if the validation is unsuccessful, we reserve the right to disallow you from logging into the Portal or reduce your play limits and/or Add Cash limits until we are able to satisfactorily validate your user account. We will in such events email you to notify you of the next steps regarding user account validation. We may also ask you for proof of identification and proof of address from time to time.</li>
                        <li>Upon receipt of suitable documents, we will try our best to enable your user account at the earliest. However, it may take a few business days to reinstate your user account.</li>
                        <li>In the event that we have made several attempts to reach out to you but have been unable to do so, we also reserve the right to permanently suspend your user account and refund the amount, if any, in your user account to the financial instrument through which the payment was made to your user account or by cheque to the address provided by you. In the event the address provided by you is incorrect, A2Z Betting will not make any additional attempts for delivery of the cheque unless a correct address is provided by you and charges for redelivery as prescribed by A2Z Betting are paid by you.</li>
                        <li>The Privacy Policy of our Websites form a part of the Terms. All personal information which is of such nature that requires protection from unauthorized dissemination shall be dealt with in the manner provided in the Privacy policy of the Website.</li>
                    </ul>
                    <h6>User restrictions
                    </h6>

                    <ul>
                        <li>winning is strictly prohibited.</li>
                        <li>Money Laundering-  The player is prohibited from partaking any activity that is  considered as money laundering including withdrawing money from unutilized funds added through credit cards and deliberately losing money to a certain player.</li>
                        <li>Anti- spamming- Spamming unregistered and unknown numbers to avail our bonus services or any other beneficial services is strictly prohibited.</li>
                        <li>Multiple IDs- The player is expected to have only one account with A2Z Betting. You are prohibited from creating or using multiple IDs to play Rummy.</li>
                        <li>You may not create a login name or password or upload, distribute, transmit, publish or post content through or on the Websites or through any service or facility including any messaging facility provided by the Websites which :

                            <ul>
                                <li>is libellous, defamatory, obscene, intimidating, invasive of privacy, abusive, illegal, harassing;</li>
                                <li>contains expressions of hatred, hurting religious sentiments, racial discrimination or pornography;</li>
                                <li>is otherwise objectionable or undesirable (whether or not unlawful);</li>
                                <li>would constitute incitement to commit a criminal offence;</li>
                                <li>violates the rights of any person;</li>
                                <li>is aimed at soliciting donations or other form of help;</li>
                                <li>violates the intellectual property of any person;</li>
                                <li>disparage in any manner A2Z Betting or any of its associates, partners, sponsors, products, services, or websites.</li>
                                <li>promotes a competing service or product; or</li>
                                <li>violates any laws</li>
                            </ul>




                        </li>


                        <li>in the event we determine that the login name created by you is indecent, objectionable, offensive or otherwise undesirable, we shall notify you of the same and you shall promptly provide us with an alternate login name so that we can change your existing login name to the new name provided by you. If you fail to provide an alternate name, we reserve the right to either permanently suspend your user account or restore your user account only after a different acceptable login name has been provided by you.</li>
                        <li>You shall not host, intercept, emulate or redirect proprietary communication protocols, used by the Website, if any, regardless of the method used, including protocol emulation, reverse engineering or modification of the Websites or any files that are part of the Websites.</li>
                        <li>You shall not frame the Portal. You may not impose editorial comments, commercial material or any information on the Websites, alter or modify Content on the Websites, or remove, obliterate or obstruct any proprietary notices or labels.</li>
                        <li>You shall not use Services on the Websites for commercial purposes including but not limited to use in a cyber cafe as a computer gaming centre, network play over the Internet or through gaming networks or connection to an unauthorized server that copies the gaming experience on the Website.</li>
                        <li>You shall not upload, distribute or publish through the Websites, any content which may contain viruses or computer contaminants (as defined in the Information Technology Act 2000 or such other laws in force in India at the relevant time) which may interrupt, destroy, limit the functionality or disrupt any software, hardware or other equipment belonging to us or that aids in providing the services offered by A2Z Betting. You shall not disseminate or upload viruses, programs, or software whether it is harmful to the Websites or not. Additionally, you shall not impersonate another person or user, attempt to get a password, other user account information, or other private information from a user, or harvest email addresses or other information.</li>
                        <li>You shall not purchase, sell, trade, rent, lease, license, grant a security interest in, or transfer your user account, Content, currency, points, standings, rankings, ratings, or any other attributes appearing in, originating from or associated with the Website.</li>
                        <li>Any form of fraudulent activity including, attempting to use or using any other person's credit card(s), debit cards, net-banking usernames, passwords, authorization codes, prepaid cash cards, mobile phones for adding cash to your user account is strictly prohibited.</li>
                        <li>Accessing or attempting to access the Services through someone else's user account is strictly prohibited.</li>
                        <li>Winnings, bonuses and prizes are unique to the player and are non-transferable. In the event you attempt to transfer any winnings, bonuses or prizes, these will be forfeited.</li>
                        <li>If you are an officer, director, employee, consultant or a relative of such persons , you are not permitted to play either directly or indirectly, any Games which entitle you to any prize on the Websites, other than in the course of your engagement with A2Z Betting. For these purposes, the term 'relative' shall include spouse and financially dependent parents and, children.</li>
                        <li>You shall not post any material or comment, on any media available for public access, which in our sole discretion, is defamatory or detrimental to our business interests, notwithstanding the fact that such media is not owned or controlled by us. In addition to any other action that we may take pursuant to the provision hereof, we reserve the right to remove any and all material or comments posted by you and restrict your access to any media available for public access that is either controlled or moderate by us; when in our sole opinion, any such material or comments posted by you is defamatory or detrimental to our business interests.</li>

                    </ul>


                    <h6>Withdrawal policy
                    </h6>
                    <p>You may withdraw your winnings by means of either an account payee cheque or an electronic bank to bank transfer for the amount of winnings.
                    </p>
                    <p>You agree that all withdrawals you make are governed by the following conditions:
                    </p>



                    <ul>
                        <li>We  can ask you for KYC documents to verify your address and identity at any stage. Withdrawals will be permitted only from accounts for which such KYC process is complete.</li>
                        <li>You can choose to withdraw money from your user account at any time, subject to bonus/prize money withdrawal restrictions, by notifying us of your withdrawal request. Bonuses (except Bring a Friend bonus) and promotional winnings are subject to withdrawal restrictions and can only be withdrawn on fulfilling some preconditions, one of which is that you have made at least one cash deposit on the Portal and thereafter played at least one Cash Game.</li>
                        <li>Once notified, post verification of the withdrawal request, we may disburse the specified amount by cheque or electronic transfer based on the mode of withdrawal selected by you. We shall make our best efforts to honor your choice on the mode of withdrawal, but reserve the right to always disburse the specified amount to you by cheque. We also reserve the right to disburse the amount on the financial instrument used to Add Cash to your user account.</li>
                        <li>Withdrawals attract processing charges as per the prevalent policy. You may be eligible to make one or more free withdrawals in a month depending on various factors including your club status, the amount of withdrawal or the mode of withdrawal. In the event that you do not have an adequate amount in your user account to pay the processing charge, your withdrawal will not be processed at that time. You may request for a free withdrawal subsequently if you become eligible for the same at such time.</li>
                        <li>If you are unable to cash the cheque within 30 days of dispatch, we will deem the cheque to have been lost and reserve the right to cancel the cheque with necessary instructions to our bank to stop payment of that cheque. This may result in dishonor of that cheque in which case we shall not be liable for the cheque amount to you. Any request for reissuing the cheque will result in deduction of applicable processing charges from the amount of the cheque on reissuance.</li>
                        <li>We will attempt our best to process your withdrawals in a timely manner, but there could be delays due to the time required for verification and completing the withdrawal transaction. We shall not be liable to pay you any form of compensation for the reason of delays in remitting payments to you from your user account.</li>
                        <li>To be eligible to win a prize, you must be a resident of India and accessing the Services of Play A2Z Betting on the Services from India.</li>
                    </ul>

                    <p>If you are a prize winner resident in India and physically present in India but not an Indian citizen, we will remit your winnings in Indian Rupees to the address/bank account given by you, provided, the address and bank account is within India.

                    </p>


                    <h6>Breach of terms and conditions</h6>
                    <p>We at A2Z Betting might want to verify after reasonable doubt, that our terms of service are being disrupted by a payer, then we reserve the right to take one or all of the actions below at our discretion:
                    </p>


                    <ul>
                        <li>Permanently suspend your user account on our website.</li>
                        <li>Forfeit the amount of cash left in your account.</li>
                        <li>Demand and order damages for breach of A2Z Betting’s Terms of Service and take required civil actions to recover damages.</li>
                        <li>Initiate steps of prosecution for damages and violation that is equivalent to offences in law.</li>
                        <li>Put restrictions on the access of our games by users who are suspected in cases of cheating or colluding.</li>
                        <li>Bar you from playing or registering on A2Z Betting in the future, as we reserve the right to restrict you from playing our online rummy games.</li>
                    </ul>

                    <p>The action taken by us will be solely due to your breach of our Terms of Service; the action shall be final and decisive that will be binding on you. Any action taken by us will be without any prejudice to our other rights and remedies that are mentioned and available in law or equity.
                    </p>

                    <h6>Modifications</h6>

                    <ul>
                        <li>We may alter or modify the Terms at any time without giving prior notice to you. We may choose to notify of some changes in the Terms either by email or by displaying a message on the Websites; however, our notification of any change shall not waive your obligation to keep yourself updated about the changes in the Terms. Your continued use of any Website and/or any Services offered constitutes your unconditional acceptance of the modified or amended Terms.</li>
                        <li>We may also post supplementary conditions for any Services that may be offered. In such an event, your use of those Services will be governed by the Terms as well as any such supplementary terms that those Services may be subject to.</li>
                    </ul>


                    <h6>Disclaimer</h6>


                    <p>The Services on A2Z Betting and are provided strictly on "as is" basis with all faults or failings. Any representations, warranties, conditions or guarantee whatsoever, express or implied (including, without limitation, any implied warranty of accuracy, completeness, uninterrupted provision, quality, merchantability, fitness for a particular purpose or non-infringement) are specifically excluded to the fullest extent permitted by law. We do not ensure or guarantee continuous, error-free, secure or virus-free operation of the Websites or its Content including software, Games, your user account, the transactions in your user account or continued operation or availability of any facility on the website.</p>
                    <p>Additionally, A2Z Betting does not promise or ensure that you will be able to access your user account or obtain Services whenever you want. It is entirely possible that you may not be able to access your user account or the Services provided at times or for extended periods of time due to, but not limited to, system maintenance and updates.</p>
                    <p>Disclaimer:  responsibility and liability for any harm resulting from cancellation of any Game organized by it is not ours. If you are a cash player on the website, you acknowledge and agree that you will not be entitled to any refund in case of any service outages that may be caused by failures of our service providers, computer viruses or contaminants, natural disasters, war, civil disturbance, or any other cause beyond the reasonable control of A2Z Betting.</p>
                    <p>We specifically disclaim any liability in connection with Games or events made available or organized on the Websites which may require specific statutory permissions, in the event such permissions are denied or cancelled whether prior to or during such Game or event.</p>
                    <p>We specifically disclaim any liability in connection with your transactions with third parties which may have advertisements or are hyperlinked on the Website.</p>
                    <p>We disclaim any liability in connection with violation of intellectual property rights of any party with respect to third party Content or user content posted on our Website. Intellectual property rights in any Content not belonging to us belong to the respective owners and any claims related to such content must be directly addressed to the respective owners.</p>
                    <p>We specifically disclaim any liability arising out of the acts or omissions of the infrastructure providers or otherwise failure of internet services used for providing and accessing the Services.</p>
                    <p>A2Z Betting disclaims liability for any risk or loss resulting to you from your participation in Cash Games, including all risk of financial loss.</p>

                    <h6>Limitation of liability:
                    </h6>






                    <p>In addition to specific references to limitation of liability of A2Z Betting elsewhere in the Terms, under no circumstances, we shall be liable for any injury, loss, claim, loss of data, loss of income, loss of profit or loss of opportunity, loss of or damage to property, general damages or any direct, indirect, special, incidental, consequential, exemplary or punitive damages of any kind whatsoever arising out of or in connection with your access to, or use of, or inability to access or use, the Services on any Website. You further agree to indemnify us and our service providers and licensors against any claims in respect of any such matter.</p>
                    <p>Without limiting the generality of the foregoing, you specifically acknowledge, agree and accept that we are not liable to you for:</p>


                    <ul>
                        <li>the defamatory, undesirable or illegal conduct of any other user of the Services;</li>
                        <li>any loss whatsoever arising from the use, abuse or misuse of your user account or any feature of our Services on the Websites;</li>
                        <li>any loss incurred in transmitting information from or to us or from or to our Websites by the internet or by other connecting media;</li>
                        <li>any technical failures, breakdowns, defects, delays, interruptions, improper or manipulated data transmission, data loss or corruption or communications' infrastructure failure, viruses or any other adverse technological occurrences arising in connection with your access to or use of our Services;</li>
                        <li>the accuracy, completeness or currency of any information services provided on the Websites;</li>
                        <li>any delay or failure on our part to intimate you where we may have concerns about your activities; and</li>
                        <li>your activities / transactions on third party websites accessed through links or advertisements posted in the Website.</li>

                    </ul>


                    <h6>Governing law, Dispute resolution and Jurisdiction:
                    </h6>


                    <p>The agreement in our Terms of Service shall be followed, governed, and interpreted and construed in accordance with the laws of India. All or any disputes, complaints, differences, etc, shall be subject to Arbitration under the law of Arbitration and Conciliation Act of 1996.
                    </p>

                    <h6>Refund Policy
                    </h6>


                    <ul>
                        <li>Cancellation of Games:A2Z Betting is not responsible for any loss resulting from the cancellation of any games due to Service outages caused by the failures of our service providers, computer viruses, natural disasters or any other reason that is beyond our control. You acknowledge that you will not be entitled to any refund due to cancellation of games caused by factors that are beyond the reasonable control of A2Z Betting.</li>
                        <li>Online Transactions: Transactions to transfer money to your cash account have to be made through third-party payment gateways. We are not responsible for the denial or rejection of any transactions as they are subject to the terms and conditions of the third-party payment gateways. A2Z Betting may reverse any transactions, and the specific amount will in such a case be refunded to the financial instrument/platform that was used by you to perform the transaction.</li>
                        <li>Account Validation:A2Z Betting has the right to check or validate the personal information of the players from time to time. The process of validation can be done through a phone call or via email. If we are not able to get in touch with you at first, several attempts will be made to establish contact with you. In the event that the validation is unsuccessful, or we are unable to contact you after making all possible attempts, we reserve the right to suspend your account and refund the amount lying in your account to the financial instrument/platform that you used to make the payment online.</li>
                    </ul>

                    <p>In cases of accidental or erroneous cash credits to any party, user or company, we can independently or you can request for a refund within two weeks of initiating the transaction. We will process the refund if it is a genuine case of accidental or erroneous cash credit, which is clear from your request or after our own independent investigation. The refund amount will be credited within two weeks to the financial instrument/platform that was used to make the corresponding transaction.</p>

                    <h6>Voluntary Termination
                    </h6>

                    <p>You may cease to use our gaming services and terminate your account by emailing us with a clear explanation of your intention. After evaluating your case, at our discretion, A2Z Betting will refund to you the entire cash amount available in your account within 10 working days and terminate your account. Please note, that any account actions you take after emailing your request for terminating your account, including, but not limited to, playing cash games, tournaments, depositing and withdrawing money, will be deemed as actions taken solely by you and A2Z Betting shall not be responsible for your actions.
                    </p>

                    <h6>Inactive Accounts and Unused Funds
                    </h6>


                    <p>If your account has been inactive for over 365 days and has unused funds in it, A2Z Betting shall remind you three times to activate your account. To activate your account, you must log in and use your funds by playing a cash game. If, despite the reminders, you do not return to the site and do not activate your account, A2Z Betting shall refund all unused funds from your account to your bank account in our records. If you do not have any bank account information associated with your account or the bank account information provided is incorrect, A2Z Betting shall serve you three more reminders to correct your bank account information. If you fail to provide your bank account information for a refund, A2Z Betting shall confiscate the funds from your account and deactivate the account. All reminders shall be sent to the email and phone number you entered on A2Z Betting as part of your account details and information.</p>
                    <p>Accounts found in breach of our Terms and Conditions shall receive no reminders, and the unused funds in such accounts shall be subject to the terms of our Breach and Consequences policy.</p>

                    <h6>Payments and Player Funds
                    </h6>

                    <p>All transactions on the Website shall be in Indian Rupees.
                    </p>


                    <ul>
                        <li>Once you register on our Portal, we maintain a user account for you to keep a record of all your transactions with us. Payments connected with participation in Cash Games have to be made through your user account. All cash prizes won by you are credited by us into this user account.</li>
                        <li>When making a payment, please ensure that the instrument used to make the payment is your own and is used to Add Cash into your user account only.</li>
                        <li>Subject to the Add Cash limits specified by us from time to time, you are free to deposit as much money as you want in your user account for the purpose of participating in Cash Games on the Websites.</li>
                        <li>A2Z Betting wants you to play responsibly on the Websites. The ability to Add Cash in your user account shall be subject to monthly Add Cash limits which we can be set by us with undertakings, indemnity, waiver and verification conditions as we deem appropriate in our sole discretion.</li>
                        <li>Credit card, Debit card, prepaid cash cards and internet banking payments are processed through third party payment gateways. Similarly, other payment modes also require an authorization by the intermediary which processes payments. We are not responsible for delays or denials at their end and processing of payments will be solely in terms of their policies and procedures without any responsibility or risk at our end. If there are any issues in connection with adding cash, a complaint may be sent to us following the complaints procedure provided in "Complaints and disputes" section below. You agree that in such an event of your credit being delayed or eventually declined for reasons beyond our control, we will not be held liable in any manner whatsoever. Once a payment/transaction is authorized, the funds are credited to your user account and are available for you to play Cash Games.</li>
                        <li>We have the right to cancel a transaction at any point of time solely according to our discretion in which case if the payment is successful, then the transaction will be reversed and the money credited back to your payment instrument.</li>
                        <li>Player funds are held in trust by us in specified bank accounts. A2Z Betting keeps all players' funds unencumbered which will be remitted to you in due course subject to the terms and conditions applicable to withdrawal of funds. Funds held in your user account are held separately from our corporate funds. Even in the highly unlikely event of an insolvency proceeding, you claims on the deposits will be given preference over all other claims to the extent permissible by law.</li>
                    </ul>


                </div>

            </div>
        </div>
    </div>
</section>