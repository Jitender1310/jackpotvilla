<?php if (isset($player_details->username)) { ?>
    <div class="card card-menu mb-3">
        <div class="card-body py-2 px-4">
            <div class="row align-items-center justify-content-between">
                <div class="col">
                    <div class="chip chip--has-icon box-shadow">
                        <div class="chip__icon">
                             <img  src="<?= STATIC_IMAGES_PATH ?>/user_img.svg"  height="80">
                        </div>
                        <div class="chip__info">
                            <div class="chip__info--holder">
                                <div>Username</div>
                                <div class="font-weight-bold"><?= $player_details->username ?></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="chip chip--has-icon chip--has-action box-shadow">
                        <div class="chip__icon">
                           <img  src="<?= STATIC_IMAGES_PATH ?>/add-cash.svg"  height="100">
                        </div>
                        <div class="chip__action" onclick="location.href = '<?= base_url() ?>deposit'"><i class="fal fa-plus fa-2x"></i></div>
                        <div class="chip__info">
                            <div class="chip__info--holder">
                                <div>Cash</div>
                                <div class="font-weight-bold">{{chipsObj.wallet_account.total_cash_balance}}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="chip chip--has-icon box-shadow">
                        <div class="chip__icon">
                              <img  src="<?= STATIC_IMAGES_PATH ?>/bonus.svg"  height="80">
                        </div>
                       <!--  <div class="chip__action nw"><span><small>Redeem</small></span></div> -->
                        <div class="chip__info">
                            <div class="chip__info--holder">
                                <div>Bonus</div>
                                <div class="font-weight-bold">{{chipsObj.wallet_account.total_bonus}}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-auto">
                    <div class="chip chip--has-icon box-shadow chip--has-action">
                        <div class="chip__icon">
                           <img  src="<?= STATIC_IMAGES_PATH ?>/fun_coins.svg"  height="80">
                        </div>
                        <div class="chip__action" ng-click="resetFunChips()"><i class="fal fa-sync fa-2x"></i></div>
                        <div class="chip__info">
                            <div class="chip__info--holder">
                                <div>Fun Chips</div>
                                <div class="font-weight-bold">{{chipsObj.wallet_account.fun_chips}}</div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--         <?php if ($this->uri->segment(1) == "game_lobby") { ?>
                                <div class="col-auto pr-3">
                                    <div class="chip chip--has-icon">
                                        <div class="chip__icon">
                                            <svg height="30px"  width="30px" viewBox="0 0 512 512">
                                            <use xlink:href="#group" />
                                            </svg>
                                        </div>
                                        <div class="chip__info">
                                            <div class="chip__info--holder">
                                                <div>Online Players</div>
                                                <div class="font-weight-bold">{{onlinePlayers||0}}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                <?php } ?> -->

                <div class="col">
                    <a href="<?= base_url() ?>level_info" class="chip chip--has-icon box-shadow ">
                        <div class="chip__icon">
                                  <img  src="<?= STATIC_IMAGES_PATH ?>/logo_face.svg"  height="80">

                        </div>

                        <div class="chip__info">
                            <div class="chip__info--holder">
                                <div>Level</div>
                                <div class="font-weight-bold">{{chipsObj.expertise_level}}</div>
                            </div>
                        </div>
                    </a>
                </div>

                <!-- <div class="col-auto">
                    <a href="<?= base_url() ?>level_info" class="eicon d-flex align-items-center justify-content-center">
                        <div>
                            <div>{{chipsObj.wallet_account.total_rps_points}}</div>
                            <div>Rps</div>
                        </div>
                    </a>
                </div> -->

                <div class="col-auto">
                    <a href="#" ng-click="showOfferNotificationsPopup()"  class="eicon eicon-notif d-flex align-items-center justify-content-center">
                        <div> <img  src="<?= STATIC_IMAGES_PATH ?>/bell_icon.svg"  height="80" style=" height: 30px;"></div>
                    </a>
                </div>
                <!-- <div class="col-auto">
                    <a href="<?= base_url() ?>logout" class="logoutbtn">
                        <div> <img  src="<?= STATIC_IMAGES_PATH ?>/lobby-logout.png"  height="80" style=" height: 30px; border: 2px solid #690909;
                                    border-radius: 50px;" onclick="google_signOut();"></div>
                        <div>Logout</div>
                    </a>
                </div> -->
            </div>
        </div>
    </div>
    <?php $this->load->view('includes/notifications') ?>
    <?php
}?>