<?php if (is_logged_in()) { ?>
    <?php $this->load->view('includes/menu-dashboard') ?>
<?php } ?>

<?php if (!is_logged_in()) { ?>
    <?php $this->load->view("includes/header"); ?>
    <?php $this->load->view('menu') ?>
<?php } ?>

<section class="innerpages py-5">
    <div class="container">
        <div class="row" style="background: #fff;padding: 25px;">
            <div class="col-4 my-3">
                <div class="card-box text-center">
                    <div class="card-body" >
                        <svg height="80px" viewBox="0 0 512 512"><use xlink:href="#trophy" /></svg>
                        <h3 class="my-4">Premium Tournaments</h3>
                        <p  style="text-align: center;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do  eiusmod tempor incididunt ut  labore. et dolore magna aliqua.</p>
                    </div>
                </div>
            </div>
            <div class="col-4 my-3">
                <div class="card-box text-center">
                    <div class="card-body">
                        <svg height="80px" viewBox="0 0 58.007 58.007"><use xlink:href="#coins" /></svg>
                        <h3 class="my-4">Premium Tournaments</h3>
                        <p  style="text-align: center;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do  eiusmod tempor incididunt ut  labore. et dolore magna aliqua.</p>
                    </div>
                </div>
            </div>
            <div class="col-4 my-3">
                <div class="card-box text-center">
                    <div class="card-body">
                        <svg height="80px" viewBox="0 0 512 512"><use xlink:href="#trophy" /></svg>
                        <h3 class="my-4">Premium Tournaments</h3>
                        <p  style="text-align: center;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do  eiusmod tempor incididunt ut  labore. et dolore magna aliqua.</p>
                    </div>
                </div>
            </div>
            <div class="col-4 my-3">
                <div class="card-box text-center">
                    <div class="card-body">
                        <svg height="80px" viewBox="0 0 512 512"><use xlink:href="#trophy" /></svg>
                        <h3 class="my-4">Premium Tournaments</h3>
                        <p  style="text-align: center;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do  eiusmod tempor incididunt ut  labore. et dolore magna aliqua.</p>
                    </div>
                </div>
            </div>
            <div class="col-4 my-3">
                <div class="card-box text-center">
                    <div class="card-body">
                        <svg height="80px" viewBox="0 0 512 512"><use xlink:href="#trophy" /></svg>
                        <h3 class="my-4">Premium Tournaments</h3>
                        <p  style="text-align: center;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do  eiusmod tempor incididunt ut  labore. et dolore magna aliqua.</p>
                    </div>
                </div>
            </div>
            <div class="col-4 my-3">
                <div class="card-box text-center">
                    <div class="card-body">
                        <svg height="80px" viewBox="0 0 512 512"><use xlink:href="#trophy" /></svg>
                        <h3 class="my-4">Premium Tournaments</h3>
                        <p  style="text-align: center;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do  eiusmod tempor incididunt ut  labore. et dolore magna aliqua.</p>
                    </div>
                </div>
            </div>
        </div>
</section>