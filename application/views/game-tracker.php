<?php $this->load->view('includes/menu-dashboard') ?>
<section class="innerpages py-5" ng-controller="gameTrackerCtrl">
    <div class="container">
        <div class="gametracker-outer">
        <?php include'chips.php'; ?>
        <div class="row mt-4">
            <?php $this->load->view('includes/dashboard-sidebar') ?>
            <div class="col-9">
                <div class="card card-gold h-100">
                    <div class="card-header card-header2 text-white">MY GAME TRACKER</div>
                    <div class="card-body">
                        
                        <table class="table table-bordered table-striped text-center">
                            <tr>
                                <td width="20%" ng-repeat="hd in gameTrackerObj.headers track by $index">
                                    <strong class="d-block">{{hd.title}}</strong><small>{{hd.sub_title}}
                                        <i class="fal fa-info-circle ml-1" ng-hide="$index==0" data-toggle="popover" data-content="{{hd.info}}"></i>
                                    </small>
                                </td>
                            </tr>
                            
                            <tr ng-repeat="gt in gameTrackerObj.grid_info track by $index">
                                <td>
                                    <div class="h6 m-0">{{gt.title}}</div>
                                    <div class="m-0">{{gt.sub_title}}</div>
                                </td>
                                <td ng-repeat="item in gt.items track by $index">{{item.played}}/{{item.max}}</td>
                            </tr>
                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>