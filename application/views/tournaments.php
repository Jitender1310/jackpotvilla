<?php include'menu-dashboard.php'; ?>
<section class="innerpages">
    <div class="container">

        <div class="inner-frame"> <?php include'chips.php'; ?>

            <div class="btn-group-special row justify-content-center mt-4 pb-5">
                <div class="col-3">
                    <a href="#" class="btn btn-block btn-light">CASH</a>
                </div>
                <div class="col-3">
                    <a href="#" class="btn btn-block btn-light btn-active">TOURNAMENTS</a>
                </div>
                <div class="col-3">
                    <a href="#" class="btn btn-block btn-light">PRACTICE</a>
                </div>
            </div>

            <div class="card card-table py-4 border-0">


                <div class="card-body">
                    <div class="card card-table-menu shadow">
                        <div class="card-body py-2">
                            <div class="row no-gutters">
                                <div class="col pr-2"><a class="special-tag text-center" href="">Level</a></div>
                                <div class="col pr-2"><a class="special-tag text-center active" href="#cash" data-toggle="modal">Cash</a></div>
                                <div class="col pr-2"><a class="special-tag text-center" href="">Special</a></div>
                                <div class="col"><a class="special-tag text-center" href="">Free</a></div>


                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-custom table-striped table-hover bg-white">
                    <thead class="bg-primary-dark text-white">   <tr>
                            <th class="border-top-0">Name</th>
                            <th class="border-top-0">Entry Fee</th>
                            <th class="border-top-0">Prize(₹)</th>
                            <th class="border-top-0">Joined</th>
                            <th class="border-top-0">Starts</th>
                            <th class="border-top-0 text-right">Action</th>
                        </tr></thead>

                    <tr>
                        <td>MTT-Cash-50</td>
                        <td>₹40</td>
                        <td>₹1,275</td>
                        <td>30/30</td>
                        <td>05:30 PM Today</td>
                        <td class="text-right">
                            <a href="#details" data-toggle="modal" class="btn rounded btn-info btn-sm text-uppercase">Details</a>
                            <a href="#" class="btn rounded btn-success btn-sm text-uppercase">Join Now</a>
                        </td>
                    </tr>
                    <tr>
                        <td>MTT-Cash-50</td>
                        <td>₹40</td>
                        <td>₹1,275</td>
                        <td>30/30</td>
                        <td>05:30 PM Today</td>
                        <td class="text-right">
                            <a href="#details" data-toggle="modal" class="btn rounded btn-info btn-sm text-uppercase">Details</a>
                            <a href="#" class="btn rounded btn-success btn-sm text-uppercase">Join Now</a>
                        </td>
                    </tr>
                    <tr>
                        <td>MTT-Cash-50</td>
                        <td>₹40</td>
                        <td>₹1,275</td>
                        <td>30/30</td>
                        <td>05:30 PM Today</td>
                        <td class="text-right">
                            <a href="#details" data-toggle="modal" class="btn rounded btn-info btn-sm text-uppercase">Details</a>
                            <a href="#" class="btn rounded btn-success btn-sm text-uppercase">Join Now</a>
                        </td>
                    </tr>
                    <tr>
                        <td>MTT-Cash-50</td>
                        <td>₹40</td>
                        <td>₹1,275</td>
                        <td>30/30</td>
                        <td>05:30 PM Today</td>
                        <td class="text-right">
                            <a href="#details" data-toggle="modal" class="btn rounded btn-info btn-sm text-uppercase">Details</a>
                            <a href="#" class="btn rounded btn-success btn-sm text-uppercase">Join Now</a>
                        </td>
                    </tr>
                    <tr>
                        <td>MTT-Cash-50</td>
                        <td>₹40</td>
                        <td>₹1,275</td>
                        <td>30/30</td>
                        <td>05:30 PM Today</td>
                        <td class="text-right">
                            <a href="#details" data-toggle="modal" class="btn rounded btn-info btn-sm text-uppercase">Details</a>
                            <a href="#" class="btn rounded btn-success btn-sm text-uppercase">Join Now</a>
                        </td>
                    </tr>
                    <tr>
                        <td>MTT-Cash-50</td>
                        <td>₹40</td>
                        <td>₹1,275</td>
                        <td>30/30</td>
                        <td>05:30 PM Today</td>
                        <td class="text-right">
                            <a href="#details" data-toggle="modal" class="btn rounded btn-info btn-sm text-uppercase">Details</a>
                            <a href="#" class="btn rounded btn-success btn-sm text-uppercase">Join Now</a>
                        </td>
                    </tr>
                    <tr>
                        <td>MTT-Cash-50</td>
                        <td>₹40</td>
                        <td>₹1,275</td>
                        <td>30/30</td>
                        <td>05:30 PM Today</td>
                        <td class="text-right">
                            <a href="#details" data-toggle="modal" class="btn rounded btn-info btn-sm text-uppercase">Details</a>
                            <a href="#" class="btn rounded btn-success btn-sm text-uppercase">Join Now</a>
                        </td>
                    </tr>
                    <tr>
                        <td>MTT-Cash-50</td>
                        <td>₹40</td>
                        <td>₹1,275</td>
                        <td>30/30</td>
                        <td>05:30 PM Today</td>
                        <td class="text-right">
                            <a href="#details" data-toggle="modal" class="btn rounded btn-info btn-sm text-uppercase">Details</a>
                            <a href="#" class="btn rounded btn-success btn-sm text-uppercase">Join Now</a>
                        </td>
                    </tr>
                    <tr>
                        <td>MTT-Cash-50</td>
                        <td>₹40</td>
                        <td>₹1,275</td>
                        <td>30/30</td>
                        <td>05:30 PM Today</td>
                        <td class="text-right">
                            <a href="#details" data-toggle="modal" class="btn rounded btn-info btn-sm text-uppercase">Details</a>
                            <a href="#" class="btn rounded btn-success btn-sm text-uppercase">Join Now</a>
                        </td>
                    </tr>
                    <tr>
                        <td>MTT-Cash-50</td>
                        <td>₹40</td>
                        <td>₹1,275</td>
                        <td>30/30</td>
                        <td>05:30 PM Today</td>
                        <td class="text-right">
                            <a href="#details" data-toggle="modal" class="btn rounded btn-info btn-sm text-uppercase">Details</a>
                            <a href="#" class="btn rounded btn-success btn-sm text-uppercase">Join Now</a>
                        </td>
                    </tr>
                    <tr>
                        <td>MTT-Cash-50</td>
                        <td>₹40</td>
                        <td>₹1,275</td>
                        <td>30/30</td>
                        <td>05:30 PM Today</td>
                        <td class="text-right">
                            <a href="#details" data-toggle="modal" class="btn rounded btn-info btn-sm text-uppercase">Details</a>
                            <a href="#" class="btn rounded btn-success btn-sm text-uppercase">Join Now</a>
                        </td>
                    </tr>
                    <tr>
                        <td>MTT-Cash-50</td>
                        <td>₹40</td>
                        <td>₹1,275</td>
                        <td>30/30</td>
                        <td>05:30 PM Today</td>
                        <td class="text-right">
                            <a href="#details" data-toggle="modal" class="btn rounded btn-info btn-sm text-uppercase">Details</a>
                            <a href="#" class="btn rounded btn-success btn-sm text-uppercase">Join Now</a>
                        </td>
                    </tr>
                    <tr>
                        <td>MTT-Cash-50</td>
                        <td>₹40</td>
                        <td>₹1,275</td>
                        <td>30/30</td>
                        <td>05:30 PM Today</td>
                        <td class="text-right">
                            <a href="#details" data-toggle="modal" class="btn rounded btn-info btn-sm text-uppercase">Details</a>
                            <a href="#" class="btn rounded btn-success btn-sm text-uppercase">Join Now</a>
                        </td>
                    </tr>
                </table>
            </div>






        </div>
    </div>
</section>