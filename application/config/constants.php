<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | Display Debug backtrace
  |--------------------------------------------------------------------------
  |
  | If set to TRUE, a backtrace will be displayed along with php errors. If
  | error_reporting is disabled, the backtrace will not display, regardless
  | of this setting
  |
 */
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
defined('FILE_READ_MODE') OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') OR define('DIR_WRITE_MODE', 0755);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */
defined('FOPEN_READ') OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
  |--------------------------------------------------------------------------
  | Exit Status Codes
  |--------------------------------------------------------------------------
  |
  | Used to indicate the conditions under which the script is exit()ing.
  | While there is no universal standard for error codes, there are some
  | broad conventions.  Three such conventions are mentioned below, for
  | those who wish to make use of them.  The CodeIgniter defaults were
  | chosen for the least overlap with these conventions, while still
  | leaving room for others to be defined in future versions and user
  | applications.
  |
  | The three main conventions used for determining exit status codes
  | are as follows:
  |
  |    Standard C/C++ Library (stdlibc):
  |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
  |       (This link also contains other GNU-specific conventions)
  |    BSD sysexits.h:
  |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
  |    Bash scripting:
  |       http://tldp.org/LDP/abs/html/exitcodes.html
  |
 */
defined('EXIT_SUCCESS') OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code



$url = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
$url .= "://" . $_SERVER['HTTP_HOST'];
$url .= str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']);

$protocol = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http").'://'. $_SERVER['HTTP_HOST'];
define("BACK_END_SERVER_URL", $protocol."/admin/");



define('WEB_ASSETS_IMAGE_PATH', $url . "web_assets/images/");
define('FILE_UPLOAD_FOLDER', "./uploads/");
define('FILE_UPLOAD_FOLDER_IMG_PATH', $url . "uploads/");

define('POSTS_UPLOAD_FOLDER', "./post_uploads/");
define('POSTS_UPLOAD_FOLDER_IMG_PATH', $url . "post_uploads/");

define('SETTLEMENT_ATTACHMENT_UPLOAD_FOLDER', "./vendor_assets/settlement_receipts/");
define('SETTLEMENT_ATTACHMENT_UPLOAD_FOLDER_IMG_PATH', $url . "vendor_assets/settlement_receipts/");

define('KYC_ATTACHMENTS_UPLOAD_FOLDER', "./kyc_attachments/");
define('KYC_ATTACHMENTS_UPLOAD_FOLDER_PATH', $url . "kyc_attachments/");

define('STATIC_IMAGES_PATH', $url . "assets/images/");
define('STATIC_CSS_PATH', $url . "assets/css/");
define('STATIC_JS_PATH', $url . "assets/js/");
define('STATIC_ANGULAR_PATH', $url . "assets/js/angular/");
define('STATIC_ANGULAR_CTRLS_PATH', $url . "assets/js/angular/controllers/");
define('STATIC_ANGULAR_SERVICES_PATH', $url . "assets/js/angular/services/");


define('STATIC_MOBILE_CSS_PATH', $url . "m_assets/css/");
define('STATIC_MOBILE_JS_PATH', $url . "m_assets/js/");
define('STATIC_MOBILE_IMAGES_PATH', $url . "m_assets/images/");

define('BLOG_IMAGES_UPLOAD_FOLDER_PATH', BACK_END_SERVER_URL . "/blog_images/");


define('ORDERS_PER_PAGE', 10);
define('DATE_FORMAT', 'd-m-Y');
define('DATE_TIME_FORMAT', 'd-m-Y h:i A');
define('DISTANCE_RADIUS', 2000);
define('CREATED_AT', time());
define('CREATED_DATE', date("Y-m-d"));
define('CREATED_TIME', date("H:i:s"));
define('CREATED_DATE_TIME', date("Y-m-d H:i:s"));

define('KYC_ADMIN_ATTACHMENTS_UPLOAD_FOLDER_PATH', BACK_END_SERVER_URL."kyc_attachments/");
define('DEFAULT_IMAGE_UPLOAD_FOLDER_PATH', BACK_END_SERVER_URL."avatars/");

define('BACK_END_SERVER_URL_FILE_UPLOAD_FOLDER_IMG_PATH', BACK_END_SERVER_URL . "uploads/");

define("GAME_WALLET_ACCOUNT_ID", 1);
define("GST_WALLET_ACCOUNT_ID", 2);
define("TDS_WALLET_ACCOUNT_ID", 3);
define("ADMIN_COMMISSION_WALLET_ACCOUNT_ID", 4);

define("DATE_TIME_HUMAN_READABLE_FORMAT", "d-m-Y h:i A");

define("PAYMENT_GATEWAY_MODE", "live");
define("SERVER_MODE", "development"); //production  