<?php

header("access-control-allow-origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Access-Control-Allow-Headers: Content-Type");
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class MY_Controller extends CI_Controller {

    public $data;

    function __construct($is_Value = null) {
        parent::__construct();
        $this->data = array();

        $mail_config['protocol'] = 'smtp';
        $mail_config['charset'] = 'utf-8';
        $mail_config['smtp_host'] = SMTP_HOST;
        $mail_config['smtp_user'] = SMTP_USERNAME;
        $mail_config['smtp_pass'] = SMTP_PASSWORD;
        $mail_config['smtp_port'] = 587;
        $mail_config['smtp_crypto'] = 'tls';
        $mail_config['send_multipart'] = FALSE;

        $mail_config['crlf'] = "\r\n";
        $mail_config['newline'] = "\r\n";
        $mail_config['wordwrap'] = TRUE;
        $mail_config['mailtype'] = 'html';
        $this->email->initialize($mail_config);
        $this->data["page_active"] = "";
        $this->data["player_details"] = [];
        if ($this->input->get_post('access_token') != '') {
            $this->player_login_model->get_player_id_by_access_token($this->input->get_post('access_token'));
            if($is_Value == null){
                $this->session->set_userdata("p_access_token", $this->input->get_post('access_token'));
            }
        }
        if ($this->session->userdata("p_access_token")) {
            $this->data["player_details"] = $this->player_login_model->get_player_details();
        }
        $this->bonus_expiry_cron_model->check_and_update_bonus_amounts();
    }

    function login_required() {
        $response = $this->player_login_model->check_for_user_logged();
        if ($response) {
            $this->data["player_details"] = $this->player_login_model->get_player_details();
//            $user_details = $this->user_model->get_user_info($this->session->userdata("user_id"));
//            if ($user_details->user_meta->status != 1) {
//                redirect("logout");
//            }
//            $this->data['name_of_logged_in_person'] = $user_details->fullname;
            return true;
        } else if ($this->input->is_ajax_request()) {
            $arr = ['err_code' => "invalid",
                "error_type" => "login_required",
                "message" => "Session Expired, Please login"
            ];
            echo json_encode($arr);
        } else {
            $this->session->set_flashdata("login_error", "Session Expired, Please login");
            redirect("home");
        }
    }

    function is_logged_in() {
        if ($this->player_login_model->check_for_user_logged()) {
            return true;
        } else {
            return false;
        }
    }

    function print_pdf_view($design) {
        $this->load->view("includes/print_page_header", $this->data);
        $this->load->view($design);
        $this->load->view("includes/print_page_footer");
    }

    function front_view($design) {
        if ($_SERVER['HTTP_HOST'] == "m.cloverrummy.com") {
            $this->mobile_view($design);
        } else {
        	//return $this->mobile_view($design);
            $this->load->view("includes/header", $this->data);
            $this->load->view($design);
            $this->load->view("includes/footer");
        }
    }

    function mobile_view($design) {
        $this->load->view("mobile_website/includes/header", $this->data);
        $this->load->view("mobile_website/" . $design);
        $this->load->view("mobile_website/includes/footer");
    }

}
