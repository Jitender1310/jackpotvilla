<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//Powered by Manikanta
class Cash_free {

//    private $MERCHANT_KEY = "A9JB3k";
//        private $SALT = "1uoCYHgv";
    // Merchant key here as provided by Payu
    private $MERCHANT_KEY = "";
    // Merchant Salt as provided by Payu
    private $SALT = "";
    private $CASH_FREE_BASE_URL = '';

    function setServerMode($servermode = 'live') {
        // End point - change to https://secure.payu.in for LIVE mode
        //$this->MERCHANT_KEY = 'JBZaLc';
        //$this->SALT = 'GQs7yium';
        $this->CASH_FREE_BASE_URL = $servermode == 'test' ?
                'https://test.cashfree.com/billpay/checkout/post/submit' :
                'https://www.cashfree.com/checkout/post/submit';
    }

    private $action = '';
    private $posted = array();
    private $txnid;
    public $hash = '';
    private $amount = '';
    private $orderCurrency = '';
    private $phonenumber = '';
    private $productInfo = '';
    private $email = '';
    private $notifyUrl = '';
    private $returnUrl = '';
    private $consumername = '';
    private $hash_string = '';
    // Hash Sequence
    private $hashSequence = "key|txnid|amount|orderCurrency|productinfo|firstname|phone|email|udf1|udf2|udf3|udf4";

    function __construct() {
        $CI = & get_instance();
    }

    function generate_auto_transaction_id() {
        if (empty($posted['txnid'])) {
            // Generate random transaction id
            $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        } else {
            $txnid = $posted['txnid'];
        }
    }

    function generateHashString() {
        $postData = array(
            "appId" => $this->MERCHANT_KEY,
            "orderId" => $this->txnid,
            "orderAmount" => $this->amount,
            "orderCurrency" => $this->orderCurrency,
            "orderNote" => $this->productInfo,
            "customerName" => $this->consumername,
            "customerPhone" => $this->phonenumber,
            "customerEmail" => $this->email,
            "returnUrl" => $this->returnUrl,
            "notifyUrl" => $this->notifyUrl,
        );
        ksort($postData);
        $signatureData = "";
        foreach ($postData as $key => $value){
             $signatureData .= $key.$value;
        }
        $signature = hash_hmac('sha256', $signatureData, $this->SALT, true);
        $this->hash_string = base64_encode($signature);
        
        return $this->hash_string;
    }

    function generateHash() {
        $this->hash =  $this->generateHashString();
    }

    function goto_collect_money() {
        $this->showForm();
        $this->action = $this->CASH_FREE_BASE_URL;
    }

    function setTransactionid($invoiceid) {
        $this->txnid = $invoiceid;
    }

    function setAmount($amount) {
        $this->amount = $amount;
    }

    function setOrderCurrency($currency) {
        $this->orderCurrency = $currency;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setConsumerName($name) {
        $this->consumername = $name;
    }

    function setProductInfo($proInfo) {
        $this->productInfo = $proInfo;
    }

    function setPhoneNumber($phone) {
        $this->phonenumber = $phone;
    }

    function setNotify_url($surl) {
        $this->notifyUrl = $surl;
    }

    function setReturn_url($furl) {
        $this->returnUrl = $furl;
    }

    function showForm() {
        $this->image_url = base_url() . 'assets/images/loading.gif';
        echo $html = <<<HTML

<html>
<head>
<script>
    var hash = '$this->hash';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.cashFreeForm;
   payuForm.submit();
    }
  </script>
</head>
<body onLoad="submitPayuForm()">
<h2 style="text-align:center">Please wait connecting to payment gateway</h2>
<center><img src="$this->image_url" alt="Connecting..." align="absmiddle"/></center>
<form action="$this->CASH_FREE_BASE_URL" method="post" name="cashFreeForm">
  <input type="hidden" name="appId" value="$this->MERCHANT_KEY" />
    <input type="hidden" name="signature" value="$this->hash"/>
  <input type="hidden" name="orderId" value="$this->txnid" />
  <table  style="display:none">
    <tr>
      <td><b>Mandatory Parameters</b></td>
    </tr>
    <tr>
      <td>Amount: </td>
      <td><input type="hidden" name="orderAmount" value="$this->amount" />
      <input type="hidden" name="orderCurrency" value="$this->orderCurrency" /></td>
      <td>First Name: </td>
      <td><input type="hidden" name="customerName" id="customerName" value="$this->consumername" /></td>
    </tr>
    <tr>
      <td>Email: </td>
      <td><input type="hidden" name="customerEmail" id="email" value="$this->email" /></td>
      <td>Phone: </td>
      <td><input type="hidden" name="customerPhone" value="$this->phonenumber" /></td>
    </tr>
    <tr>
      <td>Product Info: </td>
      <td colspan="3"><textarea name="orderNote" style="display:none">$this->productInfo</textarea></td>
    </tr>
    <tr>
      <td>Notify URI: </td>
      <td colspan="3"><input type="hidden" name="notifyUrl" value="$this->notifyUrl" size="64" /></td>
    </tr>
    <tr>
      <td>Return URI: </td>
      <td colspan="3"><input type="hidden" name="returnUrl" value="$this->returnUrl" size="64" /></td>
    </tr>
    <tr>
                
      <td colspan="4">
   <input type="submit" value="Submit" style="display:none"/></td>
    </tr>
  </table>
</form>
</body>
</html>

HTML;
    }

}
