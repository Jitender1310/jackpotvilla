<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$displayCurrency = $setData['currency'];

//These should be commented out in production
// This is for error reporting
// Add it to config.php to report any errors
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/

require_once(APPPATH . "libraries/razorpaykit/razorpay-php/Razorpay.php");
//use Razorpay\Api\Api;

// Create the Razorpay Order

use Razorpay\Api\Api;

$api = new Api($keyId, $keySecret);
//var_dump(json_encode($setData));die();
//
// We create an razorpay order using orders api
// Docs: https://docs.razorpay.com/docs/orders
//
$orderData = [
    'receipt'         => $setData["receipt"],
    'amount'          => $setData["amount"] * 100, // 2000 rupees in paise
    'currency'        => $displayCurrency,
    'payment_capture' => $setData["payment_capture"] // auto capture
];

$razorpayOrder = $api->order->create($orderData);

$razorpayOrderId = $razorpayOrder["id"];

$_SESSION['razorpay_order_id'] = $razorpayOrderId;

$displayAmount = $amount = $orderData['amount'];

if ($displayCurrency !== 'INR')
{
    $url = "https://api.fixer.io/latest?symbols=$displayCurrency&base=INR";
    $exchange = json_decode(file_get_contents($url), true);

    $displayAmount = $exchange['rates'][$displayCurrency] * $amount / 100;
}

$checkout = 'automatic';

$data = [
    "key"                   => $keyId,
    "amount"                => $amount,
    "action"                => $setData["action"],
    "name"                  => $setData["name"],
    "description"           => $setData["description"],
    "image"                 => $setData["naimageme"],
    "prefill"               => [
        "name"                => $setData["prefill_name"],
        "email"               => $setData["prefill_email"],
        "contact"             => $setData["prefill_contact"],
    ],
    "notes"                 => [
        "address"             => $setData["prefill_address"],
        "merchant_order_id"   => $setData["receipt"],
    ],
    "theme"                 => [
      "color"                 => $setData["theme_color"]
    ],
    "order_id"              => $razorpayOrderId,
    "order_transaction_id"  => $setData['receipt'],
];

if ($displayCurrency !== 'INR')
{
    $data['display_currency']  = $displayCurrency;
    $data['display_amount']    = $displayAmount;
}

$json = json_encode($data);

        // /var_dump(json_encode($data));die();
//require("checkout/{$checkout}.php");

        require(APPPATH . "libraries/razorpaykit/checkout/{$checkout}.php");
