<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . "libraries/paytmkit/lib/encdec_paytm.php");

//Powered by Manikanta
class Paytm {

//    private $MERCHANT_KEY = "A9JB3k";
//        private $SALT = "1uoCYHgv";
    // Merchant ID as provided by Paytm
    private $MERCHANT_ID = "";
    private $MERCHANT_KEY = ""; //alias merchant key
    private $PAYTM_BASE_URL = '';
    private $TRANSACTION_URL = '';
    private $serverMode = '';

    function setServerMode($servermode = 'live') {
        // End point - change to https://securegw.paytm.in/theia/processTransaction for LIVE mode
        //$this->MERCHANT_KEY = 'JBZaLc';
        //$this->SALT = 'GQs7yium';

        $this->serverMode = $servermode;

        $this->PAYTM_BASE_URL = $servermode == 'test' ?
                'https://securegw-stage.paytm.in/theia/processTransaction' :
                'https://securegw.paytm.in/theia/processTransaction';
        $this->TRANSACTION_URL = $servermode == 'test' ?
                'https://securegw-stage.paytm.in/order/status' :
                'https://securegw.paytm.in/order/status';
    }

    private $action = '';
    private $posted = array();
    private $txnid;
    private $customer_id;
    private $industry_type_id;
    private $channel_id;
    private $website_name;
    public $hash = '';
    private $amount = '';
    private $orderCurrency = '';
    private $phonenumber = '';
    private $productInfo = '';
    private $email = '';
    private $notifyUrl = '';
    private $returnUrl = '';
    private $consumername = '';
    private $hash_string = '';

    function __construct() {
        $CI = & get_instance();
    }

    function generate_auto_transaction_id() {
        if (empty($posted['txnid'])) {
            // Generate random transaction id
            $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        } else {
            $txnid = $posted['txnid'];
        }
    }

    function generateHashString() {
        $postData = array(
            "MID" => $this->MERCHANT_ID,
            "ORDER_ID" => $this->txnid,
            "CUST_ID" => $this->customer_id,
            "INDUSTRY_TYPE_ID" => $this->industry_type_id ? $this->industry_type_id : "Retail",
            "CHANNEL_ID" => $this->channel_id ? $this->channel_id : "WEB",
            "TXN_AMOUNT" => $this->amount,
            "WEBSITE" => $this->website_name,
//            "orderCurrency" => $this->orderCurrency,
//            "orderNote" => $this->productInfo,
//            "customerName" => $this->consumername,
//            "MOBILE_NO" => $this->phonenumber,
//            "EMAIL" => $this->email,
//            "VERIFIED_BY" => "EMAIL",
//            "IS_USER_VERIFIED" => "YES",
            "CALLBACK_URL" => $this->returnUrl,
//            "notifyUrl" => $this->notifyUrl,
        );

        $this->hash_string = getChecksumFromArray($postData, $this->MERCHANT_KEY);
        
//        print_r($postData);
//        die;
        
        return $this->hash_string;
    }

    function generateHash() {
        $this->hash = $this->generateHashString();
    }

    function goto_collect_money() {
        $this->showForm();
        $this->action = $this->PAYTM_BASE_URL;
    }

    function setTransactionid($invoiceid) {
        $this->txnid = $invoiceid;
    }

    function setCustomerId($customer_id) {
        $this->customer_id = "CUST".$customer_id;
    }

    function setIndustryTypeId($industry_type_id) {
        if (strtolower($this->serverMode) == "live") {
            // $this->industry_type_id = $industry_type_id;
            $this->industry_type_id = "Retail";
        } else {
            $this->industry_type_id = "Retail";
        }
    }

    function setChannelId($channel_id) {
        $this->channel_id = $channel_id;
    }

    function setWebsiteName($website_name) {
        if (strtolower($this->serverMode) == "live") {
            //$this->website_name = str_replace("-", "", $website_name);
            $this->website_name = "DEFAULT";
        } else {
            $this->website_name = "WEBSTAGING";
        }
    }

    function setAmount($amount) {
        $this->amount = $amount;
    }

    function setOrderCurrency($currency) {
        $this->orderCurrency = $currency;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setConsumerName($name) {
        $this->consumername = $name;
    }

    function setProductInfo($proInfo) {
        $this->productInfo = $proInfo;
    }

    function setPhoneNumber($phone) {
        $this->phonenumber = $phone;
    }

    function setNotify_url($surl) {
        $this->notifyUrl = $surl;
    }

    function setReturn_url($furl) {
        $this->returnUrl = $furl;
    }

    function getTransactionStatus() {
        $postData = array(
            "MID" => $this->MERCHANT_ID,
            "ORDERID" => $this->txnid
        );
        $this->hash_string = getChecksumFromArray($postData, $this->MERCHANT_KEY);
        $postData['CHECKSUMHASH'] = $this->hash_string;
        $postData = "JsonData=" . json_encode($postData, JSON_UNESCAPED_SLASHES);
        $connection = curl_init(); // initiate curl
        $transactionURL = $this->TRANSACTION_URL;
        curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($connection, CURLOPT_URL, $transactionURL);
        curl_setopt($connection, CURLOPT_POST, true);
        curl_setopt($connection, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($connection, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $responseReader = curl_exec($connection);
        $responseData = json_decode($responseReader, true);

        return $responseData;
    }

    function showForm() {
        $this->image_url = base_url() . 'assets/images/loading.gif';
        echo $html = <<<HTML

<html>
<head>
<script>
    var hash = '$this->hash';
    function submitPaytmForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.paytmForm;
   payuForm.submit();
    }
  </script>
</head>
<body onLoad="submitPaytmForm()">
<h2 style="text-align:center">Please wait connecting to payment gateway</h2>
<center><img src="$this->image_url" alt="Connecting..." align="absmiddle"/></center>
<form action="$this->PAYTM_BASE_URL" method="post" name="paytmForm">
    <input type="hidden" name="MID" value="$this->MERCHANT_ID" />
    <input type="hidden" name="ORDER_ID" value="$this->txnid" />
    <input type="hidden" name="CUST_ID" id="customerName" value="$this->customer_id" />
    <input type="hidden" name="INDUSTRY_TYPE_ID" id="INDUSTRY_TYPE_ID" value="$this->industry_type_id" />
    <input type="hidden" name="CHANNEL_ID" id="CHANNEL_ID" value="$this->channel_id" />
    <input type="hidden" name="TXN_AMOUNT" value="$this->amount" />   
    <input type="hidden" name="WEBSITE" value="$this->website_name" />
   <input type="hidden" name="CHECKSUMHASH" value="$this->hash"/>
   <input type="hidden" name="CALLBACK_URL" value="$this->returnUrl"/>
</form>
</body>
</html>
HTML;
    }

}
