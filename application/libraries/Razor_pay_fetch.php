<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$displayCurrency = $setData['currency'];

//These should be commented out in production
// This is for error reporting
// Add it to config.php to report any errors
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/

require_once(APPPATH . "libraries/razorpaykit/razorpay-php/Razorpay.php");
//use Razorpay\Api\Api;

// Create the Razorpay Order

use Razorpay\Api\Api;
$api = new Api($keyId, $keySecret);
$order_razor_data = $api->order->fetch($setData['razorpay_order_id']);
