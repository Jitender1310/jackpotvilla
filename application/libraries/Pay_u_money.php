<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//Powered by kishan
class Pay_u_money {

    // Merchant key here as provided by Payu
    private $MERCHANT_KEY = "";
    // Merchant Salt as provided by Payu
    private $SALT = "";
    private $CASH_FREE_BASE_URL = '';

    function setServerMode($servermode = 'live') {
        $this->CASH_FREE_BASE_URL = $servermode == 'test' ?
                "https://sandboxsecure.payu.in" :
                "https://secure.payu.in";
    }

    private $txnid; //pay u
    private $udf1 = ''; //pay u
    private $udf2 = ''; //pay u
    private $udf3 = ''; //pay u
    private $udf4 = ''; //pay u
    private $udf5 = ''; //pay u
    private $udf6 = ''; //pay u
    private $udf7 = ''; //pay u
    private $udf8 = ''; //pay u
    private $udf9 = ''; //pay u
    private $udf10 = ''; //pay u
    public $hash = ''; //pay u
    private $amount = ''; //pay u
    private $phonenumber = ''; //pay u
    private $productInfo = ''; //pay u
    private $email = ''; //pay u
    private $consumername = ''; //pay u
    // Hash Sequence
    private $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";

    private $success;  
    private $fail ;
    private $cancel ;

    function generateHashString() {
        $posted = array(
            "key" => $this->MERCHANT_KEY,
            "txnid" => $this->txnid,
            "amount" => $this->amount,
            "productinfo" => $this->productInfo,
            "firstname" => $this->consumername,
            "email" => $this->email,
            "udf1" => $this->udf1,
            "udf2" => $this->udf2,
            "udf3" => $this->udf3,
            "udf4" => $this->udf4,
            "udf5" => $this->udf5,
            "udf6" => $this->udf6,
            "udf7" => $this->udf7,
            "udf8" => $this->udf8,
            "udf9" => $this->udf9,
            "udf10" => $this->udf10
        );

        
        $hashVarsSeq = explode('|', $this->hashSequence);
        $hash_string = '';  
        foreach($hashVarsSeq as $hash_var) {
          $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
          $hash_string .= '|';
        }

        $hash_string .= $this->SALT;


        $this->hash = strtolower(hash('sha512', $hash_string));
        //$action = $PAYU_BASE_URL . '/_payment';
    }

    function setUrl($url) {
        $this->success = $url;
        $this->fail = $url;
        $this->cancel = $url;
    }

    function goto_collect_money() {
        $this->showForm();
        $this->action = $this->CASH_FREE_BASE_URL;
    }

    function setTransactionid($invoiceid) {
        $this->txnid = $invoiceid;
    }

    function setAmount($amount) {
        $this->amount = $amount;
    }

    function setOrderCurrency($currency) {
        $this->orderCurrency = $currency;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setConsumerName($name) {
        $this->consumername = $name;
    }

    function setProductInfo($proInfo) {
        $this->productInfo = $proInfo;
    }

    function setPhoneNumber($phone) {
        $this->phonenumber = $phone;
    }

    function setNotify_url($surl) {
        $this->notifyUrl = $surl;
    }

    function setReturn_url($furl) {
        $this->returnUrl = $furl;
    }

    function showForm() {
        //echo "<pre>";var_dump($this);die();
        $this->image_url = base_url() . 'assets/images/loading.gif';
        echo $html = <<<HTML

<html>
<head>
<script>
    var hash = '$this->hash';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var myForm = document.getElementById("myForm");
        //Extract Each Element Value
        for (var i = 0; i < myForm.elements.length; i++) {
        console.log(myForm.elements[i]);
        }
        //return;
      var payuForm = document.forms.payuForm;
   payuForm.submit();
    }
  </script>
</head>
<body onLoad="submitPayuForm()">
<h2 style="text-align:center">Please wait connecting to payment gateway</h2>
<center><img src="$this->image_url" alt="Connecting..." align="absmiddle"/></center>
<form action="$this->CASH_FREE_BASE_URL/_payment" method="post" name="payuForm" id="myForm">
  <input type="hidden" name="key" value="$this->MERCHANT_KEY" />
    <input type="hidden" name="hash" value="$this->hash"/>
  <input type="hidden" name="txnid" value="$this->txnid" />
  <table  style="display:none">
    <tr>
      <td><b>Mandatory Parameters</b></td>
    </tr>
    <tr>
      <td>Amount: </td>
      <td><input type="hidden" name="amount" value="$this->amount" />
      <td><input type="hidden" name="productinfo" value="$this->productInfo" />
      <td>First Name: </td>
      <td><input type="hidden" name="firstname" id="customerName" value="$this->consumername" /></td>
    </tr>
    <tr>
      <td>Email: </td>
      <td><input type="hidden" name="email" id="email" value="$this->email" /></td>
      <td>Phone: </td>
      <td><input type="hidden" name="phone" value="$this->phonenumber" /></td>
    </tr>
    <tr>
      <td>Product Info: </td>
      <td colspan="3"><textarea name="orderNote" style="display:none">$this->productInfo</textarea></td>
    </tr>
    
    <input name="surl" value="$this->success" size="64" type="hidden" />
    <input name="furl" value="$this->fail" size="64" type="hidden" />                             
    <input type="hidden" name="service_provider" value="payu_paisa" size="64" /> 
    <!--<tr>
      <td>Notify URI: </td>
      <td colspan="3"><input type="hidden" name="notifyUrl" value="$this->notifyUrl" size="64" /></td>
    </tr>
    <tr>
      <td>Return URI: </td>
      <td colspan="3"><input type="hidden" name="returnUrl" value="$this->returnUrl" size="64" /></td>
    </tr>-->
    
    <tr>
          <td><b>Optional Parameters</b></td>
        </tr>
        <tr>
          <td>Last Name: </td>
          <td><input name="lastname" id="lastname"  /></td>
          <td>Cancel URI: </td>
          <td><input name="curl" value="" /></td>
        </tr>
        <tr>
          <td>Address1: </td>
          <td><input name="address1"  /></td>
          <td>Address2: </td>
          <td><input name="address2" /></td>
        </tr>
        <tr>
          <td>City: </td>
          <td><input name="city" /></td>
          <td>State: </td>
          <td><input name="state" /></td>
        </tr>
        <tr>
          <td>Country: </td>
          <td><input name="country" /></td>
          <td>Zipcode: </td>
          <td><input name="zipcode"/></td>
        </tr>
        <tr>
          <td>UDF1: </td>
          <td><input name="udf1" /></td>
          <td>UDF2: </td>
          <td><input name="udf2" /></td>
        </tr>
        <tr>
          <td>UDF3: </td>
          <td><input name="udf3" /></td>
          <td>UDF4: </td>
          <td><input name="udf4" /></td>
        </tr>
        <tr>
          <td>UDF5: </td>
          <td><input name="udf5" /></td>
          <td>PG: </td>
          <td><input name="pg" /></td>
        </tr>
        <tr>
                
      <td colspan="4">
   <input type="submit" value="Submit" style="display:none"/></td>
    </tr>
  </table>
</form>
</body>
</html>

HTML;
    }

}
