var theme = {
  testimonialslider: function () {
    $('.testimonials .carousel').slick({
      autoplay: false,
      infinite: true,
      arrows: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true
    });
  },
  promotionsslider: function () {
    $('.promotions .carousel').slick({
      autoplay: false,
      infinite: true,
      arrows: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true
    });
  },
  slidercarousel: function () {
    $('.slider .carousel').slick({
      autoplay: true,
      infinite: true,
      arrows: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      centerMode: true,
      centerPadding: '10px',
      dots: false
    });
  },
  servicescarousel: function () {
    $('.services .carousel').slick({
      autoplay: false,
      infinite: true,
      arrows: false,
      slidesToShow: 2,
      slidesToScroll: 1,
      centerMode: true,
      variableWidth: false,
      centerPadding: '20px',
      dots: false
    });
  },
  screenscarousel: function () {
    $('.screens .carousel').slick({
      autoplay: false,
      infinite: true,
      arrows: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      centerMode: true,
      centerPadding: '40px',
      dots: false
    });
  },
  workspace: function () {
    function workspace() {
      var workspace_offset = 0;
      $(".workspace-offset").each(function () {
        workspace_offset += $(this).outerHeight();
      });
      $(".workspace").css({ "height": $(window).height() - workspace_offset });
    }
    setTimeout(function () { workspace(); }, 3000);
    $(window).on("resize", workspace);
  },
  sidebar: function () {
    $(".sidebar-action").on("click", function () {
      if (!$(".sidebar").hasClass('open')) {
        $(".sidebar").addClass("open");
        $(".sidebar-overlay").addClass("open");
      } else {
        $(".sidebar").removeClass("open");
        $(".sidebar-overlay").removeClass("open");
      }
    });
  },
  icons: function (path) {
      $.get(path, function (svg) {
        $(".loadicons").html(svg);
      }, 'text');
  }
}
theme.promotionsslider();
theme.testimonialslider();
theme.slidercarousel();
theme.servicescarousel();
theme.screenscarousel();
theme.workspace();
theme.sidebar();
