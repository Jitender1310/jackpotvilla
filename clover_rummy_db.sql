/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.7.27-0ubuntu0.16.04.1 : Database - clover_rummy_db
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`clover_rummy_db` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `clover_rummy_db`;

/*Table structure for table `app_downloads` */

DROP TABLE IF EXISTS `app_downloads`;

CREATE TABLE `app_downloads` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mobile` varchar(50) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `app_downloads` */

/*Table structure for table `app_version` */

DROP TABLE IF EXISTS `app_version`;

CREATE TABLE `app_version` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `version_android` varchar(20) NOT NULL,
  `version_ios` varchar(45) DEFAULT NULL,
  `description` text NOT NULL,
  `is_force` enum('0','1') NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `updated_at` varchar(25) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `app_version` */

/*Table structure for table `blog_categories` */

DROP TABLE IF EXISTS `blog_categories`;

CREATE TABLE `blog_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(100) NOT NULL,
  `seo_slug` varchar(100) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `	updated_at` varchar(25) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `blog_categories` */

/*Table structure for table `blog_pages` */

DROP TABLE IF EXISTS `blog_pages`;

CREATE TABLE `blog_pages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  `blog_categories_ids` varchar(200) NOT NULL,
  `page_title` longtext NOT NULL,
  `description` longtext NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `last_action_by_user_id` bigint(20) unsigned NOT NULL,
  `seo_slug` varchar(250) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `thumbnail_image` varchar(200) NOT NULL,
  `banner_image` varchar(200) NOT NULL,
  PRIMARY KEY (`id`,`blog_categories_ids`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `blog_pages` */

/*Table structure for table `bonus_coupons` */

DROP TABLE IF EXISTS `bonus_coupons`;

CREATE TABLE `bonus_coupons` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_code` varchar(100) NOT NULL,
  `bonus_type` enum('Deposit Bonus','Occasional Bonus') NOT NULL DEFAULT 'Deposit Bonus',
  `credit_to` enum('Deposit Wallet','Bonus Wallet') NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `from_date_time` datetime NOT NULL,
  `to_date_time` datetime NOT NULL,
  `minimum_deposit` float unsigned DEFAULT '0',
  `points_percentage` int(11) NOT NULL,
  `max_points` int(11) NOT NULL,
  `usage_times` int(11) NOT NULL,
  `terms_conditions` text NOT NULL,
  `created_by_user_id` bigint(20) unsigned NOT NULL,
  `updated_by_user_id` bigint(20) unsigned NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `valid_days` bigint(20) NOT NULL DEFAULT '30',
  `applicable_type` enum('Club Wise','All') NOT NULL DEFAULT 'All',
  `club_wise_config` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `coupon_code` (`coupon_code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `bonus_coupons` */

/*Table structure for table `bonus_transaction_history` */

DROP TABLE IF EXISTS `bonus_transaction_history`;

CREATE TABLE `bonus_transaction_history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ref_id` varchar(50) NOT NULL,
  `transaction_type` enum('Credit','Debit') NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `closing_balance` double NOT NULL DEFAULT '0' COMMENT 'Not use just for virtual only',
  `created_date_time` datetime NOT NULL,
  `expiry_date` date DEFAULT NULL,
  `wallet_account_id` bigint(20) unsigned NOT NULL,
  `remark` varchar(250) NOT NULL,
  `type` varchar(100) NOT NULL,
  `bonus_code` varchar(100) DEFAULT '',
  `is_expired` tinyint(1) unsigned DEFAULT '0',
  `utilized_amount` double unsigned DEFAULT '0',
  `bonus_transaction_history_id` bigint(20) unsigned DEFAULT NULL COMMENT 'if debit transaction then record id be here',
  `games_id` bigint(20) DEFAULT NULL,
  `game_title` varchar(100) DEFAULT NULL,
  `game_sub_type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ref_id` (`ref_id`),
  KEY `wallet_account_id` (`wallet_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `bonus_transaction_history` */

/*Table structure for table `bot_game_play_settings` */

DROP TABLE IF EXISTS `bot_game_play_settings`;

CREATE TABLE `bot_game_play_settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `game_type` enum('Practice','Cash','Tournament') NOT NULL,
  `game_sub_type` enum('Pool','Deals','Points','Tournament') NOT NULL,
  `max_play_count` int(10) unsigned NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `bot_game_play_settings` */

insert  into `bot_game_play_settings`(`id`,`game_type`,`game_sub_type`,`max_play_count`,`created_at`,`updated_at`,`status`) values (1,'Practice','Pool',4,'','1559648400',1),(2,'Practice','Deals',3,'','',1),(3,'Practice','Points',3,'','',1),(4,'Cash','Pool',3,'','',1),(5,'Cash','Deals',3,'','',1),(6,'Cash','Points',3,'','',1),(7,'Tournament','Tournament',3,'','',1);

/*Table structure for table `bot_played_history` */

DROP TABLE IF EXISTS `bot_played_history`;

CREATE TABLE `bot_played_history` (
  `id` bigint(20) unsigned NOT NULL,
  `players_id` bigint(20) unsigned NOT NULL,
  `date_time` datetime NOT NULL,
  `date` date NOT NULL,
  `games_id` bigint(20) unsigned NOT NULL,
  `room_ref_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `bot_played_history` */

/*Table structure for table `chat_item` */

DROP TABLE IF EXISTS `chat_item`;

CREATE TABLE `chat_item` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `text` varchar(200) NOT NULL,
  `img` varchar(100) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `updated_at` varchar(25) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `chat_item` */

/*Table structure for table `cloned_tournaments` */

DROP TABLE IF EXISTS `cloned_tournaments`;

CREATE TABLE `cloned_tournaments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tournament_categories_id` bigint(20) unsigned NOT NULL,
  `ref_id` varchar(100) NOT NULL,
  `allowed` enum('All','For Selected Clubs') NOT NULL,
  `allowed_club_types` varchar(100) NOT NULL,
  `tournament_title` varchar(100) NOT NULL,
  `duration_in_minutes` int(10) unsigned NOT NULL DEFAULT '20',
  `frequency` enum('Monthly','Weekly','Daily','One Time') NOT NULL,
  `tournament_format` enum('Deals') NOT NULL,
  `entry_type` enum('Free','Cash') NOT NULL,
  `entry_value` float unsigned NOT NULL DEFAULT '1' COMMENT 'Alias Entry fee in RPS or tickets',
  `max_players` bigint(20) unsigned NOT NULL,
  `players_per_table` int(10) unsigned NOT NULL DEFAULT '6',
  `registration_start_date` varchar(20) NOT NULL DEFAULT '',
  `registration_start_time` time NOT NULL,
  `registration_close_date` varchar(20) NOT NULL DEFAULT '',
  `registration_close_time` time NOT NULL,
  `tournament_start_date` varchar(20) NOT NULL DEFAULT '',
  `tournament_start_time` time NOT NULL,
  `prize_type` enum('Fixed Prize','Depend on Players Joined','Tickets','Rps') NOT NULL,
  `expected_prize_amount` double unsigned NOT NULL,
  `tournament_commission_percentage` float NOT NULL,
  `token` varchar(100) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `week` varchar(20) DEFAULT '',
  `min_players_to_start_tournament` bigint(20) DEFAULT NULL,
  `tournament_structure` text COMMENT 'JSON Format',
  `tournaments_id` bigint(20) unsigned DEFAULT NULL,
  `no_of_bots` bigint(20) unsigned DEFAULT NULL COMMENT 'automatically bots will assign  when registration starts',
  `winning_probability_to` enum('Real Players','Bots') DEFAULT 'Bots',
  `tournament_status` enum('Created','Registration_Start','Registration_Closed','Running','Completed','Rejected','CallOff') DEFAULT 'Created',
  `display_upto_on_website_after_complete_tournament` date DEFAULT NULL,
  `premium_category` enum('Bronze','Silver','Gold','Platinum') DEFAULT NULL,
  `last_action_by_users_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`),
  UNIQUE KEY `ref_id` (`ref_id`),
  KEY `tournament_categories_id` (`tournament_categories_id`),
  KEY `tournaments_id` (`tournaments_id`),
  CONSTRAINT `cloned_tournaments_ibfk_1` FOREIGN KEY (`tournaments_id`) REFERENCES `tournaments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cloned_tournaments` */

/*Table structure for table `cloned_tournaments_play_data` */

DROP TABLE IF EXISTS `cloned_tournaments_play_data`;

CREATE TABLE `cloned_tournaments_play_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `game_detail_json` longtext,
  `cloned_tournaments_id` bigint(20) unsigned DEFAULT '0',
  `cloned_tournaments_token` varchar(100) NOT NULL,
  `rooms_id` varchar(45) NOT NULL DEFAULT '0',
  `game_sub_type` enum('Points','Pool','Deals') NOT NULL DEFAULT 'Points',
  `game_type` enum('Practice','Cash','Tournament') NOT NULL DEFAULT 'Practice',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `tournament_ref_id` varchar(45) DEFAULT NULL,
  `round_number` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cloned_tournaments_id` (`cloned_tournaments_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cloned_tournaments_play_data` */

/*Table structure for table `cloned_tournaments_prize_distribution_config` */

DROP TABLE IF EXISTS `cloned_tournaments_prize_distribution_config`;

CREATE TABLE `cloned_tournaments_prize_distribution_config` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `from_rank` int(10) unsigned NOT NULL,
  `to_rank` int(10) unsigned NOT NULL DEFAULT '0',
  `min_prize_value` double unsigned DEFAULT '0',
  `prize_value` double NOT NULL COMMENT 'Rupees',
  `extra_prize_info` varchar(150) NOT NULL,
  `cloned_tournaments_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cloned_tournaments_id` (`cloned_tournaments_id`),
  CONSTRAINT `cloned_tournaments_prize_distribution_config_ibfk_1` FOREIGN KEY (`cloned_tournaments_id`) REFERENCES `cloned_tournaments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cloned_tournaments_prize_distribution_config` */

/*Table structure for table `cloned_tournaments_round_wise_data` */

DROP TABLE IF EXISTS `cloned_tournaments_round_wise_data`;

CREATE TABLE `cloned_tournaments_round_wise_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cloned_tournaments_id` bigint(20) unsigned NOT NULL,
  `round_number` bigint(20) unsigned NOT NULL,
  `players_info` longtext NOT NULL,
  `is_running` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cloned_tournaments_id` (`cloned_tournaments_id`),
  CONSTRAINT `cloned_tournaments_round_wise_data_ibfk_1` FOREIGN KEY (`cloned_tournaments_id`) REFERENCES `cloned_tournaments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cloned_tournaments_round_wise_data` */

/*Table structure for table `club_types` */

DROP TABLE IF EXISTS `club_types`;

CREATE TABLE `club_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `priority` int(10) unsigned NOT NULL,
  `from_points` bigint(20) unsigned NOT NULL,
  `to_points` bigint(20) unsigned NOT NULL,
  `points_to_achieve` varchar(100) NOT NULL,
  `validity_in_days` bigint(20) unsigned NOT NULL,
  `number_of_free_withdrawals` bigint(20) unsigned DEFAULT NULL,
  `withdraw_fees_in_percentage` float unsigned DEFAULT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `club_types` */

insert  into `club_types`(`id`,`name`,`priority`,`from_points`,`to_points`,`points_to_achieve`,`validity_in_days`,`number_of_free_withdrawals`,`withdraw_fees_in_percentage`,`created_at`,`updated_at`,`status`) values (1,'JOKER',1,0,80,'80',0,2,2.5,'1554537517','1557307468',1),(2,'CLUB',2,81,600,'600',30,2,2.5,'1554537540','1557307600',1),(3,'DIAMOND',3,601,2000,'2000',45,2,2.5,'1554537562','1557307607',1),(4,'HEART',4,2001,5000,'5000',60,2,2.5,'1554537584','1557307614',1),(5,'SPADE',5,5001,10000,'10000',90,0,0,'1554537623','1557307620',1);

/*Table structure for table `cms_pages` */

DROP TABLE IF EXISTS `cms_pages`;

CREATE TABLE `cms_pages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `page_name` varchar(100) NOT NULL,
  `description` longtext,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `cms_pages` */

insert  into `cms_pages`(`id`,`page_name`,`description`,`created_at`,`updated_at`,`status`) values (1,'About Us','<p>About Us: Here at Clover Rummy , we provide a vivid gaming experience to our fellow Rummy enthusiasts. Come join our arena to play against the best, in a fair and safe play and win exciting rewards and cash prizes! We sell to you the best stage for us Clover Rummy and potential rummy geniuses.Clover Rummy brings to you, a shift from conventional gaming sites, to a much more thrilling and sophisticated card game experience. Love for rummy is only showing an upward trend in India and we, at Clover Rummy hope to help you realize your passion for the game we love. With only access to the Internet, you can now permanently say goodbye to boredom and play at any time of the say. All you have to do is register now, play tournaments and win money! Not only can you win spectacular prizes and cash money, you can now communicate with your fellow Clover Rummy and draw ticks and tips from them. Our friendly customer service ensures top class service to our loyal users. Financial transactions are transparent and well accounted for. Learn more about us in our Support and Security.</p>\r\n','1525415853','1566468315',1),(2,'Terms and Conditions','<h2>Important Notification</h2>\r\n\r\n<p>Play Games24x7 Private Limited has migrated its Services from www.games24x7.com to www.cloverrummy.com with effect from 22nd August, 2012. All Terms of www.games24x7.com have been modified to replace references to www.games24x7.com and Games24x7, with www.cloverrummy.com, with effect from the said date.</p>\r\n\r\n<p>Play Games24x7 Private Limited offers online games including and not limited rummy and fantasy cricket, through the web-portals&nbsp;<a href=\"https://www.cloverrummy.com/\">www.cloverrummy.com</a>, www.my11circle.com, etc. which are also linked in our partner website(s) and mobile application(s) (collectively referred to as the &quot;Portal&quot;) (Play Games24x7 Private Limited referred to herein as &quot;Play Games24x7&quot; or &quot;we&quot; or &quot;us&quot; or &quot;our&quot;). Any person utilizing the Portal or any of its features including participation in the various contests, games (&quot;Game&quot;) (&quot;Amusement Facilities&quot;) being conducted on the Portal, be referred to as &quot;User&quot; or &quot;you&quot; or &quot;your&quot; and shall be bound by this Terms of Service Privacy Policy.</p>\r\n\r\n<p>As per recent changes made to the Telangana State Gaming Act 1974, online Rummy for cash has been banned in the State of Telangana. In compliance with the Ordinance we have made changes to our services and have stopped offering online rummy game for cash in Telangana.</p>\r\n\r\n<p>If you access the Service for making deposits or playing Cash Games from a jurisdiction where Cash Games are not permitted, you shall be entirely liable for any legal or penal consequences and we shall be entitled to forfeit the balance in your account.</p>\r\n\r\n<h2>Introduction</h2>\r\n\r\n<p>Your use of the products and/or services (hereinafter referred to as &quot;Services&quot;) currently offered or to be offered in future by Play Games24x7 Private Limited, its subsidiaries, affiliates, licensors, associates and partners (hereinafter referred as &quot;Play Games24x7&quot;) through the Portal, in which&nbsp;<a href=\"https://www.rummycircle.com/\">www.rummycircle.com</a>, www.my11circle.com are referred as website and collectively as websites (hereinafter referred as &quot;Website&quot; or &quot;Websites&quot;) is subject to and governed by these Terms of Service, Privacy Policy, Help, Promotions, Add Cash, My Account and Bring-A-Friend sections collective or respectively for each Website including the sub-sections of these sections on the Websites, as the case may be (hereinafter referred as &quot;Terms&quot;).</p>\r\n\r\n<p>You understand that the Terms will be binding on you. You agree that Services offered on the Portal can be accessed only in accordance with the Terms and you shall be responsible to comply with the Terms at all times. You are responsible to be aware of and agree to abide by the Terms as published and periodically amended or modified by Play Games24x7.</p>\r\n\r\n<p>If any of the Terms are determined to be unlawful, invalid, void or unenforceable for any reason by any judicial or quasi - judicial body in India, it will not affect the validity and enforceability of the remaining Terms.</p>\r\n\r\n<p>Our failure or delay to act or exercise any right or remedy with respect to a breach of any of the Terms by you shall not be construed as a waiver of our right to act with respect to the said breach or any prior, concurrent, subsequent or similar breaches. If a promotion, game, event, competition or tournament is organized by us on a Website, it shall be governed by the Terms and any supplementary terms and conditions which may be specifically applied for that promotion, game, event, competition or tournament.</p>\r\n\r\n<h2>APPLICABILITY</h2>\r\n\r\n<p><strong>BEFORE REGISTERING WITH US, YOU SHOULD CAREFULLY READ AND REVIEW THESE TERMS PROVIDED ON PLAY GAMES24X7 SITE WHICH ARE APPLICABLE TO ALL SERVICES ON THE WEBSITES AND ALSO THE PRODUCT-SPECIFIC CONDITIONS AND RULES APPLICABLE TO SPECIFIC OFFERINGS..</strong></p>\r\n\r\n<p>Links to the following sections of these Terms are provided below for easy access to the sections you intend to refer to. However, you should read all Terms as you will be bound by them all. Click on any of the following link to access that section directly.</p>\r\n\r\n<h2>1. Legality</h2>\r\n\r\n<p>You may only use the Services to play Cash Games (as defined below) if you are 18 years of age or over. Access to our Services or any part thereof may be restricted by us from time to time in our sole decision. You confirm that you are not accessing the Services to play Cash Games from</p>\r\n\r\n<ul>\r\n	<li>Outside India or from the States of Assam, Odisha, Telangana in India for accessing&nbsp;<a href=\"https://www.cloverrummy.com/\" target=\"_blank\">www.cloverrummy.com</a>.</li>\r\n	<li>Outside India or from the States of Assam, Odisha, Telangana, Sikkim and Nagaland in India for accessing&nbsp;<a href=\"https://www.cloverrummy.com/\" target=\"_blank\">www.cloverrummy.com</a></li>\r\n</ul>\r\n\r\n<h2>2. Game Services</h2>\r\n\r\n<ul>\r\n	<li>All tournaments, promotional games, practice games and cash games organized on the Websites are collectively referred as &quot;Games&quot;. The rules applicable to each type of Game are provided under the Help section on the respective Website.</li>\r\n	<li>&quot;Cash Game(s)&quot; are Games that require the participant to have a certain minimum cash balance in their user account to participate. All other Games offered on the Websites are defined as Non-Cash Game(s).</li>\r\n	<li>Play Games24x7 charges service charges for Cash Games, which may vary depending on the nature of the Cash Game and are subject to change from time to time. Non-Cash Games are offered free on the Website, but may be subject to entry restrictions in some cases. Service charges charged by Play Games24x7 are inclusive of all applicable taxes, including GST.</li>\r\n</ul>\r\n\r\n<h2>3. User representations</h2>\r\n\r\n<ul>\r\n	<li>Any information provided by you to us, whether at the stage of registration or during anytime subsequently, should be complete and truthful.</li>\r\n	<li>Prior to adding cash to your user account or participating in Cash Games, you shall be responsible to satisfy yourself about the legality of playing Cash Games in the jurisdiction from where you are accessing Cash Games. If you are not legally competent to individually enter into Indian Rupee transactions through banking channels in India and/or are not accessing any Website from a permitted jurisdiction, you are prohibited from participating in Cash Games on the website. In the event of such violation, your participation in Cash Games will be deemed to be in breach of the Terms and you will not be entitled to receive any prize that you might win in such Cash Games.</li>\r\n	<li>You represent that you are 18 years of age or older to participate in any Cash Games and are also otherwise competent to enter into transactions with other users and Play Games24x7. You are aware that participation in the Games organized by us (&quot;Activity&quot;) may result in financial loss to you. With full knowledge of the facts and circumstances surrounding this Activity, you are voluntarily participating in the Activity and assume all responsibility for and risk resulting from your participation, including all risk of financial loss. You agree to indemnify and hold Play Games24x7, its employees, directors, officers, and agents harmless with respect to any and all claims and costs associated with your participation in the Activity.</li>\r\n	<li>You represent that you have the experience and the requisite skills required to participate in the Activity and that you are not aware of any physical or mental condition that would impair your capability to fully participate in the Activity. You further acknowledge that you are solely responsible for any consequence resulting from your participating in this Activity or being associated with this Activity or around this Activity. You understand that Play Games24x7 assumes no liability or responsibility for any financial loss that you may sustain as a result of participation in the Activity.</li>\r\n	<li>You understand and accept that your participation in a Game available on the Websites does not create any obligation on us to give you a prize. Your winning a prize is entirely dependent on your skill as a player vis-a-vis other players in the Game and subject to the rules of the Game.</li>\r\n	<li>You understand and agree that you are solely responsible for all content posted, transmitted, uploaded or otherwise made available on the Websites by you. All content posted by you must be legally owned by or licensed to you. By publishing any content on the Website, you agree to grant us a royalty-free, world-wide, non-exclusive, perpetual and assignable right to use, copy, reproduce, modify, adapt, publish, edit, translate, create derivative works from, transmit, distribute, publicly display, and publicly perform your content and to use such content in any related marketing materials produced by us or our affiliates. Such content may include, without limitation, your name, username, location, messages, gender or pictures. You also understand that you do not obtain any rights, legal or equitable, in any material incorporating your content. You further agree and acknowledge that Play Games24x7 has the right to use in any manner whatsoever, all communication or feedback provided by you.</li>\r\n	<li>You understand and accept that Play Games24x7 reserves the right to record any and all user content produced by way of but not limited to chat messages on the Websites through the Play Games24x7 feature, through the in-game chat facility or other interactive features, if any, offered as part of the Services.</li>\r\n	<li>You understand that the funds in your user account held by Play Games24x7 do not carry any interest or return.</li>\r\n	<li>You shall not hold Play Games24x7 responsible for not being able to play any Game for which you may be eligible to participate. This includes but is not limited to situations where you are unable to log into your user account as your user account may be pending validation or you may be in suspected or established violation of any of the Terms.</li>\r\n	<li>You understand and accept that by viewing or using the Websites or availing of any Services, or using communication features on the Website, you may be exposed to content posted by other users which you may find offensive, objectionable or indecent. You may bring such content posted by other users to our notice that you may find offensive, objectionable or indecent and we reserve the right to act upon it as we may deem fit. The decision taken by us on this regard shall be final and binding on you.</li>\r\n</ul>\r\n\r\n<h2>4. User Account Creation &amp; Operation</h2>\r\n\r\n<ul>\r\n	<li>To use our Services, you will need to register with us on the Portal.</li>\r\n	<li>By completing the online registration process on the Portal, you confirm your acceptance of the Terms.</li>\r\n	<li>By registering on Play Games24x7, you agree to receive promotional messages relating to Tournaments &amp; Bonus through SMS, Email and Push Notifications. You may withdraw your consent by sending an email to&nbsp;<a href=\"mailto:support@cloverrummy.com\">support@cloverrummy.com</a>&nbsp;or&nbsp;<a href=\"mailto:info@cloverrummy.com\">info@cloverrummy.com</a>&nbsp;as the case may be.</li>\r\n	<li>During the registration process, you will be required to choose a login name and a password in addition to providing some other information which may not be mandatory. Additionally, you may be required to give further personal information for your user account verification and/or for adding cash to your user account. You must give us the correct details in all fields requiring your personal information, including, without limitation, your name, postal address, email address, telephone number(s) etc. You undertake that you will update this information and keep it current.</li>\r\n	<li>You acknowledge that we may, at any time, require you to verify the correctness of this information and in order to do so may require additional documentary proof from you, failing which we reserve the right to suspend or terminate your registration on the Website.</li>\r\n	<li>Any information provided by you to us should be complete and truthful to the best of your knowledge. We are not obliged to cross check or verify information provided by you and we will not take any responsibility for any outcome or consequence as a result of you providing incorrect information or concealing any relevant information from us.</li>\r\n	<li>You understand that it is your responsibility to protect the information you provide on the Websites including but not limited to your Username, Password, Email address, Contact Details and Mobile number. Play Games24x7 will not ask for your user account login password which is only to be entered at the time of login. At no other time should you provide your user account information to any user logged in on the Websites or elsewhere. You undertake that you will not allow / login and then allow, any other person to play from your user account using your username. You specifically understand and agree that we will not incur any liability for information provided by you to anyone which may result in your user account on the Websites being exposed or misused by any other person.</li>\r\n	<li>You agree to use your Play Games24x7 user account solely for the purpose of playing on the Websites and for transactions which you may have to carry out in connection with availing the Services on the Websites. Use or attempted use of your user account for any reason other than what is stated in the Terms may result in immediate termination of your user account and forfeiture of any prize, bonus or balance in the user account.</li>\r\n	<li>You also understand and agree that deposits in your user account maintained with us are purely for the purpose of participation in Cash Games made available on the Website.</li>\r\n	<li>You understand and agree that you cannot transfer any sum from your user account with us to the account of another registered user on the Portal except as may be permitted by Play Games24x7 and subject to restrictions and conditions as may be prescribed.</li>\r\n	<li>We are legally obliged to deduct tax at source (TDS) on winnings of more than&nbsp;<em>&nbsp;</em>10,000/- in a single tournament, a single Pool Rummy game, or a single Points Rummy game&nbsp;<a href=\"https://www.cloverrummy.com/help/user/tds.html\">(click here for illustrations) on www.cloverrummy.com</a>, or a single contest on my11circle&nbsp;<a href=\"https://www.rummycircle.com/help/user/tds.html\">(click here for illustrations) on www.my11circle.com</a>, as the case may be. In these cases, you will be required to furnish your Permanent Account Number (PAN) duly issued to you by the Income Tax authorities if you have not already done so. TDS at the rate of 30% will automatically be deducted from such winnings and the rest will be credited to your Play Games24x7 user account. Withdrawal of these winnings will only be permitted upon your providing your correct PAN details. These limits and rates are subject to change as per the prevailing rules and regulations. Play Games24x7&#39;s obligation is this regard is limited to deducting TDS as required by law and providing you an appropriate certificate of tax deduction. We neither advise you nor shall in any manner be responsible for your individual tax matters.</li>\r\n	<li>We reserve the right to verify your PAN from time to time and to cancel any prize should your PAN be found inconsistent in our verification process.</li>\r\n</ul>\r\n\r\n<h2>5. User Account validation and personal information verification</h2>\r\n\r\n<ul>\r\n	<li>Play Games24x7 may from time to time attempt to validate its players&#39; user accounts. These attempts may be made via a phone call or via email. In the event that we are not able to get in touch with you the first time around, we will make additional attempts to establish contact with you. If the phone number and email provided by you is not correct, we bear no responsibility for the Services being interrupted due to our being unable to establish contact with you.</li>\r\n	<li>If we are unable to reach you or if the validation is unsuccessful, we reserve the right to disallow you from logging into the Portal or reduce your play limits and/or Add Cash limits until we are able to satisfactorily validate your user account. We will in such events email you to notify you of the next steps regarding user account validation. We may also ask you for proof of identification and proof of address from time to time.</li>\r\n	<li>Upon receipt of suitable documents, we will try our best to enable your user account at the earliest. However, it may take a few business days to reinstate your user account.</li>\r\n	<li>In the event that we have made several attempts to reach out to you but have been unable to do so, we also reserve the right to permanently suspend your user account and refund the amount, if any, in your user account to the financial instrument through which the payment was made to your user account or by cheque to the address provided by you. In the event the address provided by you is incorrect, Play Games24x7 will not make any additional attempts for delivery of the cheque unless a correct address is provided by you and charges for redelivery as prescribed by Play Games24x7 are paid by you.</li>\r\n	<li>The Privacy Policy of our Websites form a part of the Terms. All personal information which is of such nature that requires protection from unauthorized dissemination shall be dealt with in the manner provided in the&nbsp;<a href=\"https://www.cloverrummy.com/misc/privacy-policy.html\">Privacy Policy</a>&nbsp;of the Website.</li>\r\n</ul>\r\n\r\n<h2>6. User restrictions</h2>\r\n\r\n<ul>\r\n	<li><strong>Anti-Cheating and Anti-Collusion:</strong></li>\r\n	<li>You undertake that you yourself will play in all Games in which you have registered/joined and not use any form of external assistance to play. You shall not add unauthorized components, create or use cheats, exploits, bots, hacks or any other third-party software designed to modify the Websites or use any third-party software that intercepts, mines or otherwise collects information from or through the Websites or through any Services. Any attempt to employ any such external assistance is strictly prohibited.</li>\r\n	<li>Formation of teams for the purpose of collusion between you and any other user(s) for participating in Games organized on the Websites or any other form of cheating is strictly prohibited.</li>\r\n	<li>When collusion or cheating is detected on www.rummycircle.com, Play Games24x7 shall settle the Game as per its&nbsp;<a href=\"https://www.cloverrummy.com/help/cancellation.html\">&quot;Game Cancellation Settlement Policy&quot;</a>&nbsp;and may take further appropriate action against offending users in terms hereof.</li>\r\n	<li><strong>Money Laundering:</strong>&nbsp;You are prohibited from doing any activity on the Website that may be construed as money laundering, including, without limitation, attempting to withdraw cash from unutilized cash added through credit cards or deliberately losing money to a certain player(s).</li>\r\n	<li><strong>Anti-SPAMMING:</strong>&nbsp;Sending SPAM emails or any other form of unsolicited communication for obtaining registrations on the Website to benefit from any promotional program of Play Games24x7 or for any other purpose is strictly prohibited.</li>\r\n	<li><strong>Multiple IDs:</strong>&nbsp;Your registration on the Portal is restricted to a single user account which will be used by you to avail of the Services provided on the Portal. You are prohibited from creating or using multiple user IDs for registering on the Portal.</li>\r\n	<li>You may not create a login name or password or upload, distribute, transmit, publish or post content through or on the Websites or through any service or facility including any messaging facility provided by the Websites which :\r\n	<ul>\r\n		<li>is libelous, defamatory, obscene, intimidating, invasive of privacy, abusive, illegal, harassing;</li>\r\n		<li>contains expressions of hatred, hurting religious sentiments, racial discrimination or pornography;</li>\r\n		<li>is otherwise objectionable or undesirable (whether or not unlawful);</li>\r\n		<li>would constitute incitement to commit a criminal offence;</li>\r\n		<li>violates the rights of any person;</li>\r\n		<li>is aimed at soliciting donations or other form of help;</li>\r\n		<li>violates the intellectual property of any person;</li>\r\n		<li>disparage in any manner Play Games24x7 or any of its subsidiaries, affiliates, licensors, associates, partners, sponsors, products, services, or websites;</li>\r\n		<li>promotes a competing service or product; or</li>\r\n		<li>violates any laws</li>\r\n	</ul>\r\n	</li>\r\n	<li>In the event we determine that the login name created by you is indecent, objectionable, offensive or otherwise undesirable, we shall notify you of the same and you shall promptly provide us with an alternate login name so that we can change your existing login name to the new name provided by you. If you fail to provide an alternate name, we reserve the right to either permanently suspend your user account or restore your user account only after a different acceptable login name has been provided by you.</li>\r\n	<li>You shall not host, intercept, emulate or redirect proprietary communication protocols, used by the Website, if any, regardless of the method used, including protocol emulation, reverse engineering or modification of the Websites or any files that are part of the Websites.</li>\r\n	<li>You shall not frame the Portal. You may not impose editorial comments, commercial material or any information on the Websites, alter or modify Content on the Websites, or remove, obliterate or obstruct any proprietary notices or labels.</li>\r\n	<li>You shall not use Services on the Websites for commercial purposes including but not limited to use in a cyber cafe as a computer gaming centre, network play over the Internet or through gaming networks or connection to an unauthorized server that copies the gaming experience on the Website.</li>\r\n	<li>You shall not upload, distribute or publish through the Websites, any content which may contain viruses or computer contaminants (as defined in the Information Technology Act 2000 or such other laws in force in India at the relevant time) which may interrupt, destroy, limit the functionality or disrupt any software, hardware or other equipment belonging to us or that aids in providing the services offered by Play Games24x7. You shall not disseminate or upload viruses, programs, or software whether it is harmful to the Websites or not. Additionally, you shall not impersonate another person or user, attempt to get a password, other user account information, or other private information from a user, or harvest email addresses or other information.</li>\r\n	<li>You shall not purchase, sell, trade, rent, lease, license, grant a security interest in, or transfer your user account, Content, currency, points, standings, rankings, ratings, or any other attributes appearing in, originating from or associated with the Website.</li>\r\n	<li>Any form of fraudulent activity including, attempting to use or using any other person&#39;s credit card(s), debit cards, net-banking usernames, passwords, authorization codes, prepaid cash cards, mobile phones for adding cash to your user account is strictly prohibited.</li>\r\n	<li>Accessing or attempting to access the Services through someone else&#39;s user account is strictly prohibited.</li>\r\n	<li>Winnings, bonuses and prizes are unique to the player and are non-transferable. In the event you attempt to transfer any winnings, bonuses or prizes, these will be forfeited.</li>\r\n	<li>If you are an officer, director, employee, consultant or agent of Play Games24x7 or a relative of such persons (&quot;Associated Person&quot;), you are not permitted to play either directly or indirectly, any Games which entitle you to any prize on the Websites, other than in the course of your engagement with Play Games24x7. For these purposes, the term &#39;relative&#39; shall include spouse and financially dependent parents and, children.</li>\r\n	<li>You shall not post any material or comment, on any media available for public access, which in our sole discretion, is defamatory or detrimental to our business interests, notwithstanding the fact that such media is not owned or controlled by us. In addition to any other action that we may take pursuant to the provision hereof, we reserve the right to remove any and all material or comments posted by you and restrict your access to any media available for public access that is either controlled or moderate by us; when in our sole opinion, any such material or comments posted by you is defamatory or detrimental to our business interests.</li>\r\n</ul>\r\n\r\n<h2>7. Payments and Player Funds</h2>\r\n\r\n<ul>\r\n	<li>All transactions on the Website shall be in Indian Rupees.</li>\r\n	<li>Once you register on our Portal, we maintain a user account for you to keep a record of all your transactions with us. Payments connected with participation in Cash Games have to be made through your Play Games24x7 user account. All cash prizes won by you are credited by us into this user account.</li>\r\n	<li>When making a payment, please ensure that the instrument used to make the payment is your own and is used to Add Cash into your user account only.</li>\r\n	<li>Subject to the Add Cash limits specified by us from time to time, you are free to deposit as much money as you want in your user account for the purpose of participating in Cash Games on the Websites.</li>\r\n	<li>Play Games24x7 wants you to play responsibly on the Websites. The ability to Add Cash in your user account shall be subject to monthly Add Cash limits which we can be set by us with undertakings, indemnity, waiver and verification conditions as we deem appropriate in our sole discretion.</li>\r\n	<li>Credit card, Debit card, prepaid cash cards and internet banking payments are processed through third party payment gateways. Similarly, other payment modes also require an authorization by the intermediary which processes payments. We are not responsible for delays or denials at their end and processing of payments will be solely in terms of their policies and procedures without any responsibility or risk at our end. If there are any issues in connection with adding cash, a complaint may be sent to us following the complaints procedure provided in &quot;Complaints and disputes&quot; section below. You agree that in such an event of your credit being delayed or eventually declined for reasons beyond our control, we will not be held liable in any manner whatsoever. Once a payment/transaction is authorized, the funds are credited to your user account and are available for you to play Cash Games.</li>\r\n	<li>We have the right to cancel a transaction at any point of time solely according to our discretion in which case if the payment is successful, then the transaction will be reversed and the money credited back to your payment instrument.</li>\r\n	<li>Player funds are held in trust by us in specified bank accounts. Play Games24x7 keeps all players&#39; funds unencumbered which will be remitted to you in due course subject to the terms and conditions applicable to withdrawal of funds. Funds held in your user account are held separately from our corporate funds. Even in the highly unlikely event of an insolvency proceeding, you claims on the deposits will be given preference over all other claims to the extent permissible by law.</li>\r\n</ul>\r\n\r\n<h2>8. Withdrawals</h2>\r\n\r\n<ol>\r\n	<li>You may withdraw your winnings by means of either an account payee cheque or an electronic bank to bank transfer for the amount of winnings.</li>\r\n	<li>You agree that all withdrawals you make are governed by the following conditions:\r\n	<ul>\r\n		<li>Play Games24x7 can ask you for KYC documents to verify your address and identity at any stage. Withdrawals will be permitted only from accounts for which such KYC process is complete.</li>\r\n		<li>You can choose to withdraw money from your user account at any time, subject to bonus/prize money withdrawal restrictions, by notifying us of your withdrawal request. Bonuses (except Bring a Friend bonus) and promotional winnings are subject to withdrawal restrictions and can only be withdrawn on fulfilling some preconditions, one of which is that you have made at least one cash deposit on the Portal and thereafter played at least one Cash Game.</li>\r\n		<li>Once notified, post verification of the withdrawal request, we may disburse the specified amount by cheque or electronic transfer based on the mode of withdrawal selected by you. We shall make our best efforts to honor your choice on the mode of withdrawal, but reserve the right to always disburse the specified amount to you by cheque. We also reserve the right to disburse the amount on the financial instrument used to Add Cash to your user account.</li>\r\n		<li>Withdrawals attract processing charges as per the prevalent policy. You may be eligible to make one or more free withdrawals in a month depending on various factors including your club status, the amount of withdrawal or the mode of withdrawal. In the event that you do not have an adequate amount in your user account to pay the processing charge, your withdrawal will not be processed at that time. You may request for a free withdrawal subsequently if you become eligible for the same at such time.</li>\r\n		<li>If a cheque is not encashed within 30 days of dispatch, we will deem the cheque to have been lost and reserve the right to cancel the cheque with necessary instructions to our bank to stop payment of that cheque. This may result in dishonor of that cheque in which case we shall not be liable for the cheque amount to you. Any request for reissuing the cheque will result in deduction of applicable processing charges from the amount of the cheque on reissuance.</li>\r\n		<li>We will attempt our best to process your withdrawals in a timely manner, but there could be delays due to the time required for verification and completing the withdrawal transaction. We shall not be liable to pay you any form of compensation for the reason of delays in remitting payments to you from your user account.</li>\r\n		<li>To be eligible to win a prize, you must be a resident of India and accessing the Services of Play Games24x7 on the Services from India.</li>\r\n	</ul>\r\n	</li>\r\n	<li>If you are a prize winner resident in India and physically present in India while accessing the services of Play Games24x7 but not an Indian citizen, we will remit your winnings in Indian Rupees to the address/bank account given by you, provided, the address and bank account is within India.</li>\r\n</ol>\r\n\r\n<h2>9. Service Disruptions</h2>\r\n\r\n<ul>\r\n	<li>You may face Service disruptions, including, but not limited to disconnection or communication interferences due to issues in the internet infrastructure used for providing or accessing the Services or due to issues with the hardware and software used by you. You understand that Play Games24x7 has no control over these factors. Play Games24x7 shall not be responsible for any interruption in Services and you take full responsibility for any risk of loss due to Service interruptions for any such reason.</li>\r\n	<li>You understand, acknowledge and agree to the fact that if you are unable to play in any Game due to any error or omission directly attributable to Play Games24x7, including technical or other glitches at our end, the settlement of such Games will be as per the&nbsp;<a href=\"https://www.cloverrummy.com/help/cancellation.html\">&quot;Game Cancellation&quot; Settlement Policy for cloverrummy.com</a>&nbsp;and entry fee would be refunded in the case of my11circle.com.</li>\r\n</ul>\r\n\r\n<p>It is clarified that, notwithstanding any limitation of liability as specified herein, Play Games24x7 specifically disclaims any liability in the case of Games where there is no money paid by you.</p>\r\n\r\n<p>You agree that under no circumstances shall you compel Play Games24x7 or hold Play Games24x7 liable to pay you any amount over and above the service charges for any of the aforementioned errors/omissions of Play Games24x7.</p>\r\n\r\n<h2>10. Content</h2>\r\n\r\n<ul>\r\n	<li>All content and material on the Websites including but not limited to information, images, marks, logos, designs, pictures, graphics, text content, hyperlinks, multimedia clips, animation, games and software (collectively referred to as &quot;Content&quot;), whether or not belonging to Play Games24x7, are protected by applicable intellectual property laws. Additionally, all chat content, messages, images, recommendations, emails, images sent by any user can be logged/recorded by us and shall form part of Content and Play Games24x7 is free to use this material in any manner whatsoever.</li>\r\n	<li>The Websites may contain information about or hyperlinks to third parties. In such a cases, we are not responsible in any manner and do not extend any express or implied warranty to the accuracy, integrity or quality of the content belonging to such third party websites. If you rely on any third party Content posted on any Website which does not belong to Play Games24x7, you may do so solely at your own risk and liability.</li>\r\n	<li>If you visit any third party website through a third party Content posted on the website, you will be subject to terms and conditions applicable to it. We neither control nor are responsible for content on such third-party websites. The fact of a link existing on our Websites to a third-party website is not an endorsement of that website by us.</li>\r\n</ul>\r\n\r\n<h2>11. Promotions</h2>\r\n\r\n<ul>\r\n	<li>Bring A Friend (&quot;BAF&quot;): You can invite a friend, acquaintance or any other person to use the Services available on the Websites by accessing the &quot;Bring - A - Friend&quot; feature on the Website. The BAF bonus plan shall be governed by the additional terms and conditions provided in the BAF section on the respective Website.</li>\r\n	<li>Cash bonuses may be subject to withdrawal restrictions. All bonus plans shall be governed by the additional terms and conditions applicable to that bonus plan which will apply in addition to the Terms.</li>\r\n	<li>The details of various promotions organized on the Websites can be found in the Promotions section on that Website. Eligibility and applicable conditions for the ongoing promotional programs are provided in the Promotions section, which form a part of the Terms.</li>\r\n	<li>Games offered under the Promotions section may be cancelled or discontinued by Play Games24x7 at any time without notice without any liability on Play Games24x7 whatsoever, except refund of entry fee, if applicable.</li>\r\n</ul>\r\n\r\n<h2>12. RummyRewardzTM</h2>\r\n\r\n<p>Details of RummyRewardzTM&nbsp;program of Play Games24x7 are provided in the RummyRewardzTM&nbsp;section on the www.cloverrummy.com. RummyRewardzTMProgram may be altered or discontinued by Play Games24x7 at any time without notice and without any liability on Play Games24x7 whatsoever.</p>\r\n\r\n<p>In addition, Play Games24x7 may, but will not be obligated to, run RummyRewardz and/or other loyalty programs which may entail grant of reward points or such other loyalty rewards which may be redeemed in a manner prescribed in its specific terms and conditions.</p>\r\n\r\n<p>The terms and conditions of any and all loyalty programs can be modified by Games24x7 with or without notice at any time. Play Games24x7&rsquo;s reserves the right at its sole discretion to terminate its loyalty programs, if any, and revoke any unredeemed loyalty points with or without notice. Games24x7 shall not be liable to pay any compensation for loyalty points remaining in your user account in any event whatsoever, including, without limitation, the discontinuation of such loyalty programs by Play Games24x7.</p>\r\n\r\n<h2>13. License Agreement &amp; Intellectual Property</h2>\r\n\r\n<ul>\r\n	<li>All Content on the Websites shall be utilized only for the purpose of availing Services in conformity with the Terms.</li>\r\n	<li>You acknowledge that all ownership rights and all copyright and other intellectual property rights in the Content are owned by Play Games24x7 or our licensors and that you have no right title or other interest in any such items except as expressly stated in the Terms.</li>\r\n	<li>You are granted a personal, non-exclusive, non-assignable and non-transferable license to use the Content solely for the purposes of accessing and using the Services and for no other purpose whatsoever.</li>\r\n	<li>You shall not sublicense, assign or transfer the license granted to you, or rent or lease or part with the whole or any part of such license or of the Content included in such license.</li>\r\n	<li>You may not transfer, copy, reproduce, distribute, exploit, reverse engineer, disassemble, translate, decode, alter, make derivations from or make any other use of Content on the Websites in any manner other than as permitted for obtaining the Services provided on the website.</li>\r\n	<li>You may not hyperlink the Websites to any other website without permission from us.</li>\r\n	<li>You may access information on download and print extracts from the Websites for your personal use only. No right, title or interest in any downloaded materials or software is transferred to you by downloading and you are expressly prohibited from using such materials for any commercial purpose unless agreed with us in writing.</li>\r\n</ul>\r\n\r\n<h2>14. Voluntary termination</h2>\r\n\r\n<p>You are free to discontinue use of the Services on any Website at any time by intimating us of your desire to do so by sending an email to us at&nbsp;<a href=\"mailto:support@cloverrummy.com\">support@cloverrummy.com</a>&nbsp;or&nbsp;<a href=\"mailto:info@cloverrummy.com\">info@cloverrummy.com</a>&nbsp;as the case may be. If at such time, there is a positive withdrawable cash balance in your user account, we will, subject to satisfactory verification, disburse the same to you by online transfer or by a cheque in a timely manner.</p>\r\n\r\n<h2>15. User Account suspension</h2>\r\n\r\n<ul>\r\n	<li>We may suspend or otherwise put restrictions on your access to the Services on the Websites during investigation for any of the following reasons:</li>\r\n	<li>Suspected violation of Terms or other abuse of your user account;</li>\r\n	<li>Suspected breach of security of your user account; or</li>\r\n	<li>If there have been charge-backs on your user account.</li>\r\n	<li>Our decision to suspend or restrict Service or any part thereof as we deem appropriate shall be final and binding on you.</li>\r\n</ul>\r\n\r\n<h2>16. Breach and consequences</h2>\r\n\r\n<ul>\r\n	<li>In the event of breach of any of the Terms being evidenced from our investigation or if there is reasonable belief, in our sole discretion, that your continued access to the Portal or any Website is detrimental to the interests of Play Games24x7, our other users or the general public; we may in our sole discretion take any or all of the following actions:</li>\r\n	<li>Restrict games between users suspected of colluding or cheating;</li>\r\n	<li>Permanently suspend your user account on the Website;</li>\r\n	<li>Forfeit the cash balance in your user account;</li>\r\n	<li>Demand damages for breach and take appropriate civil action to recover such damages; and/or</li>\r\n	<li>Initiate prosecution for violations that amount to offences in law.</li>\r\n	<li>Additionally, in the event of committing material breach hereof, we reserve the right to bar you from future registration on the Website.</li>\r\n	<li>The decision of Play Games24x7 on the action to be taken as a consequence of breach shall be final and binding on you.</li>\r\n	<li>Any action taken by Play Games24x7 shall be without prejudice to our other rights and remedies available in law or equity.</li>\r\n</ul>\r\n\r\n<h2>17. Complaints &amp; disputes</h2>\r\n\r\n<ul>\r\n	<li>If you have a complaint, you should in the first instance contact the customer support team at&nbsp;<a href=\"mailto:support@cloverrummy.com\">support@cloverrummy.com</a>&nbsp;or&nbsp;<a href=\"mailto:info@cloverrummy.com\">info@cloverrummy.com</a>, as the case may be or write to us following the procedure given in the Contact Us section on the Websites. Complaints should be made as soon as possible after circumstances arise that cause you to have a complaint.</li>\r\n	<li>You accept that any complaints and disputes are and remain confidential both whilst a resolution is sought and afterwards. You agree that you shall not disclose the existence, nature or any detail of any complaint or dispute to any third party.</li>\r\n	<li>Play Games24x7 shall make efforts to resolve complaints within reasonable time.</li>\r\n	<li>Our decision on complaints shall be final and binding on you.</li>\r\n</ul>\r\n\r\n<h2>18. Modifications and alterations</h2>\r\n\r\n<ul>\r\n	<li>We may alter or modify the Terms at any time without giving prior notice to you. We may choose to notify of some changes in the Terms either by email or by displaying a message on the Websites; however, our notification of any change shall not waive your obligation to keep yourself updated about the changes in the Terms. Your continued use of any Website and/or any Services offered constitutes your unconditional acceptance of the modified or amended Terms.</li>\r\n	<li>We may also post supplementary conditions for any Services that may be offered. In such an event, your use of those Services will be governed by the Terms as well as any such supplementary terms that those Services may be subject to.</li>\r\n</ul>\r\n\r\n<h2>19. Limitation of liability</h2>\r\n\r\n<ol>\r\n	<li>In addition to specific references to limitation of liability of Play Games24x7 elsewhere in the Terms, under no circumstances (including, without limitation, in contract, negligence or other tort), Play Games24x7 shall be liable for any injury, loss, claim, loss of data, loss of income, loss of profit or loss of opportunity, loss of or damage to property, general damages or any direct, indirect, special, incidental, consequential, exemplary or punitive damages of any kind whatsoever arising out of or in connection with your access to, or use of, or inability to access or use, the Services on any Website. You further agree to indemnify us and our service providers and licensors against any claims in respect of any such matter.</li>\r\n	<li>Without limiting the generality of the foregoing, you specifically acknowledge, agree and accept that we are not liable to you for:\r\n	<ul>\r\n		<li>the defamatory, undesirable or illegal conduct of any other user of the Services;</li>\r\n		<li>any loss whatsoever arising from the use, abuse or misuse of your user account or any feature of our Services on the Websites;</li>\r\n		<li>any loss incurred in transmitting information from or to us or from or to our Websites by the internet or by other connecting media;</li>\r\n		<li>any technical failures, breakdowns, defects, delays, interruptions, improper or manipulated data transmission, data loss or corruption or communications&#39; infrastructure failure, viruses or any other adverse technological occurrences arising in connection with your access to or use of our Services;</li>\r\n		<li>the accuracy, completeness or currency of any information services provided on the Websites;</li>\r\n		<li>any delay or failure on our part to intimate you where we may have concerns about your activities; and</li>\r\n		<li>your activities / transactions on third party websites accessed through links or advertisements posted in the Website.</li>\r\n	</ul>\r\n	</li>\r\n	<li>Notwithstanding anything to the contrary contained in the Terms or elsewhere, you agree that our maximum aggregate liability for all your claims under this agreement, in all circumstances, other than for the payment of any withdrawable balance in your user account, shall be limited to Indian Rupees One Thousand only (INR. 1,000/-)</li>\r\n</ol>\r\n\r\n<h2>20. Disclaimer and indemnity</h2>\r\n\r\n<p><strong>Disclaimer</strong></p>\r\n\r\n<ul>\r\n	<li>The Services on the Websites and the Content present on it are provided strictly on &quot;as is&quot; basis with all faults or failings. Any representations, warranties, conditions or guarantee whatsoever, express or implied (including, without limitation, any implied warranty of accuracy, completeness, uninterrupted provision, quality, merchantability, fitness for a particular purpose or non-infringement) are specifically excluded to the fullest extent permitted by law. Play Games24x7 does not ensure or guarantee continuous, error-free, secure or virus-free operation of the Websites or its Content including software, Games, your user account, the transactions in your user account or continued operation or availability of any facility on the website.</li>\r\n	<li>Additionally, Play Games24x7 does not promise or ensure that you will be able to access your user account or obtain Services whenever you want. It is entirely possible that you may not be able to access your user account or the Services provided by Play Games24x7 at times or for extended periods of time due to, but not limited to, system maintenance and updates.</li>\r\n	<li>Play Games24x7 disclaims responsibility and liability for any harm resulting from cancellation of any Game organized by it. If you are a cash player on the website, you acknowledge and agree that you will not be entitled to any refund in case of any service outages that may be caused by failures of our service providers, computer viruses or contaminants, natural disasters, war, civil disturbance, or any other cause beyond the reasonable control of Play Games24x7.</li>\r\n	<li>Play Games24x7 specifically disclaims any liability in connection with Games or events made available or organized on the Websites which may require specific statutory permissions, in the event such permissions are denied or cancelled whether prior to or during such Game or event.</li>\r\n	<li>Play Games24x7 specifically disclaims any liability in connection with your transactions with third parties which may have advertisements or are hyperlinked on the Website.</li>\r\n	<li>Play Games24x7 disclaims any liability in connection with violation of intellectual property rights of any party with respect to third party Content or user content posted on our Website. Intellectual property rights in any Content not belonging to us belong to the respective owners and any claims related to such content must be directly addressed to the respective owners.</li>\r\n	<li>Play Games24x7 specifically disclaims any liability arising out of the acts or omissions of the infrastructure providers or otherwise failure of internet services used for providing and accessing the Services.</li>\r\n	<li>Play Games24x7 disclaims liability for any risk or loss resulting to you from your participation in Cash Games, including all risk of financial loss.</li>\r\n	<li>Random Number Generator Software: iTech Labs Australia has evaluated the Random Number Generator (&quot;RNG&quot;) used by www.cloverrummy.com and has certified that the software of Play Games24x7 complies with the relevant statistical standards of randomness. You understand that Play Games24x7 maintains the said certification it considers appropriate in the best of its belief, however, it is not possible to test all possible scenarios in any testing environment and Play Games24x7 specifically disclaims any and all liability in connection with the RNG used by Play Games24x7&#39;s software.</li>\r\n</ul>\r\n\r\n<p><strong>Indemnity</strong>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>To the extent permitted by law, and in consideration for being allowed to participate in the Activity, you hereby agree to indemnify, save and hold harmless and defend us (to the extent of all benefits and awards, cost of litigation, disbursements and reasonable attorney&#39;s fees that we may incur in connection therewith including any direct, indirect or consequential losses, any loss of profit and loss of reputation) from any claims, actions, suits, taxes, damages, injuries, causes of action, penalties, interest, demands, expenses and/or awards asserted or brought against us by any person in connection with:</li>\r\n	<li>infringement of their intellectual property rights by your publication of any content on our Website.</li>\r\n	<li>defamatory, offensive or illegal conduct of any other player or for anything that turns out to be misleading, inaccurate, defamatory, threatening, obscene or otherwise illegal whether originating from another player or otherwise;</li>\r\n	<li>use, abuse or misuse of your user account on our Website in any manner whatsoever;</li>\r\n	<li>any disconnections, technical failures, system breakdowns, defects, delays, interruptions, manipulated or improper data transmission, loss or corruption of data or communication lines failure, distributed denial of service attacks, viruses or any other adverse technological occurrences arising in connection with your access to or use of our Website; and</li>\r\n	<li>access of your user account by any other person accessing the Services using your username or password, whether or not with your authorization.</li>\r\n</ul>\r\n\r\n<h2>21. Governing law, dispute resolution &amp; jurisdiction</h2>\r\n\r\n<ul>\r\n	<li>The Terms and Privacy Policy shall be interpreted in accordance with the laws of India.</li>\r\n	<li>Any dispute, controversy or claim arising out of the Terms or Privacy Policy shall be subject to the exclusive jurisdiction of the civil courts at Mumbai</li>\r\n</ul>\r\n','1525415853','1566469085',1),(3,'Refund Policy','<p><a href=\"https://www.rummycircle.com/\">Rummy</a>&nbsp;is a card game that is played with two decks of cards with total of two Jokers. To win the rummy game a player must make a valid declaration by picking and discarding cards from the two piles given. One pile is a closed deck, where a player is unable to see the card that he is picking, while the other is an open deck that is formed by the cards discarded by the players. To win at rummy card game, the player has to group the cards in 3 and 4 to form one pure sequence, one impure sequence and sets.</p>\r\n\r\n<p>In rummy, the cards in each suit rank low to high starting with Ace, 2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen and King. Ace, Jack, Queen and King each has 10 points. The remaining cards have value equal to their face value. For example, 5 cards will have 5 points and so on.</p>\r\n\r\n<h2>The Objective Of Rummy</h2>\r\n\r\n<p>The objective of rummy card game is to arrange the 13 cards in valid sets and sequences. To win the game you need a compulsory pure sequence along with an impure sequence and sets. Without a pure sequence you cannot make a valid rummy declaration. This is one of the most important rummy rules.</p>\r\n\r\n<h3><strong>How to Form Sequences?</strong></h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>In rummy, a sequence is a group of three or more consecutive cards of the same suit. There are two types of sequences that are formed; a pure sequence and an impure sequence. To win the game of rummy you need at least one pure sequence in your rummy hand.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4><strong>Pure Sequence</strong></h4>\r\n\r\n<p>A pure sequence is a group of three or more cards of the same suit, placed in consecutive order. To form a pure sequence in rummy card game, a player cannot use any Joker or wild card.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Here are a few examples of pure sequence.</p>\r\n\r\n<ol>\r\n	<li>5&hearts;&nbsp;6&hearts;&nbsp;7&hearts;&nbsp;(Pure sequence with three cards and there is no Joker or wild card used)</li>\r\n	<li>3&spades; 4&spades; 5&spades; 6&spades; (Pure sequence with four cards. There is no use of Joker or wild cards here.)</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4><strong>Impure Sequence</strong></h4>\r\n\r\n<p>An impure sequence is a group of three or more cards of the same suit with one or more Joker card used.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Here are some examples to show how an impure sequence is formed.</p>\r\n\r\n<ol>\r\n	<li>6&diams;&nbsp;7&diams;&nbsp;Q&spades; 9&diams;&nbsp;(Here Q&spades; has been used as a wild Joker replacing 8&diams;&nbsp;to form an impure sequence.)</li>\r\n	<li>5&spades; Q&hearts;&nbsp;7&spades; 8&spades; PJ (Impure sequence with Q&hearts;&nbsp;as wild joker that is replacing 6&spades; and the Printed Joker is replacing 9&spades;.)</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>How to Form Sets?</strong></h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>A set is a group of three or four cards of the same value but of different suits. When you are forming sets, you can use wild card and Jokers.</p>\r\n\r\n<p><em><strong>Examples of sets</strong></em></p>\r\n\r\n<ol>\r\n	<li>A&hearts;&nbsp;A&clubs; A&diams;&nbsp;(In this set, all the Ace are of different suits, make a valid set.)</li>\r\n	<li>8&diams;&nbsp;8&clubs; 8&spades; 8&hearts;&nbsp;(Rummy set is formed with four 8 cards of different suits.)</li>\r\n	<li>9&diams;&nbsp;Q&spades; 9&spades; 9&hearts;&nbsp;(Here Q&spades; has been used as wild joker replacing 9&clubs; to make set.)</li>\r\n	<li>5&diams;&nbsp;5&clubs; 5&spades; PJ (Printed joker replacing 5&hearts;&nbsp;to make set.)</li>\r\n</ol>\r\n\r\n<p><strong>Note:</strong>&nbsp;The set is formed with same card of different suits. However, you can&rsquo;t use two or more cards of the same suit. This is taken as an invalid declaration. Also, note that a set cannot have more than four cards. So, if you have a set of four cards and you are using an additional Joker, then in total it becomes a 5 cards group and will be an invalid set.</p>\r\n\r\n<p><em><strong>Examples of invalid set</strong></em></p>\r\n\r\n<ol>\r\n	<li>Q&hearts;&nbsp;Q&hearts;&nbsp;Q&diams;&nbsp;(There are two Qs of the same suit&nbsp;&hearts;&nbsp;making it an invalid set.)</li>\r\n	<li>7&spades; 7&hearts;&nbsp;7&nbsp;&diams;&nbsp;7&spades; Q&hearts;&nbsp;(It has two 7 spades of the same suit. Also, it has a wild card Q&hearts;&nbsp;as the fifth card. Both these points make this set as invalid.)</li>\r\n</ol>\r\n\r\n<h2>How To Play Rummy Card Game?</h2>\r\n\r\n<p>Rummy card game is played between 2 to 6 players with two decks of cards. The player has to draw and discard cards to form valid sets and sequences of the 13 cards in hand.</p>\r\n\r\n<p>To start, each player is dealt with 13 cards. One random card is picked as the Wild Card or Joker card of the game. This card can then be used to form impure sequence and sets.</p>\r\n\r\n<p>Once all the 13 cards are arranged in valid sequences and groups, he can make a declaration and win the game.</p>\r\n\r\n<h2>Quick Tips To Win The Rummy Card Game</h2>\r\n\r\n<p>Just as it&rsquo;s important to know the rummy rules, it is also necessary to play carefully and with focus. Here are quick tips to win the rummy game and stay a step ahead of your competitors.</p>\r\n\r\n<ul>\r\n	<li>Form the pure sequence at the very beginning of the game. Without a pure sequence, a player cannot make a declaration.</li>\r\n	<li>Discard cards with high points like Ace, Jack, Queen and King. Replace these cards with Joker or Wild Cards. It reduces the point load, in case you lose the game.</li>\r\n	<li>As much as possible, avoid picking from the discard pile. It gives away what hand you are trying to form.</li>\r\n	<li>Look out for smart cards. For example, a 7 of any suit can work with 5 and 6 of the same suit and also 8 and 9 of the same suit.</li>\r\n	<li>Jokers play an important role in rummy. Try using them to replace high value cards. Remember, Joker and wild cards cannot be used to form pure sequence.</li>\r\n	<li>When you are ready to make a declaration, check and recheck your cards and then press the button. An invalid declaration can turn even a winning game into a complete loss.</li>\r\n</ul>\r\n\r\n<h2>Common Terms Used In Rummy Rules</h2>\r\n\r\n<p>Here are some common terms of&nbsp;<a href=\"https://www.rummycircle.com/types-of-rummy/indian-rummy/indian-rummy.html\">Indian Rummy</a>&nbsp;that every player needs to know before he starts playing.</p>\r\n\r\n<h3><strong>What is a Rummy Table?</strong></h3>\r\n\r\n<p>It is the table where the game of rummy is played. Every rummy table can sit two to six players for each game.</p>\r\n\r\n<h3><strong>What is Joker and Wild Cards?</strong></h3>\r\n\r\n<p>In each rummy deck there is a Printed Joker and there is a Wild Card that is selected at random at the beginning of the game. The role of both these types of card is the same. Jokers are used to form sets and impure sequences. A Joker card can replace the desired number when forming the groups. This is a valid formation in a rummy game.</p>\r\n\r\n<h3><strong>What is Draw and Discard?</strong></h3>\r\n\r\n<p>In all Rummy games, each player is dealt 13 cards. Additionally, there are 2 stacks from which each player can select cards, thereby drawing a card. Once a player draws a card, he has to get rid of one card - this is called discarding. A player can draw from either the closed un-dealt cards or the open discarded pile. A player may wish to drop the game when the turn comes. However, a game can only be dropped before drawing a card.</p>\r\n\r\n<h3><strong>What is Sorting of Cards?</strong></h3>\r\n\r\n<p>Sorting of cards are done at the beginning of the game. This is done to arrange your cards to help you form your sets and sequences reducing the probability of mixing the cards. Once, the cards are displayed, you can hit the Sort button and start playing.</p>\r\n\r\n<h3><strong>What is a Drop?</strong></h3>\r\n\r\n<p>When a player decides to leave the game table at the start or middle of the rummy game, it is a drop. The act is withdrawing from the game as a personal decision. The first drop=10 points; middle drop= 30 points and last drop and maximum point loss is 80 points.</p>\r\n\r\n<p>In case of Pool rummy, if a player drops in 101 pool, the score is 20. In case, it is 201 pool rummy, the drop score is 25. In a game, where it is best of 2 and best of 3 is played, then a drop is not allowed.</p>\r\n\r\n<h3><strong>What are Cash Tournaments?</strong></h3>\r\n\r\n<p>Cash tournaments are those that are played for Real Cash and have Real Cash Prizes (in INR). These tournaments run 24x7 and are conducted in a knock-out style. To play any of the cash games, the player has to add Cash to its Clover Rummy account.</p>\r\n\r\n<h3><strong>How do I Join a Tournament?</strong></h3>\r\n\r\n<p>Go to &#39;Tournaments&#39; in the top navigation panel. Now, select the type of Tournament you wish to play. In the corresponding Tournament List, click on any of the Open tournaments that you wish to join. Finally, click on the blinking Join This Tournament button under Tournament Details.</p>\r\n\r\n<h3><strong>What is an Invalid Declaration?</strong></h3>\r\n\r\n<p>An invalid declaration happens in rummy card game when the player presses the Declaration button, but the cards are not in valid sequences and sets. Therefore, the player will lose the game and the competitor will automatically be declared as the winner.</p>\r\n\r\n<p>Here are few examples of common invalid declarations players make while playing rummy:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<h4><strong>Wrong Declaration with Invalid Sets</strong></h4>\r\n\r\n	<p>Example 1: 10&spades; 10&hearts;&nbsp;10&diams;&nbsp;10&clubs; Q&hearts;</p>\r\n\r\n	<p>A set can have a maximum of 4 cards. A set is formed with cards of the same value and different suits. In this situation, the Wild Joker (Queen of Hearts) card was added and that became the fifth card, making it an invalid set. A player needs to remember to only have four cards at maximum in one set.</p>\r\n\r\n	<p>Example 2: K&hearts;&nbsp;K&hearts;&nbsp;K&diams;</p>\r\n\r\n	<p>In this set, there are 3 cards which is well within the limit. Also, a set consists of cards of the same face value but has to be of different suits. The set cannot have more than one card of the same suit. In this example, the set consists of two cards of the same suit and that makes it a wrong declaration.</p>\r\n	</li>\r\n	<li>\r\n	<h4><strong>Wrong Declaration with Invalid Sequences</strong></h4>\r\n\r\n	<p>Example 1: 10&spades; 10&hearts;&nbsp;10&diams;&nbsp;10&clubs; | 5&spades; 5&hearts;&nbsp;5&diams;&nbsp;| 6&spades; 6&hearts;&nbsp;6&clubs; | 9&hearts;&nbsp;9&diams;&nbsp;Joker</p>\r\n\r\n	<p>A valid declaration requires one pure sequence i.e. sequence without Joker and one impure sequence i.e. sequence with Joker. However, in the given example there is neither a pure sequence or an impure sequence.</p>\r\n\r\n	<p>Example 2: K&hearts;&nbsp;K&spades; K&diams;&nbsp;| 6&hearts;&nbsp;7&hearts;&nbsp;Joker | 9&spades; 10&spades; J&spades; Joker | 5&spades; 5&hearts;&nbsp;5&diams;</p>\r\n\r\n	<p>A valid declaration must one pure sequence without Joker and one impure sequence with the use of Joker. This example shows that there is an impure sequence, however the pure sequence is not there. It is compulsory to have a pure sequence before you make a declaration.</p>\r\n\r\n	<p>Example 3: Q&hearts;&nbsp;Q&spades; Q&diams;&nbsp;| 6&hearts;&nbsp;7&hearts;&nbsp;8&hearts;&nbsp;9&hearts;&nbsp;| 5&spades; 5&hearts;&nbsp;5&diams;&nbsp;| 10&spades; 10&hearts;&nbsp;10&diams;</p>\r\n\r\n	<p>Sequences are very important for a rummy card game and you need both pure and impure sequence to win the game. In this example, there is a pure sequence, however the impure sequence is missing and hence it is an invalid declaration.</p>\r\n	</li>\r\n</ul>\r\n','1525415853','1566468542',1),(4,'FAQs','<h3 style=\"text-align:justify\"><strong>Classic Indian Rummy</strong></h3>\r\n\r\n<p style=\"text-align:justify\">The classic Indian rummy is a popular variant of 13 cards Rummy. It is a mix of Gin rummy and<br />\r\nRummy 500. What makes the game exciting are the complications that arise from forming sets<br />\r\nwith the odd number of cards dealt to them. Not only are the rules of the game simple, it<br />\r\nchallenges the player&rsquo;s memory, observation skills, concentration and quick thinking to win.</p>\r\n\r\n<h3 style=\"text-align:justify\"><br />\r\n<strong>Great Rummy Experience</strong></h3>\r\n\r\n<p style=\"text-align:justify\">The ever growing trend of rummy in India calls for a gaming arena that offers the best<br />\r\nexperience to its players. Let&rsquo;s take a break from our monotonous lives and indulge in the<br />\r\nfascinating world of rummy. Register today, with Clover Rummy and enjoy with exciting<br />\r\ntournaments, bonuses, rewards and more!</p>\r\n\r\n<h3 style=\"text-align:justify\"><br />\r\n<strong>Safe and Secure</strong></h3>\r\n\r\n<p style=\"text-align:justify\">Mr. Rummy ensures safe and fair play. User&rsquo;s personal information is held confidential. The<br />\r\ninformation you give us is used only to improve your gaming experience. We do not share your<br />\r\ninformation. We hold the protection of your privacy in the highest regard. The safety of your<br />\r\nfinancial information is guaranteed.</p>\r\n\r\n<h3 style=\"text-align:justify\"><br />\r\n<strong>Fair play</strong></h3>\r\n\r\n<p style=\"text-align:justify\">Rummy is acceptably a game of skill. Although offline rummy is mostly played amongst friends<br />\r\nand family with little to no dirt, online rummy is completely different since it is played among<br />\r\nstrangers. In order to avoid foul play and unfair practices, we at Clover Rummy strictly follow a<br />\r\nfair play policy to present the best and fair gaming experience to our users.</p>\r\n\r\n<h3 style=\"text-align:justify\"><br />\r\n<strong>Deposit and Instant withdrawal</strong></h3>\r\n\r\n<p style=\"text-align:justify\">Players of Clover Rummy can make easy deposits to their cash accounts. Play cash games to win<br />\r\nexciting prizes enjoy easy withdrawals with our flexible withdrawal policy! Join now.</p>\r\n\r\n<h3 style=\"text-align:justify\"><br />\r\n<strong>Great Rewards, Offers and Bonuses</strong></h3>\r\n\r\n<p style=\"text-align:justify\">Clover Rummy offers you a variety of generous rewards and bonuses such as the welcome<br />\r\nbonus, top up bonus and the popular refer a friend bonus. With the most minimalistic of<br />\r\nactivities, you can gain exciting rewards and bonuses.</p>\r\n','1525415853','1555045310',1),(5,'Privacy Policy','<p>This privacy policy forms part of our Website&#39;s&nbsp;<a href=\"https://www.rummycircle.com/tos/terms-of-service.html\" title=\"Terms of Service\">Terms of Service</a>, by accepting the Terms of Service and the Privacy Policy on the registration page, you consent to provide sensitive personal data or personal information and are aware of the purpose of sharing such information. Please do not register if you do not wish to share the mandatory personal information with Play Games24x7 Private Limited (hereinafter referred as &ldquo;Play Games24x7&rdquo; or &ldquo;we&rdquo; or &ldquo;us&rdquo; &ldquo;our&rdquo;) requested at the time of registration. Registration procedure cannot be completed until information in the non-optional fields is provided. Certain additional personal information may be requested by Play Games24x7 for permitting you access to any Cash Games organized by Play Games24x7.</p>\r\n\r\n<h2>1. Policy objective</h2>\r\n\r\n<p>Play Games24x7 respects your privacy and assures you that any information provided by you to Play Games24x7 is protected and will be dealt with according to this Policy and the applicable laws. To avail the services offered on&nbsp;<a href=\"https://www.rummycircle.com/\">www.</a>cloverrummy<a href=\"https://www.rummycircle.com/\">.com</a>&nbsp;and www.My11Circle.com (hereinafter singly referred as the &quot;Website&quot; and collectively referred as &ldquo;Websites&rdquo;), you may have to provide certain information to us. This Policy provides the procedure followed by Play Games24x7 to gather, uses, store, discloses and manages users&#39; personal data. Play Games24x7 only collects personal data for the purpose of verifying user accounts, maintaining the accounts of the users, completing transactions of the users and for analyzing user behavior and requirements.</p>\r\n\r\n<h2>2. What is included in personal data?</h2>\r\n\r\n<h3><strong>User&#39;s personal data includes the following types of personal data or information:</strong></h3>\r\n\r\n<ol>\r\n	<li><strong>Sensitive Personal Data:</strong>\r\n\r\n	<ul>\r\n		<li>Account password</li>\r\n		<li>Financial information such as Bank account or credit card or debit card or other payment instrument details</li>\r\n	</ul>\r\n	</li>\r\n	<li><strong>Other Personal Information:</strong>\r\n	<ul>\r\n		<li>Name</li>\r\n		<li>Date of birth</li>\r\n		<li>Telephone number</li>\r\n		<li>Postal/Contact address</li>\r\n		<li>PAN number (as applicable)</li>\r\n		<li>The IP address of your computer, browser type and language.</li>\r\n		<li>The date and the time during which you accessed the site.</li>\r\n		<li>The address of the website which you may have used to link to any Website</li>\r\n		<li>Your photograph for testimonials and other promotions.</li>\r\n		<li>Such other information that is defined as sensitive personal data or information in law.</li>\r\n	</ul>\r\n	</li>\r\n</ol>\r\n\r\n<h2>3. User Consents:</h2>\r\n\r\n<ol>\r\n	<li>\r\n	<h3><strong>Consent for use of Sensitive Personal Data and Other Personal Information:</strong></h3>\r\n\r\n	<p>All users of Play Games24x7 consent to the use of Sensitive Personal Data and Other Personal Information for the purpose stated in this policy. We restrict access of personal information to our employees, contractors and agents and only allow access to those who need to know that information in order to process it on our behalf.</p>\r\n\r\n	<p>Users are entitled to withdraw consent for use of their&nbsp;Sensitive Personal Data or Other Personal Information&nbsp;by emailing a specific request to support@cloverrummy.com or support@my11circle.com.com, as the case may be. Withdrawal of consent of all or any part of Sensitive Personal Data and Other Personal Information may result in immediate withdrawal of any right to avail the services provided by Play Games24x7. Withdrawal of consent shall not restrict the right of Play Games24x7 to use Other Personal Information for analysis of user data, subject to the condition that the Other Personal Information used in any analysis is not personally identifiable to any individual user.</p>\r\n\r\n	<p>Photographs once provided by you for marketing purposes gives Play Games24x7 irrevocable rights to use it and it is completely Play Games24x7&#39;s discretion to delete or not use it any further.</p>\r\n\r\n	<p>Play Games24x7 may also use software applications for website traffic analysis and to gather statistics, used for advertising and for determining the efficacy and popularity of Play Games24x7 among others.</p>\r\n\r\n	<p>Presently, Play Games24x7 is using display advertising and&nbsp;<a href=\"https://support.google.com/analytics/answer/2611268?hl=en\" target=\"_blank\">remarketing</a>&nbsp;through&nbsp;<a href=\"http://www.google.co.in/analytics/features/index.html\" target=\"_blank\">Google Analytics</a>, wherein third party vendors including google, display our ads on sites across the internet and uses first party cookies and third party cookies together to inform, optimize and serve ads based on visits of visitors to our website. Our visitors are free to opt out of Google Analytics for display advertising and customize google display network ads using&nbsp;<a href=\"https://www.google.com/settings/u/0/ads/authenticated?hl=en\" target=\"_blank\">ads preference manager</a>.</p>\r\n	</li>\r\n	<li>\r\n	<h3><strong>Consent to use of Cookies:</strong></h3>\r\n\r\n	<p>What is a Cookie: A cookie is a small text file that uniquely identifies your browser.</p>\r\n\r\n	<p>You understand that when you visit the Website, cookies may be left in your computer. The cookies assigned by the servers of Play Games24x7 may be used to personalize your experience on the Website. Additionally, cookies may also be used for authentication, game management, data analysis and security purposes.</p>\r\n\r\n	<p>Cookies may also be assigned by the advertisers of Play Games24x7 when you click on any of the advertisements which may be displayed on any Website in which case such cookies are controlled by these advertisers and not Play Games24x7.</p>\r\n	</li>\r\n	<li>\r\n	<h3><strong>Consent to email communication:</strong></h3>\r\n\r\n	<p>When you register your email address with Play Games24x7, you agree to receive email communication from Play Games24x7, entities specifically authorized by Play Games24x7 and other users. You also agree and acknowledge that when you use our referral program for referring someone, Play Games24x7 will send emails to such person referred by you on your behalf and the email headers will carry your email address as the address from which such emails are sent.</p>\r\n\r\n	<p>Clover Rummy may also access and store such information relating to your contacts to send invitations and other promotions to them periodically.</p>\r\n	</li>\r\n</ol>\r\n\r\n<h2>4. Data Security:</h2>\r\n\r\n<p>We take appropriate security measures to protect against unauthorized access to or unauthorized alteration, disclosure or destruction of data. These include internal reviews of our data collection, storage and processing practices and security measures, including appropriate encryption and physical security measures to guard against unauthorized access to systems where we store personal data. Play Games24x7 has a comprehensive information security program and information security policies which contain managerial, technical, operational and physical security control measures adopted by Play Games24x7 for the protection of Sensitive Personal Date and Other Personal Information.</p>\r\n\r\n<p>Information gathered by Play Games24x7 is stored securely using several information security applications including firewalls. However, security is always relative and Play Games24x7 cannot guarantee that its security measures are absolute and cannot be breached. Data which is transmitted over the Internet is inherently exposed to security risks or threats. For instance, information transmitted via chat or email can be compromised and used by others. Therefore, Play Games24x7 cannot guarantee any security for such information in the course of transmission through the internet infrastructure or any unsolicited disclosures made by any user availing the services of the Website.</p>\r\n\r\n<p>When you register with Play Games24x7, your account is protected by means of login information which includes a username and a password that is known only to you. Therefore, you should not provide your personal information to anyone whosoever and breach hereof constitutes violation of this Policy and can also result in closure of account in certain cases. If you become aware of or reasonably suspect any breach of security, including compromise of your login information, it is your responsibility to immediately notify Play Games24x7.</p>\r\n\r\n<p>The Websites may contain links to other websites. Such websites are governed by their own privacy policies and Play Games24x7 does not exercise any control over them. It is your responsibility to read and understand the privacy policy of such websites when you follow a link outside the Website. You are advised to exercise caution in sharing any personal information with any third party that advertises on the Websites and Play Games24x7 shall not be responsible for such information provided by you on third party websites.</p>\r\n\r\n<p>Play Games24x7 has a policy of not sharing any personally identifiable information with anyone other than entities specifically authorized by Play Games24x7 which may include advertisers and sponsors of Play Games24x7. However, Play Games24x7 may use your name, photo, Login ID and the state from where you are participating when announcing the results of any contests run on the Website. Such contests are further governed by terms and conditions which shall be available on the Websites as and when such a contest is run on any Website. Play Games24x7 conducts periodic analysis and survey of the traffic to cloverrummy.com for market research and advertising purposes. Play Games24x7 reserves the right to share your registration information with Play Games24x7 appointed market research and advertising companies or firms from time to time for the said purposes. Play Games24x7 may also use cumulative non-personal information for auditing and analysis purposes with the aim of improving its services.</p>\r\n\r\n<h2>5. Exclusions:</h2>\r\n\r\n<h3><strong>Play Games24x7 may share Sensitive Personal Data and Other Personal Information if sharing of such information is necessary:</strong></h3>\r\n\r\n<ul>\r\n	<li>to comply with legal processes or governmental request;</li>\r\n	<li>to enforce the Terms of Service and this Privacy Policy;</li>\r\n	<li>for prevention of fraud;</li>\r\n	<li>for issues involving information security, or</li>\r\n	<li>to protect:\r\n	<ol>\r\n		<li>your rights;</li>\r\n		<li>rights of Play Games24x7; and</li>\r\n		<li>rights of the general public.</li>\r\n	</ol>\r\n	</li>\r\n</ul>\r\n\r\n<h2>6. Limitation of Liability:</h2>\r\n\r\n<p>Play Games24x7 confirms that this Privacy Policy is only a description of its operation regarding user information. This Policy is not intended to and does not create any legal rights in your favour or in the favour of any other person. Play Games24x7 reserves the right to change this Policy at any time without giving you prior notice. The liability of Play Games24x7 shall be limited to removal of Sensitive Personal Data from the system of the Websites and removal of personally identifiable elements of the Other Personal Information. Notwithstanding anything to the contrary contained in this Policy and elsewhere, the aggregate liability of Play Games24x7 for all claims for damages pursuant to provisions of services on the Website, including claims in respect to the violation of this Policy, shall be limited to the aggregate maximum amount of liability as provided in the Terms of Service.</p>\r\n\r\n<h2>7. Grievance Officer:</h2>\r\n\r\n<p>Play Games24x7 currently appoints Mr. Vinay Pandit as the Grievance Officer. All complaints relating to personal data must be sent to the Grievance Officer at&nbsp;<a href=\"mailto:grievanceofficer@rummycircle.com\">info@</a>cloverrummy<a href=\"mailto:grievanceofficer@rummycircle.com\">.com</a>&nbsp;or&nbsp;<a href=\"mailto:grievanceofficer@my11circle.com\">info@</a>cloverrummy<a href=\"mailto:grievanceofficer@my11circle.com\">.com</a>.</p>\r\n\r\n<p>&nbsp;</p>\r\n','1525415853','1566468475',1),(6,'Legality','	<div class=\"myaccount_body_section\" style=\"padding:10px 10px;\">\r\n		<div class=\"promotion_page\">\r\n			<div class=\"promotion_right_div\">\r\n				<h3 class=\"page_heading\" style=\"color: #f00;\">Is Online Rummy Legal in India?</h3>\r\n				<div class=\"static_page\">\r\n					<div class=\"para_text\">\r\n						<div class=\"sub_title\" style=\"color: #8a1c03;\">Is playing rummy for cash online legal in India?\r\n						</div>\r\n						<p>The one word answer is Yes. It is absolutely legal to play <a href=\"https://www.cloverrummy.com/\">rummy for cash</a> in India.<br><br> cloverrummy is operated by KPIS Pvt.Ltd. Gaming legislations in all states in India with the exception of Assam, Odihsa, Sikkim and Telangana exempt “games of mere skill” from the definition of gambling. In 1967 the Supreme Court ruled that the game of rummy is a game of skill and playing rummy does not amount to gambling.\r\n							<br>\r\n							<br>\r\n							 Over the past few decades, various High Courts have quoted the apex court’s decision and stated that law enforcement authorities cannot interfere with the game of rummy played with stakes. In 1996, a three-judge bench of the Supreme Court affirmed its 1967 judgment and ruled that games involving substantial and preponderant degree of skill fall within the definition of ‘games of mere skill’ and added that betting on horse-racing is a game of skill outside the ambit of gambling.\r\n							<br>\r\n							<br> \r\n							In 2015, the Supreme Court noted that observations of a 2012 Madras High Court order which held rummy for stakes to be illegal did not survive and deemed that order to be withdrawn. Various Supreme Court judgments have also noted that offering games of skill is a fundamental right under the Indian constitution and consequently games of skill cannot be prohibited.\r\n							<br>\r\n							<br> \r\n							Based on this legal background, various legal experts and jurists have opined that online rummy is completely legal in India.\r\n							<br>\r\n							<br> \r\n							KPIS Pvt.Ltd. complies with all laws and regulations applicable to online games of skill. KPIS Pvt.Ltd. will further comply with any change in this present legal position due to any order of a court of competent jurisdiction, regulatory body, central or state government or law enforcement agency.\r\n							<br>\r\n							<br>\r\n							 We permit players from all states and union territories in India except Assam, Odihsa, Sikkim and Telangana. Since the gaming legislations in Assam and Odisha do not exempt games of skill, at present we do not permit residents of Assam, Odihsa, Sikkim and Telangana to play real money rummy.\r\n							 <br>\r\n							 <br>\r\n							  For any further clarifications or information on the legality of playing on this website, contact <span style=\"color: #965201;\">support@cloverrummy.com</span>\r\n							</p>\r\n					</div>\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n','1525415853','1559124355',1),(7,'Withdraw Charges Info','<h2>Withdraw charges</h2>\r\n\r\n<p>Play Games24x7 Private Limited has migrated its Services from www.games24x7.com to www.cloverrummy.com with effect from 22nd August, 2012. All Terms of www.games24x7.com have been modified to replace references to www.games24x7.com and Games24x7, with www.cloverrummy.com, with effect from the said date.</p>\r\n\r\n<p>Play Games24x7 Private Limited offers online games including and not limited rummy and fantasy cricket, through the web-portals&nbsp;<a href=\"https://www.cloverrummy.com/\">www.cloverrummy.com</a>, www.my11circle.com, etc. which are also linked in our partner website(s) and mobile application(s) (collectively referred to as the &quot;Portal&quot;) (Play Games24x7 Private Limited referred to herein as &quot;Play Games24x7&quot; or &quot;we&quot; or &quot;us&quot; or &quot;our&quot;). Any person utilizing the Portal or any of its features including participation in the various contests, games (&quot;Game&quot;) (&quot;Amusement Facilities&quot;) being conducted on the Portal, be referred to as &quot;User&quot; or &quot;you&quot; or &quot;your&quot; and shall be bound by this Terms of Service Privacy Policy.</p>\r\n\r\n<p>As per recent changes made to the Telangana State Gaming Act 1974, online Rummy for cash has been banned in the State of Telangana. In compliance with the Ordinance we have made changes to our services and have stopped offering online rummy game for cash in Telangana.</p>\r\n\r\n<p>If you access the Service for making deposits or playing Cash Games from a jurisdiction where Cash Games are not permitted, you shall be entirely liable for any legal or penal consequences and we shall be entitled to forfeit the balance in your account.</p>\r\n\r\n<h2>Introduction</h2>\r\n\r\n<p>Your use of the products and/or services (hereinafter referred to as &quot;Services&quot;) currently offered or to be offered in future by Play Games24x7 Private Limited, its subsidiaries, affiliates, licensors, associates and partners (hereinafter referred as &quot;Play Games24x7&quot;) through the Portal, in which&nbsp;<a href=\"https://www.cloverrummy.com/\">www.cloverrummy.com</a>, www.my11circle.com are referred as website and collectively as websites (hereinafter referred as &quot;Website&quot; or &quot;Websites&quot;) is subject to and governed by these Terms of Service, Privacy Policy, Help, Promotions, Add Cash, My Account and Bring-A-Friend sections collective or respectively for each Website including the sub-sections of these sections on the Websites, as the case may be (hereinafter referred as &quot;Terms&quot;).</p>\r\n\r\n<p>You understand that the Terms will be binding on you. You agree that Services offered on the Portal can be accessed only in accordance with the Terms and you shall be responsible to comply with the Terms at all times. You are responsible to be aware of and agree to abide by the Terms as published and periodically amended or modified by Play Games24x7.</p>\r\n\r\n<p>If any of the Terms are determined to be unlawful, invalid, void or unenforceable for any reason by any judicial or quasi - judicial body in India, it will not affect the validity and enforceability of the remaining Terms.</p>\r\n\r\n<p>Our failure or delay to act or exercise any right or remedy with respect to a breach of any of the Terms by you shall not be construed as a waiver of our right to act with respect to the said breach or any prior, concurrent, subsequent or similar breaches. If a promotion, game, event, competition or tournament is organized by us on a Website, it shall be governed by the Terms and any supplementary terms and conditions which may be specifically applied for that promotion, game, event, competition or tournament.</p>\r\n\r\n<h2>APPLICABILITY</h2>\r\n\r\n<p><strong>BEFORE REGISTERING WITH US, YOU SHOULD CAREFULLY READ AND REVIEW THESE TERMS PROVIDED ON PLAY GAMES24X7 SITE WHICH ARE APPLICABLE TO ALL SERVICES ON THE WEBSITES AND ALSO THE PRODUCT-SPECIFIC CONDITIONS AND RULES APPLICABLE TO SPECIFIC OFFERINGS..</strong></p>\r\n\r\n<p>Links to the following sections of these Terms are provided below for easy access to the sections you intend to refer to. However, you should read all Terms as you will be bound by them all. Click on any of the following link to access that section directly.</p>\r\n\r\n<h2>1. Legality</h2>\r\n\r\n<p>You may only use the Services to play Cash Games (as defined below) if you are 18 years of age or over. Access to our Services or any part thereof may be restricted by us from time to time in our sole decision. You confirm that you are not accessing the Services to play Cash Games from</p>\r\n\r\n<ul>\r\n	<li>Outside India or from the States of Assam, Odisha, Telangana in India for accessing&nbsp;<a href=\"https://www.cloverrummy.com/\" target=\"_blank\">www.cloverrummy.com</a>.</li>\r\n	<li>Outside India or from the States of Assam, Odisha, Telangana, Sikkim and Nagaland in India for accessing&nbsp;<a href=\"https://www.cloverrummy.com/\" target=\"_blank\">www.cloverrummy.com</a></li>\r\n</ul>\r\n\r\n<h2>2. Game Services</h2>\r\n\r\n<ul>\r\n	<li>All tournaments, promotional games, practice games and cash games organized on the Websites are collectively referred as &quot;Games&quot;. The rules applicable to each type of Game are provided under the Help section on the respective Website.</li>\r\n	<li>&quot;Cash Game(s)&quot; are Games that require the participant to have a certain minimum cash balance in their user account to participate. All other Games offered on the Websites are defined as Non-Cash Game(s).</li>\r\n	<li>Play Games24x7 charges service charges for Cash Games, which may vary depending on the nature of the Cash Game and are subject to change from time to time. Non-Cash Games are offered free on the Website, but may be subject to entry restrictions in some cases. Service charges charged by Play Games24x7 are inclusive of all applicable taxes, including GST.</li>\r\n</ul>\r\n\r\n<h2>3. User representations</h2>\r\n\r\n<ul>\r\n	<li>Any information provided by you to us, whether at the stage of registration or during anytime subsequently, should be complete and truthful.</li>\r\n	<li>Prior to adding cash to your user account or participating in Cash Games, you shall be responsible to satisfy yourself about the legality of playing Cash Games in the jurisdiction from where you are accessing Cash Games. If you are not legally competent to individually enter into Indian Rupee transactions through banking channels in India and/or are not accessing any Website from a permitted jurisdiction, you are prohibited from participating in Cash Games on the website. In the event of such violation, your participation in Cash Games will be deemed to be in breach of the Terms and you will not be entitled to receive any prize that you might win in such Cash Games.</li>\r\n	<li>You represent that you are 18 years of age or older to participate in any Cash Games and are also otherwise competent to enter into transactions with other users and Play Games24x7. You are aware that participation in the Games organized by us (&quot;Activity&quot;) may result in financial loss to you. With full knowledge of the facts and circumstances surrounding this Activity, you are voluntarily participating in the Activity and assume all responsibility for and risk resulting from your participation, including all risk of financial loss. You agree to indemnify and hold Play Games24x7, its employees, directors, officers, and agents harmless with respect to any and all claims and costs associated with your participation in the Activity.</li>\r\n	<li>You represent that you have the experience and the requisite skills required to participate in the Activity and that you are not aware of any physical or mental condition that would impair your capability to fully participate in the Activity. You further acknowledge that you are solely responsible for any consequence resulting from your participating in this Activity or being associated with this Activity or around this Activity. You understand that Play Games24x7 assumes no liability or responsibility for any financial loss that you may sustain as a result of participation in the Activity.</li>\r\n	<li>You understand and accept that your participation in a Game available on the Websites does not create any obligation on us to give you a prize. Your winning a prize is entirely dependent on your skill as a player vis-a-vis other players in the Game and subject to the rules of the Game.</li>\r\n	<li>You understand and agree that you are solely responsible for all content posted, transmitted, uploaded or otherwise made available on the Websites by you. All content posted by you must be legally owned by or licensed to you. By publishing any content on the Website, you agree to grant us a royalty-free, world-wide, non-exclusive, perpetual and assignable right to use, copy, reproduce, modify, adapt, publish, edit, translate, create derivative works from, transmit, distribute, publicly display, and publicly perform your content and to use such content in any related marketing materials produced by us or our affiliates. Such content may include, without limitation, your name, username, location, messages, gender or pictures. You also understand that you do not obtain any rights, legal or equitable, in any material incorporating your content. You further agree and acknowledge that Play Games24x7 has the right to use in any manner whatsoever, all communication or feedback provided by you.</li>\r\n	<li>You understand and accept that Play Games24x7 reserves the right to record any and all user content produced by way of but not limited to chat messages on the Websites through the Play Games24x7 feature, through the in-game chat facility or other interactive features, if any, offered as part of the Services.</li>\r\n	<li>You understand that the funds in your user account held by Play Games24x7 do not carry any interest or return.</li>\r\n	<li>You shall not hold Play Games24x7 responsible for not being able to play any Game for which you may be eligible to participate. This includes but is not limited to situations where you are unable to log into your user account as your user account may be pending validation or you may be in suspected or established violation of any of the Terms.</li>\r\n	<li>You understand and accept that by viewing or using the Websites or availing of any Services, or using communication features on the Website, you may be exposed to content posted by other users which you may find offensive, objectionable or indecent. You may bring such content posted by other users to our notice that you may find offensive, objectionable or indecent and we reserve the right to act upon it as we may deem fit. The decision taken by us on this regard shall be final and binding on you.</li>\r\n</ul>\r\n\r\n<h2>4. User Account Creation &amp; Operation</h2>\r\n\r\n<ul>\r\n	<li>To use our Services, you will need to register with us on the Portal.</li>\r\n	<li>By completing the online registration process on the Portal, you confirm your acceptance of the Terms.</li>\r\n	<li>By registering on Play Games24x7, you agree to receive promotional messages relating to Tournaments &amp; Bonus through SMS, Email and Push Notifications. You may withdraw your consent by sending an email to&nbsp;<a href=\"mailto:support@cloverrummy.com\">support@cloverrummy.com</a>&nbsp;or&nbsp;<a href=\"mailto:infu@cloverrummy.com\">info@cloverrummy.com</a>&nbsp;as the case may be.</li>\r\n	<li>During the registration process, you will be required to choose a login name and a password in addition to providing some other information which may not be mandatory. Additionally, you may be required to give further personal information for your user account verification and/or for adding cash to your user account. You must give us the correct details in all fields requiring your personal information, including, without limitation, your name, postal address, email address, telephone number(s) etc. You undertake that you will update this information and keep it current.</li>\r\n	<li>You acknowledge that we may, at any time, require you to verify the correctness of this information and in order to do so may require additional documentary proof from you, failing which we reserve the right to suspend or terminate your registration on the Website.</li>\r\n	<li>Any information provided by you to us should be complete and truthful to the best of your knowledge. We are not obliged to cross check or verify information provided by you and we will not take any responsibility for any outcome or consequence as a result of you providing incorrect information or concealing any relevant information from us.</li>\r\n	<li>You understand that it is your responsibility to protect the information you provide on the Websites including but not limited to your Username, Password, Email address, Contact Details and Mobile number. Play Games24x7 will not ask for your user account login password which is only to be entered at the time of login. At no other time should you provide your user account information to any user logged in on the Websites or elsewhere. You undertake that you will not allow / login and then allow, any other person to play from your user account using your username. You specifically understand and agree that we will not incur any liability for information provided by you to anyone which may result in your user account on the Websites being exposed or misused by any other person.</li>\r\n	<li>You agree to use your Play Games24x7 user account solely for the purpose of playing on the Websites and for transactions which you may have to carry out in connection with availing the Services on the Websites. Use or attempted use of your user account for any reason other than what is stated in the Terms may result in immediate termination of your user account and forfeiture of any prize, bonus or balance in the user account.</li>\r\n	<li>You also understand and agree that deposits in your user account maintained with us are purely for the purpose of participation in Cash Games made available on the Website.</li>\r\n	<li>You understand and agree that you cannot transfer any sum from your user account with us to the account of another registered user on the Portal except as may be permitted by Play Games24x7 and subject to restrictions and conditions as may be prescribed.</li>\r\n	<li>We are legally obliged to deduct tax at source (TDS) on winnings of more than&nbsp;<em>&nbsp;</em>10,000/- in a single tournament, a single Pool Rummy game, or a single Points Rummy game&nbsp;<a href=\"https://www.cloverrummy.com/help/user/tds.html\">(click here for illustrations) on www.cloverrummy.com</a>, or a single contest on my11circle&nbsp;<a href=\"https://www.cloverrummy.com/help/user/tds.html\">(click here for illustrations) on www.cloverrummy.com</a>, as the case may be. In these cases, you will be required to furnish your Permanent Account Number (PAN) duly issued to you by the Income Tax authorities if you have not already done so. TDS at the rate of 30% will automatically be deducted from such winnings and the rest will be credited to your Play Games24x7 user account. Withdrawal of these winnings will only be permitted upon your providing your correct PAN details. These limits and rates are subject to change as per the prevailing rules and regulations. Play Games24x7&#39;s obligation is this regard is limited to deducting TDS as required by law and providing you an appropriate certificate of tax deduction. We neither advise you nor shall in any manner be responsible for your individual tax matters.</li>\r\n	<li>We reserve the right to verify your PAN from time to time and to cancel any prize should your PAN be found inconsistent in our verification process.</li>\r\n</ul>\r\n\r\n<h2>5. User Account validation and personal information verification</h2>\r\n\r\n<ul>\r\n	<li>Play Games24x7 may from time to time attempt to validate its players&#39; user accounts. These attempts may be made via a phone call or via email. In the event that we are not able to get in touch with you the first time around, we will make additional attempts to establish contact with you. If the phone number and email provided by you is not correct, we bear no responsibility for the Services being interrupted due to our being unable to establish contact with you.</li>\r\n	<li>If we are unable to reach you or if the validation is unsuccessful, we reserve the right to disallow you from logging into the Portal or reduce your play limits and/or Add Cash limits until we are able to satisfactorily validate your user account. We will in such events email you to notify you of the next steps regarding user account validation. We may also ask you for proof of identification and proof of address from time to time.</li>\r\n	<li>Upon receipt of suitable documents, we will try our best to enable your user account at the earliest. However, it may take a few business days to reinstate your user account.</li>\r\n	<li>In the event that we have made several attempts to reach out to you but have been unable to do so, we also reserve the right to permanently suspend your user account and refund the amount, if any, in your user account to the financial instrument through which the payment was made to your user account or by cheque to the address provided by you. In the event the address provided by you is incorrect, Play Games24x7 will not make any additional attempts for delivery of the cheque unless a correct address is provided by you and charges for redelivery as prescribed by Play Games24x7 are paid by you.</li>\r\n	<li>The Privacy Policy of our Websites form a part of the Terms. All personal information which is of such nature that requires protection from unauthorized dissemination shall be dealt with in the manner provided in the&nbsp;<a href=\"https://www.rummycircle.com/misc/privacy-policy.html\">Privacy Policy</a>&nbsp;of the Website.</li>\r\n</ul>\r\n\r\n<h2>6. User restrictions</h2>\r\n\r\n<ul>\r\n	<li><strong>Anti-Cheating and Anti-Collusion:</strong></li>\r\n	<li>You undertake that you yourself will play in all Games in which you have registered/joined and not use any form of external assistance to play. You shall not add unauthorized components, create or use cheats, exploits, bots, hacks or any other third-party software designed to modify the Websites or use any third-party software that intercepts, mines or otherwise collects information from or through the Websites or through any Services. Any attempt to employ any such external assistance is strictly prohibited.</li>\r\n	<li>Formation of teams for the purpose of collusion between you and any other user(s) for participating in Games organized on the Websites or any other form of cheating is strictly prohibited.</li>\r\n	<li>When collusion or cheating is detected on www.rummycircle.com, Play Games24x7 shall settle the Game as per its&nbsp;<a href=\"https://www.cloverrummy.com/help/cancellation.html\">&quot;Game Cancellation Settlement Policy&quot;</a>&nbsp;and may take further appropriate action against offending users in terms hereof.</li>\r\n	<li><strong>Money Laundering:</strong>&nbsp;You are prohibited from doing any activity on the Website that may be construed as money laundering, including, without limitation, attempting to withdraw cash from unutilized cash added through credit cards or deliberately losing money to a certain player(s).</li>\r\n	<li><strong>Anti-SPAMMING:</strong>&nbsp;Sending SPAM emails or any other form of unsolicited communication for obtaining registrations on the Website to benefit from any promotional program of Play Games24x7 or for any other purpose is strictly prohibited.</li>\r\n	<li><strong>Multiple IDs:</strong>&nbsp;Your registration on the Portal is restricted to a single user account which will be used by you to avail of the Services provided on the Portal. You are prohibited from creating or using multiple user IDs for registering on the Portal.</li>\r\n	<li>You may not create a login name or password or upload, distribute, transmit, publish or post content through or on the Websites or through any service or facility including any messaging facility provided by the Websites which :\r\n	<ul>\r\n		<li>is libelous, defamatory, obscene, intimidating, invasive of privacy, abusive, illegal, harassing;</li>\r\n		<li>contains expressions of hatred, hurting religious sentiments, racial discrimination or pornography;</li>\r\n		<li>is otherwise objectionable or undesirable (whether or not unlawful);</li>\r\n		<li>would constitute incitement to commit a criminal offence;</li>\r\n		<li>violates the rights of any person;</li>\r\n		<li>is aimed at soliciting donations or other form of help;</li>\r\n		<li>violates the intellectual property of any person;</li>\r\n		<li>disparage in any manner Play Games24x7 or any of its subsidiaries, affiliates, licensors, associates, partners, sponsors, products, services, or websites;</li>\r\n		<li>promotes a competing service or product; or</li>\r\n		<li>violates any laws</li>\r\n	</ul>\r\n	</li>\r\n	<li>In the event we determine that the login name created by you is indecent, objectionable, offensive or otherwise undesirable, we shall notify you of the same and you shall promptly provide us with an alternate login name so that we can change your existing login name to the new name provided by you. If you fail to provide an alternate name, we reserve the right to either permanently suspend your user account or restore your user account only after a different acceptable login name has been provided by you.</li>\r\n	<li>You shall not host, intercept, emulate or redirect proprietary communication protocols, used by the Website, if any, regardless of the method used, including protocol emulation, reverse engineering or modification of the Websites or any files that are part of the Websites.</li>\r\n	<li>You shall not frame the Portal. You may not impose editorial comments, commercial material or any information on the Websites, alter or modify Content on the Websites, or remove, obliterate or obstruct any proprietary notices or labels.</li>\r\n	<li>You shall not use Services on the Websites for commercial purposes including but not limited to use in a cyber cafe as a computer gaming centre, network play over the Internet or through gaming networks or connection to an unauthorized server that copies the gaming experience on the Website.</li>\r\n	<li>You shall not upload, distribute or publish through the Websites, any content which may contain viruses or computer contaminants (as defined in the Information Technology Act 2000 or such other laws in force in India at the relevant time) which may interrupt, destroy, limit the functionality or disrupt any software, hardware or other equipment belonging to us or that aids in providing the services offered by Play Games24x7. You shall not disseminate or upload viruses, programs, or software whether it is harmful to the Websites or not. Additionally, you shall not impersonate another person or user, attempt to get a password, other user account information, or other private information from a user, or harvest email addresses or other information.</li>\r\n	<li>You shall not purchase, sell, trade, rent, lease, license, grant a security interest in, or transfer your user account, Content, currency, points, standings, rankings, ratings, or any other attributes appearing in, originating from or associated with the Website.</li>\r\n	<li>Any form of fraudulent activity including, attempting to use or using any other person&#39;s credit card(s), debit cards, net-banking usernames, passwords, authorization codes, prepaid cash cards, mobile phones for adding cash to your user account is strictly prohibited.</li>\r\n	<li>Accessing or attempting to access the Services through someone else&#39;s user account is strictly prohibited.</li>\r\n	<li>Winnings, bonuses and prizes are unique to the player and are non-transferable. In the event you attempt to transfer any winnings, bonuses or prizes, these will be forfeited.</li>\r\n	<li>If you are an officer, director, employee, consultant or agent of Play Games24x7 or a relative of such persons (&quot;Associated Person&quot;), you are not permitted to play either directly or indirectly, any Games which entitle you to any prize on the Websites, other than in the course of your engagement with Play Games24x7. For these purposes, the term &#39;relative&#39; shall include spouse and financially dependent parents and, children.</li>\r\n	<li>You shall not post any material or comment, on any media available for public access, which in our sole discretion, is defamatory or detrimental to our business interests, notwithstanding the fact that such media is not owned or controlled by us. In addition to any other action that we may take pursuant to the provision hereof, we reserve the right to remove any and all material or comments posted by you and restrict your access to any media available for public access that is either controlled or moderate by us; when in our sole opinion, any such material or comments posted by you is defamatory or detrimental to our business interests.</li>\r\n</ul>\r\n\r\n<h2>7. Payments and Player Funds</h2>\r\n\r\n<ul>\r\n	<li>All transactions on the Website shall be in Indian Rupees.</li>\r\n	<li>Once you register on our Portal, we maintain a user account for you to keep a record of all your transactions with us. Payments connected with participation in Cash Games have to be made through your Play Games24x7 user account. All cash prizes won by you are credited by us into this user account.</li>\r\n	<li>When making a payment, please ensure that the instrument used to make the payment is your own and is used to Add Cash into your user account only.</li>\r\n	<li>Subject to the Add Cash limits specified by us from time to time, you are free to deposit as much money as you want in your user account for the purpose of participating in Cash Games on the Websites.</li>\r\n	<li>Play Games24x7 wants you to play responsibly on the Websites. The ability to Add Cash in your user account shall be subject to monthly Add Cash limits which we can be set by us with undertakings, indemnity, waiver and verification conditions as we deem appropriate in our sole discretion.</li>\r\n	<li>Credit card, Debit card, prepaid cash cards and internet banking payments are processed through third party payment gateways. Similarly, other payment modes also require an authorization by the intermediary which processes payments. We are not responsible for delays or denials at their end and processing of payments will be solely in terms of their policies and procedures without any responsibility or risk at our end. If there are any issues in connection with adding cash, a complaint may be sent to us following the complaints procedure provided in &quot;Complaints and disputes&quot; section below. You agree that in such an event of your credit being delayed or eventually declined for reasons beyond our control, we will not be held liable in any manner whatsoever. Once a payment/transaction is authorized, the funds are credited to your user account and are available for you to play Cash Games.</li>\r\n	<li>We have the right to cancel a transaction at any point of time solely according to our discretion in which case if the payment is successful, then the transaction will be reversed and the money credited back to your payment instrument.</li>\r\n	<li>Player funds are held in trust by us in specified bank accounts. Play Games24x7 keeps all players&#39; funds unencumbered which will be remitted to you in due course subject to the terms and conditions applicable to withdrawal of funds. Funds held in your user account are held separately from our corporate funds. Even in the highly unlikely event of an insolvency proceeding, you claims on the deposits will be given preference over all other claims to the extent permissible by law.</li>\r\n</ul>\r\n\r\n<h2>8. Withdrawals</h2>\r\n\r\n<ol>\r\n	<li>You may withdraw your winnings by means of either an account payee cheque or an electronic bank to bank transfer for the amount of winnings.</li>\r\n	<li>You agree that all withdrawals you make are governed by the following conditions:\r\n	<ul>\r\n		<li>Play Games24x7 can ask you for KYC documents to verify your address and identity at any stage. Withdrawals will be permitted only from accounts for which such KYC process is complete.</li>\r\n		<li>You can choose to withdraw money from your user account at any time, subject to bonus/prize money withdrawal restrictions, by notifying us of your withdrawal request. Bonuses (except Bring a Friend bonus) and promotional winnings are subject to withdrawal restrictions and can only be withdrawn on fulfilling some preconditions, one of which is that you have made at least one cash deposit on the Portal and thereafter played at least one Cash Game.</li>\r\n		<li>Once notified, post verification of the withdrawal request, we may disburse the specified amount by cheque or electronic transfer based on the mode of withdrawal selected by you. We shall make our best efforts to honor your choice on the mode of withdrawal, but reserve the right to always disburse the specified amount to you by cheque. We also reserve the right to disburse the amount on the financial instrument used to Add Cash to your user account.</li>\r\n		<li>Withdrawals attract processing charges as per the prevalent policy. You may be eligible to make one or more free withdrawals in a month depending on various factors including your club status, the amount of withdrawal or the mode of withdrawal. In the event that you do not have an adequate amount in your user account to pay the processing charge, your withdrawal will not be processed at that time. You may request for a free withdrawal subsequently if you become eligible for the same at such time.</li>\r\n		<li>If a cheque is not encashed within 30 days of dispatch, we will deem the cheque to have been lost and reserve the right to cancel the cheque with necessary instructions to our bank to stop payment of that cheque. This may result in dishonor of that cheque in which case we shall not be liable for the cheque amount to you. Any request for reissuing the cheque will result in deduction of applicable processing charges from the amount of the cheque on reissuance.</li>\r\n		<li>We will attempt our best to process your withdrawals in a timely manner, but there could be delays due to the time required for verification and completing the withdrawal transaction. We shall not be liable to pay you any form of compensation for the reason of delays in remitting payments to you from your user account.</li>\r\n		<li>To be eligible to win a prize, you must be a resident of India and accessing the Services of Play Games24x7 on the Services from India.</li>\r\n	</ul>\r\n	</li>\r\n	<li>If you are a prize winner resident in India and physically present in India while accessing the services of Play Games24x7 but not an Indian citizen, we will remit your winnings in Indian Rupees to the address/bank account given by you, provided, the address and bank account is within India.</li>\r\n</ol>\r\n\r\n<h2>9. Service Disruptions</h2>\r\n\r\n<ul>\r\n	<li>You may face Service disruptions, including, but not limited to disconnection or communication interferences due to issues in the internet infrastructure used for providing or accessing the Services or due to issues with the hardware and software used by you. You understand that Play Games24x7 has no control over these factors. Play Games24x7 shall not be responsible for any interruption in Services and you take full responsibility for any risk of loss due to Service interruptions for any such reason.</li>\r\n	<li>You understand, acknowledge and agree to the fact that if you are unable to play in any Game due to any error or omission directly attributable to Play Games24x7, including technical or other glitches at our end, the settlement of such Games will be as per the&nbsp;<a href=\"https://www.cloverrummy.com/help/cancellation.html\">&quot;Game Cancellation&quot; Settlement Policy for cloverrummy.com</a>&nbsp;and entry fee would be refunded in the case of my11circle.com.</li>\r\n</ul>\r\n\r\n<p>It is clarified that, notwithstanding any limitation of liability as specified herein, Play Games24x7 specifically disclaims any liability in the case of Games where there is no money paid by you.</p>\r\n\r\n<p>You agree that under no circumstances shall you compel Play Games24x7 or hold Play Games24x7 liable to pay you any amount over and above the service charges for any of the aforementioned errors/omissions of Play Games24x7.</p>\r\n\r\n<h2>10. Content</h2>\r\n\r\n<ul>\r\n	<li>All content and material on the Websites including but not limited to information, images, marks, logos, designs, pictures, graphics, text content, hyperlinks, multimedia clips, animation, games and software (collectively referred to as &quot;Content&quot;), whether or not belonging to Play Games24x7, are protected by applicable intellectual property laws. Additionally, all chat content, messages, images, recommendations, emails, images sent by any user can be logged/recorded by us and shall form part of Content and Play Games24x7 is free to use this material in any manner whatsoever.</li>\r\n	<li>The Websites may contain information about or hyperlinks to third parties. In such a cases, we are not responsible in any manner and do not extend any express or implied warranty to the accuracy, integrity or quality of the content belonging to such third party websites. If you rely on any third party Content posted on any Website which does not belong to Play Games24x7, you may do so solely at your own risk and liability.</li>\r\n	<li>If you visit any third party website through a third party Content posted on the website, you will be subject to terms and conditions applicable to it. We neither control nor are responsible for content on such third-party websites. The fact of a link existing on our Websites to a third-party website is not an endorsement of that website by us.</li>\r\n</ul>\r\n\r\n<h2>11. Promotions</h2>\r\n\r\n<ul>\r\n	<li>Bring A Friend (&quot;BAF&quot;): You can invite a friend, acquaintance or any other person to use the Services available on the Websites by accessing the &quot;Bring - A - Friend&quot; feature on the Website. The BAF bonus plan shall be governed by the additional terms and conditions provided in the BAF section on the respective Website.</li>\r\n	<li>Cash bonuses may be subject to withdrawal restrictions. All bonus plans shall be governed by the additional terms and conditions applicable to that bonus plan which will apply in addition to the Terms.</li>\r\n	<li>The details of various promotions organized on the Websites can be found in the Promotions section on that Website. Eligibility and applicable conditions for the ongoing promotional programs are provided in the Promotions section, which form a part of the Terms.</li>\r\n	<li>Games offered under the Promotions section may be cancelled or discontinued by Play Games24x7 at any time without notice without any liability on Play Games24x7 whatsoever, except refund of entry fee, if applicable.</li>\r\n</ul>\r\n\r\n<h2>12. RummyRewardzTM</h2>\r\n\r\n<p>Details of RummyRewardzTM&nbsp;program of Play Games24x7 are provided in the RummyRewardzTM&nbsp;section on the www.cloverrummy.com. RummyRewardzTMProgram may be altered or discontinued by Play Games24x7 at any time without notice and without any liability on Play Games24x7 whatsoever.</p>\r\n\r\n<p>In addition, Play Games24x7 may, but will not be obligated to, run RummyRewardz and/or other loyalty programs which may entail grant of reward points or such other loyalty rewards which may be redeemed in a manner prescribed in its specific terms and conditions.</p>\r\n\r\n<p>The terms and conditions of any and all loyalty programs can be modified by Games24x7 with or without notice at any time. Play Games24x7&rsquo;s reserves the right at its sole discretion to terminate its loyalty programs, if any, and revoke any unredeemed loyalty points with or without notice. Games24x7 shall not be liable to pay any compensation for loyalty points remaining in your user account in any event whatsoever, including, without limitation, the discontinuation of such loyalty programs by Play Games24x7.</p>\r\n\r\n<h2>13. License Agreement &amp; Intellectual Property</h2>\r\n\r\n<ul>\r\n	<li>All Content on the Websites shall be utilized only for the purpose of availing Services in conformity with the Terms.</li>\r\n	<li>You acknowledge that all ownership rights and all copyright and other intellectual property rights in the Content are owned by Play Games24x7 or our licensors and that you have no right title or other interest in any such items except as expressly stated in the Terms.</li>\r\n	<li>You are granted a personal, non-exclusive, non-assignable and non-transferable license to use the Content solely for the purposes of accessing and using the Services and for no other purpose whatsoever.</li>\r\n	<li>You shall not sublicense, assign or transfer the license granted to you, or rent or lease or part with the whole or any part of such license or of the Content included in such license.</li>\r\n	<li>You may not transfer, copy, reproduce, distribute, exploit, reverse engineer, disassemble, translate, decode, alter, make derivations from or make any other use of Content on the Websites in any manner other than as permitted for obtaining the Services provided on the website.</li>\r\n	<li>You may not hyperlink the Websites to any other website without permission from us.</li>\r\n	<li>You may access information on download and print extracts from the Websites for your personal use only. No right, title or interest in any downloaded materials or software is transferred to you by downloading and you are expressly prohibited from using such materials for any commercial purpose unless agreed with us in writing.</li>\r\n</ul>\r\n\r\n<h2>14. Voluntary termination</h2>\r\n\r\n<p>You are free to discontinue use of the Services on any Website at any time by intimating us of your desire to do so by sending an email to us at&nbsp;<a href=\"mailto:support@cloverrummy.com\">support@cloverrummy.com</a>&nbsp;or&nbsp;<a href=\"mailto:imfo@cloverrummy.com\">info@cloverrummy.com</a>&nbsp;as the case may be. If at such time, there is a positive withdrawable cash balance in your user account, we will, subject to satisfactory verification, disburse the same to you by online transfer or by a cheque in a timely manner.</p>\r\n\r\n<h2>15. User Account suspension</h2>\r\n\r\n<ul>\r\n	<li>We may suspend or otherwise put restrictions on your access to the Services on the Websites during investigation for any of the following reasons:</li>\r\n	<li>Suspected violation of Terms or other abuse of your user account;</li>\r\n	<li>Suspected breach of security of your user account; or</li>\r\n	<li>If there have been charge-backs on your user account.</li>\r\n	<li>Our decision to suspend or restrict Service or any part thereof as we deem appropriate shall be final and binding on you.</li>\r\n</ul>\r\n\r\n<h2>16. Breach and consequences</h2>\r\n\r\n<ul>\r\n	<li>In the event of breach of any of the Terms being evidenced from our investigation or if there is reasonable belief, in our sole discretion, that your continued access to the Portal or any Website is detrimental to the interests of Play Games24x7, our other users or the general public; we may in our sole discretion take any or all of the following actions:</li>\r\n	<li>Restrict games between users suspected of colluding or cheating;</li>\r\n	<li>Permanently suspend your user account on the Website;</li>\r\n	<li>Forfeit the cash balance in your user account;</li>\r\n	<li>Demand damages for breach and take appropriate civil action to recover such damages; and/or</li>\r\n	<li>Initiate prosecution for violations that amount to offences in law.</li>\r\n	<li>Additionally, in the event of committing material breach hereof, we reserve the right to bar you from future registration on the Website.</li>\r\n	<li>The decision of Play Games24x7 on the action to be taken as a consequence of breach shall be final and binding on you.</li>\r\n	<li>Any action taken by Play Games24x7 shall be without prejudice to our other rights and remedies available in law or equity.</li>\r\n</ul>\r\n\r\n<h2>17. Complaints &amp; disputes</h2>\r\n\r\n<ul>\r\n	<li>If you have a complaint, you should in the first instance contact the customer support team at&nbsp;<a href=\"mailto:support@cloverrummy.com\">support@cloverrummy.com</a>&nbsp;or&nbsp;<a href=\"mailto:info@cloverrummy.com\">info@cloverrummy.com</a>, as the case may be or write to us following the procedure given in the Contact Us section on the Websites. Complaints should be made as soon as possible after circumstances arise that cause you to have a complaint.</li>\r\n	<li>You accept that any complaints and disputes are and remain confidential both whilst a resolution is sought and afterwards. You agree that you shall not disclose the existence, nature or any detail of any complaint or dispute to any third party.</li>\r\n	<li>Play Games24x7 shall make efforts to resolve complaints within reasonable time.</li>\r\n	<li>Our decision on complaints shall be final and binding on you.</li>\r\n</ul>\r\n\r\n<h2>18. Modifications and alterations</h2>\r\n\r\n<ul>\r\n	<li>We may alter or modify the Terms at any time without giving prior notice to you. We may choose to notify of some changes in the Terms either by email or by displaying a message on the Websites; however, our notification of any change shall not waive your obligation to keep yourself updated about the changes in the Terms. Your continued use of any Website and/or any Services offered constitutes your unconditional acceptance of the modified or amended Terms.</li>\r\n	<li>We may also post supplementary conditions for any Services that may be offered. In such an event, your use of those Services will be governed by the Terms as well as any such supplementary terms that those Services may be subject to.</li>\r\n</ul>\r\n\r\n<h2>19. Limitation of liability</h2>\r\n\r\n<ol>\r\n	<li>In addition to specific references to limitation of liability of Play Games24x7 elsewhere in the Terms, under no circumstances (including, without limitation, in contract, negligence or other tort), Play Games24x7 shall be liable for any injury, loss, claim, loss of data, loss of income, loss of profit or loss of opportunity, loss of or damage to property, general damages or any direct, indirect, special, incidental, consequential, exemplary or punitive damages of any kind whatsoever arising out of or in connection with your access to, or use of, or inability to access or use, the Services on any Website. You further agree to indemnify us and our service providers and licensors against any claims in respect of any such matter.</li>\r\n	<li>Without limiting the generality of the foregoing, you specifically acknowledge, agree and accept that we are not liable to you for:\r\n	<ul>\r\n		<li>the defamatory, undesirable or illegal conduct of any other user of the Services;</li>\r\n		<li>any loss whatsoever arising from the use, abuse or misuse of your user account or any feature of our Services on the Websites;</li>\r\n		<li>any loss incurred in transmitting information from or to us or from or to our Websites by the internet or by other connecting media;</li>\r\n		<li>any technical failures, breakdowns, defects, delays, interruptions, improper or manipulated data transmission, data loss or corruption or communications&#39; infrastructure failure, viruses or any other adverse technological occurrences arising in connection with your access to or use of our Services;</li>\r\n		<li>the accuracy, completeness or currency of any information services provided on the Websites;</li>\r\n		<li>any delay or failure on our part to intimate you where we may have concerns about your activities; and</li>\r\n		<li>your activities / transactions on third party websites accessed through links or advertisements posted in the Website.</li>\r\n	</ul>\r\n	</li>\r\n	<li>Notwithstanding anything to the contrary contained in the Terms or elsewhere, you agree that our maximum aggregate liability for all your claims under this agreement, in all circumstances, other than for the payment of any withdrawable balance in your user account, shall be limited to Indian Rupees One Thousand only (INR. 1,000/-)</li>\r\n</ol>\r\n\r\n<h2>20. Disclaimer and indemnity</h2>\r\n\r\n<p><strong>Disclaimer</strong></p>\r\n\r\n<ul>\r\n	<li>The Services on the Websites and the Content present on it are provided strictly on &quot;as is&quot; basis with all faults or failings. Any representations, warranties, conditions or guarantee whatsoever, express or implied (including, without limitation, any implied warranty of accuracy, completeness, uninterrupted provision, quality, merchantability, fitness for a particular purpose or non-infringement) are specifically excluded to the fullest extent permitted by law. Play Games24x7 does not ensure or guarantee continuous, error-free, secure or virus-free operation of the Websites or its Content including software, Games, your user account, the transactions in your user account or continued operation or availability of any facility on the website.</li>\r\n	<li>Additionally, Play Games24x7 does not promise or ensure that you will be able to access your user account or obtain Services whenever you want. It is entirely possible that you may not be able to access your user account or the Services provided by Play Games24x7 at times or for extended periods of time due to, but not limited to, system maintenance and updates.</li>\r\n	<li>Play Games24x7 disclaims responsibility and liability for any harm resulting from cancellation of any Game organized by it. If you are a cash player on the website, you acknowledge and agree that you will not be entitled to any refund in case of any service outages that may be caused by failures of our service providers, computer viruses or contaminants, natural disasters, war, civil disturbance, or any other cause beyond the reasonable control of Play Games24x7.</li>\r\n	<li>Play Games24x7 specifically disclaims any liability in connection with Games or events made available or organized on the Websites which may require specific statutory permissions, in the event such permissions are denied or cancelled whether prior to or during such Game or event.</li>\r\n	<li>Play Games24x7 specifically disclaims any liability in connection with your transactions with third parties which may have advertisements or are hyperlinked on the Website.</li>\r\n	<li>Play Games24x7 disclaims any liability in connection with violation of intellectual property rights of any party with respect to third party Content or user content posted on our Website. Intellectual property rights in any Content not belonging to us belong to the respective owners and any claims related to such content must be directly addressed to the respective owners.</li>\r\n	<li>Play Games24x7 specifically disclaims any liability arising out of the acts or omissions of the infrastructure providers or otherwise failure of internet services used for providing and accessing the Services.</li>\r\n	<li>Play Games24x7 disclaims liability for any risk or loss resulting to you from your participation in Cash Games, including all risk of financial loss.</li>\r\n	<li>Random Number Generator Software: iTech Labs Australia has evaluated the Random Number Generator (&quot;RNG&quot;) used by www.cloverrummy.com and has certified that the software of Play Games24x7 complies with the relevant statistical standards of randomness. You understand that Play Games24x7 maintains the said certification it considers appropriate in the best of its belief, however, it is not possible to test all possible scenarios in any testing environment and Play Games24x7 specifically disclaims any and all liability in connection with the RNG used by Play Games24x7&#39;s software.</li>\r\n</ul>\r\n\r\n<p><strong>Indemnity</strong>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>To the extent permitted by law, and in consideration for being allowed to participate in the Activity, you hereby agree to indemnify, save and hold harmless and defend us (to the extent of all benefits and awards, cost of litigation, disbursements and reasonable attorney&#39;s fees that we may incur in connection therewith including any direct, indirect or consequential losses, any loss of profit and loss of reputation) from any claims, actions, suits, taxes, damages, injuries, causes of action, penalties, interest, demands, expenses and/or awards asserted or brought against us by any person in connection with:</li>\r\n	<li>infringement of their intellectual property rights by your publication of any content on our Website.</li>\r\n	<li>defamatory, offensive or illegal conduct of any other player or for anything that turns out to be misleading, inaccurate, defamatory, threatening, obscene or otherwise illegal whether originating from another player or otherwise;</li>\r\n	<li>use, abuse or misuse of your user account on our Website in any manner whatsoever;</li>\r\n	<li>any disconnections, technical failures, system breakdowns, defects, delays, interruptions, manipulated or improper data transmission, loss or corruption of data or communication lines failure, distributed denial of service attacks, viruses or any other adverse technological occurrences arising in connection with your access to or use of our Website; and</li>\r\n	<li>access of your user account by any other person accessing the Services using your username or password, whether or not with your authorization.</li>\r\n</ul>\r\n\r\n<h2>21. Governing law, dispute resolution &amp; jurisdiction</h2>\r\n\r\n<ul>\r\n	<li>The Terms and Privacy Policy shall be interpreted in accordance with the laws of India.</li>\r\n	<li>Any dispute, controversy or claim arising out of the Terms or Privacy Policy shall be subject to the exclusive jurisdiction of the civil courts at Mumbai</li>\r\n</ul>\r\n','1525415853','1566469399',1);

/*Table structure for table `contact_us` */

DROP TABLE IF EXISTS `contact_us`;

CREATE TABLE `contact_us` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `query` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

/*Data for the table `contact_us` */

/*Table structure for table `default_avatars` */

DROP TABLE IF EXISTS `default_avatars`;

CREATE TABLE `default_avatars` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(25) NOT NULL,
  `gender` enum('Male','Female') NOT NULL,
  `image` varchar(100) NOT NULL,
  `created_at` varchar(20) NOT NULL,
  `updated_at` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

/*Data for the table `default_avatars` */

insert  into `default_avatars`(`id`,`title`,`gender`,`image`,`created_at`,`updated_at`,`status`) values (26,'male2','Male','0fd830feb8a8d6af4bce484fe4ad81cc.png','1559111458','',1),(27,'male3','Male','a8daa5b1d1d7e532d443042576b28cdb.png','1559111589','',1),(28,'male4','Male','7131c0e47dc656dadd8c34d841d22e29.png','1559112263','',1),(29,'male5','Male','a31b592cd9d60829688c5f28f60d014c.png','1559112447','',1),(30,'male1','Male','6208105e98e604cc2a937f6a5353f633.png','1559112667','',1),(32,'male6','Male','546c1734ee1b76dc155b169446e9947e.png','1559112755','',1);

/*Table structure for table `deposit_money_transactions` */

DROP TABLE IF EXISTS `deposit_money_transactions`;

CREATE TABLE `deposit_money_transactions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `players_id` bigint(20) unsigned NOT NULL,
  `comment` varchar(200) NOT NULL,
  `credits_count` double unsigned NOT NULL,
  `transaction_id` varchar(100) NOT NULL,
  `payment_gateway_id` varchar(100) NOT NULL,
  `bonus_code` varchar(50) NOT NULL DEFAULT '',
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `transaction_status` enum('Pending','Failed','Cancelled','Success') NOT NULL DEFAULT 'Pending',
  `payment_gateway_response_log` text NOT NULL,
  `signature` varchar(200) NOT NULL DEFAULT ' ',
  `bonus_credits` double unsigned NOT NULL DEFAULT '0',
  `source` enum('unity_mobile_app','website','mobile_app') NOT NULL DEFAULT 'website',
  `version_code` varchar(20) NOT NULL DEFAULT '1.0.0',
  `payment_gateway` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `players_id` (`players_id`),
  CONSTRAINT `deposit_money_transactions_ibfk_1` FOREIGN KEY (`players_id`) REFERENCES `players` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=latin1;

/*Data for the table `deposit_money_transactions` */

/*Table structure for table `download_mobile_rummy` */

DROP TABLE IF EXISTS `download_mobile_rummy`;

CREATE TABLE `download_mobile_rummy` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mobile` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `download_mobile_rummy` */

/*Table structure for table `faq_categories` */

DROP TABLE IF EXISTS `faq_categories`;

CREATE TABLE `faq_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `faq_categories` */

insert  into `faq_categories`(`id`,`category`,`icon`,`created_at`,`updated_at`,`status`) values (3,'Game play','0ac1efc775c4bca754ff8bc113da6024.png','1555334591','1564399262',1),(4,'Bank Transactions','32a2e63d0fbf9b5bceada90fd316c4d4.png','1555334736','1564399275',1),(5,'Bonus','78d1683b954bfdce6b5a2ba15b557ff4.png','1555334751','1564399251',1),(6,'Account Details','9405a24f5db1e87b0f31d7140a683c90.png','1555334775','1564399243',1),(7,'Refer a Friend','dfed721debb76a86f139158ef800dcfe.png','1555334788','1564399226',1);

/*Table structure for table `faqs` */

DROP TABLE IF EXISTS `faqs`;

CREATE TABLE `faqs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `faq_categories_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;

/*Data for the table `faqs` */

/*Table structure for table `game_play` */

DROP TABLE IF EXISTS `game_play`;

CREATE TABLE `game_play` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `game_detail_json` longtext,
  `games_id` varchar(45) NOT NULL DEFAULT '0',
  `rooms_id` varchar(45) NOT NULL DEFAULT '0',
  `game_sub_type` enum('Points','Pool','Deals') NOT NULL DEFAULT 'Points',
  `game_type` enum('Practice','Cash','Tournament') NOT NULL DEFAULT 'Practice',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `game_ref_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=411 DEFAULT CHARSET=latin1;

/*Data for the table `game_play` */

/*Table structure for table `game_played_tracker` */

DROP TABLE IF EXISTS `game_played_tracker`;

CREATE TABLE `game_played_tracker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `played_date` date NOT NULL,
  `spend_game_amount` double unsigned NOT NULL,
  `players_id` bigint(20) unsigned NOT NULL,
  `created_at` varchar(20) NOT NULL,
  `updated_at` varchar(20) NOT NULL DEFAULT '',
  `remarks` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `spend_game_amount` (`spend_game_amount`),
  KEY `players_id` (`players_id`),
  CONSTRAINT `game_played_tracker_ibfk_2` FOREIGN KEY (`players_id`) REFERENCES `players` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `game_played_tracker` */

/*Table structure for table `games` */

DROP TABLE IF EXISTS `games`;

CREATE TABLE `games` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `_uuid` varchar(255) NOT NULL,
  `deals` enum('0','2','3','4','6') NOT NULL DEFAULT '2',
  `entry_fee` float unsigned NOT NULL,
  `bet_type` enum('Low','Medium','High') DEFAULT NULL,
  `game_sub_type` enum('Points','Pool','Deals') NOT NULL DEFAULT 'Points',
  `game_title` varchar(60) NOT NULL,
  `game_type` enum('Cash','Practice') NOT NULL DEFAULT 'Cash',
  `number_of_cards` enum('13','21') NOT NULL DEFAULT '13',
  `number_of_deck` int(1) NOT NULL DEFAULT '2',
  `point_value` float unsigned NOT NULL,
  `pool_deal_prize` float unsigned NOT NULL,
  `pool_game_type` int(11) unsigned NOT NULL DEFAULT '0',
  `reward_points` float unsigned NOT NULL,
  `seats` enum('2','4','6') NOT NULL DEFAULT '2',
  `vip` bit(1) NOT NULL DEFAULT b'0',
  `token` varchar(100) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=latin1;

/*Data for the table `games` */

insert  into `games`(`id`,`active`,`_uuid`,`deals`,`entry_fee`,`bet_type`,`game_sub_type`,`game_title`,`game_type`,`number_of_cards`,`number_of_deck`,`point_value`,`pool_deal_prize`,`pool_game_type`,`reward_points`,`seats`,`vip`,`token`,`created_at`,`updated_at`,`status`) values (53,'','HSIdFSsou4XUcgeuPVLwWktEJk68kJ','0',160,NULL,'Points','Points (13)','Practice','13',2,2,0,0,0,'2','\0','4OTyuvx62XvZG3RWnwuaJa977m2fA0','1554357082','1565800300',0),(54,'','PDAsxccbVY9jGTlXD4kg6ypn0aJ0zF','0',240,NULL,'Points','Points (13)','Practice','13',2,3,0,0,0,'4','\0','cPuteyhnY43nEXscihHTgwOTg0MGnq','1554357541','',1),(55,'','WZzut8Mz7LLnx6klMAG69voA3EudQU','0',400,NULL,'Points','Points (13)','Practice','13',2,5,0,0,0,'6','\0','e84OsFoc7zvoQcrukGBTD4RqJmgxpE','1554357560','',1),(56,'','BYy7mkpP9WQ6V1XPgn0dttwmvF1gSD','0',25,NULL,'Pool','Pool (101)','Practice','13',2,0,50,101,0,'2','\0','a81V6QtX1XCeykTHreuVbERt8gQX8A','1554357773','1554358045',0),(57,'','K01cYgikcGnSdjs25SxbRnnwPjVAJU','0',25,NULL,'Pool','Pool (201)','Practice','13',2,0,50,201,0,'2','\0','yCzRWGbVtsNK3qetoAkmk7cweJ7kGa','1554357795','1554358051',0),(58,'','dqFOPPsPQDhRSdWGKrFsuIK28W2Cvq','0',25,NULL,'Pool','Pool (101)','Practice','13',2,0,100,101,0,'4','\0','Htu1EBOcJeiUM0pZiYvm2aDjcdrRrS','1554357856','1554358056',0),(59,'','NFfwLvG99dIyRAFrtWtZIILcFE7nX4','0',25,NULL,'Pool','Pool (101)','Practice','13',2,0,50,101,0,'2','\0','KPUOmoTchxzwlnQwSYx1xdUwUSCoWL','1554358173','',1),(60,'','tJuYZck8F3298h14WtQkvGi2k3B011','0',50,NULL,'Pool','Pool (101)','Practice','13',2,0,200,101,0,'4','\0','gIQaLhvEEZbozOIDu2KNPyD2d4H3IH','1554358326','',1),(61,'','WiQebaYepCTmLoLWhmjOB1ynoXv0CH','0',100,NULL,'Pool','Pool (101)','Practice','13',2,0,600,101,0,'6','\0','W5OMg4YmFvdWZ9dzdkvioscjRyD5rF','1554358354','',1),(62,'','0MjIYVJ4pQkv3y0ZqKB9Ph6MVEIknU','0',25,NULL,'Pool','Pool (201)','Practice','13',2,0,50,201,0,'2','\0','9zqfc53Ub3jFsz9RVeT3CrUseyJLA3','1554358459','',1),(63,'','QUTB13bEBCfVquYrVEB5mdcBeHR3ng','0',50,NULL,'Pool','Pool (201)','Practice','13',2,0,200,201,0,'4','\0','8eYainRlpExvitKbDjgYmDNT8A76L0','1554358483','',1),(64,'','QLhOzLhwfntirE6DIqzMLiId8NvQLR','0',100,NULL,'Pool','Pool (201)','Practice','13',2,0,600,201,0,'6','\0','sEsXOrQWnk4hDFqz007C8bs60t6mgT','1554358499','',1),(65,'','nR9mKKNCfef8MTidnbJ0f57lN4maLa','2',25,NULL,'Deals','Deals','Practice','13',2,0,50,0,0,'2','\0','9fsQkuDfeJnUX3S9h3aeJnIrAvQsTq','1554358563','',1),(66,'','UKbJFkqUSvv6KQIZQMwnXxCuzg8MA0','4',50,NULL,'Deals','Deals','Practice','13',2,0,200,0,0,'4','\0','aaIrVThGrYGgObPT9ukJvWh5vySZfh','1554358582','',1),(67,'','8ueibgt0lj25nJ5weQNhGNDFhWAS0y','6',100,NULL,'Deals','Deals','Practice','13',2,0,600,0,0,'6','\0','sQQRocdVCM1g7SPByYaqZg5pWqmapW','1554358602','',1),(68,'','IOOZILkHcaAEjywJRs8Jql0NCItapP','0',2000,NULL,'Points','Points (13)','Cash','13',2,25,0,0,0,'2','\0','jp3K1Z20i11Rdrh2ofrD3PqyU4Uoi0','1554358719','1565800483',0),(69,'','QoY0IzhRRD7u3MxDagoFpKxv73yJaq','0',4000,NULL,'Points','Points (13)','Cash','13',2,50,0,0,0,'4','\0','5zxAIxRIpFDXpl7Td5i7ZSRb3sSvgl','1554358744','1565800546',0),(70,'','2Y6P4CZv3GrxgEpH2gljwnGexbR1Ij','0',8000,NULL,'Points','Points (13)','Cash','13',2,100,0,0,0,'6','\0','hL8u6EJdDIFff1u6qPCWgJlMO9VvH9','1554358771','1565800575',0),(71,'','w4Y8wl3kfFvrmMncbksbseotm34dc3','0',25,NULL,'Pool','Pool (101)','Cash','13',2,0,50,101,0,'2','\0','4PyFSvRvKIRQzaUL44huxviugUT5rJ','1554358824','1565955311',0),(72,'','oH3x56u3x9DDJj57PjMu2QIb7OcGMX','0',50,NULL,'Pool','Pool (101)','Cash','13',2,0,200,101,0,'4','\0','b6ikbaINddW52qvAr1oRHiwMhJyaAw','1554358843','',1),(73,'','eGLR3cXNvVcH7fwaSfTEaBuboIoFFC','0',100,NULL,'Pool','Pool (101)','Cash','13',2,0,600,101,0,'6','\0','nJAadQzKV3qnbeR5NOioXy3gm3zrKy','1554358861','',1),(74,'','b1VJ7Cgeo4GbDo2ZEEAvmmTqPhwFRZ','0',25,NULL,'Pool','Pool (201)','Cash','13',2,0,50,201,0,'2','\0','VlJNjGelkY3NqHmGar2UnG2IzRuL6n','1554358892','1565955298',0),(75,'','Bp6srD8Mjm7SLN8TG5UWKCqVutSBcz','0',50,NULL,'Pool','Pool (201)','Cash','13',2,0,200,201,0,'4','\0','OopvxcXyh432QrVH9ZorSPXz3yKbkC','1554358958','',1),(76,'','mCffLFgeB0FUMrDnuTwaXLmC8WwLBV','0',100,NULL,'Pool','Pool (201)','Cash','13',2,0,600,201,0,'6','\0','swXobvH1bVo58nYr72VTp44CYiKfwb','1554358979','',1),(77,'','lDlUqkCvRHxAkpZQSSiLqQbxzgJhZe','2',25,NULL,'Deals','Deals','Cash','13',2,0,50,0,0,'2','\0','CEzYqnr5mePp2opDVai2mxOGEp1HeK','1554359013','1565954061',0),(78,'','Ak1X6EYCvqCqnQAnTWfarRS48QXBVQ','4',50,NULL,'Deals','Deals','Cash','13',2,0,200,0,0,'4','\0','LHmRscYsH0eU7WoDbcKHOKOrHxdmRK','1554359035','1565954118',0),(79,'','rIf4gtobfJfpqBf8kRhPX2aR8GPwqO','6',100,NULL,'Deals','Deals','Cash','13',2,0,600,0,0,'6','\0','lsiu9plK5mKok7ueJB3EWR7zkjDLyd','1554359064','1565954142',0),(80,'','6iAeeeS56i0R3qpEJyrbYZ2eGm4Axv','0',80,NULL,'Points','Points (13)','Practice','13',2,1,0,0,0,'2','\0','9tubiGccsDJ9fp6za39o4lqG5SZRxU','1556189903','',1),(81,'','MW3e59HxwhlDU7cTZskpTBB3HZZJLS','0',80,NULL,'Points','Points (13)','Cash','13',2,1,0,0,0,'2','\0','eR1xe0vjgVQ9QiOqDr3zHVxpHAkAib','1565775309','1565953865',0),(82,'','zdjj5iWJpXaXPkq6UjJXDC81L3Rsii','0',4,NULL,'Points','Points (13)','Cash','13',2,0.05,0,0,0,'2','\0','2Pb7h2yyYAnvs2RFleUDUVDUt6omge','1565781590','',1),(83,'','UKxrV2WO03ScsFebJnrUMfdyQu1Za7','0',8,NULL,'Points','Points (13)','Cash','13',2,0.1,0,0,0,'2','\0','T7iMJvJfin9f6LnJyVRXu82gu8Uebv','1565781611','1565953903',0),(84,'','Q6Hw9piOKCeKtEpFNVEwUjypPcO2p4','0',240,NULL,'Points','Points (13)','Cash','13',2,3,0,0,0,'2','\0','gHOlMhLnLsne08vP43OKuV4u53bkVV','1565781880','1565953837',0),(85,'','UnoUN78vZqbVYmxg5NTd4XZDLEwLzJ','0',800,NULL,'Points','Points (13)','Cash','13',2,10,0,0,0,'2','\0','jRN86BHG26OYcHt8VGt6WuWmihnaKy','1565781910','1565953812',0),(86,'','U7KUWP7uzyOtSyqT8Cz9zATH58WrPE','0',4,NULL,'Points','Points (13)','Cash','13',2,0.05,0,0,0,'4','\0','w3cAzhpgwu01ZV2Cw0W2tCoCltV6Js','1565793479','',1),(87,'','uw4phz68Q8hl4tCAzunF7oVNcEZl8e','0',8,NULL,'Points','Points (13)','Cash','13',2,0.1,0,0,0,'4','\0','PtAK2z5MczSDs04CxsGzlbio4KOxQ7','1565793529','1565953936',0),(88,'','Q9pp5UNpUg1TAQm3A0RJ61S0EpuTSm','0',80,NULL,'Points','Points (13)','Cash','13',2,1,0,0,0,'4','\0','iFkENqCQfTKWWhAHBvDewmAuqgXW7Y','1565793549','1565953925',0),(89,'','2rlli0GqnFKmvE9br1SBTgI9RhUEqj','0',240,NULL,'Points','Points (13)','Cash','13',2,3,0,0,0,'4','\0','lFNAhQ53jPcvRUTf8hG4chpwf630Um','1565793573','1565953917',0),(90,'','MttwYBvRTlGDgGccrcqyq35hHmS9tp','0',800,NULL,'Points','Points (13)','Cash','13',2,10,0,0,0,'4','\0','awVbxkuVHIlOjj80HEubmGUka5BVhY','1565793597','1565953911',0),(91,'','FADGW619QTWsKu0zLXFzk8bxV9JVLf','0',2000,NULL,'Points','Points (13)','Cash','13',2,25,0,0,0,'4','\0','V4iClGfslxgoV2ZoDJpwyiIuE4r3uh','1565793615','1565800534',0),(92,'','22KLCIA0QGNbIwnD4Db7IbqDzmXdmF','0',4,NULL,'Points','Points (13)','Cash','13',2,0.05,0,0,0,'6','\0','xTbVCnI9vlKQr0Zh3RzsUyNDf3Mhw0','1565793680','',1),(93,'','r8ZHxiI1M6vkpAXHwoibXcBA5bbtQ2','0',8,NULL,'Points','Points (13)','Cash','13',2,0.1,0,0,0,'6','\0','Wp0AIDTgmCGpcpx9Qb7eHTFpvG13ka','1565793700','1565953966',0),(94,'','gAC1JHbA6XFeebBwhLlXK9OTCwfkgZ','0',80,NULL,'Points','Points (13)','Cash','13',2,1,0,0,0,'6','\0','ubdj58aFdQTw3ZfFy707PLUZPDo4zm','1565793721','1565953961',0),(95,'','TKUb9XsCE4PlOMIJbrlUpuL5BFc5Yr','0',240,NULL,'Points','Points (13)','Cash','13',2,3,0,0,0,'6','\0','mf0ZZ5o1zE2bocFhF7tOc0FTdZEgMR','1565793770','1565953953',0),(96,'','yz9jT5XTKA2r5nTG3N4glx8Dx8ZNiJ','0',800,NULL,'Points','Points (13)','Cash','13',2,10,0,0,0,'6','\0','S3OSrNgHPl0GWPcbS7jPhp8ggZramS','1565793804','1565953946',0),(97,'','PXrEbYUI2GpgVWRSd29vRmMiWt8r6g','0',2000,NULL,'Points','Points (13)','Cash','13',2,25,0,0,0,'6','\0','UsvZFHgaPbrBOdHzHShqKYMV0ZJoEc','1565793822','1565800558',0),(98,'','I13ieiilPxDqdbGngxrxewMZLuBJ3K','0',4000,NULL,'Points','Points (13)','Cash','13',2,50,0,0,0,'6','\0','T7avR6R2O1RsS3QDGGzpGKLBCeS8Nc','1565793931','1565800562',0),(99,'','jIA0s5lmWVZNeWVqNz5cuWStqr4Vnz','0',400,NULL,'Points','Points (13)','Practice','13',2,5,0,0,0,'4','\0','gQKHZyjBUX4gzvKcbVjsRIZRDYst7V','1565794101','1565800312',0),(100,'','bOol49T9E2NKAh6UuEjVWxcfmc6WiJ','0',240,NULL,'Points','Points (13)','Practice','13',2,3,0,0,0,'6','\0','QZ73jEewlx6VSRO2iMAHTfdShtKwwn','1565794136','1565800317',0),(101,'','Jys8eDP7UW8mLp6HcM1LC1JHzLcYAk','0',10,NULL,'Pool','Pool (101)','Cash','13',2,0,20,101,0,'2','\0','kGwFMzG12P7s5rrHDfQSe1Xi0wvGnR','1565794307','1565955285',0),(102,'','G4Ne3p2W9QcwddvJCK2i4ZBnHNZdoe','0',10,NULL,'Pool','Pool (101)','Cash','13',2,0,20,101,0,'2','\0','0tnpYAZw9zfxkzKVhnX30bfCGf1nDR','1565794331','1565794422',0),(103,'','xnMtTxQmt3dhuxhICHAJRF6R9MNPZm','0',5,NULL,'Pool','Pool (101)','Cash','13',2,0,10,101,0,'2','\0','fAL3wLIhGIOB2FGVC6ukkx07pzupiu','1565794465','',1),(104,'','eIz8yNdoUfx3AQJmK5sOZ61yO6ift8','0',50,NULL,'Pool','Pool (101)','Cash','13',2,0,100,101,0,'2','\0','3v0Zw7awdkkpEQ2lXN0y2rbZzIqyBE','1565794499','1565954500',0),(105,'','flpRkrV1HaZCmbgSNocnaIcwAt3AMY','0',100,NULL,'Pool','Pool (101)','Cash','13',2,0,200,101,0,'2','\0','n81s5gbNPjrkXRdIlGX889DdbP4RM9','1565794516','1565954308',0),(106,'','iKTzOvt08zmy7FfaAYUorCsEQ0o1TB','0',200,NULL,'Pool','Pool (101)','Cash','13',2,0,400,101,0,'2','\0','ZxfifMkmJVedd12rUiVzRDDhhKo0db','1565794554','1565954275',0),(107,'','UDDb2GVPfHi6xcCSFpBa51R0QpZFdI','0',500,NULL,'Pool','Pool (101)','Cash','13',2,0,1000,101,0,'2','\0','hxotzRHQNOBO1ZFLy1rsQQt99UcjTS','1565794666','1565800706',0),(108,'','V5H4blHi7zx2bd2YKa0eLv34UNQ6B5','0',1000,NULL,'Pool','Pool (101)','Cash','13',2,0,2000,101,0,'2','\0','XPhd1WKHHJalArJG3wR8sFc4HCkhpf','1565794679','1565800699',0),(109,'','1cQBTWdTBFto7EjR4OM0AZqngxsCNu','0',5,NULL,'Pool','Pool (13)','Cash','13',2,0,10,201,0,'2','\0','8TM942CEnHZrUMyJmQ79wIzRVTPdkd','1565794710','1565954513',0),(110,'','47R0l04ACvvawd3pSgj2tRqJBxdyt1','0',10,NULL,'Pool','Pool (201)','Cash','13',2,0,20,201,0,'2','\0','3gR92A5rqbVhTldxXvTgrDnr9PybJ4','1565794725','1565954225',0),(111,'','wAWpwzZhNFzLyQhdgQH3B4ylgLKmEQ','0',50,NULL,'Pool','Pool (201)','Cash','13',2,0,100,201,0,'2','\0','HINfEGeYyEKvPCv718LQR1AnaA13HI','1565794748','1565954217',0),(112,'','UBrYISY3J4Fr21WcPeP4bslV5Q91Nd','0',100,NULL,'Pool','Pool (201)','Cash','13',2,0,200,201,0,'2','\0','j4tNAP9fBdhZyrB2EI0i2zSskl5Jex','1565794799','1565954209',0),(113,'','oIJHI1HdIxaI2RnYQZhtztKqaVWUlA','0',200,NULL,'Pool','Pool (201)','Cash','13',2,0,400,201,0,'2','\0','iy4mQJZGw9bKqyl1SjK3Qx6GLUiSO6','1565794825','1565954202',0),(114,'','oe21dULUvw6cGBvuFMYcXfHodqmxar','0',500,NULL,'Pool','Pool (201)','Cash','13',2,0,1000,201,0,'2','\0','eixSHp7EAu15PW5WzjolGEuU5K9zFD','1565794850','1565800691',0),(115,'','Eot50eNgkvPHxky30VgjH9vriqL86u','0',1000,NULL,'Pool','Pool (201)','Cash','13',2,0,2000,201,0,'2','\0','50IUC1TWUFjjwU0YyBdPk1OxeiiC8f','1565794863','1565800677',0),(116,'','INkjcVmxKWpjbkBo4ATFpxu6PnKgPn','0',25,NULL,'Pool','Pool (101)','Cash','13',2,0,100,101,0,'4','\0','K88ev52nt8R21untoT5Z4YvjOZu8XN','1565794938','',1),(117,'','mxV7p2t89jETG9VKgnmzrEZRcqaTOr','0',100,NULL,'Pool','Pool (101)','Cash','13',2,0,400,101,0,'4','\0','RJx3kO5y3uPS0HZKCgfp8zCYV56XL6','1565794952','',1),(118,'','fy5pMGY4hXMlReaBviQXrYQ2UElOlx','0',200,NULL,'Pool','Pool (101)','Cash','13',2,0,800,101,0,'4','\0','8koZLcUQxMQYw63xj6sQs1d6TYIiiy','1565794966','',1),(119,'','nX7LsRwacpj7LYBVgonkIw2XCiKE40','0',25,NULL,'Pool','Pool (201)','Cash','13',2,0,100,201,0,'4','\0','BQXieq8vN71nVEFjuSDXEDq0YEl82G','1565794988','',1),(120,'','TZqH1jSKgSKC7KOvJsD07MyYEfd7vW','0',100,NULL,'Pool','Pool (201)','Cash','13',2,0,400,201,0,'4','\0','P4dDpNZNOwVLWIIq9WAJqP2ET2eexN','1565795000','',1),(121,'','Lam1KzqadkuO1q4yMIjb1tZrc7noDq','0',200,NULL,'Pool','Pool (201)','Cash','13',2,0,800,201,0,'4','\0','gHwevIcC7TCWoB12gEUwz6aeP7MNS9','1565795022','',1),(122,'','CR8pGTL1CkoeC84nLFkRkZbKnCqI3v','0',25,NULL,'Pool','Pool (101)','Cash','13',2,0,150,101,0,'6','\0','aw5nqFXoKm7uu0q0zBfjob78qcZj4l','1565795053','',1),(123,'','WEagu9vGzzb5Y6YuMUaRL9b4xuqJEo','0',50,NULL,'Pool','Pool (101)','Cash','13',2,0,300,101,0,'6','\0','VpJRzlqOK84Q66JbNQ7kBOocFFttbz','1565795065','',1),(124,'','Wk68N6ljDOY4QhtQPNSfsiuSnvAIJ0','0',200,NULL,'Pool','Pool (101)','Cash','13',2,0,1200,101,0,'6','\0','8nReTWAvFUQEwF7vOPbitd2fLaybxV','1565795079','',1),(125,'','ZcbUgDfclPy88oXihoLAiUQCreFSCY','0',25,NULL,'Pool','Pool (201)','Cash','13',2,0,150,201,0,'6','\0','YjD3V59EZS3i9UGaMyYiSsG1ziLZ1P','1565795132','',1),(126,'','Q177pGH5ovbkqaX7xGyCmJU93cTbR3','0',50,NULL,'Pool','Pool (201)','Cash','13',2,0,300,201,0,'6','\0','GOsZIwv71FqVNX5Nw2o2GNYpVxhiZv','1565795144','',1),(127,'','gFjABTVVvBduXVWiRnRoCNVWHzaE58','0',200,NULL,'Pool','Pool (201)','Cash','13',2,0,1200,201,0,'6','\0','xlTOI5VXrk62fNg9BON0OsB2CJTE7f','1565795167','',1),(128,'','kYyvqaYtxbgmr6Noz3xW7FJFWJhS8w','2',5,NULL,'Deals','Deals','Cash','13',2,0,10,0,0,'2','\0','kvXKcSE9DBPpjIQXxeh1kKDRw6U5p4','1565795268','',1),(129,'','w6FBwBmPyXbQF70kzu7opykc0MN2zv','2',10,NULL,'Deals','Deals','Cash','13',2,0,20,0,0,'2','\0','OpwI2XSeqgBEg3smGhNqxZCvIM7UM5','1565795283','1565954006',0),(130,'','7IM06q6UkSqgTphYxfufkhTd2OhR8a','2',50,NULL,'Deals','Deals','Cash','13',2,0,100,0,0,'2','\0','Fo6zLUkQSHMTI6lkxwhub8zlxp0kEh','1565795300','1565953998',0),(131,'','59yKXhQVFjQYYrlLBx4iB3K9XW0JTi','2',100,NULL,'Deals','Deals','Cash','13',2,0,200,0,0,'2','\0','CrZzX2Q1mne1kXGRFZyVOszNEWSucJ','1565795314','1565953990',0),(132,'','d1tSZVREiNukmDHxuJdlk9EgHtOXzA','2',200,NULL,'Deals','Deals','Cash','13',2,0,400,0,0,'2','\0','iklCLQIYdNGx1kRNxg3wcHUV6ejLiE','1565795331','1565953983',0),(133,'','myx8o9h1nyApVG57dkkh3W41Cg0f1V','2',500,NULL,'Deals','Deals','Cash','13',2,0,1000,0,0,'2','\0','dPjDr0cSwo6MMlClHIMh1Sk3vIxHqj','1565795345','1565800892',0),(134,'','PZClN1NLspnlEj4BOxR9v5pZvsdmcj','2',1000,NULL,'Deals','Deals','Cash','13',2,0,2000,0,0,'2','\0','7WKcwzmGbmrZrV1guP3WldN13PG8pU','1565795358','1565800887',0),(135,'','15Td66J8EjpL25RgYyc8kJMoDW2DSW','2',5,NULL,'Deals','Deals','Cash','13',2,0,20,0,0,'4','\0','P5guUScy4HqLA4BfZtvYDSty46vDb1','1565795460','1565795890',0),(136,'','6XWq4UO2RtTBYJmZxbJU54BpJV7oB1','2',10,NULL,'Deals','Deals','Cash','13',2,0,40,0,0,'4','\0','FyjS9cf8IiPBk5wAqv0vqMgersd5zq','1565795482','1565795894',0),(137,'','4bxijz9a0mpCtRmX1KNYUSdpv4xI7y','2',25,NULL,'Deals','Deals','Cash','13',2,0,100,0,0,'4','\0','tcKIQBgQy61T6IkyAHdsxrmtMTqgk6','1565795496','1565795897',0),(138,'','aC4ghwMBaZSrPOmLMF49haD8uVZYDU','2',50,NULL,'Deals','Deals','Cash','13',2,0,200,0,0,'4','\0','dE5WpzVmJP1Enrtput1YYT7IAm2Ffh','1565795511','1565795901',0),(139,'','Cm41Yz6scuB80OTUS86MJ050B7PaMI','4',5,NULL,'Deals','Deals','Cash','13',2,0,20,0,0,'4','\0','hrHQHi6oMjCIhOTPhyTj6AFh6BEOpY','1565796101','1565801179',0),(140,'','OuJlUBvlmnV8kdmBgWN2whhKHrXhn2','4',10,NULL,'Deals','Deals','Cash','13',2,0,40,0,0,'4','\0','Zy6KcP9LB1Xjp11Fl9ECT3FkYiihNs','1565796118','1565801171',0),(141,'','HEhMQFTcWCd5OeZN7OEVRg01BsSAPz','4',25,NULL,'Deals','Deals','Cash','13',2,0,100,0,0,'4','\0','jAZ1SFIjIPG0M2q5UxblIRWELHODGF','1565796136','1565954085',0),(142,'','U73JNqC8CG1bNqXSz0KZBJcGWgf55X','4',100,NULL,'Deals','Deals','Cash','13',2,0,400,0,0,'4','\0','FKEamvDtCOHqVhXE1tWpKWuctS4VfR','1565796151','1565954077',0),(143,'','9uDETAl03qvLnR7pbgEzAZoo5bc8qo','4',200,NULL,'Deals','Deals','Cash','13',2,0,800,0,0,'4','\0','EZps5IQ6VLav4MjOcErjZmvG3axFJn','1565796164','1565954069',0),(144,'','ZYO4arPXy0N3sIg5FrN9reat0TuhXE','4',500,NULL,'Deals','Deals','Cash','13',2,0,2000,0,0,'4','\0','1OmCbV4kIiknnpMv6F2AMYnIl446hB','1565796179','1565801157',0),(145,'','ZtDXs6HYOTS7y2k3rq8Wya2SWw3fEq','4',1000,NULL,'Deals','Deals','Cash','13',2,0,4000,0,0,'4','\0','HDqCHVbn2qg0mumsI8EAxwxawNQA3Y','1565796199','1565801150',0),(146,'','8L7DSGqB0EmByXvUQFNDObR6QkUbqw','6',5,NULL,'Deals','Deals','Cash','13',2,0,30,0,0,'6','\0','hnLTpLO0POfJnZIIuHg11u5jrB2fYj','1565796215','1565801118',0),(147,'','FsegFx5bhguGy2IvIWxqpL5rnuUGsD','6',10,NULL,'Deals','Deals','Cash','13',2,0,60,0,0,'6','\0','vuyHvkbDjIZXVuugHKjtG3hq6F4SZl','1565796233','1565801135',0),(148,'','hNXJqFbS6qfBoBW5Gbangsup4mSrSU','6',25,NULL,'Deals','Deals','Cash','13',2,0,150,0,0,'6','\0','izGSrsrryFefGeFgvNcaPHwu55rQyI','1565796249','1565954136',0),(149,'','KHVmnLDIJnXVyC97Z1uGS1TXfwHbpi','6',50,NULL,'Deals','Deals','Cash','13',2,0,300,0,0,'6','\0','UOirkW9hlx0XB8uSFeAg8o8II0TFnL','1565796270','1565954131',0),(150,'','uvbhl76oVL5LsQSAWB8xYfTDkaKFEf','6',200,NULL,'Deals','Deals','Cash','13',2,0,1200,0,0,'6','\0','wldqRBqqBgUUZOU6jM4s8VTHBa1smu','1565796287','1565954126',0),(151,'','E6fMznTOSZ2951CTPzMZw2A5N4rSx8','6',500,NULL,'Deals','Deals','Cash','13',2,0,3000,0,0,'6','\0','FRKYAlvIrryMlWfgXZO0O0K3lQWW4Y','1565796302','1565800914',0),(152,'','iHepB1pEHMWhyALkINnITJ3H7rdPMU','6',1000,NULL,'Deals','Deals','Cash','13',2,0,6000,0,0,'6','\0','q2PxVsnww7Ouuz61VBXeEcHGDKeQO6','1565796314','1565800906',0),(153,'','r7WvkDsKixAh2FcTJwvXSs5uioNdNQ','0',5,NULL,'Pool','Pool (101)','Cash','13',2,0,20,101,0,'4','\0','cr3hGebapmFvMNzENprxoQ2FmgqQCy','1565947891','',1),(154,'','6EowlzpyzcnlRObLohqnjx8ERkYVQh','0',10,NULL,'Pool','Pool (101)','Cash','13',2,0,40,101,0,'4','\0','fEmkLLjrbk1bXheko6H5DdDTc7guoc','1565947906','',1),(155,'','9cFDh3TyPYIO2Derg4QIZYd89Sd5Rk','0',5,NULL,'Pool','Pool (201)','Cash','13',2,0,20,201,0,'4','\0','4dPYUYbU8JtMv2DA9LSUC2wSyAl4AI','1565947920','',1),(156,'','RJ0IfIViqxoQL11YWW79Ljuv6MrecP','0',10,NULL,'Pool','Pool (201)','Cash','13',2,0,40,201,0,'4','\0','giXXOYKMYeGiYbfokjH8Urf6WtASIi','1565947933','1565953650',0),(157,'','cxpyiKYLYDYg5TNjEXpUAAIL15m97N','4',5,NULL,'Deals','Deals','Cash','13',2,0,20,0,0,'4','\0','G1CrRPfKPer3EpheZZIkwNekZIT2L3','1565954164','',1),(158,'','W1Xk1Q9z8CK2Mq9IJIzurj0nupOo6w','6',5,NULL,'Deals','Deals','Cash','13',2,0,30,0,0,'6','\0','ewDFvbKj7uOG60J2Jp1lk8IaVJd5H4','1565954182','',1);

/*Table structure for table `gift_item` */

DROP TABLE IF EXISTS `gift_item`;

CREATE TABLE `gift_item` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `type` varchar(200) NOT NULL,
  `img` varchar(1000) NOT NULL,
  `gif_img` varchar(1000) NOT NULL,
  `gift_sound` varchar(1000) NOT NULL,
  `chips` varchar(100) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `updated_at` varchar(25) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/*Data for the table `gift_item` */

/*Table structure for table `global_configuration_constants` */

DROP TABLE IF EXISTS `global_configuration_constants`;

CREATE TABLE `global_configuration_constants` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key_name` varchar(100) NOT NULL,
  `value` varchar(100) NOT NULL,
  `display_order` int(11) NOT NULL DEFAULT '10',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `updated_at` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key_name`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;

/*Data for the table `global_configuration_constants` */

insert  into `global_configuration_constants`(`id`,`key_name`,`value`,`display_order`,`status`,`updated_at`) values (1,'SUPPORT_EMAIL','support@cloverrummy.com',2,1,'1555320813'),(2,'CONTACT_EMAIL','support@cloverrummy.com',3,1,'1555320813'),(5,'NO_REPLY_MAIL','info@cloverrummy.com',4,1,'1561551787'),(6,'BONUS_ON_EMAIL_VERIFIED','100',8,1,'1549689071'),(7,'BONUS_ON_MOBILE_VERIFIED','100',9,1,'1549689071'),(8,'BONUS_ON_REFER_FRIEND','100',1000,1,'1549689071'),(9,'MAX_FUN_CHIPS','10000',1000,1,'1549689071'),(10,'SITE_TITLE','cloverrummy',1,1,'1549689071'),(11,'BCC_EMAIL','vishnu.jangid@kpis.in',30,1,'1549689071'),(12,'BIRTH_DAY_BONUS_PERCENTAGE','10',10,1,'1549689071'),(13,'GST_PERCENTAGE','18',1000,1,'1565781511'),(14,'TDS_PERCENTAGE','31.2',1000,1,'1565781511'),(15,'ADMIN_COMMISSION_PERCENTAGE','15',1000,1,'1550835609'),(16,'FACEBOOK_PAGE_LINK','https://facebook.com',1000,1,'1550835609'),(17,'TWITTER_PAGE_LINK','https://twitter.com',1000,1,'1550835609'),(18,'GOOGLE_PLUS_PAGE_LINK','https://google.com',1000,1,'1550835609'),(20,'GOOGLE_MAP_API_KEY','please enter google map key to detect location',1000,1,'1550835609'),(21,'DOWNLOAD_ANDROID_APP_LINK','https://play.google.com/store/apps/details?id=com.venusgames',50,1,'1565613447'),(22,'CONTACT_ENQUIRES_RECEIVE_MAIL','support@cloverrummy.com',1000,1,'1555320813'),(23,'HELPDESK_NUMBER','9029373293',1000,1,'1565781511'),(24,'WELCOME_BONUS','5000',1000,1,'1550835609'),(25,'INFO_EMAIL','info@cloverrummy.com',1000,1,'1561551286'),(26,'WELCOME_BONUS_CODE','WELCOME',1000,1,''),(27,'MIN_FAKE_PLAYERS_COUNT','1000',5,1,''),(28,'MAX_FAKE_PLAYERS_COUNT','5000',6,1,''),(29,'SIGN_UP_CASH_AMOUNT','25',15,1,'1565781511'),(30,'GAME_SERVER_IP','cloverrummy.com',7,1,'1556801118'),(31,'EMAIL_VERIFIED_BONUS_VALIDITY_DAYS','30',8,1,''),(32,'MOBILE_VERIFIED_BONUS_VALIDITY_DAYS','30',9,1,''),(33,'WELCOME_BONUS_VALIDITY_DAYS','50',1000,1,'1565781511'),(34,'BIRTHDAY_BONUS_VALIDITY_DAYS','30',11,1,''),(35,'MAX_BIRTHDAY_BONUS_POINTS','500',12,1,'1553922575'),(36,'CASH_ON_EMAIL_VERIFIED','0',8,1,'1565781511'),(37,'CASH_ON_MOBILE_VERIFIED','0',9,1,'1565781511'),(38,'DOWNLOAD_IOS_APP_LINK','https://www.apple.com/',1000,1,'1559738574'),(39,'LOOSER_BONUS_CONVERSION_PERCENTAGE','5',50,1,''),(40,'MINIMUM_WINNING_AMOUNT_TO_APPLY_TDS','10000',10,1,''),(41,'INSTAGRAM_PAGE_LINK','https://www.instagram.com/?hl=en',10,1,''),(42,'RPS_POINT_PER_SPEND_CASH','10',100,1,'1553922575'),(43,'MINIMUM_AMOUNT_FOR_WITHDRAW_MONEY','500',10,1,'1565781511'),(44,'MAXIMUM_AMOUNT_FOR_WITHDRAW_MONEY','100000',10,1,'1565781511'),(45,'SMTP_HOST','smtp.zoho.com',1000,1,'1561465914'),(46,'SMTP_USERNAME','info@cloverrummy.com',1001,1,'1561549692'),(47,'SMTP_PASSWORD','ESEAj2y7h8tg',1002,1,'1561551286'),(48,'DOMAIN_NAME_WITHOUT_PREFIX','cloverrummy.com',10,1,''),(49,'WEBSITE_URL','https://cloverrummy.com',10,1,'1561374439'),(50,'MOBILE_WEBSITE_URL','https://cloverrummy.com',10,1,'1561374438'),(51,'DAILY_LOGIN_BONUS_POINTS','100',10,1,'1562402505'),(52,'DAILY_LOGIN_BONUS_VALIDITY_DAYS','1',10,1,'1562402505'),(53,'DEV_WEBSITE_URL','https://cloverrummy.com',10,1,'1562402505'),(54,'DEV_MOBILE_WEBSITE_URL','https://cloverrummy.com',10,1,'1562402505'),(55,'MAX_REFERRAL_BONUS_ON_DEPOSIT','2000',10,1,'1562402505'),(56,'DEV_GAME_SERVER_IP','https://cloverrummy.com',7,1,'1562402505'),(57,'TESTING_GAME_SERVER_IP','https://cloverrummy.com',7,1,'1561374438'),(58,'MINIMUM_AMOUNT_FOR_DEPOSIT','100',10,1,'1561549692'),(59,'CASH_GAME_BONUS_CONVERSION_PERCENTAGE','5',50,1,'1563965188'),(60,'WEBSITE_CONTACT_NUMBER','99999999999',10,1,'9999999999'),(61,'WEBSITE_CONTACT_ADDRESS','demo address',10,1,'4125244587');

/*Table structure for table `history` */

DROP TABLE IF EXISTS `history`;

CREATE TABLE `history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(100) NOT NULL,
  `table_primary_key_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `action` varchar(250) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `table_primary_key_id` (`table_primary_key_id`)
) ENGINE=InnoDB AUTO_INCREMENT=449 DEFAULT CHARSET=latin1;

/*Data for the table `history` */

/*Table structure for table `homepage_sliders` */

DROP TABLE IF EXISTS `homepage_sliders`;

CREATE TABLE `homepage_sliders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(25) NOT NULL,
  `display_in` enum('Desktop Website','Mobile Website') NOT NULL,
  `image` varchar(100) NOT NULL,
  `created_at` varchar(20) NOT NULL,
  `updated_at` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;

/*Data for the table `homepage_sliders` */

insert  into `homepage_sliders`(`id`,`title`,`display_in`,`image`,`created_at`,`updated_at`,`status`) values (71,'First Policy Latter','Desktop Website','0c121e7ae76f69b0a0967067e2317174.png','1566469656','',1);

/*Table structure for table `join_play_transactions` */

DROP TABLE IF EXISTS `join_play_transactions`;

CREATE TABLE `join_play_transactions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `players_id` bigint(20) unsigned NOT NULL,
  `games_id` bigint(20) unsigned NOT NULL,
  `rooms_id` bigint(20) NOT NULL,
  `bid_amount` double NOT NULL,
  `game_type` enum('Cash','Practice') NOT NULL DEFAULT 'Practice',
  `taken_from_deposit_balance` double NOT NULL,
  `taken_from_withdrawal_balance` double NOT NULL,
  `created_date_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `players_id` (`players_id`),
  KEY `rooms_id` (`rooms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `join_play_transactions` */

/*Table structure for table `joined_players_for_tournaments` */

DROP TABLE IF EXISTS `joined_players_for_tournaments`;

CREATE TABLE `joined_players_for_tournaments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `players_id` bigint(20) unsigned NOT NULL,
  `player_status` enum('Joined') NOT NULL,
  `joined_date_time` datetime NOT NULL,
  `cloned_tournaments_id` bigint(20) unsigned NOT NULL,
  `position_in_tournament` bigint(20) DEFAULT NULL,
  `tournament_round_no` bigint(20) DEFAULT NULL,
  `position_in_round` bigint(20) DEFAULT NULL,
  `taken_from_deposit_balance` double DEFAULT '0',
  `taken_from_withdrawal_balance` double DEFAULT '0',
  `given_prize_money` double DEFAULT '0' COMMENT 'After tournament completed prize money will be updated here',
  `is_eliminated` tinyint(1) unsigned DEFAULT '0' COMMENT 'If elimated that will be treated as 1',
  PRIMARY KEY (`id`),
  KEY `players_id` (`players_id`),
  KEY `cloned_tournaments_id` (`cloned_tournaments_id`),
  CONSTRAINT `joined_players_for_tournaments_ibfk_1` FOREIGN KEY (`players_id`) REFERENCES `players` (`id`),
  CONSTRAINT `joined_players_for_tournaments_ibfk_2` FOREIGN KEY (`cloned_tournaments_id`) REFERENCES `cloned_tournaments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `joined_players_for_tournaments` */

/*Table structure for table `login_logs` */

DROP TABLE IF EXISTS `login_logs`;

CREATE TABLE `login_logs` (
  `id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `ip_address` varchar(25) NOT NULL,
  `address` text NOT NULL,
  `created_at` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `login_logs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `login_logs` */

/*Table structure for table `modules` */

DROP TABLE IF EXISTS `modules`;

CREATE TABLE `modules` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(100) NOT NULL,
  `parent_module_id` bigint(20) unsigned DEFAULT NULL COMMENT 'Modules table primary key',
  `display_order` tinyint(4) NOT NULL DEFAULT '10',
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`),
  KEY `parent_module_id` (`parent_module_id`),
  KEY `display_order` (`display_order`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

/*Data for the table `modules` */

insert  into `modules`(`id`,`module_name`,`parent_module_id`,`display_order`,`created_at`,`updated_at`,`status`) values (1,'Master Menu',0,10,'1549006266','1549006266',''),(2,'States',1,10,'1549006266','1549006266',''),(3,'Modules',1,10,'1549006266','1549006266',''),(4,'Roles & Employees',1,1,'1549008744','1549015014',''),(5,'Roles',4,2,'1549008744','',''),(6,'Employees',4,3,'1549008744','',''),(7,'Players',0,1,'1549015033','',''),(8,'Real Players',7,1,'1549025263','',''),(10,'Games',1,4,'1549261252','1552651011',''),(11,'Game Play Data',0,3,'1549275772','',''),(12,'Practice Games',11,1,'1549275917','',''),(13,'Cash Games',11,2,'1549275934','',''),(14,'Global Configurations',1,15,'1549688105','',''),(15,'Feedback',0,10,'1550034367','',''),(16,'Reported Problems',15,1,'1550034796','',''),(17,'Promotion System',0,10,'1550038362','',''),(18,'Bonus System',17,1,'1550038373','1565772536',''),(19,'Default Avatars',7,3,'1550729518','',''),(20,'Deposit Transactions',7,4,'1550897959','',''),(21,'Tournaments',1,5,'1552650986','1552651028',''),(22,'Offer Notifications',17,2,'1552891580','',''),(23,'Club Types',1,6,'1552994323','',''),(24,'Tournament Categories',1,5,'1553581015','1553581032',''),(25,'Testimonials',15,2,'1554796050','',''),(26,'Homepage Sliders',1,11,'1556093359','',''),(27,'App Version',1,10,'1566801493','','');

/*Table structure for table `newsletter_subscribers` */

DROP TABLE IF EXISTS `newsletter_subscribers`;

CREATE TABLE `newsletter_subscribers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `newsletter_subscribers` */

/*Table structure for table `offer_notifications` */

DROP TABLE IF EXISTS `offer_notifications`;

CREATE TABLE `offer_notifications` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `web_link` varchar(250) NOT NULL DEFAULT ' ',
  `description` text,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL DEFAULT ' ',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `offer_notifications` */

insert  into `offer_notifications`(`id`,`title`,`image`,`web_link`,`description`,`created_at`,`updated_at`,`status`) values (6,'August Amaze','809a95f51559a7a4da07de88b8fd5951.jpg','august_amaze',NULL,'1564126762',' ',1),(7,'Fabulous discount','a8df42eff41721b7557c889e678faaa9.jpg','fabulous_discount',NULL,'1564126781',' ',1);

/*Table structure for table `payment_gateway_settings` */

DROP TABLE IF EXISTS `payment_gateway_settings`;

CREATE TABLE `payment_gateway_settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `payment_gateway_name` enum('Razor Pay','Instamojo') NOT NULL,
  `test_key` varchar(100) NOT NULL,
  `test_secret` varchar(100) NOT NULL,
  `live_key` varchar(100) NOT NULL,
  `live_secret` varchar(100) NOT NULL,
  `created_at` varchar(25) DEFAULT NULL,
  `updated_at` varchar(25) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `payment_gateway_settings` */

insert  into `payment_gateway_settings`(`id`,`payment_gateway_name`,`test_key`,`test_secret`,`live_key`,`live_secret`,`created_at`,`updated_at`,`status`) values (1,'Razor Pay','','','','','','',1),(2,'Instamojo','','','','','','',1);

/*Table structure for table `player_kyc_verification_log` */

DROP TABLE IF EXISTS `player_kyc_verification_log`;

CREATE TABLE `player_kyc_verification_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment` varchar(100) NOT NULL,
  `previous_status` varchar(150) DEFAULT NULL,
  `current_status` varchar(150) NOT NULL,
  `players_id` bigint(20) unsigned NOT NULL,
  `users_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `players_id` (`players_id`),
  KEY `users_id` (`users_id`),
  CONSTRAINT `player_kyc_verification_log_ibfk_1` FOREIGN KEY (`players_id`) REFERENCES `players` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `player_kyc_verification_log` */

/*Table structure for table `player_login_logs` */

DROP TABLE IF EXISTS `player_login_logs`;

CREATE TABLE `player_login_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at_date` date NOT NULL,
  `created_at_time` time NOT NULL,
  `login_success` bit(1) NOT NULL,
  `player_id` bigint(20) unsigned NOT NULL,
  `address` text,
  PRIMARY KEY (`id`),
  KEY `FK1r0e7bxr2hc3icssoqp93km9s` (`player_id`),
  CONSTRAINT `player_login_logs_ibfk_1` FOREIGN KEY (`player_id`) REFERENCES `players` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `player_login_logs` */

/*Table structure for table `players` */

DROP TABLE IF EXISTS `players`;

CREATE TABLE `players` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `wallet_account_id` bigint(20) unsigned NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `new_email` varchar(100) NOT NULL COMMENT 'This is temp column when user updated with new mail value will be stored, once verified then new email updated to permanent email',
  `mobile` varchar(100) NOT NULL,
  `new_mobile` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `salt` varchar(100) NOT NULL,
  `gender` enum('Male','Female','Other') DEFAULT 'Male',
  `date_of_birth` date NOT NULL,
  `address_line_1` varchar(200) NOT NULL,
  `address_line_2` varchar(200) NOT NULL,
  `states_id` bigint(20) unsigned DEFAULT '0',
  `city` varchar(50) NOT NULL,
  `pin_code` varchar(10) NOT NULL,
  `referred_by_code` varchar(100) NOT NULL,
  `my_referral_code` varchar(100) NOT NULL,
  `access_token` varchar(150) NOT NULL,
  `welcome_message_seen` bit(1) NOT NULL DEFAULT b'0',
  `welcome_coupon_used` enum('NO','YES') NOT NULL DEFAULT 'NO',
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `last_login` varchar(100) NOT NULL,
  `email_verification_key` varchar(100) NOT NULL,
  `email_verified` bit(1) NOT NULL DEFAULT b'0',
  `mobile_verified` bit(1) NOT NULL DEFAULT b'0',
  `password_reset_key` varchar(100) NOT NULL,
  `facebook_account_id` varchar(50) NOT NULL,
  `google_account_id` varchar(255) NOT NULL,
  `bot_player` bit(1) NOT NULL DEFAULT b'0',
  `bot_status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `expertise_level` int(10) unsigned NOT NULL DEFAULT '1',
  `play_token` varchar(100) NOT NULL,
  `otp` int(6) unsigned DEFAULT '0',
  `address_proof_type` varchar(100) NOT NULL,
  `address_proof` varchar(100) NOT NULL,
  `pan_card_number` varchar(100) NOT NULL,
  `pan_card` varchar(100) NOT NULL,
  `address_proof_verified_at` varchar(25) NOT NULL DEFAULT ' ',
  `pan_card_verified_at` varchar(25) NOT NULL DEFAULT ' ',
  `address_proof_rejected_at` varchar(25) NOT NULL DEFAULT ' ',
  `pan_card_rejected_at` varchar(25) NOT NULL DEFAULT ' ',
  `address_proof_rejected_reason` varchar(250) NOT NULL,
  `pan_card_rejected_reason` varchar(250) NOT NULL,
  `address_proof_status` enum('Pending Approval','Rejected','Approved','Not Submitted') DEFAULT 'Not Submitted',
  `pan_card_status` enum('Pending Approval','Rejected','Approved','Not Submitted') DEFAULT 'Not Submitted',
  `contact_by_sms` bit(1) NOT NULL DEFAULT b'1',
  `contact_by_phone` bit(1) NOT NULL DEFAULT b'1',
  `contact_by_email` bit(1) NOT NULL DEFAULT b'1',
  `speak_language` varchar(25) NOT NULL DEFAULT 'Hindi',
  `sms_subscriptions` bit(1) NOT NULL DEFAULT b'1',
  `newsletter_subscriptions` bit(1) NOT NULL DEFAULT b'1',
  `forgot_password_verification_link` varchar(100) NOT NULL DEFAULT ' ',
  `device_type` varchar(100) NOT NULL DEFAULT '',
  `device_token` varchar(100) NOT NULL,
  `profile_pic` varchar(100) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1',
  `address_proof_back_side` varchar(100) NOT NULL,
  `bot_can_play` enum('Pool','Deals','Points','Tournament','Practice') NOT NULL DEFAULT 'Practice',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `my_referral_code` (`my_referral_code`),
  UNIQUE KEY `access_token` (`access_token`),
  UNIQUE KEY `wallet_id` (`wallet_account_id`),
  KEY `referred_by_code` (`referred_by_code`),
  KEY `states_id` (`states_id`),
  CONSTRAINT `players_ibfk_1` FOREIGN KEY (`wallet_account_id`) REFERENCES `wallet_account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12030 DEFAULT CHARSET=latin1;

/*Data for the table `players` */

/*Table structure for table `report_problem_logs` */

DROP TABLE IF EXISTS `report_problem_logs`;

CREATE TABLE `report_problem_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_problems_id` bigint(20) unsigned NOT NULL,
  `users_id` int(12) NOT NULL,
  `status` enum('New','In Progress','Completed','Invalid') NOT NULL,
  `message` text NOT NULL,
  `created_at` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `problem_id` (`report_problems_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `report_problem_logs` */

/*Table structure for table `report_problems` */

DROP TABLE IF EXISTS `report_problems`;

CREATE TABLE `report_problems` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `players_id` bigint(20) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `type_of_issue` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` enum('New','In Progress','Complete','Invalid') NOT NULL DEFAULT 'New',
  `version_code` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `report_problems` */

/*Table structure for table `role_wise_access_rights` */

DROP TABLE IF EXISTS `role_wise_access_rights`;

CREATE TABLE `role_wise_access_rights` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `roles_id` bigint(20) unsigned NOT NULL,
  `modules_id` bigint(20) unsigned NOT NULL,
  `view_permission` bit(1) NOT NULL DEFAULT b'0',
  `add_permission` bit(1) NOT NULL DEFAULT b'0',
  `edit_permission` bit(1) NOT NULL DEFAULT b'0',
  `delete_permission` bit(1) NOT NULL DEFAULT b'0',
  `all_permission` bit(1) NOT NULL DEFAULT b'0' COMMENT 'This is dummy only',
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`),
  KEY `roles_id` (`roles_id`,`modules_id`),
  KEY `modules_id` (`modules_id`),
  CONSTRAINT `role_wise_access_rights_ibfk_1` FOREIGN KEY (`modules_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_wise_access_rights_ibfk_2` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `role_wise_access_rights` */

insert  into `role_wise_access_rights`(`id`,`roles_id`,`modules_id`,`view_permission`,`add_permission`,`edit_permission`,`delete_permission`,`all_permission`,`created_at`,`updated_at`,`status`) values (1,14,4,'\0','\0','\0','\0','\0','1549013562','',''),(2,14,5,'\0','\0','\0','\0','\0','1549013562','',''),(3,14,6,'\0','\0','\0','\0','\0','1549013562','',''),(4,14,1,'\0','\0','\0','\0','\0','1549013562','',''),(5,14,2,'\0','\0','\0','\0','\0','1549013562','',''),(6,14,3,'\0','\0','\0','\0','\0','1549013562','','');

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`role_name`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Data for the table `roles` */

insert  into `roles`(`id`,`role_name`,`created_at`,`updated_at`,`status`) values (1,'Accounts','1518692158','1564125811','\0'),(2,'Administrator','1518692175','',''),(4,'Corporate Staff','1518692202','1564125817','\0'),(7,'IT','1518692237','1564125820','\0'),(8,'Kitchens','1518692244','1549013544','\0'),(9,'Marketing','1518692252','',''),(10,'Security','1518692259','1564125824','\0'),(11,'Trainer','1518692267','',''),(12,'Developer','1518692267','1518692267',''),(13,'Tester','1518692267','1522751634','\0'),(14,'Customer','1520660827','',''),(15,'Software Tester','1521551736','1522751377','\0'),(17,'Center Admin','1523358175','1549013551','\0'),(18,'CENTER','1523358421','1523358460','\0'),(19,'CA','1523358831','1523358846','\0'),(22,'Digital Marketing','1527171954','1564125800','\0'),(24,'Management','1528271672','1564125793','\0'),(25,'Finance','1529396394','1564125807','\0'),(26,'Admin','1538978287','1553671164','');

/*Table structure for table `rooms` */

DROP TABLE IF EXISTS `rooms`;

CREATE TABLE `rooms` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `room_ref_id` varchar(20) NOT NULL,
  `created_date_time` datetime NOT NULL,
  `destroyed_date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `room_ref_id` (`room_ref_id`)
) ENGINE=InnoDB AUTO_INCREMENT=167 DEFAULT CHARSET=latin1;

/*Data for the table `rooms` */

/*Table structure for table `rps_transaction_history` */

DROP TABLE IF EXISTS `rps_transaction_history`;

CREATE TABLE `rps_transaction_history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ref_id` varchar(50) NOT NULL,
  `transaction_type` enum('Credit','Debit') NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `closing_balance` double NOT NULL DEFAULT '0',
  `created_date_time` datetime NOT NULL,
  `wallet_account_id` bigint(20) unsigned NOT NULL,
  `remark` varchar(250) NOT NULL,
  `type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ref_id` (`ref_id`),
  KEY `wallet_account_id` (`wallet_account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `rps_transaction_history` */

/*Table structure for table `site_settings` */

DROP TABLE IF EXISTS `site_settings`;

CREATE TABLE `site_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_title` varchar(100) DEFAULT NULL,
  `logo` varchar(100) NOT NULL,
  `favicon` varchar(100) NOT NULL,
  `no_reply_email_id` varchar(100) NOT NULL COMMENT 'With this id an automated email will go to order placed customers, forgot password email',
  `contact_number` varchar(100) NOT NULL,
  `contact_email` varchar(60) DEFAULT NULL,
  `support_email` varchar(100) DEFAULT NULL,
  `address` text,
  `google_analytics_id` varchar(13) DEFAULT NULL,
  `google_maps_api_key` varchar(100) DEFAULT NULL,
  `facebook_app_id` varchar(100) DEFAULT NULL,
  `facebook_secret_key` varchar(100) DEFAULT NULL,
  `google_client_id` varchar(100) DEFAULT NULL,
  `google_secret_key` varchar(100) DEFAULT NULL,
  `one_signal_web_push_notifications` varchar(100) DEFAULT NULL,
  `tax_calculation_with` enum('Global','Restaurant Wise') NOT NULL DEFAULT 'Global',
  `delivery_calculation_settings` enum('Distance Based','Order Based') NOT NULL DEFAULT 'Distance Based',
  `active_payment_gateway` enum('Razor Pay','Instamojo') NOT NULL DEFAULT 'Instamojo',
  `payment_gateway_mode` enum('Live','Test') NOT NULL DEFAULT 'Test',
  `seo_title` text,
  `seo_keywords` text,
  `seo_description` text,
  `status` bit(1) NOT NULL DEFAULT b'1',
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `site_settings` */

insert  into `site_settings`(`id`,`site_title`,`logo`,`favicon`,`no_reply_email_id`,`contact_number`,`contact_email`,`support_email`,`address`,`google_analytics_id`,`google_maps_api_key`,`facebook_app_id`,`facebook_secret_key`,`google_client_id`,`google_secret_key`,`one_signal_web_push_notifications`,`tax_calculation_with`,`delivery_calculation_settings`,`active_payment_gateway`,`payment_gateway_mode`,`seo_title`,`seo_keywords`,`seo_description`,`status`,`created_at`,`updated_at`) values (1,'cloverrummy','534f2da02be6c88a0400fbd867d57e7b.png','72d4e704dd68d53c205640df609686db.png','no_reply@cloverrummy.com','9799005744','support@cloverrummy.com','','b136, vaishali nagar, jaipur','','','2366837603409294','dea88878b1c13b573ef0938e7d7997d2','','','','Global','Order Based','Instamojo','Test','Go4Food',NULL,NULL,'','1478613170','1547307858');

/*Table structure for table `sms_gateway_settings` */

DROP TABLE IF EXISTS `sms_gateway_settings`;

CREATE TABLE `sms_gateway_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` varchar(6) NOT NULL,
  `sms_gateway_username` varchar(25) NOT NULL,
  `sms_gateway_password` varchar(25) NOT NULL,
  `apikey` varchar(50) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `sms_gateway_settings` */

insert  into `sms_gateway_settings`(`id`,`sender_id`,`sms_gateway_username`,`sms_gateway_password`,`apikey`,`created_at`,`updated_at`,`status`) values (1,'','','','','','',1);

/*Table structure for table `states` */

DROP TABLE IF EXISTS `states`;

CREATE TABLE `states` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `state_name` varchar(100) NOT NULL,
  `play_permission` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) DEFAULT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `state_name` (`state_name`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;

/*Data for the table `states` */

insert  into `states`(`id`,`state_name`,`play_permission`,`created_at`,`updated_at`,`status`) values (1,'Andhra Pradesh',1,'2019-01-31 17:00:00','1553580299',''),(2,'Rajasthan',1,'2019-01-31 17:00:00',NULL,''),(3,'Tamilnadu',1,'2019-01-31 17:00:00','1551684614',''),(4,'Karnataka',1,'0000-00-00 00:00:00','1553060794',''),(5,'Kerala',1,'1553580319',NULL,''),(6,'Maharashtra',1,'1553580329',NULL,''),(7,'Goa',1,'1553580341',NULL,''),(8,'Uttar Pradesh',1,'1553580348','1553580358',''),(9,'Punjab',1,'1553580369',NULL,''),(10,'Madhya Pradesh',1,'1553580383',NULL,''),(11,'Chattisgarh',1,'1553580395',NULL,''),(12,'West Bengal',1,'1553580403',NULL,''),(13,'Sikkim',1,'1553580423',NULL,''),(14,'Tripura',1,'1553580430',NULL,''),(15,'Arunachal Pradesh',1,'1553580456',NULL,''),(16,'Telagana',1,'1553580484',NULL,''),(17,'Uttaranchal',1,'1553580507',NULL,''),(18,'Orissa',1,'1553580519',NULL,''),(19,'Nagaland',1,'1553580533',NULL,''),(20,'Mizoram',1,'1553580542',NULL,''),(21,'Meghalaya',1,'1553580552',NULL,''),(22,'Manipur',1,'1553580572',NULL,''),(23,'Jharkhand',1,'1553580588',NULL,''),(24,'Jammu and Kashmir',1,'1553580599',NULL,''),(25,'Himachal Pradesh',1,'1553580610',NULL,''),(26,'Haryana',1,'1553580619',NULL,''),(27,'Gujarat',1,'1553580629',NULL,''),(28,'Bihar',1,'1553580650','1553666191',''),(83,'Assam',1,'1553672027',NULL,'');

/*Table structure for table `temp_used_email_mobile` */

DROP TABLE IF EXISTS `temp_used_email_mobile`;

CREATE TABLE `temp_used_email_mobile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `players_id` bigint(20) unsigned NOT NULL,
  `value` varchar(100) NOT NULL,
  `created_date_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `value` (`value`),
  KEY `players_id` (`players_id`),
  CONSTRAINT `temp_used_email_mobile_ibfk_1` FOREIGN KEY (`players_id`) REFERENCES `players` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `temp_used_email_mobile` */

/*Table structure for table `testimonials` */

DROP TABLE IF EXISTS `testimonials`;

CREATE TABLE `testimonials` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name_and_address` varchar(250) NOT NULL,
  `prize_title` varchar(250) NOT NULL,
  `message` text NOT NULL,
  `image` varchar(100) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `testimonials` */

/*Table structure for table `tournament_categories` */

DROP TABLE IF EXISTS `tournament_categories`;

CREATE TABLE `tournament_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `priority` int(10) unsigned NOT NULL DEFAULT '20',
  `image` varchar(100) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `tournament_categories` */

/*Table structure for table `tournaments` */

DROP TABLE IF EXISTS `tournaments`;

CREATE TABLE `tournaments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tournament_categories_id` bigint(20) unsigned NOT NULL,
  `allowed` enum('All','For Selected Clubs') NOT NULL,
  `allowed_club_types` varchar(100) NOT NULL,
  `tournament_title` varchar(100) NOT NULL,
  `duration_in_minutes` int(10) unsigned NOT NULL DEFAULT '20',
  `frequency` enum('Monthly','Weekly','Daily','One Time') NOT NULL,
  `tournament_format` enum('Deals') NOT NULL,
  `entry_type` enum('Free','Cash') NOT NULL,
  `entry_value` double unsigned NOT NULL DEFAULT '1' COMMENT 'Alias Entry fee in RPS or tickets',
  `max_players` bigint(20) unsigned NOT NULL,
  `players_per_table` int(10) unsigned NOT NULL DEFAULT '6',
  `registration_start_date` varchar(20) NOT NULL DEFAULT '',
  `registration_start_time` time NOT NULL,
  `registration_close_date` varchar(20) NOT NULL DEFAULT '',
  `registration_close_time` time NOT NULL,
  `tournament_start_date` varchar(20) NOT NULL DEFAULT '',
  `tournament_start_time` time NOT NULL,
  `prize_type` enum('Fixed Prize','Depend on Players Joined','Tickets','Rps') NOT NULL,
  `expected_prize_amount` double unsigned NOT NULL,
  `tournament_commission_percentage` double NOT NULL,
  `token` varchar(100) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `week` varchar(20) DEFAULT '',
  `min_players_to_start_tournament` bigint(20) DEFAULT NULL,
  `tournament_structure` text COMMENT 'JSON Format',
  `premium_category` enum('Bronze','Silver','Gold','Diamond') DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`),
  KEY `tournament_categories_id` (`tournament_categories_id`),
  CONSTRAINT `tournaments_ibfk_1` FOREIGN KEY (`tournament_categories_id`) REFERENCES `tournament_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

/*Data for the table `tournaments` */

/*Table structure for table `tournaments_prize_distribution_config` */

DROP TABLE IF EXISTS `tournaments_prize_distribution_config`;

CREATE TABLE `tournaments_prize_distribution_config` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `from_rank` int(10) unsigned NOT NULL,
  `to_rank` int(10) unsigned NOT NULL DEFAULT '0',
  `min_prize_value` double unsigned DEFAULT '0',
  `prize_value` double NOT NULL COMMENT 'Rupees',
  `extra_prize_info` varchar(150) NOT NULL,
  `tournaments_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tournaments_id` (`tournaments_id`),
  CONSTRAINT `tournaments_prize_distribution_config_ibfk_1` FOREIGN KEY (`tournaments_id`) REFERENCES `tournaments` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `tournaments_prize_distribution_config` */

/*Table structure for table `user_meta` */

DROP TABLE IF EXISTS `user_meta`;

CREATE TABLE `user_meta` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `users_id` bigint(20) unsigned NOT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `mobile` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `gender` enum('Male','Female','Other') DEFAULT NULL,
  `address` text,
  `referred_by` varchar(100) DEFAULT NULL,
  `dob` date NOT NULL,
  `joining_date` date DEFAULT NULL,
  `payroll_status` bit(1) DEFAULT b'1',
  `pan_no` varchar(100) DEFAULT NULL,
  `esi_no` varchar(100) DEFAULT NULL,
  `pf_no` varchar(100) DEFAULT NULL,
  `license_no` varchar(100) DEFAULT NULL,
  `bank_name` varchar(100) DEFAULT NULL,
  `account_no` varchar(100) DEFAULT NULL,
  `ifsc_code` varchar(100) DEFAULT NULL,
  `micr_code` varchar(100) DEFAULT NULL,
  `branch_name` varchar(100) DEFAULT NULL,
  `ctc` varchar(100) DEFAULT NULL,
  `net_salary` varchar(100) DEFAULT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_id` (`users_id`),
  CONSTRAINT `user_meta_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_meta` */

/*Table structure for table `user_wise_access_rights` */

DROP TABLE IF EXISTS `user_wise_access_rights`;

CREATE TABLE `user_wise_access_rights` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `modules_id` bigint(20) unsigned NOT NULL,
  `view_permission` bit(1) NOT NULL DEFAULT b'0',
  `add_permission` bit(1) NOT NULL DEFAULT b'0',
  `edit_permission` bit(1) NOT NULL DEFAULT b'0',
  `delete_permission` bit(1) NOT NULL DEFAULT b'0',
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`),
  KEY `roles_id` (`user_id`,`modules_id`),
  CONSTRAINT `user_wise_access_rights_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_wise_access_rights` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned DEFAULT '0',
  `fullname` varchar(100) NOT NULL,
  `username` varchar(40) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `salt` varchar(200) DEFAULT NULL,
  `token` varchar(200) NOT NULL,
  `login_status` bit(1) NOT NULL DEFAULT b'1',
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `role_id` (`role_id`),
  KEY `token` (`token`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`role_id`,`fullname`,`username`,`password`,`salt`,`token`,`login_status`,`created_at`,`updated_at`,`status`) values (1,12,'admin','admin','22343a084872afe7a6c06b8846ba8bf2','3jtMn1LatH','dksuhklsjjfdkfdsfsdfsdfdsjkldshfdklshjfkldjshfkdlsfdsfh232','','','1563365497','');

/*Table structure for table `wallet_account` */

DROP TABLE IF EXISTS `wallet_account`;

CREATE TABLE `wallet_account` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `fun_chips` double NOT NULL,
  `in_play_fun_chips` double NOT NULL,
  `real_chips_deposit` double NOT NULL,
  `in_play_real_chips` double NOT NULL,
  `real_chips_withdrawal` double NOT NULL,
  `on_hold_real_chips_withdrawal` double NOT NULL,
  `total_bonus` double unsigned NOT NULL DEFAULT '0',
  `total_rps_points` double unsigned NOT NULL DEFAULT '0',
  `rps_level_expiry_date` varchar(15) NOT NULL DEFAULT '',
  `_uuid` varchar(255) NOT NULL,
  `wallet_account_name` varchar(255) NOT NULL,
  `players_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `wallet_account_name` (`wallet_account_name`),
  KEY `players_id` (`players_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1201553 DEFAULT CHARSET=latin1;

/*Data for the table `wallet_account` */

insert  into `wallet_account`(`id`,`fun_chips`,`in_play_fun_chips`,`real_chips_deposit`,`in_play_real_chips`,`real_chips_withdrawal`,`on_hold_real_chips_withdrawal`,`total_bonus`,`total_rps_points`,`rps_level_expiry_date`,`_uuid`,`wallet_account_name`,`players_id`) values (1,100,0,0,0,-200,0,0,0,'','tz2Hdo21Fc','Game Wallet',NULL),(2,100,0,0,0,34.5,0,0,0,'','4ZqjDiASIJ','GST Wallet',NULL),(3,100,0,0,0,0,0,0,0,'','aIwfWAphxE','TDS Wallet',NULL),(4,100,0,0,0,3911.485,0,0,0,'','X0VK71pMhG','Admin Commission Wallet',NULL);

/*Table structure for table `wallet_transaction` */

DROP TABLE IF EXISTS `wallet_transaction`;

CREATE TABLE `wallet_transaction` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `game_ref_id` bigint(20) NOT NULL,
  `transaction_id` varchar(15) DEFAULT NULL,
  `transaction_amount` double NOT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `transaction_type` varchar(255) DEFAULT NULL,
  `games_id` bigint(20) unsigned NOT NULL,
  `rooms_id` bigint(20) unsigned NOT NULL,
  `game_type` enum('Cash','Practice') NOT NULL,
  `individual_bid_amount` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `transaction_id` (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `wallet_transaction` */

/*Table structure for table `wallet_transaction_history` */

DROP TABLE IF EXISTS `wallet_transaction_history`;

CREATE TABLE `wallet_transaction_history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ref_id` varchar(50) NOT NULL,
  `transaction_type` enum('Credit','Debit') NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `closing_balance` double NOT NULL DEFAULT '0',
  `created_date_time` datetime NOT NULL,
  `wallet_account_id` bigint(20) unsigned NOT NULL,
  `remark` varchar(250) NOT NULL,
  `type` varchar(100) NOT NULL,
  `game_title` varchar(100) DEFAULT NULL,
  `game_sub_type` varchar(100) DEFAULT NULL,
  `games_id` int(11) unsigned DEFAULT NULL,
  `bot_given_amount_to_admin` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ref_id` (`ref_id`),
  KEY `wallet_account_id` (`wallet_account_id`),
  CONSTRAINT `wallet_transaction_history_ibfk_1` FOREIGN KEY (`wallet_account_id`) REFERENCES `wallet_account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=233 DEFAULT CHARSET=latin1;

/*Data for the table `wallet_transaction_history` */

/*Table structure for table `withdraw_request` */

DROP TABLE IF EXISTS `withdraw_request`;

CREATE TABLE `withdraw_request` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `withdrawal_id` bigint(20) NOT NULL,
  `players_id` bigint(20) unsigned NOT NULL,
  `transfer_type` enum('IMPS','NEFT') NOT NULL,
  `transfer_to` enum('PAYTM','BANK TRANSFER') NOT NULL,
  `paytm_mobile_number` varchar(255) NOT NULL,
  `paytm_person_name` varchar(255) NOT NULL,
  `account_number` varchar(255) NOT NULL,
  `account_holder_name` varchar(100) NOT NULL,
  `ifsc_code` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `branch_name` varchar(255) NOT NULL,
  `request_amount` float NOT NULL,
  `tds_percentage` float NOT NULL,
  `tds_charge` float NOT NULL,
  `service_charge_percentage` float NOT NULL,
  `service_charge` double NOT NULL,
  `net_receivable` double NOT NULL,
  `processing_fee` float NOT NULL,
  `final_payable_amount` float NOT NULL,
  `expertize_level_id_at_withdraw_time` int(10) unsigned NOT NULL,
  `request_status` enum('Pending','Completed','Rejected','Processing','Cancelled') NOT NULL,
  `request_processed_users_id` bigint(20) unsigned DEFAULT NULL,
  `remark` text,
  `request_date_time` datetime DEFAULT NULL,
  `response_date_time` varchar(20) DEFAULT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `beneficiary_id` varchar(100) NOT NULL,
  `pg_ref_id` varchar(100) NOT NULL,
  `transfer_utr_ref_id` varchar(100) NOT NULL,
  `pg_response` text,
  `withdrawal_type` enum('INSTANT','NEFT') DEFAULT NULL,
  `instant_withdrawal_type` enum('PAYTM','NORMAL') DEFAULT NULL,
  PRIMARY KEY (`id`,`status`),
  KEY `players_id` (`players_id`),
  CONSTRAINT `withdraw_request_ibfk_1` FOREIGN KEY (`players_id`) REFERENCES `players` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `withdraw_request` */

/*Table structure for table `withdraw_system_settings` */

DROP TABLE IF EXISTS `withdraw_system_settings`;

CREATE TABLE `withdraw_system_settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `instant_withdraw_min_amount` double NOT NULL,
  `instant_withdraw_max_amount` double NOT NULL,
  `instant_withdrawal_service_charge_on_less_than` double NOT NULL,
  `instant_withdrawal_service_charge_for_less_than_amount` double NOT NULL,
  `instant_withdrawal_service_charge_on_greater_than` double NOT NULL,
  `instant_withdrawal_service_charge_for_greater_than_amount` double NOT NULL,
  `paytm_withdraw_min_amount` double NOT NULL,
  `paytm_withdraw_max_amount` double NOT NULL,
  `paytm_withdrawal_service_charge_on_less_than` double NOT NULL,
  `paytm_withdrawal_service_charge_for_less_than_amount` double NOT NULL,
  `paytm_withdrawal_service_charge_on_greater_than` double NOT NULL,
  `paytm_withdrawal_service_charge_for_greater_than_amount` double NOT NULL,
  `neft_withdraw_min_amount` double NOT NULL,
  `neft_withdraw_max_amount` double NOT NULL,
  `neft_withdrawal_service_charge_on_less_than` double NOT NULL,
  `neft_withdrawal_service_charge_for_less_than_amount` double NOT NULL,
  `neft_withdrawal_service_charge_on_greater_than` double NOT NULL,
  `neft_withdrawal_service_charge_for_greater_than_amount` double NOT NULL,
  `apply_tds_on_greater_than` double NOT NULL,
  `tds_percentage` double NOT NULL,
  `updated_at` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `withdraw_system_settings` */

insert  into `withdraw_system_settings`(`id`,`instant_withdraw_min_amount`,`instant_withdraw_max_amount`,`instant_withdrawal_service_charge_on_less_than`,`instant_withdrawal_service_charge_for_less_than_amount`,`instant_withdrawal_service_charge_on_greater_than`,`instant_withdrawal_service_charge_for_greater_than_amount`,`paytm_withdraw_min_amount`,`paytm_withdraw_max_amount`,`paytm_withdrawal_service_charge_on_less_than`,`paytm_withdrawal_service_charge_for_less_than_amount`,`paytm_withdrawal_service_charge_on_greater_than`,`paytm_withdrawal_service_charge_for_greater_than_amount`,`neft_withdraw_min_amount`,`neft_withdraw_max_amount`,`neft_withdrawal_service_charge_on_less_than`,`neft_withdrawal_service_charge_for_less_than_amount`,`neft_withdrawal_service_charge_on_greater_than`,`neft_withdrawal_service_charge_for_greater_than_amount`,`apply_tds_on_greater_than`,`tds_percentage`,`updated_at`) values (1,200,9999,5,9999,10,9999,1,5000,2.5,5000,2.5,5000,100,100000000,2.5,100,2.5,100,10000,30,'1558334527');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
