<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function send_message($message = "", $mobile_number) {

    $CI = get_instance();
    $CI->db->limit(1);
    $gateway = $CI->db->get("sms_gateway_settings")->row();
    $message = urlencode($message);

    //$URL = "http://sms.smsmoon.com/api/sendhttp.php?authkey=152249AHYLkfl75915b1b6&mobiles=$mobile_number&message=$message&sender=ASTIPS&route=4&country=91";

//    $URL = "http://login.smsmoon.com/API/sms.php"; // connecting url 
//    $post_fields = ['username' => $gateway->sms_gateway_username, 'password' => $gateway->sms_gateway_password, 'from' => $gateway->sender_id, 'to' => $mobile_number, 'msg' => $message, 'type' => 1, 'dnd_check' => 0];
//    //file_get_contents("http://login.smsmoon.com/API/sms.php?username=colourmoonalerts&password=vizag@123&from=WEBSMS&to=$mobile_number&msg=$message&type=1&dnd_check=0");
//    $ch = curl_init();
//    curl_setopt($ch, CURLOPT_URL, $URL);
//    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
//    //curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
//    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//    curl_exec($ch);
    $URL = "https://www.smsgatewayhub.com/api/mt/SendSMS?APIKey=".$gateway->apikey."&senderid=".$gateway->sender_id."&channel=2&DCS=0&flashsms=0&number=$mobile_number&text=$message&route=11";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $URL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_exec($ch);
    return true;
}

function get_balance() {

    $CI = get_instance();
    $CI->db->limit(1);
    $gateway = $CI->db->get("sms_gateway_settings")->row();

    $URL = "http://login.smsmoon.com/API/get_balance.php?username=" . $gateway->sms_gateway_username . "&password=" . $gateway->sms_gateway_password;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $URL);
    //curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
    curl_exec($ch);
}
