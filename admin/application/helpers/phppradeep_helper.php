<?php

//to get the current page url
function curPageURL() {
    $pageURL = 'http';
// if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

if (strpos(curPageURL(), "/api/")) {
    ob_start(function($json) {

        function json_validator($data = NULL) {
            if (!empty($data)) {
                json_decode($data);
                return (json_last_error() === JSON_ERROR_NONE);
            }

            return false;
        }

        if (json_validator($json)) {
            return json_encode(json_decode($json), JSON_PRETTY_PRINT);
        } else {
            return ob_get_contents();
        }
    });
}

function sms_gateway_enabled() {
    $CI = & get_instance();
    return $CI->parent_model->get_result('name,status', 'utilities', ['status' => 'YES']);
}

function email_enabled() {
    $CI = & get_instance();
    return $CI->parent_model->get_result('name,status', 'utilities', ['status' => 'YES']);
}

function get_ads($ad_size_id, $no_of_ads_to_display = 1, $image_class_name = 'img-responsive') {
    $CI = & get_instance();
    switch ($ad_size_id) {
        case 1 : //Means get 250 x 250 square ads
            return $CI->parent_model->get_result('', 'ads', ['display_size' => $ad_size_id]);
            break;
        case 2 : //Means get 200 x 200 small square ads
            return $CI->parent_model->get_result('', 'ads', ['display_size' => $ad_size_id]);
            break;
        case 3 : //Means get 468 x 60 banner
            return $CI->parent_model->get_result('', 'ads', ['display_size' => $ad_size_id]);
            break;
        case 4 : //Means get 728 x 90 Leaderboard
            return $CI->parent_model->get_result('', 'ads', ['display_size' => $ad_size_id]);
            break;
        case 5 : //Means get 300 x 250 inline rectangle
            return $CI->parent_model->get_result('', 'ads', ['display_size' => $ad_size_id]);
            break;
        case 6 : //Means get 336 x 280 large rectangle
            return $CI->parent_model->get_result('', 'ads', ['display_size' => $ad_size_id]);
            break;
        case 7 : //Means get 120 x 600 skyscraper
            return $CI->parent_model->get_result('', 'ads', ['display_size' => $ad_size_id]);
            break;
        case 8 : //Means get 160 x 600 wide skyscraper
            return $CI->parent_model->get_result('', 'ads', ['display_size' => $ad_size_id]);
            break;
        default : break;
    }
    return false;
}

function forward_to_view() {
    return;
}

function how_many_days($start_date, $end_date) {
    //print_r(func_get_args());
    $today = date('d-m-Y', strtotime($start_date));

//$exp = date('d-m-Y',strtotime($end_date)); //query result form database
    $exp = date('d-m-Y', strtotime($end_date)); //query result form database
    //echo $today;
    //echo $exp;

    $expDate = date_create($exp);
    $todayDate = date_create($today);
    $diff = date_diff($todayDate, $expDate);
    if ($diff->format("%R%a") > 0) {
//echo "active";
    } else {
//echo "inactive";
    }
//echo "Remaining Days " . $diff->format("%R%a days");
    return $diff->format("%R%a");
}

function how_many_days_with_out_sign($start_date, $end_date) {
    $today = date('d-m-Y', strtotime($start_date));

//$exp = date('d-m-Y',strtotime($end_date)); //query result form database
    $exp = date('d-m-Y', strtotime($end_date)); //query result form database

    $expDate = date_create($exp);
    $todayDate = date_create($today);
    $diff = date_diff($todayDate, $expDate);
    if ($diff->format("%R%a") > 0) {
//echo "active";
    } else {
//echo "inactive";
    }
//echo "Remaining Days " . $diff->format("%R%a days");
    return $diff->format("%a");
}

function how_many_days_left($end_date) {
    $today = date('d-m-Y', time());

//$exp = date('d-m-Y',strtotime($end_date)); //query result form database
    $exp = date('d-m-Y', $end_date); //query result form database

    $expDate = date_create($exp);
    $todayDate = date_create($today);
    $diff = date_diff($todayDate, $expDate);
    if ($diff->format("%R%a") > 0) {
//echo "active";
    } else {
//echo "inactive";
    }
//echo "Remaining Days ".$diff->format("%R%a days");
    if ($diff->format("%R%a") == 1) {
        return singular($diff->format("%R%a days"));
    }
    return $diff->format("%a days");
}

function how_many_days_left_count($end_date) {
    $today = date('d-m-Y', time());

//$exp = date('d-m-Y',strtotime($end_date)); //query result form database
    $exp = date('d-m-Y', $end_date); //query result form database 

    $expDate = date_create($exp);
    $todayDate = date_create($today);
    $diff = date_diff($todayDate, $expDate);
    if ($diff->format("%R%a") > 0) {
//echo "active";
    } else {
//echo "inactive";
    }
//echo "Remaining Days ".$diff->format("%R%a days");
    return $diff->format("%R%a");
}

function get_mystrtotime($mydate) {
    $dmy = $mydate;
    list($day, $month, $year) = explode("/", $dmy);

    $ymd = "$year-$month-$day";

    return strtotime($ymd);
}

function get_tiny_url($url) {
    $ch = curl_init();
    $timeout = 5;
    curl_setopt($ch, CURLOPT_URL, 'http://tinyurl.com/api-create.php?url=' . $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function time_slots($from, $to, $interval) {
    $arr = array();
    $start = strtotime($from);
    $in = $interval * 60;
//echo date('h:i A',strtotime("+$interval minutes", strtotime($to))); die;
//$end = strtotime($to) - $in;
    $end = strtotime("+$interval minutes", strtotime($to)) - $in;
    $time = strtotime("+0 minutes", $start);
//echo date('h:i A', $time); echo "<br />";
    $arr[] = date('h:i A', $time);

    for ($i = $start; $i < $end; $i = $i + $in) {
        if ($i >= $start && $i <= $end - $in) {
            $time = strtotime("+" . $interval . " minutes", $i);
            $arr[] = date('h:i A', $time);
//echo date('h:i A', $time); echo "<br />";
        }
    }
    return $arr;
}

function left_pad($number) {
    return (($number < 10 && $number >= 0) ? '0' : '') + $number;
}

function convert_minutes_to_string($minutes) {
    $sign = '';
    if ($minutes < 0) {
        $sign = '-';
    }
    $hours = left_pad(floor(abs($minutes) / 60));
    $minutes = left_pad(abs($minutes) % 60);
    return $sign . $hours . 'hrs ' . $minutes . 'min';
}

function convert_minutes_to_hours($minutes) {
    $hours = left_pad(floor(abs($minutes) / 60));
    $minutes = left_pad(abs($minutes) % 60);
    return $hours . "." . $minutes;
}

function get_time_difference($start_time, $end_time) {
    $firstTime = strtotime($start_time);
    $lastTime = strtotime($end_time);
    $timeDiff = $lastTime - $firstTime;
    return ($timeDiff / 60) / 60;
}

function get_letters_from_text($string) {
    $words = explode(" ", $string);
    $acronym = "";

    foreach ($words as $w) {
        $acronym .= $w[0];
    }
    return $acronym;
}

function time_elapsed_string($datetime, $full = false) {
    $today = time();
    $createdday = strtotime($datetime);
    $datediff = abs($today - $createdday);
    $difftext = "";
    $years = floor($datediff / (365 * 60 * 60 * 24));
    $months = floor(($datediff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
    $days = floor(($datediff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
    $hours = floor($datediff / 3600);
    $minutes = floor($datediff / 60);
    $seconds = floor($datediff);
//year checker 
    if ($difftext == "") {
        if ($years > 1)
            $difftext = $years . " years ago";
        elseif ($years == 1)
            $difftext = $years . " year ago";
    }
//month checker 
    if ($difftext == "") {
        if ($months > 1)
            $difftext = $months . " months ago";
        elseif ($months == 1)
            $difftext = $months . " month ago";
    }
//month checker 
    if ($difftext == "") {
        if ($days > 1)
            $difftext = $days . " days ago";
        elseif ($days == 1)
            $difftext = $days . " day ago";
    }
//hour checker 
    if ($difftext == "") {
        if ($hours > 1)
            $difftext = $hours . " hours ago";
        elseif ($hours == 1)
            $difftext = $hours . " hour ago";
    }
//minutes checker 
    if ($difftext == "") {
        if ($minutes > 1)
            $difftext = $minutes . " minutes ago";
        elseif ($minutes == 1)
            $difftext = $minutes . " minute ago";
    }
//seconds checker 
    if ($difftext == "") {
        if ($seconds > 1)
            $difftext = $seconds . " seconds ago";
        elseif ($seconds == 1)
            $difftext = $seconds . " second ago";
    }
    return $difftext;
}

function convert_currency($from_currency_code, $to_currency_code, $amount_to_convert) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://www.google.com/finance/converter?hl=en&a=$amount_to_convert&from=$from_currency_code&to=$to_currency_code");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $data = curl_exec($ch);
    $array = explode("<span class=bld>", $data);
    $get = explode("</span>", $array[1]);
    $converted_amount = preg_replace("/[^0-9\.]/", null, $get[0]);
    return $converted_amount;
}

function get_logged_person_currency() {
    $ci = & get_instance();
    $user_business_meta_id = get_logged_in_user_business_meta_id();
    $currency_info = $ci->user_model->get_currency_code_by_user_business_meta_id($user_business_meta_id);
    return $currency_info;
}

function roundTime($timestamp, $precision = 15) {
    $timestamp = strtotime($timestamp);
    $precision = 60 * $precision;
    return date('d-m-Y h:i A', round($timestamp / $precision) * $precision);
}

function is_url_exist($url) {

    $options = array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HEADER => true,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_ENCODING => "",
        CURLOPT_AUTOREFERER => true,
        CURLOPT_CONNECTTIMEOUT => 120,
        CURLOPT_TIMEOUT => 120,
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_NOBODY => true,
        CURLOPT_SSL_VERIFYPEER => false
    );

    $ch = curl_init();
    curl_setopt_array($ch, $options);
    $response = curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    /*
      if ($httpCode != 200) {
      echo "Return code is {$httpCode} \n"
      . curl_error($ch);
      } else {
      echo "<pre>" . htmlspecialchars($response) . "</pre>";
      }
     */
    curl_close($ch);

    if ($code == 200) {
        $status = true;
    } else {
        $status = false;
    }
    return $status;
}

function generateFancyPassword() {
    $rotations = rand(1, 3);
    if ($rotations == 1) {
        $password = rand(112502, 999999);
    } else if ($rotations == 2) {
        $password = rand(1, 9) * 11;
        $password .= rand(1, 9) * 11;
        $password .= rand(1, 9) * 11;
    } else if ($rotations == 3) {
        $password = rand(1, 9) * 111;
        $password .= rand(1, 9) * 111;
    }
    return $password;
}

function time_spent($start, $end) {

    $start = date_create($start); //Y-m-d H:i:s
    $end = date_create($end); // Current time and date
    $diff = date_diff($start, $end);

    $elapsed_time = "";

    if ($diff->y) {
        $elapsed_time .= $diff->y . ' years, ';
    }
    if ($diff->m) {
        $elapsed_time .= $diff->m . ' months, ';
    }
    if ($diff->d) {
        $elapsed_time .= $diff->d . ' days, ';
    }

    if ($diff->h) {
        $elapsed_time .= $diff->h . ' hours, ';
    }

    if ($diff->i) {
        if ($diff->i > 1) {
            $elapsed_time .= $diff->i . ' minutes, ';
        } else {
            $elapsed_time .= $diff->i . ' minute, ';
        }
    }

    if ($diff->s) {
        $elapsed_time .= $diff->s . ' seconds ';
    }
    return $elapsed_time;
}
