<?php

function permission_required($modules_id, $permission_type, $user_id = null, $stop_process = true) {
    if ($user_id == null) {
        $user_id = get_user_id();
    }
    $ci = & get_instance();
    if (!isset($user_id)) {
        if ($ci->input->is_ajax_request()) {
            return display_access_denied();
        }
        return access_denied();
    }
    $role_name = get_user_role_name($user_id);
    if ($role_name == "Developer") {
        return true;
    } else if ($role_name == "Tester") {
        return true;
    }

    if ($permission_type == "view") {
        $column_name = "view_permission";
    } else if ($permission_type == "add") {
        $column_name = "add_permission";
    } else if ($permission_type == "edit" || $permission_type == "update") {
        $column_name = "edit_permission";
    } else if ($permission_type == "delete") {
        $column_name = "delete_permission";
    }
    $ci->db->select($column_name);
    $ci->db->where("modules_id", $modules_id);
    $ci->db->where("user_id", $user_id);
    $ci->db->limit(1);
    $ci->db->where("status", 1);
    $result = $ci->db->get("user_wise_access_rights");
    if ($result->num_rows() == 0) {
        if ($stop_process == true) {
            return false;
        }
        if ($ci->input->is_ajax_request()) {
            return display_access_denied();
        }
        return access_denied();
        //return FALSE;
    } else if ($result->row()->{$column_name} == 1) {
        return true;
    } else {
        if ($stop_process == true) {
            return false;
        }
        if ($ci->input->is_ajax_request()) {
            return display_access_denied();
        }
        return access_denied();
        //return false;
    }
}

function display_access_denied() {
    http_response_code(401);
    $arr = [
        'err_code' => "invalid",
        'title' => "Access Denied",
        "message" => "Sorry, you dont have access please contact administrator",
        "data" => []
    ];
    echo json_encode($arr);
    die;
}

function access_denied() {
    redirect("welcome");
    //echo "Sorry, you dont have access";
    die;
}

function has_permission($modules_id, $permission_type, $user_id = null) {
    if ($user_id == null) {
        $user_id = get_user_id();
    }
    $role_name = get_user_role_name($user_id);
    if ($role_name == "Developer") {
        return true;
    } else if ($role_name == "Tester") {
        return true;
    }
    $ci = & get_instance();
    if ($permission_type == "view") {
        $column_name = "view_permission";
    } else if ($permission_type == "add") {
        $column_name = "add_permission";
    } else if ($permission_type == "edit") {
        $column_name = "edit_permission";
    } else if ($permission_type == "delete") {
        $column_name = "delete_permission";
    }
    $ci->db->select($column_name);
    $ci->db->where("modules_id", $modules_id);
    $ci->db->where("user_id", $user_id);
    $ci->db->limit(1);
    $ci->db->where("status", 1);
    $result = $ci->db->get("user_wise_access_rights");

    if ($result->num_rows() == 0) {
        return false;
    } else if ($result->row()->{$column_name} == 1) {
        return true;
    } else {
        return false;
    }
}

function get_access_enabled_modules_list($user_id = NULL) {
    if ($user_id == null) {
        $user_id = get_user_id();
    }
    $ci = & get_instance();
    $role_name = get_user_role_name($user_id);

    if ($role_name == "Developer" || $role_name == "Tester") {
        $ci->db->select("id as modules_id");
        $ci->db->order_by("id", "asc");
        $ci->db->where("status", 1);
        $result = $ci->db->get("modules")->result();
    } else {
        $ci->db->select("modules_id");
        $ci->db->where("status", 1);
        $ci->db->where("user_id", $user_id);
        $ci->db->where("view_permission", 1);
        $ci->db->order_by("modules_id", "asc");
        $result = $ci->db->get("user_wise_access_rights")->result();
    }

    $ids = [];
    foreach ($result as $item) {
        $ids[] = $item->modules_id;
    }
    return json_encode($ids, JSON_NUMERIC_CHECK);
}

function get_user_role_name($user_id) {
    $ci = & get_instance();

    $ci->db->select("role_id");
    $ci->db->where("id", $user_id);
    $role_id = $ci->db->get("users")->row()->role_id;

    $ci->db->select("role_name");
    $ci->db->where("status", 1);
    $ci->db->where("id", $role_id);
    $data = $ci->db->get("roles");
    return $data->row()->role_name;
}

function check_for_access($module_id, $action_type = null) {
    $ci = & get_instance();
    $segments = $ci->uri->segment_array();
    if ($action_type == null) {
        if (in_array("add", $segments)) {
            if ($ci->input->get_post("id")) {
                return permission_required($module_id, "edit");
            } else {
                return permission_required($module_id, "add");
            }
        } else if (in_array("delete", $segments)) {
            return permission_required($module_id, "delete");
        } else {
            return permission_required($module_id, "view");
        }
    } else {
        return permission_required($module_id, $action_type);
    }
}
