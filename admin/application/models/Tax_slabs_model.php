<?php

class Tax_slabs_model extends CI_Model {

    private $table_name = "tax_slabs";

    function add($data) {
        $this->db->set($data);
        $this->db->set("created_at", time());
        $response = $this->db->insert($this->table_name);
        if ($response) {
            $inserted_id = $this->db->insert_id();
            $this->log_model->create_log($this->table_name, __FUNCTION__, $inserted_id);
        }
        return $response;
    }

    function update($data, $id) {
        $this->db->set($data);
        $this->db->where("id", $id);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        if ($response) {
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
        }
        return $response;
    }

    function delete($id) {
        $this->db->where("id", $id);
        $this->db->set("status", 0);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        if ($response) {
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
        }
        return $response;
    }

    function get() {
        $this->db->select($this->table_name . ".*");
        $this->db->join("centers", "centers.id=" . $this->table_name . ".centers_id");
        $this->db->from($this->table_name);
        $this->db->where($this->table_name . ".status", 1);
        $this->db->where("centers.status", 1);
        $data = $this->db->get()->result();
        if ($data) {
            foreach ($data as $item) {

                $item->start_date = convert_date_to_display_format($item->start_date);
                $item->center_name = $this->centers_model->get_center_name($item->centers_id);

                if ($item->tax_type == 'Percentage') {
                    $item->tax_amount2 = $item->tax_amount . "%";
                } else {
                    $item->tax_amount2 = $item->tax_amount;
                }
                if ($item->apply_until_changed == 'Yes') {
                    $item->end_date2 = "Apply Until Changed";
                } else {
                    $item->end_date2 = convert_date_to_display_format($item->end_date);
                    $item->end_date = convert_date_to_display_format($item->end_date);
                }
            }
            return $data;
        }
        return [];
    }

    /* function is_mobile_exists($mobile, $id) {
      $this->db->where("status", 1);
      if ($id) {
      $this->db->where("(id!=$id)");
      }
      $this->db->where("mobile", $mobile);
      return $this->db->get($this->table_name)->num_rows();
      } */

    function get_by_centers_id($data) {
        $this->db->where('centers_id', $data->centers_id);
        $this->db->where('start_date <=', $data->check_in_date);
        $this->db->where('(end_date >= "' . $data->check_in_date . '" OR apply_until_changed = "Yes")');
        $this->db->where('tax_status', 'Active');
        $this->db->where('status', 1);
        $result = $this->db->get('tax_slabs');
        if ($result->num_rows()) {
            $data = $result->result();
            foreach ($data as $item) {
                if (!$item->end_date) {
                    $item->end_date = "";
                }
            }
            return $data;
        } else {
            return [];
        }
    }

}
