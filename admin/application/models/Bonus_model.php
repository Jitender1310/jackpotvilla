<?php

class Bonus_model extends CI_Model {

    private $table_name = "bonus_coupons";

    function add($data) {
        $this->db->trans_begin();
        
        $this->db->set($data);
        $this->db->set("created_by_user_id", $this->session->userdata("user_id"));
        $this->db->set("created_at", time());
        $response = $this->db->insert($this->table_name);
        if ($response) {
            $inserted_id = $this->db->insert_id();
            $this->log_model->create_log($this->table_name, __FUNCTION__, $inserted_id);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function update($data, $id) {
        $this->db->trans_begin();
        $this->db->set($data);
        $this->db->set("updated_by_user_id", $this->session->userdata("user_id"));
        $this->db->where("id", $id);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        if ($response) {
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);                

        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function delete($id) {
        $this->db->where("id", $id);
        $this->db->set("status", 0);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        if ($response) {
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
        }
        return $response;
    }

    function get() {
        $this->db->order_by("id", "desc");
        $data = $this->db->get($this->table_name)->result();
        if ($data) {
            foreach ($data as $item) {
                $item->created_at = date(DATE_TIME_HUMAN_READABLE_FORMAT, $item->created_at);
                if($item->updated_at){
                    $item->updated_at = date(DATE_TIME_HUMAN_READABLE_FORMAT, $item->updated_at);
                }else{
                    $item->updated_at = "N/a";
                }
                
                $item->from_date_time = convert_date_time_to_display_format($item->from_date_time);
                $item->to_date_time = convert_date_time_to_display_format($item->to_date_time);
                
                if (!$item->updated_at) {
                    $item->updated_at = "";
                }
                
                $item->created_by_user_name = "";
                if($item->created_by_user_id){
                    $item->created_by_user_name = $this->user_model->get_user_details($item->created_by_user_id)->fullname;
                }
                
                if($item->updated_by_user_id){
                    $item->updated_by_user_name = $this->user_model->get_user_details($item->updated_by_user_id)->fullname;
                }
            }
            return $data;
        } else {
            return [];
        }
    }

    function is_username_exists($username, $id) {
        $this->db->where("status", 1);
        if ($id) {
            $this->db->where("(id!=$id)");
        }
        $this->db->where("username", $username);
        return $this->db->get($this->table_name)->num_rows();
    }

    function is_username_exists_for_adding($username, $id) {
        if ($id) {
            $this->db->where("(id!=$id)");
        }
        $this->db->where("username", $username);
        return $this->db->get($this->table_name)->num_rows();
    }

    function is_mobile_exists_for_adding($mobile, $id) {
        if ($id) {
            $this->db->where("(id!=$id)");
        }
        $this->db->where("mobile", $mobile);
        return $this->db->get($this->table_name)->num_rows();
    }

    function is_email_exists_for_adding($email, $id) {
        if ($id) {
            $this->db->where("(id!=$id)");
        }
        $this->db->where("email", $email);
        return $this->db->get($this->table_name)->num_rows();
    }

    function generate_token($length = 20) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $token = '';
        for ($i = 0; $i < $length; $i++) {
            $token .= $characters[rand(0, $charactersLength - 1)];
        }
        if ($this->is_token_exists($token)) {
            $this->generate_token($length);
        } else {
            return $token;
        }
    }

    function generate_my_referral_code() {
        $my_referral_code = generateRandomString(6);
        if ($this->db->get_where($this->table_name, ["my_referral_code" => $my_referral_code])->num_rows() == 0) {
            return $my_referral_code;
        } else {
            return $this->generate_my_referral_code();
        }
    }

    function generate_access_token() {
        $access_token = generateRandomString(32);
        if ($this->db->get_where($this->table_name, ["access_token" => $access_token])->num_rows() == 0) {
            return $access_token;
        } else {
            return $this->generate_access_token();
        }
    }

    function is_token_exists($token) {
        $this->db->where("access_token", $token);
        return $this->db->get($this->table_name)->num_rows();
    }

    function generate_game_token() {
        $game_token = generateRandomString(30);
        if ($this->db->get_where($this->table_name, ["token" => $game_token])->num_rows() == 0) {
            return $game_token;
        } else {
            return $this->generate_game_token();
        }
    }

    function generate_uuid_token() {
        $_uuid = generateRandomString(30);
        if ($this->db->get_where($this->table_name, ["_uuid" => $_uuid])->num_rows() == 0) {
            return $_uuid;
        } else {
            return $this->generate_uuid_token();
        }
    }
    
    
    function get_released_bonus($wallet_account_id) {
        $this->db->select("sum(utilized_amount) as released_bonus");
        $this->db->where("is_expired", 0);
        $this->db->where("wallet_account_id", $wallet_account_id);
        $rb = $this->db->get("bonus_transaction_history")->row()->released_bonus;
        if ($rb) {
            return $rb;
        }
        return 0;
    }

    function get_active_bonus($wallet_account_id) {
        $this->db->select("sum(amount) as active_bonus");
        $this->db->where("is_expired", 0);
        $this->db->where("wallet_account_id", $wallet_account_id);
        $rb = $this->db->get("bonus_transaction_history")->row()->active_bonus;
        if ($rb) {
            return $rb;
        }
        return 0;
    }

    function get_pending_bonus($wallet_account_id) {
        $pending_bonus = $this->get_active_bonus($wallet_account_id) - $this->get_released_bonus($wallet_account_id);
        return $pending_bonus;
    }

}
