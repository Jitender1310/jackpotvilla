<?php

    class Players_model extends CI_Model {

        private $table_name = "players";

        function __construct() {
            parent::__construct();
        }

        function add($data, $excel_upload = false) {
            $this->db->trans_begin();
            $data["my_referral_code"] = $this->generate_my_referral_code();
            $data["access_token"] = $this->generate_access_token();
            $data["wallet_account_id"] = $this->wallet_model->create_wallet("Player");
            $data["email_verification_key"] = $this->generate_email_verification_key();

            $this->db->set($data);
            $this->db->set("created_at", time());
            $response = $this->db->insert($this->table_name);
            if ($response) {

                if ($data['bot_player'] == 1) {
                    if($excel_upload==false){
                        ping_to_sfs_server(SFS_COMMAND_STATIC_BOT_ADDED . $data["access_token"]);
                    }
                } else {
                    $this->add_signup_bonus_amount($data["wallet_account_id"]);
                }

                if($excel_upload==false){
                    $inserted_id = $this->db->insert_id();
                    $this->log_model->create_log($this->table_name, __FUNCTION__, $inserted_id);
                }
            }

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        }

        function update($data, $id) {
            $this->db->trans_begin();

            if ($data["bot_status"] == 0) {
                $response = ping_to_sfs_server(SFS_COMMAND_STATIC_BOT_INACTIVE . $id);
                if ($response != 200) {
                    return "GAME_IN_PLAY";
                } else {
                    $this->db->set($data);
                    $this->db->where("id", $id);
                    $this->db->set("updated_at", time());
                    $response = $this->db->update($this->table_name);
                    if ($response) {
                        $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
                    }
                }
            } else {
                $this->db->set($data);
                $this->db->where("id", $id);
                $this->db->set("updated_at", time());
                $response = $this->db->update($this->table_name);
                if ($response) {
                    $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
                    if ($data["bot_status"] == 1) {
                        ping_to_sfs_server(SFS_COMMAND_STATIC_BOT_ACTIVE . $id);
                    }
                }
            }

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        }

        function delete($id) {
            $this->db->where("id", $id);
            $this->db->set("status", 0);
            $this->db->set("updated_at", time());
            $response = $this->db->update($this->table_name);
            if ($response) {
                $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
            }
            return $response;
        }

        function get_user_current_password($user_id) {
            $this->db->select("id, password, salt");
            $row = $this->db->get_where($this->table_name, ['id' => $user_id])->row();
            return $row;
        }

        function get($filters = []) {
            $only_cnt = false;
            $search_key = @$filters['search_key'];
            $kyc_status = @$filters["kyc_status"];

            if ($filters['search_key']) {
                $this->db->where("(mobile='" . $search_key . "' OR username='" . $search_key . "' "
                        . "OR email='" . $search_key . "' )");
            }

            $this->db->order_by("id", "desc");

            $this->db->select("id, username, email, mobile, gender, my_referral_code");
            $this->db->select("firstname, lastname, date_of_birth");
            $this->db->select("address_line_1, address_line_2, pin_code, states_id, city");
            $this->db->select("email_verified, mobile_verified");
            $this->db->select("address_proof_type, address_proof");
            $this->db->select("pan_card_number, pan_card");
            $this->db->select("address_proof_verified_at, pan_card_verified_at");
            $this->db->select("address_proof_rejected_at, pan_card_rejected_at");
            $this->db->select("address_proof_rejected_reason, pan_card_rejected_reason");
            $this->db->select("address_proof_status, pan_card_status, created_at, wallet_account_id");
            $this->db->select("bot_can_play");

            if (isset($filters['is_bot_players_required']) && !empty($filters['is_bot_players_required'])) {
                $this->db->select("bot_status");
                $this->db->where("bot_player", (int) $filters['is_bot_players_required']);
            } else {
                $this->db->where("bot_player", 0);
            }

            if ($kyc_status == "Pending Approval") {
                $this->db->where("(address_proof_status = '$kyc_status' OR pan_card_status='$kyc_status')");
            } else if ($kyc_status == "Approved") {
                $this->db->where("address_proof_status", $kyc_status);
                $this->db->where("pan_card_status", $kyc_status);
            } else if ($kyc_status == "Rejected") {
                $this->db->where("(address_proof_status = '$kyc_status' OR pan_card_status='$kyc_status')");
            } else if ($kyc_status == "Not Submitted") {
                $this->db->where("(address_proof_status = '$kyc_status' OR pan_card_status='$kyc_status')");
            }

            $this->db->where("status", 1);


            if (isset($filters["bot_status"])) {
                $this->db->where("bot_status", $filters["bot_status"]);
            }

            if (!isset($filters["start"]) && !isset($filters["limit"])) {
                $only_cnt = true;
            }
            if ($filters["from_date"]) {
                $this->db->where($this->table_name . ".created_at > ", $filters["from_date"]);
            }
            if ($filters["bot_can_play"]) {
                $this->db->where($this->table_name . ".bot_can_play", $filters["bot_can_play"]);
            }
            if ($filters["to_date"]) {
                $this->db->where($this->table_name . ".created_at < ", $filters["to_date"]);
            }

            if ($only_cnt == true) {
                $total_records_found = $this->db->count_all_results($this->table_name);
                return $total_records_found;
            } else {
                /* if ($filters['search_key']) {
                  $this->db->where("booking_ref_number", $filters['search_key']);
                  } */
                if ($filters["sort_by"] == "Latest") {
                    $this->db->order_by("id", "desc");
                } else if ($filters["sort_by"] == "Oldest") {
                    $this->db->order_by("id", "asc");
                } else {
                    $this->db->order_by("id", "desc");
                }
                $this->db->limit($filters["limit"], $filters["start"]);
                $result = $this->db->get($this->table_name);
            }

            if ($result->num_rows()) {
                $result = $result->result();
                foreach ($result as $item) {
                    $item->fullname = $item->firstname . " " . $item->lastname;
                    $item->date_of_birth = convert_date_to_display_format($item->date_of_birth);
                    $item->registered_at = date(DATE_TIME_HUMAN_READABLE_FORMAT, $item->created_at);
                    unset($item->created_at);
                    $item->kyc_status = "Not Verified";
                    $item->kyc_status_bootstrap_css = "text-danger";
                    if ($item->pan_card_status == "Approved" && $item->address_proof_status == "Approved") {
                        $item->kyc_status = "Approved";
                        $item->kyc_status_bootstrap_css = "text-success";
                    } else if ($item->pan_card_status == "Rejected" || $item->address_proof_status == "Rejected") {
                        $item->kyc_status = "Rejected";
                        $item->kyc_status_bootstrap_css = "text-danger";
                    } else if ($item->pan_card_status == "Pending Approval" || $item->address_proof_status == "Pending Approval") {
                        $item->kyc_status = "Pending Approval";
                        $item->kyc_status_bootstrap_css = "text-warning";
                    } else if ($item->pan_card_status == "Not Submitted" || $item->address_proof_status == "Not Submitted") {
                        $item->kyc_status = "Not Submitted";
                        $item->kyc_status_bootstrap_css = "text-danger";
                    }

                    if ($item->pan_card) {
                        if (is_url_exist(KYC_ATTACHMENTS_UPLOAD_FOLDER_PATH . $item->pan_card)) {
                            $item->pan_card = KYC_ATTACHMENTS_UPLOAD_FOLDER_PATH . $item->pan_card;
                        } else {
                            $item->pan_card = KYC_PLAYER_ATTACHMENTS_UPLOAD_FOLDER_PATH . $item->pan_card;
                        }
                    } else {
                        $item->pan_card = "";
                    }

                    if ($item->address_proof) {
                        if (is_url_exist(KYC_ATTACHMENTS_UPLOAD_FOLDER_PATH . $item->address_proof)) {
                            $item->address_proof = KYC_ATTACHMENTS_UPLOAD_FOLDER_PATH . $item->address_proof;
                        } else {
                            $item->address_proof = KYC_PLAYER_ATTACHMENTS_UPLOAD_FOLDER_PATH . $item->address_proof;
                        }
                    } else {
                        $item->address_proof = "";
                    }
                    if ($item->pan_card_status == "Pending Approval") {
                        $item->pan_card_status_bootstrap_css = "text-warning";
                    } else if ($item->pan_card_status == "Not Submitted") {
                        $item->pan_card_status_bootstrap_css = "text-danger";
                    } else if ($item->pan_card_status == "Approved") {
                        $item->pan_card_status_bootstrap_css = "text-success";
                    } else {
                        $item->pan_card_status_bootstrap_css = "text-danger";
                    }
                    if ($item->address_proof_status == "Pending Approval") {
                        $item->address_proof_status_bootstrap_css = "text-warning";
                    } else if ($item->address_proof_status == "Not Submitted") {
                        $item->address_proof_status_bootstrap_css = "text-danger";
                    } else if ($item->address_proof_status == "Approved") {
                        $item->address_proof_status_bootstrap_css = "text-success";
                    } else {
                        $item->address_proof_status_bootstrap_css = "text-danger";
                    }

                    $item->state_name = $this->states_model->get_state_name($item->states_id);

                    if (isset($item->bot_status)) {
                        if ($item->bot_status == "Active") {
                            $item->bot_status_bootstrap_css = "text-success";
                        } else if ($item->bot_status == "Inactive") {
                            $item->bot_status_bootstrap_css = "text-danger";
                        }
                    }

                    $item->wallet_info = $this->wallet_model->get_wallet_account($item->wallet_account_id);
                    $item->kyc_log = $this->player_kyc_model->get_kyc_log($item->id);
                }
                return $result;
            } else {
                return [];
            }
        }

        function is_username_exists($username, $id) {
            $this->db->where("status", 1);
            if ($id) {
                $this->db->where("(id!=$id)");
            }
            $this->db->where("username", $username);
            return $this->db->get($this->table_name)->num_rows();
        }

        function is_username_exists_for_adding($username, $id) {
            if ($id) {
                $this->db->where("(id!=$id)");
            }
            $this->db->where("username", $username);
            return $this->db->get($this->table_name)->num_rows();
        }

        function is_mobile_exists_for_adding($mobile, $id) {
            if ($id) {
                $this->db->where("(id!=$id)");
            }
            $this->db->where("mobile", $mobile);
            return $this->db->get($this->table_name)->num_rows();
        }

        function is_email_exists_for_adding($email, $id) {
            if ($id) {
                $this->db->where("(id!=$id)");
            }
            $this->db->where("email", $email);
            return $this->db->get($this->table_name)->num_rows();
        }

        function generate_token($length = 20) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $token = '';
            for ($i = 0; $i < $length; $i++) {
                $token .= $characters[rand(0, $charactersLength - 1)];
            }
            if ($this->is_token_exists($token)) {
                $this->generate_token($length);
            } else {
                return $token;
            }
        }

        function generate_my_referral_code() {
            $my_referral_code = generateRandomString(6);
            if ($this->db->get_where($this->table_name, ["my_referral_code" => $my_referral_code])->num_rows() == 0) {
                return $my_referral_code;
            } else {
                return $this->generate_my_referral_code();
            }
        }

        function generate_access_token() {
            $access_token = generateRandomString(32);
            if ($this->db->get_where($this->table_name, ["access_token" => $access_token])->num_rows() == 0) {
                return $access_token;
            } else {
                return $this->generate_access_token();
            }
        }

        function is_token_exists($token) {
            $this->db->where("access_token", $token);
            return $this->db->get($this->table_name)->num_rows();
        }

        function generate_email_verification_key() {
            $email_verification_key = generateRandomString(30);
            if ($this->db->get_where($this->table_name, ["email_verification_key" => $email_verification_key])->num_rows() == 0) {
                return $email_verification_key;
            } else {
                return $this->generate_email_verification_key();
            }
        }

        function update_kyc($data, $player_id) {

            $this->db->where("id", $player_id);
            $player_details = $this->db->get($this->table_name);

            if ($player_details->pan_card_status == "Approved" &&
                    $player_details->address_proof_status == "Approved") {
                return false;
            }

            if ($player_details->pan_card_status != "Approved" && isset($data["pan_card_number"]) && isset($data["pan_card"])) {
                $this->db->set("pan_card", $data["pan_card"]);
                $this->db->set("pan_card_number", $data["pan_card_number"]);
                $this->db->set("pan_card_status", $data["pan_card_status"]);
            }

            if ($player_details->address_proof_status != "Approved" && isset($data["address_proof"])) {
                $this->db->set("address_proof", $data["address_proof"]);
                $this->db->set("address_proof_status", $data["address_proof_status"]);
            }

            $this->db->where("id", $player_id);
            $this->db->set("updated_at", time());
            $this->db->update($this->table);
            return true;
        }

        function get_player_by_mobile($mobile) {
            $result = $this->db->get_where($this->table_name, array('mobile' => $mobile));

            if ($result->num_rows() > 0) {
                $row = $result->row_array();
                $result->free_result();
                return $row;
            }

            return false;
        }

        function get_player_by_id($id) {
            $result = $this->db->get_where($this->table_name, array('id' => $id));
            if ($result->num_rows() > 0) {
                $row = $result->row();
                return $row;
            }
            return false;
        }

        function add_signup_bonus_amount($wallet_account_id) {
            if (SIGN_UP_CASH_AMOUNT > 0) {
                $transaction_data = [
                    "transaction_type" => "Credit",
                    "amount" => SIGN_UP_CASH_AMOUNT,
                    "wallet_account_id" => $wallet_account_id,
                    "remark" => "Welcome Cash Bonus added to wallet",
                    "type" => "Sign Up Bonus Cash",
                ];
                $this->wallet_transactions_model->create_transaction($transaction_data);
            }
        }

        function get_player_detail_by_id($player_id)
        {
            $this->db->select('players.*, countries.currency, countries.name AS country_name, wallet_account.real_chips_deposit, wallet_account.loyalty_points');
            $this->db->from('players');
            $this->db->join('countries', 'countries.id = players.country_id');
            $this->db->join('wallet_account', 'wallet_account.id = players.wallet_account_id');
            $this->db->where('players.id', $player_id);
            return $this->db->get()->result_array();
        }

        function get_all_players()
        {
            $this->db->select('players.*, countries.currency, wallet_account.real_chips_deposit, FROM_UNIXTIME(players.created_at, "%Y-%m-%d") AS creation_date, countries.name AS country_name, wallet_account.loyalty_points');
            $this->db->from('players');
            $this->db->join('countries', 'countries.id = players.country_id');
            $this->db->join('wallet_account', 'wallet_account.id = players.wallet_account_id');
            return $this->db->get()->result();
        }

        function get_transactions_by_player($player_id){
            $this->db->select('wallet_transaction_history.*, casino_games.display_name AS game_name');
            $this->db->from('wallet_transaction_history');
            $this->db->join('players', 'players.wallet_account_id = wallet_transaction_history.wallet_account_id');
            $this->db->join('casino_games', 'casino_games.id = wallet_transaction_history.games_id', 'left');
            $this->db->where('players.id', $player_id);
            $this->db->order_by('wallet_transaction_history.id', 'desc');
            return $this->db->get()->result_array();
        }

        function get_deposits_by_player($player_id){
            $this->db->from('wallet_transaction_history');
            $this->db->join('players', 'players.wallet_account_id = wallet_transaction_history.wallet_account_id');
            $this->db->where('players.id', $player_id);
            $this->db->where('wallet_transaction_history.transaction_type', 'Credit');
            $this->db->or_where('wallet_transaction_history.transaction_type', 'Credit Bonus');
            $this->db->order_by('wallet_transaction_history.id', 'desc');
            return $this->db->get()->result_array();
        }

        function get_withdrawals_by_player($player_id){
            $this->db->from('wallet_transaction_history');
            $this->db->join('players', 'players.wallet_account_id = wallet_transaction_history.wallet_account_id');
            $this->db->where('players.id', $player_id);
            $this->db->where('wallet_transaction_history.transaction_type', 'Debit');
            $this->db->order_by('wallet_transaction_history.id', 'desc');
            return $this->db->get()->result_array();
        }

        function get_all_transactions(){
            $this->db->select('wallet_transaction_history.*, casino_games.display_name AS game_name, players.username');
            $this->db->from('wallet_transaction_history');
            $this->db->join('players', 'players.wallet_account_id = wallet_transaction_history.wallet_account_id');
            $this->db->join('casino_games', 'casino_games.id = wallet_transaction_history.games_id', 'left');
            $this->db->order_by('wallet_transaction_history.id', 'desc');
            return $this->db->get()->result_array();
        }

        function get_all_deposits(){
            $this->db->select('wallet_transaction_history.*, players.username');
            $this->db->from('wallet_transaction_history');
            $this->db->join('players', 'players.wallet_account_id = wallet_transaction_history.wallet_account_id');
            $this->db->where('wallet_transaction_history.transaction_type', 'Credit');
            $this->db->or_where('wallet_transaction_history.transaction_type', 'Credit Bonus');
            $this->db->order_by('wallet_transaction_history.id', 'desc');
            return $this->db->get()->result_array();
        }

        function get_all_withdrawals(){
            $this->db->select('wallet_transaction_history.*, players.username');
            $this->db->from('wallet_transaction_history');
            $this->db->join('players', 'players.wallet_account_id = wallet_transaction_history.wallet_account_id');
            $this->db->where('wallet_transaction_history.transaction_type', 'Debit');
            $this->db->order_by('wallet_transaction_history.id', 'desc');
            return $this->db->get()->result_array();
        }

        function get_all_withdrawals_requests(){
            $this->db->select('withdraw_request.*, players.username, players.mobile, players.email, players.wallet_account_id');
            $this->db->from('withdraw_request');
            $this->db->join('players', 'players.id = withdraw_request.players_id');
            $this->db->order_by('withdraw_request.id', 'desc');
            return $this->db->get()->result_array();
        }

        function update_withdrawals_requests($request_id, $request_data){
            $this->db->where('id', $request_id);
            $this->db->update('withdraw_request', $request_data);
        }

        function get_withdraw_request_by_id($request_id){
            $this->db->from('withdraw_request');
            $this->db->where('id', $request_id);
            return $this->db->get()->row();
        }

        function update_wallet_account($account_id, $update_data){
            $this->db->where('id', $account_id);
            $this->db->update('wallet_account', $update_data);
        }

        function generateRandomNumber($length = 10) {
            $characters = '0123456789';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        function generate_ref_id(){
            $ref_id = $this->generateRandomNumber(10);
            if ($this->db->get_where('wallet_transaction_history', ["ref_id" => $ref_id])->num_rows() == 0) {
                return $ref_id;
            } else {
                return $this->generate_ref_id();
            }
        }

        function add_wallet_transaction_history($history_data){
            $history_data['ref_id'] = $this->generate_ref_id();
            $this->db->insert('wallet_transaction_history', $history_data);
            return $this->db->insert_id();
        }

        function get_total_deposits(){
            $this->db->select('SUM(amount) AS total_deposits, wallet_account_id');
            $this->db->from('wallet_transaction_history');
            $this->db->where('transaction_type', 'Credit');
            $this->db->group_by('wallet_account_id');
            return $this->db->get()->result_array();
        }

        function get_transactions_by_date_range($start_date, $end_date){
            $where_condition = "wallet_transaction_history.created_date_time BETWEEN '" . $start_date . "' AND '" . $end_date . "'";
            $this->db->select('wallet_transaction_history.*, casino_games.display_name AS game_name, players.username');
            $this->db->from('wallet_transaction_history');
            $this->db->join('players', 'players.wallet_account_id = wallet_transaction_history.wallet_account_id');
            $this->db->join('casino_games', 'casino_games.id = wallet_transaction_history.games_id', 'left');
            $this->db->where($where_condition);
            $this->db->order_by('wallet_transaction_history.id', 'desc');
            return $this->db->get()->result_array();
        }

    }
