<?php

class corporate_offices_model extends CI_Model {

    private $table_name = "corporate_offices";

    function add($data) {
        $this->db->trans_begin();
        $this->db->set($data);
        $this->db->set("created_at", time());
        $response = $this->db->insert($this->table_name);
        if ($response) {
            $inserted_id = $this->db->insert_id();
            $this->log_model->create_log($this->table_name, __FUNCTION__, $inserted_id);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function get_corporate_office_name($corporate_offices_id) {
        echo $corporate_offices_id;
        die;
        $this->db->select("corporate_office_name");
        $this->db->where("id", $corporate_offices_id);
        return $this->db->get($this->table_name)->row()->corporate_office_name;
    }

    function update($data, $id) {
        $this->db->trans_begin();
        $this->db->set($data);
        $this->db->where("id", $id);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        if ($response) {
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function get() {
        $this->db->where("status", 1);
        $this->db->order_by("id", 'asc');
        $data = $this->db->get($this->table_name)->result();
        if ($data) {
            return $data;
        }
        return [];
    }

    function delete($id) {
        $this->db->where("id", $id);
        $this->db->set("status", 0);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        if ($response) {
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
        }
        return $response;
    }

}
