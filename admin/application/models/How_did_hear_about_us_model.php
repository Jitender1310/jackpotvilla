<?php

class How_did_hear_about_us_model extends CI_Model {

    private $table_name = "how_did_hear_about_us";

    function add($data) {
        $this->db->set($data);
        $this->db->set("created_at", time());
        $response = $this->db->insert($this->table_name);
        if ($response) {
            $inserted_id = $this->db->insert_id();
            $this->log_model->create_log($this->table_name, __FUNCTION__, $inserted_id);
        }
        return $response;
    }

    function update($data, $id) {
        $this->db->set($data);
        $this->db->where("id", $id);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        if ($response) {
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
        }
        return $response;
    }

    function delete($id) {
        $this->db->where("id", $id);
        $this->db->set("status", 0);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        if ($response) {
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
        }
        return $response;
    }

    function get() {
        $this->db->where("status", 1);
        $data = $this->db->get($this->table_name)->result();
        if ($data) {
            foreach ($data as $item) {
                //$item->image = FILE_UPLOADED_PATH . $item->image;
            }
            return $data;
        }
        return [];
    }

    function is_title_exists($title, $id) {
        $this->db->where("status", 1);
        if ($id) {
            $this->db->where("(id!=$id)");
        }
        $this->db->where("title", $title);
        return $this->db->get($this->table_name)->num_rows();
    }

    function get_how_did_hear_about_us_name($how_did_hear_about_us_id) {
        $column_name = "title";
        $this->db->select($column_name);
        $this->db->where("id", $how_did_hear_about_us_id);
        $result = $this->db->get($this->table_name);
        if ($result->num_rows()) {
            return $result->row()->{$column_name};
        }
    }

}
