<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Login_model extends CI_Model {

    function is_username_exists($username) {
        $user_data = $this->db->get_where("users", ['username' => $username]);
        if ($user_data->num_rows() == 0) {
            return false;
        }
        $data = $user_data->row();
        return $data->id;
    }

    function check_user_status($user_id) {
        $data = $this->db->get_where("users", ['id' => $user_id]);
        $user = $data->row();
        if ($user->status == 0) {
            return false;
        }
        return true;
    }

    function check_user_role($user_id, $role_id) {
        $data = $this->db->get_where("users", ['id' => $user_id, 'role_id' => $role_id]);
        $user = $data->row();
        if ($user->status == 0) {
            return false;
        }
        return true;
    }

    function get_user_password($user_id) {
        $this->db->select("id, password, token, salt,role_id");
        $data = $this->db->get_where("users", ['id' => $user_id])->row();
        return $data;
    }

    function login_validation($user_id, $password) {
        $data = $this->get_user_password($user_id);
        if ($data->password == md5($password . $data->salt)) {
            try {
                /*
                  $ip = $_SERVER["REMOTE_ADDR"];
                  if ($ip != "::1") {
                  $ch = curl_init();
                  curl_setopt($ch, CURLOPT_URL, "http://ip-api.com/json/$ip?fields=520191");
                  //curl_setopt($ch, CURLOPT_URL, "http://freegeoip.net/json/$ip");
                  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                  $contents = curl_exec($ch);
                  $response = json_decode($contents);
                  curl_close($ch);
                  if (isset($response->city) && isset($response->region_name) && isset($response->country_name)) {
                  $address = "City : <b>$response->city</b> <br/>
                  Region : <b>$response->region_name</b><br/>
                  Country : <b>$response->country_name</b><br/>
                  TimeZone : <b>$response->time_zone</b><br/>
                  ";

                  $id = $this->get_max_id() + 1;
                  $insert_data = ["id" => $id,
                  "address" => $address,
                  "ip_address" => $this->input->ip_address(),
                  "user_id" => $data->id,
                  "created_at" => time()
                  ];
                  $this->db->insert("login_logs", $insert_data);
                  }
                  } */
            } catch (Exception $exc) {
                log_message(1, "Location not found " . $exc);
            }
            $this->session->set_userdata("user_id", $data->id);
            $this->session->set_userdata("role_id", $data->role_id);
            $_COOKIE['token'] = $data->token;
            return true;
        } else {
            return false;
        }
    }

    function get_last_login($user_id) {
        $this->db->order_by("id", "desc");
        $this->db->limit(1, 1);
        $data = $this->db->get_where("login_logs", ["user_id" => $user_id]);
        if ($data->num_rows()) {
            return $data->row()->created_at;
        } else {
            return false;
        }
    }

    function get_max_id() {
        $this->db->select_max("id");
        return $this->db->get("login_logs")->row()->id;
    }

    function reset_password($users_id) {
        $this->db->trans_begin();
        $user_details = $this->user_model->get_user_details($users_id);
        $this->db->where('users_id', $users_id);
        $user_details->user_meta = $this->db->get('user_meta');
        if ($user_details->user_meta->num_rows()) {
            $user_details->user_meta = $user_details->user_meta->row();
        } else {
            $user_details->user_meta = [];
        }
        $salt = generateRandomString();
        $password = generateRandomString(8);
        $encrypted_password = md5($password . $salt);
        $this->db->set('salt', $salt);
        $this->db->set('password', $encrypted_password);
        $this->db->set('updated_at', time());
        $this->db->where('id', $users_id);
        $response = $this->db->update('users');
        if ($response) {
            $this->log_model->create_log('users', __FUNCTION__, $users_id);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            $user_details->password = $password;
            return $user_details;
        }
    }

}
