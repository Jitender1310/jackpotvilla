<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Telecaller_update_feedback_for_reservations_model extends CI_Model {

    function add_or_update($data, $booking_ref_number) {
        $this->db->set($data);
        $this->db->where("booking_ref_number", $booking_ref_number);
        $this->db->set("updated_at", time());
        $response = $this->db->update("reservations");
        return $response;
    }

}
