<?php

class Change_password_model extends CI_Model {

    private $table_name = "users";

    function update($data, $user_id) {
        $this->db->trans_begin();

        $password = $data["password"];
        $salt = generateRandomString(10);
        $enc_password = md5($password . $salt);

        $this->db->set("password", $enc_password);
        $this->db->set("salt", $salt);
        $this->db->where("id", $user_id);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        if ($response) {
            $this->log_model->create_log($this->table_name, __FUNCTION__, $user_id);
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function check_for_current_password_is_correct($current_password, $user_id) {
        $password_row = $this->user_model->get_user_current_password($user_id);
        if ($password_row->password == md5($current_password . $password_row->salt)) {
            return true;
        }
        return false;
    }

}
