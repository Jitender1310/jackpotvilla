<?php

class Razor_pay_model extends CI_Model {

    function capture_razory_payment($booking_ref_number) {

        $reservation_details = $this->reservations_model->get_reservation_details($booking_ref_number);
        $this->db->select("transaction_number,amount");
        $this->db->where("reservations_id", $reservation_details->id);
        $this->db->where("payment_method", "Payment Gateway");
        $payment_gateway_id = $this->db->get("reservation_payments")->row()->transaction_number;

        $payment_gateway_access_key = $this->centers_model->get_paymentgateway_key_by_booking_ref_number($booking_ref_number);
        $payment_gateway_secret = $this->centers_model->get_paymentgateway_secret_by_booking_ref_number($booking_ref_number);


        $captured = false;

        //Making Captured Request
        $URL = "https://api.razorpay.com/v1/payments/" . $payment_gateway_id . "/capture";

        $post_fields = array(
            "amount" => (int) $reservation_details->payment_info->grand_total . "00"
        );


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_USERPWD, "$payment_gateway_access_key:$payment_gateway_secret");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $capture_response = curl_exec($ch);
        $php_capture_response = json_decode($capture_response);
        curl_close($ch);


        if ($php_capture_response->error->code == "BAD_REQUEST_ERROR") {
            if ($php_capture_response->error->field->status == "captured") {
                $captured = true;
            } else {
                return array(
                    "code" => "BAD_REQUEST_ERROR",
                    "description" => $php_capture_response->error->description
                );
            }
        } else if ($php_capture_response->status == "captured") {
            $captured = true;
        }
        return $captured;
    }

    function create_order($booking_ref_number) {
        $reservation_details = $this->reservations_model->get_reservation_details($booking_ref_number);
        $payment_gateway_access_key = $this->centers_model->get_paymentgateway_key_by_booking_ref_number($booking_ref_number);
        $payment_gateway_secret = $this->centers_model->get_paymentgateway_secret_by_booking_ref_number($booking_ref_number);

        if ($reservation_details->creation_source == "front_desk_portal" ||
                $reservation_details->creation_source == "agent_portal" ||
                $reservation_details->creation_source == "irctc_portal" ||
                $reservation_details->creation_source == "agent_mobile_app" ||
                $reservation_details->creation_source == "irctc_counter_booking"
        ) {
            return "";
        }

        $post_fields = [
            "amount" => $reservation_details->payment_info->grand_total * 100,
            "currency" => "INR"
        ];

        $URL = "https://api.razorpay.com/v1/orders";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_USERPWD, "$payment_gateway_access_key:$payment_gateway_secret");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $capture_response = curl_exec($ch);
        $php_capture_response = json_decode($capture_response);
        curl_close($ch);
        if ($php_capture_response->status == "created") {
            $order_id = $php_capture_response->id;
            $this->db->set("razorpay_order_id", $order_id);
            $this->db->where("booking_ref_number", $booking_ref_number);
            $this->db->update("reservations");
        }
    }

    function get_razor_pay_order_id($booking_ref_number) {
        $this->db->select("razorpay_order_id");
        $this->db->where('(id = "' . (int) $booking_ref_number . '" OR booking_ref_number = "' . $booking_ref_number . '")');
        $reservation_details = $this->db->get('reservations');
        if ($reservation_details->num_rows()) {
            return $reservation_details->row()->razorpay_order_id;
        }
    }

}
