<?php

    class Player_kyc_model extends CI_Model {

        private $table;

        public function __construct() {
            parent::__construct();
            $this->table = "players";
        }

        function update_kyc($data, $player_id) {

            $user_id = $this->session->userdata("user_id");

            $this->db->trans_begin();
            $this->db->where("id", $player_id);
            $player_details = $this->db->get($this->table)->row();

            if (isset($data["pan_card_number"])) {
                $this->db->set("pan_card_number", $data["pan_card_number"]);
                $this->db->set("pan_card_status", $data["pan_card_status"]);
            }
            if (isset($data["pan_card"])) {
                $this->db->set("pan_card", $data["pan_card"]);
            }

            if (isset($data["address_proof"])) {
                $this->db->set("address_proof", $data["address_proof"]);
            }

            if (isset($data["address_proof_type"])) {
                $this->db->set("address_proof_type", $data["address_proof_type"]);
            }

            if (isset($data["address_proof_status"])) {
                $this->db->set("address_proof_status", $data["address_proof_status"]);
            }

            if ($data["address_proof_status"] == "Rejected") {
                if ($player_details->address_proof_status != $data["address_proof_status"]) {
                    $this->db->set("address_proof_rejected_at", date("Y-m-d H:i:s"));
                }
            }

            if (isset($data["address_proof_rejected_reason"])) {
                $this->db->set("address_proof_rejected_reason", $data["address_proof_rejected_reason"]);
            }

            if ($data["address_proof_status"] == "Approved") {
                if ($player_details->address_proof_status != $data["address_proof_status"]) {
                    $this->db->set("address_proof_verified_at", date("Y-m-d H:i:s"));
                    $this->db->set("address_proof_rejected_reason", "");
                }
            }

            if ($data["pan_card_status"] == "Rejected") {
                if ($player_details->pan_card_status != $data["pan_card_status"]) {
                    $this->db->set("pan_card_rejected_at", date("Y-m-d H:i:s"));
                }
            }

            if (isset($data["pan_card_rejected_reason"])) {
                $this->db->set("pan_card_rejected_reason", $data["pan_card_rejected_reason"]);
            }

            if ($data["pan_card_status"] == "Approved") {
                if ($player_details->pan_card_status != $data["pan_card_status"]) {
                    $this->db->set("pan_card_verified_at", date("Y-m-d H:i:s"));
                    $this->db->set("pan_card_rejected_reason", "");
                }
            }

            $this->db->where("id", $player_id);
            $this->db->set("updated_at", time());
            $this->db->update($this->table);

            $this->db->where("id", $player_id);
            $new_player_details = $this->db->get($this->table)->row();

            if ($player_details->address_proof_type != $new_player_details->address_proof_type) {
                $log_data = [
                    "comment" => "Address proof updated ",
                    "previous_status" => $player_details->address_proof_status,
                    "current_status" => $data["address_proof_status"],
                    "players_id" => $new_player_details->id,
                    "users_id" => 0
                ];
                $this->create_log($log_data);
            }

            if ($player_details->pan_card_number != $new_player_details->pan_card_number) {
                $log_data = [
                    "comment" => "PAN Number updated",
                    "previous_status" => $player_details->address_proof_status,
                    "current_status" => $data["address_proof_status"],
                    "players_id" => $new_player_details->id,
                    "users_id" => 0
                ];
                $this->create_log($log_data);
            }

            if ($player_details->address_proof_status != $new_player_details->address_proof_status) {
                $log_data = [
                    "comment" => "Address Proof status Changed",
                    "previous_status" => $player_details->address_proof_status,
                    "current_status" => $data["address_proof_status"],
                    "players_id" => $new_player_details->id,
                    "users_id" => 0
                ];
                $this->create_log($log_data);
            }

            if ($player_details->pan_card_status != $new_player_details->pan_card_status) {
                $log_data = [
                    "comment" => "Address Proof status Changed ",
                    "previous_status" => $player_details->pan_card_status,
                    "current_status" => $data["pan_card_status"],
                    "players_id" => $new_player_details->id,
                    "users_id" => $user_id
                ];
                $this->create_log($log_data);
            }

            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();

                $this->send_kyc_notification_email($player_details, $data);
                //Send kyc notification message to customer
                return true;
            }
            return true;
        }

        function create_log($data) {
            $this->db->set($data);
            $this->db->set("created_at", date("Y-m-d H:i:s"));
            $this->db->insert("player_kyc_verification_log");
            return true;
        }

        function get_kyc_log($players_id) {
            $this->db->where("players_id", $players_id);
            $data = $this->db->get("player_kyc_verification_log")->result();
            if ($data) {
                foreach ($data as $item) {
                    $item->created_at = convert_date_time_to_display_format($item->created_at);
                    if ($item->users_id) {
                        $this->db->select("fullname");
                        $this->db->where("id", $item->users_id);
                        $item->name = $this->db->get("users")->row()->fullname;
                    } else {
                        $item->name = "Player";
                    }
                }
                return $data;
            } else {
                return [];
            }
        }

        function send_kyc_notification_email($player_details, $data) {

            if ($player_details->mobile) {
                send_message('Your KYC Status has been updated, Please See Your Email Account for more details.', $player_details->mobile);
            }
            $this->email->from(NO_REPLY_MAIL, SITE_TITLE);
            $this->email->to($player_details->email);
            $this->email->subject(SITE_TITLE . ' -- KYC Status');

            $fullname = $player_details->firstname . ' ' . $player_details->lastname;
            if (isset($fullname)) {
                $fullname = 'User';
            }

            $data1 = array(
                'name' => $fullname,
                'messages' => array(
                    array(
                        'name' => $data['address_proof_type'],
                        'status' => $data['address_proof_status'],
                        'reason' => $data['address_proof_rejected_reason']
                    ),
                    array(
                        'name' => 'PAN Card',
                        'status' => $data['pan_card_status'],
                        'reason' => $data['pan_card_rejected_reason']
                    )
                )
            );
            $message = $this->load->view("mail_templates/kyc_status", $data1, true);
            $this->email->message($message);
            $send = $this->email->send();

            if ($send) {
                return true;
            }
            return false;
        }

    }
    