<?php

class Games_model extends CI_Model {

    private $table_name = "games";

    function add($data) {
        $this->db->trans_begin();
        $data["token"] = $this->generate_game_token();
        $data["_uuid"] = $this->generate_uuid_token();

        $this->db->set($data);
        $this->db->set("created_at", time());
        $response = $this->db->insert($this->table_name);

        if ($response) {
            $inserted_id = $this->db->insert_id();
            ping_to_sfs_server(SFS_COMMAND_STATIC_GAME_ADDED . $data["token"]);
            $this->log_model->create_log($this->table_name, __FUNCTION__, $inserted_id);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function update($data, $id) {
        $this->db->trans_begin();

        if ($data["active"] == 0) {
            $response = ping_to_sfs_server(SFS_COMMAND_STATIC_GAME_INACTIVE . $id);
            if ($response != 200) {
                return "GAME_IN_PLAY";
            } else {
                $this->db->set($data);
                $this->db->where("id", $id);
                $this->db->set("updated_at", time());
                $response = $this->db->update($this->table_name);
                if ($response) {
                    $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
                }
            }
        } else {
            $this->db->set($data);
            $this->db->where("id", $id);
            $this->db->set("updated_at", time());
            $response = $this->db->update($this->table_name);
            if ($response) {
                $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
                if ($data["active"] == 1) {

                    $this->db->select("token");
                    $this->db->where("id", $id);
                    $token = $this->db->get($this->table_name)->row()->token;
                    ping_to_sfs_server(SFS_COMMAND_STATIC_GAME_ACTIVE . $token);
                }
            }
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function delete($id) {
        $this->db->where("id", $id);
        $this->db->set("status", 0);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        if ($response) {
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
        }
        return $response;
    }

    function get($filters = []) {

        if (isset($filters["game_type"])) {
            $this->db->where("game_type", $filters["game_type"]);
        }
        if (isset($filters["game_sub_type"])) {
            $this->db->where("game_sub_type", $filters["game_sub_type"]);
        }
        $this->db->where("status", 1);
        $this->db->order_by("id", "desc");
        $data = $this->db->get($this->table_name)->result();
        if ($data) {
            foreach ($data as $item) {
                $item->created_at = date(DATE_TIME_HUMAN_READABLE_FORMAT, $item->created_at);
                if ($item->updated_at) {
                    $item->updated_at = date(DATE_TIME_HUMAN_READABLE_FORMAT, $item->updated_at);
                } else {
                    $item->updated_at = "N/a";
                }

                if (!$item->reward_points) {
                    $item->reward_points = "";
                }
                if (!$item->vip) {
                    $item->vip = "";
                }
                if (!$item->updated_at) {
                    $item->updated_at = "";
                }
                if ($item->active) {
                    $item->active_text = "Active";
                    $item->active_bootstrap_css = "text-success";
                } else {
                    $item->active_text = "Inactive";
                    $item->active_bootstrap_css = "text-danger";
                }

                if ($item->game_type == "Cash") {
                    if ($item->game_sub_type == "Deals" || $item->game_sub_type == "Pool") {
                        $item->pool_deal_prize = $item->pool_deal_prize - ($item->pool_deal_prize * (ADMIN_COMMISSION_PERCENTAGE / 100));
                    }
                }
            }
            return $data;
        } else {
            return [];
        }
    }

    function generate_game_token() {
        $game_token = generateRandomString(30);
        if ($this->db->get_where($this->table_name, ["token" => $game_token])->num_rows() == 0) {
            return $game_token;
        } else {
            return $this->generate_game_token();
        }
    }

    function generate_uuid_token() {
        $_uuid = generateRandomString(30);
        if ($this->db->get_where($this->table_name, ["_uuid" => $_uuid])->num_rows() == 0) {
            return $_uuid;
        } else {
            return $this->generate_uuid_token();
        }
    }

    function get_all_games(){
        $this->db->select('casino_games.*, games_providers.name AS provider_name');
        $this->db->from('casino_games');
        $this->db->join('games_providers', 'games_providers.id = casino_games.provider_id');
        $query = $this->db->get();
        return $query->result_array();
    }

    function update_game($game_id, $update_data){
        $this->db->where('id', $game_id);
        $this->db->update('casino_games', $update_data);
    }
}
