<?php

class States_model extends CI_Model {

    private $table_name = "states";

    function add($data) {
        $this->db->set($data);
        $this->db->set("created_at", time());
        $response = $this->db->insert($this->table_name);
        if ($response) {
            $inserted_id = $this->db->insert_id();
            $this->log_model->create_log($this->table_name, __FUNCTION__, $inserted_id);
        }
        return $response;
    }

    function update($data, $id) {
        if ($this->is_state_name_exists($data["state_name"])) {
            $this->db->set("status", 1);
            $this->db->set("updated_at", time());
			$this->db->set("play_permission", $data["play_permission"]);
            $this->db->where("state_name", $data["state_name"]);
            $response = $this->db->update($this->table_name); 
        } else {
            $this->db->set($data);
            $this->db->where("id", $id);
            $this->db->set("updated_at", time());
            $response = $this->db->update($this->table_name);
            if ($response) {
                $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
            }
        }
        return $response;
    }

    function delete($id) {
        $this->db->where("id", $id);
        $this->db->set("status", 0);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        if ($response) {
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
        }
        return $response;
    }

    function get() {
        $this->db->select("id, state_name, play_permission");
        $this->db->where("status", 1);
        $data = $this->db->get($this->table_name)->result();
        if ($data) {
            foreach ($data as $item) {
                if ($item->play_permission) {
                    $item->play_permission_text = "Yes";
                    $item->play_permission_css = "text-success";
                } else {
                    $item->play_permission_text = "No";
                    $item->play_permission_css = "text-danger";
                }
            }
            return $data;
        }
        return [];
    }

    function is_state_name_exists($state_name, $id = false) {
        $this->db->where("status", 1);
        if ($id) {
            $this->db->where("(id!=$id)");
        }
        $this->db->where("state_name", $state_name);
        $rows = $this->db->get($this->table_name)->num_rows();


        return $rows;
    }

    function get_states_by_country_id($country_id) {
        $this->db->select("id, state_name");
        $this->db->where("status", 1);
        $this->db->order_by("state_name", "asc");
        $this->db->where("countries_id", $country_id);
        $data = $this->db->get($this->table_name)->result();
        if ($data) {
            return $data;
        } else {
            return [];
        }
    }

    function get_state_name($states_id) {
        $column_name = "state_name";
        $this->db->select($column_name);
        $this->db->where("id", $states_id);
        $data = $this->db->get($this->table_name)->row();
        if (isset($data)) {
            return $data->{$column_name};
        }
        return '';
    }

}
