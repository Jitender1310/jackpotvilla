<?php

class Roles_model extends CI_Model {

    private $table_name = "roles";

    function add($data) {
        $this->db->set($data);
        $this->db->set("created_at", time());
        $response = $this->db->insert($this->table_name);
        if ($response) {
            $inserted_id = $this->db->insert_id();
            $this->log_model->create_log($this->table_name, __FUNCTION__, $inserted_id);
        }
        return $response;
    }

    function update($data, $id) {
        $this->db->set($data);
        $this->db->where("id", $id);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        if ($response) {
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
        }
        return $response;
    }

    function delete($id) {
        $this->db->where("id", $id);
        $this->db->set("status", 0);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        if ($response) {
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
        }
        return $response;
    }

    function get() {
        $this->db->where("status", 1);
        $data = $this->db->get($this->table_name)->result();
        if ($data) {

            return $data;
        }
        return [];
    }

    function is_role_name_exists($role_name, $id) {
        $this->db->where("status", 1);
        if ($id) {
            $this->db->where("(id!=$id)");
        }
        $this->db->where("role_name", $role_name);
        return $this->db->get($this->table_name)->num_rows();
    }

    function get_role_name($roles_id) {
        $this->db->select("role_name");
        $this->db->where("id", $roles_id);
        return $this->db->get($this->table_name)->row()->role_name;
    }

    function add_or_update_privileges($data, $roles_id) {
        $this->db->trans_begin();

        foreach ($data as $item) {
            $cnd = ["roles_id" => $roles_id, "modules_id" => (int) $item->id];
            $existed_row = $this->db->get_where("role_wise_access_rights", $cnd);

            $this->db->set("roles_id", $roles_id);
            $this->db->set("modules_id", (int) $item->id);
            $this->db->set("view_permission", (int) $item->access_rights->view_permission);
            $this->db->set("add_permission", (int) $item->access_rights->add_permission);
            $this->db->set("edit_permission", (int) $item->access_rights->edit_permission);
            $this->db->set("delete_permission", (int) $item->access_rights->delete_permission);
            $this->db->set("all_permission", (int) $item->access_rights->all_permission);
            if ($existed_row->num_rows() == 0) {
                $this->db->set("created_at", time());
                $this->db->insert("role_wise_access_rights");
            } else {
                $this->db->set("updated_at", time());
                $this->db->where("id", $existed_row->row()->id);
                $this->db->update("role_wise_access_rights");
            }
            if (count($item->sub_modules)) {
                $this->add_or_update_recursive_sub_modules_privileges($item->sub_modules, $roles_id);
            }
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function add_or_update_recursive_sub_modules_privileges($data, $roles_id) {
        foreach ($data as $item) {
            $cnd = ["roles_id" => $roles_id, "modules_id" => (int) $item->id];
            $existed_row = $this->db->get_where("role_wise_access_rights", $cnd);

            $this->db->set("roles_id", $roles_id);
            $this->db->set("modules_id", (int) $item->id);
            $this->db->set("view_permission", (int) $item->access_rights->view_permission);
            $this->db->set("add_permission", (int) $item->access_rights->add_permission);
            $this->db->set("edit_permission", (int) $item->access_rights->edit_permission);
            $this->db->set("delete_permission", (int) $item->access_rights->delete_permission);
            $this->db->set("all_permission", (int) $item->access_rights->all_permission);
            if ($existed_row->num_rows() == 0) {
                $this->db->set("created_at", time());
                $this->db->insert("role_wise_access_rights");
            } else {
                $this->db->set("updated_at", time());
                $this->db->where("id", $existed_row->row()->id);
                $this->db->update("role_wise_access_rights");
            }
            if (count($item->sub_modules)) {
                $this->add_or_update_recursive_sub_modules_privileges($item->sub_modules, $roles_id);
            }
        }
    }

    function get_access_rights_for_module_id($modules_id, $roles_id) {
        $this->db->where("modules_id", $modules_id);
        $this->db->where("roles_id", $roles_id);
        $this->db->where("status", 1);
        $data = $this->db->get("role_wise_access_rights")->row();
        if ($data) {
            $data->view_permission = $data->view_permission ? true : false;
            $data->add_permission = $data->add_permission ? true : false;
            $data->edit_permission = $data->edit_permission ? true : false;
            $data->delete_permission = $data->delete_permission ? true : false;
            $data->all_permission = $data->all_permission ? true : false;
            return $data;
        } else {
            return (object) [
                        "roles_id" => $roles_id,
                        "modules_id" => $modules_id,
                        "view_permission" => false,
                        "add_permission" => false,
                        "edit_permission" => false,
                        "delete_permission" => false
            ];
        }
    }

}
