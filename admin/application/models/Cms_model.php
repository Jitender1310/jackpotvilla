<?php

class Cms_model extends CI_Model {

    private $table_name = "cms_pages";

    function update($data, $id) {
        $this->db->set($data);
        $this->db->where("id", $id);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        if ($response) {
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
        }
        return $response;
    }

    function get($id) {
        $this->db->where("id", $id);
        $this->db->where("status", 1);
        $data = $this->db->get($this->table_name)->row();
        return $data;
    }

}
