<?php

    class Faqs_model extends CI_Model {

        private $table_name = "faqs";

        function add($data) {
            $this->db->trans_begin();
            $this->db->set($data);
            $this->db->set("created_at", time());
            $response = $this->db->insert($this->table_name);
            if ($response) {
                $inserted_id = $this->db->insert_id();
                $this->log_model->create_log($this->table_name, __FUNCTION__, $inserted_id);
            }

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        }

        function update($data, $id) {
            $this->db->trans_begin();

            $this->db->set($data);
            $this->db->where("id", $id);
            $this->db->set("updated_at", time());
            $response = $this->db->update($this->table_name);
            if ($response) {
                $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
            }

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        }

        function delete($id) {
            $this->db->where("id", $id);
            $response = $this->db->delete($this->table_name);
            return $response;
        }

        function get() {
            $this->db->order_by("id", "desc");
            $this->db->where("status", 1);
            $data = $this->db->get($this->table_name)->result();
            if ($data) {
                foreach ($data as $item) {
                    $item->category = $this->db->get_where('faq_categories', array('id' => $item->faq_categories_id))->row()->category;
                    $item->created_date_time = date(DATE_TIME_HUMAN_READABLE_FORMAT, $item->created_at);
                }
                return $data;
            } else {
                return [];
            }
        }

    }
    