<?php

/*
 * To send email, sms notifications we use this model
 *
 */

class Send_sms_model extends CI_Model {

    function send_message($mobile, $data) {
        $customer_details = $this->customers_model->get_customer_details_by_mobile($mobile);
        if (count($customer_details) != 0) {
            $greet = $customer_details->fullname;
        } else {
            $greet = "Sir/Madam";
        }
        $message = "Dear " . $greet . " \n" . $data['message'] . "\n";
        send_message($message, $mobile);
//        $this->send_reservation_request_received_email($reservations_id);
    }

    function send_email($reservations_id) {
//This Email regarding booking has been confirmed..
// email_invoice
        $booking_details = $this->reservations_model->get_reservation_details($reservations_id);
        $this->email->from(NO_REPLAY_MAIL, SITE_TITLE);
        $this->email->to($booking_details->contact_email);
        if (BCC_EMAIL) {
            $this->email->bcc(BCC_EMAIL);
        }

        $selected_date = date('Y-m-d H:i:00', strtotime($booking_details->check_in_date . ' ' . $booking_details->check_in_time));
        $date_with_prefix = ordinal(date('d', strtotime($selected_date)));
        $date_time_text = $date_with_prefix . date(' M Y, h:i A', strtotime($selected_date));

        $this->email->subject("Booking confirm @ " . SITE_TITLE . "- " . $booking_details->center_info[0]->center_name . " on " . $date_time_text);
        $data["booking_details"] = $booking_details;
        $message = $this->load->view("includes/email_templates/appointment_created", $data, true);
        $this->email->message($message);
        $this->email->send();
    }

}
