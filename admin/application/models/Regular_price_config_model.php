<?php

class Regular_price_config_model extends CI_Model {

    private $table_name = "centers_regular_prices";

    function add_or_update($data) {
        $this->db->trans_begin();
        if (!isset($data["price_config"])) {
            return false;
        }
        /* --------------------Noraml Prices Code Start---------------------- */
        $this->db->where("centers_id", $data["centers_id"]);
        $this->db->where("staying_type_categories_id", $data["staying_type_categories_id"]);
        $this->db->delete($this->table_name);

        foreach ($data["price_config"] as $item) {
            if (trim($item["duration"]) == "") {
                continue;
            }
            $id = $this->get_max_id() + 1;
            $arr = [
                "id" => $id,
                "centers_id" => $data["centers_id"],
                "staying_type_categories_id" => $data["staying_type_categories_id"],
                "duration" => $item["duration"],
                "price" => $item["price"],
                "created_at" => time()
            ];
            $this->db->insert($this->table_name, $arr);
            $inserted_id = $this->db->insert_id();
            $this->log_model->create_log($this->table_name, __FUNCTION__, $inserted_id);
        }
        /* --------------------Noraml Prices Code Start---------------------- */

        /* --------------------Extended Prices Code Start---------------------- */
        $this->db->where("centers_id", $data["centers_id"]);
        $this->db->where("staying_type_categories_id", $data["staying_type_categories_id"]);
        $this->db->delete('centers_regular_prices_extended_stay');

        foreach ($data["extended_price_config"] as $item) {
            if (trim($item["duration"]) == "") {
                continue;
            }
            $id = $this->get_max_exteded_stay_id() + 1;
            $arr = [
                "id" => $id,
                "centers_id" => $data["centers_id"],
                "staying_type_categories_id" => $data["staying_type_categories_id"],
                "duration" => $item["duration"],
                "price" => $item["price"],
                "created_at" => time()
            ];
            $this->db->insert('centers_regular_prices_extended_stay', $arr);
            $inserted_id = $this->db->insert_id();
            $this->log_model->create_log('centers_regular_prices_extended_stay', __FUNCTION__, $inserted_id);
        }
        /* --------------------Extended Prices Code End---------------------- */

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function get_max_id() {
        $this->db->select_max("id");
        return $this->db->get($this->table_name)->row()->id;
    }

    function get_max_exteded_stay_id() {
        $this->db->select_max("id");
        return $this->db->get("centers_regular_prices_extended_stay")->row()->id;
    }

    function get_price_configuration($centers_id, $staying_type_categories_id, $table_name = null) {

        if ($table_name == null) {
            $table_name = $this->table_name;
        }
        //$durations_array_for_freshup = [3, 6, 9, 12, 24];

        $this->db->select("staying_types.name, availability_type");
        $this->db->where("staying_type_categories.id", $staying_type_categories_id);
        $this->db->from("staying_type_categories");
        $this->db->join("staying_types", "staying_types.id=staying_type_categories.staying_types_id");
        $stay_type_row = $this->db->get()->row();
        $type = $stay_type_row->name;
        $availability_type = $stay_type_row->availability_type;



        $this->db->select($table_name . ".id, centers_id, staying_type_categories_id, duration, price");
        $this->db->where("centers_id", $centers_id);
        $this->db->where("staying_type_categories_id", $staying_type_categories_id);
        $this->db->where($table_name . ".status", 1);
        $this->db->from($table_name);
        $this->db->order_by($table_name . ".duration", "asc");
        $data = $this->db->get()->result();

        if (!$data) {
            if ($type == "Freshup") {
                /* $temp_arr = [];
                  for ($i = 0; $i < count($durations_array_for_freshup); $i++) {
                  $temp_arr[] = [
                  "centers_id" => $centers_id,
                  "staying_type_categories_id" => $staying_type_categories_id,
                  "duration" => $durations_array_for_freshup[$i],
                  "price" => ''
                  ];
                  } */
                return [
                    [
                        "centers_id" => $centers_id,
                        "staying_type_categories_id" => $staying_type_categories_id,
                        "duration" => "",
                        "price" => ''
                    ]
                ];
            } else if ($type == "Hotels") {
                
            }
            return $temp_arr;
        }

        if ($type == "Freshup") {
            /*
              $arr = [];
              for ($i = 0; $i < count($durations_array_for_freshup); $i++) {
              $existed = false;
              foreach ($data as $item) {
              if ($item->duration == $durations_array_for_freshup[$i]) {
              $existed = true;
              $item->duration = (int) $item->duration;
              $item->price = (float) $item->price;
              $arr[] = $item;
              }
              }
              if ($existed == false) {
              $arr[] = [
              "centers_id" => $centers_id,
              "staying_type_categories_id" => $staying_type_categories_id,
              "duration" => $durations_array_for_freshup[$i],
              "price" => ''
              ];
              }
              }
              $data = $arr; */
            foreach ($data as $item) {
                $item->duration = (int) $item->duration;
                $item->price = (float) $item->price;
            }
        } else if ($type == "Hotels") {
            
        }
        return $data;
    }

    function get_extended_price_configuration($centers_id, $staying_type_categories_id) {
        return $this->get_price_configuration($centers_id, $staying_type_categories_id, "centers_regular_prices_extended_stay");
    }

}
