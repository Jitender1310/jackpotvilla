<?php

class Leads_model extends CI_Model {

    private $table_name = "agent_leads";

    function get($filters) {
        $this->db->where("status", 1);
        if ($filters['search_key']) {
            //$this->db->where("booking_ref_number", $filters['search_key']);
            $this->db->where("(booking_ref_number='" . $filters['search_key'] . "' OR mobile='" . $filters['search_key'] . "' OR name LIKE '%" . $filters['search_key'] . "%')");
        }
        if ($filters["id"]) {
            $this->db->where('id', $filters['id']);
        }
        if ($filters['from_date'] && ($filters['from_date'] != '')) {
            $this->db->where('expected_date_of_check_in >=', $filters['from_date']);
        }
        if ($filters['to_date'] && ($filters['to_date'] != '')) {
            $this->db->where('expected_date_of_check_in <=', $filters['to_date']);
        }
        if ($filters['centers_id'] && ($filters['centers_id'] != '')) {
            $this->db->where('centers_id', $filters['centers_id']);
        }

        if ($filters['sort_by'] && ($filters['sort_by'] != '')) {

            if ($filters['sort_by'] == 'Oldest') {
                $order = 'asc';
            } elseif ($filters['sort_by'] == 'Latest') {
                $order = 'desc';
            }
            $this->db->order_by('id', $order);
        } else {
            $this->db->order_by('id', 'desc');
        }

        $data = $this->db->get($this->table_name)->result();
        if ($data) {
            foreach ($data as $item) {
                $item->expected_date_of_check_in = date('d-m-Y', $item->expected_date_of_check_in);
                $item->created_at = date('d-m-Y', $item->created_at);
                $item->agent_name = $this->agents_model->get_agent_details($item->agents_id)->agent_name;
                $item->agent_mobile = $this->agents_model->get_agent_details($item->agents_id)->mobile;
                $item->agent_commission = $this->agents_model->get_agent_details($item->agents_id)->commission;
                $item->center_name = $this->centers_model->get_center_name($item->centers_id);
            }
            return $data;
        }
        return [];
    }

}
