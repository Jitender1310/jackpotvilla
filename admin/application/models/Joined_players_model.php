<?php

    class Joined_players_model extends CI_Model {

        private $table_name;

        public function __construct() {
            parent::__construct();
            $this->table_name = "joined_players_for_tournaments";
        }

        function get_joined_players_list($filters = []) {
            $only_cnt = false;
            $search_key = $filters['search_key'];

            $this->db->select("joined_players_for_tournaments.*");
            if ($filters['search_key']) {
                $this->db->where("(mobile='" . $search_key . "' OR username='" . $search_key . "' "
                        . "OR email='" . $search_key . "')");
                $this->db->join("players", "players.id=" . $this->table_name . ".players_id");
            }
            if ($filters['ref_id']) {
                $this->db->where("(ref_id='" . $filters['ref_id'] . "')");
                $this->db->join("cloned_tournaments", "cloned_tournaments.id=" . $this->table_name . ".cloned_tournaments_id");
            }
            if (!isset($filters["start"]) && !isset($filters["limit"])) {
                $only_cnt = true;
            }
            if ($only_cnt == true) {
                $total_records_found = $this->db->count_all_results($this->table_name);
                return $total_records_found;
            } else {
                if ($filters["sort_by"] == "Latest") {
                    $this->db->order_by($this->table_name . ".id", "desc");
                } else if ($filters["sort_by"] == "Oldest") {
                    $this->db->order_by($this->table_name . ".id", "asc");
                } else {
                    $this->db->order_by($this->table_name . ".id", "desc");
                }

                $this->db->limit($filters["limit"], $filters["start"]);
                $result = $this->db->get($this->table_name);
            }
            
//            echo $this->db->last_query();
//            die;

            if ($result->num_rows()) {
                $result = $result->result();

                foreach ($result as $item) {
                    $this->db->where('id', $item->players_id);
                    $player_details = $this->db->get('players')->row();
                    $item->player_name = $player_details->firstname . " " . $player_details->lastname;
                    if (!trim($item->player_name)) {
                        $item->player_name = $player_details->username;
                    }
                    
                    
                    

                    $item->username = $player_details->username;
                    $item->mobile = $player_details->mobile;
                    $item->email = $player_details->email;
                    $item->created_at = date(DATE_TIME_HUMAN_READABLE_FORMAT, $item->created_at);
                }

                return $result;
            }
        }

//        function add($data) {
//            $this->db->set($data);
//            $this->db->set("created_at", time());
//            $response = $this->db->insert($this->table_name);
//            if ($response) {
//                $inserted_id = $this->db->insert_id();
//                $this->log_model->create_log($this->table_name, __FUNCTION__, $inserted_id);
//            }
//            return $response;
//        }

        function update($data, $id) {
            $this->db->set($data);
            $this->db->where("id", $id);
            $this->db->set("updated_at", time());
            $response = $this->db->update($this->table_name);
            if ($response) {
                $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
            }
            return $response;
        }

    }
    