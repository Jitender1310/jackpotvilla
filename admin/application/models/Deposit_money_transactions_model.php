<?php

class Deposit_money_transactions_model extends CI_Model{
    private $table_name;
    
    public function __construct() {
        parent::__construct();
        $this->table_name = "deposit_money_transactions";
    }
    
    function get_deposit_transactions_list($filters = []) {
        $only_cnt = false;

        $search_key = $filters['search_key'];
        
        $this->db->select($this->table_name.".id");
        $this->db->select($this->table_name.".created_at");
        $this->db->select("players_id, comment, credits_count");
        $this->db->select("transaction_id, payment_gateway_id, bonus_code, transaction_status");
        $this->db->where($this->table_name.'.status', 1);
        if ($filters['search_key']) {
            //$this->db->where("booking_ref_number", $filters['search_key']);
            $this->db->where("(mobile='" . $search_key . "' OR username='".$search_key."' "
                    . "OR payment_gateway_id='".$search_key."' "
                    . "OR email='".$search_key."' "
                    . "OR transaction_id='".$search_key."')");
            $this->db->join("players", "players.id=".$this->table_name.".players_id");
        }

        if (!isset($filters["start"]) && !isset($filters["limit"])) {
            $only_cnt = true;
        }
        if($filters["from_date"]){
            $this->db->where($this->table_name.".created_at > ", $filters["from_date"]);
        }
        
        if($filters["to_date"]){
            $this->db->where($this->table_name.".created_at < ", $filters["to_date"]);
        }

        if ($only_cnt == true) {
            $total_records_found = $this->db->count_all_results($this->table_name);
            return $total_records_found;
        } else {
            /* if ($filters['search_key']) {
              $this->db->where("booking_ref_number", $filters['search_key']);
              } */
            if ($filters["sort_by"] == "Latest") {
                $this->db->order_by("id", "desc");
            } else if ($filters["sort_by"] == "Oldest") {
                $this->db->order_by("id", "asc");
            } else {
                $this->db->order_by("id", "desc");
            }
            $this->db->limit($filters["limit"], $filters["start"]);
            $result = $this->db->get($this->table_name);
        }

        if ($result->num_rows()) {
            $result = $result->result();
            foreach ($result as $item) {
                $this->db->where('id', $item->players_id);
                $player_details = $this->db->get('players')->row();
                 
                $item->player_name = $player_details->firstname." ".$player_details->lastname;
                if(!trim($item->player_name)){
                    $item->player_name = $player_details->username;
                }
                $item->username = $player_details->username;
                $item->mobile = $player_details->mobile;
                $item->email = $player_details->email;

                $item->created_at = date(DATE_TIME_HUMAN_READABLE_FORMAT, strtotime($item->created_at));
                
                if($item->transaction_status == "Pending"){
                    $item->transaction_status_bootstrap_class = "text-warning";
                }else if($item->transaction_status == "Failed"){
                    $item->transaction_status_bootstrap_class = "text-danger";
                }else if($item->transaction_status == "Cancelled"){
                    $item->transaction_status_bootstrap_class = "text-danger";
                }else if($item->transaction_status == "Success"){
                    $item->transaction_status_bootstrap_class = "text-success";
                }
            }
            return $result;
        }
    }
}