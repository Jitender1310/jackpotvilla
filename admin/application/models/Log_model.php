<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Log_model extends CI_Model {

    function create_log($table_name, $action, $table_primary_key_id) {
        $user_id = get_user_id();
        $this->db->set("user_id", $user_id);
        $this->db->set("table_name", $table_name);
        $this->db->set("table_primary_key_id", $table_primary_key_id);
        $this->db->set("action", $action);
        $this->db->set("created_at", time());
        $this->db->insert("history");
    }

}
