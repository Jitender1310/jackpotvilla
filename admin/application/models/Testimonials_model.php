<?php

class Testimonials_model extends CI_Model {

    private $table_name = "testimonials";

    function add($data) {
        $this->db->trans_begin();
        $this->db->set($data);
        $this->db->set("created_at", time());
        $response = $this->db->insert($this->table_name);
        if ($response) {
            $inserted_id = $this->db->insert_id();
            $this->log_model->create_log($this->table_name, __FUNCTION__, $inserted_id);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function update($data, $id) {
        $this->db->trans_begin();

        $this->db->set($data);
        $this->db->where("id", $id);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        if ($response) {
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function delete($id) {
        $this->db->where("id", $id);
        $pre_image = $this->db->get($this->table_name)->row()->image;
        $this->db->where("id", $id);
        $response = $this->db->delete($this->table_name);
        if ($response) {
            try {
                unlink(FILE_UPLOADED_PATH . "testimonials/" . $pre_image);
            } catch (Exception $ex) {
                log_message("error", "Avatar not deleted");
            }
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
        }
        return $response;
    }

    function get() {
        $this->db->order_by("id", "desc");
        $this->db->where("status", 1);
        $data = $this->db->get($this->table_name)->result();
        if ($data) {
            foreach ($data as $item) {
                if ($item->image) {
                    $item->image = FILE_UPLOADED_PATH . "testimonials/" . $item->image;
                } else {
                    $item->image = "";
                }
                $item->created_date_time = date(DATE_TIME_HUMAN_READABLE_FORMAT, $item->created_at);
            }
            return $data;
        } else {
            return [];
        }
    }

}
