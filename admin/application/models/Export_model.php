<?php

class Export_model extends CI_Model {

    function get_reservations_excel($filters = []) {
        $filters["only_id_required"] = true;
        $reservations = $this->reservations_model->get_reservations_list($filters);
        if (!count($reservations)) {
            return [];
        }
        $reservation_ids = implode(",", $reservations);
        $this->db->select("reservation_items.*");
        $this->db->from("reservation_items");
        $this->db->where("reservations_id in ($reservation_ids)");
        $data = $this->db->get()->result();
        return $data;
    }

    function get_reservation($reservation_id) {
        $this->db->select("reservations.*");
        $this->db->where('id', $reservation_id);
        $data = $this->db->get('reservations')->row();
        return $data;
    }

    function get_center($id) {
        $this->db->where('id', $id);
        $data = $this->db->get('centers')->row();
        return $data;
    }

    function get_payment($id) {
        $this->db->select("payment_method, transaction_number, amount");
        $this->db->where('reservations_id', $id);
        $data = $this->db->get('reservation_payments')->result();
        return $data;
    }

    function staying_type_categories($id) {
        $this->db->select("category_name");
        $this->db->where('id', $id);
        $data = $this->db->get('staying_type_categories')->row();
        return $data;
    }

    function get_tax($id, $tax_name) {
        $this->db->select("applied_tax_amount");
        $this->db->where('reservations_id', $id);
        $this->db->where('tax_name', $tax_name);
        $data = $this->db->get('reservation_tax_list')->row();
        return $data;
    }

}
