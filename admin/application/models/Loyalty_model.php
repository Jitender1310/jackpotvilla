<?php

class Loyalty_model extends CI_Model {

    private $table_name = "loyalty_slabs";

    function add_slab($data){
        $this->db->insert($this->table_name, $data);
	return $this->db->insert_id();
    }

    function get_all_slabs(){
        $this->db->from($this->table_name);
        return $this->db->get()->result_array();
    }

    function update_slab($slab_id, $update_data){
        $this->db->where('id', $slab_id);
        $this->db->update($this->table_name, $update_data);
    }

    function get_slab_by_id($slab_id){
        $this->db->from($this->table_name);
        $this->db->where('id', $slab_id);
        return $this->db->get()->result_array();
    }
}
