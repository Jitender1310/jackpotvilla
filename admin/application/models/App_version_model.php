<?php

class App_version_model extends CI_Model {

    private $table_name = "app_version";

    function add($data) {
        $this->db->trans_begin();
       
        $this->db->set($data);
        $this->db->set("created_at", time());
        $response = $this->db->insert($this->table_name);

        if ($response) {
            $inserted_id = $this->db->insert_id();
            ping_to_sfs_server(SFS_COMMAND_STATIC_GAME_ADDED . $inserted_id);
            $this->log_model->create_log($this->table_name, __FUNCTION__, $inserted_id);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function update($data, $id) {
        $this->db->trans_begin();

        $this->db->set($data);
        $this->db->where("id", $id);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        if ($response) {
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function delete($id) {
        $this->db->where("id", $id);
        $this->db->set("status", '0');
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        if ($response) {
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
        }
        return $response;
    }

    function get($filters = []) {

        if (isset($filters["game_type"])) {
            $this->db->where("game_type", $filters["game_type"]);
        }
        if (isset($filters["game_sub_type"])) {
            $this->db->where("game_sub_type", $filters["game_sub_type"]);
        }
        $this->db->where("status", '1');
        $this->db->order_by("id", "desc");
        $data = $this->db->get($this->table_name)->result();
        if ($data) {
            foreach ($data as $item) {
                $item->created_at = date(DATE_TIME_HUMAN_READABLE_FORMAT, $item->created_at);
                if ($item->updated_at) {
                    $item->updated_at = date(DATE_TIME_HUMAN_READABLE_FORMAT, $item->updated_at);
                } else {
                    $item->updated_at = "N/a";
                }
            }
            return $data;
        } else {
            return [];
        }
    }

    function generate_game_token() {
        $game_token = generateRandomString(30);
        if ($this->db->get_where($this->table_name, ["token" => $game_token])->num_rows() == 0) {
            return $game_token;
        } else {
            return $this->generate_game_token();
        }
    }

    function generate_uuid_token() {
        $_uuid = generateRandomString(30);
        if ($this->db->get_where($this->table_name, ["_uuid" => $_uuid])->num_rows() == 0) {
            return $_uuid;
        } else {
            return $this->generate_uuid_token();
        }
    }

}
