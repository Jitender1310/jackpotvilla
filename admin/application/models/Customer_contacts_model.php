<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Customer_contacts_model extends CI_Model {

    private $customers_table_name = "customers";

    function get_customers_list($filters = []) {
        $this->db->select("id, fullname, mobile, token, created_at, status");

        if ($filters['search_key']) {
            //$this->db->where("booking_ref_number", $filters['search_key']);
            $this->db->where("(mobile='" . $filters['search_key'] . "')");
        }

        if ($filters["sort_by"] == "Latest") {
            $this->db->order_by("id", "desc");
        } else if ($filters["sort_by"] == "Oldest") {
            $this->db->order_by("id", "asc");
        } else {
            $this->db->order_by("id", "desc");
        }
        $this->db->limit($filters["limit"], $filters["start"]);
        $result = $this->db->get('customers');

        if ($result->num_rows()) {
            $result = $result->result();
            foreach ($result as $item) {

                $this->db->select("email");
                $this->db->where('customers_id', $item->id);
                $item->customer_meta = $this->db->get('customer_meta')->row();

                $item->created_at = date(DATE_TIME_HUMAN_READABLE_FORMAT, $item->created_at);
                //$item->booking_environment_type = ucwords($item->booking_environment_type);
                //$item->customer_meta->country_name = $this->countries_model->get_country_name($item->customer_meta->countries_id);
            }
            return $result;
        }
    }

    function add($data) {
        $this->db->trans_begin();
        $customer_meta = $data['customer_meta'];
        unset($data['customer_meta']);

        $this->db->set($data);
        $this->db->set("created_at", time());
        $response = $this->db->insert($this->customers_table_name);
        if ($response) {
            $inserted_id = $this->db->insert_id();
            $this->log_model->create_log($this->customers_table_name, __FUNCTION__, $inserted_id);
            $this->add_or_update_customer_meta($customer_meta, $inserted_id);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return $inserted_id;
        }
    }

    function update($data, $id) {
        $this->db->trans_begin();
        $customer_meta = $data['customer_meta'];
        unset($data['customer_meta']);

        $this->db->set($data);
        $this->db->where("id", $id);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->customers_table_name);
        if ($response) {
            $this->log_model->create_log($this->customers_table_name, __FUNCTION__, $id);
            $this->add_or_update_customer_meta($customer_meta, $id);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function delete($id) {
        $this->db->where("id", $id);
        $this->db->set("status", 0);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->customers_table_name);
        if ($response) {
            $this->log_model->create_log($this->customers_table_name, __FUNCTION__, $id);
        }
        return $response;
    }

    function get_customer_details($customers_id) {
        $this->db->select("fullname, mobile, token");
        $data = $this->db->get_where($this->customers_table_name, ['id' => $customers_id])->row();
        return $data;
    }

    function get_customer_details_by_mobile($mobile) {
        $this->db->select("id,fullname, mobile, token");
        $data = $this->db->get_where('customers', ['mobile' => $mobile])->row();
        if (isset($data)) {
            $this->db->where('customers_id', $data->id);
            $customer_meta = $this->db->get('customer_meta');
            if ($customer_meta->num_rows()) {
                $data->customer_meta = $customer_meta->row();
            }
            return $data;
        }
    }

    function get_customer_details_by_id($id) {
        $this->db->select("id,fullname, mobile, token");
        $data = $this->db->get_where('customers', ['id' => $id])->row();
        if (isset($data)) {
            $this->db->where('customers_id', $data->id);
            $customer_meta = $this->db->get('customer_meta');
            if ($customer_meta->num_rows()) {
                $data->customer_meta = $customer_meta->row();
            }
            return $data;
        }
    }

    function is_mobile_exists($mobile, $id) {
        $this->db->where("status", 1);
        if ($id) {
            $this->db->where("(id!=$id)");
        }
        $this->db->where("mobile", $mobile);
        return $this->db->get($this->customers_table_name)->num_rows();
    }

    function is_token_exists($token) {
        $this->db->where("token", $token);
        return $this->db->get($this->customers_table_name)->num_rows();
    }

}
