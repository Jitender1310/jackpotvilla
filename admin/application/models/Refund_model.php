<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Refund_model extends CI_Model {

    function initiate_refund_via_payment_gateway_automatically($booking_ref_number, $selected_ids_for_cancel, $cancel_notes = "", $cancel_reason = "", $irctc_refund_id = null) {

        $response = $this->cancellation_charges_model->get_refund_details($booking_ref_number, $selected_ids_for_cancel);
        $reservation_details = $this->reservations_model->get_reservation_details($booking_ref_number);



        if ($response["type"] == "refundable") {


            if ($reservation_details->creation_source == 'irctc_portal' || $reservation_details->creation_source == 'irctc_counter_booking') {
                $this->db->set("payment_status", "Refund");
                $this->db->set("booking_status", "Cancelled");
                $this->db->set("cancel_reason", $cancel_notes);
                $this->db->set("cancel_reason", $cancel_reason);
                $this->db->set("cancelled_at", date("Y-m-d H:i:s"));
                $this->db->set("refunded_at", date("Y-m-d H:i:s"));
                $this->db->set("updated_at", time());
                $this->db->where("booking_ref_number", $reservation_details->booking_ref_number);
                $this->db->update("reservations");

                $refund_id = $irctc_refund_id;
                //$payment_gateway_id = $php_response->payment_id;

                $refundable_items_amount = 0;
                foreach ($response['booking_items'] as $booking_items) {
                    foreach ($booking_items->items as $reservation_row_item) {
                        if ($reservation_row_item->is_selected_for_cancel == 1) {
                            $this->db->set("payment_status", "Refund");
                            $this->db->set("reservation_status", "Cancelled");
                            $this->db->set("cancel_notes", $cancel_notes);
                            $this->db->set("cancel_reason", $cancel_reason);
                            $this->db->set("cancelled_at", date("Y-m-d H:i:s"));
                            $this->db->set("refunded_at", date("Y-m-d H:i:s"));
                            $this->db->set("updated_at", time());
                            $this->db->set("refund_summary", "Automatic refund via irctc");
                            $this->db->set("refund_amount", $reservation_row_item->refundable_amount);
                            $this->db->set("payment_refund_id", $refund_id);
                            $this->db->where("(id=" . $reservation_row_item->id . " OR reservation_items_id=" . $reservation_row_item->id . ")");
                            //$this->db->where("id", $reservation_row_item->id);
                            $this->db->update("reservation_items");
                            $refundable_items_amount += $reservation_row_item->refundable_amount;
                        }
                    }
                }



                $this->db->where("reservations_id", $reservation_details->id);
                $this->db->set("payment_refund_id", $irctc_refund_id);
                $this->db->set("refund_amount", $response["refundable_amount"]);
                $this->db->set("refunded_at", date("Y-m-d H:i:s"));
                $this->db->update("reservation_payments");

                //$this->reservation_notifications_model->send_appointment_canceled_message($booking_ref_number);
                return array(
                    "code" => "REFUNDED",
                    "description" => "Booking cancelled, Amount refund request has been initiated"
                );
            }

            $this->db->select("transaction_number,amount");
            $this->db->where("reservations_id", $reservation_details->id);
            $this->db->where("payment_method", "Payment Gateway");
            $payment_gateway_id = $this->db->get("reservation_payments")->row()->transaction_number;
            $payment_gateway_access_key = $this->centers_model->get_paymentgateway_key_by_booking_ref_number($booking_ref_number);
            $payment_gateway_secret = $this->centers_model->get_paymentgateway_secret_by_booking_ref_number($booking_ref_number);

            /* Capture Payment** */
            $captured = $this->razory_pay_model->capture_razory_payment($booking_ref_number);
            if (is_array($captured)) {
                return $captured;
            }
            /* Capture Payment** */

            if ($captured == true) {
                //Making Refund request
                $URL = "https://api.razorpay.com/v1/payments/" . $payment_gateway_id . "/refund";
                //$URL .= http_build_query($post_fields);

                $response["refundable_amount"] = floatval($response["refundable_amount"]);
                if ($response["refundable_amount"] < 1) {
                    $direct_cancel = true;
                    $refund_id = "";
                    $payment_gateway_id = "";
                } else {
                    $direct_cancel = false;
                }

                if ($direct_cancel == false) {
                    $post_fields = array(
                        "amount" => is_float($response["refundable_amount"]) ? (int) $response["refundable_amount"] . "00" : (int) $response["refundable_amount"] . "00"
//                    "notes" => [
//                        $cancel_notes,
//                        $cancel_reason
//                    ]
                    );



                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $URL);
                    curl_setopt($ch, CURLOPT_USERPWD, "$payment_gateway_access_key:$payment_gateway_secret");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $refund_response = curl_exec($ch);
                    curl_close($ch);

                    $php_response = json_decode($refund_response);

                    if ($php_response->error->code == "BAD_REQUEST_ERROR") {
                        return array(
                            "code" => "BAD_REQUEST_ERROR",
                            "description" => $php_response->error->description
                        );
                    } else if ($php_response->error->code == "BAD_REQUEST_ERROR") {
                        return array(
                            "code" => "BAD_REQUEST_ERROR",
                            "description" => $php_response->error->description
                        );
                    }
                    $refund_id = $php_response->id;
                    $payment_gateway_id = $php_response->payment_id;
                }

                $refundable_items_amount = 0;
                foreach ($response['booking_items'] as $booking_items) {
                    foreach ($booking_items->items as $reservation_row_item) {
                        if ($reservation_row_item->is_selected_for_cancel == 1) {
                            $this->db->set("payment_status", "Refund");
                            $this->db->set("reservation_status", "Cancelled");
                            $this->db->set("cancel_notes", $cancel_notes);
                            $this->db->set("cancel_reason", $cancel_reason);
                            $this->db->set("cancelled_at", date("Y-m-d H:i:s"));
                            $this->db->set("refunded_at", date("Y-m-d H:i:s"));
                            $this->db->set("updated_at", time());
                            $this->db->set("refund_summary", "Automatic refund via payment gateway");
                            $this->db->set("refund_amount", $reservation_row_item->refundable_amount);
                            $this->db->set("payment_refund_id", $refund_id);
                            $this->db->where("(id=" . $reservation_row_item->id . " OR reservation_items_id=" . $reservation_row_item->id . ")");
                            //$this->db->where("id", $reservation_row_item->id);
                            $this->db->update("reservation_items");
                            $refundable_items_amount += $reservation_row_item->refundable_amount;
                        }
                    }
                }

                $sms_content = "Your stay at " . SITE_TITLE . " " . $reservation_details->center_info->center_name . " "
                        . "with the ID " . $reservation_details->booking_ref_number . " has been "
                        . "modified upon your request. Your booking is eligible for Rs. " . $refundable_items_amount . " "
                        . "refund as per our cancellation policy. Please contact for further assitance.";
                $sms_content .= "\n" . $reservation_details->center_info[0]->display_mobile_numbers;
//                                $sms_content .= "\n" . $reservation_details->center_info[0]->emails;

                send_message($sms_content, $reservation_details->contact_mobile, true);


                $this->db->where("reservation_status", "Booked");
                $this->db->where("reservations_id", $reservation_details->id);
                $this->db->where("status", 1);
                if ($this->db->get("reservation_items")->num_rows() == 0) {
                    $this->db->set("payment_status", "Refund");
                    $this->db->set("booking_status", "Cancelled");
                    $this->db->set("cancel_notes", $cancel_notes);
                    $this->db->set("cancel_reason", $cancel_reason);
                    $this->db->set("cancelled_at", date("Y-m-d H:i:s"));
                    $this->db->set("refunded_at", date("Y-m-d H:i:s"));
                    $this->db->set("updated_at", time());
                    $this->db->where("booking_ref_number", $reservation_details->booking_ref_number);
                    $this->db->update("reservations");
                    $this->reservation_notifications_model->send_appointment_canceled_message($booking_ref_number);
                }

                $this->db->select("sum(refund_amount) as refund_amount");
                $this->db->where("reservation_status", "Cancelled");
                $this->db->where("reservations_id", $reservation_details->id);
                $this->db->where("status", 1);
                $this->db->where("reservation_items_id IS NULL");
                $refund_amount = $this->db->get("reservation_items")->row()->refund_amount;

                $this->db->where("reservations_id", $reservation_details->id);
                $this->db->where("transaction_number", $payment_gateway_id);
                $this->db->set("payment_refund_id", $refund_id);
                $this->db->set("refund_amount", $refund_amount);
                $this->db->set("refunded_at", date("Y-m-d H:i:s"));
                $this->db->update("reservation_payments");

                $this->reservation_notifications_model->send_appointment_partial_cancelled_email($booking_ref_number);
                return array(
                    "code" => "REFUNDED",
                    "description" => "Booking cancelled, Amount refund request has been initiated"
                );
            }
        }
    }

    function get_refunded_amount($reservations_id) {
        $this->db->select("sum(refund_amount) as refunded_amount");
        $this->db->where("reservations_id", $reservations_id);
        $this->db->where("reservation_items_id IS NULL");
        $this->db->where('status', 1);
        $data = $this->db->get("reservation_items")->row();
        return round($data->refunded_amount);
    }

    function initiate_refund_at_desk($booking_ref_number, $selected_ids_for_cancel, $cancel_notes = "", $cancel_reason = "", $refund_summary = "", $agents_id = "") {
        $user_id = get_user_id();
        $response = $this->cancellation_charges_model->get_refund_details($booking_ref_number, $selected_ids_for_cancel);
        $reservation_details = $this->reservations_model->get_reservation_details($booking_ref_number);
        $this->db->trans_begin();
        if ($response["type"] == "refundable") {
            if ($reservation_details->creation_source != 'front_desk_portal' && $reservation_details->creation_source != 'agent_portal') {
                return false;
            }

            $refundable_items_amount = 0;
            foreach ($response['booking_items'] as $booking_items) {
                foreach ($booking_items->items as $reservation_row_item) {
                    if ($reservation_row_item->is_selected_for_cancel == 1) {
                        $this->db->set("payment_status", "Refund");
                        $this->db->set("reservation_status", "Cancelled");
                        $this->db->set("cancel_notes", $cancel_notes);
                        $this->db->set("cancel_reason", $cancel_reason);
                        $this->db->set("cancelled_at", date("Y-m-d H:i:s"));
                        $this->db->set("refunded_at", date("Y-m-d H:i:s"));
                        $this->db->set("updated_at", time());
                        $this->db->set("refund_amount", $reservation_row_item->refundable_amount);
                        $this->db->set("refund_summary", $refund_summary);
                        if ($agents_id != "") {
                            $this->db->set("details_updated_by_users_id", $agents_id);
                            $this->db->set("refunded_by_users_id", $agents_id);
                        } else {
                            $this->db->set("details_updated_by_users_id", $user_id);
                            $this->db->set("refunded_by_users_id", $user_id);
                        }

                        $this->db->where("(id=" . $reservation_row_item->id . " OR reservation_items_id=" . $reservation_row_item->id . ")");
                        //$this->db->where("id", $reservation_row_item->id);
                        $this->db->update("reservation_items");
                        $refundable_items_amount += $reservation_row_item->refundable_amount;
                    }
                }
            }

            if ($reservation_details->creation_source == "agent_portal") {
                $agent_payment_data = [
                    "agents_id" => $agents_id,
                    "users_id" => null,
                    "amount" => $refundable_items_amount,
                    "payment_method" => 'Cash',
                    "remarks" => "Refund amount has been Credited for Booking #" . $reservation_details->booking_ref_number,
                    "transaction_id" => md5(microtime()) . $agents_id,
                    "reservations_id" => $reservation_details->id,
                    "booking_ref_number" => $reservation_details->booking_ref_number,
                    "transaction_type" => "Credit",
                    "type" => "Booking",
                    "date" => date("Y-m-d")
                ];

                $this->agents_model->update_agent_payment_details($agent_payment_data);
            }


            $send_appoint_fully_cancelled = false;
            $this->db->where("reservation_status", "Booked");
            $this->db->where("reservations_id", $reservation_details->id);
            $this->db->where("status", 1);
            if ($this->db->get("reservation_items")->num_rows() == 0) {
                $this->db->set("payment_status", "Refund");
                $this->db->set("booking_status", "Cancelled");
                $this->db->set("cancel_notes", $cancel_notes);
                $this->db->set("cancel_reason", $cancel_reason);
                $this->db->set("cancelled_at", date("Y-m-d H:i:s"));
                $this->db->set("refunded_at", date("Y-m-d H:i:s"));
                $this->db->set("updated_at", time());
                $this->db->where("booking_ref_number", $reservation_details->booking_ref_number);
                $this->db->update("reservations");
                $send_appoint_fully_cancelled = true;
            }

            $this->db->select("sum(refund_amount) as refund_amount");
            $this->db->where("reservation_status", "Cancelled");
            $this->db->where("reservations_id", $reservation_details->id);
            $this->db->where("status", 1);
            $this->db->where("reservation_items_id IS NULL");
            $refund_amount = $this->db->get("reservation_items")->row()->refund_amount;

//            $this->db->where("reservations_id", $reservation_details->id);
//            $this->db->set("payment_refund_id", null);
//            $this->db->set("refund_amount", $refund_amount);
//            $this->db->set("refunded_at", date("Y-m-d H:i:s"));
//            $this->db->update("reservation_payments");



            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                $this->reservation_notifications_model->send_appointment_partial_cancelled_email($booking_ref_number);
                if ($send_appoint_fully_cancelled == true) {
                    $this->reservation_notifications_model->send_appointment_canceled_message($booking_ref_number);
                }

                $sms_content = "Your stay at " . SITE_TITLE . " " . $reservation_details->center_info->center_name . " "
                        . "with the ID " . $reservation_details->booking_ref_number . " has been "
                        . "modified upon your request. Your booking is eligible for Rs. " . $refundable_items_amount . " "
                        . "refund as per our cancellation policy. Please contact for further assitance.";
                $sms_content .= "\n" . $reservation_details->center_info[0]->display_mobile_numbers;
//                                $sms_content .= "\n" . $reservation_details->center_info[0]->emails;

                send_message($sms_content, $reservation_details->contact_mobile, true);

                return array(
                    "code" => "REFUNDED",
                    "description" => "Booking cancelled, Amount refund details has been updated"
                );
            }
        }
    }

}
