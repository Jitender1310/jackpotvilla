<?php

class Modules_model extends CI_Model {

    private $table_name = "modules";

    function add($data) {
        $this->db->set($data);
        $this->db->set("created_at", time());
        $response = $this->db->insert($this->table_name);
        if ($response) {
            $inserted_id = $this->db->insert_id();
            $this->log_model->create_log($this->table_name, __FUNCTION__, $inserted_id);
        }
        return $response;
    }

    function update($data, $id) {
        $this->db->set($data);
        $this->db->where("id", $id);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        if ($response) {
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
        }
        return $response;
    }

    function delete($id) {
        $this->db->where("id", $id);
        $this->db->set("status", 0);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        if ($response) {
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
        }
        return $response;
    }

    function get() {
        $this->db->select("id, module_name, parent_module_id, display_order");
        $this->db->where("status", 1);
        $this->db->order_by("display_order", "asc");
        $data = $this->db->get($this->table_name)->result();
        if ($data) {
            foreach ($data as $item) {
                $item->sub_modules = $this->get_sub_modules($item->id);
                $this->p_name = $item->module_name;
                if ($item->parent_module_id) {
                    $item->parent_module_name = $this->get_module_name($item->parent_module_id);
                    $item->display_as = $this->get_recursive_module_name_child_to_parent($item->parent_module_id);

                    $arr = explode(" << ", $item->display_as);
                    rsort($arr);
                    $item->display_as_p_c = implode(" >> ", $arr);
                } else {
                    $item->parent_module_name = "No Parent";
                    $item->display_as = $item->module_name;
                    $item->display_as_p_c = $item->module_name;
                }
            }
            return $data;
        }
        return [];
    }

    private $privileges = [
        "view_permission" => true,
        "add_permission" => true,
        "edit_permission" => true,
        "delete_permission" => true,
        "all_permission" => true
    ];

    function check_for_privilegies_column_selected($row) {

        if (is_array($row)) {
            $row = (object) $row;
        }
        if (!$row->view_permission) {
            $this->privileges['view_permission'] = false;
        }
        if (!$row->add_permission) {
            $this->privileges['add_permission'] = false;
        }
        if (!$row->edit_permission) {
            $this->privileges['edit_permission'] = false;
        }
        if (!$row->delete_permission) {
            $this->privileges['delete_permission'] = false;
        }
        if (isset($row->all_permission)) {
            if (!$row->all_permission) {
                $this->privileges['all_permission'] = false;
            }
        } else {
            $this->privileges['all_permission'] = false;
        }
    }

    function get_privileges_column_headings() {
        /* if ($this->privileges['view_permission'] &&
          $this->privileges['add_permission'] &&
          $this->privileges['edit_permission'] &&
          $this->privileges['delete_permission']) {
          $this->privileges['selectedAll'] = true;
          } */
        return $this->privileges;
    }

    function get_tree_level_structure_of_modules_for_user($user_id) {
        $this->db->where(" (parent_module_id IS NULL or parent_module_id=0) ");
        $this->db->select("id, module_name, parent_module_id, display_order");
        $this->db->order_by("display_order", "asc");
        $this->db->where("status", 1);
        $data = $this->db->get($this->table_name)->result();
        if ($data) {
            foreach ($data as $item) {
                if ($user_id) {
                    $item->access_rights = $this->user_model->get_access_rights_for_module_id($item->id, $user_id);
                    $this->check_for_privilegies_column_selected($item->access_rights);
                }

                $item->sub_modules = $this->get_sub_modules_for_user($item->id, $user_id);
            }
            return $data;
        }
        return [];
    }

    function get_sub_modules_for_user($parent_module_id, $user_id = null) {
        $this->db->select("id, module_name, parent_module_id,  display_order");
        $this->db->where("parent_module_id", $parent_module_id);
        $this->db->order_by("display_order", "asc");
        $this->db->where("status", 1);
        $data = $this->db->get($this->table_name)->result();
        if ($data) {
            foreach ($data as $item) {
                if ($user_id) {
                    $item->access_rights = $this->user_model->get_access_rights_for_module_id($item->id, $user_id);
                    $this->check_for_privilegies_column_selected($item->access_rights);
                }
                $item->sub_modules = $this->get_sub_modules_for_user($item->id, $user_id);
            }
            return $data;
        }
        return [];
    }

    function get_tree_level_structure_of_modules($roles_id = null) {

        $this->db->where(" (parent_module_id IS NULL or parent_module_id=0) ");
        $this->db->select("id, module_name, parent_module_id,  display_order");
        $this->db->order_by("display_order", "asc");
        $this->db->where("status", 1);
        $data = $this->db->get($this->table_name)->result();
        if ($data) {
            foreach ($data as $item) {
                if ($roles_id) {
                    $item->access_rights = $this->roles_model->get_access_rights_for_module_id($item->id, $roles_id);
                    $this->check_for_privilegies_column_selected($item->access_rights);
                }

                $item->sub_modules = $this->get_sub_modules($item->id, $roles_id);
            }
            return $data;
        }
        return [];
    }

    function get_sub_modules($parent_module_id, $roles_id = null) {
        $this->db->select("id, module_name, parent_module_id, display_order");
        $this->db->where("parent_module_id", $parent_module_id);
        $this->db->order_by("display_order", "asc");
        $this->db->where("status", 1);
        $data = $this->db->get($this->table_name)->result();
        if ($data) {
            foreach ($data as $item) {
                if ($roles_id) {
                    $item->access_rights = $this->roles_model->get_access_rights_for_module_id($item->id, $roles_id);
                    $this->check_for_privilegies_column_selected($item->access_rights);
                }
                $item->sub_modules = $this->get_sub_modules($item->id, $roles_id);
            }
            return $data;
        }
        return [];
    }

    private $p_name = "";

    function get_recursive_module_name_child_to_parent($parent_module_id) {
        $this->db->select("id, module_name, parent_module_id,  display_order");
        $this->db->where("id", $parent_module_id);
        $this->db->order_by("display_order", "asc");
        $this->db->where("status", 1);
        $module_row = $this->db->get($this->table_name)->row();

        $module_name = $module_row->module_name;
        $this->p_name .= " << " . $module_name;

        if ($module_row->parent_module_id) {
            return $this->get_recursive_module_name_child_to_parent($module_row->parent_module_id);
        }
        return $this->p_name;
    }

    function is_module_name_exists($module_name, $id) {
        $this->db->where("status", 1);
        if ($id) {
            $this->db->where("(id!=$id)");
        }
        $this->db->where("module_name", $module_name);
        return $this->db->get($this->table_name)->num_rows();
    }

    function is_module_code_exists($code, $id) {
        $this->db->where("status", 1);
        if ($id) {
            $this->db->where("(id!=$id)");
        }
        $this->db->where("code", $code);
        return $this->db->get($this->table_name)->num_rows();
    }

    function get_module_name($modules_id) {
        $this->db->select("module_name");
        $this->db->where("id", $modules_id);
        return $this->db->get($this->table_name)->row()->module_name;
    }

}
