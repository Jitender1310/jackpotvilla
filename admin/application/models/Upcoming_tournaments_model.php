<?php

    class Upcoming_tournaments_model extends CI_Model {

        private $table_name = "cloned_tournaments";

        function get_progress($cloned_tournaments_id) {
            $this->db->where("cloned_tournaments_id", $cloned_tournaments_id);
            $this->db->order_by("round_number", "asc");
            $result = $this->db->get("cloned_tournaments_round_wise_data")->result();
            foreach ($result as $item) {
                $item->players_info = json_decode($item->players_info);
                foreach ($item->players_info as $sub_item) {
                    foreach ($sub_item->players as $p_item) {
                        $p_item->player_name = "";
                        $p_item->player_name = $this->db->select("username")->where("id", $p_item->tournamentRegPlayer->players_id)->get("players")->row()->username;
                    }
                }
            }
            return $result;
        }

        function add_or_update_prize_distributions($prizes_info, $cloned_tournaments_id) {
            $this->db->where("cloned_tournaments_id", $cloned_tournaments_id);
            $this->db->delete("cloned_tournaments_prize_distribution_config");
            for ($i = 0, $l = count($prizes_info); $i < $l; $i++) {
                $prizes_info[$i] = (object) $prizes_info[$i];
                $id = $this->get_max_id() + 1;
                $this->db->set("id", $id);
                $this->db->set("from_rank", $prizes_info[$i]->from_rank);
                $this->db->set("to_rank", $prizes_info[$i]->to_rank);
                $this->db->set("prize_value", $prizes_info[$i]->prize_value);
                $this->db->set("min_prize_value", $prizes_info[$i]->min_prize_value);
                $this->db->set("cloned_tournaments_id", $cloned_tournaments_id);
                $this->db->insert("cloned_tournaments_prize_distribution_config");
            }
        }

        function get_max_id() {
            $this->db->select_max("id");
            return $this->db->get("cloned_tournaments_prize_distribution_config")->row()->id;
        }

        function update($data, $id) {
            $this->db->trans_begin();
            $prizes_info = $data["prizes_info"];
            unset($data["prizes_info"]);
            $this->db->set($data);
            $this->db->where("id", $id);
            $this->db->set("updated_at", time());
            $this->db->update($this->table_name);
            $this->add_or_update_prize_distributions($prizes_info, $id);
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        }

        function delete($id) {
            $this->db->where("id", $id);
            $this->db->set("status", 0);
            $this->db->set("last_action_by_users_id", get_user_id());
            $this->db->set("updated_at", time());
            $response = $this->db->update($this->table_name);
            if ($response) {
                $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
            }
            return $response;
        }

        function get($filters) {
            $this->db->where("status", 1);
            if ($filters['frequency'] && $filters['frequency'] != '') {
                $this->db->where("frequency", $filters['frequency']);
            }
            if ($filters['tournament_categories_id'] && $filters['tournament_categories_id'] != '') {
                $this->db->where("tournament_categories_id", $filters['tournament_categories_id']);
            }
            if ($filters['premium_category'] && $filters['premium_category'] != '') {
                $this->db->where("premium_category", $filters['premium_category']);
            }
            if (isset($filters['to_date']) && isset($filters['from_date'])) {
//                if ($filters['from_date'] != '') {
//                    $this->db->where("strtotime(registration_start_date) >=", strtotime($filters['from_date']));
//                }
//                $this->db->where("strtotime(registration_start_date) <=", strtotime($filters['to_date']));
                $this->db->where('registration_start_date BETWEEN "' . $filters['from_date'] . '" AND "' . $filters['to_date'] . '"', '', false);
            }


            if ($filters['type'] == 'Completed') {
                $this->db->where("(tournament_status = 'Completed')");
                $this->db->order_by("registration_start_date", "desc");
            } else if ($filters['type'] == "Upcoming") {
                $this->db->where("(tournament_status = 'Created' OR tournament_status = 'Registration_Start' OR "
                        . "tournament_status = 'Registration_Closed' OR tournament_status = 'Running')");
                $this->db->order_by("registration_start_date", "asc");
            } else if ($filters['type'] == "Running") {
                $this->db->where("(tournament_status = 'Running')");
                $this->db->order_by("registration_start_date", "asc");
            } else {
                $this->db->order_by("id", "desc");
            }
            $data = $this->db->get($this->table_name)->result();


            if ($data) {
                foreach ($data as $item) {
                    $item->tournament_category = $this->tournament_categories_model->get_tournament_category_name($item->tournament_categories_id);

                    $item->allowed_club_types_text = [];

                    if ($item->allowed == "All") {
                        $club_types = $this->club_types_model->get();
                        foreach ($club_types as $c_item) {
                            $item->allowed_club_types_text[] = $c_item->name;
                        }
                        $item->allowed_club_types_text = implode(", ", $item->allowed_club_types_text);
                    } else {
                        $selected_club_types = explode(",", $item->allowed_club_types);
                        for ($i = 0; $i < count($selected_club_types); $i++) {
                            $this->db->where("id", $selected_club_types[$i]);
                            $club_types = $this->club_types_model->get();
                            foreach ($club_types as $c_item) {
                                $item->allowed_club_types_text[] = $c_item->name;
                            }
                        }
                        $item->allowed_club_types_text = implode(", ", $item->allowed_club_types_text);
                    }

                    if ($item->registration_start_date) {
                        $item->registration_start_date = convert_date_to_display_format($item->registration_start_date);
                    }

                    if ($item->registration_close_date) {
                        $item->registration_close_date = convert_date_to_display_format($item->registration_close_date);
                    }

                    if ($item->tournament_start_date) {
                        $item->tournament_start_date = convert_date_to_display_format($item->tournament_start_date);
                    }

                    $item->registration_start_time = date("h:i A", strtotime(date("Y-m-d" . " " . $item->registration_start_time)));
                    $item->registration_close_time = date("h:i A", strtotime(date("Y-m-d" . " " . $item->registration_close_time)));
                    $item->tournament_start_time = date("h:i A", strtotime(date("Y-m-d" . " " . $item->tournament_start_time)));

                    $item->created_at = date(DATE_TIME_HUMAN_READABLE_FORMAT, $item->created_at);
                    if ($item->updated_at) {
                        $item->updated_at = date(DATE_TIME_HUMAN_READABLE_FORMAT, $item->updated_at);
                    } else {
                        $item->updated_at = "N/a";
                    }


                    if (!$item->updated_at) {
                        $item->updated_at = "";
                    }
                    if ($item->active) {
                        $item->active_text = "Active";
                        $item->active_bootstrap_css = "text-success";
                    } else {
                        $item->active_text = "Inactive";
                        $item->active_bootstrap_css = "text-danger";
                    }
                    $this->db->where("cloned_tournaments_id", $item->id);
                    $item->prizes_info = $this->db->get("cloned_tournaments_prize_distribution_config")->result();
                    $item->tournament_structure_for_edit = json_decode($item->tournament_structure);
                    $item->tournament_structure = $this->get_tournament_structure($item->tournament_structure, $item->max_players);
                    $item->prize_distributions = $this->get_tournament_prize_distributions($item->id);
                }
                return $data;
            } else {
                return [];
            }
        }

        function get_tournament_structure($tournament_json_obj, $max_players) {
            $sturcture_obj = json_decode($tournament_json_obj);
            $current_round = 0;
            $arr = [];
            for ($i = 0; $i < $max_players; $i++) {
                $info = calculate_rounds($sturcture_obj, $current_round, $max_players);
                $arr[] = $info;
                if ($info->qualified > 6) {
                    $current_round = $info->round_number;
                    $max_players = $info->qualified;
                } else {
                    break;
                }
            }
            return get_round_titles($arr);
        }

        function is_token_exists($token) {
            $this->db->where("access_token", $token);
            return $this->db->get($this->table_name)->num_rows();
        }

        function generate_tournament_token() {
            $tournament_token = generateRandomString(30);
            if ($this->db->get_where($this->table_name, ["token" => $tournament_token])->num_rows() == 0) {
                return $tournament_token;
            } else {
                return $this->generate_tournament_token();
            }
        }

        function get_tournament_prize_distributions($id) {
            $this->db->select("from_rank, to_rank, min_prize_value, prize_value, extra_prize_info");
            $this->db->where("cloned_tournaments_id", $id);
            $this->db->order_by("from_rank", "asc");
            $data = $this->db->get("cloned_tournaments_prize_distribution_config")->result();
            if ($data) {
                foreach ($data as $item) {
                    $item->no_of_players = $item->to_rank - $item->from_rank;
                    $item->display_position = ordinal($item->from_rank);
                }
                return $data;
            }
            return [];
        }

    }
    