<?php

class Wallet_model extends CI_Model {
    
    private $table_name;
    public function __construct() {
        parent::__construct();
        $this->table_name = "wallet_account";
    }

    function create_wallet($wallet_account_name = "") {
        $uuid = $this->generate_wallet_token();
        $data = [
            "fun_chips" => MAX_FUN_CHIPS,
            "in_play_fun_chips" => 0,
            "real_chips_deposit" => 0,
            "in_play_real_chips" => 0,
            "real_chips_withdrawal" => 0,
            "total_bonus" => 0,
            "_uuid" => $uuid,
            "wallet_account_name" => $wallet_account_name
        ];
        $this->db->set($data);
        $this->db->insert($this->table_name);
        return $this->db->insert_id();
    }

    function generate_wallet_token() {
        $uuid = generateRandomString(10);
        if ($this->db->get_where("wallet_account", ["_uuid" => $uuid])->num_rows() == 0) {
            return $uuid;
        } else {
            return $this->generate_wallet_token();
        }
    }
    
    function get_wallet_account($wallet_account_id){
        $this->db->where("id", $wallet_account_id);
        $row = $this->db->get($this->table_name)->row();
        if($row){
            
            $row->release_bonus = $this->bonus_model->get_released_bonus($wallet_account_id);
            $row->pending_bonus = $this->bonus_model->get_pending_bonus($wallet_account_id);
            $row->active_bonus = $this->bonus_model->get_active_bonus($wallet_account_id);
            
            return $row;
        }else{
            return [];
        }
    }

}
