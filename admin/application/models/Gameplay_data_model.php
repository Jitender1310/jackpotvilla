<?php

class Gameplay_data_model extends CI_Model{
    private $table_name;
    
    public function __construct() {
        parent::__construct();
        $this->table_name = "game_play";
    }
        
    function get_data($filters = []) {
        $only_cnt = false;

        $this->db->where($this->table_name.'.status', 1);

        if(isset($filters['game_type'])){
            $this->db->where('game_type', $filters['game_type']);
        }
        
        if(isset($filters['game_sub_type'])){
            $this->db->where('game_sub_type', $filters['game_sub_type']);
        }
        
        if(isset($filters['rooms_id'])){
            $this->db->where('rooms_id', $filters['rooms_id']);
        }
        
        if ($filters["from_date"]) {
            $this->db->where($this->table_name . ".created_at > ", $filters["from_date"]);
        }

        if ($filters["to_date"]) {
            $this->db->where($this->table_name . ".created_at < ", $filters["to_date"]);
        }
        
        if (!isset($filters["start"])) {
            $only_cnt = true;
        }
 
        if ($only_cnt == true) {
            $total_records_found = $this->db->count_all_results($this->table_name);
            return $total_records_found;
        } else {
            if ( isset($filters["sort_by"]) && $filters["sort_by"] == "Latest") {
                $this->db->order_by("id", "desc");
            } else if ( isset($filters["sort_by"]) && $filters["sort_by"] == "Oldest") {
                $this->db->order_by("id", "asc");
            } else {
                $this->db->order_by("id", "desc");
            }
            $this->db->limit($filters["limit"], $filters["start"]);
            $result = $this->db->get($this->table_name);
        }

        if ($result->num_rows()) {
            $data = $result->result();
            foreach($data as $item){
//                print_r($item->game_detail_json);
//                die;

                if($item->game_type=="Cash"){
                    $item->game_type_bootstrap_css = "text-info";
                }else {
                    $item->game_type_bootstrap_css = "text-danger";
                }
                if($item->game_sub_type=="Deals"){
                    $item->game_sub_type_bootstrap_css = "text-info";
                }else if($item->game_sub_type=="Pool"){
                    $item->game_sub_type_bootstrap_css = "text-success";
                }else if($item->game_sub_type=="Points"){
                    $item->game_sub_type_bootstrap_css = "text-danger";
                }
            }
            return $data;
            foreach($data as $item){
                $item->game_detail_json = json_decode($item->game_detail_json);
            }
            return $data;
        }
        
        return [];
    }
}