<?php

    class Withdraw_requests_model extends CI_Model {

        private $table_name;

        public function __construct() {
            parent::__construct();
            $this->table_name = "withdraw_request";
        }

        function get_withdraw_requests_list($filters = []) {
            $only_cnt = false;

            $search_key = $filters['search_key'];
            $this->db->where($this->table_name . '.status', 1);
            if ($filters['search_key']) {
                $this->db->where("(mobile='" . $search_key . "' OR username='" . $search_key . "' "
                        . "OR email='" . $search_key . "')");
                $this->db->join("players", "players.id=" . $this->table_name . ".players_id");
            }

            if (!isset($filters["start"]) && !isset($filters["limit"])) {
                $only_cnt = true;
            }
            if ($filters["from_date"]) {
                $this->db->where($this->table_name . ".created_at > ", $filters["from_date"]);
            }

            if ($filters["to_date"]) {
                $this->db->where($this->table_name . ".created_at < ", $filters["to_date"]);
            }

            if ($only_cnt == true) {
                $total_records_found = $this->db->count_all_results($this->table_name);
                return $total_records_found;
            } else {
                if ($filters["sort_by"] == "Latest") {
                    $this->db->order_by($this->table_name . ".id", "desc");
                } else if ($filters["sort_by"] == "Oldest") {
                    $this->db->order_by($this->table_name . ".id", "asc");
                } else {
                    $this->db->order_by($this->table_name . ".id", "desc");
                }
                $this->db->limit($filters["limit"], $filters["start"]);
                $result = $this->db->get($this->table_name);
            }

            if ($result->num_rows()) {
                $result = $result->result();
                foreach ($result as $item) {
                    $this->db->where('id', $item->players_id);
                    $player_details = $this->db->get('players')->row();
                    $item->player_name = $player_details->firstname . " " . $player_details->lastname;
                    if (!trim($item->player_name)) {
                        $item->player_name = $player_details->username;
                    }
                    $item->username = $player_details->username;
                    $item->mobile = $player_details->mobile;
                    $item->email = $player_details->email;

                    $item->created_at = date(DATE_TIME_HUMAN_READABLE_FORMAT, $item->created_at);

                    if ($item->request_status == "Pending") {
                        $item->request_status_bootstrap_class = "text-warning";
                    } else if ($item->request_status == "Completed") {
                        $item->request_status_bootstrap_class = "text-success";
                    } else {
                        $item->request_status_bootstrap_class = "text-danger";
                    }

                    $item->request_processed_user = $this->db->get_where('users', array('id' => $item->request_processed_users_id))->row()->fullname;
                }
                return $result;
            }
        }

//        function add($data) {
//            $this->db->set($data);
//            $this->db->set("created_at", time());
//            $response = $this->db->insert($this->table_name);
//            if ($response) {
//                $inserted_id = $this->db->insert_id();
//                $this->log_model->create_log($this->table_name, __FUNCTION__, $inserted_id);
//            }
//            return $response;
//        }

        function update($data, $id) {
            $this->db->set($data);
            $this->db->where("id", $id);
            $this->db->set("updated_at", time());
            $response = $this->db->update($this->table_name);
            if ($response) {
                $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
            }
            return $response;
        }

    }
    