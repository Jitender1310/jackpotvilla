<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class User_model extends CI_Model {

    private $table_name = "users";

    function add($data) {
        $this->db->trans_begin();
        $user_meta = $data['user_meta'];
        unset($data['user_meta']);

        $this->db->set($data);
        $this->db->set("created_at", time());
        $response = $this->db->insert($this->table_name);
        if ($response) {
            $inserted_id = $this->db->insert_id();
            $this->log_model->create_log($this->table_name, __FUNCTION__, $inserted_id);
            $this->add_or_update_user_meta($user_meta, $inserted_id);
            $privileges_data = $this->modules_model->get_tree_level_structure_of_modules($data['role_id']);
            $this->add_or_update_privileges($privileges_data, $inserted_id);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function update($data, $id) {
        $this->db->trans_begin();

        $user_meta = $data['user_meta'];
        unset($data['user_meta']);

        $this->db->set($data);
        $this->db->where("id", $id);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        if ($response) {
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
            $this->add_or_update_user_meta($user_meta, $id);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function add_or_update_user_meta($user_meta, $users_id) {

        $data = $user_meta;
        $this->db->where("users_id", $users_id);
        if ($this->db->get("user_meta")->num_rows()) {
            $this->db->set($data);
            $this->db->set("updated_at", time());
            $this->db->where("users_id", $users_id);
            $this->db->update("user_meta");
        } else {
            $this->db->set($data);
            $this->db->set("users_id", $users_id);
            $this->db->set("created_at", time());
            $this->db->insert("user_meta");
        }
        return true;
    }

    function delete($id) {
        $this->db->where("id", $id);
        $this->db->set("status", 0);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        if ($response) {
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
        }
        return $response;
    }

    function add_or_update_privileges($data, $user_id) {
        foreach ($data as $item) {
            $cnd = ["user_id" => $user_id, "modules_id" => (int) $item->id];
            $existed_row = $this->db->get_where("user_wise_access_rights", $cnd);

            $this->db->set("user_id", $user_id);
            $this->db->set("modules_id", (int) $item->id);
            $this->db->set("view_permission", (int) $item->access_rights->view_permission);
            $this->db->set("add_permission", (int) $item->access_rights->add_permission);
            $this->db->set("edit_permission", (int) $item->access_rights->edit_permission);
            $this->db->set("delete_permission", (int) $item->access_rights->delete_permission);
            if ($existed_row->num_rows() == 0) {
                $this->db->set("created_at", time());
                $this->db->insert("user_wise_access_rights");
            } else {
                $this->db->set("updated_at", time());
                $this->db->where("id", $existed_row->row()->id);
                $this->db->update("user_wise_access_rights");
            }
            if (count($item->sub_modules)) {
                $this->add_or_update_recursive_sub_modules_privileges($item->sub_modules, $user_id);
            }
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function add_or_update_recursive_sub_modules_privileges($data, $user_id) {
        foreach ($data as $item) {
            $cnd = ["user_id" => $user_id, "modules_id" => (int) $item->id];
            $existed_row = $this->db->get_where("user_wise_access_rights", $cnd);

            $this->db->set("user_id", $user_id);
            $this->db->set("modules_id", (int) $item->id);
            $this->db->set("view_permission", (int) $item->access_rights->view_permission);
            $this->db->set("add_permission", (int) $item->access_rights->add_permission);
            $this->db->set("edit_permission", (int) $item->access_rights->edit_permission);
            $this->db->set("delete_permission", (int) $item->access_rights->delete_permission);
            if ($existed_row->num_rows() == 0) {
                $this->db->set("created_at", time());
                $this->db->insert("user_wise_access_rights");
            } else {
                $this->db->set("updated_at", time());
                $this->db->where("id", $existed_row->row()->id);
                $this->db->update("user_wise_access_rights");
            }
            if (count($item->sub_modules)) {
                $this->add_or_update_recursive_sub_modules_privileges($item->sub_modules, $user_id);
            }
        }
    }

    function get_access_rights_for_module_id($modules_id, $user_id) {
        $this->db->where("modules_id", $modules_id);
        $this->db->where("user_id", $user_id);
        $this->db->where("status", 1);
        $data = $this->db->get("user_wise_access_rights")->row();
        if ($data) {
            $data->view_permission = $data->view_permission ? true : false;
            $data->add_permission = $data->add_permission ? true : false;
            $data->edit_permission = $data->edit_permission ? true : false;
            $data->delete_permission = $data->delete_permission ? true : false;
            return $data;
        } else {
            return [
                "user_id" => $user_id,
                "modules_id" => $modules_id,
                "view_permission" => false,
                "add_permission" => false,
                "edit_permission" => false,
                "delete_permission" => false
            ];
        }
    }

    function get_user_details($user_id) {
        $this->db->select("fullname, username, token, role_id,id");
        $data = $this->db->get_where($this->table_name, ['id' => $user_id])->row();
        return $data;
    }

    function get_user_current_password($user_id) {
        $this->db->select("id, password, salt");
        $row = $this->db->get_where($this->table_name, ['id' => $user_id])->row();
        return $row;
    }

    function get() {
        $this->db->order_by("id", "desc");
        $this->db->select("id, role_id,fullname, username, token, login_status, created_at");
        $this->db->where("status", 1);
        $data = $this->db->get($this->table_name)->result();
        if ($data) {
            foreach ($data as $item) {
                $item->role_name = $this->roles_model->get_role_name($item->role_id);
                if ($item->login_status == 1) {
                    $item->login_status_text = 'Yes';
                } else {
                    $item->login_status_text = 'No';
                }

                $this->db->where('users_id', $item->id);
                $item->user_meta = $this->db->get('user_meta')->row();
                if ($item->user_meta) {
                    if ($item->user_meta->payroll_status == 1) {
                        $item->user_meta->payroll_status_text = 'Yes';
                    } else {
                        $item->user_meta->payroll_status_text = 'No';
                    }
                    $item->user_meta->dob = convert_date_to_display_format($item->user_meta->dob);
                    if ($item->user_meta->joining_date) {
                        $item->user_meta->joining_date = convert_date_to_display_format($item->user_meta->joining_date);
                    } else {
                        $item->user_meta->joining_date = "";
                    }
                }
            }
            return $data;
        } else {
            return [];
        }
    }

    function get_by_centers_id($centers_id) {
        $this->db->select('*');
        $this->db->from('user_centers');
        $this->db->join('users', 'users.id = user_centers.users_id');
        $this->db->where('users.role_id !=', 4);
        $this->db->where('users.role_id !=', 25);
        $this->db->where('users.status', 1);
        $this->db->where('user_centers.centers_id', $centers_id);
        $this->db->group_by('user_centers.users_id');
        $data = $this->db->get()->result();
        if ($data) {
            return $data;
        } else {
            return [];
        }
    }

    function get_by_corporate_offices_id($corporate_offices_id) {
        $this->db->select('*');
        $this->db->from('user_corporate_offices');
        $this->db->join('users', 'users.id = user_corporate_offices.users_id');
        $this->db->where('users.status', 1);
        $this->db->where('user_corporate_offices.corporate_offices_id', $corporate_offices_id);
        $this->db->group_by('user_corporate_offices.users_id');
        $data = $this->db->get()->result();
        if ($data) {
            return $data;
        } else {
            return [];
        }
    }

    function is_username_exists($username, $id) {
        $this->db->where("status", 1);
        if ($id) {
            $this->db->where("(id!=$id)");
        }
        $this->db->where("username", $username);
        return $this->db->get($this->table_name)->num_rows();
    }

    function is_username_exists_for_adding($username, $id) {
//        $this->db->where("status", 1);
        if ($id) {
            $this->db->where("(id!=$id)");
        }
        $this->db->where("username", $username);
        return $this->db->get($this->table_name)->num_rows();
    }

    function generate_token($length = 20) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $token = '';
        for ($i = 0; $i < $length; $i++) {
            $token .= $characters[rand(0, $charactersLength - 1)];
        }
        if ($this->is_token_exists($token)) {
            $this->generate_token($length);
        } else {
            return $token;
        }
    }

    function is_token_exists($token) {
        $this->db->where("token", $token);
        return $this->db->get($this->table_name)->num_rows();
    }

    function get_user_info($user_id) {
        $this->db->select("id, fullname, status");
        $this->db->where('id', $user_id);
        $data = $this->db->get($this->table_name);
        if ($data->num_rows()) {
            $data = $data->row();
            return $data;
        }
    }

}
