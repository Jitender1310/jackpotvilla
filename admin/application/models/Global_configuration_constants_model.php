<?php

class Global_configuration_constants_model extends CI_Model {

    private $table_name = "global_configuration_constants";

    function update($data) {
        $this->db->trans_begin();
        
        foreach($data as $item){
            $this->db->where("id", $item->id);
            $row = $this->db->get($this->table_name)->row();
            if($row->value!=$item->value){
                $this->db->set("value", $item->value);
                $this->db->where("id", $item->id);
                $this->db->set("updated_at", time());
                $this->db->update($this->table_name);
                if($this->db->affected_rows()){
                    $this->log_model->create_log($this->table_name, __FUNCTION__, $item->id);
                }
            }
        }
        
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function get() {
        $this->db->order_by("display_order", "asc");
        $data = $this->db->get($this->table_name)->result();
        if ($data) {
            foreach ($data as $item) {
                
                if($item->updated_at){
                    $item->updated_at = date(DATE_TIME_HUMAN_READABLE_FORMAT, $item->updated_at);
                }else{
                    $item->updated_at = "N/a";
                }
                
                if (!$item->updated_at) {
                    $item->updated_at = "";
                }
            }
            return $data;
        } else {
            return [];
        }
    }

}
