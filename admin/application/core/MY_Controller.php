<?php

header("access-control-allow-origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Access-Control-Allow-Headers: Content-Type");
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class MY_Controller extends CI_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->data = array();

        $mail_config['protocol'] = 'smtp';
        $mail_config['charset'] = 'utf-8';
        $mail_config['smtp_host'] = SMTP_HOST;
        $mail_config['smtp_user'] = SMTP_USERNAME;
        $mail_config['smtp_pass'] = SMTP_PASSWORD;
        $mail_config['smtp_port'] = 587;

        $mail_config['crlf'] = "\r\n";
        $mail_config['newline'] = "\r\n";
        $mail_config['wordwrap'] = TRUE;
        $mail_config['mailtype'] = 'html';
        $this->email->initialize($mail_config);
    }

    function admin_view($design = null) {
        $this->load->view("includes/header", $this->data);
        $this->load->view($design);
        $this->load->view("includes/footer", $this->data);
    }

    function login_required() {
        if ($this->session->userdata("user_id")) {
            $user_details = $this->user_model->get_user_info($this->session->userdata("user_id"));
            if ($user_details->status != 1) {
                redirect("logout");
            }
            $this->data['name_of_logged_in_person'] = $user_details->fullname;
            return true;
        } else if ($this->input->is_ajax_request()) {
            $arr = ['err_code' => "invalid",
                "error_type" => "login_required",
                "message" => "Session Expired, Please login"
            ];
            echo json_encode($arr);
        } else {
            $this->session->set_flashdata("login_error", "Session Expired, Please login");
            redirect("login");
        }
    }

    function is_logged_in() {
        if ($this->session->userdata("user_id")) {
            return true;
        } else {
            return false;
        }
    }

    function print_pdf_view($design) {
        $this->load->view("includes/print_page_header", $this->data);
        $this->load->view($design);
        $this->load->view("includes/print_page_footer");
    }

}
