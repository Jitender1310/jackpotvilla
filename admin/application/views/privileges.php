<section class="pageWrapper">
   <div class="pageHeader" workspace-offset >
      <div class="row">
         <div class="col-md-12"><strong>Expenses</strong></div>
      </div>
   </div>
   <div class="pageBody" workspace>
      <div class="whitebox">
         <div class="privilege">
            <ul class="head">
               <li>
                  <div class="prive_row">
                     &nbsp;
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span> All
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span> Delete
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span> Edit
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span> View
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span> Add
                        </label>
                     </div>
                  </div>
               </li>
            </ul>
            <ul>
               <li>
                  <div class="prive_row">
                     first level 1 
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                  </div>
                  <ul>
                     <li>
                        <div class="prive_row">
                           second level 1
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                        </div>
                        <ul>
                           <li>
                              <div class="prive_row">
                                 third level 1
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="prive_row">
                                 third level 2
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="prive_row">
                                 third level 3
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                              </div>
                              <ul>
                                 <li>
                                    <div class="prive_row">
                                       fourth level 1
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                    </div>
                                 </li>
                                 <li>
                                    <div class="prive_row">
                                       fourth level 2
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                    </div>
                                 </li>
                                 <li>
                                    <div class="prive_row">
                                       fourth level 3
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                    </div>
                                 </li>
                                 <li>
                                    <div class="prive_row">
                                       fourth level 4
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                    </div>
                                 </li>
                                 <li>
                                    <div class="prive_row">
                                       fourth level 5
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                       <div class="checkbox-inline">
                                          <label>
                                          <input type="checkbox" value=""><span></span>
                                          </label>
                                       </div>
                                    </div>
                                 </li>
                              </ul>
                           </li>
                           <li>
                              <div class="prive_row">
                                 third level 4
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="prive_row">
                                 third level 5
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                                 <div class="checkbox-inline">
                                    <label>
                                    <input type="checkbox" value=""><span></span>
                                    </label>
                                 </div>
                              </div>
                           </li>
                        </ul>
                     </li>
                     <li>
                        <div class="prive_row">
                           second level 2
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                        </div>
                     </li>
                     <li>
                        <div class="prive_row">
                           second level 3
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                        </div>
                     </li>
                     <li>
                        <div class="prive_row">
                           second level 4
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                        </div>
                     </li>
                     <li>
                        <div class="prive_row">
                           second level 5
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                           <div class="checkbox-inline">
                              <label>
                              <input type="checkbox" value=""><span></span>
                              </label>
                           </div>
                        </div>
                     </li>
                  </ul>
               </li>
               <li>
                  <div class="prive_row">
                     first level 2
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                  </div>
               </li>
               <li>
                  <div class="prive_row">
                     first level 3
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                  </div>
               </li>
               <li>
                  <div class="prive_row">
                     first level 4
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                  </div>
               </li>
               <li>
                  <div class="prive_row">
                     first level 5
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                  </div>
               </li>
               <li>
                  <div class="prive_row">
                     first level 6
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                     <div class="checkbox-inline">
                        <label>
                        <input type="checkbox" value=""><span></span>
                        </label>
                     </div>
                  </div>
               </li>
            </ul>
         </div>
      </div>
   </div>
   <div class="pageFooter" workspace-offset>&copy; Freshup Desk - 2017</div>
</section>