<section class="pageWrapper" ng-controller="customersCtrl">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-12"><strong>Customers</strong></div>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="GetCustomersList()">
        <div class="whitebox">
            <div class="row">
                <div class="col-md-9">
                    <form class="" ng-submit="GetCustomersList(filter.page = 1)">
                        <div class="form-group col-md-5">
                            <input type="text" class="form-control"
                                   placeholder="Mobile number"
                                   name="ref_id" title="Enter Mobile number"
                                   ng-model="filter.search_key" style="width:100%">
                        </div>
                        <button type="submit" class="btn btn-default">Search</button>
                    </form>
                </div>
                <div class="col-md-3">
                    <div class="custom-input" title="Sory By">
                        <select class="form-control" ng-model="filter.sort_by" ng-change="GetCustomersList()">
                            <option value="" selected>Sort by</option>
                            <option value="Latest">Latest First</option>
                            <option value="Oldest">Oldest First</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120" class="no-sort">S.No</th>
                        <th class="no-sort">Customer Name</th>
                        <th class="no-sort">Mobile </th>
                        <th class="no-sort">Email</th>
                        <th class="no-sort">Registered Date</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in customersList track by $index">
                        <td data-label="S.No">
                            {{($index + customersPagination.initial_id)}}
                        </td>
                        <td data-label="Customer Name">{{item.fullname}}</td>
                        <td data-label="Mobile No">{{item.mobile}}</td>
                        <td data-label="Email">{{item.customer_meta.email}}</td>
                        <td data-label="Reg Date">{{item.created_at}}</td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <!--<li ng-click="EditAgent(item)"><a href="#">Edit</a></li>-->
                                    <li ng-click="ViewCustomerInfo(item)"><a href="#">View Customer Info</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <span class="pull-right" ng-bind-html="customersPagination.pagination"></span>
    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk 2018</div>
    <div class="modal fade" id="customer_view_modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Customer Details</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <tbody>
                                <tr>
                                    <th>Customer Name</th>
                                    <td>{{customerObj.fullname}}</td>
                                </tr>
                                <tr>
                                    <th>Mobile</th>
                                    <td>{{customerObj.mobile}}</td>
                                </tr>
                                <tr>
                                    <th>Email(s)</th>
                                    <td>{{customerObj.customer_meta.email}}</td>
                                </tr>
                                <tr>
                                    <th>Alternate Mobile</th>
                                    <td>{{customerObj.customer_meta.alternative_mobile}}</td>
                                </tr>
                                <tr>
                                    <th>Door No</th>
                                    <td>{{customerObj.customer_meta.door_no}}</td>
                                </tr>
                                <tr>
                                    <th>Street</th>
                                    <td>{{customerObj.customer_meta.street_name}}</td>
                                </tr>
                                <tr>
                                    <th>Location</th>
                                    <td>{{customerObj.customer_meta.location_name}}</td>
                                </tr>
                                <tr>
                                    <th>City</th>
                                    <td>{{customerObj.customer_meta.city_name}}</td>
                                </tr>
                                <tr>
                                    <th>District</th>
                                    <td>{{customerObj.customer_meta.district_name}}</td>
                                </tr>
                                <tr>
                                    <th>State</th>
                                    <td>{{customerObj.customer_meta.state_name}}</td>
                                </tr>
                                <tr>
                                    <th>Zipcode/Postal Code/Pincode</th>
                                    <td>{{customerObj.customer_meta.zipcode}}</td>
                                </tr>
                                <tr>
                                    <th>Country</th>
                                    <td>{{customerObj.customer_meta.country_name}}</td>
                                </tr>
                                <tr>
                                    <th>Registered Date</th>
                                    <td>{{customerObj.created_at}}</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/customersCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/customersService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/centersService.js?r=<?= time() ?>"></script>