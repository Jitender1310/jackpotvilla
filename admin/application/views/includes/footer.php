<script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>jquery-ui.min.js"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>bootstrap.min.js"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>jquery-ui-timepicker-addon.min.js"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>jquery.duplicate.min.js"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>jquery.rateyo.min.js"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>SmoothScroll.min.js"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>raphael-min.js"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>morris.min.js"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>additional-methods.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>


<script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>custom.js?i=<?php echo time(); ?>"></script>

<!--        <link rel="stylesheet" href="http://mbenford.github.io/ngTagsInput/css/ng-tags-input.min.css" /> -->
<script src="//mbenford.github.io/ngTagsInput/js/ng-tags-input.min.js"></script>

<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>lib/angular-cookies.min.js"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>lib/angular-sanitize.min.js"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>lib/angular-datatables.min.js"></script>
<script>
    $("input.numbers_only").keypress(function (event) {
        return /\d/.test(String.fromCharCode(event.keyCode));
    });
    $("input.numbers_decimals_only").keypress(function (evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
            return false;

        return true;
    });
</script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/notificationsService.js?r=<?= time() ?>"></script>
</body>
</html>