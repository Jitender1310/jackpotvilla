<table width="100%" cellspacing="0" cellpadding="0" style=" font-family: sans-serif; font-size: 13px; color: #676767;">
    <tr>
        <td align="center">
            <img src="<?= STATIC_ADMIN_IMAGES_PATH ?>logo-for-pdf.jpg" style="width:270px; height:auto">
        </td>
    </tr>
</table>

<table  style="width: 100%; margin-bottom: 10px; margin-top: 10px;" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2" style="padding:10px 20px; background: #eee; vertical-align: top;border: 1px solid #ccc; font-size: 16px; font-weight: bold; text-align: center;">Booking ID : #<?= $reserverationObj->booking_ref_number ?></td>
    </tr>
    <tr>
        <td width="50%" style="padding:20px;vertical-align: middle;border: 1px solid #ccc; ">
            <h4 style="font-weight: bold; font-size: 16px; margin: 0px; margin-bottom: 10px; letter-spacing: -0.0400em;">Service Provider</h4>
            <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;">Freshup - <?= $reserverationObj->center_info[0]->center_name ?></h5></p>
            <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;">Address:</h5> <?= $reserverationObj->center_info[0]->address ?></p>
            <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;">Mobile:</h5> <?= $reserverationObj->center_info[0]->mobiles ?> </p>
            <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;">GSTIN:</h5> <?= $reserverationObj->center_gst_number ?> </p>
        </td>
        <td width="50%" style="padding:20px; vertical-align: top;border: 1px solid #ccc;">
            <h4 style="font-weight: bold; font-size: 16px; margin: 0px; margin-bottom: 10px; letter-spacing: -0.0400em;">Bill To</h4>

            <?php if ($reserverationObj->billing_to == "Organization") { ?>
                <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;"><?= $reserverationObj->company_name ?></h5></p>
                <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;"><?= $reserverationObj->contact_name ?></h5></p>
            <?php } else if ($reserverationObj->billing_to == "Others") { ?>
                <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;"><?= $reserverationObj->billing_to_others_text ?></h5></p>
                <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;"><?= $reserverationObj->contact_name ?></h5></p>
            <?php } else if ($reserverationObj->billing_to == "Customer") { ?>

                <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;"><?= $reserverationObj->contact_name ?></h5></p>
            <?php } ?>
            <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;">Address:</h5>
            <br/>

            <?= $reserverationObj->door_no ? $reserverationObj->door_no . "," : '' ?>
            <?= $reserverationObj->street_name ? $reserverationObj->street_name . "," : '' ?>
            <?= $reserverationObj->location ? $reserverationObj->location . "," : '' ?>
            <?= $reserverationObj->city ? $reserverationObj->city . "," : '' ?>
            <?= $reserverationObj->district ? $reserverationObj->district . "," : '' ?>
            <?= $reserverationObj->state ? $reserverationObj->state . "," : '' ?>
            <?= $reserverationObj->zipcode ? $reserverationObj->zipcode . "," : '' ?>
            <?= $reserverationObj->country_name ? $reserverationObj->country_name . "," : '' ?>

            </p>
            <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;">Email:</h5> <?= $reserverationObj->contact_email ?> </p>
            <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;">Phone:</h5> <?= $reserverationObj->contact_mobile ?> </p>
            <?php if ($reserverationObj->payer_gst_number != '') { ?>
                <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;">GSTIN:</h5> <?= $reserverationObj->payer_gst_number ?> </p>
            <?php } ?>

            <?php if ($reserverationObj->ticket_type) { ?>
                <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;">Ticket Type:</h5> <?= $reserverationObj->ticket_type ?> </p>
                <?php if ($reserverationObj->ticket_number) { ?>
                    <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;">Ticket Number:</h5> <?= $reserverationObj->ticket_number ?> </p>
                <?php } ?>
            <?php } ?>
        </td>
    </tr>
</table>


<table  style="width: 100%; margin-bottom: 10px;" cellpadding="0" cellspacing="0">
    <tbody>
        <tr>
            <td style="width: 33.3%; border: 1px solid #ccc; padding: 20px; vertical-align: top;">
                <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;">Check In Date and Time :</h5></p>
                <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;"><?= $reserverationObj->check_in_date ?> <br /> <?= strtoupper($reserverationObj->check_in_time) ?></p>

            </td>
            <td style="width: 33.3%; border: 1px solid #ccc; padding: 20px; vertical-align: top;">
                <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;">Check Out Date and Time :</h5></p>
                <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;"><?= $reserverationObj->check_out_date ?> <br /> <?= strtoupper($reserverationObj->check_out_time) ?></p>
            </td>
            <td style="width: 33.3%; border: 1px solid #ccc; padding: 20px; vertical-align: top;">
                <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;">Total Duration :</h5> <?= $reserverationObj->duration ?> <?= $reserverationObj->duration_type_text ?></p>
                <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;">Payment Status :</h5> <?= $reserverationObj->payment_status ?></p>
                <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;">Booking Status :</h5> <?= $reserverationObj->booking_status ?></p>
                <?php /* <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;">Booking By :</h5> <?= ucfirst($reserverationObj->creation_source) ?></p> */ ?>
            </td>
        </tr>
    </tbody>
</table>

<table  style="width: 100%; margin-bottom: 10px;" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th style="padding: 5px 10px; border: 1px solid #ccc; background: #eee;">For</th>
            <th style="padding: 5px 10px; border: 1px solid #ccc; background: #eee;">Name</th>
            <th style="padding: 5px 10px; border: 1px solid #ccc; background: #eee;">Gender (age)</th>
            <th style="padding: 5px 10px; border: 1px solid #ccc; background: #eee;">Identity <br/> Type & Number</th>
            <th style="padding: 5px 10px; border: 1px solid #ccc; background: #eee;">Status</th>
            <th style="padding: 5px 10px; border: 1px solid #ccc; background: #eee; text-align: right;">Total Price</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($reserverationObj->booking_items as $item) {
            foreach ($item->items as $sub_item) {
                ?>
                <tr>
                    <td style="padding: 5px 10px; border: 1px solid #ccc;">
                        <?= $item->stay_category_name ?>
                        <?php if ($sub_item->inventory_code) { ?>Alloted SKU : <?= $sub_item->inventory_code ?><?php } ?>
                    </td>
                    <td style="padding: 5px 10px; border: 1px solid #ccc;"><?= $sub_item->person_name ?></td>
                    <td style="padding: 5px 10px; border: 1px solid #ccc;"><?= $sub_item->gender ?> (<?= $sub_item->age ? $sub_item->age : 'n/a' ?>)</td>
                    <td style="padding: 5px 10px; border: 1px solid #ccc;"><?= $sub_item->identity_type ?> 
                        <?= $sub_item->identity_number ?>
                    </td>
                    <td style="padding: 5px 10px; border: 1px solid #ccc;">
                        <?php if ($sub_item->reservation_status == "Cancelled") { ?>
                            <?= $sub_item->reservation_status ?>
                            <br/>
                            Refund :  &#8377; <?= $sub_item->refund_amount ?>/-
                            <br/>
                            *Refund date : <br/><?= $sub_item->refunded_at ?>
                        <?php } else { ?>
                            <?= $sub_item->reservation_status ?>
                        <?php } ?>
                    </td>
                    <td style="padding: 5px 10px; border: 1px solid #ccc; text-align: right; font-weight: bold;"><i></i> <?= $sub_item->total_price ?>/-</td>
                </tr>
                <?php
                if (sizeof($sub_item)) {
                    foreach ($sub_item->sub_reservation_items as $sub_sub_item) {
                        ?>
                        <tr>
                            <td style="padding: 5px 10px; border: 1px solid #ccc;">
                                <?= $item->stay_category_name ?>
                                <?php if ($sub_sub_item->inventory_code) { ?>Alloted SKU : <?= $sub_sub_item->inventory_code ?><?php } ?>
                            </td>
                            <td style="padding: 5px 10px; border: 1px solid #ccc;"><?= $sub_sub_item->person_name ?></td>
                            <td style="padding: 5px 10px; border: 1px solid #ccc;"><?= $sub_sub_item->gender ?> (<?= $sub_sub_item->age ? $sub_sub_item->age : 'n/a' ?>)</td>
                            <td style="padding: 5px 10px; border: 1px solid #ccc;"><?= $sub_sub_item->identity_type ?> <br/>
                                <?= $sub_sub_item->identity_number ?> </td>
                            <td style="padding: 5px 10px; border: 1px solid #ccc;"><?= $sub_item->reservation_status ?> </td>
                            <td style="padding: 5px 10px; border: 1px solid #ccc; text-align: right; font-weight: bold;"><i></i>  &#8377; <?= $sub_sub_item->total_price ?>/-</td>
                        </tr>
                        <?php
                    }
                }
            }
        }
        ?>
    </tbody>
    <tfoot>
        <tr>
            <th colspan="5" style="padding: 5px 10px; border: 1px solid #ccc; background: #eee;">Total</th>
            <th style="padding: 5px 10px; border: 1px solid #ccc; background: #eee; text-align: right; font-weight: bold;"><i></i> <?= $reserverationObj->payment_info->amount ?>/-</th>
        </tr>
        <?php /* <tr>
          <td colspan="5" style="padding: 5px 10px; border: 1px solid #ccc; background: #fcfcfc">Referral Discount ( 0)</td>
          <td style="padding: 5px 10px; border: 1px solid #ccc; background: #fcfcfc; text-align: right; font-weight: bold;"><i></i> 0/-</td>
          </tr> */ ?>

        <?php if ($reserverationObj->payment_info->discount_amount > 0.00) { ?>
            <tr>
                <td colspan="5" style="padding: 5px 10px; border: 1px solid #ccc; background: #fcfcfc">Discount (<?= $reserverationObj->payment_info->discount_value ?> <?= $reserverationObj->payment_info->discount_type_symbol ?>)</td>
                <td style="padding: 5px 10px; border: 1px solid #ccc; background: #fcfcfc; text-align: right; font-weight: bold;"><i></i> <?= $reserverationObj->payment_info->discount_amount ?>/-</td>
            </tr>
        <?php } ?>
        <?php
        if (isset($reserverationObj->payment_info->coupon_code)) {
            ?>
            <tr>
                <td colspan="5" style="padding: 5px 10px; border: 1px solid #ccc; background: #fcfcfc">Promo Discount (<?= $reserverationObj->payment_info->coupon_discount_amount ?> <?= $reserverationObj->payment_info->coupon_discount_type_symbol ?>)</td>
                <td style="padding: 5px 10px; border: 1px solid #ccc; background: #fcfcfc; text-align: right; font-weight: bold;"><i></i> <?= $reserverationObj->payment_info->applied_coupon_discount_amount ?>/-</td>
            </tr>
            <?php
        }
        ?>

        <tr>
            <th colspan="5" style="padding: 5px 10px; border: 1px solid #ccc; background: #eee;">Sub Total</th>
            <th style="padding: 5px 10px; border: 1px solid #ccc; background: #eee; text-align: right; font-weight: bold;"><i></i> <?= $reserverationObj->payment_info->sub_total ?>/-</th>
        </tr>

        <?php foreach ($reserverationObj->payment_info->applied_taxes as $tax_item) { ?>
            <tr>
                <td colspan="5" style="padding: 5px 10px; border: 1px solid #ccc; background: #fcfcfc"><?= $tax_item->tax_name ?> ( <?= $tax_item->tax_amount ?> <?= $tax_item->tax_calculation_type == 'Percentage' ? '%' : 'Rs' ?>)</td>
                <td style="padding: 5px 10px; border: 1px solid #ccc; background: #fcfcfc; text-align: right; font-weight: bold;"><i></i>  <?= $tax_item->applied_tax_amount ?>/-</td>
            </tr>
        <?php } ?>

        <tr>
            <th colspan="5" style="padding: 5px 10px; border: 1px solid #ccc; background: #eee; font-size: 16px;">Grand Total</th>
            <th style="padding: 5px 10px; border: 1px solid #ccc; background: #eee;; text-align: right; font-weight: bold; font-size: 16px;"><i></i> <?= number_format($reserverationObj->payment_info->grand_total, 2) ?>/-</th>
        </tr>
        <?php if ($reserverationObj->payment_status == "Refund") { ?>
            <tr>
                <th colspan="1" style="padding: 5px 10px; border: 1px solid #ccc; background: #eee; font-size: 14px;">Money Refunded</th>
                <?php if ($reserverationObj->refunded_amount) { ?>
                    <td colspan="5" style="padding: 5px 10px; border: 1px solid #ccc; background: #eee; text-align: right;">Rs. <?= $reserverationObj->refunded_amount ?> has been refunded to your bank on <?= $reserverationObj->refunded_at ?>. Your bank may take 7-14 working days to credit balance in your account.</td>
                <?php } else { ?>
                    <td colspan="5" style="padding: 5px 10px; border: 1px solid #ccc; background: #eee; text-align: right;">Booking has been cancelled on <?= $reserverationObj->refunded_at ?> and refund is not applicable on this cancellation.</td>
                <?php } ?>
            </tr>
        <?php } ?>
        <tr>
            <td colspan="5">
                <small>Note: *All refunded amount to Your bank may take 7-14 working days to credit balance in your account.</small>
            </td>
        </tr>
    </tfoot>
</table>
<table>
    <tr>
        <td style="font-size: 10px;">

            <h5 style="text-align: center; border-bottom: 1px solid #ccc; padding-bottom: 10px; margin-top: 10px;">Terms and Conditions</h5>
            <p> <h5 style="display: inline-block; font-weight: bold; margin:0px; font-size:13px;"><em>Warm Welcome from Fresh Up</em></h5></p>
            <p>The customer hereby unconditionally accepts to the General terms and conditions, Refund &amp; Cancellation policies of Fresh Up.</p>
            <p>If the payment is made other than cash, the same is construed to be outstanding unless and until the it is cleared and mere issuance of this bill would not amount to acknowledgement of the amount by Fresh Up.</p>
            <p>The Customer is expected to check in / check out on or before the stipulated time with a grace time of 15 minutes. In case of failure to adhere the same, Fresh Up is entitled to charge the appropriate amount for such extended stay/timing or forfeit the amount so paid for such delayed check in.</p>
            <p>The Customer will not be provided with any sort of refund or accommodation on the given date or time due to the delayed check in beyond the stipulated time.</p>
            <p>The customer is requested to take rest in their designated places without causing any hindrance or trouble to the other customers or damage their belongings.</p>
            <p>The customer is requested to keep the designated places clean and tidy to the best of their efforts except to normal wear and tear.</p>
            <p>The customer is responsible to any damage caused to the properties of Fresh Up and suitable amount would be deducted from the deposit amount and the balance alone would be refunded at the time of vacating the premises.</p>
            <p>The customer is strictly prohibited from smoking, consuming alcohol or any prohibited drugs within the premises.</p>
            <p>The customer will not be allowed inside the premises if he/she is found to be under the influence of alcohol/any prohibited drugs despite having paid the amount in full.</p>
            <p>The customer is requested to take care of their belongings and Fresh Up will not be liable or responsible for loss or damage of the same. The customer is requested to make use of lockers to place their belongings safely.</p>
            <p>Any dispute or difference between the Customer &amp; Fresh Up shall be all be referred to arbitration. The arbitration proceedings shall be carried out by a sole arbitrator in accordance with the rules and regulations under the Arbitration and Conciliation Act, 1996. The arbitration shall be conducted at Chennai in English language</p>
            <h5 style="text-align: center; border-top: 1px solid #ccc; padding-top: 10px; margin-top: 10px;">Thank you for staying with us...have a wonderful day</h5>

        </td>
    </tr>
</table>
