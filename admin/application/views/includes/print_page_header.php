<!DOCTYPE html>
<html lang="en" ng-app="frontDesk" ng-cloak>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= SITE_TITLE ?></title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="<?= STATIC_ADMIN_CSS_PATH ?>bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?= STATIC_ADMIN_CSS_PATH ?>jquery.rateyo.min.css">
        <link rel="stylesheet" type="text/css" href="<?= STATIC_ADMIN_ASSETS_PATH ?>fonts/fontawesome/css/fontawesome-all.min.css">
        <link rel="stylesheet" type="text/css" href="<?= STATIC_ADMIN_CSS_PATH ?>morris.css">
        <link rel="stylesheet" type="text/css" href="<?= STATIC_ADMIN_ASSETS_PATH ?>fonts/ProximaNova-Regular/ProximaNova-Regular.css">
        <link rel="stylesheet" type="text/css" href="<?= STATIC_ADMIN_ASSETS_PATH ?>fonts/ProximaNova-Bold/ProximaNova-Bold.css">
        <link rel="stylesheet" type="text/css" href="<?= STATIC_ADMIN_ASSETS_PATH ?>fonts/ProximaNova-Semibold/ProximaNova-Semibold.css">
        <!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="<?= STATIC_ADMIN_CSS_PATH ?>jquery-ui-timepicker-addon.min.css"> -->
        <link rel="stylesheet" type="text/css" href="<?= STATIC_ADMIN_CSS_PATH ?>style.css?i=<?php echo time(); ?>">
        <style>
            body{
                overflow: inherit;
                background: #ffffff;
                color:#32353a;
            }
        </style>
    </head>
    <body>