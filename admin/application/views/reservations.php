<section class="pageWrapper">
    <div class="pageHeader" workspace-offset  valign-parent>
        <div class="row">
            <div class="col-md-6"><strong>Reservations</strong></div>
            <div class="col-md-6 text-right">
                <div valign-holder>



                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>

        <div class="row rm-5">
            <div class="col-md-12 cp-5"> 

                <div class="row rm-5">
                    <div class="col-md-3 cp-5">
                        <div class="bg-success text-white py-20-xs px-20-xs mb-10-xs text-center"><div>Available</div><div class="h3 mb-0-xs">20</div></div>
                    </div>
                    <div class="col-md-3 cp-5">
                        <div class="bg-danger text-white py-20-xs px-20-xs mb-10-xs text-center"><div>Occupied</div><div class="h3 mb-0-xs">20</div></div>
                    </div>
                    <div class="col-md-3 cp-5">
                        <div class="bg-warning text-white py-20-xs px-20-xs mb-10-xs text-center"><div>Maintenance</div><div class="h3 mb-0-xs">20</div></div>
                    </div>
                    <div class="col-md-3 cp-5">
                        <div class="bg-info text-white py-20-xs px-20-xs mb-10-xs text-center"><div>Housekeeping</div><div class="h3 mb-0-xs">20</div></div>
                    </div>
                </div>

            </div>
            <div class="col-md-6 cp-5"> <div class="whitebox mb-10-xs">
                    <div class="whiteboxHead">
                        <strong>Today Accomidation Sales</strong>
                    </div>
                    <div valign-parent>
                        <div class="row">
                            <div class="col-md-8">
                                <div id="today-accomidation-sales"></div>
                            </div>
                            <div class="col-md-4">
                                <ul class="ul-block" valign-holder>
                                    <li><i class="fas fa-circle text-warning"></i> Cash <i class="fal fa-rupee-sign ml-10-xs"></i><b> 100</b></li>
                                    <li><i class="fas fa-circle text-info"></i> Card <i class="fal fa-rupee-sign ml-10-xs"></i><b> 25</b></li>
                                    <li><i class="fas fa-circle text-success"></i> Paytm <i class="fal fa-rupee-sign ml-10-xs"></i><b> 25</b></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div></div>
            <div class="col-md-6 cp-5"> <div class="whitebox mb-10-xs">
                    <div class="whiteboxHead">
                        <strong>Today F&B Sales</strong>
                    </div>
                    <div valign-parent>
                        <div class="row">
                            <div class="col-md-8">
                                <div id="today-fnb-sales"></div>
                            </div>
                            <div class="col-md-4">
                                <ul class="ul-block" valign-holder>
                                    <li><i class="fas fa-circle text-warning"></i> Cash <i class="fal fa-rupee-sign ml-10-xs"></i><b> 25</b></li>
                                    <li><i class="fas fa-circle text-info"></i> Card <i class="fal fa-rupee-sign ml-10-xs"></i><b> 100</b></li>
                                    <li><i class="fas fa-circle text-success"></i> Paytm <i class="fal fa-rupee-sign ml-10-xs"></i><b> 25</b></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div></div>
        </div>

    </div>

    <?php $this->load->view('includes/reservations_menu'); ?>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk - 2017</div>
</section>