<section class="pageWrapper sidebarRight">

    <div class="pageHeader" workspace-offset >

        <div class="row">

            <div class="col-md-6"><strong>Reservation List</strong></div>

            <div class="col-md-6">

                <ul class="actions ul">

                    <!-- <li><a href="">Graphical View</a></li>  -->

                </ul>

            </div>

        </div>

    </div>

    <div class="pageBody" workspace>



        <div class="whitebox mb-10-xs">

            <div class="row">

                <div class="col-md-4">

                    <div class="form-horizontal">

                        <div class="form-group mb-10-xs">

                            <label class="col-sm-3 control-label">Search by</label>

                            <div class="col-sm-9">

                                <div class="row rm-5">

                                    <div class="col-md-6 cp-5">

                                        <div class="custom-input">

                                            <select class="form-control input-sm">

                                                <option>All</option>

                                            </select>

                                            <div class="ci-icon"><i class="fal fa-chevron-down"></i></div>

                                        </div>

                                    </div>

                                    <div class="col-md-6 cp-5">

                                        <input type="email" class="form-control input-sm" placeholder="Res,Guest">

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="form-group mb-10-xs">

                            <label class="col-sm-3 control-label">Booking Type</label>

                            <div class="col-sm-9">

                                <div class="custom-input">

                                    <select class="form-control input-sm">

                                        <option>All</option>

                                    </select>

                                    <div class="ci-icon"><i class="fal fa-chevron-down"></i></div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="col-md-4">

                    <div class="form-horizontal">

                        <div class="form-group mb-10-xs">

                            <label class="col-sm-3 control-label">Arrival</label>

                            <div class="col-sm-9">

                                <div class="row rm-5">

                                    <div class="col-md-6 cp-5">

                                        <div class="custom-input">

                                            <input type="text" class="form-control input-sm" placeholder="dd/mm/yy">

                                            <div class="ci-icon"><i class="fal fa-calendar"></i></div>

                                        </div>

                                    </div>

                                    <div class="col-md-6 cp-5">

                                        <div class="custom-input">

                                            <input type="text" class="form-control input-sm" placeholder="dd/mm/yy">

                                            <div class="ci-icon"><i class="fal fa-calendar"></i></div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="form-group mb-10-xs">

                            <label class="col-sm-3 control-label">Type</label>

                            <div class="col-sm-9">

                                <div class="row rm-5">

                                    <div class="col-md-6 cp-5">

                                        <div class="custom-input">

                                            <select class="form-control input-sm">

                                                <option>Select</option>

                                            </select>

                                            <div class="ci-icon"><i class="fal fa-chevron-down"></i></div>

                                        </div>

                                    </div>

                                    <div class="col-md-6 cp-5">

                                        <div class="custom-input">

                                            <select class="form-control input-sm">

                                                <option>PID</option>

                                            </select>

                                            <div class="ci-icon"><i class="fal fa-chevron-down"></i></div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="col-md-4">

                    <div class="form-horizontal">

                        <div class="form-group mb-10-xs">

                            <label class="col-sm-3 control-label">Res. Date</label>

                            <div class="col-sm-9">

                                <div class="row rm-5">

                                    <div class="col-md-6 cp-5">

                                        <div class="custom-input">

                                            <input type="text" class="form-control input-sm" placeholder="dd/mm/yy">

                                            <div class="ci-icon"><i class="fal fa-calendar"></i></div>

                                        </div>

                                    </div>

                                    <div class="col-md-6 cp-5">

                                        <div class="custom-input">

                                            <input type="text" class="form-control input-sm" placeholder="dd/mm/yy">

                                            <div class="ci-icon"><i class="fal fa-calendar"></i></div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="form-group mb-10-xs">

                            <label class="col-sm-3 control-label">Status</label>

                            <div class="col-sm-9">

                                <div class="row rm-5">

                                    <div class="col-md-6 cp-5">

                                        <div class="custom-input">

                                            <select class="form-control input-sm">

                                                <option>All</option>

                                            </select>

                                            <div class="ci-icon"><i class="fal fa-chevron-down"></i></div>

                                        </div>

                                    </div>

                                    <div class="col-md-6 cp-5">

                                        <div class="custom-input">

                                            <select class="form-control input-sm">

                                                <option>Company</option>

                                            </select>

                                            <div class="ci-icon"><i class="fal fa-chevron-down"></i></div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="col-md-8 ">

                    <ul class="ul-inline">

                        <li><i class="fal fa-circle color-confirmed"></i>Confirmed</li>

                        <li><i class="fal fa-circle color-guest"></i>Guest Check-in</li>

                        <li><i class="fal fa-circle color-completed"></i>Completed</li>

                        <li><i class="fal fa-circle color-cancelled"></i>Cancelled</li>

                        <li><i class="fal fa-circle color-unconfirmed"></i>Un Confirmed</li>

                    </ul>

                </div>

                <div class="col-md-4 text-right">

                    <button type="reset" class="btn btn-default">Reset <i class="fal fa-sync"></i></button>

                    <button type="button" class="btn btn-primary">Search <i class="fal fa-search"></i></button>

                </div>

            </div>

        </div>



        <table class="table data-table">

            <thead>

                <tr>

                    <th>Res</th>

                    <th>Arrival</th>

                    <th>Depature</th>

                    <th>Guest Name</th>

                    <th>Type</th>

                    <th>PID</th>

                    <th>Company</th>

                    <th>Total</th>

                    <th>Deposit</th>

                    <th>User</th>

                    <th class="no-sort text-right">Action</th>

                </tr>

            </thead>

            <tbody>

                <?php for ($i = 0; $i < 100; $i++) { ?>



                    <tr>

                        <td <?php
                        if ($i == 0) {
                            echo 'class="border-unconfirmed"';
                        } else if ($i == 1) {
                            echo 'class="border-completed"';
                        } else if ($i == 2) {
                            echo 'class="border-cancelled"';
                        } else if ($i == 4) {
                            echo 'class="border-guest"';
                        } else {
                            echo 'class="border-confirmed"';
                        }
                        ?>><?php echo $i; ?></td>

                        <td>

                            Date : 30-12-2017<br/>

                            Time : 10:46 AM

                        </td>

                        <td> Date : 30-12-2017<br/>

                            Time : 10:46 AM</td>

                        <td>Mrs. Pavani</td>

                        <td>Room</td>

                        <td>132</td>

                        <td>N/A</td>

                        <td>100.00</td>

                        <td>300.00</td>

                        <td>Admin</td>

                        <td class="text-right"><a href="#modal-view" data-toggle="modal" class="btn btn-sm btn-default"><i class="fal fa-ellipsis-v"></i></a></td>

                    </tr>





<?php } ?>

            </tbody>

        </table>



    </div>

    <div class="pageFooter" workspace-offset>&copy; Freshup Desk - 2017</div>

    <div class="pageSidebar">

        <h5 class="mt-0-xs">Recent Booking Log</h5>



        <div class="logBlock">

            <p><small>01/01/2017</small><br/> Lorem ipsum dolor sit  sed do eiusmod

                tempor incididunt ut labore et dolore magna aliqua. </p>

        </div>



        <div class="logBlock">

            <p><small>01/01/2017</small><br/> Lorem ipsum dolor sit  sed do eiusmod

                tempor incididunt ut labore et dolore magna aliqua. </p>

        </div>

        <div class="logBlock">

            <p><small>01/01/2017</small><br/> Lorem ipsum dolor sit  sed do eiusmod

                tempor incididunt ut labore et dolore magna aliqua. </p>

        </div>









    </div>



</section>



<div class="modal fade" id="modal-view">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Reservation Info</h4>

            </div>

            <div class="modal-body">



            </div>

            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>



            </div>

        </div>

    </div>

</div>