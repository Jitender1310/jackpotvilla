<section class="pageWrapper">
    <div class="pageHeader" workspace-offset  valign-parent>
        <div class="row">
            <div class="col-md-3"><strong>Lead id: 5412e214  <span class="label label-success ml-10-xs">Confirmed</span></strong></div>
            <div class="col-md-9 text-right hidden-xs">
                <div valign-holder>
                 <button class="btn btn-default btn-sm"><i class="fal fa-arrow-left"></i> Back</button>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>

      <div class="row rm-5" same-height-row>
        <div class="col-md-6 cp-5">
          <div class="whitebox" same-height-col>
            <div class="whiteboxHead"><strong>Lead Overview</strong></div>

         <table class="table table-custom table-custom-px-0">
           <tr>
             <td class="brt-0 font-bold">Created Date & Time</td>
             <td class="brt-0">01/2017 3:30 PM</td>
           </tr>
               <tr>
             <td class="font-bold">Lead Id</td>
             <td>5412e214</td>
           </tr>
           <tr>
             <td class="font-bold">Name</td>
             <td>subbu</td>
           </tr>
              <tr>
             <td class="font-bold">Mobile</td>
             <td>9676969175</td>
           </tr>
            <tr>
             <td class="font-bold">Location</td>
             <td>Hyderabad</td>
           </tr>
                <tr>
             <td class="font-bold">Exp Check-in Date</td>
             <td> 01/04/2018</td>
           </tr>
                   <tr>
             <td class="font-bold">Pax</td>
             <td>2</td>
           </tr>
            <tr>
             <td class="font-bold">Intrest In</td>
             <td>Bunker Beds,Wing chair</td>
           </tr>
               <tr>
             <td class="font-bold">Agent</td>
             <td>Ravikumar</td>
           </tr>
         </table>

         <div class="row rm-5">
         <div class="col-md-6 cp-5">       <div class="form-group">
                                 <label>Change Lead Status</label>
                                    <div class="custom-input">
                                                     <select class="form-control" id="leadtype">
                                                         <option>Select</option>
         <option value="1">Followup</option>
         <option value="2">Convert to Booking</option>
         <option value="3">Not Intrested</option>
         
         
                                                     </select>
                                                     <div class="ci-icon"><i class="fal fa-chevron-down"></i></div>
                                                 </div>
                             </div></div>
                    <div class="col-md-6 cp-5 leadtypediv hidden">    <div class="form-group">
                                            <label>Date</label>
                                            <input type="text" class="form-control datepicker">
                                        </div></div>
                  <div class="col-md-12 cp-5 leadtypediv hidden">       <div class="form-group">
                                          <label>Remarks</label>
                                          <textarea class="form-control" rows="6"></textarea>
                                      </div></div>
                    <div class="col-md-12 cp-5">
                      <button type="button" class="btn btn-primary">Update <i class="fal fa-arrow-right"></i></button>
                    </div>
         </div>
            
          </div>
        </div>
          <div class="col-md-6 cp-5">
          <div class="whitebox" same-height-col>
            <div class="whiteboxHead"><strong>Lead Log</strong></div>

    <div class="logBlockScrollY">        <div class="logBlock">
                <p><small>01/01/2017 3:30 PM - Created</small><br> Lorem ipsum dolor sit  sed do eiusmod
                   tempor incididunt ut labore et dolore magna aliqua. 
                </p>
             </div>

                 <div class="logBlock">
                <p><small>01/01/2017 3:30 PM - Followup</small></p>
                <p><span class="label label-warning">Next Followup Date & Time 01/2017 3:30 PM</span></p>
                <p><strong>Remarks: </strong>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. 
                </p>
             </div>
                 <div class="logBlock">
                <p><small>01/01/2017 3:30 PM - Followup</small></p>
                <p><span class="label label-warning">Next Followup Date & Time 01/2017 3:30 PM</span></p>
                <p><strong>Remarks: </strong>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. 
                </p>
             </div>
    
        
    
             <div class="logBlock">
                <p><small>01/01/2017 3:30 PM - Lead Closed</small></p>
                <p><span class="label label-success">Successfully Lead converted</span></p>
                <p><strong>Booking Info: </strong> </p>
               <p>Booking Id : 5412e214</p>
               <p>Total Amount : 2000:00</p>
               <p>
                 <a href="reservation_view" class="btn btn-default">Click  to See Booking Info</a>
               </p>
               
             </div>
         </div>

          </div>
        </div>
      </div>
      
    </div>
    <?php $this->load->view('admin/includes/leads_menu'); ?>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk - 2017</div>
</section>

<script type="text/javascript">
  $("#leadtype").on("change",function(){

    if ($(this).val() == '1') {

      $(".leadtypediv").removeClass("hidden");

    }else{
      $(".leadtypediv").addClass("hidden");
    }

    setTimeout(function(){
       freshupdesk.sameHeight();
    },1000);



  });
</script>