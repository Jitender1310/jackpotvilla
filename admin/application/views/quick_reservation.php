<style>
    .ng-invalid.error{
        border: 1px solid red;
    }
    .dynamic-search-result{
        z-index: 999999999999999999!important;
    }
</style>
<section class="pageWrapper" ng-controller="quickReservationCtrl" ng-init="GetCentersList('<?= get_user_id() ?>'); user_role_id =<?= get_user_role_id() ?>; minimum_payment_percentage =<?= MINIMUM_PAYMENT_PERCENT ?>">
    <div class="pageHeader" workspace-offset valign-parent>
        <div class="row">
            <div class="col-md-6">
                <?php if ($is_past_reservation) { ?>
                    <strong>Old Quick Reservation</strong>
                <?php } else { ?>
                    <strong>New Quick Reservation</strong>
                <?php } ?>
            </div>
            <div class="col-md-6">
                <div valign-holder>
                    <div class="row">
                        <div class="col-md-4 pull-right">
                            <div class="custom-input" title="My Center">
                                <select class="form-control input-sm" ng-model="reservationObj.centers_id" ng-disabled="!centersList.length > 0" ng-change="GetTaxSlabsList(); CheckIsIRCTCCenter(); UpdateCenterSession('<?= get_user_id() ?>', reservationObj.centers_id)">
                                    <option value="" selected disabled="">Choose Center</option>
                                    <option ng-value="item.id" value="item.id" ng-repeat="item in centersList">{{item.center_name}}</option>
                                </select>
                                <span class="ci-icon">
                                    <i ng-show="centersSpinner" class="fal fa-circle-notch fa-spin"></i>
                                    <i ng-show="!centersSpinner" class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="pageBody slimScroll" workspace ng-show="reservationObj.centers_id">
        <form ng-submit="SubmitReservationForm()" id="createReservationForm" ng-keyup="resetFormValidations()" ng-blur="resetFormValidations()" ng-click="resetFormValidations()">
            <?php if ($this->input->get_post('agent_leads_id')) { ?>
                <input type="hidden"  name="agent_leads_id" value="<?= $this->input->get_post('agent_leads_id') ?>" ng-init="FetchLeadDetails(<?= $this->input->get_post('agent_leads_id') ?>)" ng-model="reservationObj.agent_leads_id"/>
            <?php } ?>
            <div class="row" ng-show="is_irctc_center == true">
                <div class="col-md-12">

                </div>
            </div>

            <div class="upper-grid" >
                <div class="ug-col">
                    <div class="whitebox">
                        <div class="whiteboxHead">
                            <h4>Guest Information</h4>
                        </div>
                        <div class="form-horizontal">


                            <div ng-show="is_irctc_center == true">
                                <div class="form-group" >
                                    <label class="col-sm-3 control-label">Ticket Type</label>
                                    <div class="col-sm-9" ng-init="reservationObj.ticket_type = 'Train Ticket'">
                                        <div class="custom-group">
                                            <div class="custom-input">
                                                <label><input type="radio" ng-model="reservationObj.ticket_type" value="Train Ticket"> Train Ticket</label>
                                                &nbsp;&nbsp;
                                                <label><input type="radio" ng-model="reservationObj.ticket_type" value="Platform Ticket"> Platform Ticket</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" ng-if="reservationObj.ticket_type != ''">
                                    <label class="col-sm-3 control-label"> {{reservationObj.ticket_type=="Platform Ticket"?"UTS":"PNR"}} Number</label>
                                    <div class="col-sm-9">
                                        <div class="custom-group">
                                            <div class="custom-input">
                                                <input type="text" class="form-control"  name="ticket_number" ng-model="reservationObj.ticket_number" ng-blur="FetchPNRInfo()" autocomplete="ticket_number" placeholder='{{reservationObj.ticket_type=="Platform Ticket"?"UTS":"PNR"}} Number'>
                                                <span class="ci-icon"><i class="fal fa-train"></i></span>
                                            </div>
                                            <p ng-if="reservationObj.valid_pnr_number == true">
                                                Train Name : <span class="text-info">{{reservationObj.pnr_info.pnr_data.train_name}}</span>
                                                <br/>
                                                From Station : <span class="text-success">{{reservationObj.pnr_info.pnr_data.origin_city}}</span>
                                                <br/>
                                                To Station : <span class="text-danger">{{reservationObj.pnr_info.pnr_data.to_full}}</span>
                                                <br/>
                                                Travel Date & Time : <span class="text-info">{{reservationObj.pnr_info.pnr_data.travel_date}}</span>
                                                <br/>
                                            </p>
                                        </div>
                                    </div>
                                </div> 


                                <div class="form-group" ng-if="reservationObj.ticket_type == 'Platform Ticket'">
                                    <label class="col-sm-3 control-label">Ticket Attachment</label>
                                    <div class="col-sm-9">
                                        <div class="custom-group">
                                            <div class="custom-input">
                                                <input type="file" class="form-control" name="ticket_attachment" 
                                                       ng-model="reservationObj.ticket_attachment" accept="image/x-png,image/gif,image/jpeg,image/png,application/pdf" maxsize="5000" 
                                                       base-sixty-four-input>
                                                <span class="ci-icon"><i class="fal fa-image"></i></span>
                                            </div>
                                        </div>

                                        <img ng-show="reservationObj.ticket_attachment.filetype"
                                             ng-src='data:{{reservationObj.ticket_attachment.filetype}};base64,{{reservationObj.ticket_attachment.base64}}' style="height: 50px" />
                                        <span ng-show="form.files.$error.maxsize">Files must not exceed 5000 KB</span>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 control-label">Mobile</label>
                                <div class="col-sm-9">
                                    <div class="custom-group">
                                        <div class="custom-input">
                                            <input type="text" class="form-control number"  name="contact_mobile" ng-model="reservationObj.contact_mobile" ng-blur="FetchCustomerData()" autocomplete="mobile" placeholder="Mobile">
                                            <span class="ci-icon"><i class="fal fa-mobile"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Name</label>
                                <div class="col-sm-9">
                                    <div class="custom-group">
                                        <div class="custom-input">
                                            <input type="text" class="form-control" name="contact_name" ng-model="reservationObj.contact_name" autocomplete="name" placeholder="Name" pattern="[a-zA-Z\s]+">
                                            <span class="ci-icon"><i class="fal fa-user"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9">
                                    <div class="custom-group">
                                        <div class="custom-input">
                                            <input type="email" class="form-control" name="contact_email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,5}$" ng-model="reservationObj.contact_email"  autocomplete="email" placeholder="Email">
                                            <span class="ci-icon"><i class="fal fa-envelope"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mb-0-xs">
                                <label class="col-sm-3 control-label">Pincode</label>
                                <div class="col-sm-9">
                                    <div class="custom-group">
                                        <div class="custom-input">
                                            <input type="text" class="form-control number" placeholder="Pincode" ng-model="reservationObj.customer_pincode">
                                            <span class="ci-icon"><i class="fal fa-map-pin"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ug-col">
                    <div class="whitebox" ng-show="freshupBookingAvailable">
                        <div class="whiteboxHead">
                            <h4>Freshup</h4>
                        </div>
                        <div class="form-horizontal" ng-init="reservationObj.is_past_booking = '<?= $is_past_reservation ?>'">
                            <!--<span class="text-danger">{{check_in_error_message}}</span>-->
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Check-In Date</label>
                                <div class="col-sm-9">
                                    <div class="custom-group">
                                        <div class="custom-input">
                                            <input type="text" 
                                            <?php if ($is_past_reservation) { ?>
                                                       class="form-control dwpdates" 
                                                   <?php } else { ?>
                                                       class="form-control datepicker" 
                                                   <?php } ?>
                                                   onkeydown="event.preventDefault()" readonly2 
                                                   placeholder="Check-In Date" 
                                                   name="check_in_date" 
                                                   ng-model="reservationObj.check_in_date"
                                                   ng-init="reservationObj.check_in_date = '<?= date("d-m-Y") ?>'"
                                                   autocomplete="check_in_date" ng-disabled="disableStayForm == true">
                                            <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Check-In Time</label>
                                <div class="col-sm-9">
                                    <div class="custom-group">
                                        <div class="custom-input">
                                            <input type="text" class="form-control timepicker" onkeydown="event.preventDefault()" readonly2 
                                                   ng-init="reservationObj.check_in_time = '<?= date("h:i A") ?>'"
                                                   placeholder="Check-In Time" name="check_in_time" ng-model="reservationObj.check_in_time" ng-disabled="disableStayForm == true">
                                            <span class="ci-icon"><i class="fal fa-clock"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Duration</label>
                                <div class="col-sm-9">
                                    <div class="custom-group">
                                        <div class="custom-input">
                                            <select class="form-control" name="duration" ng-model="reservationObj.duration" pattern="\d*" ng-disabled="disableStayForm == true">
                                                <option value="">Duration</option>
                                                <option>1</option>
                                                <option>3</option>
                                                <option>6</option>
                                                <option>9</option>
                                                <option>24</option>
                                            </select>
                                            <span class="ci-icon"><i class="fal fa-clock"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mb-0-xs">
                                <div class="col-sm-9 col-md-offset-3">
                                    <button type="button" class="btn btn-primary" ng-click="getStayAvailability()" 
                                            ng-disabled="!reservationObj.check_in_date || !reservationObj.check_in_time || !reservationObj.duration">Search <i class="fal fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ug-col">
                    <div class="other-forms">
                        <div class="of-col">
                            <div class="inner">
                                <i class="fal fa-file-alt"></i>
                                <div>
                                    <h4>Billing Information</h4>
                                    <button type="button" ng-click="ShowUpdateBillingInformation()" class="btn btn-primary btn-sm text-uppercase">Update</button>
                                </div>
                            </div>
                        </div>
                        <div class="of-col">
                            <div class="inner">
                                <i class="fal fa-ellipsis-h-alt"></i>
                                <div>
                                    <h4>Other Information</h4>
                                    <button type="button" ng-click="ShowUpdateOtherInformation()" class="btn btn-primary btn-sm text-uppercase">Update</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="table-responsive whitebox"  ng-show="stayAvailabilityList.length > 0">
                <table class="table table-responsive table-bordered table-condensed  nowrapingtable">
                    <tbody>
                        <tr ng-repeat="item in stayAvailabilityList track by $index">
                            <td>
                                <table class="table table-responsive table-bordered table-condensed  nowrapingtable">
                                    <tr>
                                        <th rowspan="2" class="col-md-2 col-sm-2 col-xs-2 col-lg-2" style="text-align: center">
                                            <img ng-src="{{item.image}}" width="50" height="50" alt="{{item.category_name}}" style="margin-bottom: 3px"><br>
                                            <strong style="font-weight: 700">{{item.category_name}} </strong><br>
                                            <span class="badge" style="background-color: #F7941E;font-size: 14px">{{item.availability_count}} Available</span>
                                        </th>
                                        <td style="width:200px; vertical-align: middle ">
                                            Unit Price: <strong style="font-weight: 700"><i class="fal fa-rupee-sign"></i> {{item.price|currency:''}}</strong>
                                        </td>
                                        <td style="width:200px; vertical-align: middle ">
                                            <div class="custom-input">
                                                <select class="form-control input-sm" ng-init="item.selectedQty = '0'" ng-model="item.selectedQty" 
                                                        ng-change="updateItemQty()">
                                                    <option value="0">Qty</option>
                                                    <option ng-repeat="i in []| range:item.availability_count" value="{{i + 1}}">{{i + 1}} {{item.category_name}}{{i+1>1?"s":""}}</option>
                                                </select>
                                                <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                                            </div>
                                        </td>
                                        <td  style="vertical-align: middle">
                                            Total  Price: <strong style="font-weight: 700"><i class="fal fa-rupee-sign"></i>   {{item.price * item.selectedQty|currency:""}}</strong>
                                        </td>
                                    </tr>
                                    <tr >
                                        <td colspan="3">
                                            <p><font color="red">Note: </font> {{item.display_note}} <span class="text-info" style="font-weight: bold " ng-if="item.selectedQty > 0 && item.show_unmarried_note == 1">Unmarried couples are not allowed</span></p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </form>
    </div>
    <div class="pageSidebar right">
        <!-- <div class="SidebarHead offset"></div> -->
        <div class="SidebarBody">
            <table class="table font-bold mb-0-xs">
                <tr class="table-total">
                    <td >Total</td>
                    <td class="text-right">{{reservationObj.payment_info.grand_total|number:2}}</td>
                </tr>
            </table>
            <table class="table table-custom table-custom-px-0 font-bold mb-10-xs">
                <tr ng-repeat="item in reservationObj.booking_items track by $index">
                    <td>{{item.items.length}} {{item.stay_category_name}}</td>
                    <td class="text-right">{{item.total_amount|number:2}}</td>
                </tr>
                <tr class="table-devider">
                    <td>Total</td>
                    <td class="text-right">{{reservationObj.payment_info.amount|number:2}}</td>
                </tr>
                <tr>
                    <td><a class="text-primary" data-toggle="modal" href="#modal-discount">Discount <i class="fal fa-pen"></i></a> <small ng-bind-html="discount_text"></small></td>
                    <td class="text-right">- {{reservationObj.payment_info.discount_amount|number:2}}</td>
                </tr>
                <tr class="table-devider">
                    <td>Sub Total</td>
                    <td class="text-right">{{reservationObj.payment_info.sub_total|number:2}}</td>
                </tr>
                <tr ng-repeat="item in reservationObj.payment_info.applied_taxes track by $index">
                    <td>{{item.tax_calculation_type=='Flat Amount' ? "Rs.":''}} {{item.tax_amount}} {{item.tax_calculation_type=='Percentage' ? "%":''}} {{item.tax_name}}</td>
                    <td class="text-right">{{item.applied_tax_amount|number:2}}</td>
                </tr>
                <tr>
                    <td>Total</td>
                    <td class="text-right">{{reservationObj.payment_info.grand_total|number:2}}</td>
                </tr>
                <tr class="table-devider">
                    <td>Amount Paid</td>
                    <td class="text-right">{{reservationObj.payment_info.payments_total|number:2}}</td>
                </tr>
                <tr>
                    <td>Amount Due</td>
                    <td class="text-right">{{(reservationObj.payment_info.grand_total - reservationObj.payment_info.payments_total)|number:2}}</td>
                </tr>
            </table>
        </div>
        <div class="SidebarFooter offset">
            <div class="row rm-5">
                <div class="col-md-12 cp-5"><button type="button" class="btn btn-primary btn-block mb-10-xs" ng-click="ShowPaymentForm();"  ng-disabled="!reservationObj.centers_id"><b>Continue <i class="fal fa-arrow-right"></i></b></button></div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="billing-information-add">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Billing Information</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <span class="text-danger" ng-bind-html="error_message"></span>
                        <div class="col-md-5">
                            <div class="form-group">
                                <div class="custom-group">
                                    <div class="custom-input">
                                        <select class="form-control" name="billing_to" ng-model="reservationObj.billing_to" ng-change="(reservationObj.billing_to == 'Customer') ? FetchCustomerData() : ''">
                                            <option value="Customer">Customer</option>
                                            <option value="Organization">Organization</option>
                                            <option value="Others">Others</option>
                                        </select>
                                        <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="" style="margin-bottom: 15px;">
                                <div ng-show="reservationObj.billing_to === 'Organization'">
                                    <div table>
                                        <div tr>
                                            <div td>
                                                <div class="dynamic-search">
                                                    <input type="text"  ng-keyup="GetCompaniesListWithSearchKey()" ng-model="reservationObj.company_name" placeholder="Company Name" class="form-control" ng-disabled="reservationObj.billing_to != 'Organization'">
                                                    <div class="dynamic-search-result" ng-show="companiesList_display">
                                                        <ul> 
                                                            <li ng-repeat="item in companiesList" ng-click="companiesListClick(item)">{{item.display_text}}</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php if (permission_required(30, 'add', get_user_id(), true)) { ?>
                                                <div td btn>
                                                    <button type="button" class="btn btn-default" ng-click="ShowCompanyAddForm()" ng-disabled="reservationObj.billing_to != 'Organization'">
                                                        <i ng-show="companySpinner" class="fal fa-circle-notch fa-spin"></i>
                                                        <i ng-show="!companySpinner" class="fal fa-plus"></i>
                                                    </button>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div ng-show="reservationObj.billing_to === 'Others'">
                                    <div table>
                                        <div tr>
                                            <div td>
                                                <input type="text"  ng-model="reservationObj.billing_to_others_text"
                                                       placeholder="Enter Organisation/trust/NGO/College or any other type of group" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Door No</label>
                                <div class="custom-group">
                                    <div class="custom-input">
                                        <input type="text" class="form-control" placeholder="Door No" name="door_no" ng-model="reservationObj.door_no" autocomplete="door-no">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Street Name</label>
                                <div class="custom-group">
                                    <div class="custom-input">
                                        <input type="text" class="form-control" placeholder="Street Name" name="street_name" ng-model="reservationObj.street_name" autocomplete="street_name">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Area/Location</label>
                                <div class="custom-group">
                                    <input type="text" class="form-control" placeholder="Area/Location" name="location" ng-model="reservationObj.location" autocomplete="location">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>City</label>
                                <div class="custom-group">
                                    <input type="text" class="form-control" placeholder="City" name="city" ng-model="reservationObj.city" autocomplete="city" pattern="[a-zA-Z\s]+">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>District</label>
                                <div class="custom-group">
                                    <input type="text" class="form-control" placeholder="District" name="district" ng-model="reservationObj.district" autocomplete="district" pattern="[a-zA-Z\s]+">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>State</label>
                                <div class="custom-group">
                                    <input type="text" class="form-control" placeholder="Statename" name="state" ng-model="reservationObj.state" autocomplete="state" pattern="[a-zA-Z\s]+">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Pin Code</label>
                                <div class="custom-group">
                                    <input type="text" class="form-control number" maxlength="6" minlength="6" placeholder="Pincode/Zipcode/Postal Code" name="zipcode" ng-model="reservationObj.zipcode" autocomplete="zipcode">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Country</label>
                                <div class="custom-group">
                                    <div class="custom-input">
                                        <select class="form-control" name="countries_id" ng-model="reservationObj.countries_id">
                                            <option value="">Choose Country</option>
                                            <option ng-value="item.id" value="item.id" ng-repeat="item in countriesList">{{item.country_name}}</option>
                                        </select>
                                        <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>GST Number</label>
                                <div class="custom-group">
                                    <div class="custom-input">
                                        <input type="text" class="form-control" placeholder="GST Number" ng-required="reservationObj.billing_to === 'Company'" name="payer_gst_number" ng-model="reservationObj.payer_gst_number" maxlength="15" minlength="15" autocomplete="gst_number" pattern="[a-zA-Z0-9\s]+">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button class="pull-right btn btn-success" data-dismiss="modal" >Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="other-information-add">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Other Information</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>How did you know us</label>
                                <div class="custom-group">
                                    <div class="custom-input">
                                        <select class="form-control" ng-model="reservationObj.how_did_hear_about_us_id">
                                            <option value="">Choose</option>
                                            <option ng-value="item.id" value="item.id" ng-repeat="item in referencesList">{{item.title}}</option>
                                        </select>
                                        <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Agent</label>
                                <div table>
                                    <div tr>
                                        <div td>
                                            <div class="dynamic-search">
                                                <input type="text"  autocomplete="agent_mobile" ng-keyup="GetAgentsListWithSearchKey()" ng-model="reservationObj.agent_mobile" placeholder="Agent Name or Mobile" class="form-control">
                                                <div class="dynamic-search-result" ng-show="agentsList_display">
                                                    <ul>
                                                        <li ng-repeat="item in agentsList" ng-click="agentsListClick(item)">{{item.display_text}}</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if (permission_required(24, 'add', get_user_id(), true)) { ?>
                                            <div td btn> 
                                                <button  class="btn btn-default"  ng-click="ShowAgentAddForm()"> 
                                                    <i ng-show="!agentSpinner" class="fal fa-plus"></i>
                                                    <i ng-show="agentSpinner" class="fal fa-circle-notch fa-spin"></i>
                                                </button> 
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Commission <small>(%)</small></label>
                                <input type="number" min="0" max="100" class="form-control numbers_decimals_only" placeholder="Commission" ng-disabled="reservationObj.agent_mobile == ''" readonly="" ng-model="reservationObj.agent_commission">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Reservation Status</label>
                                <div class="custom-group">
                                    <div class="custom-input">
                                        <select class="form-control" name="booking_status" ng-model="reservationObj.booking_status">
                                            <option value="">Choose Reservation Status</option>
                                            <option value="Pending">Pending</option>
                                            <option value="Blocked" ng-if="((user_role_id == 2) || (user_role_id == 12))">Blocked</option>
                                        </select>
                                        <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Business</label>
                                <div class="custom-group">
                                    <div class="custom-input">
                                        <select class="form-control" ng-model="reservationObj.business_sources_id">
                                            <option value="">Choose Business</option>
                                            <option ng-value="item.id" value="item.id" ng-repeat="item in businessSourcesList">{{item.business_source_name}}</option>
                                        </select>
                                        <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button class="pull-right btn btn-success" data-dismiss="modal" >Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('includes/reservation_add_payment_popup') ?>
    <?php $this->load->view('includes/reservation_discount_popup') ?>

    <?php if (permission_required(24, 'add', get_user_id(), true) || permission_required(24, 'edit', get_user_id(), true)) { ?>
        <section ng-controller="agentsCtrl">
            <!-- Modal -->
            <div class="modal fade" id="agent-modal-popup">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">{{agentObj.id?"Update":"Add"}} Agent</h4>
                        </div>
                        <div class="modal-body">
                            <form ng-submit="AddOrUpdateAgent()" id="agentForm" autocomplete="off">
                                <?php $this->load->view("includes/add_agent_form"); ?>
                                <div class="text-right">
                                    <button class="btn btn-default" type="reset" ng-click="ResetAgentForm()" data-dismiss="modal" aria-hidden="true"><b>Cancel <i class="fal fa-times"></i></b></button>
                                    <button type="submit" class="btn btn-primary"><b>{{agentObj.id?'Update':'Add'}} <i class="fal fa-plus"></i></b></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Modal end -->
    <?php } ?>

</section>
<div ng-controller="companiesCtrl">
    <?php $this->load->view("includes/company_add_popup") ?>
</div>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/common_validations/agentValidations.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/quickReservationCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/createReservationService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/countriesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/howDidHearAboutUsService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/businessSourcesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/companiesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/companiesCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/companiesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/organizationTypesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/agentsService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/agentTypesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/centersService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/taxSlabsService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/customersService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/leadsService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/agentsCtrl.js?r=<?= time() ?>"></script>