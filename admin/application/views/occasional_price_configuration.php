<section class="pageWrapper" ng-controller="occasionalPriceConfigurationCtrl">
    <div class="pageHeader" workspace-offset valign-parent>
        <div class="row">
            <div class="col-md-6"><strong>Occasional Price Configuration <span ng-show="occasioanlPriceObj.occasion_name"> for {{occasioanlPriceObj.occasion_name}}</span></strong></div>
            <div class="col-md-6">
                <div valign-holder>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="custom-input">
                                <select class="form-control input-sm" ng-change="GetOccasionsListForCenter(occasioanlPriceObj.center_id)" ng-model="occasioanlPriceObj.center_id" ng-disabled="!centersList.length > 0">
                                    <option value="">Choose Center</option>
                                    <option ng-value="item.id" value="item.id" ng-repeat="item in centersList">{{item.center_name}}</option>
                                </select>
                                <span class="ci-icon">
                                    <i ng-show="centersSpinner" class="fal fa-circle-notch fa-spin"></i>
                                    <i ng-show="!centersSpinner" class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4 pl-10-xs">
                            <div class="custom-input">
                                <select class="form-control input-sm" ng-change="GetStayingCategoriesListForCenter(occasioanlPriceObj.center_id)" ng-model="occasioanlPriceObj.occasions_id" ng-disabled="!occasionsList.length > 0">
                                    <option value="">Choose Occasion</option>
                                    <option value="{{item.id}}" ng-repeat="item in occasionsList track by $index">{{item.occasion_name}}</option>
                                </select>
                                <span class="ci-icon">
                                    <i ng-show="!occasionsSpinner" class="fal fa-chevron-down"></i>
                                    <i ng-show="occasionsSpinner" class="fal fa-circle-notch fa-spin"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4 pl-10-xs" ng-disabled="!occasionsList.length > 0">
                            <div class="custom-input" ng-init="occasioanlPriceObj.selected_type = ''">
                                <select class="form-control input-sm" ng-model="occasioanlPriceObj.selected_type" ng-disabled="!occasionsList.length > 0">
                                    <option value="">Choose Type</option>
                                    <option value="Hotels">Hotels</option>
                                    <option value="Freshup">Freshup</option>
                                </select>
                                <span class="ci-icon">
                                    <i class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>
        <div class="responsive-table" ng-if="occasioanlPriceObj.center_id && occasioanlPriceObj.occasions_id && occasioanlPriceObj.selected_type !== ''">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Type</th>
                        <th>Category</th>
                        <th>For Gender</th>
                        <th>Image</th>
                        <th>Availability Status</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in stayingCategoriesList track by $index" ng-if="occasioanlPriceObj.selected_type === item.type">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Type">{{item.type}}</td>
                        <td data-label="Category">{{item.category_name}}</td>
                        <td data-label="For Gender">{{item.gender}}</td>
                        <td data-label="Image">
                            <img ng-src="{{item.image}}" class="img-thumbnail" style="height: 40px"/>
                        </td>
                        <td data-label="Availability Status">
                            <span class="{{item.bootstrap_class}}">{{item.availability_status}}</span>
                        </td>
                        <td data-label="Action" class="text-right">
                            <button class="btn btn-sm btn-default" ng-click="ViewPricesPopUp(item)">View</button>
                            <button class="btn btn-sm btn-default" ng-click="ShowPriceConfigPopUp(item)">Configure</button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk - 2017</div>


    <!-- View Center-->
    <div class="modal fade" id="view_prices_popup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Occasional Price Details "{{configuringObj.center_name}}"</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th>Center Name :</th>
                                            <td><b ng-bind="configuringObj.center_name"></b></td>
                                        </tr>
                                        <tr>
                                            <th>Category :</th>
                                            <td><b ng-bind="configuringObj.category_name"></b></td>
                                        </tr>
                                        <tr>
                                            <th>Occasion Name :</th>
                                            <td><b ng-bind="configuringObj.occasion_name"></b></td>
                                        </tr>
                                        <tr>
                                            <th>From Date : </th>
                                            <td><b ng-bind="configuringObj.from_date"></b></td>
                                        </tr>
                                        <tr>
                                            <th>To Date : </th>
                                            <td><b ng-bind="configuringObj.to_date"></b></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="text-center text-success">{{configuringObj.availability_type}} Price Configuration</h4>
                            <div class="width-700 mx-auto-xs">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>For </th>
                                                <th>Regular Price</th>
                                                <th>Occasional Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="item in configuringObj.price_config track by $index">
                                                <td><strong>{{item.duration}}  {{configuringObj.availability_type_name}}</strong></td>
                                                <td><strong>{{item.price}}</strong></td>
                                                <td><strong>{{item.occasional_price}}</strong>
                                                    <button ng-show="!item.occasional_price"class="btn btn-sm btn-default" ng-click="ShowPriceConfigPopUp(configuringObj)">Configure</button>
                                                </td>
                                            </tr>
                                            <tr ng-show="configuringObj.price_config.length === 0">
                                                <td colspan="3" class="text-center">
                                                    Please configure the regular price first.
                                                    <br/>
                                                    <button ng-click="ShowPriceConfigPopUp(configuringObj)" class="btn btn-info">Configure now</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4 class="text-center text-danger">Extended Stay Configuration</h4>
                            <div class="width-700 mx-auto-xs">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>For </th>
                                                <th>Regular Price</th>
                                                <th>Occasional Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="item in configuringObj.extended_price_config track by $index">
                                                <td><strong>{{item.duration}}  {{configuringObj.availability_type_name}}</strong></td>
                                                <td><strong>{{item.price}}</strong></td>
                                                <td><strong>{{item.occasional_price}}</strong>
                                                    <button min="1" required ng-show="!item.occasional_price"class="btn btn-sm btn-default" ng-click="ShowPriceConfigPopUp(item)">Configure</button>
                                                </td>
                                            </tr>
                                            <tr ng-show="configuringObj.extended_price_config.length === 0">
                                                <td colspan="3" class="text-center">
                                                    Please configure the price first.
                                                    <br/>
                                                    <button ng-click="ShowPriceConfigPopUp(configuringObj)" class="btn btn-info">Configure now</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--View Center popup end-->

    <!--Add Edit Popup-->
    <div class="modal fade" id="configure_popup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form ng-submit="AddOrUpdateCenterPrices()">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Configure Center Prices</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <th>Center Name :</th>
                                                <td><b ng-bind="configuringObj.center_name"></b></td>
                                            </tr>
                                            <tr>
                                                <th>Category :</th>
                                                <td><b ng-bind="configuringObj.category_name"></b></td>
                                            </tr>
                                            <tr>
                                                <th>Occasion Name :</th>
                                                <td><b ng-bind="configuringObj.occasion_name"></b></td>
                                            </tr>
                                            <tr>
                                                <th>From Date : </th>
                                                <td><b ng-bind="configuringObj.from_date"></b></td>
                                            </tr>
                                            <tr>
                                                <th>To Date : </th>
                                                <td><b ng-bind="configuringObj.to_date"></b></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <p ng-bind="error_message" class="text-danger"></p>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="width-400 mx-auto-xs">
                                    <h4 class="text-center text-success">{{configuringObj.availability_type}} Price Configuration</h4>

                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>For</th>
                                                    <th>Regular Price (Rs.)</th>
                                                    <th>Occasional Price (Rs.)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="item in configuringObj.price_config track by $index">
                                                    <td>
                                                        <label style="padding:7px">{{item.duration}}  {{configuringObj.availability_type_name}}</label>
                                                    </td>
                                                    <td>
                                                        <label style="padding:7px">{{item.price}}</label>
                                                    </td>
                                                    <td>
                                                        <input type="number" class="form-control" min="1" required ng-model="item.occasional_price" style="width:150px">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="width-400 mx-auto-xs">
                                    <h4 class="text-center text-danger">Extended Stay Configuration</h4>
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>For</th>
                                                    <th>Regular Price (Rs.)</th>
                                                    <th>Occasional Price (Rs.)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="item in configuringObj.extended_price_config track by $index">
                                                    <td>
                                                        <label style="padding:7px">{{item.duration}} {{configuringObj.availability_type_name}}</label>
                                                    </td>
                                                    <td>
                                                        <label style="padding:7px">{{item.price}}</label>
                                                    </td>
                                                    <td>
                                                        <input type="number" class="form-control" min="1" required ng-model="item.occasional_price" style="width:150px">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary pull-right" ng-disabled="duplication">Save <i class="fal fa-arrow-right"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--Add Edit Popup End-->
</section>

<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/occasionalPriceConfigurationCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/centersService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/occasionsService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/priceConfigService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/stayingCategoriesService.js?r=<?= time() ?>"></script>
