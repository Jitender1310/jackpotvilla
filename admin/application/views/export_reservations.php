<section class="pageWrapper pageSidebar">
    <div class="pageHeader" workspace-offset valign-parent>
        <div class="row">
            <div class="col-md-10">
                <strong>Export Reservations</strong>
            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>
        <div class="whitebox">
            <div class="row rm-5">
                <form class="" action="<?php echo base_url(); ?>reservations/export_to_excel/generate" method="post" >
                    <div class="col-md-4 cp-5">
                        <div class="custom-group">
                            <div class="custom-input">
                                <input type="text" class="form-control datepickerNormal" required placeholder="From Date" name="from_date" onkeydown="event.preventDefault()" ng-model="filter.from_date">
                                <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 cp-5">
                        <div class="custom-group">
                            <div class="custom-input">
                                <input type="text" class="form-control datepickerNormal" required placeholder="To Date" name="to_date" onkeydown="event.preventDefault()" ng-model="filter.to_date">
                                <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 cp-5">
                        <div class="form-group mb-0-xs"> 
                            <div class="custom-input" title="Centers">
                                <select class="form-control" name="center_id">
                                    <option value="" selected>All Centers</option>
                                    <?php
                                    foreach ($assigned_centers as $item) {
                                        ?>
                                        <option value="<?= $item->id ?>"><?= $item->center_name ?></option>
                                        <?php
                                    }
                                    ?>

                                </select>
                                <span class="ci-icon">
                                    <i ng-show="centersSpinner" class="fal fa-circle-notch fa-spin"></i>
                                    <i ng-show="!centersSpinner" class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 cp-5">
                        <button type="submit" class="btn btn-primary "><i class="fas fa-download"></i></button>

                        <button type="reset" ng-click="ResetFilter()" class="btn btn-danger " title="Clear Search"><i class="fas fa-times-circle"></i></button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk <?= date('Y') ?></div>
</section>