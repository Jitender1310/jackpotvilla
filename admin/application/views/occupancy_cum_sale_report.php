<section class="pageWrapper">
    <div class="pageHeader" workspace-offset  valign-parent>
        <div class="row">
            <div class="col-md-6"><strong>Occupancy cum Sale
                    Report</strong></div>
            <div class="col-md-6 text-right">
                <div valign-holder>





                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>

        <div class="whitebox mb-0-xs">


            <div class="fit-parent">

                <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered fit-child no-wrap">

                    <tr class="bg-default font-bold text-capitalize ">
                        <td>S.no</td>
                        <td>Guest name</td>
                        <td>Location</td>
                        <td>pax</td>
                        <td>Type of Room</td>
                        <td>C/in Time</td>
                        <td>package</td>
                        <td>Rate</td>
                        <td colspan="3">pay mode</td>
                        <td>Total Sale</td>
                        <td colspan="3"></td>
                    </tr>
                    <tr class="font-bold text-capitalize">
                        <td colspan="8"></td>
                        <td>cash</td>
                        <td>card</td>
                        <td>Online</td>
                        <td>Cash/Card/Online</td>
                        <td>Tax</td>
                        <td>Commisson</td>
                        <td>Remarks</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Manoj Kumar</td>
                        <td>Kuppam</td>
                        <td>3</td>
                        <td>Chaise Launge</td>
                        <td>04:00 AM</td>
                        <td>3 hrs</td>
                        <td>750</td>
                        <td>750</td>
                        <td>0</td>
                        <td>0</td>
                        <td>750</td>
                        <td>0</td>
                        <td>0</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Rohith Sai</td>
                        <td>Kuppam</td>
                        <td>2</td>
                        <td>Chaise Launge</td>
                        <td>04:00 AM</td>
                        <td>3 hrs</td>
                        <td>500</td>
                        <td>500</td>
                        <td>0</td>
                        <td>0</td>
                        <td>500</td>
                        <td>0</td>
                        <td>0</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Guru Raj</td>
                        <td>Anantapur</td>
                        <td>2</td>
                        <td>Double room</td>
                        <td>05:00 AM</td>
                        <td>3 hrs</td>
                        <td>450</td>
                        <td>450</td>
                        <td>0</td>
                        <td>0</td>
                        <td>450</td>
                        <td>0</td>
                        <td>0</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Sai Pavan</td>
                        <td>Nellore</td>
                        <td>2</td>
                        <td>Double room</td>
                        <td>05:30 AM</td>
                        <td>3 hrs</td>
                        <td>450</td>
                        <td>0</td>
                        <td>450</td>
                        <td>0</td>
                        <td>450</td>
                        <td>0</td>
                        <td>0</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Raju</td>
                        <td>Bangalore</td>
                        <td>1</td>
                        <td>Bunker bed</td>
                        <td>06:00 AM</td>
                        <td>3 hrs</td>
                        <td>350</td>
                        <td>350</td>
                        <td>0</td>
                        <td>0</td>
                        <td>350</td>
                        <td>0</td>
                        <td>0</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>Manohar reddy</td>
                        <td>Anantapur</td>
                        <td>1</td>
                        <td>Bunker bed</td>
                        <td>07:00 AM</td>
                        <td>3 hrs</td>
                        <td>350</td>
                        <td>350</td>
                        <td>0</td>
                        <td>0</td>
                        <td>350</td>
                        <td>0</td>
                        <td>0</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Srikanth</td>
                        <td>Hyderabad</td>
                        <td>2</td>
                        <td>Double room</td>
                        <td>09:50 AM</td>
                        <td>3 hrs</td>
                        <td>450</td>
                        <td>450</td>
                        <td>0</td>
                        <td>0</td>
                        <td>450</td>
                        <td>0</td>
                        <td>0</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>Sundaram</td>
                        <td>Nellore</td>
                        <td>1</td>
                        <td>Chaise Launge</td>
                        <td>14:40 hrs</td>
                        <td>3 hrs</td>
                        <td>250</td>
                        <td>250</td>
                        <td>0</td>
                        <td>0</td>
                        <td>250</td>
                        <td>0</td>
                        <td>0</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td>Rambabu</td>
                        <td>Khammam</td>
                        <td>2</td>
                        <td>Double room</td>
                        <td>21:00 hrs</td>
                        <td>3 hrs</td>
                        <td>400</td>
                        <td>400</td>
                        <td>0</td>
                        <td>0</td>
                        <td>400</td>
                        <td>0</td>
                        <td>0</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>10</td>
                        <td>Ravi kumar</td>
                        <td>Hubli</td>
                        <td>2</td>
                        <td>Chaise Launge</td>
                        <td>21:15 hrs</td>
                        <td>6 hrs</td>
                        <td>800</td>
                        <td>800</td>
                        <td>0</td>
                        <td>0</td>
                        <td>800</td>
                        <td>0</td>
                        <td>0</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>11</td>
                        <td>Rangaiah</td>
                        <td>Hubli</td>
                        <td>2</td>
                        <td>Chaise Launge</td>
                        <td>21:15 hrs</td>
                        <td>6 hrs</td>
                        <td>800</td>
                        <td>800</td>
                        <td>0</td>
                        <td>0</td>
                        <td>800</td>
                        <td>0</td>
                        <td>0</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>12</td>
                        <td>Rajeswari</td>
                        <td>Hubli</td>
                        <td>2</td>
                        <td>Chaise Launge</td>
                        <td>21:20 hrs</td>
                        <td>6 hrs</td>
                        <td>800</td>
                        <td>800</td>
                        <td>0</td>
                        <td>0</td>
                        <td>800</td>
                        <td>0</td>
                        <td>0</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>13</td>
                        <td>Vijaylakshmi</td>
                        <td>Hubli</td>
                        <td>1</td>
                        <td>Chaise Launge</td>
                        <td>21:30 hrs</td>
                        <td>6 hrs</td>
                        <td>400</td>
                        <td>400</td>
                        <td>0</td>
                        <td>0</td>
                        <td>400</td>
                        <td>0</td>
                        <td>0</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>14</td>
                        <td>Siva Ram Prasad</td>
                        <td>West Godavari</td>
                        <td>2</td>
                        <td>Double room</td>
                        <td>23:00 hrs</td>
                        <td>3 hrs</td>
                        <td>450</td>
                        <td>450</td>
                        <td>0</td>
                        <td>0</td>
                        <td>450</td>
                        <td>0</td>
                        <td>0</td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><b>Total</b></td>
                        <td>7200</td>
                        <td>6750</td>
                        <td>450</td>
                        <td>0</td>
                        <td>7200</td>
                        <td>0</td>
                        <td>0</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><b>F&B/ Other Sale</b></td>
                        <td>95</td>
                        <td>95</td>
                        <td>0</td>
                        <td>0</td>
                        <td>95</td>
                        <td>0</td>
                        <td>0</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><b>Grand Total</b></td>
                        <td>7295</td>
                        <td>6845</td>
                        <td>450</td>
                        <td>0</td>
                        <td>7295</td>
                        <td>0</td>
                        <td>0</td>
                        <td></td>
                    </tr>
                    <!--[if supportMisalignedColumns]-->

                    <!--[endif]-->
                    </tbody></table>
            </div>















        </div>

    </div>

    <?php $this->load->view('admin/includes/reports_menu'); ?>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk - 2017</div>
</section>