<section class="pageWrapper" ng-controller="regularPriceConfigurationCtrl" ng-init="GetCentersList('<?= get_user_id() ?>');">
    <div class="pageHeader" workspace-offset valign-parent>
        <div class="row">
            <div class="col-md-8"><strong>Regular Price Configuration</strong></div>
            <div class="col-md-2">
                <div valign-holder>
                    <div class="custom-input">
                        <select class="form-control input-sm" ng-change="GetStayingCategoriesListForCenter(regularPriceObj.center_id)" ng-model="regularPriceObj.center_id" ng-disabled="!centersList.length > 0">
                            <option value="">Choose Center</option>
                            <option ng-value="item.id" value="item.id" ng-repeat="item in centersList">{{item.center_name}}</option>
                        </select>
                        <span class="ci-icon">
                            <i ng-show="centersSpinner" class="fal fa-circle-notch fa-spin"></i>
                            <i ng-show="!centersSpinner" class="fal fa-chevron-down"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-2 pl-10-xs">
                <div valign-holder>
                    <div class="custom-input" ng-init="regularPriceObj.selected_type = ''">
                        <select class="form-control input-sm" ng-model="regularPriceObj.selected_type" ng-disabled="!centersList.length > 0">
                            <option value="">Choose Type</option>
                            <option value="Hotels">Hotels</option>
                            <option value="Freshup">Freshup</option>
                        </select>
                        <span class="ci-icon">
                            <i class="fal fa-chevron-down"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>
        <div class="responsive-table" ng-if="regularPriceObj.center_id && regularPriceObj.selected_type !== ''">
            <table class="table table-custom data-table">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Type</th>
                        <th>Category</th>
                        <th>For Gender</th>
                        <th>Image</th>
                        <th>Availability Status</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in stayingCategoriesList track by $index" ng-if="regularPriceObj.selected_type === item.type">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Type">{{item.type}}</td>
                        <td data-label="Category">{{item.category_name}}</td>
                        <td data-label="For Gender">{{item.gender}}</td>
                        <td data-label="Image">
                            <img ng-src="{{item.image}}" class="img-thumbnail" style="height: 40px"/>
                        </td>
                        <td data-label="Availability Status">
                            <span class="{{item.bootstrap_class}}">{{item.availability_status}}</span>
                        </td>
                        <td data-label="Action" class="text-right">
                            <button class="btn btn-sm btn-default" ng-click="ViewPricesPopUp(item)">View</button>
                            <button class="btn btn-sm btn-default" ng-click="ShowPriceConfigPopUp(item)">Configure</button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk - 2017</div>


    <!-- View Center-->
    <div class="modal fade" id="view_prices_popup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Regular Price Details "{{configuringObj.center_name}}"</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="text-center" style="text-decoration: underline !important">For Category : <b ng-bind="configuringObj.category_name"></b></h5>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="width-400 mx-auto-xs">
                                <h4 class="text-center text-success">{{configuringObj.availability_type_name}} Price Configuration</h4>
                                <div class="table-responsive">

                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <th>Center Name :</th>
                                                <td><b ng-bind="configuringObj.center_name"></b></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>For </th>
                                                <th>Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="item in configuringObj.price_config track by $index">
                                                <td><strong>{{item.duration}} {{configuringObj.availability_type_name}}</strong></td>
                                                <td><strong>{{item.price}}</strong></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="width-400 mx-auto-xs">
                                <h4 class="text-center text-danger">Extended Stay Configuration</h4>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <th>Center Name :</th>
                                                <td><b ng-bind="configuringObj.center_name"></b></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>For </th>
                                                <th>Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="item in configuringObj.extended_price_config track by $index">
                                                <td><strong>{{item.duration}} {{configuringObj.availability_type_name}}</strong></td>
                                                <td><strong>{{item.price}}</strong></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <!--View Center popup end-->

    <!--Add Edit Popup-->
    <div class="modal fade" id="configure_popup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form ng-submit="AddOrUpdateCenterPrices()">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Configure Center Prices "{{configuringObj.center_name}}"</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="text-center" style="text-decoration: underline !important">For Category : <b ng-bind="configuringObj.category_name"></b></h5>
                            </div>
                        </div>

                        <p ng-bind="error_message" class="text-danger"></p>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="width-400 mx-auto-xs">
                                    <h4 class="text-center text-success">{{configuringObj.availability_type_name}} Price Configuration</h4>
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>{{configuringObj.availability_type_name}}</th>
                                                    <th>Price (Rs.)</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="item in configuringObj.price_config track by $index">
                                                    <td>
                                                        <label >
                                                            <input type="text" ng-model="item.duration" class="form-control" ng-change="CheckForDuplication('normal'); CheckForMaxHoursCondition(item, 24, configuringObj.availability_type_name)">
                                                        </label>
                                                        <span class="text-danger" ng-if="item.duplication">Duplication found</span>
                                                        <span class="text-danger" ng-if="item.exceed_condition">{{item.exceed_condition}}</span>
                                                    </td>
                                                    <td>
                                                        <input type="number" class="form-control" min="1" required ng-model="item.price" style="width:150px">
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-danger" ng-click="RemoveItem('normal', $index)"><i class="fa fa-times"></i></button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <button type="button" class="btn btn-primary btn-block" ng-click="AddItemRow()">Add More <i class="fal fa-plus"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="width-400 mx-auto-xs">
                                    <h4 class="text-center text-danger">Extended Stay Configuration</h4>
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>{{configuringObj.availability_type_name}}</th>
                                                    <th>Price (Rs.)</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="item in configuringObj.extended_price_config track by $index">
                                                    <td>
                                                        <label >
                                                            <input type="text" ng-model="item.duration" class="form-control" ng-change="CheckForDuplication('extended')">
                                                        </label>
                                                        <span class="text-danger" ng-if="item.duplication">Duplication found</span>
                                                    </td>
                                                    <td>
                                                        <input type="number" class="form-control" min="1" required ng-model="item.price" style="width:150px">
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-danger" ng-click="RemoveItem('extended', $index)"><i class="fa fa-times"></i></button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <button type="button" class="btn btn-primary btn-block" ng-click="AddExtendedItemRow()">Add More <i class="fal fa-plus"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary pull-right" ng-disabled="duplication || excced_duration">Save <i class="fal fa-arrow-right"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--Add Edit Popup End-->

</section>

<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/regularPriceConfigurationCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/centersService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/priceConfigService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/stayingCategoriesService.js?r=<?= time() ?>"></script>
