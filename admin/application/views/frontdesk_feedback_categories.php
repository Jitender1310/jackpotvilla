<section class="pageWrapper" ng-controller="frontdeskFeedBackCategoriesCtrl">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-12"><strong>Frontdesk Feed Back Categories</strong></div>
        </div>
    </div>

    <div class="pageBody" workspace ng-init="GetFrontdeskFeedBackCategoriesList()">
        <div class="responsive-table" ng-hide="frontdeskFeedBackCategoriesList.length > 0">
            <div class="alert alert-danger">
                No Data found
            </div>
        </div> 

        <div class="responsive-table" ng-show="frontdeskFeedBackCategoriesList.length > 0">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="150">S.No</th>
                        <th width="350">Frontdesk Feed Back Category Name</th>
                        <th width="350">Icon Image in App</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in frontdeskFeedBackCategoriesList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Restrooom Checklist Category Name">{{item.frontdesk_feedback_category_name}}</td>
                        <td data-label="Image">
                            <img ng-src="{{item.image}}" class="img-thumbnail" style="height: 50px"/>
                        </td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditFrontDeskFeedBackCategory(item)"><a href="#">Edit</a></li>
                                    <li ng-click="DeleteFrontDeskFeedBackCategory(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageSidebar">
        <form ng-submit="AddOrUpdateFrontDeskFeedBackCategory()" id="frontdesk_feedback_categories_form">
            <div class="SidebarHead offset">
                {{frontdeskFeedBackCategoriesObj.id?'Update':'Add'}} Category
            </div>
            <div class="SidebarBody">

                <span class="text-danger" ng-bind-html="error_message"></span>

                <div class="form-group">
                    <label>Frontdesk Feed Back Category Name</label>
                    <input type="text" class="form-control" name="frontdesk_feedback_category_name" ng-model="frontdeskFeedBackCategoriesObj.frontdesk_feedback_category_name">
                </div>
                <div class="form-group rm-5">
                    <div class="col-sm-12 cp-5">
                        <div class="btn btn-default btn-block btn-file">
                            <input type="file" name="{{frontdeskFeedBackCategoriesObj.id?'':'image'}}" class="form-control" accept="image/x-png,image/gif,image/jpeg,image/png" maxsize="2000"
                                   ng-model="frontdeskFeedBackCategoriesObj.image" base-sixty-four-input>
                            <b>Browse Image </b> <i class="fas fa-ellipsis-h"></i>
                        </div>
                        <span ng-show="form.files.$error.maxsize">Files must not exceed 2000 KB</span>

                        <img ng-show="frontdeskFeedBackCategoriesObj.image.filetype"
                             ng-src='data:{{frontdeskFeedBackCategoriesObj.image.filetype}};base64,{{frontdeskFeedBackCategoriesObj.image.base64}}' style="height: 50px" />

                        <img ng-hide="frontdeskFeedBackCategoriesObj.image.filetype" ng-src="{{frontdeskFeedBackCategoriesObj.image}}" style="height: 50px"/>
                    </div>
                </div>
            </div>

            <div class="SidebarFooter offset">
                <div class="text-right">
                    <button class="btn btn-default" type="reset" ng-click="ResetForm()"><b>Cancel <i class="fal fa-times"></i></b></button>
                    <button type="submit" class="btn btn-primary"><b>{{frontdeskFeedBackCategoriesObj.id?'Update':'Add'}} <i class="fal fa-plus"></i></b></button>
                </div>
            </div>
        </form>
    </div>
    <footer-copy-right></footer-copy-right>
</section>

<div id="dialog-confirm" title="Confirm">
    <p>
        <span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>
        This item will be deleted. Are you sure?
    </p>
</div>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/frontdeskFeedBackCategoriesCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/frontdeskFeedBackCategoriesService.js?r=<?= time() ?>"></script>