<script src="<?= STATIC_ADMIN_JS_PATH ?>ckeditor-full/ckeditor/ckeditor.js"></script>
<section class="pageWrapper">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-12"><strong>Terms and Conditions in Website</strong></div>
        </div>
    </div>
    <div class="pageBody" workspace>
        <?php if ($this->session->flashdata('msg')) { ?>
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> <?= $this->session->flashdata('msg') ?>
            </div>
        <?php } ?>
        <form method="post" action="<?= base_url("master/cms/terms_and_conditions") ?>">
            <div class="row">
                <div class="col-md-12">
                    <textarea class="ckeditor form-control" name="description"><?= $description ?></textarea>  
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-success">Save</button>
                </div>
            </div>
        </form>
    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk <?= date('Y') ?></div>
</section>