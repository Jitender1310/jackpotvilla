<section class="pageWrapper" ng-controller="leadsCtrl" ng-init="GetCentersList('<?= get_user_id() ?>')">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-10">
                <strong>Leads</strong>
            </div>
            <div class="col-md-2">
                <div class="custom-input" title="Sory By">
                    <select class="form-control" ng-model="filter.sort_by" ng-change="GetLeadsList()">
                        <option value="" selected>Sort by</option>
                        <option value="Latest">Latest First</option>
                        <option value="Oldest">Oldest First</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="GetLeadsList()">

        <div class="whitebox">
            <div class="row">
                <div class="col-md-12">
                    <form class="" ng-submit="GetLeadsList(filter.page = 1)">
                        <div class="form-group col-md-5">
                            <input type="text" class="form-control"
                                   placeholder="Enter Ref id, mobile number, Contact Name"
                                   name="ref_id" title="Enter ref id or mobile number"
                                   ng-model="filter.search_key" style="width:100%">
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" class="form-control datepickerNormal" placeholder="From Date" name="from_date" ng-model="filter.from_date">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" class="form-control datepickerNormal" placeholder="To Date" name="to_date" ng-model="filter.to_date">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="custom-input" title="Centers">
                                <select class="form-control" ng-model="filter.centers_id" ng-disabled="!centersList.length > 0">
                                    <option value="" selected>All Centers</option>
                                    <option ng-value="item.id" value="item.id" ng-repeat="item in centersList">{{item.center_name}}</option>
                                </select>
                                <span class="ci-icon">
                                    <i ng-show="centersSpinner" class="fal fa-circle-notch fa-spin"></i>
                                    <i ng-show="!centersSpinner" class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-default">Search</button>
                        </div>
                    </form>
                </div>
            </div>


        </div>
        <div class="responsive-table" ng-hide="leadsList.length > 0">
            <div class="alert alert-danger">
                No Data found
            </div>
        </div> 

        <div class="responsive-table" ng-show="leadsList.length > 0">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120" class="no-sort">S.No</th>
                        <th class="no-sort">Agent Name</th>
                        <th class="no-sort">Customer Name</th>
                        <th class="no-sort">Customer Mobile </th>
                        <th class="no-sort">Expected Date of Checkin(dd-mm-yy) </th>
                        <th class="no-sort">Booking Id</th>

                        <th class="no-sort">Created At(dd-mm-yy)</th>
                        <th class="no-sort">Lead Status</th>

                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in leadsList track by $index">
                        <td data-label="S.No">
                            {{($index + leadsPagination.initial_id)}}
                        </td>

                        <td data-label="Contact Name">{{item.agent_name}}</td>
                        <td data-label="Mobile No">{{item.name}}</td>
                        <td data-label="Email">{{item.mobile}}</td>
                        <td data-label="Check-in Date Time">{{item.expected_date_of_check_in}}</td>
                        <td data-label="Booking Ref Number"> <a ng-href="<?= base_url() ?>reservations/quick_view?booking_id={{item.booking_ref_number}}" target="_blank">{{item.booking_ref_number}}</a></td>
                        <td data-label="Created at">{{item.created_at}}</td>
                        <td data-label="Lead Status">{{item.lead_status}}</td>

                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a ng-href="<?= base_url() ?>reservations/quick_reservation?agent_leads_id={{item.id}}" target="_blank" ng-show="item.booking_ref_number === '' || item.booking_ref_number === null">Create Booking</a></li>
                                    <li><a ng-href="<?= base_url() ?>reservations/invoice?booking_ref_number={{item.booking_ref_number}}" ng-hide="item.booking_ref_number === '' || item.booking_ref_number === null" target="_blank">View Invoice</a></li>

                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <span class="pull-right" ng-bind-html="leadsPagination.pagination"></span>
    </div>

    <div class="pageFooter" workspace-offset>&copy; Freshup Desk 2018</div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/leadsCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/leadsService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/centersService.js?r=<?= time() ?>"></script>