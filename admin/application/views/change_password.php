<section class="pageWrapper" ng-controller="changePasswordCtrl">
    <div class="pageHeader" workspace-offset  valign-parent>
        <div class="row">
            <div class="col-md-6"><strong>Change Password</strong></div>
            <div class="col-md-6">
                <div valign-holder class="text-right"></div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>
        <div class="container">
            <form ng-submit="UpdatePassword()" id="updatePasswordForm" ng-keyup="cp_error = {}">

                <div class="width-400 mx-auto-xs">
                    <div class="whitebox">
                        <div class="whiteboxHead"><strong>Change Password</strong></div>
                        <span class="text-danger" ng-bind-html="error_message"></span>
                        <div class="form-group">
                            <label>Current Password <span class="text-danger">*</span></label>
                            <input type="password" class="form-control" name="current_password" ng-model="passwordObj.current_password">
                            <label class="error" ng-show="cp_error.current_password">{{cp_error.current_password}}</label>
                        </div>
                        <div class="form-group">
                            <label>New Password <span class="text-danger">*</span></label>
                            <input type="password" class="form-control" id="password" name="new_password" ng-model="passwordObj.new_password">
                            <label class="error" ng-show="cp_error.new_password">{{cp_error.new_password}}</label>
                        </div>
                        <div class="form-group">
                            <label>Re-Enter Password <span class="text-danger">*</span></label>
                            <input type="password" class="form-control" name="confirm_new_password" ng-model="passwordObj.confirm_new_password">
                            <label class="error" ng-show="cp_error.confirm_new_password">{{cp_error.confirm_new_password}}</label>
                        </div>

                        <div class="text-right">
                            <button class="btn btn-default" type="reset" ng-click="ResetForm()"><b>Cancel <i class="fal fa-times"></i></b></button>
                            <button type="submit" class="btn btn-primary"><b class="ng-binding">Update <i class="fal fa-arrow-right"></i></b></button>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="pageFooter" workspace-offset></div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/changePasswordCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/changePasswordService.js?r=<?= time() ?>"></script>