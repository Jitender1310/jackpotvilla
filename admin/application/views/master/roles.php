<section class="pageWrapper" ng-controller="rolesCtrl">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-12"><strong>Master > Roles</strong></div>
        </div>
    </div>
    <div class="pageBody" workspace>
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Role Name</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in rolesList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Country Name">{{item.role_name}}</td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditRole(item)"><a href="#">Edit</a></li>
                                    <li ng-click="EditPrivileges(item.id)"><a href="#">Manage Privileges</a></li>
                                    <li ng-click="DeleteRole(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageSidebar">
        <form ng-submit="AddOrUpdateRole()" id="roleForm">
            <div class="SidebarHead offset">
                {{roleObj.id?'Update':'Add'}} Role
            </div>
            <div class="SidebarBody">
                <span class="text-danger" ng-bind-html="error_message"></span>
                <div class="form-group">
                    <label>Role Name</label>
                    <input type="text" class="form-control" name="role_name" ng-model="roleObj.role_name">
                    <label class="error" ng-if="r_error.role_name">{{r_error.role_name}}</label>
                </div>
            </div>
            <div class="SidebarFooter offset">
                <div class="text-right">
                    <button class="btn btn-default" type="reset" ng-click="ResetForm()"><b>Cancel <i class="fal fa-times"></i></b></button>
                    <button type="submit" class="btn btn-primary"><b>{{roleObj.id?'Update':'Add'}} <i class="fal fa-plus"></i></b></button>
                </div>
            </div>
        </form>
    </div>
    <!--popup-->
    <!-- Modal -->
    <div class="modal fade" id="privilegesModalPopup" role="dialog">
        <div class="modal-dialog modal-fluid">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Manage Privileges</h4>
                </div>
                <div class="modal-body">
                    <div class="privilege">
                        <ul class="head">
                            <li>
                                <div class="prive_row">
                                    &nbsp;
                                    <div class="checkbox-inline">
                                        <label>
                                            <input type="checkbox" value="1" ng-model="privilegie.all_permission" ng-change="ToggleSelectionForAllPrivileges()"><span></span> All
                                        </label>
                                    </div>
                                    <div class="checkbox-inline">
                                        <label>
                                            <input type="checkbox" value="1" ng-model="privilegie.delete_permission" ng-change="ToggleSelectionForColumn('delete_permission')"><span></span> Delete
                                        </label>
                                    </div>
                                    <div class="checkbox-inline">
                                        <label>
                                            <input type="checkbox" value="1" ng-model="privilegie.edit_permission" ng-change="ToggleSelectionForColumn('edit_permission')"><span></span> Edit
                                        </label>
                                    </div>

                                    <div class="checkbox-inline">
                                        <label>
                                            <input type="checkbox" value="1"  ng-model="privilegie.add_permission" ng-change="ToggleSelectionForColumn('add_permission')"><span></span> Add
                                        </label>
                                    </div>
                                    <div class="checkbox-inline">
                                        <label>
                                            <input type="checkbox" value="1"  ng-model="privilegie.view_permission" ng-change="ToggleSelectionForColumn('view_permission')"><span></span> View
                                        </label>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <script type="text/ng-template" id="categoryTree">
                            <div class="prive_row">
                            {{item.module_name}} 
                            <div class="checkbox-inline">
                            <label title="All Permissions">
                            <input type="checkbox" value="1" ng-model="item.access_rights.all_permission" ng-change="ToggleSelectionForRow(item.access_rights)"><span></span>
                            </label>
                            </div>
                            <div class="checkbox-inline">
                            <label title="Delete Permission">
                            <input type="checkbox" value="1" ng-model="item.access_rights.delete_permission" ng-change="CheckIsAllRowValuesSelected(item.access_rights)"><span></span>
                            </label>
                            </div>
                            <div class="checkbox-inline">
                            <label title="Edit Permission">
                            <input type="checkbox" value="1" ng-model="item.access_rights.edit_permission" ng-change="CheckIsAllRowValuesSelected(item.access_rights)"><span></span>
                            </label>
                            </div>
                            <div class="checkbox-inline">
                            <label title="Add Permission">
                            <input type="checkbox" value="1" ng-model="item.access_rights.add_permission" ng-change="CheckIsAllRowValuesSelected(item.access_rights)"><span></span>
                            </label>
                            </div>
                            <div class="checkbox-inline">
                            <label title="View Permission">
                            <input type="checkbox" value="1" ng-model="item.access_rights.view_permission" ng-change="CheckIsAllRowValuesSelected(item.access_rights)"><span></span>
                            </label>
                            </div>
                            </div>
                            <ul ng-if="item.sub_modules">                   
                            <li ng-repeat="item in item.sub_modules" ng-include="'categoryTree'"></li>
                            </ul>
                        </script>
                        <ul style="height: 400px; overflow-y: scroll">
                            <li ng-repeat="item in modulesList" ng-include="'categoryTree'"></li>
                        </ul>
                        </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" ng-click="ResetPrivileges()" class="btn btn-danger">Reset</button> -->
                    <button type="submit" ng-click="SavePrivileges()" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--popup end-->
    <footer-copy-right></footer-copy-right>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/rolesCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/rolesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/modulesService.js?r=<?= time() ?>"></script>