<div class="modal fade" id="view-tournament-modal-popup">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tournament Details</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <td>Category</td>
                            <td>{{viewTournamentObj.tournament_category}}  <span ng-if="viewTournamentObj.tournament_categories_id === 5">({{viewTournamentObj.premium_category}})</span></td>
                        </tr>
                        <tr>
                            <td>Title</td>
                            <td>{{viewTournamentObj.tournament_title}}</td>
                        </tr>
                        <tr>
                            <td>Allowed Club Types</td>
                            <td>{{viewTournamentObj.allowed_club_types_text}}</td>
                        </tr>
                        <tr>
                            <td>Duration</td>
                            <td>{{viewTournamentObj.duration_in_minutes}} minutes</td>
                        </tr>
                        <tr>
                            <td>Frequency</td>
                            <td>{{viewTournamentObj.frequency}}</td>
                        </tr>
                        <tr>
                            <td>Format</td>
                            <td>{{viewTournamentObj.tournament_format}}</td>
                        </tr>
                        <tr>
                            <td>Entry type</td>
                            <td>{{viewTournamentObj.entry_type}}</td>
                        </tr>
                        <tr>
                            <td>Entry Value</td>
                            <td><i class="fal fa-rupee-sign mr-10-xs"></i>{{viewTournamentObj.entry_value}}</td>
                        </tr>
                        <tr>
                            <td>Max players</td>
                            <td><i class="fal fa-users mr-10-xs"></i>{{viewTournamentObj.max_players}} </td>
                        </tr>
                        <tr>
                            <td>Min players </td>
                            <td><i class="fal fa-users mr-10-xs"></i>{{viewTournamentObj.min_players_to_start_tournament}}</td>
                        </tr>
                        <tr>
                            <td>Players Per Table <i class="fal fa-table mr-10-xs"></i></td>
                            <td>{{viewTournamentObj.players_per_table}}</td>
                        </tr>
                        <tr>
                            <td>Registration Start Date time </td>
                            <td><i class="fal fa-clock mr-10-xs"></i>{{viewTournamentObj.registration_start_date + " " + viewTournamentObj.registration_start_time}}</td>
                        </tr>
                        <tr>
                            <td>Registration Close Date time </td>
                            <td><i class="fal fa-clock mr-10-xs"></i>{{viewTournamentObj.registration_close_date}} {{viewTournamentObj.registration_close_time}}</td>
                        </tr>
                        <tr>
                            <td>Tournament Start Date time </td>
                            <td><i class="fal fa-clock mr-10-xs"></i>{{viewTournamentObj.tournament_start_date}} {{viewTournamentObj.tournament_start_time}}</td>
                        </tr>
                        <tr>
                            <td>Expected Prize</td>
                            <td>{{viewTournamentObj.expected_prize_amount}}</td>
                        </tr>
                        <tr>
                            <td>Conducting Charge</td>
                            <td>{{viewTournamentObj.tournament_commission_percentage}} %</td>
                        </tr>
<!--                        <tr>
                            <td colspan="2" class="text-center"><h4>Qualify Info</h4></td>
                        </tr>
                        <tr>
                            <td>Final Round</td>
                            <td>{{viewTournamentObj.final_round_qualify_per_table}}</td> 
                        </tr>
                        <tr>
                            <td>Semi Final Round</td>
                            <td>{{viewTournamentObj.semi_final_round_qualify_per_table}}</td> 
                        </tr>
                        <tr>
                            <td>Quarter Final Round</td>
                            <td>{{viewTournamentObj.quarter_round_qualify_per_table}}</td> 
                        </tr>
                        <tr>
                            <td>Normal Round</td>
                            <td>{{viewTournamentObj.normal_round_qualify_per_table}}</td> 
                        </tr>-->

                    </table>
                </div>
                <h4 align="center">Prize Distribution</h4>
                <div class="table-responsive" style="margin-top: 20px;">
                    <table class="table table-bordered">
                        <tr ng-repeat="p_info in viewTournamentObj.prizes_info track by $index">
                            <td>{{p_info.from_rank}} {{p_info.to_rank>0?"-"+p_info.to_rank:""}}</td> 
                            <td>{{p_info.prize_value}}</td>
                            <td>{{(p_info.to_rank>0) ? ((p_info.to_rank+1) - p_info.from_rank) * p_info.prize_value : p_info.prize_value }}</td>
                        </tr>
                    </table>
                </div>
                <h4 align="center">Qualify Info</h4>
                <div class="table-responsive" style="margin-top: 20px;">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Round</th>
                                <th>Players</th>
                                <th>Qualification</th>
                                <th>Deals</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="item in viewTournamentObj.tournament_structure">
                                <td>Round {{item.round_number}} ({{item.round_title}})</td>
                                <td>{{item.players_text}}</td>
                                <td>{{item.qualifiction_text}}</td>
                                <td>{{item.deals_text}}</td>
                            </tr>
                        </tbody>
                    </table>

                </div>
                <br/>
                <div align="center">
                    <a target="_blank"  href="<?= base_url() ?>real_tournaments/joined_players?ref_id={{viewTournamentObj.ref_id}}"><button class="btn btn-primary">Click Here To Joined Players</button></a>
                </div>
                <br/>
                Note : 
                <p>Estimated duration  : Total Rounds * 20 minutes</p>
                <p>Total Prizes  : (Joined Players * Entry Fee) - Tournament Conduction Charge Percentage</p>
            </div>
        </div>
    </div>
</div>