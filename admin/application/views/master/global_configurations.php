<section class="pageWrapper" ng-controller="globalConfigurationsCtrl">
    <div class="pageHeader" workspace-offset valign-parent >
        <div class="row">
            <div class="col-md-6"><strong>Master > Global Configuration Constants</strong></div>
            <div class="col-md-6">
                <div valign-holder class="text-right">
                    <button type="button" class="btn btn-primary" ng-click="showConstantsEditForm()"><i class="fal fa-edit"></i> Edit Constants</button>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Constant Name</th>
                        <th>Value</th>
                        <th>Updated Date</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in gamesList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Title">{{item.key_name}}</td>
                        <td data-label="Decks">{{item.value}}</td>
                        <td data-label="Updated Date">{{item.updated_at}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageFooter" workspace-offset></div>

    <div class="modal fade" id="edit_constants_modal">
        <div class="modal-dialog modal-lg" style="width:98%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Update Constants</h4>
                </div>
                <div class="modal-body">
                    <form ng-submit="updateConstants()" id="constantsForm" ng-keyup="g_error = {}">
                        <div class="col-md-12">
                            <span class="text-danger" ng-bind-html="error_message"></span>
                        </div>
                        <div class="row" id="sortable">
                            <div class="col-md-6" ng-repeat="edConst in editConstantObj track by $index">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label ng-if="$index <= 1">Key Name <span class="text-danger">*</span></label>
                                            <input type="text" readonly class="form-control" required name="key_name" ng-model="edConst.key_name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label ng-if="$index <= 1">Value <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" required name="value" ng-model="edConst.value">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <button class="btn btn-default " ng-click="ResetForm()" data-dismiss="modal"><b>Cancel <i class="fal fa-times"></i></b></button>
                        <button class="btn btn-primary pull-right" type="submit"><b>Update <i class="fal fa-arrow-right"></i></b></button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/globalConfigurationsCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/globalConfigurationsService.js?r=<?= time() ?>"></script>