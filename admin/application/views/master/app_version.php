<section class="pageWrapper" ng-controller="gamesCtrl">
    <div class="pageHeader" workspace-offset valign-parent >
        <div class="row">
            <div class="col-md-6"><strong>Master > App Version</strong></div>
            <div class="col-md-6">
                <div valign-holder class="text-right">
                    <button type="button" class="btn btn-primary" ng-click="ShowGameAddForm()"><i class="fal fa-plus"></i> Add App Version</button>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>
        
        <?php /*
        <div class="whitebox">
            <div class="row rm-5">
                <form class="" ng-submit="getGamesList(filter.page = 1)">
                    
                    <div class="col-md-3 cp-5">
                        <div class="form-group mb-0-xs"> 
                            <div class="custom-input" title="Game Type">
                                <select class="form-control" ng-model="filter.game_type">
                                    <option value="" selected>Choose Game Type</option>
                                    <option>Cash</option>
                                    <option>Practice</option>
                                </select>
                                <span class="ci-icon">
                                    <i class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-3 cp-5">
                        <div class="form-group mb-0-xs"> 
                            <div class="custom-input" title="Game Sub Type">
                                <select class="form-control" ng-model="filter.game_sub_type">
                                    <option value="" selected>Choose Game Type</option>
                                    <option>Deals</option>
                                    <option>Pool</option>
                                    <option>Points</option>
                                </select>
                                <span class="ci-icon">
                                    <i class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 cp-5">
                        <button type="submit" class="btn btn-primary "><i class="fas fa-search"></i></button>
                        <button type="reset" ng-click="ResetFilter()" class="btn btn-danger " title="Clear Search"><i class="fas fa-times-circle"></i></button>
                    </div>
                </form>
            </div>
        </div>
        
        <P><span class="text-danger">Note:</span> For cash games Prize Amount will be calculated for 
            Deals and Pools as (Entry Fee * Number of Players) - Admin Commission Percentage ( <?=ADMIN_COMMISSION_PERCENTAGE?>%)</P>
        */?>
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Version Android</th>
                        <th>Version iOS</th>
                        <th>Description</th>
                        <th>Is Force</th>
                        <th>Created Date</th>
                        <th>Updated Date</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in appVersionList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Version Android">{{item.version_android}}</td>
                        <td data-label="Version iOS">{{item.version_ios}}</td>
                        <td data-label="description">{{item.description}}</td>
                        <td data-label="Is Force">{{item.is_force == '0' ?"No":"yes"}} </td>
                        <td data-label="Created Date">{{item.created_at}}</td>
                        <td data-label="Updated Date">{{item.updated_at}}</td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditGame(item)"><a href="#">Edit</a></li>
                                    <li ng-click="DeleteGame(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageFooter" workspace-offset></div>

    <div class="modal fade" id="game-modal-popup">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{appVersionObj.id?"Update":"Add"}} App Version</h4>
                </div>
                <div class="modal-body" >
                    <label class="error">It Will Take 5 To 10 Minutes To Reflect On Server</label>
                    <form ng-submit="AddOrUpdateAppVersion()" id="appVersionForm" ng-keyup="g_error = {};">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="text-danger" ng-bind-html="error_message"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Version Android<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" required name="version_android" ng-model="appVersionObj.version_android">
                                </div>
                                <label class="error" ng-if="g_error.version_android">{{g_error.version_android}}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Version iOS<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" required name="version_ios" ng-model="appVersionObj.version_ios">
                                </div>
                                <label class="error" ng-if="g_error.version_ios">{{g_error.version_ios}}</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" required name="pool_deal_prize" ng-model="appVersionObj.description">
                                </div>
                                <label class="error" ng-if="g_error.description">{{g_error.description}}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Is Force <span class="text-danger">*</span></label>
                                    <div class="custom-input">
                                        <select class="form-control" name="active" required ng-model="appVersionObj.is_force">
                                            <option value="" selected disabled="">Choose</option>
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i class="fal fa-chevron-down"></i>
                                        </span>
                                    </div>
                                </div>
                                <label class="error" ng-if="g_error.is_force">{{g_error.is_force}}</label>
                            </div>
                        </div>

                        <button class="btn btn-default " ng-click="ResetForm()" data-dismiss="modal"><b>Cancel <i class="fal fa-times"></i></b></button>
                        <button class="btn btn-primary pull-right" type="submit"><b>{{appVersionObj.id?"Update":"Add"}} <i class="fal fa-arrow-right"></i></b></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php /* <script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/gamesCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/gamesService.js?r=<?= time() ?>"></script> */?>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/appVersionCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/appVersionService.js?r=<?= time() ?>"></script>