<section class="pageWrapper" ng-controller="statesCtrl">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-6"><strong>Master > States</strong></div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>State Name</th>
                        <th>Play Permission</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in statesList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="State Name">{{item.state_name}}</td>
                        <td data-label="Play Permission" class="{{item.play_permission_css}}">{{item.play_permission_text}}</td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditState(item)"><a href="#">Edit</a></li>
                                    <li ng-click="DeleteState(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageSidebar">
        <form ng-submit="AddOrUpdateState()" id="stateForm" ng-keyup="s_error = {}">
            <div class="SidebarHead offset">
                {{stateObj.id?'Update':'Add'}} State
            </div>
            <div class="SidebarBody">
                <span class="text-danger" ng-bind-html="error_message"></span>
                <div class="form-group">
                    <label>State Name</label>
                    <input type="text" class="form-control" name="state_name" ng-model="stateObj.state_name">
                    <label class="error" ng-if="s_error.state_name">{{s_error.state_name}}</label>
                </div>
                <div class="form-group">
                    <label>Play Permission <span class="text-danger">*</span></label>
                    <div class="custom-input">
                        <select class="form-control" name="play_permission" required ng-model="stateObj.play_permission">
                            <option value="" selected disabled="">Choose</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                        <span class="ci-icon">
                            <i class="fal fa-chevron-down"></i>
                        </span>
                    </div>
                    <label class="error" ng-if="s_error.play_permission">{{s_error.play_permission}}</label>
                </div>
            </div>

            <div class="SidebarFooter offset">
                <div class="text-right">
                    <button class="btn btn-default" type="reset" ng-click="ResetForm()"><b>Cancel <i class="fal fa-times"></i></b></button>
                    <button type="submit" class="btn btn-primary"><b>{{stateObj.id?'Update':'Add'}} <i class="fal fa-plus"></i></b></button>
                </div>
            </div>
        </form>
    </div>
    <div class="pageFooter" workspace-offset></div>
</section>

<div id="dialog-confirm" title="Confirm">
    <p>
        <span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>
        This item will be deleted. Are you sure?
    </p>
</div>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/statesCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/statesService.js?r=<?= time() ?>"></script>