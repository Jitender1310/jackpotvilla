<section class="pageWrapper" ng-controller="homepageSlidersCtrl">
    <div class="pageHeader" workspace-offset valign-parent >
        <div class="row">
            <div class="col-md-6"><strong>Homepage Slider Banners</strong></div>
            <div class="col-md-6">
                <div valign-holder class="text-right">
                    <button type="button" class="btn btn-primary" ng-click="ShowBannerAddForm()"><i class="fal fa-plus"></i> Add Banner</button>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Title</th>
                        <th>Display in</th>
                        <th>Image</th>
                        <th>Created Time</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in slidersList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Title">{{item.title}}</td>
                        <td data-label="Display In">{{item.display_in}}</td>
                        <td data-label="Image">
                            <a target="_blank" href="{{item.image}}">
                                <img src="{{item.image}}" style="width:100px;height: auto"/>
                            </a>
                        </td>
                        <td data-label="Registered Time">{{item.created_date_time}}</td>
                        <td data-label="Actions" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="DeleteSlider(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageFooter" workspace-offset></div>

    <div class="modal fade" id="slider_banner-modal-popup">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{homepageSliderObj.id?"Update":"Add"}} Avatar</h4>
                </div>
                <div class="modal-body">
                    <form ng-submit="AddOrUpdateSlider()" id="sliderForm" ng-keyup="p_error = {}">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="text-danger" ng-bind-html="error_message"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" class="form-control" name="title" ng-model="homepageSliderObj.title">
                                    <label class="error" ng-if="p_error.title">{{p_error.title}}</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Gender</label>
                                    <div class="custom-input">
                                        <select class="form-control" name="display_in" ng-model="homepageSliderObj.display_in">
                                            <option value="">Choose Display in</option>
                                            <option>Desktop Website</option>
                                            <option>Mobile Website</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i class="fal fa-chevron-down"></i>
                                        </span>
                                    </div>
                                    <label class="error" ng-if="p_error.display_in">{{p_error.display_in}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Image</label>
                                    <input type="file" class="form-control" name="image" id="image">
                                    <label class="error" ng-if="p_error.image">{{p_error.image}}</label>
                                </div>
                            </div>
                        </div>

                        <br/>
                        <br/>

                        <button class="btn btn-default " ng-click="ResetForm()" data-dismiss="modal"><b>Cancel <i class="fal fa-times"></i></b></button>
                        <button class="btn btn-primary pull-right" type="submit"><b>{{homepageSliderObj.id?"Update":"Add"}} <i class="fal fa-arrow-right"></i></b></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/homepageSlidersCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/homepageSlidersService.js?r=<?= time() ?>"></script>