<section class="pageWrapper" ng-controller="clubTypesCtrl">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-12"><strong>Master > Club Types</strong></div>
        </div>
    </div>
    <div class="pageBody" workspace>
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">Priority</th>
                        <th>Club Name</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Validity days</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in clubTypes track by $index">
                        <td data-label="SNO">{{item.priority}}</td>
                        <td data-label="Club Name">{{item.name}}</td>
                        <td data-label="From">{{item.from_points}}</td>
                        <td data-label="To">{{item.to_points}}</td>
                        <td data-label="Validity days">{{item.validity_in_days}} days</td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditClubType(item)"><a href="#">Edit</a></li>
                                    <li ng-click="DeleteClubType(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageSidebar">
        <form ng-submit="AddOrUpdateClubType()" id="clubTypeForm">
            <div class="SidebarHead offset">
                {{clubType.id?'Update':'Add'}} Role
            </div>
            <div class="SidebarBody">
                <div class="form-group">
                    <label>Club Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="name" ng-model="clubType.name">
                    <label class="error" ng-if="c_error.name">{{c_error.name}}</label>
                </div>
                <div class="form-group">
                    <label>Priority <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="priority" ng-model="clubType.priority">
                    <label class="error" ng-if="c_error.priority">{{c_error.priority}}</label>
                </div>
                <div class="form-group">
                    <label>From <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="from_points" ng-model="clubType.from_points">
                    <label class="error" ng-if="c_error.from_points">{{c_error.from_points}}</label>
                </div>
                <div class="form-group">
                    <label>To <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="to_points" ng-model="clubType.to_points">
                    <label class="error" ng-if="c_error.to_points">{{c_error.to_points}}</label>
                </div>
                <div class="form-group">
                    <label>Validity in days <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="validity_in_days" ng-model="clubType.validity_in_days">
                    <label class="error" ng-if="c_error.validity_in_days">{{c_error.validity_in_days}}</label>
                </div>
            </div>
            <div class="SidebarFooter offset">
                <div class="text-right">
                    <button class="btn btn-default" type="reset" ng-click="ResetForm()"><b>Cancel <i class="fal fa-times"></i></b></button>
                    <button type="submit" class="btn btn-primary"><b>{{clubType.id?'Update':'Add'}} <i class="fal fa-plus"></i></b></button>
                </div>
            </div>
        </form>
    </div>
    <footer-copy-right></footer-copy-right>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/clubTypesCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/clubTypesService.js?r=<?= time() ?>"></script>