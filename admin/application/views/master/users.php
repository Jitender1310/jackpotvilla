<section class="pageWrapper" ng-controller="usersCtrl">
    <div class="pageHeader" workspace-offset valign-parent >
        <div class="row">
            <div class="col-md-6"><strong>Master > Employees</strong></div>
            <div class="col-md-6">
                <div valign-holder class="text-right">
                    <button type="button" class="btn btn-primary" ng-click="ShowUserAddForm()"><i class="fal fa-plus"></i> Add Employee</button>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="GetUsersList();">
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Role</th>
                        <th>Fullname</th>
                        <th>Username</th>
                        <th>Login Status</th>
                        <th>Joined Date</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in usersList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Role">{{item.role_name}}</td>
                        <td data-label="Username">{{item.fullname}}</td>
                        <td data-label="Username">{{item.username}}</td>
                        <td data-label="Login Status">{{item.login_status_text}}</td>
                        <td data-label="Joined Date">{{item.user_meta.joining_date}}</td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditUser(item)"><a href="#">Edit</a></li>
                                    <li ng-click="EditPrivileges(item)" ng-if="item.login_status_text === 'Yes'"><a href="#">Manage Privileges</a></li>
                                    <li ng-if="item.role_id === '24'" ng-click="ShowUserAgentTypesAccesForm(item)"><a href="#">Access To Agent Types</a></li>
                                    <li ng-click="ViewUser(item)"><a href="#">View</a></li>
                                    <li ng-click="DeleteUser(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageFooter" workspace-offset></div>

    <div class="modal fade" id="user-modal-popup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{userObj.id?"Update":"Add"}} Employee</h4>
                </div>
                <div class="modal-body">
                    <form ng-submit="AddOrUpdateUser()" id="userForm" >
                        <div class="row">
                            <div class="col-md-12">
                                <span class="text-danger" ng-bind-html="error_message"></span>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Role</label>
                                    <div class="custom-input">
                                        <select class="form-control" name="role_id" ng-model="userObj.role_id" ng-disabled="!rolesList.length > 0">
                                            <option value="" selected disabled="">Choose Role</option>
                                            <option value="{{item.id}}" ng-repeat="item in rolesList track by $index">{{item.role_name}}</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i ng-show="!rolesSpinner" class="fal fa-chevron-down"></i>
                                            <i ng-show="rolesSpinner" class="fal fa-circle-notch fa-spin"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Login Status</label>
                                    <div class="custom-input">
                                        <select class="form-control" name="login_status" ng-model="userObj.login_status">
                                            <option value="">Choose Status</option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i class="fal fa-chevron-down"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Fullname</label>
                                    <input type="text" class="form-control" name="fullname" ng-model="userObj.fullname">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4" ng-show="userObj.login_status == 1">
                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" class="form-control" name="username" ng-model="userObj.username">
                                </div>
                            </div>
                            <div class="col-md-4" ng-show="userObj.login_status == 1">
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="{{userObj.id?'':'password'}}" id="password" ng-model="userObj.password">
                                    <span ng-if="userObj.id">Leave Blank if you don't want to update</span>
                                </div>
                            </div>
                            <div class="col-md-4" ng-show="userObj.login_status == 1">
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input type="password" class="form-control" name="{{userObj.id?'':'confirm_password'}}" ng-model="userObj.confirm_password">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Designation</label>
                                    <input type="text" class="form-control" name="designation" ng-model="userObj.user_meta.designation">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Mobile</label>
                                    <input type="tel" class="form-control number" maxlength="10" minlength="10" name="mobile" ng-model="userObj.user_meta.mobile">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email" ng-model="userObj.user_meta.email">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Payroll Status</label>
                                    <div class="custom-input">
                                        <select class="form-control" name="payroll_status" ng-model="userObj.user_meta.payroll_status">
                                            <option value="">Choose Status</option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i class="fal fa-chevron-down"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Gender</label>
                                    <div class="custom-input">
                                        <select class="form-control" name="gender" ng-model="userObj.user_meta.gender">
                                            <option value="">Choose Gender</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                            <option value="Other">Other</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i class="fal fa-chevron-down"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Date of Birth (DD-MM-YYYY)</label>
                                    <input type="text" class="form-control datepickerNormal" name="dob" ng-model="userObj.user_meta.dob">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" ng-show="userObj.user_meta.payroll_status == 1">
                                <div class="form-group">
                                    <label><abbr title="Cost to company">CTC</abbr></label>
                                    <input type="text" class="form-control" name="ctc" ng-model="userObj.user_meta.ctc">
                                </div>
                            </div>
                            <div class="col-md-6" ng-show="userObj.user_meta.payroll_status == 1">
                                <div class="form-group">
                                    <label>Net Salary</abbr></label>
                                    <input type="text" class="form-control" name="net_salary" ng-model="userObj.user_meta.net_salary">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea class="form-control" rows="4" name="address" ng-model="userObj.user_meta.address"></textarea>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Date of Joining (DD/MM/YYYY)</label>
                                    <input type="text" class="form-control datepickerNormal" name="joining_date" ng-model="userObj.user_meta.joining_date">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><abbr title="Permanent Account Number">PAN</abbr> Card Number</label>
                                    <input type="text" class="form-control" name="pan_no" ng-model="userObj.user_meta.pan_no">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><abbr title="Employees' State Insurance">ESI</abbr> Number</label>
                                    <input type="text" class="form-control" name="esi_no" ng-model="userObj.user_meta.esi_no">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><abbr title="Provident Fund">PF</abbr> Number</label>
                                    <input type="text" class="form-control" name="pf_no" ng-model="userObj.user_meta.pf_no">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><abbr title="Driving">License</abbr> Number</label>
                                    <input type="text" class="form-control" name="license_no" ng-model="userObj.user_meta.license_no">
                                </div>
                            </div>
                        </div>
                        <div class="row" ng-show="userObj.user_meta.payroll_status == 1">
                            <div class="col-md-12">
                                <div class="title-line text-left"><span class="h4">Bank Details</span></div>
                            </div>
                        </div>
                        <div class="row" ng-show="userObj.user_meta.payroll_status == 1">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Bank Name</label>
                                    <input type="text" class="form-control" name="bank_name" ng-model="userObj.user_meta.bank_name">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Branch Name</label>
                                    <input type="text" class="form-control" name="branch_name" ng-model="userObj.user_meta.branch_name">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Account Number</label>
                                    <input type="text" class="form-control" name="account_no" ng-model="userObj.user_meta.account_no">
                                </div>
                            </div>
                        </div>
                        <div class="row" ng-show="userObj.user_meta.payroll_status == 1">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><abbr title="Indian Financial System Code">IFSC</abbr> Code</label>
                                    <input type="text" class="form-control" name="ifsc_code" ng-model="userObj.user_meta.ifsc_code">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><abbr title="Magnetic Ink Character Recognition">MICR</abbr> Code</label>
                                    <input type="text" class="form-control" name="micr_code" ng-model="userObj.user_meta.micr_code">
                                </div>
                            </div>

                        </div>

                        <button class="btn btn-default " ng-click="ResetForm()" data-dismiss="modal"><b>Cancel <i class="fal fa-times"></i></b></button>
                        <button class="btn btn-primary pull-right" type="submit"><b>{{userObj.id?"Update":"Add"}} <i class="fal fa-arrow-right"></i></b></button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="user-agent-types-access-modal-popup">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Agent Types Access For - ({{viewObj.fullname}})</h4>
                </div>
                <div class="modal-body">
                    <form ng-submit="AddOrUpdateAgentTypesAccess()" id="agent_type_access_for_user_form" method="POST">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="text-danger" ng-bind-html="error_message"></span>
                            </div>
                            <input type="hidden" ng-model="agentTypeAccessObj.users_id"  >
                            <ul class="list-group" ng-repeat="item in userAgentTypesList track by $index">
                                <li class="list-group-item">
                                    {{item.agent_type}}
                                    <div class="material-switch pull-right">
                                        <input name="agent_types_id" type="checkbox"  ng-model="item.is_checked" ng-checked="item.is_checked"/>
                                        <label for="agent_types_id" class="label-default"></label>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <button class="btn btn-default " ng-click="ResetForm()" data-dismiss="modal"><b>Cancel <i class="fal fa-times"></i></b></button>
                        <button class="btn btn-primary pull-right" type="submit"><b>Update <i class="fal fa-arrow-right"></i></b></button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="view-user-modal-popup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Employee Details</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="width-700 mx-auto-xs">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Full Name</th>
                                        <td>
                                            {{userObj.fullname}}
                                        </td>
                                    </tr>
                                    <tr ng-if="userObj.user_meta.payroll_status == 1">
                                        <th>Email</th>
                                        <td>
                                            {{userObj.user_meta.payroll_status_text}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Mobile</th>
                                        <td>
                                            {{userObj.user_meta.mobile}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Role</th>
                                        <td>
                                            {{userObj.role_name}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Login Status</th>
                                        <td>
                                            {{userObj.login_status_text}}
                                        </td>
                                    </tr>
                                    <tr ng-if="userObj.login_status == 1">
                                        <th>Username</th>
                                        <td>
                                            {{userObj.username}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Designation</th>
                                        <td>
                                            {{userObj.user_meta.designation}}
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>Gender</th>
                                        <td>
                                            {{userObj.user_meta.gender}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Date of Birth (DD/MM/YYYY)</th>
                                        <td>
                                            {{userObj.user_meta.dob}}
                                        </td>
                                    </tr>
                                    <tr  ng-show="userObj.user_meta.payroll_status == 1">
                                        <th>CTC</th>
                                        <td>
                                            {{userObj.user_meta.ctc}}
                                        </td>
                                    </tr>
                                    <tr  ng-show="userObj.user_meta.payroll_status == 1">
                                        <th>Net Salary</th>
                                        <td>
                                            {{userObj.user_meta.net_salary}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Address</th>
                                        <td>
                                            {{userObj.user_meta.address}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Date of Joining (DD/MM/YYYY)</th>
                                        <td>
                                            {{userObj.user_meta.joining_date}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th><abbr title="Permanent Account Number">PAN</abbr> Card Number</th>
                                        <td>
                                            {{userObj.user_meta.pan_no}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th><abbr title="Employees' State Insurance">ESI</abbr> Number</th>
                                        <td>
                                            {{userObj.user_meta.esi_no}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th><abbr title="Provident Fund">PF</abbr> Number</th>
                                        <td>
                                            {{userObj.user_meta.pf_no}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th><abbr title="Driving">License</abbr> Number</th>
                                        <td>
                                            {{userObj.user_meta.license_no}}
                                        </td>
                                    </tr>

                                    <tr ng-show="userObj.user_meta.payroll_status == 1">
                                        <th colspan="2">Bank Details</th>
                                    </tr>

                                    <tr  ng-show="userObj.user_meta.payroll_status == 1">
                                        <th>Bank Name</th>
                                        <td>
                                            {{userObj.user_meta.bank_name}}
                                        </td>
                                    </tr>
                                    <tr  ng-show="userObj.user_meta.payroll_status == 1">
                                        <th>Branch Name</th>
                                        <td>
                                            {{userObj.user_meta.branch_name}}
                                        </td>
                                    </tr>
                                    <tr  ng-show="userObj.user_meta.payroll_status == 1">
                                        <th>Account Number</th>
                                        <td>
                                            {{userObj.user_meta.account_no}}
                                        </td>
                                    </tr>
                                    <tr  ng-show="userObj.user_meta.payroll_status == 1">
                                        <th><abbr title="Indian Financial System Code">IFSC</abbr> Code</th>
                                        <td>
                                            {{userObj.user_meta.ifsc_code}}
                                        </td>
                                    </tr>
                                    <tr  ng-show="userObj.user_meta.payroll_status == 1">
                                        <th><abbr title="Magnetic Ink Character Recognition">MICR</abbr> Code</th>
                                        <td>
                                            {{userObj.user_meta.micr_code}}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-danger " data-dismiss="modal"><b>Close <i class="fal fa-times"></i></b></button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="privilegesModalPopup" role="dialog">
        <div class="modal-dialog modal-fluid">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Manage Privileges</h4>
                </div>
                <div class="modal-body">
                    <div class="privilege">
                        <ul class="head">
                            <li>
                                <div class="prive_row">
                                    &nbsp;
                                    <div class="checkbox-inline">
                                        <label>
                                            <input type="checkbox" value="1" ng-model="privilegie.delete_permission" ng-change="ToggleSelectionForColumn('delete_permission')"><span></span> Delete
                                        </label>
                                    </div>
                                    <div class="checkbox-inline">
                                        <label>
                                            <input type="checkbox" value="1" ng-model="privilegie.edit_permission" ng-change="ToggleSelectionForColumn('edit_permission')"><span></span> Edit
                                        </label>
                                    </div>

                                    <div class="checkbox-inline">
                                        <label>
                                            <input type="checkbox" value="1"  ng-model="privilegie.add_permission" ng-change="ToggleSelectionForColumn('add_permission')"><span></span> Add
                                        </label>
                                    </div>
                                    <div class="checkbox-inline">
                                        <label>
                                            <input type="checkbox" value="1"  ng-model="privilegie.view_permission" ng-change="ToggleSelectionForColumn('view_permission')"><span></span> View
                                        </label>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <script type="text/ng-template" id="categoryTree">
                            <div class="prive_row">
                            {{item.module_name}}
                            <div class="checkbox-inline">
                            <label title="Delete Permission">
                            <input type="checkbox" value="1" ng-model="item.access_rights.delete_permission" ng-change="CheckIsAllRowValuesSelected(item.access_rights)"><span></span>
                            </label>
                            </div>
                            <div class="checkbox-inline">
                            <label title="Edit Permission">
                            <input type="checkbox" value="1" ng-model="item.access_rights.edit_permission" ng-change="CheckIsAllRowValuesSelected(item.access_rights)"><span></span>
                            </label>
                            </div>
                            <div class="checkbox-inline">
                            <label title="Add Permission">
                            <input type="checkbox" value="1" ng-model="item.access_rights.add_permission" ng-change="CheckIsAllRowValuesSelected(item.access_rights)"><span></span>
                            </label>
                            </div>
                            <div class="checkbox-inline">
                            <label title="View Permission">
                            <input type="checkbox" value="1" ng-model="item.access_rights.view_permission" ng-change="CheckIsAllRowValuesSelected(item.access_rights)"><span></span>
                            </label>
                            </div>
                            </div>
                            <ul ng-if="item.sub_modules">
                            <li ng-repeat="item in item.sub_modules" ng-include="'categoryTree'"></li>
                            </ul>
                        </script>
                        <ul>
                            <li ng-repeat="item in modulesList" ng-include="'categoryTree'"></li>
                        </ul>
                        </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" ng-click="SavePrivileges()" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--popup end-->

</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/usersCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/usersService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/rolesService.js?r=<?= time() ?>"></script>