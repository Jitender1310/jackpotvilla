<!-- Modal -->
<div class="modal fade" id="tournamentDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Tournament Details - {{tournamentDetailsObj.tournament_title}} (#{{tournamentDetailsObj.ref_id}})</h4>
            </div>
            <div class="modal-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tournamentTimings" aria-controls="tournamentTimings" role="tab" data-toggle="tab">Timings</a>
                        </li>
                        <li role="presentation"><a href="#tournamentSummary" aria-controls="tournamentSummary" role="tab" data-toggle="tab">Summary</a>
                        </li>
                        <li role="presentation"><a href="#tournamentStructure" aria-controls="tournamentStructure" role="tab" data-toggle="tab">Structure</a>
                        </li>
                        <li role="presentation"><a href="#tournamentPrizes" aria-controls="tournamentPrizes" role="tab" data-toggle="tab">Prizes</a>
                        </li>
                        <li ng-show="tournamentDetailsObj.tournament_status === 'Completed' || tournamentDetailsObj.tournament_status === 'Running'" role="presentation" ng-click="GetGameProgress(tournamentDetailsObj.id);"><a href="#tournamentProgress" aria-controls="tournamentProgress" role="tab" data-toggle="tab">Progress</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="tournamentTimings">
                            <div class="clearfix"></div>
                            <div style="padding-top: 100px;padding-bottom: 100px;font-weight: bold;color: green;font-size: 20px;">
                                <div class="col-md-4">
                                    <p  align="center">Registration Starts</p>
                                    <h5 align="center">{{tournamentDetailsObj.registration_start_date}}  </h5>
                                    <h5 align="center">{{tournamentDetailsObj.registration_start_time}}  </h5>

                                </div>
                                <div class="col-md-4">
                                    <p align="center">Registration Closes</p>
                                    <h5 align="center">{{tournamentDetailsObj.registration_close_date}} </h5>
                                    <h5 align="center">{{tournamentDetailsObj.registration_close_time}}  </h5>

                                </div>
                                <div class="col-md-4">
                                    <p align="center">Tournament Starts</p>
                                    <h5 align="center">{{tournamentDetailsObj.tournament_start_date}} </h5>
                                    <h5 align="center">{{tournamentDetailsObj.tournament_start_time}}  </h5>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tournamentSummary">
                            <div class="table-responsive" style="margin-top: 20px;">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th >Name</th>
                                            <th>Duration</th>
                                            <th>Format</th>
                                            <th>Frequency</th>
                                            <th>Entry</th>
                                            <th>M.Players/Table</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{tournamentDetailsObj.tournament_title}} (#{{tournamentDetailsObj.ref_id}})</td>
                                            <td>{{tournamentDetailsObj.duration_in_minutes}}</td>
                                            <td>{{tournamentDetailsObj.tournament_format}}</td>
                                            <td>{{tournamentDetailsObj.frequency}}</td>
                                            <td>{{tournamentDetailsObj.entry_value}}</td>
                                            <td>{{tournamentDetailsObj.players_per_table}}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="5"><h3>Expected Prize:  <span style="color:green;">{{tournamentDetailsObj.expected_prize_amount}} Rs /-</span></h3></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><h5>Total Prizes :  <span style="color:green;">{{tournamentDetailsObj.prize_distributions.length}}</span></h5></td>
                                            <td colspan="2"><h5>Total Rounds :  <span style="color:green;">{{tournamentDetailsObj.tournament_structure.length}}</span></h5></td>
                                            <td colspan="2"><h5>Prizes Start from Round 1</h5></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tournamentStructure">
                            <div class="table-responsive" style="margin-top: 20px;">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th >Round</th>
                                            <th>Players</th>
                                            <th>Qualification</th>
                                            <th>Deals</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item in tournamentDetailsObj.tournament_structure">
                                            <td>Round {{item.round_number}} ({{item.round_title}})</td>
                                            <td>{{item.players_text}}</td>
                                            <td>{{item.qualifiction_text}}</td>
                                            <td>{{item.deals_text}}</td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tournamentPrizes">
                            <div class="table-responsive" style="margin-top: 20px;">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th >Position</th>
                                            <th>Players</th>
                                            <th>Prize</th>
                                            <th>Given in Round</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item in tournamentDetailsObj.prize_distributions">
                                            <td>{{item.display_position}}</td>
                                            <td>{{item.no_of_players}}</td>
                                            <td>{{item.prize_value}}</td>
                                            <td>Round 2 (Final)</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <style>
                            .nopad {
                                padding-left:0px;
                                padding-right:0px;
                            }
                            .clr {
                                background-color: #ffc107;
                            }
                        </style>
                        <div role="tabpanel" class="tab-pane" id="tournamentProgress">
                            <div class="row " style="margin-top: 20px;">
                                <div class="col-sm-3 nopad" >
                                    <table class="table table-bordered" >
                                        <tr class="bg-light" style="width: 100%;">
                                            <th >Round</th>
                                        </tr>
                                        <tr ng-repeat="item in gameProgress track by $index" style="cursor:pointer;" ng-click="GetTablesInfo(item);" ng-class="item.round_number === roundno ? 'clr' : '';">
                                            <td >Round {{item.round_number}}</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-sm-3 nopad" >
                                    <table class="table table-bordered" >
                                        <tr class="bg-light">
                                            <th >Table</th>
                                            <th >Deal</th>
                                        </tr>
                                        <tr ng-repeat="item1 in tablesInfo" style="cursor:pointer;" ng-if="tablesInfo.length > 0" ng-click="GetPlayers(item1);" ng-class="item1.tournamentTableId === tableid ? 'clr' : '';">
                                            <td>{{item1.tournamentTableId}}</td>
                                            <td>{{item1.players.length}} / 6</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-sm-6 nopad" >
                                    <table class="table table-bordered" >
                                        <tr class="bg-light">
                                            <th >Players</th>
                                            <th >Chips</th>
                                            <th >Status</th>
                                        </tr>
                                        <tr ng-repeat="item2 in playersInfo" ng-if="playersInfo.length > 0" >
                                            <td>{{item2.player_name}}</td>
                                            <td>{{item2.chips}}</td>
                                            <td ng-if="item2.isQualified">Qualified</td>
                                            <td ng-if="!item2.isQualified">Not Qualified</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>