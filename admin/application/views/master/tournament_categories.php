<section class="pageWrapper" ng-controller="tournamentCategoriesCtrl">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-12"><strong>Master > Tournament Categories</strong></div>
        </div>
    </div>
    <div class="pageBody" workspace>
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Tournament Category</th>
                        <th>Image</th>
                        <th>Priority</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in tournamentCategoires track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Tournament Category">{{item.name}}</td>
                        <td data-label="Image">
                            <a target="_blank" href="{{item.image}}">
                                <img src="{{item.image}}" style="width:100px;height: auto"/>
                            </a>
                        </td>
                        <td data-label="Priority">{{item.priority}}</td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditTournamentCategory(item)"><a href="#">Edit</a></li>
                                    <li ng-click="DeleteTournamentCategory(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageSidebar">
        <form ng-submit="AddOrUpdateTournamentCategory()" id="tournamentCategoryForm">
            <div class="SidebarHead offset">
                {{tournamentCategory.id?'Update':'Add'}} Tournament Category
            </div>
            <div class="SidebarBody">
                <div class="form-group">
                    <label>Tournament Category Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="name" ng-model="tournamentCategory.name">
                    <label class="error" ng-if="c_error.name">{{c_error.name}}</label>
                </div>

                <div class="form-group">
                    <label>Priority <span class="text-danger">*</span></label>
                    <input type="text" class="form-control number" name="priority" ng-model="tournamentCategory.priority">
                    <label class="error" ng-if="c_error.priority">{{c_error.priority}}</label>
                </div>
                <div class="form-group">
                    <label>Image</label>
                    <input type="file" class="form-control" name="image" id="image">
                    <label class="error" ng-if="p_error.image">{{p_error.image}}</label>
                </div>
                <p>Preferred Dimensions : Width : 300px; height: 100px</p>
            </div>

            <div class="SidebarFooter offset">
                <div class="text-right">
                    <button class="btn btn-default" type="reset" ng-click="ResetForm()"><b>Cancel <i class="fal fa-times"></i></b></button>
                    <button type="submit" class="btn btn-primary"><b>{{tournamentCategory.id?'Update':'Add'}} <i class="fal fa-plus"></i></b></button>
                </div>
            </div>
        </form>
    </div>
    <footer-copy-right></footer-copy-right>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/tournamentCategoriesCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/tournamentCategoriesService.js?r=<?= time() ?>"></script>