<section class="pageWrapper" ng-controller="createRealTournamentCtrl" ng-init="tournaments_id = '<?= $_GET['tournaments_id'] ?>';">
    <div class="pageHeader" workspace-offset valign-parent >
        <div class="row">
            <div class="col-md-6"><strong>Create Real Tournament</strong></div>
            <div class="col-md-6">
                <div valign-holder class="text-right">

                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace >
        <div class="col-sm-10 col-sm-offset-1" style="margin-bottom: 45px;background-color: #fff;padding: 25px;" >
            <form  ng-submit="AddOrUpdateClonedTournaments()" id="tournamentForm" ng-keyup="t_error = {}; calculateExpectedPrize()">
                <div class="row">
                    <div class="col-md-12">
                        <span class="text-danger" ng-bind-html="error_message"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Category <span class="text-danger">*</span></label>
                            <div class="custom-input">
                                <select class="form-control" name="tournament_categories_id" ng-model="tournamentObj.tournament_categories_id" required >
                                    <option value="" selected disabled="">Choose Category</option>
                                    <option  ng-repeat="item in tournamentCategoires track by $index" ng-value="{{item.id}}">{{item.name}}</option>
                                </select>
                                <span class="ci-icon">
                                    <i class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                        <label class="error" ng-if="t_error.game_type">{{t_error.game_type}}</label>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tournament Title <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" required name="tournament_title" ng-model="tournamentObj.tournament_title">
                        </div>
                        <label class="error" ng-if="t_error.tournament_title">{{t_error.tournament_title}}</label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6" ng-hide="tournamentObj.tournament_categories_id === 5">
                        <div class="form-group">
                            <label>Allowed <span class="text-danger">*</span></label>
                            <div class="custom-input">
                                <select class="form-control" ng-model="tournamentObj.allowed" required name="allowed">
                                    <option value="">Choose</option>
                                    <option>All</option>
                                    <option>For Selected Clubs</option>
                                </select>
                                <span class="ci-icon">
                                    <i class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                        <label class="error" ng-if="t_error.allowed">{{t_error.allowed}}</label>
                    </div>
                    <div class="col-md-6" ng-show="tournamentObj.tournament_categories_id === 5">
                        <div class="form-group">
                            <label>Premium Category<span class="text-danger">*</span></label>
                            <div class="custom-input">
                                <select class="form-control" ng-model="tournamentObj.premium_category" required name="premium_category">
                                    <option value="">Choose Premium Category</option>
                                    <option value="Bronze" >Bronze (25)</option>
                                    <option value="Silver" >Silver (50)</option>
                                    <option value="Gold" >Gold (100)</option>
                                    <option value="Diamond" >Diamond (500)</option>
                                </select>
                                <span class="ci-icon">
                                    <i class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                        <label class="error" ng-if="t_error.allowed">{{t_error.allowed}}</label>
                    </div>
                    <div class="col-md-6" ng-if="tournamentObj.allowed == 'For Selected Clubs'">
                        <div class="form-group">
                            <label>Club Types <span class="text-danger">*</span></label>
                            <div class="custom-input">
                                <select class="form-control" ng-model="tournamentObj.allowed_club_types" name="allowed_club_types" multiple="">
                                    <option value="" selected disabled="">Choose</option>
                                    <option ng-repeat="item in clubTypes track by $index" ng-value="item.id">{{item.name}}</option>
                                </select>
                                <span class="ci-icon">
                                    <i class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                        <label class="error" ng-if="t_error.allowed_club_types">{{t_error.allowed_club_types}}</label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="title-line text-left"><span class="h4">Frequency & Dates</span></div>
                    </div>
                </div>

                <div class="row" >
                    <div class="col-md-2" style="display:none;">
                        <div class="form-group">
                            <label>Frequency <span class="text-danger">*</span></label>
                            <div class="custom-input">
                                <select class="form-control" ng-model="tournamentObj.frequency" required name="frequency">
                                    <option value="" disabled>Choose</option>
                                    <option>Monthly</option>
                                    <option>Weekly</option>
                                    <option>Daily</option>
                                    <option ng-hide="tournamentObj.tournament_categories_id === 5">One Time</option>
                                </select>
                                <span class="ci-icon">
                                    <i class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                        <label class="error" ng-if="t_error.frequency">{{t_error.frequency}}</label>
                    </div>

                    <div class="col-md-3" ng-if="tournamentObj.frequency == 'Weekly'" style="display:none;">
                        <div class="form-group">
                            <label>Week<span class="text-danger">*</span></label>
                            <div class="custom-input">
                                <select class="form-control" ng-model="tournamentObj.week" required name="week">
                                    <option value="" disabled>Choose</option>
                                    <option ng-repeat="witem in weeks">{{witem}}</option>                                        
                                </select>
                                <span class="ci-icon">
                                    <i class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                        <label class="error" ng-if="t_error.week">{{t_error.week}}</label>
                    </div>
                    <input type="hidden" ng-model="tournamentObj.no_of_bots" name="no_of_bots" value="0" required>
                    <div class="col-md-6" style="display: none;">
                        <div class="form-group">
                            <label>Winning Probability To <span class="text-danger">*</span></label>
                            <div class="custom-input">
                                <input type="text" class="form-control" ng-model="tournamentObj.winning_probability_to" required name="winning_probability_to" value="Real Players">
                                <span class="ci-icon">
                                    <i class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3" >
                        <div class="form-group">
                            <label>Registration Start Date <span class="text-danger">*</span></label>
                            <input type="text" class="form-control datepickerFrom" ng-model="tournamentObj.registration_start_date" name="registration_start_date" required>
                        </div>
                        <label class="error" ng-if="t_error.registration_start_date">{{t_error.registration_start_date}}</label>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Registration Start Time <span class="text-danger">*</span></label>
                            <input type="text" class="form-control timepicker" ng-model="tournamentObj.registration_start_time" name="registration_start_time" required>
                        </div>
                        <label class="error" ng-if="t_error.registration_start_time">{{t_error.registration_start_time}}</label>
                    </div>

                    <div class="col-md-3" >
                        <div class="form-group">
                            <label>Registration Closes Date <span class="text-danger">*</span></label>
                            <input type="text" class="form-control datepickerFrom" ng-model="tournamentObj.registration_close_date" name="registration_close_date" required>
                        </div>
                        <label class="error" ng-if="t_error.registration_close_date">{{t_error.registration_close_date}}</label>
                    </div>

                    <div class="col-md-3" >
                        <div class="form-group">
                            <label>Registration Closes Time <span class="text-danger">*</span></label>
                            <input type="text" class="form-control timepicker" ng-model="tournamentObj.registration_close_time" name="registration_close_time" required>
                        </div>
                        <label class="error" ng-if="t_error.registration_close_time">{{t_error.registration_close_time}}</label>
                    </div>

                    <div class="col-md-3" >
                        <div class="form-group">
                            <label>Tournament Start Date <span class="text-danger">*</span></label>
                            <input type="text" class="form-control datepickerFrom" ng-model="tournamentObj.tournament_start_date" name="tournament_start_date" required>
                        </div>
                        <label class="error" ng-if="t_error.tournament_start_date">{{t_error.tournament_start_date}}</label>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Tournament Start Time <span class="text-danger">*</span></label>
                            <input type="text" class="form-control timepicker" ng-model="tournamentObj.tournament_start_time" name="tournament_start_time" required>
                        </div>
                        <label class="error" ng-if="t_error.tournament_start_date">{{t_error.tournament_start_time}}</label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="title-line text-left"><span class="h4">Entry Fee and Tournament conducting Admin Charge</span></div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Entry Fee Type <span class="text-danger">*</span></label>
                            <div class="custom-input">
                                <select class="form-control" name="entry_type" required ng-model="tournamentObj.entry_type" ng-change="ChangeComission(); calculateExpectedPrize();">
                                    <option value="" selected disabled="">Choose</option>
                                    <option value="Cash">Cash</option>
                                    <option value="Free">Free</option>
                                </select>
                                <span class="ci-icon">
                                    <i class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                        <label class="error" ng-if="t_error.entry_type">{{t_error.entry_type}}</label>
                    </div>

                    <div class="col-md-6" ng-if="tournamentObj.entry_type == 'Free'">
                        <div class="form-group">
                            <label>Entry Fee (Given by Admin for prize calculation only)<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" required ng-model="tournamentObj.entry_fee">
                        </div>
                        <label class="error" ng-if="t_error.entry_fee">{{t_error.entry_fee}}</label>
                    </div>

                    <div class="col-md-6" ng-if="tournamentObj.entry_type == 'Cash'">
                        <div class="form-group">
                            <label>Entry Fee (Cash)<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" required name="entry_value" ng-model="tournamentObj.entry_value">
                        </div>
                        <label class="error" ng-if="t_error.entry_value">{{t_error.entry_value}}</label>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Max Players in Tournament<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" required name="max_players" ng-model="tournamentObj.max_players">
                        </div>
                        <label class="error" ng-if="t_error.max_players">{{t_error.max_players}}</label>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Min Players to Start Tournament<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" required name="min_players_to_start_tournament" ng-model="tournamentObj.min_players_to_start_tournament">
                        </div>
                        <label class="error" ng-if="t_error.min_players_to_start_tournament">{{t_error.min_players_to_start_tournament}}</label>
                    </div>


                    <div class="col-md-6" ng-if="tournamentObj.entry_type != 'Free'">
                        <div class="form-group">
                            <label>(%) Charge for conducting Tournament <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" ng-model="tournamentObj.tournament_commission_percentage" required>
                        </div>
                        <label class="error" ng-if="t_error.tournament_commission_percentage">{{t_error.tournament_commission_percentage}}</label>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="title-line text-left"><span class="h4">Prize info</span></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Prize Type <span class="text-danger">*</span></label>
                            <div class="custom-input">
                                <select class="form-control" name="prize_type" required ng-model="tournamentObj.prize_type">
                                    <option value="" selected disabled="">Choose</option>
                                    <!--<option>Fixed Prize</option>-->
                                    <option>Depend on Players Joined</option>
                                </select>
                                <span class="ci-icon">
                                    <i class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                        <label class="error" ng-if="t_error.entry_type">{{t_error.entry_type}}</label>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Expected Prize Amount<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="expected_prize_amount" ng-model="tournamentObj.expected_prize_amount" required>
                        </div>
                        <label class="error" ng-if="t_error.expected_prize_amount">{{t_error.expected_prize_amount}}</label>
                    </div>
                </div>

                <h3 class="hidden">Tournament Structure</h3>

                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Round</th>
                                        <th>Qualify Per Table</th>
                                        <th><?php // % of bots should win from this round ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="p_row in tournamentObj.tournament_structure track by $index">
                                        <td>Round {{$index + 1}}</td>
                                        <td><input type="number" min="1" class="form-control" required ng-model="p_row.qualify"></td>
                                        <td><input type="hidden" value="0" min="1" class="form-control" required ng-model="p_row.winning_bot_percentage_on_round"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><strong>Total Rounds : {{tournamentObj.tournament_structure.length}}</strong></td>
                                        <td><button class="btn btn-primary pull-right" type="button" ng-click="AddTournamentRow()">Add Round</button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <h3>Prize Positions</h3>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>From Position</th>
                                        <th>To Position</th>
                                        <th>Minimum Prize</th>
                                        <th>Player Prize Cash</th>
                                        <th>Total Prize</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="p_row in tournamentObj.prizes_info track by $index">
                                        <td>{{$index + 1}}</td>
                                        <td><input type="number" class="form-control" min="1" required ng-model="p_row.from_rank"></td>
                                        <td><input type="number" class="form-control" min="1" required ng-model="p_row.to_rank"></td>
                                        <td><input type="number" class="form-control"  min="1" required ng-model="p_row.min_prize_value"></td>
                                        <td><input type="number" class="form-control"  min="1" required ng-model="p_row.prize_value"></td>
                                        <td>
                                            {{p_row.row_total_prize}}
                                        </td>
                                        <td><button type="button" class="btn btn-danger" ng-click="RemovePrizeRow($index)">Remove</button></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            Expected Prize Amount {{tournamentObj.expected_prize_amount}}
                                        </td>
                                        <td>Total Prize</td>
                                        <td>{{totalPrize}}</td>
                                        <td><button class="btn btn-primary pull-right" type="button" ng-click="AddPrizeRow()">Add Row</button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <br/>
                <p class="text-danger" ng-show="tournamentObj.expected_prize_amount != totalPrize">To create tournament Expected Prize should match with Total Prize Distributions</p>
                <br/>
                <div class="col-md-12" align="center">
                    <button class="btn btn-primary btn-lg" type="submit"
                            ng-disabled="tournamentObj.expected_prize_amount != totalPrize"><b>{{tournamentObj.id?"Create Real Tournament":"Create Real Tournament"}} <i class="fal fa-arrow-right"></i></b></button>
                </div>

            </form>
            Note :
            <br />
            Estimated duration : Total Rounds * 20 minutes
            <br />
            Total Prizes : (Joined Players * Entry Fee) - Tournament Conduction Charge Percentage
        </div>
    </div>
    <div class="pageFooter" workspace-offset></div>

</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/createRealTournamentCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/createRealTournamentService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/clubTypesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/tournamentCategoriesService.js?r=<?= time() ?>"></script>