<section class="pageWrapper" ng-controller="occasionsCtrl">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-12"><strong>Occasions</strong></div>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="GetOccasionsList()">




        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Occasion Name</th>
                        <th>Center</th>
                        <th>From Date</th>
                        <th>To Date</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in occasionsList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Occasion Name">{{item.occasion_name}}</td>
                        <td data-label="Center">{{item.center_name}}</td>
                        <td data-label="From Date">{{item.from_date}}</td>
                        <td data-label="To Date">{{item.to_date}}</td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditOccasion(item)"><a href="#">Edit</a></li>
                                    <li ng-click="DeleteOccasion(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageSidebar">
        <form ng-submit="AddOrUpdateOccasion()" id="occasionForm">
            <div class="SidebarHead offset">
                {{occasionObj.id?'Update':'Add'}} Occasion
            </div>
            <div class="SidebarBody">
                <span class="text-danger" ng-bind-html="error_message"></span>
                <div class="form-group">
                    <label>Occasion Name</label>
                    <input type="text" class="form-control" name="occasion_name" ng-model="occasionObj.occasion_name">
                </div>
                <div class="form-group">
                    <label>Choose Center</label>
                    <div class="custom-input">
                        <select class="form-control" name="centers_id" ng-model="occasionObj.centers_id">
                            <option value="">Choose Center</option>
                            <option ng-value="item.id" value="item.id" ng-repeat="item in centersList">{{item.center_name}}</option>
                        </select>
                        <span class="ci-icon">
                            <i ng-show="centersSpinner" class="fal fa-circle-notch fa-spin"></i>
                            <i ng-show="!centersSpinner" class="fal fa-chevron-down"></i>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label>From Date (DD/MM/YYYY)</label>
                    <input type="text" class="form-control datepickerFrom" name="from_date" ng-model="occasionObj.from_date">
                </div>
                <div class="form-group">
                    <label>To Date (DD/MM/YYYY)</label>
                    <input type="text" class="form-control datepickerTo" name="to_date" ng-model="occasionObj.to_date">
                </div>
            </div>
            <div class="SidebarFooter offset">
                <div class="text-right">
                    <button class="btn btn-default" type="reset" ng-click="ResetForm()"><b>Cancel <i class="fal fa-times"></i></b></button>
                    <button type="submit" class="btn btn-primary"><b>{{occasionObj.id?'Update':'Add'}} <i class="fal fa-plus"></i></b></button>
                </div>
            </div>
        </form>
    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk 2018</div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/occasionsCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/occasionsService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/centersService.js?r=<?= time() ?>"></script>