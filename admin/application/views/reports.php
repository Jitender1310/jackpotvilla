<section class="pageWrapper">
    <div class="pageHeader" workspace-offset  valign-parent>
        <div class="row">
            <div class="col-md-6"><strong>Reports</strong></div>
            <div class="col-md-6 text-right">
                <div valign-holder>



                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>

        <div class="row rm-5">
            <div class="col-md-12 cp-5"> <div class="whitebox mb-10-xs">

                    <div table responsive class="reports-info">
                        <div tr>
                            <div td><strong class="text-uppercase">Today Accomidation Sales</strong> <h4 class="mb-0-xs font-bold text-success"><i class="fal fa-rupee-sign"></i> 5000.00</h4></div>
                            <div td class="hidden-xs"><i class="fal fa-plus"></i></div>
                            <div td><strong class="text-uppercase">Today F & B Sales</strong> <h4 class="mb-0-xs font-bold text-success"><i class="fal fa-rupee-sign"></i> 5000.00</h4></div>
                            <div td class="hidden-xs"><i class="fal fa-arrow-right"></i></div>
                            <div td><strong class="text-uppercase">Today Total  Sales</strong> <h4 class="mb-0-xs font-bold text-success"><i class="fal fa-rupee-sign"></i> 5000.00</h4></div>
                            <div td><strong class="text-uppercase">Today Expenses</strong> <h4 class="mb-0-xs font-bold text-success"><i class="fal fa-rupee-sign"></i> 5000.00</h4></div>
                        </div>
                    </div>




                </div></div>
            <div class="col-md-6 cp-5"> <div class="whitebox mb-10-xs">
                    <div class="whiteboxHead">
                        <strong>Today Accomidation Sales</strong>
                    </div>
                    <div valign-parent>
                        <div class="row">
                            <div class="col-md-8">
                                <div id="today-accomidation-sales"></div>
                            </div>
                            <div class="col-md-4">
                                <ul class="ul-block" valign-holder>
                                    <li><i class="fas fa-circle text-warning"></i> Cash <i class="fal fa-rupee-sign ml-10-xs"></i><b> 100</b></li>
                                    <li><i class="fas fa-circle text-info"></i> Card <i class="fal fa-rupee-sign ml-10-xs"></i><b> 25</b></li>
                                    <li><i class="fas fa-circle text-success"></i> Paytm <i class="fal fa-rupee-sign ml-10-xs"></i><b> 25</b></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div></div>
            <div class="col-md-6 cp-5"> <div class="whitebox mb-10-xs">
                    <div class="whiteboxHead">
                        <strong>Today F&B Sales</strong>
                    </div>
                    <div valign-parent>
                        <div class="row">
                            <div class="col-md-8">
                                <div id="today-fnb-sales"></div>
                            </div>
                            <div class="col-md-4">
                                <ul class="ul-block" valign-holder>
                                    <li><i class="fas fa-circle text-warning"></i> Cash <i class="fal fa-rupee-sign ml-10-xs"></i><b> 25</b></li>
                                    <li><i class="fas fa-circle text-info"></i> Card <i class="fal fa-rupee-sign ml-10-xs"></i><b> 100</b></li>
                                    <li><i class="fas fa-circle text-success"></i> Paytm <i class="fal fa-rupee-sign ml-10-xs"></i><b> 25</b></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div></div>
        </div>

    </div>

    <?php $this->load->view('includes/reports_menu'); ?>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk - 2017</div>
</section>