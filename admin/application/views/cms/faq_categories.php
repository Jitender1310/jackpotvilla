<section class="pageWrapper" ng-controller="faqCategoriesCtrl">
    <div class="pageHeader" workspace-offset valign-parent >
        <div class="row">
            <div class="col-md-6"><strong>Faq Categoriess</strong></div>
            <div class="col-md-6">
                <div valign-holder class="text-right">
                    <button type="button" class="btn btn-primary" ng-click="ShowFaqCategoryAddForm()"><i class="fal fa-plus"></i> Add Categories</button>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Category</th>
                        <th>Image</th>
                        <th>Created Time</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in faqCategoriesList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Category">{{item.category}}</td>
                        <td data-label="Image">
                            <a target="_blank" href="{{item.icon}}">
                                <img src="{{item.icon}}" style="width:100px;height: auto"/>
                            </a>
                        </td>
                        <td data-label="Created Time">{{item.created_date_time}}</td>
                        <td data-label="Actions" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditFaqCategory(item)"><a href="#">Edit</a></li>
                                    <li ng-click="DeleteFaqCategory(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageFooter" workspace-offset></div>

    <div class="modal fade" id="faqcategory-modal-popup">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{faqCategoriesObj.id?"Update":"Add"}} Faq Categories</h4>
                </div>
                <div class="modal-body">
                    <form ng-submit="AddOrUpdateFaqCategory()" id="categoriesForm" ng-keyup="t_error = {}">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="text-danger" ng-bind-html="error_message"></span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Category</label>
                                    <input type="text" class="form-control" name="category" ng-model="faqCategoriesObj.category">
                                    <label class="error" ng-if="t_error.category">{{t_error.category}}</label>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Image</label>
                                    <input type="file" class="form-control" name="icon" id="icon">
                                    <label class="error" ng-if="t_error.icon">{{t_error.icon}}</label>
                                    <br />
                                    <img ng-if="item.icon != ''" width="250" src="{{item.icon}}" alt="jdh">
                                </div>
                            </div>
                        </div>

                        <br/>
                        <br/>

                        <button class="btn btn-default " ng-click="ResetForm()" data-dismiss="modal"><b>Cancel <i class="fal fa-times"></i></b></button>
                        <button class="btn btn-primary pull-right" type="submit"><b>{{faqCategoriesObj.id?"Update":"Add"}} <i class="fal fa-arrow-right"></i></b></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/faqCategoriesCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/faqCategoriesService.js?r=<?= time() ?>"></script>