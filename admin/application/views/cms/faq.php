<section class="pageWrapper" ng-controller="faqsCtrl">
    <div class="pageHeader" workspace-offset valign-parent >
        <div class="row">
            <div class="col-md-6"><strong>Faqs</strong></div>
            <div class="col-md-6">
                <div valign-holder class="text-right">
                    <button type="button" class="btn btn-primary" ng-click="ShowFaqsAddForm()"><i class="fal fa-plus"></i> Add Faqs</button>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Category</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Created Time</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in faqsList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Category">{{item.category}}</td>
                        <td data-label="Title">{{item.title}}</td>
                        <td data-label="Description">{{item.description}}</td>
                        <td data-label="Created Time">{{item.created_date_time}}</td>
                        <td data-label="Actions" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditFaq(item)"><a href="#">Edit</a></li>
                                    <li ng-click="DeleteFaq(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageFooter" workspace-offset></div>

    <div class="modal fade" id="faqs-modal-popup">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{faqsObj.id?"Update":"Add"}} Faq Categories</h4>
                </div>
                <div class="modal-body">
                    <form ng-submit="AddOrUpdateFaq()" id="faqsForm" ng-keyup="t_error = {}">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="text-danger" ng-bind-html="error_message"></span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Faq Category</label>
                                    <select id="faq_categories_id" name="faq_categories_id" ng-model="faqsObj.faq_categories_id" class="form-control">
                                        <option value="" >-Select Category-</option>
                                        <option ng-repeat="item in faqCategoriesList" value="{{item.id}}">{{item.category}}</option>
                                    </select>

                                    <label class="error" ng-if="t_error.category">{{t_error.category}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" class="form-control" name="title" ng-model="faqsObj.title">
                                    <label class="error" ng-if="t_error.title">{{t_error.title}}</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea cols="10" rows="10" class="form-control" name="description" ng-model="faqsObj.description"></textarea>
                                    <label class="error" ng-if="t_error.description">{{t_error.description}}</label>
                                </div>
                            </div>
                        </div>


                        <br/>
                        <br/>

                        <button class="btn btn-default " ng-click="ResetForm()" data-dismiss="modal"><b>Cancel <i class="fal fa-times"></i></b></button>
                        <button class="btn btn-primary pull-right" type="submit"><b>{{faqsObj.id?"Update":"Add"}} <i class="fal fa-arrow-right"></i></b></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/faqsCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/faqsService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/faqCategoriesService.js?r=<?= time() ?>"></script>