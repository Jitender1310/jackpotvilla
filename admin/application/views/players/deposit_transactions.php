<section class="pageWrapper pageSidebar" ng-controller="depositTransactionsCtrl" ng-init="filter.search_key = '<?= $this->input->get_post('username') ? $this->input->get_post('username') : '0' ?>'">
    <div class="pageHeader" workspace-offset valign-parent>
        <?php
        if ($this->input->get_post('players_id') != 0) {
            ?>
            <div class="row">
                <div class="col-md-3">
                    <strong class="text-left"> Full name : <?= $players_details->firstname ?> <?= $players_details->lastname ?></strong>
                </div>
                <div class="col-md-3">
                    <strong class="text-left"> Mobile Number : <?= $players_details->mobile ?></strong>
                </div>
                <div class="col-md-3">
                    <strong class="text-left"> Username: <?= $agent_details->username ?></strong>
                </div>
                <div class="col-md-3">
                    <strong class="text-right">Current Wallet Balance <i class="fa fa-rupee-sign"></i> 100 /-</strong>
                </div>
            </div>
            <?php
        } else {
            ?>
            <div class="row">
                <div class="col-md-10">
                    <strong ng-if="filter.booking_status === 'completed_but_proofs_updation_pending'">Pending Reservation awaiting for Guest Proofs updating</strong>
                    <strong ng-if="filter.booking_status != 'completed_but_proofs_updation_pending'">{{selected_menut_title}}</strong>
                </div>
                <div class="col-md-2">
                    <div class="custom-input" title="Sory By" valign-holder>
                        <select class="form-control input-sm" ng-model="filter.sort_by" ng-change="GetTransactionsList()">
                            <option value="" selected>Sort by</option>
                            <option value="Latest">Latest First</option>
                            <option value="Oldest">Oldest First</option>
                        </select>
                        <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
    <div class="pageBody" workspace ng-init="GetTransactionsList()">
        <div class="whitebox">
            <div class="row rm-5">
                <form class="" ng-submit="GetTransactionsList(filter.page = 1)">
                    <div class="col-md-4 cp-5">
                        <div class="form-group mb-0-xs">
                            <input type="text" class="form-control"
                                   placeholder="Enter Player username, payment transaction id"
                                   ng-model="filter.search_key">
                        </div>

                    </div>
                    <div class="col-md-2 cp-5">
                        <div class="custom-group">
                            <div class="custom-input">
                                <input type="text" class="form-control datepickerNormal" placeholder="From Date" name="from_date" onkeydown="event.preventDefault()" ng-model="filter.from_date">
                                <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 cp-5">
                        <div class="custom-group">
                            <div class="custom-input">
                                <input type="text" class="form-control datepickerNormal" placeholder="To Date" name="to_date" onkeydown="event.preventDefault()" ng-model="filter.to_date">
                                <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 cp-5">
                        <button type="submit" class="btn btn-primary "><i class="fas fa-search"></i></button>
                        <button type="reset" ng-click="ResetFilter()" class="btn btn-danger " title="Clear Search"><i class="fas fa-times-circle"></i></button>                        
                    </div>
                </form>
            </div>

        </div>

        <div class="responsive-table" ng-hide="transactionList.length > 0">
            <div class="alert alert-danger">
                No Data found
            </div>
        </div> 

        <div class="responsive-table" ng-show="transactionList.length > 0">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="30" class="no-sort">#</th>
                        <th class="no-sort">Txn ID</th>
                        <th class="no-sort">Contact Info</th>
                        <th class="no-sort">Amount</th>
                        <th class="no-sort">Date Time</th>
                        <th class="no-sort">Payment Status</th>
                        <th class="no-sort">Payment ID</th>
                        <th class="no-sort">Comment</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in transactionList track by $index">
                        <td data-label="S.No">
                            {{($index + transactionsPagination.initial_id)}}
                        </td>
                        <td data-label="Ref ID">
                            <strong>{{item.transaction_id}}</strong>
                        </td>
                        <td data-label="Player name" class="contact_info_td">
                            <div><strong>{{item.player_name}}</strong> 
                                <a class="far fa-plus-circle text-primary" data-toggle="collapse" aria-expanded="false" href="#{{$index}}"></a>
                            </div>
                            <div class="collapse" id="{{$index}}">
                                <div><i class="fal fa-phone"></i> {{item.mobile}}</div>
                                <div><i class="fal fa-envelope"></i> {{item.email}}</div>
                            </div>
                        </td>
                        <td data-label="Amount">{{item.credits_count}}</td>
                        <td data-label="Date Time">{{item.created_at}}</td>
                        <td data-label="Payment Status" class="{{item.transaction_status_bootstrap_class}}">
                            {{item.transaction_status}}
                            <span ng-if="item.transaction_status == 'Refund'">Rs.{{item.refunded_amount}}</span>
                        </td>
                        <td data-label="Payment id">{{item.payment_gateway_id}}</td>
                        <td data-label="Comment" style="width:25%">{{item.comment}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <span class="pull-right" ng-bind-html="transactionsPagination.pagination"></span>
    </div>
    <div class="pageFooter" workspace-offset>&copy;</div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/depositTransactionsCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/depositTransactionsService.js?r=<?= time() ?>"></script>