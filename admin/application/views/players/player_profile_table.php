<div class="table-responsive">
    <table class="table table-bordered">
        <tr>
            <th>Full Name</th>
            <td>
                {{playerObj.fullname}}
            </td>
        </tr>
        <tr>
            <th>Username</th>
            <td>
                {{playerObj.username}}
            </td>
        </tr>
        <tr>
            <th>Email</th>
            <td>
                {{playerObj.email}}
            </td>
        </tr>
        <tr>
            <th>Mobile</th>
            <td>
                {{playerObj.mobile}}
            </td>
        </tr>
        <tr>
            <th>Gender</th>
            <td>
                {{playerObj.gender}}
            </td>
        </tr>
        <tr>
            <th>Date of Birth (DD/MM/YYYY)</th>
            <td>
                {{playerObj.date_of_birth}}
            </td>
        </tr>

        <tr>
            <th>Address Line 1</th>
            <td>
                {{playerObj.address_line_1}}
            </td>
        </tr>
        <tr>
            <th>Address Line 2</th>
            <td>
                {{playerObj.address_line_2}}
            </td>
        </tr>

        <tr>
            <th>City</th>
            <td>
                {{playerObj.city}}
            </td>
        </tr>

        <tr>
            <th>State</th>
            <td>
                {{playerObj.state_name}}
            </td>
        </tr>

        <tr>
            <th>Date of Joining (DD/MM/YYYY)</th>
            <td>
                {{playerObj.registered_at}}
            </td>
        </tr>

        <tr>
            <th><abbr title="Permanent Account Number">PAN</abbr> Card File</th>
            <td>
                <a target="_blank" ng-href="{{playerObj.pan_card}}"  ng-if="playerObj.pan_card != ''">View Attachment</a>
                <span class="{{playerObj.kyc_status_bootstrap_css}}">{{playerObj.kyc_status}}</span>
            </td>
        </tr>
        <tr>
            <th><abbr title="Permanent Account Number">PAN</abbr> Card Number</th>
            <td>
                {{playerObj.pan_card_number}}
            </td>
        </tr>

        <tr>
            <th>Address Proof Type</th>
            <td>
                {{playerObj.address_proof_type}}
            </td>
        </tr>

        <tr>
            <th>Address Proof </th>
            <td>
                <a target="_blank" ng-href="{{playerObj.address_proof}}"  ng-if="playerObj.address_proof != ''">View Attachment</a>
                <span class="{{playerObj.kyc_status_bootstrap_css}}">{{playerObj.kyc_status}}</span>
            </td>
        </tr>

        <tr>
            <th>KYC Status</th>
            <td>
                <span class="{{playerObj.kyc_status_bootstrap_css}}">{{playerObj.kyc_status}}</span>
            </td>
        </tr>
        <tr>
            <th colspan="2" class="text-center"><h4>Wallet Info</h4></th>
        </tr>
        <tr>
            <th>Fun Chips</th>
            <td>
                {{playerObj.wallet_info.fun_chips}}
            </td>
        </tr>
        <tr>
            <th>In Play Fun chips</th>
            <td>
                {{playerObj.wallet_info.in_play_fun_chips}}
            </td>
        </tr>
        <tr>
            <th>Real Chips deposit</th>
            <td>
                {{playerObj.wallet_info.real_chips_deposit}}
            </td>
        </tr>
        <tr>
            <th>In Play Real Chips</th>
            <td>
                {{playerObj.wallet_info.in_play_real_chips}}
            </td>
        </tr>
        <tr>
            <th>Real Chips Withdrawal</th>
            <td>
                {{playerObj.wallet_info.real_chips_withdrawal}}
            </td>
        </tr>
        
        <tr>
            <th>RPS Points</th>
            <td>
                {{playerObj.wallet_info.total_rps_points}}
            </td>
        </tr>
        
        <tr>
            <th>Total Bonus</th>
            <td>
                {{playerObj.wallet_info.total_bonus}}
            </td>
        </tr>
        
        <tr>
            <th>Active Bonus</th>
            <td>
                {{playerObj.wallet_info.active_bonus}}
            </td>
        </tr>
        <tr>
            <th>Release Bonus</th>
            <td>
                {{playerObj.wallet_info.release_bonus}}
            </td>
        </tr>
        <tr>
            <th>Pending Bonus</th>
            <td>
                {{playerObj.wallet_info.pending_bonus}}
            </td>
        </tr>
    </table>
</div>