<section class="pageWrapper pageSidebar" ng-controller="withdrawRequestsCtrl" ng-init="filter.search_key = '<?= $this->input->get_post('username') ? $this->input->get_post('username') : '0' ?>'">
    <div class="pageHeader" workspace-offset valign-parent>
        <?php
            if ($this->input->get_post('players_id') != 0) {
                ?>
                <div class="row">
                    <div class="col-md-3">
                        <strong class="text-left"> Full name : <?= $players_details->firstname ?> <?= $players_details->lastname ?></strong>
                    </div>
                    <div class="col-md-3">
                        <strong class="text-left"> Mobile Number : <?= $players_details->mobile ?></strong>
                    </div>
                    <div class="col-md-3">
                        <strong class="text-left"> Username: <?= $agent_details->username ?></strong>
                    </div>
                    <div class="col-md-3">
                        <strong class="text-right">Current Wallet Balance <i class="fa fa-rupee-sign"></i> 100 /-</strong>
                    </div>
                </div>
                <?php
            } else {
                ?>
                <div class="row">
                    <div class="col-md-10"><strong>Withdraw Requests</strong></div>
                    <div class="col-md-2">
                        <div class="custom-input" title="Sory By" valign-holder>
                            <select class="form-control input-sm" ng-model="filter.sort_by" ng-change="GetWithdrawRequestsList()">
                                <option value="" selected>Sort by</option>
                                <option value="Latest">Latest First</option>
                                <option value="Oldest">Oldest First</option>
                            </select>
                            <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                        </div>
                    </div>
                </div>
                <?php
            }
        ?>
    </div>
    <div class="pageBody" workspace>
        <div class="whitebox">
            <div class="row rm-5">
                <form class="" ng-submit="GetWithdrawRequestsList(filter.page = 1)">
                    <div class="col-md-4 cp-5">
                        <div class="form-group mb-0-xs">
                            <input type="text" class="form-control"
                                   placeholder="Enter Player username, payment transaction id"
                                   ng-model="filter.search_key">
                        </div>

                    </div>
                    <div class="col-md-2 cp-5">
                        <div class="custom-group">
                            <div class="custom-input">
                                <input type="text" class="form-control datepickerNormal" placeholder="From Date" name="from_date" onkeydown="event.preventDefault()" ng-model="filter.from_date">
                                <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 cp-5">
                        <div class="custom-group">
                            <div class="custom-input">
                                <input type="text" class="form-control datepickerNormal" placeholder="To Date" name="to_date" onkeydown="event.preventDefault()" ng-model="filter.to_date">
                                <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 cp-5">
                        <button type="submit" class="btn btn-primary "><i class="fas fa-search"></i></button>
                        <button type="reset" ng-click="ResetFilter()" class="btn btn-danger " title="Clear Search"><i class="fas fa-times-circle"></i></button>                        
                    </div>
                </form>
            </div>

        </div>

        <div class="responsive-table" ng-hide="withdrawRequestsList.length > 0">
            <div class="alert alert-danger">
                No Data found
            </div>
        </div> 

        <div class="responsive-table" ng-show="withdrawRequestsList.length > 0">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="30" class="no-sort">#</th>
                        <th class="no-sort">Contact Info</th>
                        <th class="no-sort">Requested bank details</th>
                        <th class="no-sort">Amount</th>
                        <th class="no-sort">Date Time</th>
                        <th class="no-sort">Request Status</th>
                        <th class="no-sort">Requested Date</th>
                        <th class="no-sort">Remark</th>
                        <th class="no-sort">Request Processed User</th>
                        <th class="no-sort">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in withdrawRequestsList track by $index">
                        <td data-label="S.No">
                            {{($index + withdrawRequestsPagination.initial_id)}}
                        </td>

                        <td data-label="Player name" class="contact_info_td">
                            <div><strong>{{item.player_name}}</strong> 
                                <a class="far fa-plus-circle text-primary" data-toggle="collapse" aria-expanded="false" href="#{{$index}}"></a>
                            </div>
                            <div class="collapse" id="{{$index}}">
                                <div><i class="fal fa-phone"></i> {{item.mobile}}</div>
                                <div><i class="fal fa-envelope"></i> {{item.email}}</div>
                            </div>
                        </td>
                        <td data-label="Bank Details">
                            <div><strong>{{item.bank_name}}</strong> 
                                <a data-toggle="collapse1" aria-expanded="false" href="#s{{$index}}"></a>
                            </div>
                            <div class="collapse1" id="s{{$index}}">
                                <div>Ac.no - {{item.branch_name}}</div>
                                <div>IFSC code - {{item.ifsc_code}}</div>
                                <div>Branch - {{item.branch_name}}</div>
                            </div>
                        </td>
                        <td data-label="Amount">{{item.request_amount}}</td>
                        <td data-label="Date Time">{{item.created_at}}</td>
                        <td data-label="Payment Status" class="{{item.request_status_bootstrap_class}}">
                            {{item.request_status}}

                        </td>
                        <td data-label="Requested Date">{{item.request_date_time}}</td>
                        <td data-label="Remark">{{item.remark}}</td>
                        <td data-label="Request Processed User">{{item.request_processed_user}}</td>
                        <td data-label="Actions" class="text-right" >
                            <span class="dropdown" ng-if="item.request_status != 'Completed'">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditRequest(item)"><a href="#">Edit</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <span class="pull-right" ng-bind-html="withdrawRequestsPagination.pagination"></span>
    </div>
    <div class="pageFooter" workspace-offset>&copy;</div>
    <div class="modal fade" id="withdraw-request-modal-popup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{withdrawRequestObj.id?"Update":"Add"}} Request</h4>
                </div>
                <div class="modal-body">
                    <form ng-submit="AddOrUpdateRequest()" id="withdrawRequestForm" ng-keyup="p_error = {}">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="text-danger" ng-bind-html="error_message"></span>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Request Amount </label>
                                    <input type="text" class="form-control" name="request_amount " ng-model="withdrawRequestObj.request_amount">
                                    <label class="error" ng-if="p_error.request_amount">{{p_error.request_amount}}</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Request Status </label>
                                    <select id="request_status" class="form-control" name="request_status" ng-model="withdrawRequestObj.request_status">
                                        <option value="Pending">Pending</option>
                                        <option value="Completed">Completed</option>
                                        <option value="Rejected">Rejected</option>
                                    </select>
                                    <label class="error" ng-if="p_error.request_status">{{p_error.request_status}}</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Remarks </label>
                                    <textarea class="form-control" name="remark " ng-model="withdrawRequestObj.remark"></textarea>
                                    <label class="error" ng-if="p_error.remark">{{p_error.remark}}</label>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-default " ng-click="ResetForm()" data-dismiss="modal"><b>Cancel <i class="fal fa-times"></i></b></button>
                        <button class="btn btn-primary pull-right" type="submit"><b>{{withdrawRequestObj.id?"Update":"Add"}} <i class="fal fa-arrow-right"></i></b></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/withdrawRequestsCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/withdrawRequestsService.js?r=<?= time() ?>"></script>