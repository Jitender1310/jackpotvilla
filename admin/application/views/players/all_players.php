<section class="pageWrapper" ng-controller="playersCtrl">
    <div class="pageHeader" workspace-offset valign-parent >
        <div class="row">
            <div class="col-md-10"><strong>Players > Real Players</strong></div>
            <div class="col-md-2">
                <div valign-holder class="text-right">
                    <button type="button" class="btn btn-primary" ng-click="ShowPlayerAddForm()"><i class="fal fa-plus"></i> Add Player</button>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>
        
        <div class="whitebox">
            <div class="row rm-5">
                <form class="" ng-submit="GetPlayersList(filter.page = 1)">
                    <div class="col-md-4 cp-5">
                        <div class="form-group mb-0-xs">
                            <input type="text" class="form-control"
                                   placeholder="Enter Player username, email, mobile"
                                   ng-model="filter.search_key">
                        </div>
                    </div>
                    <div class="col-md-2 cp-5">
                        <div class="custom-group">
                            <div class="custom-input">
                                <input type="text" class="form-control datepickerNormal" placeholder="From Date" name="from_date" onkeydown="event.preventDefault()" ng-model="filter.from_date">
                                <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 cp-5">
                        <div class="custom-group">
                            <div class="custom-input">
                                <input type="text" class="form-control datepickerNormal" placeholder="To Date" name="to_date" onkeydown="event.preventDefault()" ng-model="filter.to_date">
                                <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 cp-5">
                        <div class="form-group mb-0-xs"> 
                            <div class="custom-input" title="Player KYC Status">
                                <select class="form-control" ng-model="filter.kyc_status">
                                    <option value="" selected>KYC Status</option>
                                    <option value="Not Submitted" selected>Not Submitted</option>
                                    <option value="Pending Approval" selected>Pending Approval</option>
                                    <option value="Approved" selected>Approved</option>
                                    <option value="Rejected" selected>Rejected</option>
                                </select>
                                <span class="ci-icon">
                                    <i class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 cp-5">
                        <button type="submit" class="btn btn-primary "><i class="fas fa-search"></i></button>
                        <button type="reset" ng-click="ResetFilter()" class="btn btn-danger " title="Clear Search"><i class="fas fa-times-circle"></i></button>
                    </div>
                </form>
            </div>

        </div>
        
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Full name</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Registered Time</th>
                        <th>Address Proof</th>
                        <th>PAN Card</th>
                        <th>KYC Status</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in playersList track by $index">
                        <td data-label="S.No">
                            {{($index + playersPagination.initial_id)}}
                        </td>
                        <td data-label="Name">{{item.fullname}}</td>
                        <td data-label="Username">{{item.username}}</td>
                        <td data-label="Email">{{item.email}}</td>
                        <td data-label="Mobile">{{item.mobile}}</td>
                        <td data-label="Registered Time">{{item.registered_at}}</td>
                        <td data-label="Address Proof Status" class="{{item.address_proof_status_bootstrap_css}}">{{item.address_proof_status}}</td>
                        <td data-label="PAN Card Status" class="{{item.pan_card_status_bootstrap_css}}">{{item.pan_card_status}}</td>
                        <td data-label="KYC Status" class="{{item.kyc_status_bootstrap_css}}">{{item.kyc_status}}</td>
                        <td data-label="Actions" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditPlayer(item)"><a href="#">Edit</a></li>
                                    <li ng-click="EditKYCInformation(item)"><a href="#">KYC Information</a></li>
                                    <li><a href="<?=base_url()?>players/deposit_transactions?username={{item.username}}">Deposit Transactions</a></li>
                                    <li ng-click="ViewPlayer(item)"><a href="#">View</a></li>
                                    <li ng-click="DeletePlayer(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <span class="pull-right" ng-bind-html="playersPagination.pagination"></span>
        <?php $this->load->view("players/kyc_popup") ?>
    </div>
    <div class="pageFooter" workspace-offset></div>

    <div class="modal fade" id="player-modal-popup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{playerObj.id?"Update":"Add"}} Player</h4>
                </div>
                <div class="modal-body">
                    <form ng-submit="AddOrUpdatePlayer()" id="playerForm" ng-keyup="p_error = {}">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="text-danger" ng-bind-html="error_message"></span>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Firstname</label>
                                    <input type="text" class="form-control" name="firstname" ng-model="playerObj.firstname">
                                    <label class="error" ng-if="p_error.firstname">{{p_error.firstname}}</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Lastname</label>
                                    <input type="text" class="form-control" name="lastname" ng-model="playerObj.lastname">
                                    <label class="error" ng-if="p_error.lastname">{{p_error.lastname}}</label>
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" class="form-control" name="username" ng-model="playerObj.username">
                                    <label class="error" ng-if="p_error.username">{{p_error.username}}</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="{{playerObj.id?'':'password'}}" id="password" ng-model="playerObj.password">
                                    <span ng-if="playerObj.id">Leave Blank if you don't want to update</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input type="password" class="form-control" name="{{playerObj.id?'':'confirm_password'}}" ng-model="playerObj.confirm_password">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Mobile</label>
                                    <input type="tel" class="form-control number" maxlength="10" minlength="10" name="mobile" ng-model="playerObj.mobile">
                                    <label class="error" ng-if="p_error.mobile">{{p_error.mobile}}</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email" ng-model="playerObj.email">
                                    <label class="error" ng-if="p_error.email">{{p_error.email}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Gender</label>
                                    <div class="custom-input">
                                        <select class="form-control" name="gender" ng-model="playerObj.gender">
                                            <option value="">Choose Gender</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                            <option value="Other">Other</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i class="fal fa-chevron-down"></i>
                                        </span>
                                    </div>
                                    <label class="error" ng-if="p_error.gender">{{p_error.gender}}</label>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Date of Birth (DD-MM-YYYY)</label>
                                    <input type="text" class="form-control datepickerNormal" name="date_of_birth" ng-model="playerObj.date_of_birth">
                                    <label class="error" ng-if="p_error.date_of_birth">{{p_error.date_of_birth}}</label>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Address Line 1</label>
                                    <input type="text" class="form-control" name="address_line_1" ng-model="playerObj.address_line_1">
                                    <label class="error" ng-if="p_error.address_line_1">{{p_error.address_line_1}}</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Address Line 2</label>
                                    <input type="text" class="form-control" name="address_line_2" ng-model="playerObj.address_line_2">
                                    <label class="error" ng-if="p_error.address_line_2">{{p_error.address_line_2}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>City</label>
                                    <input type="text" class="form-control" name="city" ng-model="playerObj.city">
                                    <label class="error" ng-if="p_error.city">{{p_error.city}}</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>State</label>
                                    <select class="form-control" name="states_id" ng-model="playerObj.states_id">
                                        <option value="">--Choose State--</option>
                                        <option ng-repeat="state in statesList" ng-value="state.id">{{state.state_name}}</option>
                                    </select>
                                    <label class="error" ng-if="p_error.state_name">{{p_error.state_name}}</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Pin Code</label>
                                    <input type="text" class="form-control" name="pin_code" ng-model="playerObj.pin_code">
                                    <label class="error" ng-if="p_error.pin_code">{{p_error.pin_code}}</label>
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-default " ng-click="ResetForm()" data-dismiss="modal"><b>Cancel <i class="fal fa-times"></i></b></button>
                        <button class="btn btn-primary pull-right" type="submit"><b>{{playerObj.id?"Update":"Add"}} <i class="fal fa-arrow-right"></i></b></button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="view-player-modal-popup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Player Details</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="width-700 mx-auto-xs">
                            <?php $this->load->view("players/player_profile_table") ?>
                        </div>
                    </div>
                    <button class="btn btn-danger " data-dismiss="modal"><b>Close <i class="fal fa-times"></i></b></button>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/playersCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/playersService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/statesService.js?r=<?= time() ?>"></script>