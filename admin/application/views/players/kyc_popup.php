<!-- Edit kyc Popup Starts -->
<div class="modal" id="editKycModal" data-backdrop='static' data-keyboard='false'>
    <div class="modal-dialog modal-lg" style="width: 90%">
        <form ng-submit="updateKycDetails()" ng-click="kyc_error = {}">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">{{editProfileObj.firstname}} {{editProfileObj.lastname}} KYC Status</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-4">
                            <?php $this->load->view("players/player_profile_table") ?>
                        </div>
                        <div class="col-md-8">
                            <div class="table-responsive">
                                <table class="table table-custom table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Information</th>
                                            <th>Details</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                Address Proof
                                            </td>
                                            <td>
                                                <select class="form-control"   id="address_proof_type" required name="address_proof_type" ng-model="editProfileObj.address_proof_type">
                                                    <option value="">--Select Proof--</option>
                                                    <option value="Aadhaar Card">Aadhaar Card</option>
                                                    <option value="Voter ID">Voter ID</option>
                                                    <option value="Driving License">Driving License</option>
                                                    <option value="Passport">Passport</option>
                                                </select>
                                                <label class="error" ng-if="kyc_error.address_proof_type">{{kyc_error.address_proof_type}}</label>
                                            </td>
                                            <td>
                                                <select class="form-control" required name="address_proof_status" ng-model="editProfileObj.address_proof_status">
                                                    <option value="">--Select Proof--</option>
                                                    <option value="Pending Approval">Pending Approval</option>
                                                    <option value="Rejected">Rejected</option>
                                                    <option value="Approved">Approved</option>
                                                    <option value="Not Submitted">Not Submitted</option>
                                                </select>
                                                <input style="margin-top: 10px" placeholder="Reason" ng-if="editProfileObj.address_proof_status == 'Rejected'" type="text" ng-model="editProfileObj.address_proof_rejected_reason" class="form-control" name="address_proof_rejected_reason">
                                                <label class="error" ng-if="kyc_error.address_proof_status">{{kyc_error.address_proof_status}}</label>
                                            </td>
                                            <td>
                                                <input type="file" name="address_proof" id="address_proof" required ng-if="editProfileObj.address_proof === ''">
                                                <label class="error" ng-if="kyc_error.address_proof">{{kyc_error.address_proof}}</label>
                                                <a target="_blank" href="{{editProfileObj.address_proof}}" ng-if="editProfileObj.address_proof !== ''">View file</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                PAN Card
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" required name="pan_card_number" placeholder="Enter pan card number" ng-model="editProfileObj.pan_card_number">
                                                <label class="error" ng-if="kyc_error.pan_card_number">{{kyc_error.pan_card_number}}</label>
                                            </td>
                                            <td>
                                                <select class="form-control" required name="pan_card_status" ng-model="editProfileObj.pan_card_status">
                                                    <option value="">--Select Proof--</option>
                                                    <option value="Pending Approval">Pending Approval</option>
                                                    <option value="Rejected">Rejected</option>
                                                    <option value="Approved">Approved</option>
                                                    <option value="Not Submitted">Not Submitted</option>
                                                </select>
                                                <input style="margin-top: 10px" placeholder="Reason" ng-if="editProfileObj.pan_card_status == 'Rejected'" type="text" ng-model="editProfileObj.pan_card_rejected_reason" class="form-control" name="pan_card_rejected_reason">
                                                <label class="error" ng-if="kyc_error.pan_card_status">{{kyc_error.pan_card_status}}</label>
                                            </td>
                                            <td>
                                                <input type="file" name="pan_card" id="pan_card" required ng-if="editProfileObj.pan_card === ''">
                                                <label class="error" ng-if="kyc_error.pan_card">{{kyc_error.pan_card}}</label>
                                                <a target="_blank" href="{{editProfileObj.pan_card}}" ng-if="editProfileObj.pan_card !== ''">View file</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="well"><span class="text-danger">Note:</span> Max allowed size is : 2 MB, Allowed file formats JPEG, JPG, and PDF</p>    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>KYC Log</h3>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <td>S.no</td>
                                                <td>Comment</td>
                                                <td>Previous Status</td>
                                                <td>Current Status</td>
                                                <td>Modified User</td>
                                                <td>Date</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="item in editProfileObj.kyc_log track by $index">
                                                <td>{{$index + 1}}</td>
                                                <td>{{item.comment}}</td>
                                                <td>{{item.previous_status}}</td>
                                                <td>{{item.current_status}}</td>
                                                <td>{{item.name}}</td>
                                                <td>{{item.created_at}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary text-white" type="submit"><b>Update</b></button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Edit Kyc POPUP Ends -->