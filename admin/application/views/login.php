<!DOCTYPE html>
<html lang="en" ng-app="frontDesk" ng-cloak>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= SITE_TITLE ?></title>
        <!-- Bootstrap CSS -->
        <link rel="shortcut icon" href="<?= STATIC_ADMIN_IMAGES_PATH ?>favicon.ico" />
        <link rel="stylesheet" type="text/css" href="<?= STATIC_ADMIN_CSS_PATH ?>bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?= STATIC_ADMIN_CSS_PATH ?>jquery.rateyo.min.css">
        <link rel="stylesheet" type="text/css" href="<?= STATIC_ADMIN_CSS_PATH ?>jquery-ui-timepicker-addon.min.css">
        <link rel="stylesheet" type="text/css" href="<?= STATIC_ADMIN_ASSETS_PATH ?>fonts/fontawesome/css/fontawesome-all.min.css">
        <link rel="stylesheet" type="text/css" href="<?= STATIC_ADMIN_CSS_PATH ?>morris.css">
        <link rel="stylesheet" type="text/css" href="<?= STATIC_ADMIN_CSS_PATH ?>style.css?i=<?php echo time(); ?>">

        <!-- jQuery -->
        <script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>jquery.min.js"></script>
        <script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>jquery.validate.min.js"></script>
        <script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>lib/angular.min.js"></script>
        <script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>lib/angular-cookies.min.js"></script>
        <script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>lib/angular-sanitize.min.js"></script>
        <script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>app.js?r=<?= time() ?>"></script>
        <script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>commonCtrl.js?r=<?= time() ?>"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
            var projectConfig = {
            date_format : 'dd-mm-yy'
            };
        </script>
    </head>
    <body ng-controller="commonCtrl">

        <div class="noty"  data-show="{{notyDisplay}}" ng-class="notyType">
            <i></i>
            <strong>{{notyTitle}}</strong>
            <p ng-bind-html="notyMessage"></p>
        </div>


        <div class="sweetyOverlay" data-show="{{sweetyDisplay}}"></div>
        <div class="sweety" data-show="{{sweetyDisplay}}">
            <div class="sweetyTitle">Are you sure you wish to delete this? <strong>{{sweetyTitle}}</strong></div>
            <button type="button" ng-click="action(true)" class="btn btn-primary">Yes</button>
            <button type="button" ng-click="action(false)" class="btn btn-default">No</button>
        </div>


        <div class="spinny" ng-show="spinnyLoader">
            <div class="spinnyLoader"><i class="fal fa-circle-notch fa-spin"></i></div>

        </div>

        <div class="whitebox whitebox-login" ng-controller="loginCtrl" ng-keyup="login_error = {}">
        <!-- <div class="whiteboxHead"><strong>Login</strong></div> -->
            <form ng-submit="VerifyLogin()" id="loginForm" ng-init="error_message = '<?= $this->session->flashdata('msg') ?>'">

                <div class="alert alert-danger" ng-show="error_message">
                    <button type="button" class="close" ng-click="error_message = ''"  aria-hidden="true">&times;</button>
                    <strong>Note!</strong> <span ng-bind-html="error_message"></span>
                </div>
                
                <img src="<?= STATIC_ADMIN_IMAGES_PATH ?>logo.png" style=" width: 100%; border-radius: 10px; padding: 15px 20px;     background: linear-gradient(to bottom, #9e4efc 0%, #570dc8 100%); ">

                <div class="form-group" style="margin-top: 20px;">
                    <label for="username">User Name</label>
                    <input type="text" class="form-control input-lg" name="username" placeholder="User Name" ng-model="user.username">
                    <span class="text-danger">{{login_error.username}}</span>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control input-lg" name="password" placeholder="Password" ng-model="user.password">
                    <span class="text-danger">{{login_error.password}}</span>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Login <i class="fal fa-arrow-right"></i></button>
                </div>
                <!--<a href="<?= base_url() ?>forgot_password" class="pull-right">Forgot Password?</a>-->
            </form>
        </div>

        <script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/loginCtrl.js?r=<?= time() ?>"></script>
        <script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/loginService.js?r=<?= time() ?>"></script>

        <script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>jquery.ui.touch-punch.min.js"></script>
        <script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>jquery-ui-timepicker-addon.min.js"></script>
        <script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>jquery.duplicate.min.js"></script>
        <script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>jquery.rateyo.min.js"></script>
        <script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>SmoothScroll.min.js"></script>
        <script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>raphael-min.js"></script>
        <script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>morris.min.js"></script>

        <script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>custom.js?i=<?php echo time(); ?>"></script>
    </body>
</html>