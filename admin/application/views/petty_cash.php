<section class="pageWrapper" ng-controller="pettyCashCtrl" ng-init="GetCentersList('<?= get_user_id() ?>');GetCentersListForAddingPettyCash('<?= get_user_id() ?>')">
    <div class="pageHeader" workspace-offset valign-parent >
        <div class="row">
            <div class="col-md-7" ><strong>Petty Cash</strong></div>
            <div class="col-md-4 ">
                <div valign-holder>
                    <div class="row">   
                        <div class="col-md-8 pull-right">
                            <div class="custom-input" title="My Center">
                                <select class="form-control" ng-model="filter.centers_id" ng-disabled="!centersList.length > 0" ng-change="UpdateCenterSession('<?= get_user_id() ?>', filter.centers_id)">
                                    <option value="" selected>All centers</option>
                                    <option ng-value="item.id" value="item.id" ng-repeat="item in centersList">{{item.center_name}}</option>
                                </select>
                                <span class="ci-icon">
                                    <i ng-show="centersSpinner" class="fal fa-circle-notch fa-spin"></i>
                                    <i ng-show="!centersSpinner" class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-1">
                <div valign-holder class="text-right" >
                    <button type="button" class="btn btn-primary" ng-click="ShowPettycashAddForm()"><i class="fal fa-plus"></i> Add Petty Cash</button>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace  >
        <div >
            <div class="whitebox">
                <div class="row rm-5">
                    <form autocomplete="off" class="" ng-submit="GetPettyCashList(filter.page = 1)">

                        <div class="col-md-3 cp-5">
                            <div class="custom-group">
                                <div class="custom-input">
                                    <input autocomplete="off" type="text" class="form-control datepickerNormal" placeholder="From Date" name="from_date" ng-model="filter.from_date">
                                    <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 cp-5">
                            <div class="custom-group">
                                <div class="custom-input">
                                    <input autocomplete="off" type="text" class="form-control datepickerNormal" placeholder="To Date" name="to_date" ng-model="filter.to_date">
                                    <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 cp-5">
                            <button type="submit" class="btn btn-primary "><i class="fas fa-search"></i></button>
                            <button type="reset" ng-click="ResetFilter()" class="btn btn-danger " title="Clear Search"><i class="fas fa-times-circle"></i></button>
                            <button type="button" class="btn btn-info" ng-click="ExportToExcel()" title="Export To Excel"><i class="far fa-download"></i> <i class="far fa-file-excel"></i></button>
                        </div>
                    </form>
                </div>

            </div>

            <div >
                <div class="row rm-5 inactive" same-height-row >
                    <div class="col-md-3 col-xs-6 cp-5">
                        <div class="whitebox mb-10-xs text-center" same-height-col>
                            <strong>Month - to - Date</strong>
                            <h4 class="mb-0-xs font-bold text-success"><i class="fal fa-rupee-sign"></i> {{month.cash?month.cash:'0'}}</h4>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6 cp-5">
                        <div class="whitebox mb-10-xs text-center" same-height-col>
                            <strong>Total cash</strong>
                            <h4 class="mb-0-xs font-bold text-success"><i class="fal fa-rupee-sign"></i> {{total.cash?total.cash:'0'}}</h4>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6 cp-5">
                        <div class="whitebox mb-10-xs text-center" same-height-col>
                            <strong >Deposited cash <span ng-if="filter.from_date != ''">between <span  ng-bind-html="filter.from_date"></span> - <span ng-bind-html="filter.to_date"></span></span>  </strong>
                            <h4 class="mb-0-xs font-bold text-success"><i class="fal fa-rupee-sign"></i> {{filtered.cash?filtered.cash:'0'}}</h4>
                        </div>
                    </div>

                </div>

            </div>

            <div class="responsive-table" ng-hide="pettyCashList.length > 0">
                <div class="alert alert-danger">
                    No Data found
                </div>
            </div> 
            <div class="responsive-table" ng-show="pettyCashList.length > 0">
                <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions" >
                    <thead>
                        <tr>

                            <th>S.No</th>
                            <th>Center</th>
                            <th>Remarks</th>
                            <th>Amount(Rs/-)</th>
                            <th>Type</th>
                            <th>Date</th>
                            <th>Added By</th>
                            <th>Attachment1</th>
<!--                            <th>Updated By</th>-->
<!--                            <th class="no-sort text-right">Action</th>-->
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="item in pettyCashList track by $index">

                            <td data-label="SNO">{{$index + 1}}</td>
                            <td data-label="Center">{{item.center_name}}</td>
                            <td data-label="Remarks">{{item.remarks}}</td>
                            <td data-label="Amount" >{{item.amount}}</td>
                            <td data-label="Type">{{item.type}}</td>
                            <td data-label="Date">{{item.created_at}}</td>
                            <td data-label="Added by">{{item.user_name1}}</td>
                            <td data-label="Attachment">
                                <a ng-show="{{item.attachment1 != NULL}}" class="text-primary"  ng-href="<?= base_url() ?>uploads/petty_cash_receipts/{{item.attachment1}}" target="_blank"><strong>Attachment1</strong></a>
                            </td>
<!--                            <td data-label="Updated by">{{item.user_name2}}</td>-->

<!--                            <td data-label="Action" class="text-right">
                                <span class="dropdown" >
                                    <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                    <div class="clear"></div>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li ng-click="EditPettyCash(item)"><a href="#">Edit</a></li>
                                       <li ng-click="DeletePettyCash(item)"><a href="#">Delete</a></li>
                                    </ul>
                                </span>
                            </td>-->
                        </tr>
                    </tbody>
                </table>
            </div>
            <span class="pull-right" ng-bind-html="pettCashPagination.pagination"></span>
        </div>
    </div>
    <div class="pageFooter" workspace-offset >&copy; Freshup Desk <?= date('Y') ?></div>

    <div class="modal fade" id="pettycash-modal-popup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{pettyCashObj.id?"Update":"Add"}} Expense</h4>
                </div>
                <div class="modal-body">
                    <form ng-submit="AddOrUpdatePettyCash()" id="petty_cash_form" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="text-danger" ng-bind-html="error_message"></span>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Center</label>
                                    <select class="form-control" name="centers_id" ng-model="pettyCashObj.centers_id" ng-disabled="!centersListForAddPettyCash.length > 0">
                                        <option value="" selected disabled>Select Center</option>
                                        <option ng-value="item.id" value="item.id" ng-repeat="item in centersListForAddPettyCash">{{item.center_name}}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Amount</label>
                                    <input type="text" class="form-control number" min="1" name="amount" ng-model="pettyCashObj.amount">
                                </div>
                            </div>


                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Remarks</label>
                                    <textarea rows="4" cols="3" class="form-control" name="remarks" ng-model="pettyCashObj.remarks"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label> Attachment 1 </label>
                                    <div class="input-group">
                                        <input type="file" class="form-control"  name="{{pettyCashObj.id?'':'attachment1'}}" ng-model="pettyCashObj.attachment1" id="attachment1" base-sixty-four-input>
                                        <div>Images,Pdf, Docx, Doc formats only allowed here</div>
                                        <a ng-show="pettyCashObj.id && pettyCashObj.attachment1 != NULL" class="text-primary"  ng-href="<?= base_url() ?>uploads/expense_receipts/{{pettyCashObj.attachment1}}" target="_blank"><strong>Previous Attachment1</strong></a>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="row">
                            <div class="col-md-3 pull-right">
                                <button class="btn btn-default  " ng-click="ResetForm()" data-dismiss="modal"><b>Cancel <i class="fal fa-times"></i></b></button>
                                <button class="btn btn-primary pull-right" type="submit"><b>{{pettyCashObj.id?"Update":"Add"}} <i class="fal fa-arrow-right"></i></b></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</section>
<div id="dialog-confirm" title="Confirm">
    <p>
        <span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>
        This item will be deleted. Are you sure?
    </p>
</div>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/pettyCashCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/pettyCashService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/centersService.js?r=<?= time() ?>"></script>