<section class="pageWrapper" ng-init="filter.ref_id:'<?= $this->input->get_post('ref_id') ? $this->input->get_post('ref_id') : '' ?>'"  ng-controller="joinedPlayersCtrl" >
    <div class="pageHeader" workspace-offset valign-parent >
        <div class="row">
            <div class="col-md-6"><strong>Real Tournaments > Upcoming Tournaments > Tournament name > Joined Players List</strong></div>
            <div class="col-md-6">
                <div valign-holder class="text-right">

                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace  ng-init="GetJoinedPlayersList(filter.ref_id =<?= $this->input->get_post('ref_id') ?>);">
        <div class="whitebox">
            <div class="row rm-5">
                <form class="" ng-submit="GetJoinedPlayersList(filter.page = 1)">
                    <div class="col-md-3 cp-5">
                        <div class="form-group mb-0-xs"> 
                            <div class="custom-input" title="Playername">
                                <input type="text" class="form-control" id="search_key" ng-model="filter.search_key" placeholder="Search Key">
                                <span class="ci-icon">
                                    <i class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 cp-5">
                        <button type="submit" class="btn btn-primary "><i class="fas fa-search"></i></button>
                        <button type="reset" ng-click="ResetFilter()" class="btn btn-danger " title="Clear Search"><i class="fas fa-times-circle"></i></button>
                    </div>
                </form>
            </div>
        </div>
        <div class="responsive-table" ng-hide="joinedPlayersList.length > 0">
            <table class="table table-custom table-bordered data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                <h3 align="center">No records found</h3>
                <thead>
            </table>
        </div>
        <div class="responsive-table" ng-show="joinedPlayersList.length > 0">
            <table class="table table-custom table-bordered data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Player Name</th>
                        <th>Player Type</th>
<!--                        <th>Frequency</th>-->
                        <th>Mobile</th>
                        <th>Email</th>
                        <th>Position In Tournament</th>
                        <th>Tournament Round No</th>
                        <th>Position In Round</th>
                        <th>Joined Date</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in joinedPlayersList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Player Name">{{item.player_name}}</td>
                        <td data-label="Player Type" class="{{item.player_type_text_css}}">{{item.player_type}}</td>
<!--                        <td data-label="Frequency">{{item.frequency}}</td>-->
                        <td data-label="Mobile">{{item.mobile}}</td>
                        <td data-label="Email">{{item.email}}</td>
                        <td data-label="position In Tournament">{{item.position_in_tournament}}</td>
                        <td data-label="Tournament Round No">{{item.tournament_round_no}}</td>
                        <td data-label="Position in Round No">{{item.position_in_round}}</td>
                        <td data-label="Joined Date">{{item.joined_date_time}}</td>
                    </tr>
                </tbody>
            </table>
            <span class="pull-right" ng-bind-html="joinedPlayersPagination.pagination"></span>
        </div>
    </div>
    <div class="pageFooter" workspace-offset></div>

</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/real_tournaments/joinedPlayersCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/real_tournaments/joinedPlayersService.js?r=<?= time() ?>"></script>