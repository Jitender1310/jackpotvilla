<section class="pageWrapper" ng-controller="corporateOfficesCtrl">
    <div class="pageHeader" workspace-offset valign-parent>
        <div class="row">
            <div class="col-md-6"><strong>Corporate Offices</strong></div>
            <div class="col-md-6">
                <div valign-holder class="text-right">
                    <button type="button" class="btn btn-primary" ng-click="ShowCorporateOfficeAddForm()"><i class="fal fa-plus"></i> Add Corporate Office</button>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="GetCorporateOfficesList()">
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Country</th>
                        <th>State</th>
                        <th>City</th>
                        <th>Location</th>
                        <th>Corporate Office Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>GST</th>
                        <th>Address</th>
                        <th>Status</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in corporateOfficesList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Country Name"> {{item.country_name}}</td>
                        <td data-label="State Name"> {{item.state_name}}</td>
                        <td data-label="City Name"> {{item.city_name}}</td>
                        <td data-label="Location Name"> {{item.location_name}}</td>
                        <td data-label="Corporate Office Name"> {{item.corporate_office_name}}</td>
                        <td data-label="Email"> {{item.email}}</td>
                        <td data-label="Mobile"> {{item.mobile}}</td>
                        <td data-label="GST"> {{item.gst_number}}</td>
                        <td data-label="Address">{{item.address}}</td>
                        <td data-label="Running Status">
                            <span class="text-{{item.office_status=='Inactive'?'danger':'success'}}">{{item.office_status}}</span>
                        </td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditCorporateOffice(item)"><a href="#">Edit</a></li>
                                    <li ng-click="DeleteCorporateOffice(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk - <?= date('Y') ?></div>
    <div class="modal fade" id="corporate_office_form">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{corporateOfficesObj.id?"Update":"Add"}} Corporate Office</h4>
                </div>
                <div class="modal-body">
                    <form ng-submit="AddOrUpdateCorporateOffice()" id="corporate_form" >
                        <span class="text-danger" ng-bind-html="error_message"></span>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Country</label>
                                    <div class="custom-input">
                                        <select class="form-control" name="countries_id" ng-model="corporateOfficesObj.countries_id" ng-change="GetStatesList(corporateOfficesObj.countries_id)" ng-disabled="!countriesList.length > 0">
                                            <option value="" selected disabled="">Choose Country</option>
                                            <option value="{{item.id}}" ng-repeat="item in countriesList track by $index">{{item.country_name}}</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i class="fal fa-chevron-down"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>State</label>
                                    <div class="custom-input">
                                        <select class="form-control" name="states_id" ng-model="corporateOfficesObj.states_id" ng-change="GetCitiesList(corporateOfficesObj.states_id)" ng-disabled="!statesList.length > 0">
                                            <option value="" selected disabled="">Choose State</option>
                                            <option value="{{item.id}}" ng-repeat="item in statesList track by $index">{{item.state_name}}</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i ng-show="!stateSpinner" class="fal fa-chevron-down"></i>
                                            <i ng-show="stateSpinner" class="fal fa-circle-notch fa-spin"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Choose City</label>
                                    <div class="custom-input">
                                        <select class="form-control" name="cities_id" ng-model="corporateOfficesObj.cities_id" ng-change="GetLocationsList(corporateOfficesObj.cities_id)" ng-disabled="!citiesList.length > 0">
                                            <option value="" selected disabled="">Choose City</option>
                                            <option value="{{item.id}}" ng-repeat="item in citiesList track by $index">{{item.city_name}}</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i ng-show="!citySpinner" class="fal fa-chevron-down"></i>
                                            <i ng-show="citySpinner" class="fal fa-circle-notch fa-spin"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Location</label>
                                    <div class="custom-input">
                                        <select class="form-control" name="locations_id" ng-model="corporateOfficesObj.locations_id"  ng-disabled="!locationsList.length > 0">
                                            <option value="" selected disabled="">Choose Location</option>
                                            <option value="{{item.id}}" ng-repeat="item in locationsList track by $index">{{item.location_name}}</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i ng-show="!locationSpinner" class="fal fa-chevron-down"></i>
                                            <i ng-show="locationSpinner" class="fal fa-circle-notch fa-spin"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Corporate Office Name</label>
                                    <input type="text" class="form-control" name="corporate_office_name" ng-model="corporateOfficesObj.corporate_office_name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Mobile</label>
                                    <input type="text" class="form-control" name="mobile" ng-model="corporateOfficesObj.mobile">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email" ng-model="corporateOfficesObj.email">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>GST Number</label>
                                    <input type="text" class="form-control" name="gst_number" ng-model="corporateOfficesObj.gst_number" minlength="15" maxlength="15" pattern="[a-zA-Z0-9\s]+">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea class="form-control" rows="4" name="address" ng-model="corporateOfficesObj.address"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Office Status</label>
                                    <select class="form-control" name="office_status" ng-model="corporateOfficesObj.office_status">
                                        <option value="">-Select Status-</option>
                                        <option value="Active">Actvie</option>
                                        <option value="Inactive">Inactvie</option>
                                    </select>
                                </div>
                            </div>

                        </div>


                        <button class="btn btn-default " ng-click="ResetForm()" data-dismiss="modal"><b>Cancel <i class="fal fa-times"></i></b></button>
                        <button class="btn btn-primary pull-right" type="submit"><b>{{corporateOfficesObj.id?"Update":"Add"}} <i class="fal fa-arrow-right"></i></b></button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--View Center popup end-->

    <!--Pause Center Popup end-->
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/corporateOfficesCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/corporateOfficesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/countriesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/statesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/citiesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/locationsService.js?r=<?= time() ?>"></script>