<section class="pageWrapper" ng-controller="reportedProblemsCtrl">
    <div class="pageHeader" workspace-offset valign-parent >
        <div class="row">
            <div class="col-md-6"><strong>Feedback > Reported Problems</strong></div>
            <div class="col-md-6">
                <div valign-holder class="text-right">
                    <button type="button" class="btn btn-primary" ng-click="ShowProblemAddForm()"><i class="fal fa-plus"></i> Report A Problem</button>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Reported Player</th>
                        <th>Contact Name</th>
                        <th>Email</th>
                        <th>Type of Issue</th>
                        <th>Message</th>
                        <th>Status</th>
                        <th>Reported Date</th>
                        <th>Updated Date</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in problemList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Reported Player">{{item.player_mobile_number}}</td>
                        <td data-label="Contact Name">{{item.name}}</td>
                        <td data-label="Email">{{item.email}}</td>
                        <td data-label="Type of Issue">{{item.type_of_issue}}</td>
                        <td data-label="Message">{{item.message}}</td>
                        <td data-label="Status" ng-class="item.status_css">{{item.status}}</td>
                        <td data-label="Reported Date">{{item.created_at}}</td>
                        <td data-label="Updated Date">{{item.updated_at}}</td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="ViewProblem(item)"><a href="#">View</a></li>
                                    <!--<li ng-click="UpdateProblemStatus(item)"><a href="#">Update Status</a></li>-->
                                    <li ng-click="DeleteProblem(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageFooter" workspace-offset></div>

    <div class="modal fade" id="report-modal-popup">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{problemObj.id?"Update":"Add"}} Problem</h4>
                </div>
                <div class="modal-body">
                    <form ng-submit="AddOrUpdateProblem()" id="reportForm" ng-keyup="g_error = {}">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="text-danger" ng-bind-html="error_message"></span>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Mobile Number <span class="text-danger">*</span></label>
                                    <div class="custom-input">
                                        <input class="form-control" type="text" name="player_mobile_number" ng-model="reportObj.player_mobile_number" required>
                                    </div>
                                </div>
                                <label class="error" ng-if="g_error.player_mobile_number">{{g_error.player_mobile_number}}</label>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Contact Name <span class="text-danger">*</span></label>
                                    <div class="custom-input">
                                        <input class="form-control" type="text" name="name" ng-model="reportObj.name" required>
                                    </div>
                                </div>
                                <label class="error" ng-if="g_error.name">{{g_error.name}}</label>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email Address <span class="text-danger">*</span></label>
                                    <div class="custom-input">
                                        <input class="form-control" type="email" name="email" ng-model="reportObj.email" required>
                                    </div>
                                </div>
                                <label class="error" ng-if="g_error.email">{{g_error.email}}</label>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Type of Issue <span class="text-danger">*</span></label>
                                    <div class="custom-input">
                                        <input class="form-control" type="text" name="type_of_issue" ng-model="reportObj.type_of_issue" required>
                                    </div>
                                </div>
                                <label class="error" ng-if="g_error.type_of_issue">{{g_error.type_of_issue}}</label>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Message <span class="text-danger">*</span></label>
                                    <div class="custom-input">
                                        <textarea class="form-control" required rows="5" name="message" ng-model="reportObj.message"></textarea>
                                    </div>
                                </div>
                                <label class="error" ng-if="g_error.message">{{g_error.message}}</label>
                            </div>
                        </div>

                        <button class="btn btn-default " ng-click="ResetForm()" data-dismiss="modal"><b>Cancel <i class="fal fa-times"></i></b></button>
                        <button class="btn btn-primary pull-right" type="submit"><b>{{reportObj.id?"Update":"Add"}} <i class="fal fa-arrow-right"></i></b></button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="view-problem-modal-popup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Problem Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Reported Player</th>
                                        <td>
                                            {{reportObj.player_mobile_number}}
                                        </td>
                                        <th>Contact Name</th>
                                        <td>
                                            {{reportObj.name}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td>
                                            {{reportObj.email}}
                                        </td>
                                        <th>Status</th>
                                        <td>
                                            {{reportObj.status}}
                                        </td>
                                    </tr>
                                    <tr>
                                         <th>Type of Issue</th>
                                        <td>
                                            {{reportObj.type_of_issue}}
                                        </td>
                                        <th>Message</th>
                                        <td>
                                            {{reportObj.message}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Reported On</th>
                                        <td>
                                            {{reportObj.created_at}}
                                        </td>
                                        <th>Last Updated On</th>
                                        <td>
                                            {{reportObj.updated_at}}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class='col-md-12'>
                            <h4 style='margin-top: 0px;'>Logs</h4>
                            <div class="log-container">
                                <div class='log-single' ng-repeat="log in logList">
                                    <p><b>{{log.username}}</b> <small>{{log.created_at}}</small></p>
                                    <p>{{log.message}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <form ng-submit="AddProblemLog()" id="reportLogForm" ng-keyup="g_error = {}">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class='form-group'>
                                            <label>Status</label>
                                            <select class='form-control' ng-model='report_log.status'>
                                                <option value='New'>New</option>
                                                <option value='In Progress'>In Progress</option>
                                                <option value='Complete'>Complete</option>
                                                <option value='Invalid'>Invalid</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6"> </div>
                                    <div class='col-md-12'>
                                        <div class='form-group'>
                                            <label>Message</label>
                                            <textarea type="text" class="form-control" rows='3' ng-model='report_log.message'></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class='col-md-12'>
                                    <button class="btn btn-danger pull-left" data-dismiss="modal"><b>Close <i class="fal fa-times"></i></b></button>
                                    <button type='submit' class='btn btn-primary pull-right'>Submit <i class="fal fa-arrow-right"></i></button>
                                    <div class='clearfix'></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<style type='text/css'>
    .log-container{
        height:250px;
        overflow-y: scroll;
    }
    .log-container .log-single{
        margin-bottom:5px;
        background-color:#f9f2f2;
        border-radius:5px;
        padding:10px;
        padding-bottom: 1px;
    }
</style>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/feedback/reportedProblemsCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/feedback/reportedProblemsService.js?r=<?= time() ?>"></script>