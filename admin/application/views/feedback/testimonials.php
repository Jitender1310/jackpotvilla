<section class="pageWrapper" ng-controller="testimonialsCtrl">
    <div class="pageHeader" workspace-offset valign-parent >
        <div class="row">
            <div class="col-md-6"><strong>Testimonials</strong></div>
            <div class="col-md-6">
                <div valign-holder class="text-right">
                    <button type="button" class="btn btn-primary" ng-click="ShowTestimonialAddForm()"><i class="fal fa-plus"></i> Add Testimonial</button>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Name</th>
                        <th>Prize Title</th>
                        <th>Message</th>
                        <th>Image</th>
                        <th>Created Time</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in testimonialsList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Name">{{item.name_and_address}}</td>
                        <td data-label="Prize Title">{{item.prize_title}}</td>
                        <td data-label="Message">{{item.message}}</td>
                        <td data-label="Image">
                            <a target="_blank" href="{{item.image}}">
                                <img src="{{item.image}}" style="width:100px;height: auto"/>
                            </a>
                        </td>
                        <td data-label="Registered Time">{{item.created_date_time}}</td>
                        <td data-label="Actions" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="DeleteTestimonial(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageFooter" workspace-offset></div>

    <div class="modal fade" id="testimonial-modal-popup">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{testimonialObj.id?"Update":"Add"}} Testimonial</h4>
                </div>
                <div class="modal-body">
                    <form ng-submit="AddOrUpdateTestimonial()" id="testimonialForm" ng-keyup="t_error = {}">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="text-danger" ng-bind-html="error_message"></span>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Name and Address</label>
                                    <input type="text" class="form-control" name="name_and_address" ng-model="testimonialObj.name_and_address">
                                    <label class="error" ng-if="t_error.name_and_address">{{t_error.name_and_address}}</label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Prize Title</label>
                                    <input type="text" class="form-control" name="prize_title" ng-model="testimonialObj.prize_title">
                                    <label class="error" ng-if="t_error.prize_title">{{t_error.prize_title}}</label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Message</label>
                                    <textarea class="form-control" name="message" ng-model="testimonialObj.message"></textarea>
                                    <label class="error" ng-if="t_error.message">{{t_error.message}}</label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Image</label>
                                    <input type="file" class="form-control" name="image" id="image">
                                    <label class="error" ng-if="t_error.image">{{t_error.image}}</label>
                                </div>
                            </div>
                        </div>

                        <br/>
                        <br/>

                        <button class="btn btn-default " ng-click="ResetForm()" data-dismiss="modal"><b>Cancel <i class="fal fa-times"></i></b></button>
                        <button class="btn btn-primary pull-right" type="submit"><b>{{testimonialObj.id?"Update":"Add"}} <i class="fal fa-arrow-right"></i></b></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/testimonialsCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/testimonialsService.js?r=<?= time() ?>"></script>