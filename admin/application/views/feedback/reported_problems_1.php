<section class="pageWrapper" ng-controller="reportedProblemsCtrl">
    <div class="pageHeader" workspace-offset valign-parent >
        <div class="row">
            <div class="col-md-6"><strong>Feedback > Reported Problems</strong></div>
            <div class="col-md-6">
                <div valign-holder class="text-right">
                    <button type="button" class="btn btn-primary" ng-click="ShowProblemAddForm()"><i class="fal fa-plus"></i> Report A Problem</button>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="getProblemsList();">
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Reported Player</th>
                        <th>Contact Name</th>
                        <th>Email</th>
                        <th>Type of Issue</th>
                        <th>Message</th>
                        <th>Reported Date</th>
                        <th>Updated Date</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in gamesList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Reported Player">{{item.game_title}}</td>
                        <td data-label="Contact Name">{{item.game_type}}</td>
                        <td data-label="Email">{{item.number_of_deck}}</td>
                        <td data-label="Type of Issue">{{item.seats}}</td>
                        <td data-label="Message">{{item.point_value}}</td>
                        <td data-label="Reported Date">{{item.created_at}}</td>
                        <td data-label="Updated Date">{{item.updated_at}}</td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditProblem(item)"><a href="#">Edit</a></li>
                                    <li ng-click="DeleteProblem(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageFooter" workspace-offset></div>

    <div class="modal fade" id="game-modal-popup">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{problemObj.id?"Update":"Add"}} Problem</h4>
                </div>
                <div class="modal-body">
                    <form ng-submit="AddOrUpdateProblem()" id="gameForm" ng-keyup="g_error = {}">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="text-danger" ng-bind-html="error_message"></span>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Game Type <span class="text-danger">*</span></label>
                                    <div class="custom-input">
                                        <select class="form-control" name="game_type" required ng-model="gameObj.game_type">
                                            <option value="" selected disabled="">Choose</option>
                                            <option value="Cash">Cash Rummy</option>
                                            <option value="Practice">Practice Rummy</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i class="fal fa-chevron-down"></i>
                                        </span>
                                    </div>
                                </div>
                                <label class="error" ng-if="g_error.game_type">{{g_error.game_type}}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Game Sub Type <span class="text-danger">*</span></label>
                                    <div class="custom-input">
                                        <select class="form-control" name="game_sub_type" required ng-model="gameObj.game_sub_type">
                                            <option value="">Choose</option>
                                            <option value="Points">Points Rummy</option>
                                            <option value="Pool">Pool Rummy</option>
                                            <option value="Deals">Deals Rummy</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i class="fal fa-chevron-down"></i>
                                        </span>
                                    </div>
                                </div>
                                <label class="error" ng-if="g_error.game_sub_type">{{g_error.game_sub_type}}</label>
                            </div>
                        </div>
                        <div class="row" ng-if="gameObj.game_sub_type == 'Points'">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Number Of Cards <span class="text-danger">*</span></label>
                                    <div class="custom-input">
                                        <select class="form-control" name="game_type" required ng-model="gameObj.number_of_cards">
                                            <option value="" selected disabled="">Choose </option>
                                            <option value="13">13 Cards</option>
                                            <option value="21">21 Cards</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i class="fal fa-chevron-down"></i>
                                        </span>
                                    </div>
                                </div>
                                <label class="error" ng-if="g_error.number_of_cards">{{g_error.number_of_cards}}</label>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Number Of Decks <span class="text-danger">*</span></label>
                                    <div class="custom-input">
                                        <select class="form-control" name="number_of_deck" required ng-model="gameObj.number_of_deck">
                                            <option value="">Choose Game Sub Type</option>
                                            <option value="1">1 Deck</option>
                                            <option value="2">2 Decks</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i class="fal fa-chevron-down"></i>
                                        </span>
                                    </div>
                                </div>
                                <label class="error" ng-if="g_error.number_of_deck">{{g_error.number_of_deck}}</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Seats <span class="text-danger">*</span></label>
                                    <div class="custom-input">
                                        <select class="form-control" name="seats" required ng-model="gameObj.seats">
                                            <option value="" selected disabled="">Choose</option>
                                            <option value="2">2 Seats</option>
                                            <option value="6">6 Seats</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i class="fal fa-chevron-down"></i>
                                        </span>
                                    </div>
                                </div>
                                <label class="error" ng-if="g_error.seats">{{g_error.seats}}</label>
                            </div>
                        </div>

                        <div class="row" ng-if="gameObj.game_sub_type == 'Points'">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Point Value <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control number" required name="point_value" ng-model="gameObj.point_value">
                                </div>
                                <label class="error" ng-if="g_error.point_value">{{g_error.point_value}}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Entry Fee <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control number" required name="entry_fee" ng-model="gameObj.entry_fee">
                                </div>
                                <label class="error" ng-if="g_error.entry_fee">{{g_error.entry_fee}}</label>
                            </div>
                        </div>

                        <div class="row" ng-if="gameObj.game_sub_type != 'Points'">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Prize <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control number" required name="pool_deal_prize" ng-model="gameObj.pool_deal_prize">
                                </div>
                                <label class="error" ng-if="g_error.pool_deal_prize">{{g_error.pool_deal_prize}}</label>
                            </div>
                        </div>

                        <div class="row" ng-if="gameObj.game_sub_type == 'Pool'">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Pool Game Type <span class="text-danger">*</span></label>
                                    <div class="custom-input">
                                        <select class="form-control" name="pool_game_type" required ng-model="gameObj.pool_game_type">
                                            <option value="" selected disabled="">Choose</option>
                                            <option value="101">101</option>
                                            <option value="201">201</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i class="fal fa-chevron-down"></i>
                                        </span>
                                    </div>
                                </div>
                                <label class="error" ng-if="g_error.pool_game_type">{{g_error.pool_game_type}}</label>
                            </div>
                        </div>

                        <div class="row" ng-if="gameObj.game_sub_type == 'Deals'">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Deals <span class="text-danger">*</span></label>
                                    <div class="custom-input">
                                        <select class="form-control" name="deals" required ng-model="gameObj.deals">
                                            <option value="" selected disabled="">Choose</option>
                                            <option value="2">2 Deals</option>
                                            <option value="3">3 Deals</option>
                                            <option value="6">6 Deals</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i class="fal fa-chevron-down"></i>
                                        </span>
                                    </div>
                                </div>
                                <label class="error" ng-if="g_error.deals">{{g_error.deals}}</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Status <span class="text-danger">*</span></label>
                                    <div class="custom-input">
                                        <select class="form-control" name="active" required ng-model="gameObj.active">
                                            <option value="" selected disabled="">Choose</option>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i class="fal fa-chevron-down"></i>
                                        </span>
                                    </div>
                                </div>
                                <label class="error" ng-if="g_error.deals">{{g_error.deals}}</label>
                            </div>
                        </div>

                        <button class="btn btn-default " ng-click="ResetForm()" data-dismiss="modal"><b>Cancel <i class="fal fa-times"></i></b></button>
                        <button class="btn btn-primary pull-right" type="submit"><b>{{gameObj.id?"Update":"Add"}} <i class="fal fa-arrow-right"></i></b></button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="view-user-modal-popup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Employee Details</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="width-700 mx-auto-xs">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Full Name</th>
                                        <td>
                                            {{gameObj.fullname}}
                                        </td>
                                    </tr>
                                    <tr ng-if="gameObj.user_meta.payroll_status == 1">
                                        <th>Email</th>
                                        <td>
                                            {{gameObj.user_meta.payroll_status_text}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Mobile</th>
                                        <td>
                                            {{gameObj.user_meta.mobile}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Role</th>
                                        <td>
                                            {{gameObj.role_name}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Login Status</th>
                                        <td>
                                            {{gameObj.login_status_text}}
                                        </td>
                                    </tr>
                                    <tr ng-if="gameObj.login_status == 1">
                                        <th>Username</th>
                                        <td>
                                            {{gameObj.username}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Designation</th>
                                        <td>
                                            {{gameObj.user_meta.designation}}
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>Gender</th>
                                        <td>
                                            {{gameObj.user_meta.gender}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Date of Birth (DD/MM/YYYY)</th>
                                        <td>
                                            {{gameObj.user_meta.dob}}
                                        </td>
                                    </tr>
                                    <tr  ng-show="gameObj.user_meta.payroll_status == 1">
                                        <th>CTC</th>
                                        <td>
                                            {{gameObj.user_meta.ctc}}
                                        </td>
                                    </tr>
                                    <tr  ng-show="gameObj.user_meta.payroll_status == 1">
                                        <th>Net Salary</th>
                                        <td>
                                            {{gameObj.user_meta.net_salary}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Address</th>
                                        <td>
                                            {{gameObj.user_meta.address}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Date of Joining (DD/MM/YYYY)</th>
                                        <td>
                                            {{gameObj.user_meta.joining_date}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th><abbr title="Permanent Account Number">PAN</abbr> Card Number</th>
                                        <td>
                                            {{gameObj.user_meta.pan_no}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th><abbr title="Employees' State Insurance">ESI</abbr> Number</th>
                                        <td>
                                            {{gameObj.user_meta.esi_no}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th><abbr title="Provident Fund">PF</abbr> Number</th>
                                        <td>
                                            {{gameObj.user_meta.pf_no}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th><abbr title="Driving">License</abbr> Number</th>
                                        <td>
                                            {{gameObj.user_meta.license_no}}
                                        </td>
                                    </tr>

                                    <tr ng-show="gameObj.user_meta.payroll_status == 1">
                                        <th colspan="2">Bank Details</th>
                                    </tr>

                                    <tr  ng-show="gameObj.user_meta.payroll_status == 1">
                                        <th>Bank Name</th>
                                        <td>
                                            {{gameObj.user_meta.bank_name}}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-danger " data-dismiss="modal"><b>Close <i class="fal fa-times"></i></b></button>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/feedback/reportedProblemsCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/feedback/reportedProblemsService.js?r=<?= time() ?>"></script>