<section class="pageWrapper" ng-controller="stayingCategoriesCtrl">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-12"><strong>Stay Categories</strong></div>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="GetStayingCategoriesList()">
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Category Type</th>
                        <th>Category</th>
                        <th>For Gender</th>
                        <th>No of Persons<br/>
                            allowed per item</th>
                        <th>Display Note</th>
                        <th>Availability Status</th>
                        <th>Image</th>
                        <th>Real Image</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in stayingCategoriesList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Category">{{item.staying_types_name}}</td>
                        <td data-label="Category">{{item.category_name}}</td>
                        <td data-label="For Gender">{{item.gender}}</td>
                        <td data-label="No of Persons allowed per item">
                            {{item.no_of_persons_allowed_per_item}}
                        </td>
                        <td data-label="Display Note">{{item.display_note}}</td>
                        <td data-label="Availability Status">{{item.availability_status}}</td>
                        <td data-label="Image">
                            <img ng-src="{{item.image}}" class="img-thumbnail" style="height: 50px"/>
                        </td>
                        <td data-label="Real Pic">
                            <img ng-src="{{item.real_picture}}" class="img-thumbnail" style="height: 50px"/>
                        </td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditCategory(item)"><a href="#">Edit</a></li>
                                    <li ng-click="DeleteCategory(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageSidebar">
        <form ng-submit="AddOrUpdateStayingCategory()" id="stayingCategoryForm">
            <div class="SidebarHead offset">
                {{stayingCategoryObj.id?'Update':'Add'}} Stay Category
            </div>
            <div class="SidebarBody">

                <span class="text-danger" ng-bind-html="error_message"></span>

                <div class="form-group">
                    <label>Category Type</label>
                    <select class="form-control" name="staying_types_id" ng-model="stayingCategoryObj.staying_types_id">
                        <option value="" selected disabled="">Choose Category</option>
                        <option value="1">Hotels</option>
                        <option value="2">Freshup</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Category Name</label>
                    <input type="text" class="form-control" name="category_name" ng-model="stayingCategoryObj.category_name">
                </div>
                <div class="form-group">
                    <label>For Gender </label>
                    <div class="">
                        <label><input type="radio" ng-model="stayingCategoryObj.gender" name="gender" value="Both"><span></span> Both </label>
                    </div>
                    <div class="">
                        <label><input type="radio" ng-model="stayingCategoryObj.gender" name="gender" value="Male"><span></span> Male </label>
                    </div>
                    <div class="">
                        <label><input type="radio" ng-model="stayingCategoryObj.gender" name="gender" value="Female"><span></span> Female </label>
                    </div>
                </div>

                <div class="form-group">
                    <label>No of Persons allowed per item</label>
                    <input type="number" min="0" class="form-control number" name="no_of_persons_allowed_per_item" ng-model="stayingCategoryObj.no_of_persons_allowed_per_item">
                </div>

                <div class="form-group">
                    <label>Display Note</label>
                    <textarea class="form-control" name="display_note" ng-model="stayingCategoryObj.display_note"></textarea>
                </div>

                <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" name="description" ng-model="stayingCategoryObj.description"></textarea>
                </div>

                <div class="form-group">
                    <label>Availability Status</label>
                    <select class="form-control" name="availability_status" ng-model="stayingCategoryObj.availability_status">
                        <option value="" selected disabled="">Choose Availability Status</option>
                        <option value="Active" >Active</option>
                        <option value="Inactive">Inactive</option>
                    </select>
                </div>

                <div class="form-group rm-5">
                    <div class="col-sm-12 cp-5">
                        <div class="btn btn-default btn-block btn-file">
                            <input type="file" name="{{stayingCategoryObj.id?'':'image'}}" class="form-control" accept="image/x-png,image/gif,image/jpeg,image/png" maxsize="5000"
                                   ng-model="stayingCategoryObj.image" base-sixty-four-input>
                            <b>Browse Image </b> <i class="fas fa-ellipsis-h"></i>
                        </div>
                        <span ng-show="form.files.$error.maxsize">Files must not exceed 5000 KB</span>

                        <img ng-show="stayingCategoryObj.image.filetype"
                             ng-src='data:{{stayingCategoryObj.image.filetype}};base64,{{stayingCategoryObj.image.base64}}' style="height: 50px" />

                        <img ng-hide="stayingCategoryObj.image.filetype" ng-src="{{stayingCategoryObj.image}}" style="height: 50px"/>
                    </div>
                </div>

                <div class="form-group rm-5">
                    <div class="col-sm-12 cp-5">
                        <div class="btn btn-default btn-block btn-file">
                            <input type="file" name="{{stayingCategoryObj.id?'':'image'}}" class="form-control" accept="image/x-png,image/gif,image/jpeg,image/png" maxsize="5000"
                                   ng-model="stayingCategoryObj.real_picture" base-sixty-four-input>
                            <b>Browse Real Image </b> <i class="fas fa-ellipsis-h"></i>
                        </div>
                        <span ng-show="form.files.$error.maxsize">Files must not exceed 5000 KB</span>

                        <img ng-show="stayingCategoryObj.real_picture.filetype"
                             ng-src='data:{{stayingCategoryObj.real_picture.filetype}};base64,{{stayingCategoryObj.real_picture.base64}}' style="height: 50px" />

                        <img ng-hide="stayingCategoryObj.real_picture.filetype" ng-src="{{stayingCategoryObj.real_picture}}" style="height: 50px"/>
                    </div>
                </div>

                <div class="form-group rm-5">
                    <div class="col-sm-12 cp-5">
                        <div class="btn btn-default btn-block btn-file">
                            <input type="file" name="{{stayingCategoryObj.id?'':'mobile_image_1'}}" class="form-control" accept="image/x-png,image/gif,image/jpeg,image/png" maxsize="5000"
                                   ng-model="stayingCategoryObj.mobile_image_1" base-sixty-four-input>
                            <b>Browse Mobile Image 1</b> <i class="fas fa-ellipsis-h"></i>
                        </div>
                        <span ng-show="form.files.$error.maxsize">Files must not exceed 5000 KB</span>

                        <img ng-show="stayingCategoryObj.mobile_image_1.filetype"
                             ng-src='data:{{stayingCategoryObj.mobile_image_1.filetype}};base64,{{stayingCategoryObj.mobile_image_1.base64}}' style="height: 50px" />

                        <img ng-hide="stayingCategoryObj.mobile_image_1.filetype" ng-src="{{stayingCategoryObj.mobile_image_1}}" style="height: 50px"/>
                    </div>
                </div>

                <div class="form-group rm-5">
                    <div class="col-sm-12 cp-5">
                        <div class="btn btn-default btn-block btn-file">
                            <input type="file" name="{{stayingCategoryObj.id?'':'mobile_image_2'}}" class="form-control" accept="image/x-png,image/gif,image/jpeg,image/png" maxsize="5000"
                                   ng-model="stayingCategoryObj.mobile_image_2" base-sixty-four-input>
                            <b>Browse Mobile Image 2</b> <i class="fas fa-ellipsis-h"></i>
                        </div>
                        <span ng-show="form.files.$error.maxsize">Files must not exceed 5000 KB</span>

                        <img ng-show="stayingCategoryObj.mobile_image_2.filetype"
                             ng-src='data:{{stayingCategoryObj.mobile_image_2.filetype}};base64,{{stayingCategoryObj.mobile_image_2.base64}}' style="height: 50px" />

                        <img ng-hide="stayingCategoryObj.mobile_image_2.filetype" ng-src="{{stayingCategoryObj.mobile_image_2}}" style="height: 50px"/>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="SidebarFooter offset">
                <div class="text-right">
                    <button class="btn btn-default" type="reset" ng-click="ResetForm()"><b>Cancel <i class="fal fa-times"></i></b></button>
                    <button type="submit" class="btn btn-primary"><b>{{stayingCategoryObj.id?'Update':'Add'}} <i class="fal fa-plus"></i></b></button>
                </div>
            </div>
        </form>
    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk 2018</div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/stayingCategoriesCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/stayingCategoriesService.js?r=<?= time() ?>"></script>