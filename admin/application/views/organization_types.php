<section class="pageWrapper" ng-controller="organizationTypesCtrl">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-12"><strong>Organization types</strong></div>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="GetTypes()">
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Title</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in typesList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Title">{{item.title}}</td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditType(item)"><a href="#">Edit</a></li>
                                    <li ng-click="DeleteType(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageSidebar">
        <form ng-submit="AddOrUpdateType()" id="typeForm">
            <div class="SidebarHead offset">
                {{typeObj.id?'Update':'Add'}} Organization type
            </div>
            <div class="SidebarBody">
                <span class="text-danger" ng-bind-html="error_message"></span>
                <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" name="title" ng-model="typeObj.title">
                </div>
            </div>
            <div class="SidebarFooter offset">
                <div class="text-right">
                    <button class="btn btn-default" type="reset" ng-click="ResetForm()"><b>Cancel <i class="fal fa-times"></i></b></button>
                    <button type="submit" class="btn btn-primary"><b>{{typeObj.id?'Update':'Add'}} <i class="fal fa-plus"></i></b></button>
                </div>
            </div>
        </form>
    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk 2018</div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/organizationTypesCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/organizationTypesService.js?r=<?= time() ?>"></script>