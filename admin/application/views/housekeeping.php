<section class="pageWrapper">
    <div class="pageHeader" workspace-offset  valign-parent>
        <div class="row">
            <div class="col-md-6"><strong>Housekeeping</strong></div>
            <div class="col-md-6 text-right hidden-xs">
                <ul class="ul-inline" valign-holder>
                    <li><i class="fas fa-circle text-danger"></i>Dirty</li>
                    <li><i class="fas fa-circle text-success"></i>Clean</li>
                    <li><i class="fas fa-circle text-warning"></i>Inspection</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>
        <div class="responsive-table">
            <table class="table table-custom data-table">
                <thead>
                    <tr>
                        <th>Room</th>
                        <th>Housing</th>
                        <th>Floor</th>
                        <th>Room Type</th>
                        <th>Status</th>
                        <th>Date & Time</th>
                        <th>Availability</th>
                        <th>Housekeepers</th>
                        <!-- <th class="no-sort text-right">Action</th> -->
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td data-label="Room" style="border-left: 3px solid #fb9678;">102</td>
                        <td data-label="Housing">BUILDING 4</td>
                        <td data-label="floor">4</td>
                        <td data-label="Room Type">double</td>
                        <td data-label="Status">
                            <span class="label label-danger"><i class="fal fa-times"></i> Dirty</span>
                            <a href="#modal-status" data-toggle="modal" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fal fa-cog"></i> Change</a>
                        </td>
                        <td data-label="Date & Time">05/02/2017 - 06:30 Pm</td>
                        <td data-label="Availability">Occupied</td>
                        <td data-label="Housekeepers">nuna, Jessica</td>
                    </tr>
                    <tr>
                        <td data-label="Room" style="border-left: 3px solid #00c292;">102</td>
                        <td data-label="Housing">BUILDING 4</td>
                        <td data-label="floor">4</td>
                        <td data-label="Room Type">double</td>
                        <td data-label="Status">
                            <span class="label label-success"><i class="fal fa-check"></i> Clean</span>
                            <a href="#modal-status" data-toggle="modal" class="btn btn-default btn-sm"><i class="fal fa-cog"></i> Change</a>
                        </td>
                        <td data-label="Date & Time">05/02/2017 - 06:30 Pm</td>
                        <td data-label="Availability">Available</td>
                        <td data-label="Housekeepers">nuna, Jessica <i class="fal fa-pen" data-target="#modal-housekeeper" data-toggle="modal"></i></td>
                    </tr>
                    <tr>
                        <td data-label="Room" style="border-left: 3px solid #fec107;">102</td>
                        <td data-label="Housing">BUILDING 4</td>
                        <td data-label="floor">4</td>
                        <td data-label="Room Type">double</td>
                        <td data-label="Status">
                            <span class="label label-warning"><i class="fal fa-paint-brush"></i> Inspection</span>
                            <a href="#modal-status" data-toggle="modal" class="btn btn-default btn-sm"><i class="fal fa-cog"></i> Change</a>
                        </td>
                        <td data-label="Date & Time">05/02/2017 - 06:30 Pm</td>
                        <td data-label="Availability">Available</td>
                        <td data-label="Housekeepers"><a href="#modal-housekeeper" data-toggle="modal" class="btn btn-default btn-sm"><i class="fal fa-plus"></i> Add Housekeeper</a></td>
                    </tr>
                    <tr>
                        <td data-label="Room" style="border-left: 3px solid #fb9678;">102</td>
                        <td data-label="Housing">BUILDING 4</td>
                        <td data-label="floor">4</td>
                        <td data-label="Room Type">double</td>
                        <td data-label="Status">
                            <span class="label label-success"><i class="fal fa-check"></i> Clean</span>
                            <a href="#modal-status" data-toggle="modal" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fal fa-cog"></i> Change</a>
                        </td>
                        <td data-label="Date & Time">05/02/2017 - 06:30 Pm</td>
                        <td data-label="Availability">Occupied</td>
                        <td data-label="Housekeepers">nuna, Jessica</td>
                    </tr>
                    <tr>
                        <td data-label="Room" style="border-left: 3px solid #00c292;">102</td>
                        <td data-label="Housing">BUILDING 4</td>
                        <td data-label="floor">4</td>
                        <td data-label="Room Type">double</td>
                        <td data-label="Status">
                            <span class="label label-success"><i class="fal fa-check"></i> Clean</span>
                            <a href="#modal-status" data-toggle="modal" class="btn btn-default btn-sm"><i class="fal fa-cog"></i> Change</a>
                        </td>
                        <td data-label="Date & Time">05/02/2017 - 06:30 Pm</td>
                        <td data-label="Availability">Available</td>
                        <td data-label="Housekeepers">nuna, Jessica <i class="fal fa-pen" data-target="#modal-housekeeper" data-toggle="modal"></i></td>
                    </tr>
                    <tr>
                        <td data-label="Room" style="border-left: 3px solid #fec107;">102</td>
                        <td data-label="Housing">BUILDING 4</td>
                        <td data-label="floor">4</td>
                        <td data-label="Room Type" >double</td>
                        <td data-label="Status">
                            <span class="label label-success"><i class="fal fa-check"></i> Clean</span>
                            <a href="#modal-status" data-toggle="modal" class="btn btn-default btn-sm"><i class="fal fa-cog"></i> Change</a>
                        </td>
                        <td data-label="Date & Time">05/02/2017 - 06:30 Pm</td>
                        <td data-label="Availability">Available</td>
                        <td data-label="Housekeepers"><a href="#modal-housekeeper" data-toggle="modal" class="btn btn-default btn-sm"><i class="fal fa-plus"></i> Add Housekeeper</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk - 2017</div>
</section>
<div class="modal fade" id="modal-housekeeper">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Housekeeper</h4>
            </div>
            <div class="modal-body">
                <div class="form-group width-300 mb-0-xs mx-auto-xs">
                    <div class="custom-input">
                        <select class="form-control">
                            <option>Select</option>
                            <option>Nuna</option>
                            <option>Jessica</option>
                        </select>
                        <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Add</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-status">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change Status</h4>
            </div>
            <div class="modal-body">
                <div class="form-group width-300 mb-0-xs mx-auto-xs">
                    <label>Current Status: <i class="fas fa-circle ml-10-xs" style="color: #fb9678"></i> Dirty</label>
                    <div class="custom-input">
                        <select class="form-control">
                            <option>Select</option>
                            <option>Dirty</option>
                            <option>Clean Up</option>
                            <option>Ready</option>
                        </select>
                        <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Update</button>
            </div>
        </div>
    </div>
</div>