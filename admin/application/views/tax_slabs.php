<section class="pageWrapper" ng-controller="taxSlabsCtrl">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-12"><strong>Tax Slabs</strong></div>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="GetTaxSlabsList()">
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Center Name</th>
                        <th>Tax Name</th>
                        <th>Tax</th>
                        <th>Apply Above</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Status</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in agentsList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Center Name">{{item.center_name}}</td>
                        <td data-label="Tax Name">{{item.tax_name}}</td>
                        <td data-label="Tax">{{item.tax_amount2}}</td>
                        <td data-label="Apply Above">{{item.tax_apply_above}}</td>
                        <td data-label="Start Date">{{item.start_date}}</td>
                        <td data-label="End Date">{{item.end_date2}}</td>
                        <td data-label="Status">{{item.tax_status}}</td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditTaxSlab(item)"><a href="#">Edit</a></li>
                                    <li ng-click="DeleteTaxSlab(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageSidebar">
        <form ng-submit="AddOrUpdateTaxSlab()" id="taxSlabForm">
            <div class="SidebarHead offset">
                {{taxSlabObj.id?'Update':'Add'}} Tax Slab
            </div>
            <div class="SidebarBody">
                <span class="text-danger" ng-bind-html="error_message"></span>
                <div class="form-group">
                    <label>Choose Center</label>
                    <select class="form-control" name="centers_id" ng-model="taxSlabObj.centers_id">
                        <option value="" selected disabled="">Choose Center</option>
                        <!--<option value="0" selected>All Centers</option>-->
                        <option value="{{item.id}}" ng-repeat="item in centersList track by $index">{{item.center_name}}</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Tax Name</label>
                    <input type="text" class="form-control" name="tax_name" ng-model="taxSlabObj.tax_name">
                </div>
                <div class="form-group">
                    <label>Tax Type</label>
                    <select class="form-control" name="tax_type" ng-model="taxSlabObj.tax_type">
                        <option value=""  >Choose Tax Type</option>
                        <option value="Percentage" selected >Percentage</option>
                        <option value="Flat Amount"  >Flat Amount</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Tax Amount</label>
                    <input type="text" class="form-control numbers_decimals_only" name="tax_amount" ng-model="taxSlabObj.tax_amount">
                </div>
                <div class="form-group">
                    <label>Tax Apply Above</label>
                    <input type="text" class="form-control numbers_decimals_only" name="tax_apply_above" ng-model="taxSlabObj.tax_apply_above">
                </div>
                <div class="form-group">
                    <label>Start Date</label>
                    <div class="custom-group">
                        <div class="custom-input">
                            <input type="text" class="form-control datepickerFrom" name="start_date" onkeydown="event.preventDefault()" ng-model="taxSlabObj.start_date" from-date>
                            <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                        </div>

                    </div>
                </div>
                <div class="form-group">
                    <label>Apply Until Changed</label>
                    <select class="form-control" name="apply_until_changed" ng-model="taxSlabObj.apply_until_changed">
                        <option value=""  >Choose Apply Until Changed</option>
                        <option value="No" selected >No</option>
                        <option value="Yes"  >Yes</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>End Date</label>
                    <div class="custom-group">
                        <div class="custom-input">
                            <input type="text" class="form-control datepickerTo" onkeydown="event.preventDefault()"  ng-disabled="taxSlabObj.apply_until_changed == 'Yes'" name="end_date" ng-model="taxSlabObj.end_date">
                            <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <label>Status</label>
                    <select class="form-control" name="tax_status" ng-model="taxSlabObj.tax_status">
                        <option value=""  >Choose Tax Status</option>
                        <option value="Active" selected >Active</option>
                        <option value="Inactive"  >Inactive</option>
                    </select>
                </div>
            </div>
            <div class="SidebarFooter offset">
                <div class="text-right">
                    <button class="btn btn-default" type="reset" ng-click="ResetForm()"><b>Cancel <i class="fal fa-times"></i></b></button>
                    <button type="submit" class="btn btn-primary"><b>{{taxSlabObj.id?'Update':'Add'}} <i class="fal fa-plus"></i></b></button>
                </div>
            </div>
        </form>
    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk 2018</div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/taxSlabsCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/taxSlabsService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/centersService.js?r=<?= time() ?>"></script>