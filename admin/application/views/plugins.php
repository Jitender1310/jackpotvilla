<section class="pageWrapper">
   <div class="pageHeader" workspace-offset  valign-parent>
      <div class="row">
         <div class="col-md-6"><strong>Plugins & Snippets</strong></div>
         <div class="col-md-6">
            <div valign-holder class="text-right"></div>
         </div>
      </div>
   </div>
   <div class="pageBody" workspace>
      <div class="container">
                     <div class="whitebox">
            <div class="whiteboxHead"><strong>Select Box</strong></div>



            <selectbox></selectbox>



      
       
       
         
         </div>
               <div class="whitebox">
            <div class="whiteboxHead"><strong>Tags Input</strong></div>



            <div class="dynamic-search">
               <input type="text"  ng-keyup="cs_keyup()" ng-model="cs_model" class="form-control">
               <div class="dynamic-search-result" ng-show="cs_display">
                  <ul>
                     <li ng-repeat="item in cs_list" ng-click="cs_click(item)">{{item.company_name}}</li>
                  </ul>                  
               </div>
            </div>
       
       
         
         </div>
         <div class="whitebox">
            <div class="whiteboxHead"><strong>Tags Input</strong></div>
            <tags-input ng-model="tags"></tags-input>
            {{tags}}
         </div>
         <div class="whitebox">
            <div class="whiteboxHead"><strong>Jquery UI Timepicker</strong></div>
            <div class="timepicker"></div>
         </div>
         <div class="whitebox">
            <div class="whiteboxHead"><strong>Jquery UI Datepicker</strong></div>
            <div class="datepicker"></div>
         </div>
         <div class="whitebox">
            <div class="whiteboxHead"><strong>Notifications</strong></div>
            <button type="button" class="btn btn-success" ng-click="noty('success','Notifications Title','Notifications Message.')">Success</button>
            <button type="button" class="btn btn-danger" ng-click="noty('danger','Notifications Title','Notifications Message.')">Danger</button>
            <button type="button" class="btn btn-info" ng-click="noty('info','Notifications Title','Notifications Message.')">Info</button>
            <button type="button" class="btn btn-warning" ng-click="noty('warning','Notifications Title','Notifications Message.')">Warning</button>
         </div>
         <div class="whitebox">
            <div class="whiteboxHead"><strong>Alerts</strong></div>
            <button type="button" class="btn btn-default" ng-click="sweety('Hi, Buddies','')">Click Here</button>
         </div>
         <div class="whitebox">
            <div class="whiteboxHead"><strong>Checkboxs Blocks</strong></div>
            <div class="checkbox">
               <label>
               <input type="checkbox" value=""><span></span>
               Checkbox
               </label>
            </div>
            <div class="checkbox">
               <label>
               <input type="checkbox" value=""><span></span>
               Checkbox
               </label>
            </div>
            <div class="checkbox">
               <label>
               <input type="checkbox" value=""><span></span>
               Checkbox
               </label>
            </div>
            <div class="checkbox">
               <label>
               <input type="checkbox" value=""><span></span>
               Checkbox
               </label>
            </div>
         </div>
         <div class="whitebox">
            <div class="whiteboxHead"><strong>Checkboxs Inline</strong></div>
            <div class="checkbox-inline">
               <label>
               <input type="checkbox" value=""><span></span>
               Checkbox
               </label>
            </div>
            <div class="checkbox-inline">
               <label>
               <input type="checkbox" value=""><span></span>
               Checkbox
               </label>
            </div>
            <div class="checkbox-inline">
               <label>
               <input type="checkbox" value=""><span></span>
               Checkbox
               </label>
            </div>
            <div class="checkbox-inline">
               <label>
               <input type="checkbox" value=""><span></span>
               Checkbox
               </label>
            </div>
         </div>
         <div class="whitebox">
            <div class="whiteboxHead"><strong>Radio Inline</strong></div>
            <div class="radio-inline">
               <label>
               <input type="radio" name="radio"> <span></span>
               Radio Box
               </label>
            </div>
            <div class="radio-inline">
               <label>
               <input type="radio" name="radio"> <span></span>
               Radio Box
               </label>
            </div>
            <div class="radio-inline">
               <label>
               <input type="radio" name="radio"> <span></span>
               Radio Box
               </label>
            </div>
            <div class="radio-inline">
               <label>
               <input type="radio" name="radio"> <span></span>
               Radio Box
               </label>
            </div>
         </div>
         <div class="whitebox">
            <div class="whiteboxHead"><strong>Radio Blocks</strong></div>
            <div class="radio">
               <label>
               <input type="radio" name="radio"> <span></span>
               Radio Box
               </label>
            </div>
            <div class="radio">
               <label>
               <input type="radio" name="radio"> <span></span>
               Radio Box
               </label>
            </div>
            <div class="radio">
               <label>
               <input type="radio" name="radio"> <span></span>
               Radio Box
               </label>
            </div>
            <div class="radio">
               <label>
               <input type="radio" name="radio"> <span></span>
               Radio Box
               </label>
            </div>
         </div>
         <div class="whitebox">
            <div class="whiteboxHead"><strong>Custom Select</strong></div>
            <div class="custom-input">
               <select class="form-control">
                  <option value="">Select</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
               </select>
               <span class="ci-icon">
               <i class="fal fa-chevron-down"></i>
               </span>
            </div>
         </div>
         <div class="whitebox">
            <div class="whiteboxHead"><strong>Text Input With Icon</strong></div>
            <div class="custom-input">
               <input type="text" name="" id="input" class="form-control" value="" required="required" pattern="" title="">
               <span class="ci-icon">
               <i class="fal fa-user"></i>
               </span>
            </div>
         </div>
         <div class="whitebox">
            <div class="whiteboxHead"><strong>Text Input With Buttons</strong></div>
            <div table="">
               <div tr="">
                  <div td="">
                     <input type="text" class="form-control" placeholder="Company">
                  </div>
                  <div td="" btn=""> <button type="button" class="btn btn-default"><i class="fal fa-plus"></i></button> </div>
                  <div td="" btn=""> <button type="button" class="btn btn-default"><i class="fal fa-search"></i></button> </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="pageFooter" workspace-offset>&copy; Freshup Desk - 2017</div>
</section>