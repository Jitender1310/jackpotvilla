<section class="pageWrapper">
    <div class="pageHeader" workspace-offset  valign-parent>
        <div class="row">
            <div class="col-md-6"><strong>Invoice</strong></div>
            <div class="col-md-6 text-right" valign-holder>
                <button type="button" id="btnPrint" class="btn btn-default btn-sm"><i class="fal fa-print"></i> <b>Print</b></button>
                <a href="<?= base_url() ?>MPDF56/view_pdf.php?url=<?= base_url() ?>reservations/invoice/pdf_view?booking_ref_number=<?= $reserverationObj->booking_ref_number ?>" 
                   class="btn btn-default btn-sm" target="_blank"><i class="fal fa-file-pdf"></i> <b>Download PDF</b></a>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace >
        <div class="whitebox width-800 mx-auto-xs" id="booking_summary_session">
            <?php $this->load->view('includes/invoice_code'); ?>
        </div>
    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk - 2017</div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>printPreview.js"></script>
<script>
    $(function () {
        $("#btnPrint").printPreview({
            obj2print: '#booking_summary_session',
            width: '1028',
            style: '<style>#downloadpdfbtn{display:none} body{overflow:inherit}</style>'
        });
    });
</script>
