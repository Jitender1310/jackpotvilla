<script src="<?= STATIC_ADMIN_JS_PATH ?>ckeditor-full/ckeditor/ckeditor.js"></script>
<section class="pageWrapper">
    <div class="pageHeader" workspace-offset >

        <div class="row">
            <div class="col-md-12"><strong>About Us Content in Website</strong></div>

        </div>
    </div>
    <div class="pageBody" workspace>
        <?php if ($this->session->flashdata('msg')) { ?>
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> <?= $this->session->flashdata('msg') ?>
            </div>
        <?php } ?>
        <form method="post" id="offer_form" enctype="multipart/form-data">
            <div class=" row">
                <div class="modal-body">
                    <!-- <form ng-submit="AddOrUpdateNotification()" id="offerNotificationsForm" ng-keyup="p_error = {}" novalidate="novalidate" class="ng-pristine ng-valid ng-valid-url"> -->
                        <div class="row">
                            <div class="col-md-12">
                                <span class="text-danger ng-binding" ng-bind-html="error_message"></span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" class="form-control " name="title" value="<?php echo empty($offer_notifications) ? "": $offer_notifications->title ?>">
                                    <!-- ngIf: p_error.title -->
                                </div>
                            </div>
                            <!-- <div class="col-md-6">
                                <div class="form-group">
                                    <label>Web Link</label>
                                    <input type="url" class="form-control ng-pristine ng-untouched ng-valid ng-empty ng-valid-url" name="title" ng-model="offerNotificationObj.web_link">
                                   
                                </div>
                            </div> -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Image (Preferred Dimensions : Width : 300px; height: 100px)</label>
                                    <input type="file" class="form-control" name="image" id="image">
                                    <!-- ngIf: p_error.image -->
                                </div>
                                
                            </div>
                        </div>
                       <!--  <button class="btn btn-primary pull-right" type="submit"><b class="ng-binding">Add <i class="fal fa-arrow-right"></i></b></button> -->
                    <!-- </form> -->
                </div>                
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>Description</label>
                    <textarea class="ckeditor form-control" name="description"><?php echo  empty($offer_notifications) ? "": $offer_notifications->description ?></textarea>  
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-success" onclick="document.getElementById('offer_form').submit();"> submit </button>
                     <a class="btn btn-success" href="<?php echo base_url("promotions/offer_notifications") ?>" <?php /* ng-click="ResetForm()" */ ?> data-dismiss="modal">Cancel </a>
                </div>
            </div>
        </form>
    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk  <?= date('Y') ?></div>
</section>