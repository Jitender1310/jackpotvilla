<section class="pageWrapper" ng-controller="bonusCtrl">
    <div class="pageHeader" workspace-offset valign-parent >
        <div class="row">
            <div class="col-md-6"><strong>Promotions > Bonus System</strong></div>
            <div class="col-md-6">
                <div valign-holder class="text-right">
                    <button type="button" class="btn btn-primary" ng-click="ShowBonusAddForm()"><i class="fal fa-plus"></i> Add Bonus</button>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Bonus Code</th>
                        <th>Bonus Type</th>
                        <th>Title</th>
                        <th>From Date</th>
                        <th>To Date</th>
                        <th>Points Percentage</th>
                        <th>Max Points</th>
                        <th>Usage Times</th>
                        <th>Credit To</th>
                        <th>Valid days</th>
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Created Date</th>
                        <th>Updated Date</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in bonusList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Bonus Code">{{item.coupon_code}}</td>
                        <td data-label="Bonus Type">{{item.bonus_type}}</td>
                        <td data-label="Title">{{item.title}}</td>
                        <td data-label="From Date">{{item.from_date_time}}</td>
                        <td data-label="To Date">{{item.to_date_time}}</td>
                        <td data-label="Points Percentage">{{item.points_percentage}}</td>
                        <td data-label="Max Points">{{item.max_points}}</td>
                        <td data-label="Usage Times">{{item.usage_times}}</td>
                        <td data-label="Credit To">{{item.credit_to}}</td>
                        <td data-label="Valid days">{{item.valid_days}}</td>
                        <td data-label="Created By">{{item.created_by_user_name}}</td>
                        <td data-label="Updated By">{{item.updated_by_user_name}}</td>
                        <td data-label="Created Date">{{item.created_at}}</td>
                        <td data-label="Updated Date">{{item.updated_at}}</td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditBonus(item)"><a href="#">Edit</a></li>
                                    <li ng-click="DeleteBonus(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageFooter" workspace-offset></div>

    <div class="modal fade" id="bonus-modal-popup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{bonusObj.id?"Update":"Add"}} Bonus</h4>
                </div>
                <div class="modal-body">
                    <form ng-submit="AddOrUpdateBonus()" id="bonusForm" ng-keyup="g_error = {}">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="text-danger" ng-bind-html="error_message"></span>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Title<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" required name="title" ng-model="bonusObj.title">
                                </div>
                                <label class="error" ng-if="b_error.title">{{b_error.title}}</label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Bonus Type <span class="text-danger">*</span></label>
                                    <div class="custom-input">
                                        <select class="form-control" name="bonus_type" required ng-model="bonusObj.bonus_type">
                                            <option value="" selected disabled="">Choose</option>
                                            <option value="Deposit Bonus">Deposit Bonus</option>
                                            <option value="Occasional Bonus">Occasional Bonus</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i class="fal fa-chevron-down"></i>
                                        </span>
                                    </div>
                                </div>
                                <label class="error" ng-if="b_error.bonus_type">{{b_error.bonus_type}}</label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Bonus Code<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" style="text-transform: uppercase"  required name="coupon_code" ng-model="bonusObj.coupon_code">
                                </div>
                                <label class="error" ng-if="b_error.coupon_code">{{b_error.coupon_code}}</label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Credit To<span class="text-danger">*</span></label>
                                    <div class="custom-input">
                                        <select class="form-control" name="credit_to" required ng-model="bonusObj.credit_to">
                                            <option value="" selected disabled="">Choose</option>
                                            <option>Bonus Wallet</option>
                                            <option>Deposit Wallet</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i class="fal fa-chevron-down"></i>
                                        </span>
                                    </div>
                                </div>
                                <label class="error" ng-if="b_error.credit_to">{{b_error.credit_to}}</label>
                            </div>
                            <div class="col-md-3" ng-show="bonusObj.credit_to=='Bonus Wallet'">
                                <div class="form-group">
                                    <label>Valid Days<span class="text-danger">*</span></label>
                                    <input type="number" min="0" class="form-control" style="text-transform: uppercase"  required name="valid_days" ng-model="bonusObj.valid_days">
                                </div>
                                <label class="error" ng-if="b_error.valid_days">{{b_error.valid_days}}</label>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>
                                        <span ng-show="bonusObj.credit_to=='Bonus Wallet'">Points</span>
                                        <span ng-show="bonusObj.credit_to=='Deposit Wallet'">Credit</span>
                                        Percentage<span class="text-danger">*</span></label>
                                    <input type="number" class="form-control" required name="points_percentage" min="1" max="100" step="1" ng-model="bonusObj.points_percentage">
                                </div>
                                <label class="error" ng-if="b_error.points_percentage">{{b_error.points_percentage}}</label>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Max Limit<span class="text-danger">*</span></label>
                                    <input type="number" class="form-control" required name="max_points" min="0"  step="1"  ng-model="bonusObj.max_points">
                                </div>
                                <label class="error" ng-if="b_error.max_points">{{b_error.max_points}}</label>
                            </div>
                            
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Usage Times<span class="text-danger">*</span></label>
                                    <input type="number" class="form-control" required name="usage_times" min="0"  step="1"  ng-model="bonusObj.usage_times">
                                </div>
                                <label class="error" ng-if="b_error.usage_times">{{b_error.usage_times}}</label>
                            </div>
                            
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>From Date<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control datetimepickerAll" required name="from_date_time" ng-model="bonusObj.from_date_time">
                                </div>
                                <label class="error" ng-if="b_error.from_date_time">{{b_error.from_date_time}}</label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>To Date<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control datetimepickerAll" required name="to_date_time" ng-model="bonusObj.to_date_time">
                                </div>
                                <label class="error" ng-if="b_error.to_date_time">{{b_error.to_date_time}}</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description <span class="text-danger">*</span></label>
                                    <textarea type="text" class="form-control" rows="5" required name="description" ng-model="bonusObj.description"></textarea>
                                </div>
                                <label class="error" ng-if="b_error.description">{{b_error.description}}</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Terms And Conditions <span class="text-danger">*</span></label>
                                    <textarea type="text" class="form-control" rows="5" required name="terms_conditions" ng-model="bonusObj.terms_conditions"></textarea>
                                </div>
                                <label class="error" ng-if="b_error.terms_conditions">{{b_error.terms_conditions}}</label>
                            </div>
                        </div>
                        
                        
                        <div class="row">
                            <div class="col-md-12 mb-10-lg">
                                Note: 
                                <ul>
                                    <li>Max Limit Zero means unlimited there is no limit for max upto bonus points</li>
                                    <li>Usage times Zero means unlimited there is no limit for coupon usage</li>
                                </ul>
                            </div>
                        </div>

                        <button class="btn btn-default " ng-click="ResetForm()" data-dismiss="modal"><b>Cancel <i class="fal fa-times"></i></b></button>
                        <button class="btn btn-primary pull-right" type="submit"><b>{{bonusObj.id?"Update":"Add"}} <i class="fal fa-arrow-right"></i></b></button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/promotions/bonusCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/promotions/bonusService.js?r=<?= time() ?>"></script>