<section class="pageWrapper" ng-controller="offerNotificationsCtrl">
    <div class="pageHeader" workspace-offset valign-parent >
        <div class="row">
            <div class="col-md-6"><strong>Offer Notifications</strong></div>
            <div class="col-md-6">
                <div valign-holder class="text-right">
                    <?php /* <button type="button" class="btn btn-primary" ng-click="ShowOfferNotificationAddForm()"><i class="fal fa-plus"></i> Add Notification</button> */ ?>
                    <a href="<?php echo base_url("promotions/offer_notifications/add") ?>" class="btn btn-primary"><i class="fal fa-plus"></i> Add Notification</a>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="GetOfferNotifications();" style="display: none;">
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Created Time</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in offerNotificationsList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Title">{{item.title}}</td>
                        <td data-label="Image">
                            <a target="_blank" href="{{item.image}}">
                                <img src="{{item.image}}" style="width:100px;height: auto"/>
                            </a>
                        </td>
                        <td data-label="Registered Time">{{item.created_date_time}}</td>
                        <td data-label="Actions" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="DeleteOfferNotifications(item)"><a href="#">Delete</a></li>
                                    <li><a href="<?php echo base_url("promotions/offer_notifications/add/{{item.id}}") ?>">Edit</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="GetOfferNotifications();">
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Created Time</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in offerNotificationsList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Title">{{item.title}}</td>
                        <td data-label="Image">
                            <a target="_blank" href="{{item.image}}">
                                <img src="{{item.image}}" style="width:100px;height: auto"/>
                            </a>
                        </td>
                        <td data-label="Registered Time">{{item.created_date_time}}</td>
                        <td data-label="Actions" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="DeleteOfferNotifications(item)"><a href="#">Delete</a></li>
                                    <li><a href="<?php echo base_url("promotions/offer_notifications/add/{{item.id}}") ?>">Edit</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageFooter" workspace-offset></div>

    <div class="modal fade" id="offer-notification-modal-popup">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{offerNotificationObj.id?"Update":"Add"}} Notification</h4>
                </div>
                <div class="modal-body">
                    <form ng-submit="AddOrUpdateNotification()" id="offerNotificationsForm" ng-keyup="p_error = {}">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="text-danger" ng-bind-html="error_message"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" class="form-control" name="title" ng-model="offerNotificationObj.title">
                                    <label class="error" ng-if="p_error.title">{{p_error.title}}</label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Web Link</label>
                                    <input type="url" class="form-control" name="title" ng-model="offerNotificationObj.web_link">
                                    <label class="error" ng-if="p_error.web_link">{{p_error.web_link}}</label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Image</label>
                                    <input type="file" class="form-control" name="image" id="image">
                                    <label class="error" ng-if="p_error.image">{{p_error.image}}</label>
                                </div>
                                <p>Preferred Dimensions : Width : 300px; height: 100px</p>
                            </div>
                        </div>

                        <br/>
                        <br/>

                        <button class="btn btn-default " ng-click="ResetForm()" data-dismiss="modal"><b>Cancel <i class="fal fa-times"></i></b></button>
                        <button class="btn btn-primary pull-right" type="submit"><b>{{offerNotificationObj.id?"Update":"Add"}} <i class="fal fa-arrow-right"></i></b></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/promotions/offerNotificationsCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/promotions/offerNotificationsService.js?r=<?= time() ?>"></script>