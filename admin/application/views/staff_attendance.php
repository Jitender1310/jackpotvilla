<section class="pageWrapper">
    <div class="pageHeader" workspace-offset  valign-parent>
        <div class="row">
            <div class="col-md-6"><strong>Freshup Staff attendance</strong></div>
            <div class="col-md-6 text-right">
                <div valign-holder>





                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>

        <div class="whitebox mb-0-xs">



            <div class="fit-parent">
                <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered mb-0-xs table-cell-width fit-child staff-attendance-table">


                    <tr class="bg-default font-bold">

                        <td class="fixed-col" style="vertical-align: bottom; width: 150px">Employee Name</td>
                        <td class="fixed-col" style="vertical-align: bottom; width: 100px">Designation</td>

                        <td class="adjustable-col">
                            <div>01 sep Fri</div>
                        </td>
                        <td class="adjustable-col">
                            <div>02 sep Sat</div>
                        </td>
                        <td class="adjustable-col">
                            <div>03 sep Sun</div>
                        </td>
                        <td class="adjustable-col">
                            <div>04 sep Mon</div>
                        </td>
                        <td class="adjustable-col">
                            <div>05 sep Tue</div>
                        </td>
                        <td class="adjustable-col">
                            <div>06 sep Wed</div>
                        </td>
                        <td class="adjustable-col">
                            <div>07 sep Thu</div>
                        </td>
                        <td class="adjustable-col">
                            <div>08 sep Fri</div>
                        </td>
                        <td class="adjustable-col">
                            <div>09 sep Sat</div>
                        </td>
                        <td class="adjustable-col">
                            <div>10 sep Sun</div>
                        </td>
                        <td class="adjustable-col">
                            <div>11 sep Mon</div>
                        </td>
                        <td class="adjustable-col">
                            <div>12 sep Tue</div>
                        </td>
                        <td class="adjustable-col">
                            <div>13 sep Wed</div>
                        </td>
                        <td class="adjustable-col">
                            <div>14 sep Thu</div>
                        </td>
                        <td class="adjustable-col">
                            <div>15 sep Fri</div>
                        </td>
                        <td class="adjustable-col">
                            <div>16 sep Sat</div>
                        </td>
                        <td class="adjustable-col">
                            <div>17 sep Sun</div>
                        </td>
                        <td class="adjustable-col">
                            <div>18 sep Mon</div>
                        </td>
                        <td class="adjustable-col">
                            <div>19 sep Tue</div>
                        </td>
                        <td class="adjustable-col">
                            <div>20 sep Thu</div>
                        </td>
                        <td class="adjustable-col">
                            <div>21 sep Fri</div>
                        </td>
                        <td class="adjustable-col">
                            <div>22 sep Sat</div>
                        </td>
                        <td class="adjustable-col">
                            <div>23 sep Sun</div>
                        </td>
                        <td class="adjustable-col">
                            <div>24 sep Mon</div>
                        </td>
                        <td class="adjustable-col">
                            <div>25 sep Tue</div>
                        </td>
                        <td class="adjustable-col">
                            <div>26 sep Wed</div>
                        </td>
                        <td class="adjustable-col">
                            <div>27 sep Thu</div>
                        </td>
                        <td class="adjustable-col">
                            <div>28 sep Fri</div>
                        </td>
                        <td class="adjustable-col">
                            <div>29 sep Sat</div>
                        </td>
                        <td class="adjustable-col">
                            <div>30 sep Sun</div>
                        </td>

                    </tr>
                    <tr>

                        <td>S.DEVENDRA</td>
                        <td>FOM</td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="absent"><span>A</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="absent"><span>A</span></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                        <td>D.PRASAD</td>
                        <td>H/K SUP</td>
                        <td class="absent"><span>A</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                        <td>T.YESWANTH SAI</td>
                        <td>Marketing</td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="absent"><span>A</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="absent"><span>A</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                        <td>P.SUDARSHANAM</td>
                        <td>H/K SUP</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                        <td>D.PRASAD</td>
                        <td>House Man</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                        <td>K.VENKATESH</td>
                        <td>House Man</td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                        <td>M.VENKATESH</td>
                        <td>House Man</td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="absent"><span>A</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                        <td>B.SUBBALAKSHMI</td>
                        <td>House Maid</td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="absent"><span>A</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="present"><span>P</span></td>
                        <td class="absent"><span>A</span></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>



                    </tbody></table></div>














        </div>

    </div>

    <?php $this->load->view('admin/includes/reports_menu'); ?>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk - 2017</div>
</section>