<section class="pageWrapper pageSidebarSmall" ng-controller="reservationsCtrl" ng-init="GetCentersList('<?= get_user_id() ?>');filter.agents_id = <?= $this->input->get_post('agents_id') ? $this->input->get_post('agents_id') : '0' ?>;filter.booking_status = '<?= $this->input->get_post('selected_type') ? $this->input->get_post('selected_type') : '' ?>';">
    <div class="pageHeader" workspace-offset valign-parent>
        <?php
        if ($this->input->get_post('agents_id') != 0) {
            ?>
            <div class="row">
                <div class="col-md-3">
                    <strong class="text-left"> Agent Name : <?= $agent_details->agent_name ?></strong>
                </div>
                <div class="col-md-3">
                    <strong class="text-left"> Mobile Number : <?= $agent_details->mobile ?></strong>
                </div>
                <div class="col-md-3">
                    <strong class="text-left"> Agent Code: <?= $agent_details->unique_id ?></strong>
                </div>
                <div class="col-md-3">
                    <strong class="text-right">Current Wallet Balance <i class="fa fa-rupee-sign"></i> <?= $wallet_ballance ?> /-</strong>
                </div>
            </div>
            <?php
        } else {
            ?>
            <div class="row">
                <div class="col-md-10">
                    <strong ng-if="filter.booking_status === 'completed_but_proofs_updation_pending'">Pending Reservation awaiting for Guest Proofs updating</strong>
                    <strong ng-if="filter.booking_status != 'completed_but_proofs_updation_pending'">{{selected_menut_title}}</strong>
                </div>
                <div class="col-md-2">
                    <div class="custom-input" title="Sory By" valign-holder>
                        <select class="form-control input-sm" ng-model="filter.sort_by" ng-change="GetReservationsList()">
                            <option value="" selected>Sort by</option>
                            <option value="Latest">Latest First</option>
                            <option value="Oldest">Oldest First</option>
                        </select>
                        <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>

    </div>
    <div class="pageBody" workspace ng-init="GetReservationsList()">

        <div class="whitebox">
            <div class="row rm-5">
                <form class="" ng-submit="GetReservationsList(filter.page = 1)">
                    <div class="col-md-4 cp-5">
                        <div class="form-group mb-0-xs">
                            <input type="text" class="form-control"
                                   placeholder="Enter Booking ID, mobile number, Contact Name, Email"
                                   name="ref_id" title="Enter Booking ID or mobile number"
                                   ng-model="filter.search_key" >
                        </div>

                    </div>
                    <div class="col-md-2 cp-5">
                        <div class="custom-group">
                            <div class="custom-input">
                                <input type="text" class="form-control datepickerNormal" placeholder="From Date" name="from_date" onkeydown="event.preventDefault()" ng-model="filter.from_date">
                                <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 cp-5">
                        <div class="custom-group">
                            <div class="custom-input">
                                <input type="text" class="form-control datepickerNormal" placeholder="To Date" name="to_date" onkeydown="event.preventDefault()" ng-model="filter.to_date">
                                <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 cp-5">
                        <div class="form-group mb-0-xs"> 
                            <div class="custom-input" title="Centers">
                                <select class="form-control" ng-model="filter.centers_id" ng-disabled="!centersList.length > 0">
                                    <option value="" selected>All Centers</option>
                                    <option ng-value="item.id" value="item.id" ng-repeat="item in centersList">{{item.center_name}}</option>
                                </select>
                                <span class="ci-icon">
                                    <i ng-show="centersSpinner" class="fal fa-circle-notch fa-spin"></i>
                                    <i ng-show="!centersSpinner" class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 cp-5">
                        <button type="submit" class="btn btn-primary "><i class="fas fa-search"></i></button>
                        <button type="reset" ng-click="ResetFilter()" class="btn btn-danger " title="Clear Search"><i class="fas fa-times-circle"></i></button>
                        <button type="button" ng-show="reservationsList.length > 0" class="btn btn-info" ng-click="ExportToExcel()" title="Export To Excel"><i class="far fa-download"></i> <i class="far fa-file-excel"></i></button>
                    </div>
                </form>
            </div>

        </div>

        <div class="responsive-table" ng-hide="reservationsList.length > 0">
            <div class="alert alert-danger">
                No Data found
            </div>
        </div> 

        <div class="responsive-table" ng-show="reservationsList.length > 0">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="30" class="no-sort">#</th>
                        <th class="no-sort">Booking ID</th>
                        <th class="no-sort">Contact Info</th>
                 <!--        <th class="no-sort">Mobile </th>
                        <th class="no-sort">Email</th> -->
                        <th class="no-sort">Duration</th>
                        <th class="no-sort">Check-in Date Time</th>
                        <th class="no-sort">Booking Status</th>
                        <th class="no-sort">Payment Status</th>
                        <th class="no-sort">Amount</th>
                        <th class="no-sort">Booked From</th>
                        <th class="no-sort">Booked By</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in reservationsList track by $index">
                        <td data-label="S.No">
                            {{($index + reservationsPagination.initial_id)}}
                        </td>
                        <td data-label="Ref ID">
                            <a class="text-primary"
                               ng-if="item.created_with != 'Quick'"
                               ng-href="<?= base_url() ?>reservations/view?booking_id={{item.booking_ref_number}}" 
                               target="_blank"><strong>{{item.booking_ref_number}}</strong></a>
                            <a class="text-primary"
                               ng-if="item.created_with == 'Quick'"
                               ng-href="<?= base_url() ?>reservations/quick_view?booking_id={{item.booking_ref_number}}" 
                               target="_blank"><strong>{{item.booking_ref_number}}</strong></a>
                        </td>
                        <td data-label="Contact Name" class="contact_info_td">
                            <div><strong>{{item.contact_name}}</strong> 
                                <a class="far fa-plus-circle text-primary" data-toggle="collapse" aria-expanded="false" href="#{{$index}}"></a>
                            </div>
                            <div class="collapse" id="{{$index}}">
                                <div><i class="fal fa-phone"></i> {{item.contact_mobile}}</div>
                                <div><i class="fal fa-envelope"></i> {{item.contact_email}}</div>
                            </div>
                        </td>
              <!--               <td data-label="Mobile No">{{item.contact_mobile}}</td>
                            <td data-label="Email">{{item.contact_email}}</td> -->
                        <td data-label="Duration">{{item.duration}} {{item.duration_type}}</td>
                        <td data-label="Check-in Date Time">{{item.check_in_date_time}}</td>
                        <td data-label="Booking Status" class="text-{{item.booking_status_bootstrap_class}}" title="{{item.remark}}">{{item.booking_status}}</td>
                        <td data-label="Payment Status" class="text-{{item.payment_status_bootstrap_class}}">
                            {{item.payment_status}}
                            <span ng-if="item.payment_status == 'Refund'">Rs.{{item.refunded_amount}}</span>
                        </td>
                        <td data-label="Amount">{{item.grand_total}}</td>
                        <td data-label="Booked by">{{item.creation_source}}</td>
                        <td  data-label="Booked by">{{item.booked_by_users_name}}</td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <!--<li ng-click="EditAgent(item)"><a href="#">Edit</a></li>-->
                                    <li ng-if="item.created_with != 'Quick'"><a ng-href="<?= base_url() ?>reservations/view?booking_id={{item.booking_ref_number}}" target="_blank">View Reservation</a></li>
                                    <li ng-if="item.created_with == 'Quick'"><a ng-href="<?= base_url() ?>reservations/quick_view?booking_id={{item.booking_ref_number}}" target="_blank">View Reservation</a></li>
                                    <li><a ng-href="<?= base_url() ?>reservations/invoice?booking_ref_number={{item.booking_ref_number}}" target="_blank" ng-show="item.booking_status === 'Completed'">View Invoice</a></li>
                                    <!--<li ng-click="DeleteAgent(item)"><a href="#">Delete</a></li>-->
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <span class="pull-right" ng-bind-html="reservationsPagination.pagination"></span>
    </div>
    <?php $this->load->view('includes/reservations_menu'); ?>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk 2018</div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/reservationsCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/reservationViewService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/centersService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/howDidHearAboutUsService.js?r=<?= time() ?>"></script>