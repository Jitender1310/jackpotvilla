<section class="pageWrapper pageSidebar" ng-controller="reservationsCtrl" ng-init="GetCentersList('<?= get_user_id() ?>'); filter.agents_id = <?= $this->input->get_post('agents_id') ? $this->input->get_post('agents_id') : '0' ?>; filter.booking_status = '<?= $this->input->get_post('selected_type') ? $this->input->get_post('selected_type') : '' ?>';">
    <div class="pageHeader" workspace-offset valign-parent>

        <div class="row">
            <div class="col-md-10">
                <strong ng-if="filter.booking_status === 'completed_but_proofs_updation_pending'">Pending Reservation awaiting for Guest Proofs updating</strong>
                <strong ng-if="filter.booking_status != 'completed_but_proofs_updation_pending'">{{selected_menut_title}}</strong>
            </div>
            <div class="col-md-2">
                <div class="custom-input" title="Sory By" valign-holder>
                    <select class="form-control input-sm" ng-model="filter.sort_by" ng-change="GetReservationsList()">
                        <option value="" selected>Sort by</option>
                        <option value="Latest">Latest First</option>
                        <option value="Oldest">Oldest First</option>
                    </select>
                    <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="GetReservationsList()">
        <div class="whitebox">
            <div class="row rm-5">
                <form class="" ng-submit="GetReservationsList(filter.page = 1)">
                    <div class="row rm-5">
                        <div class="col-md-3 cp-5">
                            <div class="form-group mb-0-xs">
                                <input type="text" class="form-control"
                                       placeholder="Booking ID, mobile number, Contact Name, Email"
                                       name="ref_id" title="Enter Booking ID or mobile number"
                                       ng-model="filter.search_key" >
                            </div>
                        </div>
                        <div class="col-md-2 cp-5">
                            <div class="custom-group">
                                <div class="custom-input">
                                    <input type="text" class="form-control datepickerNormal" placeholder="From Date" name="from_date" onkeydown="event.preventDefault()" ng-model="filter.from_date">
                                    <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 cp-5">
                            <div class="custom-group">
                                <div class="custom-input">
                                    <input type="text" class="form-control datepickerNormal" placeholder="To Date" name="to_date" onkeydown="event.preventDefault()" ng-model="filter.to_date">
                                    <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 cp-5">
                            <div class="form-group mb-0-xs"> 
                                <div class="custom-input" title="Centers">
                                    <select class="form-control" ng-model="filter.centers_id" ng-disabled="!centersList.length > 0">
                                        <option value="" selected>All Centers</option>
                                        <option ng-value="item.id" value="item.id" ng-repeat="item in centersList">{{item.center_name}}</option>
                                    </select>
                                    <span class="ci-icon">
                                        <i ng-show="centersSpinner" class="fal fa-circle-notch fa-spin"></i>
                                        <i ng-show="!centersSpinner" class="fal fa-chevron-down"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 cp-5">
                            <div class="form-group mb-0-xs"> 
                                <div class="custom-input" title="Filter Type" ng-init="filter.booking_status = 'all'">
                                    <select class="form-control" ng-model="filter.booking_status" ng-disabled="!centersList.length > 0">
                                        <option value="all">All Reservations</option>
                                        <option value="confirmed">Confirmed</option>
                                        <option value="today_checkin">Today Check In List</option>
                                        <option value="today_checkout">Today Check Out List</option>
                                        <option value="inhouse_customers">Inhouse Customers</option>
                                        <option value="completed">Completed Bookings</option>
                                        <option value="cancelled">Cancelled Bookings</option>
                                        <option value="unconfirmed">Un Confirmed Bookings</option>
                                        <option value="missed_stay">Missed Stay</option>
                                    </select>
                                    <span class="ci-icon">
                                        <i class="fal fa-chevron-down"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row rm-5">
                        <div class="col-md-2 cp-5">
                            <div class="form-group mb-0-xs"> 
                                <div class="custom-input" title="Filter Type" ng-init="filter.how_did_hear_about_us_id = 'all'">
                                    <select class="form-control" ng-model="filter.how_did_hear_about_us_id" ng-disabled="!centersList.length > 0">
                                        <option value="all">All Sources</option>
                                        <option ng-value="item.id" value="item.id" ng-repeat="item in referencesList">{{item.title}}</option>
                                    </select>
                                    <span class="ci-icon">
                                        <i class="fal fa-chevron-down"></i>
                                    </span>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-1 cp-5">
                            <button type="submit" class="btn btn-primary "><i class="fas fa-search"></i></button>
                            <button type="reset" ng-click="ResetFilter()" class="btn btn-danger " title="Clear Search"><i class="fas fa-times-circle"></i></button>
                        </div>
                    </div>
                </form>
            </div>

        </div>

        <div class="responsive-table" ng-hide="reservationsList.length > 0">
            <div class="alert alert-danger">
                No Data found
            </div>
        </div> 

        <div class="responsive-table" ng-show="reservationsList.length > 0">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="30" class="no-sort">#</th>
                        <th class="no-sort">Booking ID</th>
                        <th class="no-sort">Contact Info</th>
                 <!--        <th class="no-sort">Mobile </th>
                        <th class="no-sort">Email</th> -->
                        <th class="no-sort">Duration</th>
                        <th class="no-sort">Check-in Date Time <br/> Booking Status</th>
                        <th class="no-sort">Amount <br/> Payment Status</th>
                        <th class="no-sort">Booked By/From</th>
                        <th class="no-sort">Soruce</th>
                        <th class="no-sort">Feedback Comment</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in reservationsList track by $index">
                        <td data-label="S.No">
                            {{($index + reservationsPagination.initial_id)}}
                        </td>
                        <td data-label="Ref ID">
                            <a><strong>{{item.booking_ref_number}}</strong></a>
                        </td>
                        <td data-label="Contact Name" class="contact_info_td">
                            <div><strong>{{item.contact_name}}</strong> 
                                <a class="far fa-plus-circle text-primary" data-toggle="collapse" aria-expanded="false" href="#{{$index}}"></a>
                            </div>
                            <div class="collapse" id="{{$index}}">
                                <div><i class="fal fa-phone"></i> {{item.contact_mobile}}</div>
                                <div><i class="fal fa-envelope"></i> {{item.contact_email}}</div>
                            </div>
                        </td>
              <!--               <td data-label="Mobile No">{{item.contact_mobile}}</td>
                            <td data-label="Email">{{item.contact_email}}</td> -->
                        <td data-label="Duration">{{item.duration}} {{item.duration_type}}</td>
                        <td data-label="Check-in Date Time & Booking Status">{{item.check_in_date_time}}
                            <br/>
                            <span title="{{item.remark}}" class="text-{{item.booking_status_bootstrap_class}}">{{item.booking_status}}</span>
                        </td>
                        <td data-label="Amount/Payment Status">{{item.grand_total|currency:''}}<br/>
                            <span class="text-{{item.payment_status_bootstrap_class}}">{{item.payment_status}}</span>
                        </td>
                        <td data-label="Booked from/by">{{item.booked_by_users_name}}<br/>
                            {{item.creation_source}}</td>
                        <td data-label="Source">{{item.how_did_hear_about_us_text!=''?item.how_did_hear_about_us_text:'N/A'}}</td>
                        <td data-label="Feedback Comment">
                            <p ng-if="item.telecaller_feedback_has_provided == 'yes'"> {{item.telecaller_feedback}}</p>
                            <p ng-if="item.telecaller_feedback_has_provided == 'yes'" class="text-right">--Updated by {{item.telecaller_name}}</p>
                            <textarea ng-if="item.telecaller_feedback_has_provided == 'no'" class="form-control" style="margin-bottom: 3px" ng-model="item.telecaller_feedback"></textarea>
                            <button class="btn btn-default btn-xs" ng-if="item.telecaller_feedback_has_provided == 'no'" type="button" ng-click="UpdateComment(item, $index)">Update comment</button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <span class="pull-right" ng-bind-html="reservationsPagination.pagination"></span>
    </div>
    <?php $this->load->view('includes/reservations_menu'); ?>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk 2018</div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/reservationsCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/reservationViewService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/centersService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/howDidHearAboutUsService.js?r=<?= time() ?>"></script>