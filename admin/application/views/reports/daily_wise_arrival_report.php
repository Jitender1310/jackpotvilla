<section class="pageWrapper" ng-controller="dailyWiseArrivalReportCtrl">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-12"><strong>Daily wise arrival report</strong></div>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="GetDailyWiseArrivalReportList(); GetCentersList('<?= get_user_id() ?>');">


        <div class="whitebox ">
            <div class="row">
                <div class="col-md-12">
                    <form class="" ng-submit="GetDailyWiseArrivalReportList(filter.page = 1)">


                        <div class="col-md-2 cp-5" >
                            <div class="custom-group">
                                <div class="custom-input">
                                    <input type="text" class="form-control datepickerNormal" placeholder="From Date" name="from_date" onkeydown="event.preventDefault()" ng-model="filter.from_date" >
                                    <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 cp-5" >
                            <div class="custom-group">
                                <div class="custom-input">
                                    <input type="text" class="form-control datepickerNormal" placeholder="To Date" name="to_date" onkeydown="event.preventDefault()" ng-model="filter.to_date" >
                                    <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="custom-input" title="My Center">
                                <select class="form-control" ng-model="filter.centers_id" ng-disabled="!centersList.length > 0">
                                    <option value="" selected >All Centers</option>
                                    <option ng-value="item.id" value="item.id" ng-repeat="item in centersList">{{item.center_name}}</option>
                                </select>
                                <span class="ci-icon">
                                    <i ng-show="centersSpinner" class="fal fa-circle-notch fa-spin"></i>
                                    <i ng-show="!centersSpinner" class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>


                        <div class="col-md-2">
                            <button type="submit" class="btn btn-default">Search</button>
                            <button type="" ng-click="ResetFilter()" class="btn btn-danger " title="Clear Search"><i class="fas fa-times-circle"></i></button>
                            <button ng-show="filter.from_date != '' || filter.to_date != '' || filter.payment_method != '' || filter.centers_id != ''" type="button" class="btn btn-info hide" ng-click="ExportToExcel()" title="Export To Excel"><i class="far fa-download"></i> <i class="far fa-file-excel"></i></button>

                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div>
            <div class="responsive-table" ng-hide="dailyWiseArrivalReportList.length > 0">
                <div class="alert alert-danger">
                    No Data found
                </div>
            </div> 
            <style>
                .table td {
                    border: 1px solid #ddd;
                    padding: 8px;
                }
            </style>
            <div class="responsive-table" ng-if="dailyWiseArrivalReportList.length > 0" >

                <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                    <thead>
                        <tr >
                            <td rowspan="2" align="center" style="vertical-align: middle;"><b>Date</b></td>
                            <td rowspan="2" align="center" style="vertical-align: middle;"><b>S.No</b><span>&nbsp;</span></td>
                            <td rowspan="2" align="center"style="vertical-align: middle;"><b>Booking ID</b></td>
<!--                            <td rowspan="2" align="center"style="vertical-align: middle;"><b>Guest Name</b></td>
                            <td rowspan="2" align="center" style="vertical-align: middle;"><b>Mobile Number</b></td>
                            <td rowspan="2" align="center" style="vertical-align: middle;"><b>Email</b></td>
                            <td rowspan="2" align="center" style="vertical-align: middle;"><b>Nativity</b></td>-->
                            <td rowspan="2" align="center" style="vertical-align: middle;"><b>Center</b></td>
                            <td rowspan="2" align="center" style="vertical-align: middle;"><b>Duration</b></td>
                            <td rowspan="2" align="center" style="vertical-align: middle;"><b>Room Type</b></td>
                            <td rowspan="2" align="center" style="vertical-align: middle;"><b>No Of Pax</b></td>
                            <td rowspan="2" align="center" style="vertical-align: middle;"><b>Tariff</b></td>
                            <td colspan="4" align="center" style="vertical-align: middle;"><b>Paid by</b></td>
                            <td rowspan="2" align="center" style="vertical-align: middle;"><b>Grand Sale</b></td>
                        </tr>
                        <tr>
                            <td align="center"><b>Cash</b></td>
                            <td align="center"><b>Cards</b></td>
                            <td align="center"><b>Online</b></td>
                            <td align="center"><b>Others</b></td>
                        </tr>
                    </thead>
                    <tbody ng-repeat="item in dailyWiseArrivalReportList">
                        <tr >
                            <td data-label="Date" rowspan="{{item.reservations.length + 1}}">
                                {{item.date}}
                            </td>
                            <td ng-show="item.reservations.length === 0" data-label="Date" colspan="15" class="text-center" >
                                No Reservations Found On This Date
                            </td>
                        </tr>

                        <tr ng-repeat="item in item.reservations track by $index" ng-hide="item.reservations.length === 0">
                            <td data-label="S.No">
                                {{$index + 1}}
                            </td>
                            <td data-label="Ref ID">
                                <a class="text-primary"  ng-href="<?= base_url() ?>reservations/quick_view?booking_id={{item.booking_ref_number}}" target="_blank"><strong>{{item.booking_ref_number}}</strong></a>
                            </td>
<!--                            <td data-label="Name"class="text-left">{{item.contact_name}}</td>
                            <td data-label="Mobile"class="text-left">{{item.contact_mobile}}</td>
                            <td data-label="Email"class="text-left">{{item.contact_email}}</td>
                            <td data-label="Nativity"class="text-left">{{item.nativity}}</td>-->
                            <td data-label="Center"class="text-left">{{item.center_name}}</td>
                            <td data-label="Duration"class="text-right">{{item.duration}} hrs</td>
                            <td data-label="Room"class="text-left">{{item.rooms}}</td>
                            <td data-label="Pax"class="text-left">{{item.pax}}</td>
                            <td data-label="Grand totla"class="text-right">{{item.grand_total}}</td>
                            <td data-label="Cash"class="text-right">{{item.cash}}</td>
                            <td data-label="Cards"class="text-right">{{item.cards}}</td>
                            <td data-label="Online"class="text-right">{{item.online}}</td>
                            <td data-label="Other"class="text-right">{{item.other}}</td>
                            <td data-label="Grand"class="text-right">{{item.grand}}</td>
                        </tr>
                        <tr ng-show="item.reservations.length > 0" style="color:red !important">
                            <td data-label="Grand Totals" colspan="6" class="text-right" >Grand Total</td>
                            <td data-label="Total Pax"  >{{item.total_pax}}</td>
                            <td data-label="Total Grand Total" class="text-right">{{item.net_grand_total}}</td>
                            <td data-label="Total Cash Sale" class="text-right">{{item.total_cash}}</td>
                            <td data-label="Total Cards Sale" class="text-right">{{item.total_cards}}</td>
                            <td data-label="Total Online Sale" class="text-right">{{item.total_online}}</td>
                            <td data-label="Grand Other Payment Types Sale" class="text-right">{{item.total_other}}</td>
                            <td data-label="Grand Total Sale" class="text-right">{{item.total_grand}}</td>
                        </tr>
                        <tr style="background-color: #f3f2f1">
                            <td colspan="17"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <span  class="pull-right" ng-bind-html="dailyWiseArrivalReportPaginantion.pagination"></span>
    </div>

    <div class="pageFooter" workspace-offset>&copy; Freshup Desk <?= date('Y') ?></div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/reports/dailyWiseArrivalReportCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/reports/dailyWiseArrivalReportService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/centersService.js?r=<?= time() ?>"></script>