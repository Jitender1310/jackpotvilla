<section class="pageWrapper" ng-controller="occupancyCtrl">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-12"><strong>Occupancy report</strong></div>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="GetCategoryWiseDurationBookingReport(); GetCentersList('<?= get_user_id() ?>');">
        <div class="whitebox">
            <div class="row">
                <div class="col-md-12">
                    <form class="" ng-submit="GetCategoryWiseDurationBookingReport()">
                        <div class="col-md-2">
                            <div class="custom-input" title="My Center">
                                <select class="form-control" ng-model="filter.centers_id" ng-disabled="!centersList.length > 0" ng-change="GetStayingCategoriesListForCenter(filter.centers_id, 2)">
                                    <option value="" selected >All Centers</option>
                                    <option ng-value="item.id" value="item.id" ng-repeat="item in centersList">{{item.center_name}}</option>
                                </select>
                                <span class="ci-icon">
                                    <i ng-show="centersSpinner" class="fal fa-circle-notch fa-spin"></i>
                                    <i ng-show="!centersSpinner" class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-2 pl-10-xs">
                            <div class="custom-input">
                                <select class="form-control input-sm"  ng-model="filter.staying_type_categories_id" ng-disabled="!stayingCategoriesList.length > 0">
                                    <option value="">All Types</option>
                                    <option value="{{item.id}}" ng-repeat="item in stayingCategoriesList track by $index">{{item.category_name}}</option>
                                </select>
                                <span class="ci-icon">
                                    <i ng-show="!stayingTypesSpinner" class="fal fa-chevron-down"></i>
                                    <i ng-show="stayingTypesSpinner" class="fal fa-circle-notch fa-spin"></i>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-2 pl-10-xs">
                            <div class="custom-input">
                                <select class="form-control input-sm"  ng-model="filter.interval_type">
                                    <option value="">All Time</option>
                                    <!--<option value="1 HOUR">Past Hour</option>-->
                                    <option value="1 DAY">Past 24 hours</option>
                                    <option value="1 WEEK">Past Week</option>
                                    <option value="1 MONTH">Past Month</option>
                                    <option value="1 QUARTER">Past Quarter</option>
                                    <option value="1 YEAR">Past Year</option>
                                    <option value="custom">Custom Range</option>
                                </select>
                                <span class="ci-icon">
                                    <i class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-2 cp-5" ng-if="filter.interval_type === 'custom'">
                            <div class="custom-group">
                                <div class="custom-input">
                                    <input type="text" class="form-control datepickerNormal" placeholder="From Date" name="from_date" onkeydown="event.preventDefault()" ng-model="filter.from_date">
                                    <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 cp-5" ng-if="filter.interval_type === 'custom'">
                            <div class="custom-group">
                                <div class="custom-input">
                                    <input type="text" class="form-control datepickerNormal" placeholder="To Date" name="to_date" onkeydown="event.preventDefault()" ng-model="filter.to_date">
                                    <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-1">
                            <button type="submit" class="btn btn-default">Search</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>



        <div class="row rm-5 inactive" same-height-row>
            <div class="col-md-6 col-xs-6 cp-5">
                <div class="whitebox mb-10-xs text-center" same-height-col>
                    <strong>Total Availability Hours</strong>
                    <h4 class="mb-0-xs font-bold text-success"><i class="fal fa-clock"></i> {{dataList.total_hours ? dataList.total_hours : '0'|number:''}} hrs</h4>
                </div>
            </div>
            <div class="col-md-6 col-xs-6 cp-5">
                <div class="whitebox mb-10-xs text-center" same-height-col>
                    <strong>Total Booked Hours</strong>
                    <h4 class="mb-0-xs font-bold text-info"><i class="fal fa-clock"></i> {{dataList.total_hours_booked ? dataList.total_hours_booked : '0'|number:''}} hrs</h4>
                </div>
            </div>
            <div class="col-md-6 col-xs-6 cp-5">
                <div class="whitebox mb-10-xs text-center" same-height-col>
                    <strong>Total Down Hours</strong>
                    <h4 class="mb-0-xs font-bold text-danger"><i class="fal fa-clock"></i> {{dataList.total_down_hours ? dataList.total_down_hours : '0'|number:''}} hrs</h4>
                </div>
            </div>
            <div class="col-md-6 col-xs-6 cp-5">
                <div class="whitebox mb-10-xs text-center" same-height-col>
                    <strong>Occupancy Rate</strong>
                    <h4 class="mb-0-xs font-bold text-info"> {{dataList.occupency_rate ? dataList.occupency_rate : '0'|number:''}} %</h4>
                </div>
            </div>
        </div>



        <div class="responsive-table">

            <div class="row">
                <div class="col-md-12">
                    <h4>{{reportsTitle}}</h4>
                </div>
            </div>

            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120" class="no-sort">S.No</th>
                        <th>Center</th>
                        <th>Type</th>
                        <th>Duration  (hrs)</th>
                        <th>Occupancy (%)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in dataList.table_items track by $index">
                        <td data-label="S.No">{{($index + 1)}}</td>
                        <td data-label="Center">{{item.center_name}}</td>
                        <td data-label="Type">{{item.category_name}}</td>
                        <td data-label="Duration">{{item.duration}}</td>
                        <td data-label="Occupancy (%)">{{item.no_of_bookings}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="pageFooter" workspace-offset>&copy; Freshup Desk 2018</div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/reports/occupancyCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/reports/occupancyService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/centersService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/stayingCategoriesInventoryService.js?r=<?= time() ?>"></script>