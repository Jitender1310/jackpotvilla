<section class="pageWrapper" ng-controller="restRoomFeedbackCtrl">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-12"><strong>Restroom Feedback Reports</strong></div>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="GetRestRoomFeedBack(filter.page = 1); GetCentersList('<?= get_user_id() ?>');">
        <div class="whitebox ">
            <div class="row">
                <div class="col-md-12">
                    <form class="" ng-submit="GetRestRoomFeedBack(filter.page = 1)">
                        <div class="col-md-2">
                            <div class="custom-input" title="My Center">
                                <select class="form-control" ng-model="filter.rate">
                                    <option value="" selected >All Ratings</option>
                                    <option value="Excellent" >Excellent</option>
                                    <option value="Good" >Good</option>
                                    <option value="Average" >Average</option>
                                    <option value="Poor" >Poor</option>
                                    <option value="Very Poor" >Very Poor</option>
                                </select>
                                <span class="ci-icon">
                                    <i  class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="custom-input" title="My Center">
                                <select class="form-control" ng-model="filter.centers_id" ng-disabled="!centersList.length > 0">
                                    <option value="" selected >All Centers</option>
                                    <option ng-value="item.id" value="item.id" ng-repeat="item in centersList">{{item.center_name}}</option>
                                </select>
                                <span class="ci-icon">
                                    <i ng-show="centersSpinner" class="fal fa-circle-notch fa-spin"></i>
                                    <i ng-show="!centersSpinner" class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="custom-input" title="Gender">
                                <select class="form-control" ng-model="filter.gender">
                                    <option value="" selected >All Genders</option>
                                    <option value="Male" >Male</option>
                                    <option value="Female" >Female</option>
                                </select>
                                <span class="ci-icon">
                                    <i  class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-2 cp-5" >
                            <div class="custom-group">
                                <div class="custom-input">
                                    <input type="text" class="form-control datepickerNormal" placeholder="From Date" name="from_date" onkeydown="event.preventDefault()" ng-model="filter.from_date" >
                                    <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 cp-5" >
                            <div class="custom-group">
                                <div class="custom-input">
                                    <input type="text" class="form-control datepickerNormal" placeholder="To Date" name="to_date" onkeydown="event.preventDefault()" ng-model="filter.to_date" >
                                    <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>




                        <div class="col-md-2">
                            <button type="submit" class="btn btn-default">Search</button>
                            <button ng-click="ResetFilter()" class="btn btn-danger " title="Clear Search"><i class="fas fa-times-circle"></i></button>
                            <button type="button" class="btn btn-info" ng-click="ExportToExcel()" title="Export To Excel"><i class="far fa-download"></i> <i class="far fa-file-excel"></i></button>

                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row rm-5 inactive" same-height-row>
            <div class="col-md-12 col-xs-12 cp-5">
                <div class="whitebox mb-10-xs" same-height-col>
                    <div class="whiteboxHead" >
                        <strong><b>Restroom Feedback by Users <span  style="position: inherit;color: green;" ng-if="fromdate">From {{fromdate}} </span><span  style="position: inherit;color: green;" ng-if="todate"> - {{todate}}</span> <span style="position: inherit;color: green;" ng-if="center_name">in {{center_name}} Center</span></b></strong>
                    </div>
                    <div valign-parent>
                        <div class="row">
                            <div class="col-md-8 col-xs-7">
                                <div class="chart-container"><div class="chart" id="feedback"></div></div>
                            </div>
                            <div class="col-md-4 col-xs-5">
                                <ul class="ul-block" valign-holder>
                                    <li><i class="fas fa-circle text-success"></i> Excellent - {{counts.Excellent}} Members</li>
                                    <li><i class="fas fa-circle text-info"></i> Good - {{counts.Good}} Members</li>
                                    <li><i class="fas fa-circle text-warning"></i> Average - {{counts.Average}} Members</li>
                                    <li><i class="fas fa-circle text-muted"></i> Poor - {{counts.Poor}} Members</li>
                                    <li><i class="fas fa-circle text-danger"></i> Very Poor - {{counts.Very_Poor}} Members</li>
                                    <li><strong style="font-size: 20px;">Total submitted feedback - {{counts.Total}} Members</strong></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="responsive-table" ng-hide="restRoomFeedBackList.length > 0">
            <div class="alert alert-danger">
                No Data found
            </div>
        </div> 
        <div class="responsive-table" ng-show="restRoomFeedBackList.length > 0">


            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th class="text-left">S.No</th>
                        <th class="text-left">Rate</th>
                        <th class="text-left">Cause</th>
                        <th class="text-left">Gender</th>
                        <th class="text-left">Remarks</th>
                        <th class="text-left">Center</th>
                        <th class="text-left">Mobile</th>
                        <th class="text-left">Feedback Submitted On</th>

                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in restRoomFeedBackList track by $index" style="color: {{item.rate==='Very Poor'?'red':''}}">
                        <td data-label="S.No">
                            {{($index + restRoomFeedBackPagination.initial_id)}}
                        </td>
                        <td data-label="Rate"class="text-left">{{item.rate}}</td>
                        <td data-label="Cause"class="text-left"><span ng-repeat="item1 in item.cause track by $index">{{item1}} <br ></span></td>
                        <td data-label="Gender"class="text-left">{{item.gender}}</td>
                        <td data-label="Remarks"class="text-left">{{item.remarks}}</td>
                        <td data-label="Center"class="text-left">{{item.center_name}}</td>
                        <td data-label="Mobile"class="text-left">{{item.mobile}}</td>
                        <td data-label="Feedback Submitted on"class="text-left">{{item.created_at}}</td>

                    </tr>
                </tbody>
            </table>
        </div>
        <span  class="pull-right" ng-bind-html="restRoomFeedBackPagination.pagination"></span>
    </div>

    <div class="pageFooter" workspace-offset>&copy; Freshup Desk <?= date('Y') ?></div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/reports/restRoomFeedbackCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/reports/restRoomFeedbackService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/centersService.js?r=<?= time() ?>"></script>