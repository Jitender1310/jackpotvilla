<section class="pageWrapper" ng-controller="salesReportsCtrl">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-12"><strong>Sales report</strong></div>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="GetSalesList(); GetCentersList('<?= get_user_id() ?>');">


        <div class="whitebox ">
            <div class="row">
                <div class="col-md-12">
                    <form class="" ng-submit="GetSalesList()">
                        <div class="col-md-3 cp-5" >
                            <div class="custom-group">
                                <div class="custom-input">
                                    <input type="text" class="form-control datepickerNormal" autocomplete="off" placeholder="From Date" name="from_date" onkeydown="event.preventDefault()" ng-model="filter.from_date" >
                                    <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 cp-5" >
                            <div class="custom-group">
                                <div class="custom-input">
                                    <input type="text" class="form-control datepickerNormal" autocomplete="off" placeholder="To Date" name="to_date" onkeydown="event.preventDefault()" ng-model="filter.to_date" >
                                    <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="custom-input" title="My Center">
                                <select class="form-control" ng-model="filter.centers_id" ng-disabled="!centersList.length > 0" ng-change="GetStayingCategoriesListForCenter(filter.centers_id, 2)">
                                    <option value="" selected >All Centers</option>
                                    <option ng-value="item.id" value="item.id" ng-repeat="item in centersList">{{item.center_name}}</option>
                                </select>
                                <span class="ci-icon">
                                    <i ng-show="centersSpinner" class="fal fa-circle-notch fa-spin"></i>
                                    <i ng-show="!centersSpinner" class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>


                        <div class="col-md-3">
                            <button type="submit" class="btn btn-default">Search</button>
                            <button type="button" class="btn btn-info" ng-click="ExportToExcel()" title="Export To Excel"><i class="far fa-download"></i> <i class="far fa-file-excel"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="whitebox " ng-if="filter.from_date != ''" style="text-transform:uppercase;font-size: 15px;color:#000066 ">
            <div class="row">
                <div class="col-md-12 text-center ">
                    <strong>Sales reports <span ng-if="filter.from_date != ''">between dates <span  ng-bind-html="filter.from_date" ></span> - <span ng-bind-html="filter.to_date"></span><span ng-if="center_name != ''"> in {{center_name}}</span></span></strong>
                </div>
            </div>
        </div>



        <div class="row rm-5 inactive" same-height-row>
            <div class="col-md-2 col-xs-6 cp-5">
                <div class="whitebox mb-10-xs text-center" same-height-col>
                    <strong>By Cash</strong>
                    <h4 class="mb-0-xs font-bold text-info"><i class="fal fa-rupee-sign"></i> {{cash_sales ? cash_sales : '0'}}</h4>
                </div>
            </div>
            <div class="col-md-2 col-xs-6 cp-5">
                <div class="whitebox mb-10-xs text-center" same-height-col>
                    <strong>By Credit/Debit card</strong>
                    <h4 class="mb-0-xs font-bold text-info"><i class="fal fa-rupee-sign"></i> {{credit_card_sales ? credit_card_sales : '0'}}</h4>
                </div>
            </div>
            <div class="col-md-2 col-xs-6 cp-5">
                <div class="whitebox mb-10-xs text-center" same-height-col>
                    <strong>By Online</strong>
                    <h4 class="mb-0-xs font-bold text-info"><i class="fal fa-rupee-sign"></i> {{online_sales ? online_sales : '0'}}</h4>
                </div>
            </div>
            <div class="col-md-2 col-xs-6 cp-5">
                <div class="whitebox mb-10-xs text-center" same-height-col>
                    <strong>By Other payment types</strong>
                    <h4 class="mb-0-xs font-bold text-info"><i class="fal fa-rupee-sign"></i> {{other_sales ? other_sales : '0'}}</h4>
                </div>
            </div>

            <div class="col-md-4 col-xs-6 cp-5">
                <div class="whitebox mb-10-xs text-center" same-height-col>
                    <strong>Total sale amount <span ng-if="filter.from_date != ''">between <span  ng-bind-html="filter.from_date"></span> - <span ng-bind-html="filter.to_date"></span></span></strong>
                    <h4 class="mb-0-xs font-bold text-info"><i class="fal fa-rupee-sign"></i> {{total_sales ? total_sales : '0'}}</h4>
                </div>
            </div>
        </div>



        <div class="responsive-table">

            <!--            <div class="row">
                            <div class="col-md-12">
                                <h4>{{reportsTitle}}</h4>
                            </div>
                        </div>-->

            <table class="table table-custom">
                <thead>
                    <tr>
                        <th width="120" class="text-left">S.No</th>
                        <th  class="text-left">Date</th>
                        <th class="text-right">Cash (Rs/-)</th>
                        <th class="text-right">Card/Debit Card (Rs/-)</th>
                        <th class="text-right">Online (Rs/-)</th>
                        <th class="text-right">Other types (Rs/-)</th>
                        <th class="text-right">Grand Total (Rs/-)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in salesList track by $index">
                        <td data-label="S.No"class="text-left">{{($index + 1)}}</td>
                        <td data-label="Date" class="text-left">{{item.date}}</td>
                        <td data-label="Cash Sales"  class="text-right">{{item.cash_sales ? item.cash_sales : '0'}}</td>
                        <td data-label="Credit Card Sales"  class="text-right">{{item.credit_card_sales ? item.credit_card_sales : '0'}}</td>
                        <td data-label="Online Sales"  class="text-right">{{item.online_sales ? item.online_sales : '0'}}</td>
                        <td data-label="Other Sales"  class="text-right">{{item.other_sales ? item.other_sales : '0'}}</td>
                        <td data-label="Grand Total"  class="text-right">{{item.grand_total ? item.grand_total : '0'}}</td>                   
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="pageFooter" workspace-offset>&copy; Freshup Desk <?= date('Y') ?></div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/reports/salesReportsCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/reports/salesReportsService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/centersService.js?r=<?= time() ?>"></script>