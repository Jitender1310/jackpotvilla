<section class="pageWrapper" ng-controller="restroomCleaningChecklistCtrl" ng-init="GetCentersList('<?= get_user_id() ?>');">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-12"><strong>Restroom Cleaning Checklist Reports</strong></div>
        </div>
    </div>
    <div class="pageBody" workspace>
        <div class="whitebox ">
            <div class="row">
                <div class="col-md-12">
                    <form class="" ng-submit="GetRestRoomCleaningChecklist()">

                        <div class="col-md-2">

                            <div class="custom-input" title="My Center">
                                <select class="form-control" ng-model="filter.centers_id" ng-disabled="!centersList.length > 0" ng-change="GetStaffList(filter.centers_id);">

                                    <option ng-value="item.id" value="item.id" ng-repeat="item in centersList">{{item.center_name}}</option>
                                </select>
                                <span class="ci-icon">
                                    <i ng-show="centersSpinner" class="fal fa-circle-notch fa-spin"></i>
                                    <i ng-show="!centersSpinner" class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-2" ng-if="filter.centers_id">
                            <div class="custom-input" title="Cleaning Staff">
                                <select class="form-control" ng-model="filter.staff_id" ng-disabled="!staffList.length > 0">
                                    <option value="" selected >Cleaning Staff</option>
                                    <option ng-value="item.id" value="item.id" ng-repeat="item in staffList">{{item.name}}</option>
                                </select>
                                <span class="ci-icon">
                                    <i ng-show="centersSpinner" class="fal fa-circle-notch fa-spin"></i>
                                    <i ng-show="!centersSpinner" class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-2 cp-5" >
                            <div class="custom-group">
                                <div class="custom-input">
                                    <input type="text" class="form-control datepickerNormal" placeholder="From Date" name="from_date" onkeydown="event.preventDefault()" ng-model="filter.from_date" >
                                    <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 cp-5" >
                            <div class="custom-group">
                                <div class="custom-input">
                                    <input type="text" class="form-control datepickerNormal" placeholder="To Date" name="to_date" onkeydown="event.preventDefault()" ng-model="filter.to_date" >
                                    <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <button type="submit" class="btn btn-default">Search</button>
                            <button ng-click="ResetFilter()" class="btn btn-danger " title="Clear Search"><i class="fas fa-times-circle"></i></button>
                            <button type="button" class="btn btn-info" ng-click="ExportToExcel()" title="Export To Excel"><i class="far fa-download"></i> <i class="far fa-file-excel"></i></button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="responsive-table" ng-hide="restRoomCleaningCheckList.length > 0">
            <div class="alert alert-danger">
                No Data found
            </div>
        </div> 
        <div class="responsive-table" ng-show="restRoomCleaningCheckList.length > 0">
            <style>
                .table td {
                    border: 1px solid #ddd;
                    padding: 8px;
                }
            </style>

            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
<!--                <thead>
                    <tr>
                        <th class="text-left">Area</th>
                        <th class="text-left">7:00 AM</th>
                        <th class="text-left">9:00 AM</th>
                        <th class="text-left">11:00 AM</th>
                        <th class="text-left">1:00 PM</th>
                        <th class="text-left">3:00 PM</th>
                        <th class="text-left">6:00 PM</th>
                        <th class="text-left">8:00 PM</th>
                        <th class="text-left">11:00 PM</th>
                        <th class="text-left">12:00 AM</th>
                        <th class="text-left">2:00 AM</th>
                        <th class="text-left">4:00 AM</th>
                        <th class="text-left">6:00 AM</th>

                    </tr>
                </thead>-->
                <tbody ng-repeat="item in restRoomCleaningCheckList track by $index">
                    <tr style="font-weight:bold;">
                        <td class="text-center text-info" colspan="13">Restroom Cleaning Checklist on {{item.date}} <span ng-if="item.center_name"> in {{item.center_name}}</span> <span ng-if="item.staff_name"> By {{item.staff_name}}</span></td>
                    </tr>
                    <tr style="font-weight:bold;">
                        <td class="text-left">Area</td>
                        <td class="text-left">7:00 AM</td>
                        <td class="text-left">9:00 AM</td>
                        <td class="text-left">11:00 AM</td>
                        <td class="text-left">1:00 PM</td>
                        <td class="text-left">3:00 PM</td>
                        <td class="text-left">6:00 PM</td>
                        <td class="text-left">8:00 PM</td>
                        <td class="text-left">11:00 PM</td>
                        <td class="text-left">12:00 AM</td>
                        <td class="text-left">2:00 AM</td>
                        <td class="text-left">4:00 AM</td>
                        <td class="text-left">6:00 AM</td>
                    </tr>
                    <tr  ng-repeat="item1 in item.checklist track by $index">
                        <td data-label="Area"class="text-left" style="font-weight:'bold';">{{item1.restroom_cleaning_checklist_category_name}}</td>
                        <td data-label="6:00 AM - 7:00 AM"class="text-left" style="color:{{item1.time1==0?'red':'green'}}">{{item1.time1==0?'Not Done':''}} <span ng-hide="item1.time1 == 0">{{item1.time1.staff_name}} - {{item1.time1.time_stamp}}</span></td>
                        <td data-label="7:00 AM - 9:00 AM"class="text-left" style="color:{{item1.time2==0?'red':'green'}}">{{item1.time2==0?'Not Done':''}} <span ng-hide="item1.time2 == 0">{{item1.time2.staff_name}} - {{item1.time2.time_stamp}}</span></td>
                        <td data-label="9:00 AM - 11:00 AM"class="text-left" style="color:{{item1.time3==0?'red':'green'}}">{{item1.time3 == 0?'Not Done':''}} <span ng-hide="item1.time3 == 0">{{item1.time3.staff_name}} - {{item1.time3.time_stamp}}</span></td>
                        <td data-label="11:00 AM - 1:00 PM"class="text-left" style="color:{{item1.time4==0?'red':'green'}}">{{item1.time4==0?'Not Done':''}} <span ng-hide="item1.time4 == 0">{{item1.time4.staff_name}} - {{item1.time4.time_stamp}}</span></td>
                        <td data-label="1:00 PM - 3:00 PM"class="text-left" style="color:{{item1.time5==0?'red':'green'}}">{{item1.time5==0?'Not Done':''}} <span ng-hide="item1.time5 == 0">{{item1.time5.staff_name}} - {{item1.time5.time_stamp}}</span></td>
                        <td data-label="3:00 PM - 6:00 PM"class="text-left" style="color:{{item1.time6==0?'red':'green'}}">{{item1.time6==0?'Not Done':''}} <span ng-hide="item1.time6 == 0">{{item1.time6.staff_name}} - {{item1.time6.time_stamp}}</span></td>
                        <td data-label="6:00 PM - 8:00 PM"class="text-left"style="color:{{item1.time7==0?'red':'green'}}">{{item1.time7==0?'Not Done':''}} <span ng-hide="item1.time7 == 0">{{item1.time7.staff_name}} - {{item1.time7.time_stamp}}</span></td>
                        <td data-label="8:00 PM - 11:00 PM"class="text-left" style="color:{{item1.time8==0?'red':'green'}}">{{item1.time8==0?'Not Done':''}} <span ng-hide="item1.time8 == 0">{{item1.time8.staff_name}} - {{item1.time8.time_stamp}}</span></td>
                        <td data-label="11:00 PM - 12:00 PM"class="text-left" style="color:{{item1.time9==0?'red':'green'}}">{{item1.time9==0?'Not Done':''}} <span ng-hide="item1.time9 == 0">{{item1.time9.staff_name}} - {{item1.time9.time_stamp}}</span></td>
                        <td data-label="12:00 AM - 2:00 AM"class="text-left" style="color:{{item1.time10==0?'red':'green'}}">{{item1.time10==0?'Not Done':''}} <span ng-hide="item1.time10 == 0">{{item1.time10.staff_name}} - {{item1.time10.time_stamp}}</span></td>
                        <td data-label="2:00 AM - 4:00 AM"class="text-left" style="color:{{item1.time11==0?'red':'green'}}">{{item1.time11==0?'Not Done':''}} <span ng-hide="item1.time11 == 0">{{item1.time11.staff_name}} - {{item1.time11.time_stamp}}</span></td>
                        <td data-label="4:00 AM - 6:00 AM"class="text-left" style="color:{{item1.time12==0?'red':'green'}}">{{item1.time12==0?'Not Done':''}} <span ng-hide="item1.time12 == 0">{{item1.time12.staff_name}} - {{item1.time12.time_stamp}}</span></td>
                    </tr>
                    <tr style="background-color: #f3f2f1">
                        <td colspan="13"></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <span  class="pull-right" ng-bind-html="restRoomCleaningCheckListPagination.pagination"></span>
    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk <?= date('Y') ?></div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/reports/restroomCleaningChecklistCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/reports/restroomCleaningChecklistService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/restroomCleaningChecklistCategoriesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/centersService.js?r=<?= time() ?>"></script>