<section class="pageWrapper" ng-controller="salesAndReservationsCtrl">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-12"><strong>Sales and Reservations</strong></div>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="GetSalesAndReservations(); GetCentersList('<?= get_user_id() ?>');">


        <div class="whitebox ">
            <div class="row">
                <div class="col-md-12">
                    <form class="" ng-submit="GetSalesAndReservations(filter.page = 1)">
                        <div class="col-md-2">
                            <div class="custom-input" title="My Center">
                                <select class="form-control" ng-model="filter.payment_method">
                                    <option value="" selected >All Payment Modes</option>
                                    <option value="Cash" >Cash</option>
                                    <option value="Credit Card" >Credit/Debit Card</option>
                                    <option value="Payment Gateway" >Online</option>
                                    <option value="Other" >Other</option>

                                </select>
                                <span class="ci-icon">
                                    <i  class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="custom-input" title="My Center">
                                <select class="form-control" ng-model="filter.centers_id" ng-disabled="!centersList.length > 0">
                                    <option value="" selected >All Centers</option>
                                    <option ng-value="item.id" value="item.id" ng-repeat="item in centersList">{{item.center_name}}</option>
                                </select>
                                <span class="ci-icon">
                                    <i ng-show="centersSpinner" class="fal fa-circle-notch fa-spin"></i>
                                    <i ng-show="!centersSpinner" class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-2 cp-5" >
                            <div class="custom-group">
                                <div class="custom-input">
                                    <input type="text" autocomplete="off" class="form-control datepickerNormal" placeholder="From Date" name="from_date" onkeydown="event.preventDefault()" ng-model="filter.from_date" >
                                    <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 cp-5" >
                            <div class="custom-group">
                                <div class="custom-input">
                                    <input type="text" autocomplete="off" class="form-control datepickerNormal" placeholder="To Date" name="to_date" onkeydown="event.preventDefault()" ng-model="filter.to_date" >
                                    <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>




                        <div class="col-md-2">
                            <button type="submit" class="btn btn-default">Search</button>
                            <button type="reset" ng-click="ResetFilter()" class="btn btn-danger " title="Clear Search"><i class="fas fa-times-circle"></i></button>
                            <button ng-show="filter.from_date != '' || filter.to_date != '' || filter.payment_method != '' || filter.centers_id != ''" type="button" class="btn btn-info" ng-click="ExportToExcel()" title="Export To Excel"><i class="far fa-download"></i> <i class="far fa-file-excel"></i></button>

                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!--        <div class="whitebox " ng-if="filter.from_date != ''" style="text-transform:uppercase;font-size: 15px;color:#000066 ">
                    <div class="row">
                        <div class="col-md-12 text-center ">
                            <strong>Sales reports <span ng-if="filter.from_date != ''">between dates <span  ng-bind-html="filter.from_date" ></span> - <span ng-bind-html="filter.to_date"></span><span ng-if="center_name != ''"> in {{center_name}}</span></span></strong>
                        </div>
                    </div>
                </div>-->


        <div class="row rm-5 inactive" same-height-row>
            <div class="col-md-3 col-xs-6 cp-5">
                <div class="whitebox mb-10-xs text-center" same-height-col>
                    <strong>By Cash</strong>
                    <h4 class="mb-0-xs font-bold text-info"><i class="fal fa-rupee-sign"></i> {{cash_sales ? cash_sales : '0'}}</h4>
                </div>
            </div>
            <div class="col-md-3 col-xs-6 cp-5">
                <div class="whitebox mb-10-xs text-center" same-height-col>
                    <strong>By Credit/Debit card</strong>
                    <h4 class="mb-0-xs font-bold text-info"><i class="fal fa-rupee-sign"></i> {{credit_card_sales ? credit_card_sales : '0'}}</h4>
                </div>
            </div>
            <div class="col-md-3 col-xs-6 cp-5">
                <div class="whitebox mb-10-xs text-center" same-height-col>
                    <strong>By Online </strong>
                    <h4 class="mb-0-xs font-bold text-info"><i class="fal fa-rupee-sign"></i> {{online_sales ? online_sales : '0'}}</h4>
                </div>
            </div>
            <div class="col-md-3 col-xs-6 cp-5">
                <div class="whitebox mb-10-xs text-center" same-height-col>
                    <strong>By Other types</strong>
                    <h4 class="mb-0-xs font-bold text-info"><i class="fal fa-rupee-sign"></i> {{other_sales ? other_sales : '0'}}</h4>
                </div>
            </div>

        </div>



        <div class="responsive-table" ng-if="salesAndReservationsList.length > 0" >

            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th class="no-sort text-left">S.No</th>
                        <th class="no-sort text-left">Booking ID</th>
                        <th class="no-sort text-left">Name</th>
                        <th class="no-sort text-left">Mobile</th>
                        <th class="no-sort text-left">Center</th>
                        <th class="no-sort text-left">Check-in Date Time</th>
                        <th class="no-sort text-right">Amount (Rs/-)</th>
                        <th class="no-sort text-left">Payment Method</th>
                        <th class="no-sort text-left">Booking Status</th>
                        <th class="no-sort text-left">Booked From</th>

                        <th class="no-sort text-left">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in salesAndReservationsList track by $index">
                        <td data-label="S.No">
                            {{($index + salesAndReservationsPaginantion.initial_id)}}
                        </td>
                        <td data-label="Ref ID">
                            <a class="text-primary"  ng-href="<?= base_url() ?>reservations/view?booking_id={{item.booking_ref_number}}" target="_blank"><strong>{{item.booking_ref_number}}</strong></a>
                        </td>
                        <td data-label="Name"class="text-left">{{item.contact_name}}</td>
                        <td data-label="Mobile"class="text-left">{{item.contact_mobile}}</td>
                        <td data-label="City"class="text-left">{{item.center_name}}</td>
                        <td data-label="Check-in Date Time"class="text-left">{{item.check_in_date_time}}</td>
                        <td data-label="Amount"class="text-right">{{item.net_amount}}</td>
                        <td data-label="Payment Mode"class="text-left">{{item.payment_method}}</td>
                        <td data-label="Booking Status"class="text-left">{{item.booking_status}}</td>
                        <td data-label="Booked From"class="text-left">{{item.creation_source}}</td>
                        <td data-label="Action"class="text-left">
                            <a class="text-primary"  ng-href="<?= base_url() ?>reservations/quick_view?booking_id={{item.booking_ref_number}}" target="_blank"><button type="button" class="btn btn-info" title="Booking View">View</button></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <span  class="pull-right" ng-bind-html="salesAndReservationsPaginantion.pagination"></span>
    </div>

    <div class="pageFooter" workspace-offset>&copy; Freshup Desk <?= date('Y') ?></div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/reports/salesAndReservationsCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/reports/salesAndReservationsService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/centersService.js?r=<?= time() ?>"></script>