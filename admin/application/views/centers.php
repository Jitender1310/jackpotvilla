<section class="pageWrapper" ng-controller="centersCtrl">
    <div class="pageHeader" workspace-offset valign-parent>
        <div class="row">
            <div class="col-md-6"><strong>Centers</strong></div>
            <div class="col-md-6">
                <div valign-holder class="text-right">
                    <button type="button" class="btn btn-info" ng-show="showActive" ng-click="GetCentersList('active');showActive = false"><i class="fal fa-eye"></i> Show active Centers</button>
                    <button type="button" class="btn btn-danger" ng-hide="showActive" ng-click="GetCentersList('inactive');showActive = true"><i class="fal fa-eye"></i> Show inactive Centers</button>
                    <button type="button" class="btn btn-primary" ng-click="ShowCenterAddForm()"><i class="fal fa-plus"></i> Add Center</button>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="GetCentersList('active')">
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th class="col-md-1">S.No</th>
                        <th class="col-md-1">Center Name</th>
                        <th class="col-md-3">Address</th>
                        <th class="col-md-2">Live Payment Gateway details</th>
                        <th class="col-md-2">Running Status</th>
                        <th class="col-md-2">IRCTC Center</th>
                        <th class="col-md-2">Active Status</th>
                        <th class="no-sort text-right" class="col-md-1">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in centersList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Center Name"> {{item.center_name}}</td>
                        <td data-label="Address">{{item.address}}</td>
                        <td data-label="Paymentgateway details" ng-if="item.paymentgateway_productionkey != ''">Configured</td>
                        <td data-label="Paymentgateway details" ng-if="item.paymentgateway_productionkey == ''">Not Configured</td>
                        <td data-label="Running Status">
                            <span class="text-{{item.centers_status=='Paused'?'danger':'success'}}">{{item.centers_status}}</span>
                        </td>
                        <td data-label="IRCTC Center">
                            <span class="{{item.is_irctc_based_center_text_bootstrap_class}}">{{item.is_irctc_based_center_text}}</span>
                        </td>
                        <td data-label="Active Status">
                            <span class="text-{{item.status=='0'?'danger':'success'}}">{{item.status=='1'?'Active':'Inactive'}}</span>
                        </td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditCenter(item)"><a href="#">Edit</a></li>
                                    <li ng-click="ManagePaymentgatewayKeys(item)"><a href="#">Manage Payment gateway keys</a></li>
                                    <li ng-click="ViewCenter(item)"><a href="#">View Details</a></li>
                                    <li ng-click="PauseCenterBookings(item)"><a href="#">Pause Bookings</a></li>
                                    <li ng-click="DeleteCenter(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk - 2017</div>
    <div class="modal fade" id="center-add">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{centerObj.id?"Update":"Add"}} Center</h4>
                </div>
                <div class="modal-body">
                    <form ng-submit="AddOrUpdateCenter()" id="centerForm" >
                        <span class="text-danger" ng-bind-html="error_message"></span>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Country</label>
                                    <div class="custom-input">
                                        <select class="form-control" name="countries_id" ng-model="centerObj.countries_id" ng-change="GetStatesList(centerObj.countries_id)" ng-disabled="!countriesList.length > 0">
                                            <option value="" selected disabled="">Choose Country</option>
                                            <option value="{{item.id}}" ng-repeat="item in countriesList track by $index">{{item.country_name}}</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i class="fal fa-chevron-down"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>State</label>
                                    <div class="custom-input">
                                        <select class="form-control" name="states_id" ng-model="centerObj.states_id" ng-change="GetCitiesList(centerObj.states_id)" ng-disabled="!statesList.length > 0">
                                            <option value="" selected disabled="">Choose State</option>
                                            <option value="{{item.id}}" ng-repeat="item in statesList track by $index">{{item.state_name}}</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i ng-show="!stateSpinner" class="fal fa-chevron-down"></i>
                                            <i ng-show="stateSpinner" class="fal fa-circle-notch fa-spin"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Choose City</label>
                                    <div class="custom-input">
                                        <select class="form-control" name="cities_id" ng-model="centerObj.cities_id" ng-change="GetLocationsList(centerObj.cities_id)" ng-disabled="!citiesList.length > 0">
                                            <option value="" selected disabled="">Choose City</option>
                                            <option value="{{item.id}}" ng-repeat="item in citiesList track by $index">{{item.city_name}}</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i ng-show="!citySpinner" class="fal fa-chevron-down"></i>
                                            <i ng-show="citySpinner" class="fal fa-circle-notch fa-spin"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Location</label>
                                    <div class="custom-input">
                                        <select class="form-control" name="locations_id" ng-model="centerObj.locations_id"  ng-disabled="!locationsList.length > 0">
                                            <option value="" selected disabled="">Choose Location</option>
                                            <option value="{{item.id}}" ng-repeat="item in locationsList track by $index">{{item.location_name}}</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i ng-show="!locationSpinner" class="fal fa-chevron-down"></i>
                                            <i ng-show="locationSpinner" class="fal fa-circle-notch fa-spin"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Center Name</label>
                                    <input type="text" class="form-control" name="center_name" ng-model="centerObj.center_name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Center Code</label>
                                    <input type="hidden" id="center_id" value="{{centerObj.id}}">
                                    <input type="text" class="form-control" name="center_code" id="center_code" ng-model="centerObj.center_code">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Latitude</label>
                                    <input type="number" class="form-control" name="latitude" ng-model="centerObj.latitude">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Longitude</label>
                                    <input type="number" class="form-control" name="longitude" ng-model="centerObj.longitude">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Map</label>
                                    <textarea class="form-control" rows="4" name="map_location" required ng-model="centerObj.map_location"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea class="form-control" rows="4" name="address" ng-model="centerObj.address"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row" ng-if="!centerObj.id">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email</label>
                                    <small>( Enter by Comma Separated )</small>
                                    <tags-input ng-model="centerObj.emails_array" placeholder="Add a Email"></tags-input>
                                </div>
                            </div>
                        </div>
                        <div class="row" ng-if="centerObj.id">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email</label>
                                    <small>( Enter by Comma Separated )</small>
                                    <tags-input ng-model="centerObj.emails_array" placeholder="Add a Email"></tags-input>
                                </div>
                            </div>
                        </div>

                        <div class="row" ng-if="!centerObj.id">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Mobile</label>
                                    <small>( Enter by Comma Separated )</small>
                                    <tags-input ng-model="centerObj.mobiles_array" placeholder="Add a Mobile"></tags-input>
                                </div>
                            </div>
                        </div>
                        <div class="row" ng-if="centerObj.id">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Mobile</label>
                                    <small>( Enter by Comma Separated )</small>
                                    <tags-input ng-model="centerObj.mobiles_array" placeholder="Add a Mobile"></tags-input>
                                </div>
                            </div>
                        </div>



                        <div class="row" ng-if="!centerObj.id">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Display Mobile Numbers</label>
                                    <small>( Enter by Comma Separated )</small>
                                    <tags-input ng-model="centerObj.display_mobile_numbers_array" placeholder="Add a Mobile to Display"></tags-input>
                                </div>
                            </div>
                        </div>
                        <div class="row" ng-if="centerObj.id">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Display Mobile Numbers</label>
                                    <small>( Enter by Comma Separated )</small>
                                    <tags-input ng-model="centerObj.display_mobile_numbers_array" placeholder="Add a Mobile to Display"></tags-input>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>GST Number</label>
                                    <input type="text" class="form-control" name="gst_number" ng-model="centerObj.gst_number" minlength="15" maxlength="15" pattern="[a-zA-Z0-9\s]+">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Trade License Number</label>
                                    <input type="text" class="form-control" name="trade_license_number" ng-model="centerObj.trade_license_number" required minlength="8" maxlength="15">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Started Date (DD-MM-YYYY)</label>
                                    <input type="text" class="form-control" name="started_date" ng-model="centerObj.started_date">
                                    <small>Enter date manually as per format (DD-MM-YYYY)</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Facebook Pixel Wishlist Tracking Content IDs</label>
                                    <input type="text" class="form-control" name="facebook_pixel_wish_list_tracking_content_id" ng-model="centerObj.facebook_pixel_wish_list_tracking_content_id">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>If IRCTC Based Center Choose Yes Otherwise No</label>
                                    <div class="custom-input">
                                        <select class="form-control" name="is_irctc_based_center" ng-model="centerObj.is_irctc_based_center">
                                            <option value="" selected disabled="">Choose Option</option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i class="fal fa-chevron-down"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Center Deleted</label>
                                    <div class="custom-input">
                                        <select class="form-control" name="status" ng-model="centerObj.status">
                                            <option value="" selected disabled="">Choose Option</option>
                                            <option value="1">No</option>
                                            <option value="0">Yes</option>
                                        </select>
                                        <span class="ci-icon">
                                            <i class="fal fa-chevron-down"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" ng-show="centerObj.staying_types.length > 0">
                                <div class="title-line text-left"><span class="h4">Manage Staying Types for this center</span></div>
                                <ul class="manageStayingTypes">
                                    <li ng-repeat="item in centerObj.staying_types track by $index">
                                        <div class="checkbox-inline">
                                            <label>
                                                <input type="checkbox" value="1" ng-model="item.is_selected"><span></span>
                                                {{item.name}}
                                            </label>
                                        </div>
                                        <small>{{item.staying_type_categories.length}}</small>

                                        <table class="table table-bordered" ng-show="item.is_selected">
                                            <tbody>
                                                <tr ng-repeat="sub_item in item.staying_type_categories">
                                                    <th style="width: 25%">{{sub_item.category_name}}</th>
                                                    <td style="width: 30%">
                                                        <div class="actions">
                                                            <div class="radio-inline">
                                                                <label>
                                                                    <input type="radio" name="availability_type{{sub_item.category_name}}" ng-model="sub_item.availability_status" value="Available" ng-checked="sub_item.availability_status == 'Available'">
                                                                    <span></span>
                                                                    Available
                                                                </label>
                                                            </div>

                                                            <div class="radio-inline">
                                                                <label>
                                                                    <input type="radio" name="availability_type{{sub_item.category_name}}" ng-model="sub_item.availability_status" value="Not Available" ng-checked="sub_item.availability_status == 'Not Available'">
                                                                    <span></span>
                                                                    Not Available
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td style="width:45%">
                                                        <div class="actions" ng-show="sub_item.availability_status === 'Available'">
                                                            <div class="radio-inline">
                                                                <label>
                                                                    <input type="radio" name="availability_type{{sub_item.category_name}}_type" ng-model="sub_item.availability_type" value="1" ng-checked="sub_item.availability_type == 1">
                                                                    <span></span>
                                                                    Hourly
                                                                </label>
                                                            </div>

                                                            <div class="radio-inline">
                                                                <label>
                                                                    <input type="radio" name="availability_type{{sub_item.category_name}}_type" ng-model="sub_item.availability_type" value="2" ng-checked="sub_item.availability_type == 2">
                                                                    <span></span>
                                                                    Minutes
                                                                </label>
                                                            </div>

                                                            <div class="radio-inline" ng-hide="item.name === 'Freshup'">
                                                                <label>
                                                                    <input type="radio" name="availability_type{{sub_item.category_name}}_type" ng-model="sub_item.availability_type" value="3" ng-checked="sub_item.availability_type == 3">
                                                                    <span></span>
                                                                    Days
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <button class="btn btn-default " ng-click="ResetForm()" data-dismiss="modal"><b>Cancel <i class="fal fa-times"></i></b></button>
                        <button class="btn btn-primary pull-right" type="submit"><b>{{centerObj.id?"Update":"Add"}} <i class="fal fa-arrow-right"></i></b></button>
                    </form>
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="manage-paymentgateway">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Update Manage Paymentgateway Keys</h4>
                </div>
                <div class="modal-body">
                    <form ng-submit="Updatepaymentkeys()" id="managepaymentgatewaykeysform" >
                        <span class="text-danger" ng-bind-html="error_message"></span>

                        <h4 class="text-success">Test Environment</h4>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Payment gateway test key</label>
                                    <input type="text" class="form-control" name="paymentgateway_testkey" ng-model="paymentkeysObj.paymentgateway_testkey">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Payment gateway test Secret Key</label>
                                    <input type="text" class="form-control" name="paymentgateway_testsecret" ng-model="paymentkeysObj.payment_gateway_test_secret">
                                </div>
                            </div>
                        </div>

                        <hr/>

                        <h4 class="text-danger">Production Environment</h4>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Payment gateway production key</label>
                                    <input type="text" class="form-control" name="paymentgateway_productionkey" ng-model="paymentkeysObj.paymentgateway_productionkey">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Payment gateway production Secret Key</label>
                                    <input type="text" class="form-control" name="paymentgateway_productionsecret" ng-model="paymentkeysObj.payment_gateway_production_secret">
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-primary pull-right" type="submit"><b>Update Keys <i class="fal fa-arrow-right"></i></b></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- View Center-->
    <div class="modal fade" id="view_center_popup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Center Details</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <tbody>
                                <tr>
                                    <th>Center Name</th>
                                    <td>{{viewCenterObj.center_name}}</td>
                                </tr>
                                <tr>
                                    <th>Address</th>
                                    <td>{{viewCenterObj.address}}</td>
                                </tr>
                                <tr>
                                    <th>Email(s)</th>
                                    <td>{{viewCenterObj.emails}}</td>
                                </tr>
                                <tr>
                                    <th>Mobiles</th>
                                    <td>{{viewCenterObj.mobiles}}</td>
                                </tr>
                                <tr>
                                    <th>Display Mobiles</th>
                                    <td>{{viewCenterObj.display_mobile_numbers}}</td>
                                </tr>
                                <tr>
                                    <th>Country</th>
                                    <td>{{viewCenterObj.country_name}}</td>
                                </tr>
                                <tr>
                                    <th>State</th>
                                    <td>{{viewCenterObj.state_name}}</td>
                                </tr>
                                <tr>
                                    <th>City</th>
                                    <td>{{viewCenterObj.city_name}}</td>
                                </tr>
                                <tr>
                                    <th>Location</th>
                                    <td>{{viewCenterObj.location_name}}</td>
                                </tr>
                                <tr>
                                    <th>Latitude</th>
                                    <td>{{viewCenterObj.latitude}}</td>
                                </tr>
                                <tr>
                                    <th>Longitude</th>
                                    <td>{{viewCenterObj.longitude}}</td>
                                </tr>
                                <tr>
                                    <th>GST Number</th>
                                    <td>{{viewCenterObj.gst_number}}</td>
                                </tr>
                                <tr>
                                    <th>Trade License Number</th>
                                    <td>{{viewCenterObj.trade_license_number}}</td>
                                </tr>
                                <tr>
                                    <th>Started Date</th>
                                    <td>{{viewCenterObj.started_date}}</td>
                                </tr>
                                <tr>
                                    <th>Payment-gateway test key</th>
                                    <td>{{viewCenterObj.paymentgateway_testkey}}</td>
                                </tr>
                                <tr>
                                    <th>Payment-gateway production key</th>
                                    <td>{{viewCenterObj.paymentgateway_productionkey}}</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <h4 class="text-info text-center">Staying Types</h4>
                                    </td>
                                </tr>
                                <tr ng-repeat="item in viewCenterObj.staying_types track by $index">
                                    <td colspan="2"><b>{{item.name}}</b>
                                        <table class="table table-bordered table-hover" ng-show="item.staying_type_categories.length > 0">
                                            <tbody>
                                                <tr>
                                                    <th>Category</th>
                                                    <th>Availability Status</th>
                                                    <th>Calculation Type</th>
                                                </tr>
                                                <tr ng-repeat="subitem in item.staying_type_categories track by $index">
                                                    <td>{{subitem.category_name}}</td>
                                                    <td>
                                                        <span class="{{subitem.bootstrap_class}}">{{subitem.availability_status}}</span>
                                                    </td>
                                                    <td>{{(subitem.availability_type == 1) ? 'Hourly':((subitem.availability_type == 2) ? 'Minutes':((subitem.availability_type == 3) ? 'Days':''))}}</td>
                                                </tr>
                                                <tr ng-if="item.staying_type_categories.length == 0">
                                                    <td colspan="2">None</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr ng-if="viewCenterObj.staying_types.length == 0">
                                    <td colspan="2">None</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--View Center popup end-->

    <!--Pause Center Popup start-->
    <div class="modal fade" id="center-pause">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Pause Bookings for Center - {{pauseCenter.center_name}} [{{pauseCenter.center_code}}]</h4>
                </div>
                <div class="modal-body">
                    <form ng-submit="UpdateCenterPauseBookingDates()" id="pauseCenterDatesForm" >
                        <span class="text-danger" ng-bind-html="error_message"></span>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>From Date <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control dwfdateswithtime" name="pause_from_date" autocomplete="from_date" onkeydown="event.preventDefault()" ng-model="pauseCenter.paused_from_date">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>To Date <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control dwfdateswithtime" name="pause_to_date" autocomplete="to_date" onkeydown="event.preventDefault()" ng-model="pauseCenter.paused_to_date">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Reason For Pausing (Internal Purpose)<span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="pause_reason" ng-model="pauseCenter.paused_reason"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Reason For Pausing (This content will be shown in website for customers)<span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="paused_reason_display_in_website" ng-model="pauseCenter.paused_reason_display_in_website"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <p><span class="text-danger">Note: </span> Center will be in-active for bookings during the selected time period.</p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <button class="btn btn-primary pull-left" ng-click="UpdateCenterAsRunning()" type="button"><b>Start Running</b></button>
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-danger pull-right" type="submit"><b>Pause these dates <i class="fal fa-arrow-right"></i></b></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--Pause Center Popup end-->
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/centersCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/centersService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/countriesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/statesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/citiesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/locationsService.js?r=<?= time() ?>"></script>