<section class="pageWrapper" ng-controller="couponsCtrl">
    <div class="pageHeader" workspace-offset valign-parent>
        <div class="row">
            <div class="col-md-6">
                <strong>Coupons</strong>
            </div>
            <div class="col-md-6">
                <div valign-holder class="text-right">
                    <button type="button" class="btn btn-primary" ng-click="ShowCouponAddForm()"><i class="fal fa-plus"></i> Add Coupon</button>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="GetCouponsList()">
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Coupon Code</th>
                        <th>Center</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Discount Value</th>
                        <th>Discount Type</th>
                        <th>Minimum Amount To Apply Discount</th>
                        <th>Applicable On</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in couponsList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Type">{{item.coupon_code}}</td>
                        <td data-label="Coupon Name">{{item.center_names}}</td>
                        <td data-label="City">{{item.start_date}}</td>
                        <td data-label="State">{{item.end_date}}</td>
                        <td data-label="Country Name">{{item.discount_value}}</td>
                        <td data-label="Mobile">{{item.discount_type}}</td>
                        <td data-label="Email">{{item.minimum_to_apply_discount}}</td>
                        <td data-label="GST Number">{{item.applicable_on}}</td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditCoupon(item)"><a href="#">Edit</a></li>
                                    <li ng-click="DeleteCoupon(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="pageFooter" workspace-offset>&copy; Freshup Desk 2018</div>

    <div class="modal fade" id="coupon-add">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{couponObj.id?'Update':'Add'}} Coupon</h4>
                </div>
                <div class="modal-body">
                    <form ng-submit="AddOrUpdateCoupon()" id="couponForm" autocomplete="off">

                        <span class="text-danger" ng-bind-html="error_message"></span>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Centers</label>
                                    <div class="custom-input">
                                        <select class="form-control" name="centers_id" ng-model="couponObj.centers_id" multiple="true" ng-disabled="!centersList.length > 0">
                                            <option ng-repeat="item in centersList track by $index" value="{{item.id}}" >{{item.center_name}}</option>
                                        </select>
                                        <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Coupon Code</label>
                                    <input type="text" class="form-control" name="coupon_code" ng-model="couponObj.coupon_code">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Start Date</label>
                                    <input class="form-control datepicker" name="start_date" ng-model="couponObj.start_date"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>End Date</label>
                                    <input class="form-control datepicker" name="end_date" ng-model="couponObj.end_date"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Discount Type</label>
                                    <select class="form-control" name="discount_type" ng-model="couponObj.discount_type">
                                        <option value="">Choose Discount Type</option>
                                        <option value="percentage">Percentage</option>
                                        <option value="flat">Flat</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Discount Value</label>
                                    <input class="form-control" name="discount_value" ng-model="couponObj.discount_value"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Minimum Amount to Apply Discount</label>
                                    <input class="form-control" name="minimum_to_apply_discount" ng-model="couponObj.minimum_to_apply_discount"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Applicable On</label>
                                    <select class="form-control" name="applicable_on" ng-model="couponObj.applicable_on">
                                        <option value="">Choose</option>
                                        <option value="android_app">Only Android App</option>
                                        <option value="ios_app">Only iOS App</option>
                                        <option value="any_mobile_app">Any Mobile App</option>
                                        <option value="website">Website</option>
                                        <option value="all">All</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Usage Type</label>
                                    <select class="form-control" name="coupon_usage_type" ng-model="couponObj.coupon_usage_type">
                                        <option value="">Choose usage type</option>
                                        <option value="single">Single time usage</option>
                                        <option value="multiple">Multiple times usage</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6" ng-if="couponObj.coupon_usage_type == 'multiple'">
                                <div class="form-group">
                                    <label>Coupon Usage attempts</label>
                                    <input class="form-control" name="coupon_usage_attempts" ng-model="couponObj.coupon_usage_attempts"/>
                                </div>
                            </div>

                        </div>

                        <div class="text-right">
                            <button class="btn btn-default" type="reset" ng-click="ResetForm()"><b>Cancel <i class="fal fa-times"></i></b></button>
                            <button type="submit" class="btn btn-primary"><b>{{couponObj.id?'Update':'Add'}} <i class="fal fa-plus"></i></b></button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/couponsCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/couponsService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/centersService.js?r=<?= time() ?>"></script>