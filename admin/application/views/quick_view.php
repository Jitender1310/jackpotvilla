<section class="pageWrapper">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-6"><strong>Quick View</strong></div>
            <div class="col-md-6 hidden-xs">
                <ul class="actions ul">
                    <li><a href="">Graphical View</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>
        <div class="row rm-10">
            <div class="col-md-4 cp-10">
                <div class="whitebox mb-10-xs">
                    <div class="whiteboxHead">
                        <strong>Today Satistics</strong>
                    </div>
                    <ul class="ul-list-one">
                        <li>
                            Current Rooms Occupied
                            <span class="label label-info">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-danger">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-warning">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-success">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-purple">20</span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 cp-10">
                <div class="whitebox mb-10-xs">
                    <div class="whiteboxHead"><strong>Total Guests</strong></div>
                    <ul class="ul-list-one">
                        <li>
                            Current Rooms Occupied
                            <span class="label label-info">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-danger">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-warning">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-success">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-purple">20</span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 cp-10">
                <div class="whitebox mb-10-xs">
                    <div class="whiteboxHead"><strong>Today Activity</strong></div>
                    <ul class="ul-list-one">
                        <li>
                            Current Rooms Occupied
                            <span class="label label-info">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-danger">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-warning">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-success">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-purple">20</span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 cp-10">
                <div class="whitebox mb-0-md mb-10-xs">
                    <div class="whiteboxHead"><strong>Hotel Inventory</strong></div>
                    <ul class="ul-list-one">
                        <li>
                            Current Rooms Occupied
                            <span class="label label-info">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-danger">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-warning">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-success">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-purple">20</span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 cp-10">
                <div class="whitebox mb-0-md mb-10-xs">
                    <div class="whiteboxHead"><strong>Future Inventory</strong></div>
                    <ul class="ul-list-one">
                        <li>
                            Current Rooms Occupied
                            <span class="label label-info">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-danger">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-warning">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-success">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-purple">20</span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 cp-10">
                <div class="whitebox mb-0-md mb-0-xs">
                    <div class="whiteboxHead"><strong>Other</strong></div>
                    <ul class="ul-list-one">
                        <li>
                            Current Rooms Occupied
                            <span class="label label-info">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-info">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-info">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-info">20</span>
                        </li>
                        <li>
                            Current Rooms Occupied
                            <span class="label label-info">20</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk - 2017</div>
    <!-- <div class="pageSidebar">Page Sidebar</div> -->
</section>