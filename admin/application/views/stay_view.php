<section class="pageWrapper">

    <div class="pageHeader" workspace-offset >

        <div class="row">

            <div class="col-md-6"><strong>Stay View</strong></div>

            <div class="col-md-6 hidden-xs">

                <ul class="actions ul">

                    <li><a href="">Today</a></li>

                    <li><a href="">Tomorrow</a></li>

                    <li><span class="actionsDatepicker"><input type="text"  placeholder="Custom Date" class="datepicker"> <i class="fal fa-chevron-down"></i></span></li>

                </ul>

            </div>

        </div>

    </div>

    <div class="pageBody" workspace>

        <div class="table-responsive hidden-xs">

            <table class="table table-bordered table-booking table-cell-width">

                <tr>

                    <td colspan="49">

                        <table class="">

                            <tr>

                                <td class="active">Beds</td>

                                <td>Bunker Beds</td>

                                <td>Chaise Lounge</td>

                                <td>Wing Chair</td>

                                <td>Recliner</td>

                            </tr>

                        </table>

                    </td>

                </tr>

                <tr>

                    <td rowspan="2" class="rcell rcell2 fixed-col" width="60">

                        <div>Room Number</div>

                    </td>

                    <td colspan="24" class="dcell">Today - 23/12/2017  </td>

                    <td colspan="24" class="dcell">Tomorrow - 24/12/2017</td>

                </tr>

                <tr>

                    <td class="hcell adjustable-col">

                        <div>6 am</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>7 am</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>8 am</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>9 am</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>10 am</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>11 am</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>12 am</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>1 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>2 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>3 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>4 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>5 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>6 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>7 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>8 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>9 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>10 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>11 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>12 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>1 am</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>2 am</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>3 am</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>4 am</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>5 am</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>6 am</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>7 am</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>8 am</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>9 am</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>10 am</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>11 am</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>12 am</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>1 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>2 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>3 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>4 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>5 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>6 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>7 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>8 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>9 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>10 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>11 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>12 pm</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>1 am</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>2 am</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>3 am</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>4 am</div>

                    </td>

                    <td class="hcell adjustable-col">

                        <div>5 am</div>

                    </td>

                </tr>

                <tbody>

                    <tr class="hrow HoursRow">

                        <td class="rcell">101 <span class="rlabel rlabel-ready"></span></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="today" data-hour="24" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="101" data-day="tomorrow" data-hour="24" data-meridiem="pm"></td>

                    </tr>

                    <tr class="hrow HoursRow">

                        <td class="rcell">102 <span class="rlabel rlabel-cleanup"></span></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol booked" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol booked" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol booked" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol booked" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol booked" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol booked" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="today" data-hour="24" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="102" data-day="tomorrow" data-hour="24" data-meridiem="pm"></td>

                    </tr>

                    <tr class="hrow HoursRow">

                        <td class="rcell">103 <span class="rlabel rlabel-cleanup"></span></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="today" data-hour="24" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="103" data-day="tomorrow" data-hour="24" data-meridiem="pm"></td>

                    </tr>

                    <tr class="hrow HoursRow">

                        <td class="rcell">104 <span class="rlabel rlabel-ready"></span></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="today" data-hour="24" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="104" data-day="tomorrow" data-hour="24" data-meridiem="pm"></td>

                    </tr>

                    <tr class="hrow HoursRow">

                        <td class="rcell">105 <span class="rlabel rlabel-dirty"></span></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="today" data-hour="24" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="105" data-day="tomorrow" data-hour="24" data-meridiem="pm"></td>

                    </tr>

                    <tr class="hrow HoursRow">

                        <td class="rcell">106 <span class="rlabel rlabel-cleanup"></span></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="today" data-hour="24" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="106" data-day="tomorrow" data-hour="24" data-meridiem="pm"></td>

                    </tr>

                    <tr class="hrow HoursRow">

                        <td class="rcell">107 <span class="rlabel rlabel-cleanup"></span></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="today" data-hour="24" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="107" data-day="tomorrow" data-hour="24" data-meridiem="pm"></td>

                    </tr>

                    <tr class="hrow HoursRow">

                        <td class="rcell">108 <span class="rlabel rlabel-cleanup"></span></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="today" data-hour="24" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="108" data-day="tomorrow" data-hour="24" data-meridiem="pm"></td>

                    </tr>

                    <tr class="hrow HoursRow">

                        <td class="rcell">109 <span class="rlabel rlabel-ready"></span></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="today" data-hour="24" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="109" data-day="tomorrow" data-hour="24" data-meridiem="pm"></td>

                    </tr>

                    <tr class="hrow HoursRow">

                        <td class="rcell">110 <span class="rlabel rlabel-dirty"></span></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="24" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="24" data-meridiem="pm"></td>

                    </tr>
                    <tr class="hrow HoursRow">

                        <td class="rcell">110 <span class="rlabel rlabel-dirty"></span></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="24" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="24" data-meridiem="pm"></td>

                    </tr>
                    <tr class="hrow HoursRow">

                        <td class="rcell">110 <span class="rlabel rlabel-dirty"></span></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="24" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="24" data-meridiem="pm"></td>

                    </tr>

                    <tr class="hrow HoursRow">

                        <td class="rcell">110 <span class="rlabel rlabel-dirty"></span></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="24" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="24" data-meridiem="pm"></td>

                    </tr>
                    <tr class="hrow HoursRow">

                        <td class="rcell">110 <span class="rlabel rlabel-dirty"></span></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="24" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="24" data-meridiem="pm"></td>

                    </tr>
                    <tr class="hrow HoursRow">

                        <td class="rcell">110 <span class="rlabel rlabel-dirty"></span></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="24" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="24" data-meridiem="pm"></td>

                    </tr>
                    <tr class="hrow HoursRow">

                        <td class="rcell">110 <span class="rlabel rlabel-dirty"></span></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="24" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="24" data-meridiem="pm"></td>

                    </tr>
                    <tr class="hrow HoursRow">

                        <td class="rcell">110 <span class="rlabel rlabel-dirty"></span></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="today" data-hour="24" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="1" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="2" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="3" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="4" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="5" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="6" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="7" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="8" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="9" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="10" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="11" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="12" data-meridiem="am"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="13" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="14" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="15" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="16" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="17" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="18" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="19" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="20" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="21" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="22" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="23" data-meridiem="pm"></td>

                        <td class="cell HoursCol" data-type="Bunker Beds" data-room="110" data-day="tomorrow" data-hour="24" data-meridiem="pm"></td>

                    </tr>

                </tbody>

            </table>

        </div>

    </div>

    <div class="pageFooter text-capitalize" workspace-offset><span bookingInfo></span></div>

    <!-- <div class="pageSidebar">Page Sidebar</div> -->

</section>











<div class="modal fade" id="modal-booking">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Confirm Booking</h4>

            </div>

            <div class="modal-body">

                <table class="table table-bordered text-center text-capitalize mb-0-xs">

                    <tr>

                        <td colspan="2" class="h5"><span bookingType></span>, Room: <span bookingRoom></span></td>

                    </tr>

                    <tr>

                        <td>

                            <span bookingFromDay></span>

                            <span bookingFromHour></span>

                            <span bookingFromMeridiem></span>

                        </td>

                        <td>

                            <span bookingToDay></span>

                            <span bookingToHour></span>

                            <span bookingToMeridiem></span>

                        </td>

                    </tr>

                </table>

            </div>

            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

                <button type="button" class="btn btn-primary"><b>Confirm</b> <i class="glyphicon glyphicon-menu-right"></i></button>

            </div>

        </div>

    </div>

</div>





<div class="modal fade" id="modal-duplicate">

    <div class="modal-dialog modal-sm">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Oops!</h4>

            </div>

            <div class="modal-body">

                <p class="text-center my-30-xs">Already Booked This Slot!</p>

            </div>

        </div>

    </div>

</div>



<div class="modal fade" id="modal-limit">

    <div class="modal-dialog modal-sm">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Oops!</h4>

            </div>

            <div class="modal-body">

                <p class="text-center my-30-xs">Select Atleast 3 Hours!</p>

            </div>

        </div>

    </div>

</div>