<section class="pageWrapper">
    <div class="pageHeader" workspace-offset  valign-parent>
        <div class="row">
            <div class="col-md-6"><strong>Sale Cash & Card Details</strong></div>
            <div class="col-md-6 text-right">
                <div valign-holder>





                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>

        <div class="whitebox mb-0-xs">
            <div class="fit-parent">
                <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered no-wrap fit-child">
                    <tbody>
                        <tr class="bg-default">
                            <td rowspan="2" align="center" style="vertical-align: middle;"><b>S.NO</b></td>
                            <td rowspan="2" align="center" style="vertical-align: middle;"><b>DATE</b></td>
                            <td colspan="3" align="center"><b>CASH</b><span>&nbsp;</span></td>
                            <td colspan="3" align="center"><b>CARD</b></td>
                            <td colspan="3" align="center"><b>ON-LINE</b></td>
                            <td rowspan="2" align="center" style="vertical-align: middle;"><b>Grand Total</b></td>

                        </tr>
                        <tr>

                            <td align="center"><b>BED
                                    SALE</b></td>
                            <td align="center"><b>F&B/H.K
                                    SALE</b></td>
                            <td align="center"><b>TOTAL
                                    CASH</b></td>
                            <td align="center"><b>BED
                                    SALE</b></td>
                            <td align="center"><b>F&B
                                    SALE</b></td>
                            <td align="center"><b>TOTAL
                                    CARD</b></td>
                            <td align="center"><b>BED
                                    SALE</b></td>
                            <td align="center"><b>F&B
                                    SALE</b></td>
                            <td align="center"><b>TOTAL
                                    ON-LINE</b></td>

                        </tr>
                        <tr>
                            <td>1</td>
                            <td>01-09-2017</td>
                            <td align="right">3550</td>
                            <td align="right">25</td>
                            <td align="right">3575</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">3575</td>

                        </tr>
                        <tr>
                            <td>2</td>
                            <td>02-09-2017</td>
                            <td align="right">6750</td>
                            <td align="right">95</td>
                            <td align="right">6845</td>
                            <td align="right">450</td>
                            <td align="right">0</td>
                            <td align="right">450</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">7295</td>

                        </tr>
                        <tr>
                            <td>3</td>
                            <td>03-09-2017</td>
                            <td align="right">1600</td>
                            <td align="right">0</td>
                            <td align="right">1600</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">1600</td>

                        </tr>
                        <tr>
                            <td>4</td>
                            <td>04-09-2017</td>
                            <td align="right">1050</td>
                            <td align="right">350</td>
                            <td align="right">1400</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">1400</td>

                        </tr>
                        <tr>
                            <td>5</td>
                            <td>05-09-2017</td>
                            <td align="right">3500</td>
                            <td align="right">70</td>
                            <td align="right">3570</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">3570</td>

                        </tr>
                        <tr>
                            <td>6</td>
                            <td>06-09-2017</td>
                            <td align="right">700</td>
                            <td align="right">0</td>
                            <td align="right">700</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">700</td>

                        </tr>
                        <tr>
                            <td>7</td>
                            <td>07-09-2017</td>
                            <td align="right">2550</td>
                            <td align="right">125</td>
                            <td align="right">2675</td>
                            <td align="right">1599</td>
                            <td align="right">0</td>
                            <td align="right">1599</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">4274</td>

                        </tr>
                        <tr>
                            <td>8</td>
                            <td>08-09-2017</td>
                            <td align="right">2800</td>
                            <td align="right">0</td>
                            <td align="right">2800</td>
                            <td align="right">800</td>
                            <td align="right">0</td>
                            <td align="right">800</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">3600</td>

                        </tr>
                        <tr>
                            <td>9</td>
                            <td>09-09-2017</td>
                            <td align="right">4640</td>
                            <td align="right">185</td>
                            <td align="right">4825</td>
                            <td align="right">250</td>
                            <td align="right">0</td>
                            <td align="right">250</td>
                            <td align="right">1900</td>
                            <td align="right">0</td>
                            <td align="right">1900</td>
                            <td align="right">6975</td>

                        </tr>
                        <tr>
                            <td>10</td>
                            <td>10-09-2017</td>
                            <td align="right">1900</td>
                            <td align="right">100</td>
                            <td align="right">2000</td>
                            <td align="right">450</td>
                            <td align="right">0</td>
                            <td align="right">450</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">2450</td>

                        </tr>
                        <tr>
                            <td>11</td>
                            <td>11-09-2017</td>
                            <td align="right">1800</td>
                            <td align="right">70</td>
                            <td align="right">1870</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">1870</td>

                        </tr>
                        <tr>
                            <td>12</td>
                            <td>12-09-2017</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>

                        </tr>
                        <tr>
                            <td>13</td>
                            <td>13-09-2017</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>

                        </tr>
                        <tr>
                            <td>14</td>
                            <td>14-09-2017</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>

                        </tr>
                        <tr>
                            <td>15</td>
                            <td>15-09-2017</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>

                        </tr>
                        <tr>
                            <td>16</td>
                            <td>16-09-2017</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>

                        </tr>
                        <tr>
                            <td>17</td>
                            <td>17-09-2017</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>

                        </tr>
                        <tr>
                            <td>18</td>
                            <td>18-09-2017</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>

                        </tr>
                        <tr>
                            <td>19</td>
                            <td>19-09-2017</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>

                        </tr>
                        <tr>
                            <td>20</td>
                            <td>20-09-2017</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>

                        </tr>
                        <tr>
                            <td>21</td>
                            <td>21-09-2017</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>

                        </tr>
                        <tr>
                            <td>22</td>
                            <td>22-09-2017</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>

                        </tr>
                        <tr>
                            <td>23</td>
                            <td>23-09-2017</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>

                        </tr>
                        <tr>
                            <td>24</td>
                            <td>24-09-2017</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>

                        </tr>
                        <tr>
                            <td>25</td>
                            <td>25-09-2017</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>

                        </tr>
                        <tr>
                            <td>26</td>
                            <td>26-09-2017</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>

                        </tr>
                        <tr>
                            <td>27</td>
                            <td>27-09-2017</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>

                        </tr>
                        <tr>
                            <td>28</td>
                            <td>28-09-2017</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>

                        </tr>
                        <tr>
                            <td>29</td>
                            <td>29-09-2017</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>

                        </tr>
                        <tr>
                            <td>30</td>
                            <td>30-09-2017</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>
                            <td align="right">0</td>

                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2" align="right"><b>Cash Grand Total</b></td>
                            <td align="right"><b>31860</b></td>
                            <td colspan="2" align="right"><b>Card Grand Total</b></td>
                            <td align="right"><b>3549</b></td>
                            <td colspan="2" align="right"><b>On-line Grand Total</b></td>
                            <td align="right"><b>1900</b></td>
                            <td align="right"><b>37309</b></td>

                        </tr>

                    </tbody></table>
            </div>
        </div>

    </div>

    <?php $this->load->view('admin/includes/reports_menu'); ?>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk - 2017</div>
</section>