<section class="pageWrapper" ng-controller="smsTemplatesCtrl">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-12"><strong>Sms Templates</strong></div>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="GetTemplatesList()">
        <div class="responsive-table" ng-hide="sms_templatesList.length > 0">
            <div class="alert alert-danger">
                No Data found
            </div>
        </div> 

        <div class="responsive-table" ng-show="sms_templatesList.length > 0">

            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th width="180">Title</th>
                        <th>Message</th>

                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>


                    <tr ng-repeat="item in sms_templatesList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Title">{{item.title}}</td>
                        <td data-label="Message">{{item.message}}</td>

                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditTemplate(item)"><a href="#">Edit</a></li>
                                    <li ng-click="DeleteTemplate(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageSidebar">
        <form ng-submit="AddOrUpdateTemplate()" id="template_form">
            <div class="SidebarHead offset">
                {{templateObj.id?'Update':'Add'}} Sms Template
            </div>
            <div class="SidebarBody">
                <span class="text-danger" ng-bind-html="error_message"></span>

                <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" name="title" ng-model="templateObj.title">
                </div>

                <div class="form-group">
                    <label>Message</label>
                    <textarea rows="7" cols="7" class="form-control" name="message" ng-model="templateObj.message"></textarea>

                </div>
            </div>
            <div class="SidebarFooter offset">
                <div class="text-right">
                    <button class="btn btn-default" type="reset" ng-click="ResetForm()"><b>Cancel <i class="fal fa-times"></i></b></button>
                    <button type="submit" class="btn btn-primary"><b>{{templateObj.id?'Update':'Add'}} <i class="fal fa-plus"></i></b></button>
                </div>
            </div>
        </form>
    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk <?= date('Y') ?></div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/crm/smsTemplatesCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/crm/smsTemplatesService.js?r=<?= time() ?>"></script>
