
<section class="pageWrapper" ng-controller="customerContactsCtrl">
    <div class="pageHeader" workspace-offset valign-parent >
        <div class="row">
            <div class="col-md-6"><strong>Customers Mobile Numbers</strong></div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="GetCustomersList()">
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Mobile</th>
                        <th>Email</th>
                        <th>Created at</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in customersList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Mobile">{{item.mobile}}</td>
                        <td data-label="Email">{{item.customer_meta.email}}</td>
                        <td data-label="Created at">{{item.created_at}}</td>
                        <td data-label="Status">{{item.status=='1'?'Active':'Inactive'}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk <?= date('Y') ?></div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/crm/customerContactsCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/crm/customerContactsService.js?r=<?= time() ?>"></script>