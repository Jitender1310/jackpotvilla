<section class="pageWrapper" ng-controller="sendSmsCtrl">
    <div class="pageHeader" workspace-offset  valign-parent>
        <div class="row">
            <div class="col-md-6"><strong>Send Sms</strong></div>
            <div class="col-md-6">
                <div valign-holder class="text-right"></div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>
        <div class="container">
            <form ng-submit="SendSms()" id="sms_form">
                <div class="width-1000 mx-auto-xs">
                    <div class="whitebox">
                        <div class="whiteboxHead"><strong>Send Sms</strong> 
                            <span class="pull-right"><i class="fal fa-credit-card mr-10-xs"></i>Available SMS Credits : <?= is_numeric(balance()) ? number_format(balance()) : "N/a" ?></span>
                        </div>
                        <span class="text-danger" ng-bind-html="error_message"></span>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Mobile</label>
                                    <small>( Enter by Comma Separated )</small>
                                    <tags-input name="mobile" ng-model="smsObj.mobile" placeholder="Add a Mobile Numbers"></tags-input>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Templates</label>
                                    <select class="form-control" name="title" ng-model="smsObj.template_id" ng-change="GetMessage(smsObj.template_id)">
                                        <option value="">--Select Template--</option>
                                        <option value="{{item.id}}" ng-repeat="item in sms_templatesList track by $index">{{item.title}}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Message </label>
                                    <textarea cols="10" rows="10" class="form-control" name="message" ng-model="smsObj.message"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="text-right">
                            <button class="btn btn-default" type="reset" ng-click="ResetForm()"><b>Cancel <i class="fal fa-times"></i></b></button>
                            <button type="submit" class="btn btn-primary"><b class="ng-binding">Send <i class="fal fa-arrow-right"></i></b></button>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk - <?= date('Y') ?></div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/crm/sendSmsCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/crm/sendSmsService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/crm/smsTemplatesService.js?r=<?= time() ?>"></script>