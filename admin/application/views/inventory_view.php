<section class="pageWrapper">
    <div class="pageHeader" workspace-offset  valign-parent>
        <div class="row">
            <div class="col-md-6"><strong>Inventory Info</strong></div>
            <div class="col-md-6 text-right hidden-xs">
                <div valign-holder>
                    <button class="btn btn-default btn-sm"><b>Print </b> <i class="fal fa-print"></i></button>
                    <button href="#modal-update-stock" data-toggle="modal" class="btn btn-default btn-sm"><b>Update Stock</b> <i class="fal fa-pen"></i></button>
                    <button class="btn btn-default btn-sm"><b>Delete Stock</b> <i class="fal fa-times"></i></button>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>
        <div class="responsive-table">
            <table class="table table-custom depth-b-1 bg-special mb-10-xs">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Date Received</th>
                        <th>Category</th>
                        <th>Quantity</th>
                        <th class="text-right">Stock</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td data-label="Name">Water Tins</td>
                        <td data-label="Date Received">02/04/2018</td>
                        <td data-label="Category">Food</td>
                        <td data-label="Quantity">50 <i class="fal fa-arrow-right"></i> 10</td>
                        <td class="text-right" data-label="Stock"><span class="label label-success">In Stock</span></td>

                    </tr>

                </tbody>
            </table>
        </div>
        <div class="responsive-table">
            <table class="table table-custom data-table">
                <thead>
                    <tr>
                        <th>Date</th>

                        <th>Quantity</th>
                        <th>Note</th>

                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td data-label="Date Received">02/04/2018</td>


                        <td data-label="Quantity">10</td>
                        <td data-label="Note">Note Comes Here.</td>

                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="" data-toggle="dropdown" class="btn btn-default btn-sm"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clearfix"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="#modal-update-stock" data-toggle="modal"><i class="mr-10-xs fal fa-pen"></i>Update</a></li>
                                    <li><a href=""><i class="mr-10-xs fal fa-trash"></i>Delete</a></li>



                                </ul>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td data-label="Date Received">03/04/2018</td>


                        <td data-label="Quantity">10</td>
                        <td data-label="Note">Note Comes Here.</td>

                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="" data-toggle="dropdown" class="btn btn-default btn-sm"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clearfix"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="#modal-update-stock" data-toggle="modal"><i class="mr-10-xs fal fa-pen"></i>Update</a></li>
                                    <li><a href=""><i class="mr-10-xs fal fa-trash"></i>Delete</a></li>



                                </ul>
                            </span>
                        </td>
                    </tr>

                    <tr>
                        <td data-label="Date Received">04/04/2018</td>


                        <td data-label="Quantity">20</td>
                        <td data-label="Note">Note Comes Here.</td>

                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="" data-toggle="dropdown" class="btn btn-default btn-sm"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clearfix"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="#modal-update-stock" data-toggle="modal"><i class="mr-10-xs fal fa-pen"></i>Update</a></li>
                                    <li><a href=""><i class="mr-10-xs fal fa-trash"></i>Delete</a></li>



                                </ul>
                            </span>
                        </td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk - 2017</div>
</section>


<div class="modal fade" id="modal-update-stock">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Update Stock</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Type</label>
                            <select class="form-control">
                                <option>Select</option>
                                <option>Stock In</option>
                                <option>Stock Out</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Category</label>
                            <select class="form-control">
                                <option>Select</option>
                                <option>Equipment</option>
                                <option selected="">Food</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Stock Name</label>
                            <input type="text" class="form-control" disabled="" value="Water Tins">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Date</label>
                            <input type="text" class="form-control datepicker">
                        </div>
                    </div>



                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Quantity</label>
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group mb-0-xs">
                            <label>Note</label>
                            <textarea class="form-control" rows="4"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Update</button>
            </div>
        </div>
    </div>
</div>