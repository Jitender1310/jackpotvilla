<section class="pageWrapper" ng-controller="companiesCtrl">
    <div class="pageHeader" workspace-offset valign-parent>
        <div class="row">
            <div class="col-md-6">
                <strong>Organizations</strong>
            </div>
            <div class="col-md-6">
                <div valign-holder class="text-right">
                    <button type="button" class="btn btn-primary" ng-click="ShowCompanyAddForm()"><i class="fal fa-plus"></i> Add Organization</button>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="GetCompaniesList()">
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Type</th>
                        <th>Name</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Country</th>
                        <th>Mobile</th>
                        <th>Email</th>
                        <th>GST Tax Number</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in companiesList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Type">{{item.organization_type_text}}</td>
                        <td data-label="Company Name">{{item.company_name}}</td>
                        <td data-label="City">{{item.city_name}}</td>
                        <td data-label="State">{{item.state_name}}</td>
                        <td data-label="Country Name">{{item.country_name}}</td>
                        <td data-label="Mobile">{{item.contact_mobile}}</td>
                        <td data-label="Email">{{item.contact_email}}</td>
                        <td data-label="GST Number">{{item.tax_identity_number}}</td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditCompany(item)"><a href="#">Edit</a></li>
                                    <li ng-click="DeleteCompany(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk 2018</div>

    <?php $this->load->view("includes/company_add_popup") ?>

</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/companiesCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/companiesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/countriesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/organizationTypesService.js?r=<?= time() ?>"></script>