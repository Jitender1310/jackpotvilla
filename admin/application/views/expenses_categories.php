<section class="pageWrapper" ng-controller="expensesCategoriesCtrl">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-12"><strong>Expenses Categories</strong></div>
        </div>
    </div>

    <div class="pageBody" workspace ng-init="GetExpensesCategoriesList()">
        <div class="responsive-table" ng-hide="expensesCategoriesList.length > 0">
            <div class="alert alert-danger">
                No Data found
            </div>
        </div> 

        <div class="responsive-table" ng-show="expensesCategoriesList.length > 0">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="150">S.No</th>
                        <th width="350">Category Name</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in expensesCategoriesList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Expense Category">{{item.expenses_category_name}}</td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditExpenseCategory(item)"><a href="#">Edit</a></li>
                                    <li ng-click="DeleteExpenseCategory(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageSidebar">
        <form ng-submit="AddOrUpdateExpenseCategory()" id="expenses_categories_form">
            <div class="SidebarHead offset">
                {{expensesCategoryObj.id?'Update':'Add'}} Expense Category
            </div>
            <div class="SidebarBody">

                <span class="text-danger" ng-bind-html="error_message"></span>

                <div class="form-group">
                    <label>Expense Category Name</label>
                    <input type="text" class="form-control" name="expenses_category_name" ng-model="expensesCategoryObj.expenses_category_name">
                </div>
            </div>
            <div class="SidebarFooter offset">
                <div class="text-right">
                    <button class="btn btn-default" type="reset" ng-click="ResetForm()"><b>Cancel <i class="fal fa-times"></i></b></button>
                    <button type="submit" class="btn btn-primary"><b>{{expensesCategoryObj.id?'Update':'Add'}} <i class="fal fa-plus"></i></b></button>
                </div>
            </div>
        </form>
    </div>
    <footer-copy-right></footer-copy-right>
</section>

<div id="dialog-confirm" title="Confirm">
    <p>
        <span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>
        This item will be deleted. Are you sure?
    </p>
</div>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/expensesCategoriesCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/expensesCategoriesService.js?r=<?= time() ?>"></script>