<section class="pageWrapper" ng-controller="latestUpdatesCtrl">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-12"><strong>Facilities</strong></div>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="GetLatestUpdatesList()">
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th class="no-sort">Website Link</th>
                        <th class="no-sort">Text</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in latestUpdatesList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Website Link">{{item.website_link}}</td>
                        <td data-label="Text">{{item.text}}</td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditLatestUpdate(item)"><a href="#">Edit</a></li>
                                    <li ng-click="DeleteLatestUpdate(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageSidebar">
        <form ng-submit="AddOrUpdatelatestUpdates()" id="latsetUpdateForm">
            <div class="SidebarHead offset">
                {{latestUpdateObj.id?'Update':'Add'}} Update
            </div>
            <div class="SidebarBody">
                <span class="text-danger" ng-bind-html="error_message"></span>
                <div class="form-group">
                    <label>Website Link (optional)</label>
                    <input type="text" class="form-control" name="website_link" ng-model="latestUpdateObj.website_link">
                </div>
                <div class="form-group">
                    <label>Notification Text</label>
                    <textarea cols="10" rows="10" class="form-control" name="text" ng-model="latestUpdateObj.text"></textarea>
                </div>
            </div>
            <div class="SidebarFooter offset">
                <div class="text-right">
                    <button class="btn btn-default" type="reset" ng-click="ResetForm()"><b>Cancel <i class="fal fa-times"></i></b></button>
                    <button type="submit" class="btn btn-primary"><b>{{latestUpdateObj.id?'Update':'Add'}} <i class="fal fa-plus"></i></b></button>
                </div>
            </div>
        </form>
    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk 2018</div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/others/latestUpdatesCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/others/latestUpdatesService.js?r=<?= time() ?>"></script>