<section class="pageWrapper" ng-controller="whatsnewCtrl">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-12"><strong>Whats New</strong></div>
        </div>
    </div>
    <div class="pageBody" workspace ng-init="GetWhatsNewList()">
        <div class="responsive-table">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>

                        <th>Title</th>
                        <th>Website Link</th>
                        <th>Image</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in whatsnewList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>

                        <td data-label="Title">
                            {{item.title}}
                        </td>
                        <td data-label="Website Link">
                            {{item.website_link}}
                        </td>
                        <td data-label="Image">
                            <img ng-src="{{item.image}}" class="img-thumbnail" style="height: 50px"/>
                        </td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditWhatsNew(item)"><a href="#">Edit</a></li>
                                    <li ng-click="DeleteWhatsNew(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageSidebar">
        <form ng-submit="AddOrUpdateWhatsNew()" id="whatsnewForm">
            <div class="SidebarHead offset">
                {{whatsnewObj.id?'Update':'Add'}} Whats New
            </div>
            <div class="SidebarBody">
                <span class="text-danger" ng-bind-html="error_message"></span>

                <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" name="title" ng-model="whatsnewObj.title">
                </div>

                <div class="form-group">
                    <label>Website Link (optional)</label>
                    <input type="text" class="form-control" name="website_link" ng-model="whatsnewObj.website_link">
                </div>

                <div class="form-group rm-5">
                    <label  class="col-sm-3 cp-5 control-label text-left">Image</label>
                    <div class="col-sm-9 cp-5">
                        <div class="btn btn-default btn-block btn-file">
                            <input type="file" class="form-control" accept="image/x-png,image/gif,image/jpeg,image/png" maxsize="5000"
                                   ng-model="whatsnewObj.image" base-sixty-four-input>
                            <b>Browse</b> <i class="fas fa-ellipsis-h"></i>
                        </div>
                        <span ng-show="form.files.$error.maxsize">Files must not exceed 5000 KB</span>

                        <img ng-show="whatsnewObj.image.filetype"
                             ng-src='data:{{whatsnewObj.image.filetype}};base64,{{whatsnewObj.image.base64}}' style="height: 50px" />

                        <img ng-hide="whatsnewObj.image.filetype" ng-src="{{whatsnewObj.image}}" style="height: 50px"/>

                        <br/>
                        <span><font class="text-danger">Note:</font> Width:575px, height:300px</span>
                    </div>
                </div>
            </div>
            <div class="SidebarFooter offset">
                <div class="text-right">
                    <button class="btn btn-default" type="reset" ng-click="ResetForm()"><b>Cancel <i class="fal fa-times"></i></b></button>
                    <button type="submit" class="btn btn-primary"><b>{{whatsnewObj.id?'Update':'Add'}} <i class="fal fa-plus"></i></b></button>
                </div>
            </div>
        </form>
    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk 2018</div>
</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/others/whatsnewCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/others/whatsnewService.js?r=<?= time() ?>"></script>