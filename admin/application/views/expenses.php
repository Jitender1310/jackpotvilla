<section class="pageWrapper" ng-controller="expensesCtrl" ng-init="GetCentersList('<?= get_user_id() ?>'); GetCentersListForAddExpenses('<?= get_user_id() ?>'); GetCategoriesListForExpenses(); filter.finance_status = '<?= $this->input->get_post('status') ?>'; filter.expense_categories_id = '<?php
    if ($this->input->get_post('status') == 'Pending') {
        echo 8;
    }
?>';">
    <div class="pageHeader" workspace-offset valign-parent >
        <div class="row">
            <div class="col-md-7" ng-show="filter.finance_status != ''"><strong>Pending Approvals</strong></div>
            <div class="col-md-7" ng-show="filter.finance_status == ''"><strong>Expenses</strong></div>

            <div class="col-md-4 {{filter.finance_status?'pull-right':''}}" style="padding-right: 10px;">
                <div valign-holder>
                    <div class="row">   
                        <div class="col-md-8 pull-right">
                            <div class="custom-input" title="My Center">
                                <select class="form-control" ng-model="filter.centers_id" ng-disabled="!centersList.length > 0" ng-change="UpdateCenterSession('<?= get_user_id() ?>', filter.centers_id)">
                                    <option value="" selected>Please Select Center</option>
                                    <option ng-value="item.id" value="item.id" ng-repeat="item in centersList">{{item.center_name}}</option>
                                </select>
                                <span class="ci-icon">
                                    <i ng-show="centersSpinner" class="fal fa-circle-notch fa-spin"></i>
                                    <i ng-show="!centersSpinner" class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-1" ng-if="filter.finance_status == ''">
                <div valign-holder class="text-right" >
                    <button type="button" class="btn btn-primary" ng-click="ShowExpenseAddForm()"><i class="fal fa-plus"></i> Add Expense</button>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace  >
        <div >
            <div ng-if="filter.finance_status == ''">
                <div class="row rm-5 inactive" same-height-row >
                    <div class="col-md-4 col-xs-6 cp-5">
                        <div class="whitebox mb-10-xs text-center" same-height-col>
                            <strong>Today Expenses</strong>
                            <h4 class="mb-0-xs font-bold text-success"><i class="fal fa-rupee-sign"></i> {{expenses.today_expenses?expenses.today_expenses:'0'}}</h4>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-6 cp-5">
                        <div class="whitebox mb-10-xs text-center" same-height-col>
                            <strong>This Week Expenses</strong>
                            <h4 class="mb-0-xs font-bold text-success"><i class="fal fa-rupee-sign"></i> {{expenses.this_week_expenses?expenses.this_week_expenses:'0'}}</h4>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-6 cp-5">
                        <div class="whitebox mb-10-xs text-center" same-height-col>
                            <strong>This Month Expenses</strong>
                            <h4 class="mb-0-xs font-bold text-success"><i class="fal fa-rupee-sign"></i> {{expenses.this_month_expenses?expenses.this_month_expenses:'0'}}</h4>
                        </div>
                    </div>

                </div>
                <div class="row rm-5 inactive" same-height-row >
                    <div class="col-md-6 col-xs-6 cp-5">
                        <div class="whitebox px-0-xs mb-10-xs text-center" same-height-col>
                            <strong>Total Expenses</strong>
                            <h4 class="mb-0-xs font-bold text-success"><i class="fal fa-rupee-sign"></i>  {{expenses.total_expenses?expenses.total_expenses:'0'}}</h4>
                        </div>
                    </div>
                    <!--                    <div class="col-md-6 col-xs-6 cp-5">
                                            <div class="whitebox px-0-xs mb-10-xs text-center" same-height-col>
                                                <strong>Yesterday's Closing Balance & Today Opening Balance</strong>
                                                <h4 class="mb-0-xs font-bold text-success"><i class="fal fa-rupee-sign"></i>  {{opening_balance?opening_balance:'0'}}</h4>
                                            </div>
                                        </div>-->
                    <div class="col-md-6 col-xs-6 cp-5">
                        <div class="whitebox px-0-xs mb-10-xs text-center" same-height-col>
                            <strong>Current Petty Cash Balance</strong>
                            <h4 class="mb-0-xs font-bold text-success"><i class="fal fa-rupee-sign"></i>  {{balance?balance:'0'}}</h4>
                        </div>
                    </div>
                </div>
            </div>

            <div class="whitebox">
                <div class="row rm-5">
                    <form autocomplete="off" class="" ng-submit="GetExpensesList(filter.page = 1)">

                        <div class="col-md-2 cp-5">
                            <div class="custom-group">
                                <div class="custom-input">
                                    <input autocomplete="off" type="text" class="form-control datepickerNormal" placeholder="From Date" name="from_date" ng-model="filter.from_date">
                                    <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 cp-5">
                            <div class="custom-group">
                                <div class="custom-input">
                                    <input autocomplete="off" type="text" class="form-control datepickerNormal" placeholder="To Date" name="to_date" ng-model="filter.to_date">
                                    <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-2 cp-5" ng-show="filter.finance_status == ''">
                            <div class="form-group mb-0-xs"> 
                                <div class="custom-input" title="Centers">
                                    <select class="form-control" ng-model="filter.expense_categories_id" ng-disabled="!categoriesList.length > 0">
                                        <option value="" selected>All Categories</option>
                                        <option ng-value="item.id" value="item.id" ng-repeat="item in categoriesList">{{item.expenses_category_name}}</option>
                                    </select>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 cp-5 hide" ng-show="filter.finance_status == ''">
                            <div class="form-group mb-0-xs"> 
                                <div class="custom-input" title="Centers">
                                    <select class="form-control" ng-model="filter.type">
                                        <option value="" selected>All Types</option>
                                        <option value="Credit" >Credit</option>
                                        <option value="Debit" >Debit</option>
                                    </select>

                                </div>
                            </div> 
                        </div>

                        <div  class="col-md-2 cp-5 hide">
                            <div class="form-group mb-0-xs"> 
                                <div class="custom-input" title="Status">
                                    <select class="form-control" ng-model="filter.finance_status">
                                        <option value="" selected>All Status</option>
                                        <option value="Approved" >Approved</option>
                                        <option value="Pending" >Pending</option>
                                        <option value="Refused" >Refused</option>
                                    </select>
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-2 cp-5">
                            <button type="submit" class="btn btn-primary "><i class="fas fa-search"></i></button>
                            <button ng-if="hasPermission(47) && filter.finance_status == ''" type="reset" ng-click="ResetFilter()" class="btn btn-danger " title="Clear Search"><i class="fas fa-times-circle"></i></button>
                            <button type="button" class="btn btn-info" ng-click="ExportToExcel()" title="Export To Excel"><i class="far fa-download"></i> <i class="far fa-file-excel"></i></button>
                        </div>
                    </form>
                </div>

            </div>

            <div class="responsive-table" ng-hide="expensesList.length > 0">
                <div class="alert alert-danger">
                    No Data found
                </div>
            </div> 
            <div class="responsive-table" ng-show="expensesList.length > 0">
                <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions" >
                    <thead>
                        <tr>
                            <th width="120">S.No</th>
                            <th>Center</th>
                            <th>Category Name</th>
                            <th>Title</th>
                            <th width="260">Remarks</th>
                            <th>Amount (Rs/-)</th>
                            <th>Type</th>
                            <th>Date</th>
                            <th>Added By</th>
                            <th>Updated By</th>
                            <th>Status</th>
                            <th>Attachments</th>
    <!--                            <th>Approval</th>-->
                            <th class="no-sort text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="item in expensesList track by $index">
                            <td data-label="SNO">{{$index + 1}}</td>
                            <td data-label="Center">{{item.center_name}}</td>
                            <td data-label="Expense Category">{{item.expense_category}}</td>
                            <td data-label="Title">{{item.title}}</td>
                            <td data-label="Remarks">{{item.remarks}}</td>
                            <td data-label="Amount" class="text-right">{{item.amount}}</td>

                            <td data-label="Type"><span class="label label-{{item.transaction_type_bootstrap_class}}">{{item.type}}</span></td>
                            <td data-label="Date">{{item.date}}</td>

                            <td data-label="Added by">{{item.user_name1}} At {{item.added_time}}</td>
                            <td data-label="Updated by">{{item.user_name2}} {{item.updated_time? 'At ' + item.updated_time:''}}</td>
                            <td data-label="Finance status">{{item.finance_status}}</td>
                            <td data-label="Reciept">
                                <p ng-show="{{item.attachment1 != NULL && item.attachment1 != ''}}"><a  class="text-primary"  ng-href="<?= base_url() ?>uploads/expense_receipts/{{item.attachment1}}" target="_blank"><strong>Attachment1</strong></a></p>
                                <p ng-show="{{item.attachment2 != NULL && item.attachment2 != ''}}"><a  class="text-primary"  ng-href="<?= base_url() ?>uploads/expense_receipts/{{item.attachment2}}" target="_blank"><strong>Attachment2</strong></a></p>
                                <p ng-show="{{item.attachment3 != NULL && item.attachment3 != ''}}"><a  class="text-primary"  ng-href="<?= base_url() ?>uploads/expense_receipts/{{item.attachment3}}" target="_blank"><strong>Attachment3</strong></a></p>
                            </td>
    <!--                            <td data-label="Status">{{item.finance_status}}</td>-->
                            <td  data-label="Action" class="text-right" >
                                <span ng-if="item.type != 'Credit'" class="dropdown" ng-hide="item.finance_status == 'Approved' || item.finance_status == 'Refused'">
                                    <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                    <div class="clear"></div>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li ng-hide="item.finance_status == 'Approved'"  ng-click="EditExpense(item)"><a href="#">Edit</a></li>
                                        <li ng-hide="item.finance_status == 'Approved'" ng-click="DeleteExpense(item)"><a href="#">Delete</a></li>
                                    </ul>
                                </span>
                            </td>

                        </tr>
                    </tbody>
                </table>
            </div>
            <span class="pull-right" ng-bind-html="expensesPagination.pagination"></span>
        </div>
    </div>
    <div class="pageFooter" workspace-offset >&copy; Freshup Desk <?= date('Y') ?></div>

    <div class="modal fade" id="expense-modal-popup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{expensesObj.id?"Update":"Add"}} Expense</h4>
                </div>
                <div class="modal-body">
                    <form ng-submit="AddOrUpdateExpense()" id="expenses_form" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="text-danger" ng-bind-html="error_message"></span>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Center</label>
                                    <select class="form-control" name="centers_id" ng-model="expensesObj.centers_id" ng-disabled="!centersListForAddExpenses.length > 0">
                                        <option value="" selected disabled>All centers</option>
                                        <option ng-value="item.id" value="item.id" ng-repeat="item in centersListForAddExpenses">{{item.center_name}}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Expenses Category</label>

                                    <select class="form-control" name="expense_categories_id" ng-model="expensesObj.expense_categories_id" ng-disabled="!expensesCategoriesList.length > 0">
                                        <option value="" selected disabled>--Select Expenses Categoriess--</option>
                                        <option ng-value="item.id" value="item.id" ng-repeat="item in expensesCategoriesList">{{item.expenses_category_name}}</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" class="form-control" name="title" ng-model="expensesObj.title">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Amount</label>
                                    <input type="text" class="form-control number" min="1" name="amount" ng-model="expensesObj.amount">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Date</label>
                                    <input autocomplete="off" type="text" class="form-control datepickerNormal" onkeydown="event.preventDefault()" name="date" ng-model="expensesObj.date">
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Remarks</label>
                                    <textarea rows="4" cols="3" class="form-control" name="remarks" ng-model="expensesObj.remarks"></textarea>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label> Attachment 1 </label>
                                    <div class="input-group">
                                        <input type="file" class="form-control"  name="attachment1"  id="attachment1">
                                        <div>Image,Pdf, Docx, Doc formats allowed here</div>
                                        <a ng-show="expensesObj.id && expensesObj.attachment1 != NULL" class="text-primary"  ng-href="<?= base_url() ?>uploads/expense_receipts/{{expensesObj.attachment1}}" target="_blank"><strong>Previous Attachment1</strong></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label> Attachment 2 </label>
                                    <div class="input-group">
                                        <input type="file" class="form-control"  id="attachment2">
                                        <div>Image,Pdf, Docx, Doc formats allowed here</div>
                                        <a ng-show="expensesObj.id && expensesObj.attachment2 != ''" class="text-primary"  ng-href="<?= base_url() ?>uploads/expense_receipts/{{expensesObj.attachment2}}" target="_blank"><strong>Previous attachment 2</strong></a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label> Attachment 3 </label>
                                    <div class="input-group">
                                        <input type="file" class="form-control"   id="attachment3">
                                        <div>Image,Pdf, Docx, Doc formats allowed here</div>
                                        <a ng-show="expensesObj.id && expensesObj.attachment3 != ''" class="text-primary"  ng-href="<?= base_url() ?>uploads/expense_receipts/{{expensesObj.attachment3}}" target="_blank"><strong>Previous attachment 3</strong></a>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div  class="row <?php
                            if ($this->session->userdata('role_id') == 25 || $this->session->userdata('role_id') == 12) {
                                echo "";
                            } else {
                                echo"hide";
                            }
                        ?>" >
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Finanace Dept. Approval Status</label>
                                    <select class="form-control" name="finance_status" ng-model="expensesObj.finance_status">
                                        <option value="" selected>Select Status</option>
                                        <option value="Approved">Approved</option>
                                        <option value="Pending">Pending</option>
                                        <option value="Refused">Refused</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 pull-right">
                                <button class="btn btn-default  " ng-click="ResetForm()" data-dismiss="modal"><b>Cancel <i class="fal fa-times"></i></b></button>
                                <button class="btn btn-primary pull-right" type="submit"><b>{{expensesObj.id?"Update":"Add"}} <i class="fal fa-arrow-right"></i></b></button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</section>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/expensesCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/expensesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/expensesCategoriesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/centersService.js?r=<?= time() ?>"></script>
