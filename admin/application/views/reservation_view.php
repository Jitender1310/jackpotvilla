<style>
    .webcam-live{
        width:100%;
        height: auto;
    }
</style>
<section class="pageWrapper" ng-controller="reservationViewCtrl" ng-init="reservations_id = '<?= $this->input->get_post('booking_id') ?>'; GetReservationDetails(); SetUserId('<?= get_user_id() ?>'); user_role_id =<?= get_user_role_id() ?>; minimum_payment_percentage =<?= MINIMUM_PAYMENT_PERCENT ?>">
    <div class="pageHeader" workspace-offset >
        <div class="row">
            <div class="col-md-4">
                <strong>Reservation ID # {{reservationObj.booking_ref_number}}</strong>
            </div>
            <div class="col-md-4 text-center">
                <strong>Center : {{reservationObj.center_info[0].center_name}} [{{reservationObj.center_info[0].center_code}}]</strong>
            </div>
            <div class="col-md-4 text-right">
                <strong>Booking created at {{reservationObj.booking_created_at}}</strong>
                <ul class="actions ul">
                    <!-- <li><a href="">Graphical View</a></li>  -->
                </ul>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>
        <div class="grid sameheight001">
            <div class="col">
                <div class="whitebox mb-10-xs pa-0-xs">
                    <div class="whiteboxHead ma-0-xs">
                        <strong>Customer Information</strong>
                        <span class="dropdown" ng-show="reservationObj.has_permission_to_update_customer_information">
                            <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a ng-click="EditCustomerInfo()" style="cursor: pointer">Edit Customer Information</a></li>
                            </ul>
                        </span>
                    </div>
                    <table class="table table-custom mb-0-xs">
                        <tbody>
                            <tr>
                                <td><b>Mobile</b></td>
                                <td>{{reservationObj.contact_mobile}}</td>
                            </tr>
                            <tr>
                                <td><b>Name</b></td>
                                <td>{{reservationObj.contact_name}}</td>
                            </tr>
                            <tr>
                                <td><b>Email</b></td>
                                <td>{{reservationObj.contact_email}}</td>
                            </tr>
                            <tr>
                                <td><b>Alternate Mobile</b></td>
                                <td>{{reservationObj.alternative_mobile?reservationObj.alternative_mobile:'N/a'}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col">
                <div class="whitebox mb-10-xs pa-0-xs">
                    <div class="whiteboxHead ma-0-xs">
                        <strong>Stay Information</strong>
                        <?php /* if (permission_required(41, 'edit', get_user_id(), true)) { ?>
                          <span class="dropdown" ng-hide="reservationObj.check_in_data_taken_users_id > 0 || reservationObj.booking_status == 'Cancelled'">
                          <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                          <ul class="dropdown-menu dropdown-menu-right">
                          <li><a ng-click="EditStayInfo()" style="cursor: pointer">Edit Check-in Time</a></li>
                          </ul>
                          </span>
                          <?php } */ ?>
                    </div>
                    <table class="table table-custom mb-0-xs">
                        <tbody>
                            <tr>
                                <td><b>Check-in</b></td>
                                <td>
                                    <div ng-show="reservationObj.has_do_check_in_permission || (reservationObj.check_in_data_taken_users_id > 0)">
                                        <span ng-show="reservationObj.check_in_data_taken_users_id > 0">{{reservationObj.check_in_details.customer_checked_in_at}} </span>
                                        <button type="button" ng-click="ShowCheckInPopUp()" class="btn btn-primary btn-sm"
                                                ng-hide="reservationObj.check_in_data_taken_users_id > 0"><b>Check In</b><i class="fal fa-sign-in ml-10-xs"></i></button>
                                    </div>

                                    <!--<div ng-hide="reservationObj.has_do_check_in_permission && reservationObj.check_in_details.customer_checked_in_at !== null">
                                        --
                                    </div>-->
                                </td>
                            </tr>
                            <tr>
                                <td><b>Check-out</b></td>
                                <td>
                                    <div ng-show="reservationObj.has_do_check_out_permission">
                                        <span ng-show="reservationObj.check_out_data_taken_users_id > 0">{{reservationObj.check_out_details.customer_checked_out_at}} </span>
                                        <button type="button" data-target="#modal-checkout" data-toggle="modal"
                                                class="btn btn-primary btn-sm" ng-hide="reservationObj.check_out_data_taken_users_id > 0"><b>Check Out</b><i class="fal fa-sign-out ml-10-xs"></i></button>
                                    </div>
                                    <div ng-hide="reservationObj.has_do_check_out_permission">
                                        --
                                    </div>
                                </td>
                            </tr>
                            <tr ng-show="reservationObj.check_in_data_taken_users_id > 0">
                                <td><b>Spent Duration</b>
                                    <br/><small>(Check Out - Actual Check in Time)</small></td>
                                <td><span class="text-{{reservationObj.spent_duration_bootstrap_class}}">{{reservationObj.spent_duration}}</span></td>
                            </tr>
                            <tr>
                                <td><b>Total Members</b></td>
                                <td>{{reservationObj.persons}}</td>
                            </tr>
                            <tr>
                                <td>Actual Check in time</td>
                                <td>{{reservationObj.check_in_date}} {{reservationObj.check_in_time}}</td>
                            </tr>
                            <tr>
                                <td>Actual Check out time</td>
                                <td>{{reservationObj.check_out_date}} {{reservationObj.check_out_time}}</td>
                            </tr>
                            <tr>
                                <td>Actual Duration</td>
                                <td>{{reservationObj.duration}} {{reservationObj.duration_type_text}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col">
                <div class="whitebox mb-10-xs pa-0-xs">
                    <div class="whiteboxHead ma-0-xs">
                        <strong>Other Information</strong>
                        <!--<span class="dropdown" ng-show="reservationObj.has_permission_to_update_other_information">-->
                        <span class="dropdown">
                            <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a style="cursor: pointer" ng-click="EditOtherInfo()">Edit</a></li>
                            </ul>
                        </span>
                    </div>
                    <table class="table table-custom mb-0-xs">
                        <tbody>
                            <tr>
                                <td><b>Reservation Type</b></td>
                                <td class="text-right"><span class="label label-{{reservationObj.staying_type_bootstrap_class}}">{{reservationObj.stay_type_text}}</span></td>
                            </tr>
                            <tr>
                                <td><b>Payment Status</b></td>
                                <td class="text-right"><span class="label label-{{reservationObj.payment_status_bootstrap_class}}">{{reservationObj.payment_status}}</span></td>
                            </tr>
                            <tr>
                                <td><b>Reservation Status</b></td>
                                <td class="text-right"><span class="label label-{{reservationObj.booking_status_bootstrap_class}}">{{reservationObj.booking_status}}</span></td>
                            </tr>
                            <tr>
                                <td><b>Business Source</b></td>
                                <td>{{reservationObj.business_sources_text?reservationObj.business_sources_text:'N/a'}}</td>
                            </tr>
                            <tr>
                                <td><b>Agent</b></td>
                                <td>
                                    {{reservationObj.agent_info.agent_name?reservationObj.agent_info.agent_name+' '+reservationObj.agent_info.mobile:'N/a'}}
                                </td>
                            </tr>
                            <tr>
                                <td><b>Agent Commission</b></td>
                                <td>{{reservationObj.agent_commission_percentage?reservationObj.agent_commission_percentage+"%":'N/a'}}</td>
                            </tr>
                            <tr>
                                <td><b>How did you know about us</b></td>
                                <td>{{reservationObj.how_did_hear_about_us_text?reservationObj.how_did_hear_about_us_text:'N/a'}}</td>
                            </tr>

                            <tr ng-if="reservationObj.booked_by_users_name != ''">
                                <td><b>Booked By</b></td>
                                <td>{{reservationObj.booked_by_users_name?reservationObj.booked_by_users_name:'N/a'}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col">
                <div class="whitebox mb-10-xs">
                    <div class="whiteboxHead">
                        <strong>Photos & Proofs</strong>
                        <span class="dropdown hide">
                            <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="">Edit</a></li>
                            </ul>
                        </span>
                    </div>
                    <div class="text-center proofs" ng-hide="reservationObj.check_in_data_taken_users_id > 0">
                        <h4>Awaiting for customer Check In</h4>
                    </div>
                    <div class="text-center proofs" ng-show="reservationObj.check_in_data_taken_users_id > 0">
                        <img ng-if="reservationObj.check_in_details.captured_image != ''" class="img-thumbnail" ng-src="{{reservationObj.check_in_details.captured_image}}" alt="Image">
                        <div ng-if="reservationObj.check_in_details.captured_image != ''"><a href="{{reservationObj.check_in_details.captured_image}}" download target="_blank" class="btn btn-default btn-sm">Download Image</a></div>
                        <div><b>Arrived at:</b> {{reservationObj.check_in_details.customer_checked_in_at}}</div>
                        <div><b>{{reservationObj.check_in_details.proof_type}}:</b> {{reservationObj.check_in_details.proof_number}}</div>
                        <div><a href="{{reservationObj.check_in_details.proof_image}}" download target="_blank" class="btn btn-default btn-sm">Download Proof</a></div>
                    </div>


                </div>
            </div>
        </div>
        <div class="row rm-10">
            <div class="col-md-12 cp-10">
                <div class="whitebox pa-0-xs mb-10-xs" >
                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul role="tablist">
                            <li role="presentation" class="active">
                                <a href="#genral_information" aria-controls="genral_information" role="tab" data-toggle="tab">general information</a>
                            </li>
                            <!--<li role="presentation">
                                <a href="#room-charges" aria-controls="room-charges" role="tab" data-toggle="tab">room charges</a>
                            </li>
                            <li role="presentation">
                                <a href="#fnb-charges" aria-controls="fnb-charges" role="tab" data-toggle="tab">F & B Charges</a>
                            </li>
                            <li role="presentation">
                                <a href="#folio-details" aria-controls="folio-details" role="tab" data-toggle="tab">folio details</a>
                            </li>-->
                            <li role="presentation">
                                <a href="#payment-history" aria-controls="payment-history" role="tab" data-toggle="tab">Payment History</a>
                            </li>
                            <?php if (has_permission(45, "view")) { ?>
                                <li role="presentation">
                                    <a href="#activity-log" aria-controls="activity-log" role="tab" data-toggle="tab">Activity Log</a>
                                </li>
                            <?php } ?>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content px-20-xs py-20-xs">
                            <div role="tabpanel" class="tab-pane active" id="genral_information">
                                <div class="row rm-10" >
                                    <div class="col-md-4 cp-10">
                                        <div class="whitebox bordered mb-10-xs form-horizontal" >
                                            <div class="whiteboxHead">
                                                <strong>Billing Information</strong>
                                                <!--<span class="dropdown" ng-show="reservationObj.has_permission_to_update_billing_information">-->
                                                <span class="dropdown">
                                                    <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a ng-click="EditBillingInfo()" style="cursor: pointer">Edit Billing Information</a></li>
                                                    </ul>
                                                </span>
                                            </div>
                                            <div class="row rm-5">

                                                <table class="table table-bordered">
                                                    <tbody>
                                                        <tr ng-if="reservationObj.billing_to == 'Organization'">
                                                            <th>Billing To</th>
                                                            <td>{{reservationObj.billing_to}}</td>
                                                        </tr>
                                                        <tr ng-if="reservationObj.billing_to == 'Organization'">
                                                            <th>Organization Name</th>
                                                            <td>{{reservationObj.company_name}}</td>
                                                        </tr>
                                                        <tr ng-if="reservationObj.billing_to == 'Customer'">
                                                            <th>Billing To</th>
                                                            <td>{{reservationObj.billing_to}}</td>
                                                        </tr>
                                                        <tr ng-if="reservationObj.billing_to == 'Others'">
                                                            <th>Billing To</th>
                                                            <td>{{reservationObj.billing_to_others_text}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>​Door No</th>
                                                            <td>{{reservationObj.door_no}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Street Name</th>
                                                            <td>{{reservationObj.street_name}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Area/Location</th>
                                                            <td>{{reservationObj.location}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>City</th>
                                                            <td>{{reservationObj.city}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>District</th>
                                                            <td>{{reservationObj.district}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>State</th>
                                                            <td>{{reservationObj.state}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Pin code</th>
                                                            <td>{{reservationObj.zipcode}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Country</th>
                                                            <td>{{reservationObj.countries_name}}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 cp-10">
                                        <div class="whitebox bordered mb-10-xs pa-0-xs form-horizontal"  ng-repeat="booking_items in reservationObj.booking_items track by $index">
                                            <div class="whiteboxHead ma-0-xs"><strong ng-bind="booking_items.stay_category_name"></strong>
                                                <span ng-show="reservationObj.booking_status != 'Completed' && reservationObj.booking_status != 'Cancelled'"><button type="button" class="btn btn-default btn-sm" ng-click="ShowGuestDetailsEditPopUp(reservationObj.booking_items)">Modify Guest Details</button></span>
                                                <?php /* <span><a href="#modal-add-profile" data-toggle="modal" class="btn btn-default btn-sm"><i class="fal fa-plus"></i></a></span> */ ?>
                                            </div>
                                            <table class="table table-custom">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Gender(age)</th>
                                                        <th>Proof</th>
                                                        <th class="hide">Unit Price</th>
                                                        <th>Total Price</th>
                                                        <th>SKU</th>
                                                        <th>Status</th>
                                                        <th class="no-sort text-right">Action {{booking_items.item}}</th>
                                                    </tr>
                                                </thead>
                                                <tbody ng-repeat="item in booking_items.items track by $index" >
                                                    <tr>
                                                        <td>{{item.person_name}}</td>
                                                        <td>{{item.gender}} ({{item.age?item.age:'n/a'}})</td>
                                                        <td>{{item.identity_type}} <br/>({{item.identity_number}})
                                                            <a ng-href="{{item.identity_image}}" target="_blank" ng-show="item.identity_image" title="View/Download Proof"><br/>View Proof</a>
                                                        </td>
                                                        <td class="hide">{{item.unit_price}}</td>
                                                        <td>{{item.total_price}}</td>
                                                        <td>{{item.inventory_code}}</td>
                                                        <td>{{item.person_status}}
                                                            <br/><small>{{item.customer_depatured_at_readable}}</small>
                                                        </td>
                                                        <td class="text-right">
                                                            <span ng-show="reservationObj.check_in_data_taken_users_id">
                                                                <span class="dropdown" ng-hide="item.check_out_data_taken_users_id && item.check_in_data_taken_users_id">
                                                                    <a href="" data-toggle="dropdown" class="btn btn-default btn-sm"><i class="fas fa-ellipsis-h"></i></a>
                                                                    <div class="clearfix"></div>
                                                                    <ul class="dropdown-menu dropdown-menu-right" >
                                                                        <li ng-hide="item.check_out_data_taken_users_id">
                                                                            <a href="" ng-click="MarkAsCheckoutForSinglePerson(item, booking_items.stay_category_name)">Check Out</a>
                                                                        </li>
                                                                    </ul>
                                                                </span>
                                                            </span>
                                                            <span  ng-show="item.check_out_data_taken_users_id && item.check_in_data_taken_users_id || !reservationObj.check_in_data_taken_users_id">
                                                                --
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr ng-repeat="sub_item in item.sub_reservation_items track by $index">
                                                        <td>{{sub_item.person_name}}<br/>
                                                            {{sub_item.mobile_number}}</td>
                                                        <td>{{sub_item.gender}} ({{sub_item.age?sub_item.age:'n/a'}})</td>
                                                        <td>{{sub_item.identity_type}} <br/>({{sub_item.identity_number}})
                                                            <a ng-href="{{sub_item.identity_image}}" target="_blank" ng-show="sub_item.identity_image" title="View/Download Proof"><br/>View Proof</a>
                                                        </td>
                                                        <td class="hide">{{sub_item.unit_price}}</td>
                                                        <td>{{sub_item.total_price}}</td>
                                                        <td>{{item.inventory_code}}</td>
                                                        <td>{{sub_item.person_status}}
                                                            <br/><small>{{sub_item.customer_depatured_at_readable}}</small>
                                                        </td>
                                                        <td class="text-right">
                                                            <span ng-show="reservationObj.check_in_data_taken_users_id">
                                                                <span class="dropdown" ng-hide="sub_item.check_out_data_taken_users_id && sub_item.check_in_data_taken_users_id">
                                                                    <a href="" data-toggle="dropdown" class="btn btn-default btn-sm"><i class="fas fa-ellipsis-h"></i></a>
                                                                    <div class="clearfix"></div>
                                                                    <ul class="dropdown-menu dropdown-menu-right" >
                                                                        <li ng-hide="sub_item.check_out_data_taken_users_id">
                                                                            <a href="" ng-click="MarkAsCheckoutForSinglePerson(item, booking_items.stay_category_name)">Check Out</a>
                                                                        </li>
                                                                    </ul>
                                                                </span>
                                                            </span>
                                                            <span  ng-show="sub_item.check_out_data_taken_users_id && sub_item.check_in_data_taken_users_id || !reservationObj.check_in_data_taken_users_id">
                                                                --
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>
                                        <?php

                                        function payments_table() { ?>
                                            <table class="table table-custom table-custom-px-0 width-300 pull-right mb-0-xs mr-20-xs">
                                                <tr>
                                                    <td class="brt-0"><strong>Total</strong></td>
                                                    <td class="brt-0 text-right">Rs {{reservationObj.payment_info.amount}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Discount ({{(reservationObj.payment_info.discount_type == 'flat') ? reservationObj.payment_info.discount_type_symbol+'. ':''}}{{reservationObj.payment_info.discount_value}}{{(reservationObj.payment_info.discount_type == 'percentage') ? ' '+reservationObj.payment_info.discount_type_symbol:''}})</strong></td>
                                                    <td class="text-right">Rs {{reservationObj.payment_info.discount_amount}}</td>
                                                </tr>
                                                <tr ng-if="reservationObj.payment_info.coupon_code">
                                                    <td><strong>Coupon Discount ({{(reservationObj.payment_info.coupon_discount_type == 'flat') ? reservationObj.payment_info.coupon_discount_type_symbol+'. ':''}}{{reservationObj.payment_info.coupon_discount_amount}}{{(reservationObj.payment_info.coupon_discount_type == 'percentage') ? ' '+reservationObj.payment_info.coupon_discount_type_symbol:''}})</strong></td>
                                                    <td class="text-right">Rs {{reservationObj.payment_info.applied_coupon_discount_amount}}</td>
                                                </tr>
                                                <tr>
                                                    <td ><strong>Sub Total</strong></td>
                                                    <td class="text-right">Rs {{reservationObj.payment_info.sub_total||0}}</td>
                                                </tr>
                                                <tr ng-repeat="tax_item in reservationObj.payment_info.applied_taxes">
                                                    <td><strong ng-bind="tax_item.tax_name"></strong> ({{tax_item.tax_amount}} {{tax_item.tax_calculation_type=='Percentage'?'%':'Rs'}})</td>
                                                    <td class="text-right">Rs {{tax_item.applied_tax_amount}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Grand Total</strong></td>
                                                    <td class="text-right">Rs {{reservationObj.payment_info.grand_total}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Paid Amount</strong></td>
                                                    <td class="text-right">Rs {{reservationObj.payment_info.payments_total||0}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Amount Due</strong></td>
                                                    <td class="text-right">Rs {{reservationObj.payment_info.amount_due|currency:''}}</td>
                                                </tr>
                                            </table>
                                            <?php
                                        }

                                        payments_table()
                                        ?>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!--           <div class="col-md-4 cp-10">
                                                  <div class="whitebox bordered mb-10-xs pa-0-xs form-horizontal" >
                                                      <div class="whiteboxHead ma-0-xs"><strong>Kitchen Order Ticket </strong>
                                                          <span><a href="#modal-kot" data-toggle="modal" class="btn btn-default btn-sm"><i class="fal fa-plus"></i></a></span>
                                                      </div>
                                                      <table class="table table-custom">
                                                          <thead>
                                                              <tr>
                                                                  <th>Item</th>
                                                                  <th>Qty</th>
                                                                  <th class="no-sort text-right">Action</th>
                                                              </tr>
                                                          </thead>
                                                          <tbody>
                                                              <tr>
                                                                  <td>Cool Drinks</td>
                                                                  <td>2</td>
                                                                  <td class="text-right"><span class="dropdown">
                                                                          <a href="" data-toggle="dropdown" class="btn btn-default btn-sm"><i class="fas fa-ellipsis-h"></i></a>
                                                                          <div class="clearfix"></div>
                                                                          <ul class="dropdown-menu dropdown-menu-right">
                                                                              <li><a href="">Option One</a></li>
                                                                              <li><a href="">Option Tow</a></li>
                                                                              <li><a href="">Option Three</a></li>
                                                                          </ul>

                                                                      </span></td>
                                                              </tr>
                                                              <tr>
                                                                  <td>Breakfast</td>
                                                                  <td>2</td>
                                                                  <td class="text-right"><span class="dropdown">
                                                                          <a href="" data-toggle="dropdown" class="btn btn-default btn-sm"><i class="fas fa-ellipsis-h"></i></a>
                                                                          <div class="clearfix"></div>
                                                                          <ul class="dropdown-menu dropdown-menu-right">
                                                                              <li><a href="">Option One</a></li>
                                                                              <li><a href="">Option Tow</a></li>
                                                                              <li><a href="">Option Three</a></li>
                                                                          </ul>

                                                                      </span></td>
                                                              </tr>
                                                          </tbody>
                                                      </table>
                                                  </div>
                                              </div> -->
                                </div>


                                <div class="clearfix"></div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="room-charges">
                                <div class="responsive-table">
                                    <table class="table table-custom table-custom-px-0 mb-0-xs">
                                        <thead class="hidden-xs">
                                            <tr>
                                                <th>Date</th>
                                                <th>Room</th>
                                                <th>Rate Type</th>
                                                <th>Pax(A/C)</th>
                                                <th>Charge (Rs)</th>
                                                <th>Disc (Rs)</th>
                                                <th>Tax (Rs)</th>
                                                <th>Adj (Rs)</th>
                                                <th>Net Amt (Rs)</th>
                                                <th>User</th>
                                                <th class="text-right">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td data-label="Date">2017-12-29  Fri</td>
                                                <td data-label="Room">QUE</td>
                                                <td data-label="Rate Type">Room Only</td>
                                                <td data-label="Pax(A/C)">2/2</td>
                                                <td data-label="Charge (Rs)">100.00</td>
                                                <td data-label="Disc (Rs)">0.00</td>
                                                <td data-label="Tax (Rs)">0.00</td>
                                                <td data-label="Adj (Rs)">0.00</td>
                                                <td data-label="Net Amt (Rs)">100.00</td>
                                                <td data-label="User">admin</td>
                                                <td data-label="Actions" class="text-right">
                                                    <span class="dropdown">
                                                        <a href="" data-toggle="dropdown" class="btn btn-default btn-sm"><i class="fas fa-ellipsis-h"></i></a>
                                                        <div class="clearfix"></div>
                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                            <li><a href="">Option One</a></li>
                                                            <li><a href="">Option Tow</a></li>
                                                            <li><a href="">Option Three</a></li>
                                                        </ul>

                                                    </span>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td data-label="Date">2017-12-29  Fri</td>
                                                <td data-label="Room">QUE</td>
                                                <td data-label="Rate Type">Room Only</td>
                                                <td data-label="Pax(A/C)">2/2</td>
                                                <td data-label="Charge (Rs)">100.00</td>
                                                <td data-label="Disc (Rs)">0.00</td>
                                                <td data-label="Tax (Rs)">0.00</td>
                                                <td data-label="Adj (Rs)">0.00</td>
                                                <td data-label="Net Amt (Rs)">100.00</td>
                                                <td data-label="User">admin</td>
                                                <td data-label="Actions" class="text-right">      <span class="dropdown">
                                                        <a href="" data-toggle="dropdown" class="btn btn-default btn-sm"><i class="fas fa-ellipsis-h"></i></a>
                                                        <div class="clearfix"></div>
                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                            <li><a href="">Option One</a></li>
                                                            <li><a href="">Option Tow</a></li>
                                                            <li><a href="">Option Three</a></li>
                                                        </ul>

                                                    </span></td>
                                            </tr>
                                            <tr>
                                                <td data-label="Date">2017-12-29  Fri</td>
                                                <td data-label="Room">QUE</td>
                                                <td data-label="Rate Type">Room Only</td>
                                                <td data-label="Pax(A/C)">2/2</td>
                                                <td data-label="Charge (Rs)">100.00</td>
                                                <td data-label="Disc (Rs)">0.00</td>
                                                <td data-label="Tax (Rs)">0.00</td>
                                                <td data-label="Adj (Rs)">0.00</td>
                                                <td data-label="Net Amt (Rs)">100.00</td>
                                                <td data-label="User">admin</td>
                                                <td data-label="Actions" class="text-right">      <span class="dropdown">
                                                        <a href="" data-toggle="dropdown" class="btn btn-default btn-sm"><i class="fas fa-ellipsis-h"></i></a>
                                                        <div class="clearfix"></div>
                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                            <li><a href="">Option One</a></li>
                                                            <li><a href="">Option Tow</a></li>
                                                            <li><a href="">Option Three</a></li>
                                                        </ul>

                                                    </span></td>
                                            </tr>
                                            <tr>
                                                <td data-label="Date">2017-12-29  Fri</td>
                                                <td data-label="Room">QUE</td>
                                                <td data-label="Rate Type">Room Only</td>
                                                <td data-label="Pax(A/C)">2/2</td>
                                                <td data-label="Charge (Rs)">100.00</td>
                                                <td data-label="Disc (Rs)">0.00</td>
                                                <td data-label="Tax (Rs)">0.00</td>
                                                <td data-label="Adj (Rs)">0.00</td>
                                                <td data-label="Net Amt (Rs)">100.00</td>
                                                <td data-label="User">admin</td>
                                                <td data-label="Actions" class="text-right">      <span class="dropdown">
                                                        <a href="" data-toggle="dropdown" class="btn btn-default btn-sm"><i class="fas fa-ellipsis-h"></i></a>
                                                        <div class="clearfix"></div>
                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                            <li><a href="">Option One</a></li>
                                                            <li><a href="">Option Tow</a></li>
                                                            <li><a href="">Option Three</a></li>
                                                        </ul>

                                                    </span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="payment-history">

                                <div class="row" ng-if="(reservationObj.payment_info.grand_total - reservationObj.payment_info.payments_total) > 0 && reservationObj.booking_status != 'Cancelled'">
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-primary  pull-right" ng-click="ShowPaymentForm()">Add Payment</button>
                                    </div>
                                </div>

                                <br/>

                                <div class="responsive-table">
                                    <table class="table table-custom table-bordered table-custom-px-0 mb-0-xs">
                                        <thead class="hidden-xs">
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Date</th>
                                                <th>Payment Mode</th>
                                                <th>Card/Bank Ac Number/Cheque Number</th>
                                                <th>Card Transaction ID / Bank Ref Number</th>
                                                <th>Remark</th>
                                                <th class="text-right" style="padding-right:16px">Amount (Rs)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="payment_item in reservationObj.payment_info.payments_list track by $index">
                                                <td style="padding-left:16px">{{$index + 1}}</td>
                                                <td>{{payment_item.created_date}}</td>
                                                <td>{{payment_item.payment_method}}</td>
                                                <td>
                                                    {{payment_item.card_number}}
                                                    {{payment_item.cheque_number}}
                                                    {{payment_item.bank_account_number}}

                                                    {{!payment_item.card_number && !payment_item.cheque_number && !payment_item.bank_account_number ? "--" : ""}}
                                                </td>
                                                <td>
                                                    {{payment_item.transaction_number ?payment_item.transaction_number :'--'}}
                                                </td>
                                                <td>{{payment_item.remarks ?payment_item.remarks :'--'}}</td>
                                                <td class="text-right" style="padding-right:16px">{{payment_item.amount}}</td>
                                            </tr>
                                            <tr ng-show="reservationObj.payment_info.payments_list.length == 0">
                                                <td colspan="7">
                                                    <h4 class="text-center">No Payments history found</h4>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <!-- <h4 class="text-right">Total Paid: {{reservationObj.payment_info.payments_total}}</h4>-->

                                    <?php payments_table() ?>

                                </div>

                            </div>


                            <div role="tabpanel" class="tab-pane" id="activity-log">

                                <div class="row" ng-if="(reservationObj.payment_info.grand_total - reservationObj.payment_info.payments_total) > 0 && reservationObj.booking_status != 'Cancelled'">
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-primary  pull-right" ng-click="ShowPaymentForm()">Add Payment</button>
                                    </div>
                                </div>

                                <br/>

                                <div class="responsive-table">
                                    <table class="table table-custom table-bordered table-custom-px-0 mb-0-xs">
                                        <thead class="hidden-xs">
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Component</th>
                                                <th>Comment</th>
                                                <th>Activity Time</th>
                                                <th>Source</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="actvity in reservationObj.activity_tracker track by $index">
                                                <td style="padding-left:16px">{{$index + 1}}</td>
                                                <td>{{actvity.component}}</td>
                                                <td>{{actvity.comment}}</td>
                                                <td>
                                                    {{actvity.activity_start_time}}
                                                </td>
                                                <td>
                                                    {{actvity.activity_source}}
                                                </td>
                                            </tr>
                                            <tr ng-show="reservationObj.activity_tracker.length == 0">
                                                <td colspan="6">
                                                    <h4 class="text-center">No Track history found</h4>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>

                            </div>


                            <div role="tabpanel" class="tab-pane" id="folio-details">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-horizontal">
                                            <div class="row">
                                                <label class="col-md-2 control-label">Date</label>
                                                <div class="col-md-10 mb-10-xs">
                                                    <div class="custom-input">
                                                        <input type="text" class="form-control">
                                                        <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-md-2 control-label">Rec/Vou #</label>
                                                <div class="col-md-10 mb-10-xs">
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-horizontal">
                                            <div class="row">
                                                <label class="col-md-2 control-label">Type</label>
                                                <div class="col-md-10">
                                                    <div class="row rm-5">

                                                        <div class="col-md-6 mb-10-xs cp-5">
                                                            <div class="custom-input">
                                                                <select class="form-control">

                                                                </select>
                                                                <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 mb-10-xs cp-5">
                                                            <div class="custom-input">
                                                                <select class="form-control">

                                                                </select>
                                                                <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-md-2 control-label">Amount</label>
                                                <div class="col-md-10">
                                                    <div class="row rm-5">

                                                        <div class="col-md-6 mb-10-xs cp-5">
                                                            <div class="custom-input">
                                                                <select class="form-control">

                                                                </select>
                                                                <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 mb-10-xs cp-5">
                                                            <input type="text" class="form-control">
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-horizontal">
                                            <div class="row">
                                                <label class="col-md-2 control-label">Folio</label>
                                                <div class="col-md-10 mb-10-xs">
                                                    <div class="custom-input">
                                                        <select class="form-control">

                                                        </select>
                                                        <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-md-2 control-label">Comments</label>
                                                <div class="col-md-10 mb-10-xs">
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-right mb-10-xs"><button type="button" class="btn btn-primary">Add</button></div>


                                <div class="responsive-table">
                                    <table class="table table-custom table-custom-px-0 mb-0-xs">
                                        <thead class="hidden-xs">
                                            <tr>
                                                <th>Date</th>
                                                <th>Room</th>
                                                <th>Ref. No</th>
                                                <th>Particular</th>
                                                <th>Description</th>
                                                <th>User</th>
                                                <th>Amount (Rs)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td data-label="Date">2017-12-28 Thu </td>
                                                <td data-label="Room">104</td>
                                                <td data-label="Ref. No">Ref. No</td>
                                                <td data-label="Room Charges">Room Charges   </td>
                                                <td data-label="Description">Description</td>
                                                <td data-label="User">admin</td>
                                                <td data-label="Amount">Rs 100.00   </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>




                            </div>
                            <div role="tabpanel" class="tab-pane" id="fnb-charges">

                                <div class="responsive-table">
                                    <table class="table table-custom table-custom-px-0 mb-0-xs">
                                        <thead class="hidden-xs">
                                            <tr>
                                                <th>Order id</th>
                                                <th>Items</th>
                                                <th>Date & time</th>
                                                <th>Total Amount</th>
                                                <th>Amount Paid</th>
                                                <th>Pending Amount</th>
                                                <th class="text-right">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>FSP001</td>
                                                <td>2</td>
                                                <td>0/02/2018</td>
                                                <td>2000.00</td>
                                                <td>1000.00</td>
                                                <td>1000.00</td>                          <td class="text-right">
                                                    <button type="button" class="btn btn-default btn-sm" data-target="#modal-view-order" data-toggle="modal">View <i class="fal fa-arrow-right"></i></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <h4 class="text-right">Total Amount: 1000.00</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right hide">
                    <button type="button" class="btn btn-default">Cancel <i class="fal fa-times"></i></button>
                    <button type="button" class="btn btn-primary">Save <i class="fal fa-arrow-right"></i></button>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('includes/reservation_discount_popup') ?>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk - <?= date('Y') ?></div>
    <div class="modal fade" id="customer_info_popup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Customer Information</h4>
                </div>
                <form ng-submit="UpdateCustomerInfo()" id="customer_form">
                    <div class="modal-body">
                        <div class="whiteboxs mb-10-xs" >
                            <div class="row rm-5">
                                <label class="col-sm-4 cp-5 control-label text-left">Mobile</label>
                                <div class="col-sm-8 cp-5 mb-10-xs">
                                    <div class="custom-group">
                                        <div class="custom-input">
                                            <input type="text" class="form-control"  name="contact_mobile" ng-model="customerObj.contact_mobile" autocomplete="mobile" readonly>
                                            <span class="ci-icon"><i class="fal fa-mobile"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row rm-5" >
                                <label  class="col-sm-4 cp-5 control-label text-left">Name</label>
                                <div class="col-sm-8 cp-5 mb-10-xs">
                                    <div class="custom-group">
                                        <div class="custom-input">
                                            <input type="text" class="form-control" name="contact_name" ng-model="customerObj.contact_name" autocomplete="name" readonly>
                                            <span class="ci-icon"><i class="fal fa-user"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row rm-5">
                                <label  class="col-sm-4 cp-5 control-label text-left">Email</label>
                                <div class="col-sm-8 cp-5 mb-10-xs">
                                    <div class="custom-group">
                                        <div class="custom-input">
                                            <input type="text" class="form-control" name="contact_email" ng-model="customerObj.contact_email"  autocomplete="email" readonly>
                                            <span class="ci-icon"><i class="fal fa-envelope"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row rm-5">
                                <label  class="col-sm-4 cp-5 control-label text-left">Alternate Mobile</label>
                                <div class="col-sm-8 cp-5">
                                    <div class="custom-group">
                                        <div class="custom-input">
                                            <input type="text" class="form-control" name="alternative_mobile" ng-model="customerObj.alternative_mobile"  autocomplete="alternative_mobile">
                                            <span class="ci-icon"><i class="fal fa-mobile"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="other_info_popup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Reservation Information</h4>
                </div>
                <form ng-submit="UpdateOtherInfo()" id="other_info_form">
                    <div class="modal-body">
                        <div class="whiteboxs mb-10-xs" >
                            <div class="row rm-5">
                                <label  class="col-sm-4 cp-5 control-label text-left">How did you know us</label>
                                <div class="col-sm-8 mb-10-xs cp-5">
                                    <div class="custom-group">
                                        <div class="custom-input">
                                            <select class="form-control" ng-model="otherInfoObj.how_did_hear_about_us_id" name="how_did_hear_about_us_id">
                                                <option value="">Choose</option>
                                                <option ng-value="item.id" value="item.id" ng-repeat="item in referencesList">{{item.title}}</option>
                                            </select>
                                            <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div ng-show="reservationObj.has_permission_to_update_other_information">
                                <div class="row rm-5">
                                    <label  class="col-sm-4 cp-5 control-label text-left">Agent </label>
                                    <div class="col-sm-8 cp-5">
                                        <div class="row rm-5">
                                            <div class="col-md-12 mb-10-xs cp-5">
                                                <div table>
                                                    <div tr>
                                                        <div td>
                                                            <div class="dynamic-search">
                                                                <input type="text"  autocomplete="agent_mobile" ng-keyup="GetAgentsListWithSearchKey()" ng-model="otherInfoObj.agent_mobile" placeholder="Agent Name or Mobile" class="form-control" name="agent_mobile">
                                                                <div class="dynamic-search-result" ng-show="agentsList_display">
                                                                    <ul>
                                                                        <li ng-repeat="item in agentsList" ng-click="agentsListClick(item)">{{item.display_text}}</li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--<div td btn> <button type="button" class="btn btn-default" ng-click="ShowAgentAddForm()"><i ng-show="!agentSpinner" class="fal fa-plus"></i><i ng-show="agentSpinner" class="fal fa-circle-notch fa-spin"></i></button> </div>-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row rm-5">
                                    <label  class="col-sm-4 cp-5 control-label text-left">Commission (%)</label>
                                    <div class="col-sm-8 cp-5">
                                        <div class="row rm-5">
                                            <div class="col-md-12 mb-10-xs cp-5">
                                                <input type="text" class="form-control" placeholder="Commission" ng-model="otherInfoObj.agent_commission" name="agent_commission" max='100'>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row rm-5">
                                    <label  class="col-sm-4 cp-5 control-label text-left">Reservation Status</label>
                                    <div class="col-sm-8 mb-10-xs cp-5">
                                        <div class="custom-group">
                                            <div class="custom-input">
                                                <select class="form-control" name="booking_status" name="booking_status" ng-model="otherInfoObj.booking_status" name="booking_status">
                                                    <option value="">Choose Reservation Status</option>
                                                    <option value="Pending">Pending</option>
                                                    <option value="Blocked">Blocked</option>
                                                    <option value="Confirmed">Confirmed</option>
                                                    <option value="Cancelled">Cancelled</option>
                                                    <!--                                                <option value="Rejected">Rejected</option>-->
                                                    <option value="Completed">Completed</option>
                                                </select>
                                                <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row rm-5">
                                <label  class="col-sm-4 cp-5 control-label text-left">Business</label>
                                <div class="col-sm-8 cp-5">
                                    <div class="custom-group">
                                        <div class="custom-input">
                                            <select class="form-control" ng-model="otherInfoObj.business_sources_id" name="business_sources_id">
                                                <option value="">Choose Business</option>
                                                <option ng-value="item.id" value="item.id" ng-repeat="item in businessSourcesList">{{item.business_source_name}}</option>
                                            </select>
                                            <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-add-company">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Company </h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group rm-5 mb-10-xs">
                            <label  class="col-sm-2 cp-5 control-label text-left">Company</label>
                            <div class="col-sm-10 cp-5">
                                <input type="text" class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="form-group rm-5 mb-10-xs">
                            <label class="col-sm-2 cp-5 control-label">Person</label>
                            <div class="col-sm-10 cp-5">
                                <div class="row rm-5">
                                    <div class="col-md-3 cp-5">
                                        <div class="custom-input">
                                            <select class="form-control">
                                                <option>Mr.</option>
                                            </select>
                                            <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-md-9 cp-5">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group rm-5 mb-10-xs">
                            <label  class="col-sm-2 cp-5 control-label text-left">Address</label>
                            <div class="col-sm-10 cp-5">
                                <div class="row rm-5">
                                    <div class="col-md-12 cp-5 mb-10-xs">
                                        <textarea class="form-control" placeholder="Address"></textarea>
                                    </div>
                                    <div class="col-md-6 cp-5 mb-10-xs">
                                        <div class="custom-input">
                                            <select class="form-control">
                                                <option>India</option>
                                            </select>
                                            <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6 cp-5 mb-10-xs">
                                        <input type="text" class="form-control" placeholder="State">
                                    </div>
                                    <div class="col-md-6 cp-5">
                                        <input type="text" class="form-control" placeholder="City">
                                    </div>
                                    <div class="col-md-6 cp-5">
                                        <input type="text" class="form-control" placeholder="Zip">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group rm-5 mb-10-xs">
                            <label  class="col-sm-2 cp-5 control-label text-left">Contact No</label>
                            <div class="col-sm-10 cp-5">
                                <div class="row rm-5">
                                    <div class="col-md-6 cp-5">
                                        <input type="text" class="form-control" placeholder="Mobile">
                                    </div>
                                    <div class="col-md-6 cp-5">
                                        <input type="text" class="form-control" placeholder="Phone">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group rm-5 mb-10-xs">
                            <label  class="col-sm-2 cp-5 control-label text-left">Email</label>
                            <div class="col-sm-10 cp-5">
                                <input type="text" class="form-control" placeholder="Email">
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="edit_arrival_time_popup">
        <div class="modal-dialog ">
            <div class="modal-content">
                <form ng-submit="getStayAvailability()" id="edit_arrival_time_form">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Edit Check-in Time</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row rm-5">
                                <span class="text-danger" ng-bind-html="error_message"></span>
                                <label  class="col-sm-2 cp-5 control-label text-left">Check-in</label>
                                <div class="col-sm-10 cp-5">
                                    <div class="row rm-5">
                                        <div class="col-sm-6 mb-10-xs cp-5">
                                            <div class="custom-group">
                                                <div class="custom-input">
                                                    <input type="text" class="form-control datepicker" placeholder="Check-In Date" name="check_in_date" ng-model="stayInfoObj.check_in_date">
                                                    <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 mb-10-xs cp-5">
                                            <div class="custom-group">
                                                <div class="custom-input">
                                                    <input type="text" class="form-control timepicker" placeholder="Hours" name="check_in_time" ng-model="stayInfoObj.check_in_time">
                                                    <span class="ci-icon"><i class="fal fa-clock"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row rm-5 hide">
                                <label  class="col-sm-2 cp-5 control-label text-left">Duration</label>
                                <div class="col-sm-10 cp-5">
                                    <div class="row rm-5">
                                        <div class="col-sm-6 mb-10-xs cp-5">
                                            <div class="custom-group">
                                                <div class="custom-input">
                                                    <input type="text" class="form-control hourspicker" placeholder="Hours" name="duration" ng-model="stayInfoObj.duration">
                                                    <span class="ci-icon"><i class="fal fa-clock"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 mb-10-xs cp-5">
                                            <label  class="control-label text-left">{{reservationObj.duration_type_text}}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <p><font class="text-danger">Note:</font>Subject to change amount</p>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <!-- <button type="button" ng-click="getStayAvailability()" class="btn btn-primary">Check Availability</button>-->
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-kot">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Kitchen Order Ticket</h4>
                </div>
                <div class="modal-body">

                    <div class="form-horizontal">
                        <div class="form-group rm-5 mb-10-xs">
                            <label  class="col-sm-3 cp-5 control-label text-left">Item</label>
                            <div class="col-sm-9 cp-5">
                                <div class="custom-input">
                                    <select class="form-control">
                                        <option>Select</option>
                                    </select>
                                    <span class="ci-icon"><i class="fal fa fa-chevron-down"></i></span></div>
                            </div>
                        </div>
                        <div class="form-group rm-5 mb-10-xs">
                            <label  class="col-sm-3 cp-5 control-label text-left">Qty</label>
                            <div class="col-sm-9 cp-5">
                                <div class="custom-input">
                                    <select class="form-control">
                                        <option>1</option>
                                    </select>
                                    <span class="ci-icon"><i class="fal fa fa-chevron-down"></i></span></div>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-checkin">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Check In</h4>
                </div>
                <form ng-submit="UpdateCheckInformation()" id="checkin_form">
                    <div class="modal-body bg-default">
                        <div class="width-400 mx-auto-xs">
                            <span class="text-danger" ng-bind-html="error_message"></span>
                            <div class="form-group" ng-init="enable_camera = 0; captured_snap = 0">
                                <label>Take Photo</label>
                                <div class="alert alert-error ng-scope" ng-show="webcamError" style="">
                                    <span>Webcam could not be started. Did you give access to it?</span>
                                </div>
                                <div class="captureScreen">
                                    <div id="results" width="100%" height="100%" ng-show="captured_snap === 1"></div>
                                    <div id="my_camera" ng-show="captured_snap === 0"></div>
                                </div>
                                <!--<button type="button" class="btn btn-default btn-block" ng-click="enable_camera = 1" ng-show="enable_camera === 0">
                                    <b>Enable Camera</b><i class="far fa-camera ml-10-xs"></i>
                                </button>-->
                                <button type="button" class="btn btn-default btn-block" ng-click="TakeSnapshot(); captured_snap = 1" ng-show="captured_snap === 0">
                                    <b>Capture</b><i class="far fa-camera ml-10-xs"></i>
                                </button>
                                <button type="button" class="btn btn-default btn-block" ng-click="captured_snap = 0" ng-show="captured_snap === 1">
                                    <b>Retake</b><i class="far fa-camera ml-10-xs"></i>
                                </button>
                            </div>

                            <!--This block will be displayed for the old bookings only-->
                            <div class="form-group" ng-if="reservationObj.created_as == 'Old'">
                                <label>Check In Time</label>
                                <input type="text" class="form-control dwpdateswithtime" name="check_in_time" 
                                       ng-model="reservationObj.check_in_details.customer_checked_in_at" autocomplete="off" onkeydown="event.preventDefault()">
                            </div>
                            <!--This block will be displayed for the old bookings only-->

                            <div class="form-group">
                                <label>Choose Guest</label>
                                <div class="custom-input">
                                    <select class="form-control" name="guest" ng-model="guests" ng-change="AutoFillProofDetails()">
                                        <option value="">Select</option>
                                        <option ng-value="{{item}}" ng-repeat="item in reservationObj.guests track by $index">{{item.person_name}}</option>
                                    </select>
                                    <span class="ci-icon">
                                        <i class="fal fa-chevron-down"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Proof Type</label>
                                <div class="custom-input">
                                    <select class="form-control" name="proof_type" ng-model="reservationObj.check_in_details.proof_type" ng-change="ApplyIdentiyNumberValidation()">
                                        <option value="">Select</option>
                                        <option value="Aadhaar Card">Aadhaar Card</option>
                                        <option value="Voter ID">Voter ID</option>
                                        <option value="PAN Card">PAN Card</option>
                                        <option value="Driving License">Driving License</option>
                                        <option value="Passport">Passport</option>
                                        <option value="Other">Other</option>
                                    </select>
                                    <span class="ci-icon">
                                        <i class="fal fa-chevron-down"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Proof No.</label>
                                <input type="text" class="form-control" name="proof_number" ng-model="reservationObj.check_in_details.proof_number" maxlength="{{reservationObj.check_in_details.max_length?reservationObj.check_in_details.max_length:''}}" minlength="{{reservationObj.check_in_details.min_length?reservationObj.check_in_details.min_length:''}}">
                            </div>
                            <div ng-hide="reservationObj.check_in_details.proof_already_existed">
                                <div class="form-group mb-0-xs">
                                    <label>Upload Proof</label>
                                    <div class="btn btn-default btn-block btn-file">
                                        <input type="file" class="form-control" name="proof_image"  ng-model="reservationObj.check_in_details.proof_image" accept="image/x-png,image/gif,image/jpeg,image/PNG,image/png,application/pdf" maxsize="5000" base-sixty-four-input>
                                        <b>Browse Image </b> <i class="fas fa-ellipsis-h"></i>
                                    </div>
                                    <span ng-show="form.files.$error.maxsize">Files must not exceed 5000 KB</span>
                                </div>

                                <img ng-show="reservationObj.check_in_details.proof_image.filetype"
                                     ng-src='data:{{reservationObj.check_in_details.proof_image.filetype}};base64,{{reservationObj.check_in_details.proof_image.base64}}' style="height: 50px" />

                                <img ng-hide="reservationObj.check_in_details.proof_image.filetype" ng-src="{{reservationObj.check_in_details.proof_image}}" style="height: 50px"/>
                            </div>
                            <div ng-show="reservationObj.check_in_details.proof_already_existed">
                                <div class="form-group mb-0-xs">
                                    <label>Proof</label>
                                </div>
                                <img ng-show="reservationObj.check_in_details.existed_proof_image"
                                     ng-src='{{reservationObj.check_in_details.existed_proof_image}}' style="height: 50px" />
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" ng-click="HideCheckInPopup()"  class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary" ng-click="">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-checkout">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Check Out</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-custom table-custom-px-0 font-bold mb-10-xs">
                        <tr ng-repeat="item in reservationObj.booking_items track by $index">
                            <td>{{item.items.length}} {{item.stay_category_name}}</td>
                            <td class="text-right">{{item.total_amount|number:2}}</td>
                        </tr>

                        <tr>
                            <td><strong>Discount ({{(reservationObj.payment_info.discount_type == 'flat') ? reservationObj.payment_info.discount_type_symbol+'. ':''}}{{reservationObj.payment_info.discount_value}}{{(reservationObj.payment_info.discount_type == 'percentage') ? reservationObj.payment_info.discount_type_symbol+'. ':''}})</strong></td>
                            <td class="text-right">Rs {{reservationObj.payment_info.discount_amount}}</td>
                        </tr>
                        <tr class="table-devider">
                            <td>Sub Total</td>
                            <td class="text-right">{{reservationObj.payment_info.sub_total||0}}</td>
                        </tr>
                        <tr ng-repeat="item in reservationObj.payment_info.applied_taxes track by $index">
                            <td> {{item.tax_name}} {{item.tax_amount}}% </td>
                            <td class="text-right">{{item.applied_tax_amount|number:2}}</td>
                        </tr>

                        <tr ng-if="reservationObj.payment_info.coupon_code">
                            <td><strong>Coupon Discount ({{(reservationObj.payment_info.coupon_discount_type == 'flat') ? reservationObj.payment_info.coupon_discount_type_symbol+'. ':''}}{{reservationObj.payment_info.coupon_discount_amount}}{{(reservationObj.payment_info.coupon_discount_type == 'percentage') ? reservationObj.payment_info.coupon_discount_type_symbol+'. ':''}})</strong></td>
                            <td class="text-right">Rs {{reservationObj.payment_info.applied_coupon_discount_amount}}</td>
                        </tr>
                        <tr>
                            <td>Total</td>
                            <td class="text-right">{{reservationObj.payment_info.grand_total|number:2}}</td>
                        </tr>
                        <tr class="table-devider">
                            <td>Paid Amount</td>
                            <td class="text-right">{{reservationObj.payment_info.payments_total|number:2}}</td>
                        </tr>
                        <tr>
                            <td>Amount Due</td>
                            <td class="text-right">{{(reservationObj.payment_info.grand_total - reservationObj.payment_info.payments_total)|number:2}}</td>
                        </tr>


                        <!--This block will be displayed for the old bookings only-->
                        <tr ng-if="reservationObj.created_as == 'Old'">
                            <td>Check Out Time</td>
                            <td class="text-right">
                                <input type="text" class="form-control dwpdateswithtime" name="check_out_time" 
                                       ng-model="reservationObj.check_out_details.customer_checked_out_at" autocomplete="off" onkeydown="event.preventDefault()">  
                            </td>
                        </tr>
                        <!--This block will be displayed for the old bookings only-->
                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" ng-click="HideCheckOutPopup()" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" ng-click="MarkAsCheckOut(reservationObj.booking_ref_number)"
                            ng-disabled="{{(reservationObj.payment_info.grand_total - reservationObj.payment_info.payments_total) > 0?true:false}}">Check Out</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-view-order">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Order View</h4>
                </div>
                <div class="modal-body">

                    <table class="table table-custom">
                        <thead>
                            <tr>
                                <th>Item</th>
                                <th>Qty</th>
                                <th class="no-sort text-right">Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Cool Drinks</td>
                                <td>2</td>
                                <td class="text-right">1000.00</td>
                            </tr>
                            <tr>
                                <td>Breakfast</td>
                                <td>2</td>
                                <td class="text-right">1000.00</td>
                            </tr>
                        </tbody>
                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="agent-modal-popup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{agentObj.id?"Update":"Add"}} Agent</h4>
                </div>
                <div class="modal-body">
                    <form ng-submit="AddOrUpdateAgent()" id="agentForm" autocomplete="off">
                        <span class="text-danger" ng-bind-html="error_message"></span>
                        <div class="form-group">
                            <label>Choose Agent Type</label>
                            <select class="form-control" name="agent_types_id" ng-model="agentObj.agent_types_id">
                                <option value="" selected disabled="">Choose Agent Type</option>
                                <option value="{{item.id}}" ng-repeat="item in agentTypesList track by $index">{{item.agent_type}}</option>
                            </select>
                        </div>
                        <div class="form-group" ng-if="agentObj.agent_types_id === '2'">
                            <label>Choose Center</label>
                            <select class="form-control" name="centers_id" ng-model="agentObj.centers_id">
                                <option value="" selected disabled="">Choose Center</option>
                                <option value="{{item.id}}" ng-repeat="item in centersList track by $index">{{item.center_name}}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Agent Name</label>
                            <input type="text" class="form-control" name="agent_name" ng-model="agentObj.agent_name">
                        </div>
                        <div class="form-group">
                            <label>Mobile No</label>
                            <input type="text" class="form-control" name="mobile" ng-model="agentObj.mobile">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control" name="email" ng-model="agentObj.email">
                        </div>
                        <div class="form-group">
                            <label>Commission%</label>
                            <input type="text" class="form-control" name="commission" ng-model="agentObj.commission">
                        </div>
                        <div class="form-group">
                            <label>Login Required</label>
                            <select class="form-control" name="login_required" ng-model="agentObj.login_required">
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                        </div>
                        <div class="form-group" ng-show="agentObj.login_required == 1">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password" ng-model="agentObj.password">
                        </div>
                        <div class="text-right">
                            <button class="btn btn-default" type="reset" ng-click="ResetAgentForm()"><b>Cancel <i class="fal fa-times"></i></b></button>
                            <button type="submit" class="btn btn-primary"><b>{{agentObj.id?'Update':'Add'}} <i class="fal fa-plus"></i></b></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->

    <div class="modal fade" id="billing_info_popup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Billing Information</h4>
                </div>
                <form ng-submit="UpdateBillingInfo()" id="billing_form">
                    <div class="modal-body">
                        <div class="whiteboxs mb-10-xs" >
                            <div class="row rm-5">
                                <span class="text-danger" ng-bind-html="error_message"></span>
                                <label  class="col-sm-2 cp-5 control-label text-left">Billing  To</label>
                                <div class="col-sm-10 cp-5">
                                    <div class="row rm-5">
                                        <div class="col-sm-4 mb-10-xs cp-5">
                                            <div class="custom-group">
                                                <div class="custom-input">
                                                    <select class="form-control" name="billing_to" ng-model="billingInfoObj.billing_to" ng-change="(billingInfoObj.billing_to == 'Customer') ? FetchCustomerData():''">
                                                        <option value="Customer">Customer</option>
                                                        <option value="Organization">Organization</option>
                                                        <option value="​Others">​Others</option>
                                                    </select>
                                                    <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-8 mb-10-xs cp-5" ng-show="billingInfoObj.billing_to === 'Organization'">
                                            <div table>
                                                <div tr>
                                                    <div td>
                                                        <div class="dynamic-search">
                                                            <input type="text"  ng-keyup="GetCompaniesListWithSearchKey()" ng-model="billingInfoObj.company_name" placeholder="Company Name" class="form-control" ng-disabled="billingInfoObj.billing_to != 'Organization'">
                                                            <div class="dynamic-search-result" ng-show="companiesList_display">
                                                                <ul>
                                                                    <li ng-repeat="item in companiesList" ng-click="companiesListClick(item)">{{item.display_text}}</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div td btn>
                                                        <button type="button" class="btn btn-default" ng-click="ShowCompanyAddForm()" ng-disabled="billingInfoObj.billing_to != 'Organization'">
                                                            <i ng-show="companySpinner" class="fal fa-circle-notch fa-spin"></i>
                                                            <i ng-show="!companySpinner" class="fal fa-plus"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-8 mb-10-xs cp-5" ng-show="billingInfoObj.billing_to === '​Others'">
                                            <div table>
                                                <div tr>
                                                    <div td>
                                                        <div class="form-group">
                                                            <input type="text"  ng-model="billingInfoObj.billing_to_others_text"
                                                                   placeholder="Enter Organisation/trust/NGO/College or any other type of group" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row rm-5">
                                <label  class="col-sm-12 cp-5 control-label text-center">Address</label>
                                <div class="row rm-5">
                                    <label  class="col-sm-4 cp-5 control-label text-left">​Door No</label>
                                    <div class="col-sm-8 cp-5 mb-10-xs">
                                        <div class="custom-group">
                                            <div class="custom-input">
                                                <input type="text" class="form-control" placeholder="Door No" name="door_no" ng-model="billingInfoObj.door_no" autocomplete="door-no">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row rm-5">
                                    <label  class="col-sm-4 cp-5 control-label text-left">Street Name</label>
                                    <div class="col-sm-8 cp-5 mb-10-xs">
                                        <div class="custom-group">
                                            <div class="custom-input">
                                                <input type="text" class="form-control" placeholder="Street Name" name="street_name" ng-model="billingInfoObj.street_name" autocomplete="street_name">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row rm-5">
                                    <label  class="col-sm-4 cp-5 control-label text-left">Area/Location</label>
                                    <div class="col-sm-8 cp-5 mb-10-xs">
                                        <div class="custom-group">
                                            <input type="text" class="form-control" placeholder="Area/Location" name="location" ng-model="billingInfoObj.location" autocomplete="location">
                                        </div>
                                    </div>
                                </div>

                                <div class="row rm-5">
                                    <label  class="col-sm-4 cp-5 control-label text-left">City</label>
                                    <div class="col-sm-8 cp-5 mb-10-xs">
                                        <div class="custom-group">
                                            <input type="text" class="form-control" placeholder="City" name="city" ng-model="billingInfoObj.city" autocomplete="city">
                                        </div>
                                    </div>
                                </div>
                                <div class="row rm-5">
                                    <label  class="col-sm-4 cp-5 control-label text-left">District</label>
                                    <div class="col-sm-8 cp-5 mb-10-xs">
                                        <div class="custom-group">
                                            <input type="text" class="form-control" placeholder="District" name="district" ng-model="billingInfoObj.district" autocomplete="district">
                                        </div>
                                    </div>
                                </div>
                                <div class="row rm-5">
                                    <label  class="col-sm-4 cp-5 control-label text-left">State</label>
                                    <div class="col-sm-8 cp-5 mb-10-xs">
                                        <div class="custom-group">
                                            <input type="text" class="form-control" placeholder="Statename" name="state" ng-model="billingInfoObj.state" autocomplete="state">
                                        </div>
                                    </div>
                                </div>
                                <div class="row rm-5">
                                    <label  class="col-sm-4 cp-5 control-label text-left">Pin Code</label>
                                    <div class="col-sm-8 cp-5 mb-10-xs">
                                        <div class="custom-group">
                                            <input type="text" class="form-control" placeholder="Pincode/Zipcode/Postal Code" name="zipcode" ng-model="billingInfoObj.zipcode" autocomplete="zipcode">
                                        </div>
                                    </div>
                                </div>
                                <div class="row rm-5">
                                    <label  class="col-sm-4 cp-5 control-label text-left">Country</label>
                                    <div class="col-sm-8 cp-5 mb-10-xs">
                                        <div class="custom-group">
                                            <div class="custom-input">
                                                <select class="form-control" name="countries_id" ng-model="billingInfoObj.countries_id">
                                                    <option value="">Choose Country</option>
                                                    <option ng-value="item.id" value="item.id" ng-repeat="item in countriesList">{{item.country_name}}</option>
                                                </select>
                                                <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row rm-5">
                                    <label  class="col-sm-4 cp-5 control-label text-left">GST Number</label>
                                    <div class="col-sm-8 cp-5 mb-10-xs">
                                        <div class="custom-group">
                                            <div class="custom-input">
                                                <input type="text" class="form-control" placeholder="GST Number" ng-required="billingInfoObj.billing_to === 'Company'" name="payer_gst_number" ng-model="billingInfoObj.payer_gst_number" maxlength="15" minlength="15" autocomplete="gst_number">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="guest-details-edit">
        <div class="modal-dialog modal-lg" style="max-width: 90% !important;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Modify Guests Details</h4>
                </div>
                <div class="modal-body">
                    <form  id="guest-details-form">
                        <div class="whitebox bordered mb-10-xs pa-0-xs form-horizontal"  ng-repeat="booking_items in edit_booking_items track by $index">
                            <div class="whiteboxHead ma-0-xs"><strong ng-bind="booking_items.stay_category_name"></strong>
                            </div>
                            <table class="table table-custom table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th class="col-md-2">Name</th>
                                        <th class="col-md-2">Gender</th>
                                        <th class="col-md-2">Age</th>
                                        <th class="col-md-2">Proof Type</th>
                                        <th class="col-md-2">Proof ID</th>
                                        <th class="col-md-2">SKU</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in booking_items.items track by $index" >
                                        <td>
                                            <div style="width:80%">
                                                <input type="text" class="form-control" ng-model="item.person_name" name="person_name[{{$index}}{{$parent.$index}}]" pattern="[a-zA-Z\s]+" required placeholder="Name"  >
                                            </div>
                                        </td>
                                        <td>
                                            <div class="custom-input">
                                                <select class="form-control" ng-model="item.gender" name="gender[{{$index}}{{$parent.$index}}]" required>
                                                    <option value="">--Select Gender--</option>
                                                    <option value="Male" ng-hide="booking_items.gender === 'Female'">Male</option>
                                                    <option value="Female" ng-hide="booking_items.gender === 'Male'">Female</option>
                                                </select>
                                                <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                                            </div>
                                        </td>
                                        <td>
                                            <div style="width:80%">
                                                <input type="number" class="form-control number" name="age[{{$index}}{{$parent.$index}}]" placeholder="Age" ng-model="item.age" required min="0" max="110" pattern="[0-9\s]+">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="custom-input">
                                                <select class="form-control"  ng-model="item.identity_type" name="identity_type[{{$index}}{{$parent.$index}}]"   ng-change="ApplyIdentiyNumberValidationGuestsDetail(item)" ng-init="ApplyIdentiyNumberValidationGuestsDetailInit(item)">
                                                    <option value="">--Select Proof--</option>
                                                    <option value="Aadhaar Card">Aadhaar Card</option>
                                                    <option value="Voter ID">Voter ID</option>
                                                    <option value="PAN Card">PAN Card</option>
                                                    <option value="Driving License">Driving License</option>
                                                    <option value="Passport">Passport</option>
                                                    <option value="Other">Other</option>
                                                </select>
                                                <span class="ci-icon"><i class="fal fa-chevron-down"></i></span>
                                            </div>
                                        </td>
                                        <td>
                                            <div style="width:80%">
                                                <input type="text"  class="form-control {{item.identity_type=='Aadhaar Card'?'number':''}}" name="identity_number[{{$index}}{{$parent.$index}}]"
                                                       placeholder="Proof No. {{item.identity_number}}" ng-model="item.identity_number" ng-required="item.identity_type!=''"
                                                       maxlength="{{item.max_length?item.max_length:''}}" minlength="{{item.min_length?item.min_length:''}}">
                                            </div>
                                        </td>
                                        <td>{{item.inventory_code}}</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" ng-click="HideGuestDetailsPopup()" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" ng-click="UpdateGuestsDetails()" class="btn btn-primary">Update</button>
                </div>
            </div>
        </div>
    </div>

    <div class="sweetyOverlay" data-show="{{sweetyDisplay}}"></div>
    <div class="sweety" data-show="{{sweetyDisplay}}">
        <div class="sweetyTitle">Do you want to perform individual checkout operation?
            <strong>{{sweetyTitle}}</strong>
            <table class="table table-bordered">
                <tr>
                    <th>Type</th>
                    <td>{{sweetyInfo.category_name}}</td>
                </tr>
                <tr>
                    <th>Person Name</th>
                    <td>{{sweetyInfo.person_name}}</td>
                </tr>
                <tr>
                    <th>Gender(Age)</th>
                    <td>{{sweetyInfo.gender}} ({{sweetyInfo.age ? sweetyInfo.age : 'N/a'}})</td>
                </tr>
                <tr>
                    <th>Proof Type</th>
                    <td>{{sweetyInfo.identity_type}}</td>
                </tr>
                <tr>
                    <th>Proof Number</th>
                    <td>{{sweetyInfo.identity_number}}</td>
                </tr>
            </table>
        </div>
        <button type="button" ng-click="action_fn(true)" class="btn btn-primary">Yes</button>
        <button type="button" ng-click="action_fn(false)" class="btn btn-default">No</button>
    </div>

    <?php $this->load->view('includes/reservation_add_payment_popup') ?>
</section>

<div ng-controller="companiesCtrl">
    <?php $this->load->view("includes/company_add_popup") ?>
</div>

<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/reservationViewCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/reservationViewService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/taxSlabsService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/countriesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/businessSourcesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/howDidHearAboutUsService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/companiesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/companiesCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/companiesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/organizationTypesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/agentsService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/agentTypesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/centersService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/taxSlabsService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/customersService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/createReservationService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>webcam.min.js"></script>
<style>
    @media (min-width: 992px){
        .modal-lg {
            width: 90% !important;
        }
    }
</style>