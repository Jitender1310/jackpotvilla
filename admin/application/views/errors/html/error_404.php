<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>404 Page Not Found</title>
        <link rel="stylesheet" type="text/css" href="<?= STATIC_ADMIN_CSS_PATH ?>bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?= STATIC_ADMIN_CSS_PATH ?>style.css?i=<?php echo time(); ?>">
    </head>
    <body >
        <div class="page-404">
            <div class="whitebox">
                <div class="width-300 mx-auto-xs">

                </div>
                <h2>404 Page Not Found</h2>
                <p>The page you requested was not found.</p>
                <a href="<?php echo config_item('base_url'); ?>">Click here Go to Homepage</a>
            </div>
        </div>
    </body>
</html>