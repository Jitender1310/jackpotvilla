<style>
    .table-booking table tr td.active {
        background-color: #79B833;
        color: white;
    }
</style>
<section class="pageWrapper" ng-controller="reservationAvailabilityCtrl" ng-init="reservationAvailabilityObj.date = '<?= date('d-m-Y') ?>'">
    <div class="pageHeader" workspace-offset valign-parent>
        <div class="row">
            <div class="col-md-4"><strong>Reservation Availability</strong></div>
            <div class="col-md-8 hidden-xs">
                <div valign-holder>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="custom-input" ng-init="GetCentersList('<?= get_user_id() ?>')">
                                <select class="form-control input-sm" ng-change="GetAssignedStayingTypes(reservationAvailabilityObj.centers_id)" ng-model="reservationAvailabilityObj.centers_id" ng-disabled="!centersList.length > 0">
                                    <option value="">Choose Center</option>
                                    <option ng-value="item.id" value="item.id" ng-repeat="item in centersList">{{item.center_name}}</option>
                                </select>
                                <span class="ci-icon">
                                    <i ng-show="centersSpinner" class="fal fa-circle-notch fa-spin"></i>
                                    <i ng-show="!centersSpinner" class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-3 pl-10-xs">
                            <div class="custom-input">
                                <select class="form-control input-sm" ng-change="GetStayingTypesCategoriesForCenter(reservationAvailabilityObj.centers_id, reservationAvailabilityObj.staying_types_id); GetStayingTypesCategoriesForCenter(reservationAvailabilityObj.centers_id, reservationAvailabilityObj.staying_types_id)" ng-model="reservationAvailabilityObj.staying_types_id" ng-disabled="!stayingTypesList.length > 0">
                                    <option value="">Choose Type</option>
                                    <option value="{{item.staying_types_id}}" ng-repeat="item in stayingTypesList track by $index">{{item.name}}</option>
                                </select>
                                <span class="ci-icon">
                                    <i ng-show="!stayingTypesSpinner" class="fal fa-chevron-down"></i>
                                    <i ng-show="stayingTypesSpinner" class="fal fa-circle-notch fa-spin"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-3 pl-10-xs">
                            <div class="custom-input">
                                <input type="text" class="form-control input-sm datepickerNormal" onkeydown="event.preventDefault()" readonly2 placeholder="Date" name="date" ng-model="reservationAvailabilityObj.date" ng-disabled="(!stayingTypesList.length > 0) || reservationAvailabilityObj.staying_types_id == ''">
                                <span class="ci-icon"><i class="fal fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-3 pl-10-xs">
                            <button class="btn btn-primary" type="button" ng-click="GetReservationAvailabilityListForCenter()" ng-disabled="(!stayingTypesList.length > 0) || reservationAvailabilityObj.staying_types_id == ''">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--<div class="col-md-3 hidden-xs">
                <ul class="actions ul">
                    <li><a href="">Today</a></li>
                    <li><a href="">Tomorrow</a></li>
                    <li><span class="actionsDatepicker"><input type="text"  placeholder="Custom Date" class="datepicker"> <i class="fal fa-chevron-down"></i></span></li>
                </ul>
            </div>-->
        </div>
    </div>
    <div class="pageBody" workspace>
        <div class="table-responsive hidden-xs">
            <table class="table table-bordered table-booking table-cell-width">
                <tr>
                    <td colspan="26">
                        <table class="">
                            <tr>
                                <td  style="cursor: pointer" ng-repeat="item in stayingTypeCategories track by $index" class="{{(reservationAvailabilityObj.staying_type_categories_id == item.id) ? 'active':''}}" ng-click="UpdateStayTypeCategory(item.id)">{{item.category_name}}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr ng-if="reservationAvailabilityList.slots.length > 0">
                    <td rowspan="2" class="rcell rcell2 fixed-col" width="60">
                        <div>Identity Code</div>
                    </td>
                    <td colspan="24" class="dcell">{{reservationAvailabilityObj.date}}  </td>
                </tr>
                <tr ng-if="reservationAvailabilityList.slots.length > 0">
                    <td class="hcell adjustable-col" ng-repeat="item in reservationAvailabilityList.slots track by $index">
                        <div>{{item}}</div>
                    </td>
                </tr>
                <tbody ng-if="reservationAvailabilityList">
                    <tr class="hrow HoursRow" ng-repeat="item in reservationAvailabilityList.stock track by $index">
                        <td class="rcell">{{item.identity_code}} <span class="rlabel rlabel-ready"></span></td>
                        <td class="cell HoursCol {{(sub_item.available_status == 1) ? 'booked':''}}" 
                            style="{{(sub_item.available_status == 1) ? 'cursor:pointer':''}}" 
                            ng-click="ViewReservationDetails(sub_item)" 
                            ng-repeat="sub_item in item.skus track by $index" 
                            data-type="{{item.category_name}}" 
                            data-room="{{item.identity_code}}" 
                            data-date="{{reservationAvailabilityObj.date}}" 
                            data-hour="{{sub_item.hour}}" 
                            data-meridiem="{{sub_item.meridiem}}">
                        </td>
                    </tr>
                    <tr ng-if="reservationAvailabilityList.stock.length === 0">
                        <td colspan="25" class="dcell text-center">No Items Found</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageFooter text-capitalize" workspace-offset><span bookingInfo></span></div>
    <!-- <div class="pageSidebar">Page Sidebar</div> -->
    <div class="modal fade" id="reservation_details_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Booking Details</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered text-center text-capitalize mb-0-xs">
                        <tr>
                            <td colspan="2" class="h5">Booking Ref ID: #{{reservationObj.booking_ref_number}}</td>
                        </tr>
                        <tr>
                            <td class="h5">Name: </td>
                            <td class="h5">{{reservationObj.contact_name}}</td>
                        </tr>
                        <tr>
                            <td class="h5">Check-in Date Time: </td>
                            <td class="h5" style="text-transform: uppercase">{{reservationObj.check_in_date}} {{reservationObj.check_in_time}}</td>
                        </tr>
                        <tr>
                            <td class="h5">Check-out Date Time: </td>
                            <td class="h5">{{reservationObj.check_out_date_time}}</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <a href="<?= base_url() ?>reservations/quick_view?booking_id={{reservationObj.booking_ref_number}}" target="_blank" class="btn btn-primary">View Reservation</a>

                                <a class="text-primary"
                                   ng-if="item.created_with != 'Quick'"
                                   ng-href="<?= base_url() ?>reservations/quick_view?booking_id={{reservationObj.booking_ref_number}}" 
                                   target="_blank">{{reservationObj.booking_ref_number}}</a>
                                <a class="text-primary"
                                   ng-if="item.created_with == 'Quick'"
                                   ng-href="<?= base_url() ?>reservations/quick_view?booking_id={{reservationObj.booking_ref_number}}" 
                                   target="_blank">{{reservationObj.booking_ref_number}}</a>

                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/reservationAvailabilityCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/reservationAvailabilityService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/centersService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/stayingCategoriesInventoryService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/reservationViewService.js?r=<?= time() ?>"></script>




<div class="modal fade" id="modal-booking">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Confirm Booking</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered text-center text-capitalize mb-0-xs">
                    <tr>
                        <td colspan="2" class="h5"><span bookingType></span>, Room: <span bookingRoom></span></td>
                    </tr>
                    <tr>
                        <td>
                            <span bookingFromDay></span>
                            <span bookingFromHour></span>
                            <span bookingFromMeridiem></span>
                        </td>
                        <td>
                            <span bookingToDay></span>
                            <span bookingToHour></span>
                            <span bookingToMeridiem></span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary"><b>Confirm</b> <i class="glyphicon glyphicon-menu-right"></i></button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-duplicate">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Oops!</h4>
            </div>
            <div class="modal-body">
                <p class="text-center my-30-xs">Already Booked This Slot!</p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-limit">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Oops!</h4>
            </div>
            <div class="modal-body">
                <p class="text-center my-30-xs">Select Atleast 3 Hours!</p>
            </div>
        </div>
    </div>
</div>