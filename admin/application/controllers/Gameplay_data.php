<?php

class Gameplay_data extends MY_Controller {
    public function __construct(){
        parent::__construct();
        $this->login_required();
        if (!permission_required(7, "view")) { redirect("login"); }
        $this->load->model('Gameplay_data_model');
    }
    
    public function index(){
        $this->admin_view('gameplay');
    }
}
