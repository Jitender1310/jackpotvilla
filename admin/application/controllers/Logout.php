<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Logout extends MY_Controller {

    function index() {
        $this->session->unset_userdata("user_id");
        foreach ($this->session->all_userdata() as $key => $item) {
            if ($key == "w_user_id") {
                continue;
            }
            delete_cookie("token");
            $this->session->unset_userdata($key);
        }
        $this->session->set_flashdata("msg", "You Have Successfully Logged Out.");
        redirect("login");
    }

}
