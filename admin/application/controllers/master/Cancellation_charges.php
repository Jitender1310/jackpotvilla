<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cancellation_charges extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_required();
    }

    function index() {
        $this->admin_view("cancellation_charges");
    }

}
