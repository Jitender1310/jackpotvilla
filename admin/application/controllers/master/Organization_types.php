<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Organization_types extends MY_Controller {

        public function __construct() {
            parent::__construct();
            $this->login_required();
            if (!permission_required(35, "view")) {
                redirect("login");
            }
        }

        function index() {
            $this->admin_view("organization_types");
        }

    }
    