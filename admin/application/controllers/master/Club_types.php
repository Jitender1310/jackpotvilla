<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Club_types extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_required();
        if (!permission_required(23, "view")) {
            redirect("login");
        }
    }

    function index() {
        $this->admin_view("master/club_types");
    }

}
