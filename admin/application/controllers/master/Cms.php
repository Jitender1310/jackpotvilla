<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cms extends MY_Controller {

    private $model_name = "cms_model";
    private $title = "cms";

    public function __construct() {
        parent::__construct();
        $this->login_required();
        $this->load->model($this->model_name);
    }

    function index() {
        $this->admin_view("master/cms");
    }

    function about_us() {
        if (!empty($_POST)) {
            $data = [
                "description" => $this->input->get_post('description')
            ];
            $this->{$this->model_name}->update($data, 1);
            $this->session->set_flashdata("msg", "Updated successfully.");
            redirect("master/cms/about_us");
        }
        $this->data["description"] = $this->{$this->model_name}->get(1)->description;
        $this->admin_view("about_us");
    }

    function pricing_policy() {
        if (!empty($_POST)) {
            $data = [
                "description" => $this->input->get_post('description')
            ];
            $this->{$this->model_name}->update($data, 19);
            $this->session->set_flashdata("msg", "Updated successfully.");
            redirect("master/cms/pricing_policy");
        }
        $this->data["description"] = $this->{$this->model_name}->get(19)->description;
        $this->admin_view("pricing_policy");
    }

    function privacy_policy() {
        if (!empty($_POST)) {
            $data = [
                "description" => $this->input->get_post('description')
            ];
            $this->{$this->model_name}->update($data, 4);
            $this->session->set_flashdata("msg", "Updated successfully.");
            redirect("master/cms/privacy_policy");
        }
        $this->data["description"] = $this->{$this->model_name}->get(4)->description;
        $this->admin_view("privacy_policy");
    }

    function refund_policy() {
        if (!empty($_POST)) {
            $data = [
                "description" => $this->input->get_post('description')
            ];
            $this->{$this->model_name}->update($data, 20);
            $this->session->set_flashdata("msg", "Updated successfully.");
            redirect("master/cms/refund_policy");
        }
        $this->data["description"] = $this->{$this->model_name}->get(20)->description;
        $this->admin_view("refund_policy");
    }

    function terms_and_conditions() {
        if (!empty($_POST)) {
            $data = [
                "description" => $this->input->get_post('description')
            ];
            $this->{$this->model_name}->update($data, 5);
            $this->session->set_flashdata("msg", "Updated successfully.");
            redirect("master/cms/terms_and_conditions");
        }
        $this->data["description"] = $this->{$this->model_name}->get(5)->description;
        $this->admin_view("terms_and_conditions");
    }

    function irctc_terms_and_conditions() {
        if (!empty($_POST)) {
            $data = [
                "description" => $this->input->get_post('description')
            ];
            $this->{$this->model_name}->update($data, 22);
            $this->session->set_flashdata("msg", "Updated successfully.");
            redirect("master/cms/irctc_terms_and_conditions");
        }
        $this->data["description"] = $this->{$this->model_name}->get(22)->description;
        $this->admin_view("irctc_terms_and_conditions");
    }

    function terms_of_use() {
        if (!empty($_POST)) {
            $data = [
                "description" => $this->input->get_post('description')
            ];
            $this->{$this->model_name}->update($data, 21);
            $this->session->set_flashdata("msg", "Updated successfully.");
            redirect("master/cms/terms_of_use");
        }
        $this->data["description"] = $this->{$this->model_name}->get(21)->description;
        $this->admin_view("terms_of_use");
    }

}
