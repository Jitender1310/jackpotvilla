<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Create_real_tournament extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_required();
        if (!permission_required(21, "add")) {
            redirect("login");
        }
    }

    function index() {
        $this->admin_view("master/create_real_tournament");
    }

}
