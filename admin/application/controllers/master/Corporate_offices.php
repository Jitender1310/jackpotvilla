<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Corporate_offices extends MY_Controller {

        public function __construct() {
            parent::__construct();
            $this->login_required();
            if (!permission_required(83, "view")) {
                redirect("login");
            }
        }

        function index() {
            $this->admin_view("corporate_offices");
        }

    }
    