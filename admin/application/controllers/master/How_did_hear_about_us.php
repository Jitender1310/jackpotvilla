<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class How_did_hear_about_us extends MY_Controller {

        public function __construct() {
            parent::__construct();
            $this->login_required();
            if (!permission_required(31, "view")) {
                redirect("login");
            }
        }

        function index() {
            $this->admin_view("how_did_hear_about_us");
        }

    }
    