<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Business_sources extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_required();
    }

    function index() {
        $this->admin_view("business_sources");
    }

}
