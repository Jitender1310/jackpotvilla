<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Tax_slabs extends MY_Controller {

        public function __construct() {
            parent::__construct();
            $this->login_required();
            if (!permission_required(27, "view")) {
                redirect("login");
            }
        }

        function index() {
            $this->admin_view("tax_slabs");
        }

    }
    