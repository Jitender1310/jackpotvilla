<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Global_configurations extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_required();
        if (!permission_required(14, "view")) {
            redirect("login");
        }
    }

    function index() {
        $this->admin_view("master/global_configurations");
    }

}
