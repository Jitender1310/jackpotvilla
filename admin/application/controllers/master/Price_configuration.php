<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Price_configuration extends MY_Controller {

        public function __construct() {
            parent::__construct();
            $this->login_required();
            if (!permission_required(18, "view")) {
                redirect("login");
            }
        }

        function index() {
            redirect("master/price_configuration/regular");
        }

        function regular() {
            $this->admin_view("regular_price_configuration");
        }

        function occasional() {
            $this->admin_view("occasional_price_configuration");
        }

    }
    