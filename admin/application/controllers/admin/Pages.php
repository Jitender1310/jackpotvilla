<?php

class Pages extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_required();
    }

    function login() {
        $this->load->view('admin/login');
    }

    function change_password() {
        $this->admin_view('change_password');
    }

    function dashboard() {
        $this->admin_view('dashboard');
    }

    function booking_list() {
        $this->admin_view('booking_list');
    }

    function brefing() {
        $this->admin_view('brefing');
    }

    function plugins() {
        $this->admin_view('plugins');
    }

    function daily_maintenance_checklist() {
        $this->admin_view('daily_maintenance_checklist');
    }

    function day_wise_arrival_report() {
        $this->admin_view('day_wise_arrival_report');
    }

    function expenses() {
        $this->admin_view('expenses');
    }

    function guest_reviews() {
        $this->admin_view('guest_reviews');
    }

    function housekeeping() {
        $this->admin_view('housekeeping');
    }

    function inventory() {
        $this->admin_view('inventory');
    }

    function inventory_view() {
        $this->admin_view('inventory_view');
    }

    function invoice() {
        $this->admin_view('invoice');
    }

    function market_segment() {
        $this->admin_view('market_segment');
    }

    function master_housekeepers() {
        $this->admin_view('master_housekeepers');
    }

    function master_product_category() {
        $this->admin_view('master_product_category');
    }

    function master_products() {
        $this->admin_view('master_products');
    }

    function new_booking() {
        $this->admin_view('new_booking');
    }

    function new_reservation() {
        $this->admin_view('new_reservation');
    }

    function occupancy_cum_sale_report() {
        $this->admin_view('occupancy_cum_sale_report');
    }

    function pos() {
        $this->admin_view("pos");
    }

    function quick_view() {
        $this->admin_view('quick_view');
    }

    function reports() {
        $this->admin_view('reports');
    }

    function privilege() {
        $this->admin_view('privileges');
    }

    function leads() {
        $this->admin_view('leads');
    }

    function new_leads() {
        $this->admin_view('new_leads');
    }

    function lead_view() {
        $this->admin_view('lead_view');
    }

    function completed_bookings() {
        $this->admin_view('completed_bookings');
    }

    function confirmed_bookings() {
        $this->admin_view('confirmed_bookings');
    }

    function add_center() {
        $this->admin_view('add_center');
    }

    function reservations() {
        $this->admin_view('reservations');
    }

    function reservation_view() {
        $this->admin_view('reservation_view');
    }

    function restroom_cleaning_checklist() {
        $this->admin_view('restroom_cleaning_checklist');
    }

    function sale_cash_card_details() {
        $this->admin_view('sale_cash_card_details');
    }

    function staff_attendance() {
        $this->admin_view('staff_attendance');
    }

    function stay_view() {
        $this->admin_view('stay_view');
    }

}
