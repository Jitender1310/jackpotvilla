<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Welcome extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->login_required();
    }

    function index() {
        $this->admin_view("welcome_screen");
    }

}
