<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    class Upcoming extends MY_Controller {

        public function __construct() {
            parent::__construct();
            $this->login_required();
            if (!permission_required(22, "view")) {
                redirect("login");
            }
        }

        function index() {
            $this->admin_view("real_tournaments/upcoming_tournaments");
        }

    }
    