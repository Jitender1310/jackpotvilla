<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Reservation_availability extends MY_Controller {

        public function __construct() {
            parent::__construct();
            $this->login_required();
            if (!permission_required(77, "view")) {
                redirect("login");
            }
        }

        function index() {
            $this->admin_view('reservation_availability');
        }

    }
    