<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class View_all extends MY_Controller {

        public function __construct() {
            parent::__construct();
            $this->login_required();
            if (!permission_required(49, "add", null, false)) {
                redirect("login");
            }
        }

        function index() {
            //$this->admin_view('reservations');
            if ($this->input->get_post('agents_id') != '' || $this->input->get_post('agents_id') != 0) {
                $this->data['agent_details'] = $this->agents_model->get_agent_details($this->input->get_post('agents_id'));
                $this->data['wallet_ballance'] = $this->agents_model->get_wallet_balance($this->input->get_post('agents_id'));
            }
            $this->admin_view('all_reservations');
        }

        /*
          function confirmed() {
          $this->admin_view('reservations/confirmed');
          }

          function today_checkin() {
          $this->admin_view('reservations/today_checkin');
          }

          function today_check_out() {
          $this->admin_view('reservations/today_check_out');
          }

          function inhouse() {
          $this->admin_view('reservations/inhouse');
          }

          function completed() {
          $this->admin_view('reservations/completed');
          }

          function cancelled() {
          $this->admin_view('reservations/cancelled');
          }

          function unconfirmed() {
          $this->admin_view('reservations/unconfirmed');
          }
         */
    }
    