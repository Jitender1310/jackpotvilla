<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Quick_reservation extends MY_Controller {

        public function __construct() {
            parent::__construct();
            $this->login_required();
            if (!permission_required(61, "add")) {
                redirect("login");
            }
        }

        function index() {
            $this->data["is_past_reservation"] = false;
            $this->admin_view('quick_reservation');
        }

    }
    