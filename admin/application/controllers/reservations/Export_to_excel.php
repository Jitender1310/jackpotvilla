<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 0);

class Export_to_excel extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('export_model');
        $this->load->model('agents_model');
        $this->load->model('user_model');
    }

    function index() {
        $this->login_required();
        $users_id = get_user_id();
        $this->data["assigned_centers"] = $this->centers_model->get_assigned_centers($users_id);
        $this->admin_view('export_reservations');
    }

    function generate() {
        $filters = array();
        $filters["search_key"] = $this->input->get_post('search_key') ? $this->input->get_post('search_key') : '';
        $filters["sort_by"] = $this->input->get_post('sort_by') ? $this->input->get_post('sort_by') : '';
        $filters["from_date"] = $this->input->get_post('from_date') ? convert_date_to_db_format($this->input->get_post('from_date') . ' 00:00 AM', 'd-m-Y h:i A') : '';
        $filters["to_date"] = $this->input->get_post('to_date') ? convert_date_to_db_format($this->input->get_post('to_date') . ' 11:59 PM', 'd-m-Y h:i A') : '';
        $filters["centers_id"] = $this->input->get_post('centers_id') ? $this->input->get_post('centers_id') : '';
        $filters["booking_status"] = $this->input->get_post('booking_status') ? $this->input->get_post('booking_status') : '';
        $filters["customers_id"] = $this->input->get_post('customers_id') ? $this->input->get_post('customers_id') : '';
        $filters["agents_id"] = $this->input->get_post('agents_id') ? $this->input->get_post('agents_id') : '';
        $filters["source"] = $this->input->get_post('source') ? $this->input->get_post('source') : '';

        $filters["limit"] = $this->reservations_model->get_reservations_list($filters);
        $filters["start"] = 0;
        $result = $this->export_model->get_reservations_excel($filters);

        foreach ($result as $item) {
            //$item->reservation_details = $this->reservations_model->get_reservation_details($item->reservations_id);
            $item->reservation_details = $this->export_model->get_reservation($item->reservations_id);
            $item->center_name = $this->centers_model->get_center_name($item->reservation_details->centers_id);
            $item->payment_details = $this->export_model->get_payment($item->reservations_id);
            $item->stay_type = $this->export_model->staying_type_categories($item->staying_type_categories_id);
            $item->cgst = $this->export_model->get_tax($item->reservations_id, 'CGST')->applied_tax_amount;
            $item->sgst = $this->export_model->get_tax($item->reservations_id, 'SGST')->applied_tax_amount;

            $item->payment_gateway = 0;
            $item->cash = 0;
            $item->card = 0;
            $item->other = 0;
            $item->agent_collection = 0;
            $item->paytm = 0;
            $item->phonepay = 0;
            $item->cheque = 0;
            $item->dd = 0;
            $item->bank_transfer = 0;

            foreach ($item->payment_details as $p_item) {
                if ($p_item->payment_method == "Cash") {
                    $item->cash += $p_item->amount;
                }
                if ($p_item->payment_method == "Credit Card") {
                    $item->card += $p_item->amount;
                }
                if ($p_item->payment_method == "Paytm") {
                    $item->paytm += $p_item->amount;
                }
                if ($p_item->payment_method == "Phonepay") {
                    $item->phonepay += $p_item->amount;
                }
                if ($p_item->payment_method == "Other") {
                    $item->other += $p_item->amount;
                }
                if ($p_item->payment_method == "Bank Transfer") {
                    $item->bank_transfer += $p_item->amount;
                }
                if ($p_item->payment_method == "DD") {
                    $item->dd += $p_item->amount;
                }

                if ($p_item->payment_method == "Freshup Agent Collection") {
                    $item->agent_collection += $p_item->amount;
                }

                if ($p_item->payment_method == "Payment Gateway" || $p_item->payment_method == "Online") {
                    $item->payment_gateway += $p_item->amount;
                }

                if ($p_item->payment_method == "Cheque") {
                    $item->cheque += $p_item->amount;
                }
            }
        }


        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=reservations_data.csv');
        $output = fopen("php://output", "w");
        fputcsv($output, array("Center name", "Booking ID", "Name", "Gender", "Mobile", "Email ID", "Duration",
            "Check-in Date&Time", "Item", "Inventory Code", "Price", "Subtotal", "Discount", "CGST", "SGST", "Grand Total", "Payment Mode",
            "Payment Ref.no", "Payment Status", "Booking Status", "Booking From",
            "Booked By", "Refund Amount", "Payment Refund Ref Id",
            "Payment Gateway", "Cash", "Credit/Debit Card", "Other",
            "Freshup Agent Collection", "Paytm", "Phonepay", "Cheque",
            "DD", "Bank Transfer"
        ));

        foreach ($result as $item) {
            //$row["created_at"] = date("m-d-Y h:iA", $item->created_at);
            $row["Center name"] = $item->center_name;
            $row["Booking ID"] = $item->reservation_details->booking_ref_number;
            $row["Name"] = $item->person_name;
            $row["Gender"] = $item->gender;
            $row["Mobile"] = $item->reservation_details->contact_mobile;
            $row["Email ID"] = $item->reservation_details->contact_email;
            $row["Duration"] = $item->reservation_details->duration;
            $row["Check-in Date&Time"] = $item->check_in_date_time;
            $row["Item"] = $item->stay_type->category_name;
            $row["Inventory Code"] = $item->staying_categories_centers_inventory_code;
            $row["Price"] = $item->total_price;
            $row["Subtotal"] = $item->reservation_details->sub_total;
            $row["Discount"] = $item->reservation_details->applied_discount_amount;
            $row["CGST"] = $item->cgst;
            $row["SGST"] = $item->sgst;
            $row["Grand Total"] = $item->reservation_details->grand_total;
            if (count($item->payment_details) > 1) {
                $row["Payment Mode"] = "Split";
            } else {
                $row["Payment Mode"] = $item->payment_details[0]->payment_method;
            }

            $row["Payment Ref.no"] = $item->payment_details->transaction_number;
            $row["Payment Status"] = $item->reservation_details->payment_status;
            $row["Booking Status"] = $item->reservation_details->booking_status;
            $row["Booking From"] = ucwords(str_replace('_', ' ', $item->reservation_details->creation_source));
            $row["Booked By"] = ucwords(str_replace('_', ' ', $item->reservation_details->booked_by_users_name));
            $row["Refund Amount"] = $item->refund_amount;
            $row["Payment Refund Ref Id"] = $item->payment_refund_id;


            $row["Payment Gateway"] = $item->payment_gateway;
            $row["Cash"] = $item->cash;
            $row["Credit/Debit Card"] = $item->card;
            $row["Other"] = $item->other;
            $row["Freshup Agent Collection"] = $item->agent_collection;
            $row["Paytm"] = $item->paytm;
            $row["Phonepay"] = $item->phonepay;
            $row["Cheque"] = $item->cheque;
            $row["DD"] = $item->dd;
            $row["Bank Transfer"] = $item->bank_transfer;

            fputcsv($output, $row);
        }
        fclose($output);
    }

}
