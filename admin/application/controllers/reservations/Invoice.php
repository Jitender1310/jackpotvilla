<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends MY_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('reservations_model');
        $this->load->model('user_model');
        $this->load->model('companies_model');
        $this->load->model('agents_model');
        $this->load->model('centers_model');
        $this->load->model('countries_model');
        $this->load->model('how_did_hear_about_us_model');
        $this->load->model('business_sources_model');
        $this->load->model('tax_slabs_model');
        $this->load->model('states_model');
        $this->load->model('cities_model');
        $this->load->model('locations_model');
        $this->load->model('staying_type_categories_model');
    }

    function index() {
        $this->login_required();
        $booking_ref_number = $this->input->get_post("booking_ref_number");
        $this->data["reserverationObj"] = $this->reservations_model->get_reservation_details($booking_ref_number);



        $this->admin_view('invoice');
    }

    function pdf_view() {
        $booking_ref_number = $this->input->get_post("booking_ref_number");
        $this->data["reserverationObj"] = $this->reservations_model->get_reservation_details($booking_ref_number);
        $this->print_pdf_view("includes/invoice_code");
    }

}
