<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class View extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_required();
    }

    function index() {
        $this->admin_view('reservation_view');
    }

}
