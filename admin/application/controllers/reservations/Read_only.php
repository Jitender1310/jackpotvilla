<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Read_only extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_required();
    }

    function index() {
        if (!permission_required(69, "view")) {
            redirect("login");
        }
        $this->admin_view('read_only_reservations');
    }

    function admin_view_of_reservations() {
        if (!permission_required(79, "view")) {
            redirect("login");
        }
        $this->admin_view('admin_view_for_reservations');
    }

}
