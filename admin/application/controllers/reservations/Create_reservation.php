<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Create_reservation extends MY_Controller {

        public function __construct() {
            parent::__construct();
            $this->login_required();
            if (!permission_required(34, "view")) {
                redirect("login");
            }
        }

        function index() {
            $this->data["is_past_reservation"] = false;
            $this->admin_view('new_reservation');
        }

    }
    