<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class View_file extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_required();
    }

    function index() {
        $file = $this->input->get_post('file');
        $filename = basename($file);
        $file_extension = strtolower(substr(strrchr($filename, "."), 1));

        switch ($file_extension) {
            case "gif": $ctype = "image/gif";
                break;
            case "png": $ctype = "image/png";
                break;
            case "jpeg":
            case "jpg": $ctype = "image/jpeg";
                break;
            default:
        }
        header('Content-type: ' . $ctype);
        $image = base_url() . 'uploads/center_licence_documents/' . base64_decode($this->input->get_post('file'));
        $content = file_get_contents($image);
        echo $content;
        exit();
    }

}
