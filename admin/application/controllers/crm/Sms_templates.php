<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sms_templates extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_required();
        if (!permission_required(67, "view")) {
            redirect("login");
        }
    }

    function index() {
        $this->admin_view("crm/sms_templates");
    }

}
