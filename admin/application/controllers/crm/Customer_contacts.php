<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_contacts extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_required();
        if (!permission_required(66, "view")) {
            redirect("login");
        }
    }

    function index() {
        $this->admin_view("crm/customer_contacts");
    }

}
