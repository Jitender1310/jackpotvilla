<?php

class Pdf_generate extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function online_booking_order_view() {
        $booking_id = $this->input->get_post("booking_id");
        $response = $this->business_model->get_booking_order_details($booking_id);
        $this->data['data'] = $response;
        $this->print_pdf_view("invoice_view");
    }

}
