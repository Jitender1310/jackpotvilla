<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Send_Kyc_Notification_Email extends MY_Controller
{

	function __construct() {
		parent::__construct();
	}

	function index() {
		
		$data = array(
			'name' => $this->input->get('fullname'),
			'messages' => array(
				array(
					'name'=>$this->input->get('document'),
					'status'=>$this->input->get('status'),
					'reason'=>$this->input->get('reason')
				)
			)
		);
		echo $this->load->view("mail_templates/kyc_status", $data, true);
	}

}
