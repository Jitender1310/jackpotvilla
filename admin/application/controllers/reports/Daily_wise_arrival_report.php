<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Daily_wise_arrival_report extends MY_Controller {

        public function __construct() {
            parent::__construct();
            $this->login_required();

            if (!permission_required(74, "view")) {
                redirect("login");
            }
            $this->load->model('daily_wise_arrival_report_model');
            $this->load->model('centers_model');
            $this->load->model('customer_contacts_model');
        }

        function index() {
            $this->admin_view('reports/daily_wise_arrival_report');
        }

        function download() {

            $from_date = $this->input->get_post('from_date') ? $this->input->get_post('from_date') : date('01-m-Y');
            $to_date = $this->input->get_post('to_date') ? $this->input->get_post('to_date') : date('d-m-Y');
            $begin = new DateTime($from_date);
            $end = new DateTime($to_date);
            $end->modify('+1 day');
            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($begin, $interval, $end);
            $data = [];
            foreach ($period as $dt) {
                $data[]->date = $dt->format("d-m-Y");
            }
            foreach ($data as $item1) {
                $pax = 0;
                $cash = 0;
                $cards = 0;
                $online = 0;
                $other = 0;
                $net_grand_total = 0;
                $grand = 0;
                $booking_filters['from_date'] = convert_date_to_db_format($item1->date . ' 00:01 AM', 'd-m-Y h:i A');
                $booking_filters['to_date'] = convert_date_to_db_format($item1->date . ' 11:59 PM', 'd-m-Y h:i A');
                $booking_filters["centers_id"] = $this->input->get_post('centers_id') ? $this->input->get_post('centers_id') : '';
                $item1->reservations = $this->daily_wise_arrival_report_model->get_all_reservations($booking_filters);
                foreach ($item1->reservations as $item) {
                    if ($item->creation_source == 'website' || $item->creation_source == 'android_app' || $item->creation_source == 'ios_app') {
                        $item->nativity = $this->customer_contacts_model->get_customer_details_by_id($item->booked_by_users_id)->customer_meta->city_name;
                    } else {
                        $item->nativity = '';
                    }
                    $item->duration = round($item->duration);
                    $item->creation_source = ucwords(str_replace('_', ' ', $item->creation_source));
                    $item->center_name = $this->centers_model->get_center_name($item->centers_id);
                    $item->rooms = $this->daily_wise_arrival_report_model->get_room_types($item->id);
                    $item->pax = $this->daily_wise_arrival_report_model->get_pax_count($item->id);
                    $item->cash = $this->daily_wise_arrival_report_model->get_amount_by_pament_method_and_reservations_id('Cash', $item->id);
                    $item->cards = $this->daily_wise_arrival_report_model->get_amount_by_pament_method_and_reservations_id('Credit Card', $item->id);
                    $item->online = $this->daily_wise_arrival_report_model->get_amount_by_pament_method_and_reservations_id('Payment Gateway', $item->id);
                    $item->other = $this->daily_wise_arrival_report_model->get_amount_by_pament_method_and_reservations_id('Other', $item->id);
                    $item->grand = $item->cash + $item->cards + $item->online + $item->other;

                    $pax += $item->pax;
                    $cash += $item->cash;
                    $cards += $item->cards;
                    $online += $item->online;
                    $other += $item->other;
                    $net_grand_total += $item->grand_total;
                    $grand += $item->grand;
                }

                $item1->total_pax = $pax;
                $item1->total_cash = $cash;
                $item1->total_cards = $cards;
                $item1->total_online = $online;
                $item1->total_other = $other;
                $item1->net_grand_total = $net_grand_total;
                $item1->total_grand = $grand;
            }
            if ($this->input->get_post('centers_id') != '') {
                $center_name = $this->centers_model->get_center_name($this->input->get_post('centers_id'));
            } else {
                $center_name = '';
            }





            require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';

            // Create new Spreadsheet object

            $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
            $styleArray = array(
                'font' => array(
                    'bold' => true,
                ),
                'alignment' => array(
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ),
                'borders' => array(
                    'top' => array(
                        'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ),
                ),
                'fill' => array(
                    'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                    'rotation' => 90,
                    'startcolor' => array(
                        'argb' => 'FCF60C',
                    ),
                    'endcolor' => array(
                        'argb' => 'FCF60C',
                    ),
                ),
            );

            $styleArray1 = array(
                'font' => array(
                    'bold' => true,
                ),
                'alignment' => array(
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ),
            );

            $styleArray2 = array(
                'font' => array(
                    'bold' => false,
                ),
                'alignment' => array(
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ),
            );
            $styleArray3 = array(
                'font' => array(
                    'bold' => false,
                    'color' => array('rgb' => 'F10808'),
                ),
                'alignment' => array(
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ),
            );

            $styleArray4 = array(
                'alignment' => array(
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ),
            );




            $spreadsheet->getActiveSheet()->getStyle('A1:Q1')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A2:Q2')->applyFromArray($styleArray1);
            $spreadsheet->getActiveSheet()->getStyle('M3:P3')->applyFromArray($styleArray1);
            $spreadsheet->getActiveSheet()->getStyle('A')->applyFromArray($styleArray2);
//        $spreadsheet->getActiveSheet()->getStyle('A3:F3')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->mergeCells('A1:Q1');
            if ($from_date != '' || $to_date != '') {
                $content = "Between " . $from_date . " - " . $to_date;
            } else {
                $content = "";
            }
            $spreadsheet->getActiveSheet()->setCellValue('A1', 'Daily wise arrival report ' . $content);
//        auto fit column to content

            $spreadsheet->getActiveSheet()->mergeCells('A2:A3');
            $spreadsheet->getActiveSheet()->mergeCells('B2:B3');
            $spreadsheet->getActiveSheet()->mergeCells('C2:C3');
            $spreadsheet->getActiveSheet()->mergeCells('D2:D3');
            $spreadsheet->getActiveSheet()->mergeCells('E2:E3');
            $spreadsheet->getActiveSheet()->mergeCells('F2:F3');
            $spreadsheet->getActiveSheet()->mergeCells('G2:G3');
            $spreadsheet->getActiveSheet()->mergeCells('H2:H3');
            $spreadsheet->getActiveSheet()->mergeCells('I2:I3');
            $spreadsheet->getActiveSheet()->mergeCells('J2:J3');
            $spreadsheet->getActiveSheet()->mergeCells('K2:K3');
            $spreadsheet->getActiveSheet()->mergeCells('L2:L3');
            $spreadsheet->getActiveSheet()->mergeCells('M2:P2');
            $spreadsheet->getActiveSheet()->mergeCells('Q2:Q3');

//        auto fit column to content
//        foreach (range('A', 'Q') as $columnID) {
//            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
//                    ->setAutoSize(true);
//        }
            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(5);
            $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(15);
            $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(15);
            $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(15);
            $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(10);
            $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(35);
            $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(10);
            $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(10);
            $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(10);
            $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(10);
            $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(10);
            $spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(10);
            $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
            // set the names of header cells

            $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue("A2", 'Date')
                    ->setCellValue("B2", 'S.No')
                    ->setCellValue("C2", 'Booking ID')
                    ->setCellValue("D2", 'Guest Name')
                    ->setCellValue("E2", 'Mobile Number')
                    ->setCellValue("F2", 'Email')
                    ->setCellValue("G2", 'Nativity')
                    ->setCellValue("H2", 'Center')
                    ->setCellValue("I2", 'Duration')
                    ->setCellValue("J2", 'Room Type')
                    ->setCellValue("K2", 'No Of Pax')
                    ->setCellValue("L2", 'Tariff')
                    ->setCellValue("M2", 'Paid by')
                    ->setCellValue("Q2", 'Grand Sale');


            $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue("M3", 'Cash')
                    ->setCellValue("N3", 'Cards')
                    ->setCellValue("O3", 'Online')
                    ->setCellValue("P3", 'Others');


            // Add some data

            $x = 4;
            foreach ($data as $sub) {
                if (count($sub->reservations) != 0) {
                    $count = $x + count($sub->reservations) - 1;
                } else {
                    $count = $x;
                }
                $i = $x;
                $j = 1;
                $k = $count + 1;
                if (count($sub->reservations) != 0) {
                    $k = $count + 1;
                } else {
                    $k = $count;
                }
                $spreadsheet->getActiveSheet()->mergeCells("A$x:A$count");
                $spreadsheet->setActiveSheetIndex(0)
                        ->setCellValue("A$x", $sub->date);
                if (count($sub->reservations) == 0 || count($sub->reservations) == '') {
                    $spreadsheet->getActiveSheet()->getStyle("B$x:Q$x")->applyFromArray($styleArray4);
                    $spreadsheet->getActiveSheet()->mergeCells("B$x:Q$x");
                    $spreadsheet->setActiveSheetIndex(0)
                            ->setCellValue("B$x", "No Reservations Found On This Date ");
                }

                if (count($sub->reservations) != 0) {
                    foreach ($sub->reservations as $item) {
                        $spreadsheet->setActiveSheetIndex(0)
                                ->setCellValue("B$i", $j)
                                ->setCellValue("C$i", $item->booking_ref_number)
                                ->setCellValue("D$i", $item->contact_name)
                                ->setCellValue("E$i", $item->contact_mobile)
                                ->setCellValue("F$i", $item->contact_email)
                                ->setCellValue("G$i", $item->nativity)
                                ->setCellValue("H$i", $item->center_name)
                                ->setCellValue("I$i", $item->duration . ' hrs')
                                ->setCellValue("J$i", $item->rooms)
                                ->setCellValue("K$i", $item->pax)
                                ->setCellValue("L$i", $item->grand_total)
                                ->setCellValue("M$i", $item->cash)
                                ->setCellValue("N$i", $item->cards)
                                ->setCellValue("O$i", $item->online)
                                ->setCellValue("P$i", $item->other)
                                ->setCellValue("Q$i", $item->grand);
                        $i++;
                        $j++;
                    }
                }

                if (count($sub->reservations) != 0) {
                    $spreadsheet->getActiveSheet()->getStyle("A$k:Q$k")->applyFromArray($styleArray3);
                    $spreadsheet->getActiveSheet()->mergeCells("A$k:J$k");
                    $spreadsheet->setActiveSheetIndex(0)
                            ->setCellValue("A$k", 'Grand Total')
                            ->setCellValue("K$k", $sub->total_pax)
                            ->setCellValue("L$k", $sub->net_grand_total)
                            ->setCellValue("M$k", $sub->total_cash)
                            ->setCellValue("N$k", $sub->total_cards)
                            ->setCellValue("O$k", $sub->total_online)
                            ->setCellValue("P$k", $sub->total_other)
                            ->setCellValue("Q$k", $sub->total_grand);
                }

                $x = $k;
                $x++;
            }
// Rename worksheet
//        foreach (range('A', $spreadsheet->getActiveSheet()->getHighestDataColumn()) as $col) {
//            $spreadsheet->getActiveSheet()
//                    ->getColumnDimension($col)
//                    ->setAutoSize(true);
//        }
//        foreach (range('A', $spreadsheet->getActiveSheet()->getHighestDataColumn()) as $col) {
//            $spreadsheet->getActiveSheet()
//                    ->getColumnDimension($col)
//                    ->setAutoSize(true);
//        }
            $spreadsheet->getActiveSheet()->setTitle('Sales_Information');

// set right to left direction
//		$spreadsheet->getActiveSheet()->setRightToLeft(true);
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;
        filename = "sales_reports_sheet.xlsx"');
            header('Cache-Control: max-age = 0');
// If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Excel2007');
            $writer->save('php://output');
            exit;
        }

    }
    