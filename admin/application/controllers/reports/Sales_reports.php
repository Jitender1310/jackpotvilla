<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_reports extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_required();
        if (!permission_required(72, "view")) {
            redirect("login");
        }
        $this->load->model('centers_model');
        $this->load->model('sales_reports_model');
    }

    function index() {
        $this->admin_view('reports/sales_reports');
    }

    public function download() {

        $begin = new DateTime($this->input->get_post('from_date'));
        $end = new DateTime($this->input->get_post('to_date'));
        $end->modify('+1 day');
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        $dates = [];
        foreach ($period as $dt) {
            $dates[] = $dt->format("d-m-Y");
        }

        $data['sales'] = [];
        foreach ($dates as $item) {
            $booking_filters['date'] = $item;
            $booking_filters['from_date'] = convert_date_to_db_format($item . ' 00:00 AM', 'd-m-Y h:i A');
            $booking_filters['to_date'] = convert_date_to_db_format($item . ' 11:59 PM', 'd-m-Y h:i A');
            $booking_filters["centers_id"] = $this->input->get_post('centers_id') ? $this->input->get_post('centers_id') : '';
            $data['sales'][] = $this->sales_reports_model->get_sales($booking_filters);
        }
        $all['from_date'] = $this->input->get_post('from_date') ? convert_date_to_db_format($this->input->get_post('from_date') . ' 00:00 AM', 'd-m-Y h:i A') : '';
        $all['to_date'] = $this->input->get_post('to_date') ? convert_date_to_db_format($this->input->get_post('to_date') . ' 11:59 PM', 'd-m-Y h:i A') : '';
        $all["centers_id"] = $this->input->get_post('centers_id') ? $this->input->get_post('centers_id') : '';

        $cash_sales = round($this->sales_reports_model->get_cash_sales($all)->total);
        $credit_card_sales = round($this->sales_reports_model->get_credit_card_sales($all)->total);
        $other_sales = round($this->sales_reports_model->get_other_sales($all)->total);
        $online_sales = round($this->sales_reports_model->get_online_sales($all)->total);
        $total_sales = $cash_sales + $credit_card_sales + $online_sales + $other_sales;
        if ($this->input->get_post('centers_id') != '') {
            $center_name = 'In ' . $this->centers_model->get_center_name($this->input->get_post('centers_id')) . " Center";
        } else {
            $center_name = 'In All Centers';
        }


        require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';

        // Create new Spreadsheet object

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'top' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
            ),
            'fill' => array(
                'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startcolor' => array(
                    'argb' => 'FFA0A0A0',
                ),
                'endcolor' => array(
                    'argb' => 'FFFFFFFF',
                ),
            ),
        );

        $spreadsheet->getActiveSheet()->getStyle('A1:F1')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('A2:F2')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('A3:F3')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->mergeCells('A1:F1');
        $spreadsheet->getActiveSheet()->setCellValue('A1', 'Sales reports between dates ' . $this->input->get_post('from_date') . '  -  ' . $this->input->get_post('to_date') . ' ' . $center_name . '');
//        auto fit column to content

        $spreadsheet->getActiveSheet()->mergeCells('A2:F2');
        $spreadsheet->getActiveSheet()->setCellValue('A2', $center_name . " Total Sale Amount - " . $total_sales . " Rs/-");
//        auto fit column to content

        foreach (range('A', 'F') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
        }
        // set the names of header cells

        $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue("A3", 'Date')
                ->setCellValue("B3", 'Cash (Rs/-)')
                ->setCellValue("C3", 'Credit/Debit Card (Rs/-)')
                ->setCellValue("D3", 'Online (Rs/-)')
                ->setCellValue("E3", 'Other types (Rs/-)')
                ->setCellValue("F3", 'Grand Total (Rs/-)');

        // Add some data

        $x = 4;
        foreach ($data['sales'] as $sub) {
            $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue("A$x", $sub['date'])
                    ->setCellValue("B$x", $sub['cash_sales'])
                    ->setCellValue("C$x", $sub['credit_card_sales'])
                    ->setCellValue("D$x", $sub['online_sales'])
                    ->setCellValue("E$x", $sub['other_sales'])
                    ->setCellValue("F$x", $sub['grand_total']);
            $x++;
        }


// Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Sales_Information');

// set right to left direction
//		$spreadsheet->getActiveSheet()->setRightToLeft(true);
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename = "sales_reports_sheet.xlsx"');
        header('Cache-Control: max-age = 0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Excel2007');
        $writer->save('php://output');
        exit;

        //  create new file and remove Compatibility mode from word title
    }

}
