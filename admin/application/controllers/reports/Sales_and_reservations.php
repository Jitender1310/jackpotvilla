<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Sales_and_reservations extends MY_Controller {

        public function __construct() {
            parent::__construct();
            $this->login_required();
            if (!permission_required(73, "view")) {
                redirect("login");
            }
            $this->load->model('centers_model');
            $this->load->model('sales_and_reservations_model');
        }

        function index() {
            $this->admin_view('reports/sales_and_reservations');
        }

        public function download() {
            $booking_filters['from_date'] = convert_date_to_db_format($this->input->get_post('from_date') . ' 00:00 AM', 'd-m-Y h:i A');
            $booking_filters['to_date'] = convert_date_to_db_format($this->input->get_post('to_date') . ' 11:59 PM', 'd-m-Y h:i A');
            $booking_filters["centers_id"] = $this->input->get_post('centers_id') ? $this->input->get_post('centers_id') : '';
            $booking_filters["payment_method"] = $this->input->get_post('payment_method') ? $this->input->get_post('payment_method') : '';
            if ($this->input->get_post('centers_id') != '') {
                $center_name = $this->centers_model->get_center_name($this->input->get_post('centers_id'));
            } else {
                $center_name = 'All centers';
            }

            $cash_sales = round($this->sales_and_reservations_model->get_cash_sales($booking_filters)->total);
            $credit_card_sales = round($this->sales_and_reservations_model->get_credit_card_sales($booking_filters)->total);
            $online_sales = round($this->sales_and_reservations_model->get_online_sales($booking_filters)->total);
            $other_sales = round($this->sales_and_reservations_model->get_other_sales($booking_filters)->total);
            $total_sales = $cash_sales + $credit_card_sales + $online_sales + $other_sales;
            if ($booking_filters["payment_method"] != '') {
                if ($booking_filters["payment_method"] == 'Credit Card') {
                    $payment_method = 'By Payment method - Credit/Debit Cards';
                    $sales = $credit_card_sales;
                }
                if ($booking_filters["payment_method"] == 'Payment Gateway') {
                    $payment_method = 'By Payment method - Online';
                    $sales = $online_sales;
                }
                if ($booking_filters["payment_method"] == 'Other') {
                    $payment_method = 'By Other types of Payment';
                    $sales = $other_sales;
                }
                if ($booking_filters["payment_method"] == 'Cash') {
                    $payment_method = 'By Payment method - Cash';
                    $sales = $cash_sales;
                }
            } else {
                $payment_method = 'By All Payment methods';
            }

            $data = $this->sales_and_reservations_model->get_all_reservations($booking_filters);


            require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';

            // Create new Spreadsheet object

            $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
            $styleArray = array(
                'font' => array(
                    'bold' => true,
                ),
                'alignment' => array(
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ),
                'borders' => array(
                    'top' => array(
                        'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ),
                ),
                'fill' => array(
                    'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                    'rotation' => 90,
                    'startcolor' => array(
                        'argb' => 'BEC754',
                    ),
                    'endcolor' => array(
                        'argb' => 'FFFFFFFF',
                    ),
                ),
            );

            $spreadsheet->getActiveSheet()->getStyle('A1:I1')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A2:I2')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A3:I3')->applyFromArray($styleArray);

            $spreadsheet->getActiveSheet()->mergeCells('A1:I1');
            $spreadsheet->getActiveSheet()->setCellValue('A1', "Total Sale Amount - " . $total_sales . " Rs/-");
//        auto fit column to content

            $spreadsheet->getActiveSheet()->mergeCells('A2:I2');
            $spreadsheet->getActiveSheet()->setCellValue('A2', $center_name . " Sale Amount - " . $sales . " Rs/-" . $payment_method);
//        auto fit column to content

            foreach (range('A', 'I') as $columnID) {
                $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                        ->setAutoSize(true);
            }
            // set the names of header cells

            $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue("A3", 'Booking ID')
                    ->setCellValue("B3", 'Name')
                    ->setCellValue("C3", 'Mobile')
                    ->setCellValue("D3", 'Center')
                    ->setCellValue("E3", 'Check-in Date Time')
                    ->setCellValue("F3", 'Amount')
                    ->setCellValue("G3", 'Payment Method')
                    ->setCellValue("H3", 'Booking Status')
                    ->setCellValue("I3", 'Booked From');

            // Add some data

            $x = 4;
            foreach ($data as $sub) {
                $spreadsheet->setActiveSheetIndex(0)
                        ->setCellValue("A$x", $sub->booking_ref_number)
                        ->setCellValue("B$x", $sub->contact_name)
                        ->setCellValue("C$x", $sub->contact_mobile)
                        ->setCellValue("D$x", $sub->center_name)
                        ->setCellValue("E$x", $sub->check_in_date_time)
                        ->setCellValue("F$x", $sub->net_amount)
                        ->setCellValue("G$x", $sub->payment_method)
                        ->setCellValue("H$x", $sub->booking_status)
                        ->setCellValue("I$x", $sub->creation_source);
                $x++;
            }
// Rename worksheet
            $spreadsheet->getActiveSheet()->setTitle('Sales_Reservations_Information');

// set right to left direction
//		$spreadsheet->getActiveSheet()->setRightToLeft(true);
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename = "sales_and_reservations.xlsx"');
            header('Cache-Control: max-age = 0');
// If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Excel2007');
            $writer->save('php://output');
            exit;

            //  create new file and remove Compatibility mode from word title
        }

    }
    