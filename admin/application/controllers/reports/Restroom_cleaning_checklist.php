<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    class Restroom_cleaning_checklist extends MY_Controller {

        private $model_name = "restroom_cleaning_checklist_model";
        private $title = "Restroom cleaning checklist";

        function __construct() {
            parent::__construct();
            $this->login_required();
            if (!permission_required(102, "view")) {
                redirect("login");
            }
            $this->load->model($this->model_name);
            $this->load->model('centers_model');
            $this->load->model('restroom_cleaning_checklist_categories_model');
        }

        function index() {
            $this->admin_view("reports/restroom_cleaning_checklist");
        }

        public function download() {

            $centers_id = $this->input->get_post('centers_id') ? $this->input->get_post('centers_id') : '';
            $cleaning_staff_id = $this->input->get_post('staff_id') ? $this->input->get_post('staff_id') : '';
            if ($centers_id != '') {
                $center_name = " In " . $this->centers_model->get_center_name($centers_id);
            } else {
                $center_name = '';
            }

            if ($cleaning_staff_id != '') {
                $staff_name = " By " . $this->{$this->model_name}->get_staff_name($cleaning_staff_id)->name;
            } else {
                $staff_name = '';
            }

            $from_date = $this->input->get_post('from_date') ? $this->input->get_post('from_date') : date('d-m-Y');
            $to_date = $this->input->get_post('to_date') ? $this->input->get_post('to_date') : date('d-m-Y');
            $begin = new DateTime($from_date);
            $end = new DateTime($to_date);
            $end->modify('+1 day');
            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($begin, $interval, $end);
            $data = [];
            foreach ($period as $dt) {
                $data[]->date = $dt->format("d-m-Y");
            }
            foreach ($data as $item1) {
                $item1->checklist = $this->{$this->model_name}->get_check_list($item1->date, $centers_id, $cleaning_staff_id);
                $item1->date = date('d-M-Y', strtotime($item1->date));
                $item1->center_name = $center_name;
            }
            require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';

            // Create new Spreadsheet object

            $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
            $styleArray = array(
                'font' => array(
                    'bold' => true,
                ),
                'alignment' => array(
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ),
                'borders' => array(
                    'top' => array(
                        'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ),
                ),
                'fill' => array(
                    'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                    'rotation' => 90,
                    'startcolor' => array(
                        'argb' => 'fff76f',
                    ),
                    'endcolor' => array(
                        'argb' => 'fff76f',
                    ),
                ),
            );

            $styleArray1 = array(
                'font' => array(
                    'bold' => true,
                ),
            );


//        auto fit column to content

            foreach (range('A', 'M') as $columnID) {
                $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                        ->setAutoSize(true);
            }
            // set the names of header cells
            $x = 1;
            foreach ($data as $item) {
                $y = $x + 1;
                $spreadsheet->getActiveSheet()->getStyle("A$x:M$x")->applyFromArray($styleArray);
                $spreadsheet->getActiveSheet()->getStyle("A$y:M$y")->applyFromArray($styleArray1);

                $spreadsheet->getActiveSheet()->mergeCells("A$x:M$x");
                $spreadsheet->getActiveSheet()->setCellValue("A$x", "Restroom Cleaning Checklist on " . $item->date . $center_name . $staff_name);

                $spreadsheet->setActiveSheetIndex(0)
                        ->setCellValue("A$y", 'Area')
                        ->setCellValue("B$y", '7:00 AM')
                        ->setCellValue("C$y", '9:00 AM')
                        ->setCellValue("D$y", '11:00 AM')
                        ->setCellValue("E$y", '1:00 PM')
                        ->setCellValue("F$y", '3:00 PM')
                        ->setCellValue("G$y", '6:00 PM')
                        ->setCellValue("H$y", '8:00 PM')
                        ->setCellValue("I$y", '11:00 PM')
                        ->setCellValue("J$y", '12:00 AM')
                        ->setCellValue("K$y", '2:00 AM')
                        ->setCellValue("L$y", '4:00 AM')
                        ->setCellValue("M$y", '6:00 AM');
                $z = $y + 1;
                foreach ($item->checklist as $sub) {
                    $spreadsheet->setActiveSheetIndex(0)
                            ->setCellValue("A$z", $sub->restroom_cleaning_checklist_category_name)
                            ->setCellValue("B$z", $sub->time1 === 0 ? 'Not Done' : $sub->time1->staff_name . ' - ' . $sub->time1->time_stamp)
                            ->setCellValue("C$z", $sub->time2 === 0 ? 'Not Done' : $sub->time2->staff_name . ' - ' . $sub->time2->time_stamp)
                            ->setCellValue("D$z", $sub->time3 === 0 ? 'Not Done' : $sub->time3->staff_name . ' - ' . $sub->time3->time_stamp)
                            ->setCellValue("E$z", $sub->time4 === 0 ? 'Not Done' : $sub->time4->staff_name . ' - ' . $sub->time4->time_stamp)
                            ->setCellValue("F$z", $sub->time5 === 0 ? 'Not Done' : $sub->time5->staff_name . ' - ' . $sub->time5->time_stamp)
                            ->setCellValue("G$z", $sub->time6 === 0 ? 'Not Done' : $sub->time6->staff_name . ' - ' . $sub->time6->time_stamp)
                            ->setCellValue("H$z", $sub->time7 === 0 ? 'Not Done' : $sub->time7->staff_name . ' - ' . $sub->time7->time_stamp)
                            ->setCellValue("I$z", $sub->time8 === 0 ? 'Not Done' : $sub->time8->staff_name . ' - ' . $sub->time8->time_stamp)
                            ->setCellValue("J$z", $sub->time9 === 0 ? 'Not Done' : $sub->time9->staff_name . ' - ' . $sub->time9->time_stamp)
                            ->setCellValue("K$z", $sub->time10 === 0 ? 'Not Done' : $sub->time10->staff_name . ' - ' . $sub->time10->time_stamp)
                            ->setCellValue("L$z", $sub->time11 === 0 ? 'Not Done' : $sub->time11->staff_name . ' - ' . $sub->time11->time_stamp)
                            ->setCellValue("M$z", $sub->time12 === 0 ? 'Not Done' : $sub->time12->staff_name . ' - ' . $sub->time12->time_stamp);
                    $y = $z;
                    $x = $z;
                    $z++;
                }

                $x = $z;
                $x++;
            }

            // Add some data
//        $x = 4;
//        foreach ($data as $sub) {
//            $spreadsheet->setActiveSheetIndex(0)
//                    ->setCellValue("A$x", $sub->booking_ref_number)
//                    ->setCellValue("B$x", $sub->contact_name)
//                    ->setCellValue("C$x", $sub->contact_mobile)
//                    ->setCellValue("D$x", $sub->center_name)
//                    ->setCellValue("E$x", $sub->check_in_date_time)
//                    ->setCellValue("F$x", $sub->net_amount)
//                    ->setCellValue("G$x", $sub->payment_method)
//                    ->setCellValue("H$x", $sub->booking_status)
//                    ->setCellValue("I$x", $sub->creation_source);
//            $x++;
//        }
// Rename worksheet
            $spreadsheet->getActiveSheet()->setTitle('Restroom_cleaning_checklist');

// set right to left direction
//		$spreadsheet->getActiveSheet()->setRightToLeft(true);
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename = "restroom_cleaning_checklist.xlsx"');
            header('Cache-Control: max-age = 0');
// If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Excel2007');
            $writer->save('php://output');
            exit;

            //  create new file and remove Compatibility mode from word title
        }

    }
    