<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    class Restroom_feedback extends MY_Controller {

        function __construct() {
            parent::__construct();
            $this->login_required();
            if (!permission_required(80, "view")) {
                redirect("login");
            }
            $this->load->model('restroom_feedback_model');
            $this->load->model('centers_model');
        }

        function index() {
            $this->admin_view("reports/restroom_feedback");
        }

        function generate() {
            $filters = array();
            $filters['from_date'] = $this->input->get_post('from_date') ? strtotime($this->input->get_post('from_date')) : '';
            $filters['to_date'] = $this->input->get_post('to_date') ? strtotime($this->input->get_post('to_date') . ' 23:59:59') : '';
            $filters["centers_id"] = $this->input->get_post('centers_id') ? $this->input->get_post('centers_id') : '';
            $filters["rate"] = $this->input->get_post('rate') ? $this->input->get_post('rate') : '';
            if ($this->input->get_post('centers_id') != '') {
                $center_name = $this->centers_model->get_center_name($this->input->get_post('centers_id'));
            } else {
                $center_name = '';
            }

            $filters["limit"] = 10000000;
            $filters["start"] = 0;
            $data = $this->restroom_feedback_model->get_all_feedback_list($filters);
            foreach ($data as $item) {
                $item->center_name = $this->centers_model->get_center_name($item->centers_id);
                $item->created_at = date('d-M-Y h:i A', $item->created_at);
            }

            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=restroom_feedback.csv');
            $output = fopen("php://output", "w");
            fputcsv($output, array("Rate", "Cause", "Remarks", "Center", "Mobile", "Feedback Submitted On"));

            foreach ($data as $item) {
                $row["Rate"] = $item->rate;
                $row["Cause"] = $item->cause;
                $row["Remarks"] = $item->remarks;
                $row["Center"] = $item->center_name;
                $row["Mobile"] = $item->mobile;
                $row["Feedback Submitted On"] = $item->created_at;
                fputcsv($output, $row);
            }
            fclose($output);
        }

    }
    