<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Reported_problems extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_required();
        if (!permission_required(16, "view")) {
            redirect("login");
        }
    }

    function index() {
        $this->admin_view("feedback/reported_problems");
    }

}
