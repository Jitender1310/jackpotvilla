<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Offer_notifications extends MY_Controller {

    private $model_name = "offer_notifications_model";
    private $table_name = "offer_notifications";

    public function __construct() {
        parent::__construct();
        $this->login_required();
        $this->load->model($this->model_name);
        if (!permission_required(22, "view")) {
            redirect("login");
        }
    }

    function index() {
        $this->admin_view("promotions/offer_notifications");
    }
    function add($id=null) {
         if (!empty($_POST)) {
            
            $url_alias_string = str_replace(' ', '_', $this->input->post('title')); // Replaces all spaces with hyphens.
            $url_alias_string =  strtolower(preg_replace('/[^A-Za-z0-9_\-]/', '', $url_alias_string)); // Removes special chars.
            
            $data = [
                "title" => $this->input->post('title'),
                "web_link" => $url_alias_string,
                "description" => $this->input->post('description')
            ];
            
            
            if (@$_FILES["image"]["name"]) {
                $config['upload_path'] = FILE_UPLOAD_FOLDER;
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = 1000;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;
                $this->load->library('upload', $config);
                
                $this->upload->do_upload('image');
                $uploaded_data = array('upload_data' => $this->upload->data());
                $file_name = $uploaded_data["upload_data"]["file_name"];
                $data["image"] = $file_name;
                var_dump($_FILES["image"]);
            }
            
        
            if ($id) {
                $this->{$this->model_name}->update_by($data, $id);
                $this->session->set_flashdata("msg", "Updated successfully.");
            }else{
                $id = $this->{$this->model_name}->add_by($data);
                $this->session->set_flashdata("msg", "Insert successfully.");
            }
            redirect("promotions/offer_notifications/add/".$id);
            die();
        }
        if($id != null){
            $this->data["offer_notifications"] = $this->db->where("id", $id)->get($this->table_name)->row();
            //var_dump($offer_notifications);die();
        }
        $this->admin_view("promotions/offer_notifications_add");
    }

}
