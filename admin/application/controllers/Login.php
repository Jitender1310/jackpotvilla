<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Login extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        if ($this->is_logged_in()) {
            redirect("dashboard");
        }
        $this->load->view("login");
    }

}
