<?php

class Refund_policy extends MY_Controller {

    private $model_name = "cms_model";
    private $title = "cms";

    public function __construct() {
        parent::__construct();
        $this->login_required();
        $this->load->model($this->model_name);
        if (!permission_required(118, "view")) {
            redirect("login");
        }
    }
	
	function index() {
        if (!empty($_POST)) {
            $data = [
                "description" => $this->input->get_post('description')
            ];
            $this->{$this->model_name}->update($data, 3);
            $this->session->set_flashdata("msg", "Updated successfully.");
            redirect("cms/refund_policy");
        }
        $this->data["description"] = $this->{$this->model_name}->get(3)->description;
        $this->admin_view("cms/refund_policy");
    }
	
}