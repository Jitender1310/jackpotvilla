<?php

class Withdraw_charges_info extends MY_Controller {

    private $model_name = "cms_model";
    private $title = "cms";

    public function __construct() {
        parent::__construct();
        $this->login_required();
        $this->load->model($this->model_name);
        if (!permission_required(118, "view")) {
            redirect("login");
        }
    }

    function index() {
        if (!empty($_POST)) {
            $data = [
                "description" => $this->input->get_post('description')
            ];
            $this->{$this->model_name}->update($data, 7);
            $this->session->set_flashdata("msg", "Updated successfully.");
            redirect("cms/withdraw_charges_info");
        }
        $this->data["description"] = $this->{$this->model_name}->get(7)->description;
        $this->admin_view("cms/withdraw_charges_info");
    }

}
