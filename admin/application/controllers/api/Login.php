<?php

class Login extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function index_post() {
        $this->form_validation->set_rules("username", "Username", "required", array(
            'required' => 'Username cannot be empty'
        ));
        $this->form_validation->set_rules("password", "Password", "required", array(
            'required' => 'Password cannot be empty'
        ));
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "username" => form_error("username"),
                "password" => form_error("password")
            ];
            $arr = [
                "status" => "invalid_form",
                "data" => $errors
            ];
            $this->response($arr);
            die;
        }

        $user_id = $this->login_model->is_username_exists($this->post("username"));
        if ($user_id == false || $user_id == "") {
            $arr = ['status' => "invalid",
                'title' => "",
                "error_type" => "not_exists",
                "message" => "User details not found"
            ];
            $this->response($arr);
            die;
        }
        if ($this->login_model->check_user_status($user_id) == false) {
            $arr = ['status' => "invalid",
                'title' => "",
                "error_type" => "account_inactive",
                "message" => "Your account is inactive"
            ];
            $this->response($arr);
            die;
        }

        if ($this->post('role_id')) {
            if ($this->login_model->check_user_role($user_id, $this->post('role_id')) == false) {
                $arr = ['status' => "invalid",
                    'title' => "",
                    "error_type" => "access_denied",
                    "message" => "Access Denied. Please contact admin for access."
                ];
                $this->response($arr);
                die;
            }
        }

        $this->verify_login($user_id, $this->post('password'));
    }

    function verify_login($user_id, $password) {
        $is_verified = $this->login_model->login_validation($user_id, $password);
        if ($is_verified) {
            $user_details = $this->user_model->get_user_details($user_id);
            $arr = [
                'status' => "valid",
                'title' => "Valid",
                "message" => "Login Successful",
                "data" => $user_details
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => "Invalid Credentials"
            ];
        }
        $this->response($arr);
    }

}
