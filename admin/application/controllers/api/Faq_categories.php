<?php

    class Faq_categories extends REST_Controller {

        private $model_name = "faq_categories_model";
        private $title = "Faq categories";

        function __construct() {
            parent::__construct();
            $this->load->model($this->model_name);
            check_for_access(25);
        }

        function index_get() {
            $data = $this->{$this->model_name}->get();
            $arr = [
                'status' => "valid",
                "message" => $this->title . " list",
                "data" => $data
            ];
            $this->response($arr);
        }

        function add_post() {
            if ($this->input->get_post("id")) {
                check_for_access(25, "update");
            } else {
                check_for_access(25, "add");
            }
            $this->form_validation->set_rules("category", "Category", "required", array(
                'required' => 'Category name cannot be empty'
            ));

            if ($this->form_validation->run() == FALSE) {
                $errors = [
                    "category" => form_error("category"),
                ];
                $arr = [
                    "status" => "invalid_form",
                    "data" => $errors
                ];
                $this->response($arr);
            }

            $data = [
                "category" => $this->post('category'),
            ];


            if (@$_FILES["icon"]["name"]) {
                $config['upload_path'] = FILE_UPLOAD_FOLDER . "faq_icons/";
                $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|JPG|PNG';
                $config['max_size'] = 10000;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('icon')) {
                    $this->data['error'] = array('error' => $this->upload->display_errors());
//                print_r($this->data['error']);
//                die;

                    $errors = [
                        "title" => form_error("title"),
                        "gender" => form_error("gender"),
                        "icon" => strip_tags($this->data['error']["error"]),
                    ];
                    $arr = [
                        "status" => "invalid_form",
                        "data" => $errors
                    ];
                    $this->response($arr);
                    die;
                } else {
                    $uploaded_data = array('upload_data' => $this->upload->data());
                    $file_name = $uploaded_data["upload_data"]["file_name"];
                    $data["icon"] = $file_name;
                }
            }


            if ($this->input->post("id")) {
                return $this->update($data, $this->post("id"));
            }

            $response = $this->{$this->model_name}->add($data);
            if ($response) {
                $arr = [
                    'status' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " added successfully"
                ];
            } else {
                $arr = [
                    'status' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not added, please try again"
                ];
            }
            $this->response($arr);
        }

        function update($data) {
            check_for_access(25);
            $response = $this->{$this->model_name}->update($data, $this->post('id'));
            if ($response) {
                $arr = [
                    'status' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " updated successfully"
                ];
            } else {
                $arr = [
                    'status' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not updated, please try again"
                ];
            }
            $this->response($arr);
        }

        function delete_post() {
            check_for_access(25);
            $id = (int) $this->post("id");
            $response = $this->{$this->model_name}->delete($id);
            if ($response) {
                $arr = [
                    'status' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " deleted successfully"
                ];
            } else {
                $arr = [
                    'status' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not deleted, please try again"
                ];
            }
            $this->response($arr);
        }

    }
    