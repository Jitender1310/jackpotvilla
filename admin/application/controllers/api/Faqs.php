<?php

    class Faqs extends REST_Controller {

        private $model_name = "faqs_model";
        private $title = "Faqs";

        function __construct() {
            parent::__construct();
            $this->load->model($this->model_name);
            check_for_access(25);
        }

        function index_get() {
            $bot_players = 1;
            $data = $this->{$this->model_name}->get($bot_players);
            $arr = [
                'status' => "valid",
                "message" => $this->title . " list",
                "data" => $data
            ];
            $this->response($arr);
        }

        function add_post() {
            if ($this->input->get_post("id")) {
                check_for_access(25, "update");
            } else {
                check_for_access(25, "add");
            }
            $this->form_validation->set_rules("faq_categories_id", "FAQ Category", "required", array(
                'required' => 'Category cannot be empty'
            ));
            $this->form_validation->set_rules("title", "Title", "required", array(
                'required' => 'Title cannot be empty'
            ));
            $this->form_validation->set_rules("description", "Description", "required", array(
                'required' => 'Description cannot be empty'
            ));

            if ($this->form_validation->run() == FALSE) {
                $errors = [
                    "category" => form_error("category"),
                ];
                $arr = [
                    "status" => "invalid_form",
                    "data" => $errors
                ];
                $this->response($arr);
            }

            $data = [
                "faq_categories_id" => $this->post('faq_categories_id'),
                "title" => $this->post('title'),
                "description" => $this->post('description'),
            ];

            if ($this->input->post("id")) {
                return $this->update($data, $this->post("id"));
            }

            $response = $this->{$this->model_name}->add($data);
            if ($response) {
                $arr = [
                    'status' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " added successfully"
                ];
            } else {
                $arr = [
                    'status' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not added, please try again"
                ];
            }
            $this->response($arr);
        }

        function update($data) {
            check_for_access(25);
            $response = $this->{$this->model_name}->update($data, $this->post('id'));

            if ($response) {
                $arr = [
                    'status' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " updated successfully"
                ];
            } else {
                $arr = [
                    'status' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not updated, please try again"
                ];
            }
            $this->response($arr);
        }

        function delete_post() {
            check_for_access(25);
            $id = (int) $this->post("id");
            $response = $this->{$this->model_name}->delete($id);
            if ($response) {
                $arr = [
                    'status' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " deleted successfully"
                ];
            } else {
                $arr = [
                    'status' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not deleted, please try again"
                ];
            }
            $this->response($arr);
        }

    }
    