<?php

class Change_password extends REST_Controller {

    private $model_name = "change_password_model";
    private $title = "Password";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
    }

    function update_post() {
        $this->form_validation->set_rules("current_password", "Current Password", "trim|required|min_length[6]|max_length[32]");
        $this->form_validation->set_rules("new_password", "New password", "trim|required|min_length[6]|max_length[32]");
        $this->form_validation->set_rules("confirm_new_password", "Confirm password", "trim|required|matches[new_password]");
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "current_password" => form_error("current_password"),
                "new_password" => form_error("new_password"),
                "confirm_new_password" => form_error("confirm_new_password")
            ];
            $arr = [
                "status" => "invalid_form",
                "data" => $errors
            ];
            $this->response($arr);
            die;
        }

        if ($this->session->userdata('user_id') != '') {
            $user_id = get_user_id();
        } else {
            $user_id = $this->post('user_id');
        }

        $current_password = $this->post('current_password');

        $valid = $this->{$this->model_name}->check_for_current_password_is_correct($current_password, $user_id);

        if (!$valid) {
            $arr = [
                'status' => "invalid_form",
                'title' => "",
                "message" => "Please enter valid current password"
            ];
            $this->response($arr);
            die;
        }

        $password = $this->post('new_password');
        $data = [
            "password" => $password
        ];
        $response = $this->{$this->model_name}->update($data, $user_id);
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "Success",
                "message" => $this->title . " updated successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "Success",
                "message" => $this->title . " not updated, please try again"
            ];
        }
        $this->response($arr);
    }

}
