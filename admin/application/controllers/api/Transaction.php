<?php

class Transaction extends REST_Controller {
    private $model_name = "players_model";
    private $title = "Dashboard";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
    }

    // This API is being used in new backoffice
    function all_get() {
        $all_transactions = $this->players_model->get_all_transactions();
        $this->response($all_transactions);
    }

    function index_get() {
        $start_date = $this->get('from');
        $end_date = $this->get('to');
        $all_transactions = $this->players_model->get_transactions_by_date_range($start_date, $end_date);
        $this->response($all_transactions);
    }

    function deposit_get() {
        $all_deposits = $this->players_model->get_all_deposits();
        $this->response($all_deposits);
    }

    function withdrawal_get() {
        $all_withdrawals = $this->players_model->get_all_withdrawals();
        $this->response($all_withdrawals);
    }

    function withdraw_request_get(){
        $all_withdrawals_requests = $this->players_model->get_all_withdrawals_requests();
        $this->response($all_withdrawals_requests);
    }

    function withdraw_process_put(){
        $request_id = $this->put('id');
        $wallet_account_id = $this->put('wallet_account_id');
        $request_data = [
            "request_status" => $this->put('request_status'),
            "response_date_time " => date('Y-m-d H:i:s', time()),
        ];

        $this->players_model->update_withdrawals_requests($request_id, $request_data);
        $withdraw_request_data = $this->players_model->get_withdraw_request_by_id($request_id);
        $requested_amount = $withdraw_request_data->request_amount;

        $wallet = $this->wallet_model->get_wallet_account($wallet_account_id);
        $wallet_account_update_data = [
            "real_chips_deposit" => $wallet->real_chips_deposit - $requested_amount,
            "real_chips_withdrawal" => $wallet->real_chips_withdrawal + $requested_amount,
            "on_hold_real_chips_withdrawal" => $wallet->on_hold_real_chips_withdrawal - $requested_amount
        ];
        $this->players_model->update_wallet_account($wallet->id, $wallet_account_update_data);

        $history_data = array(
            'transaction_type' => 'Debit',
            'amount' => $requested_amount,
            'wallet_account_id' => $wallet->id,
            'remark' => 'Withdraw Processed',
            'type' => 'Withdraw',
            'closing_balance' => $wallet->real_chips_deposit - $requested_amount,
            'created_date_time' => date('Y-m-d H:i:s')
        );
        $this->players_model->add_wallet_transaction_history($history_data);

        $arr = [
            "success"=> "true"
        ];
        $this->response($arr);
    }

}
