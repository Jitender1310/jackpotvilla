<?php

class Petty_cash extends REST_Controller {

    private $model_name = "petty_cash_model";
    private $title = "Petty Cash";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
        $this->load->model('centers_model');
        $this->load->model('user_model');
        $this->load->model('notifications_model');
    }

    function index_post() {
        check_for_access(48);
        $filters = array();
        $filters["from_date"] = $this->post('from_date') ? strtotime($this->post('from_date')) : '';
        $filters["to_date"] = $this->post('to_date') ? strtotime($this->post('to_date') . ' 11:59 PM') : '';
        $filters["centers_id"] = $this->post('centers_id') ? $this->post('centers_id') : '';

        $limit = $this->input->get_post("limit") ? $this->input->get_post("limit") : 10;
        $_GET['start'] = $this->input->get_post("page");
        $start = $this->input->get_post("page") > 1 ? ($this->input->get_post("page") - 1) * $limit : 0;

        $total_results = count($this->{$this->model_name}->get($filters));

        $filters["limit"] = $limit;
        $filters["start"] = $start;
        $pagination = my_pagination("petty_cash", $limit, $total_results, true);
        $pagination['pagination'] = str_replace("&amp;", "&", $pagination['pagination']);
        $cur_page = ($pagination['pagination_helper']->cur_page != 0) ? ($pagination['pagination_helper']->cur_page - 1) : $pagination['pagination_helper']->cur_page;
        $pagination['initial_id'] = (($cur_page) * $pagination['pagination_helper']->per_page) + 1;
        $data = $this->{$this->model_name}->get($filters);

        foreach ($data as $item) {

            $item->center_name = $this->centers_model->get_center_name($item->centers_id);
            $item->user_name1 = $this->user_model->get_user_details($item->added_by_user_id)->fullname;
            $item->user_name2 = $this->user_model->get_user_details($item->updated_by_user_id)->fullname;
            $item->created_at = date('d-M-Y H:i:s', $item->created_at);
        }
        $arr = [
            'err_code' => "valid",
            "message" => $this->title . " list",
            "pagination" => $pagination,
            "data" => $data
        ];
        $this->response($arr);
    }

    function add_post() {
        check_for_access(48);
        $this->form_validation->set_rules("amount", "Amount", "required");
        $this->form_validation->set_rules("centers_id", "Center", "required");
        $this->form_validation->set_rules("remarks", "Remarks", "required");
        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => validation_errors()
            ];
            $this->response($arr);
            die;
        }
        $data = [
            "centers_id" => $this->post('centers_id'),
            "amount" => $this->post('amount'),
            "type" => 'Deposit',
            "remarks" => $this->post('remarks'),
        ];

        if ($this->post("id")) {
            $data['updated_by_user_id'] = get_user_id();
            return $this->update($data, $this->post("id"));
        } else {
            $data['added_by_user_id'] = get_user_id();
        }

        if (is_array($_POST['attachment1'])) {
            $extension = pathinfo($_POST['attachment1']['filename'], PATHINFO_EXTENSION);
            $file_name = time() . '_petty_cash_attachment1.' . $extension;
            $target_path = FILE_UPLOAD_FOLDER . "petty_cash_receipts/" . $file_name;
            if (file_put_contents($target_path, base64_decode($_POST['attachment1']['base64']))) {
                $data["attachment1"] = $file_name;
            }
        }
        $response = $this->{$this->model_name}->add($data);
        $user_id = get_user_id();
        $center_name = $this->centers_model->get_center_name($this->post('centers_id'));
        $user_name = $this->user_model->get_user_details($user_id)->fullname;
        $amount = $this->post('amount');
        $notification = "Petty cash " . $amount . " Rs/- deposited into " . $center_name . " center by " . $username;
        $ndata = [
            'notification' => $notification,
            'user_id' => get_user_id(),
            'centers_id' => $this->post('centers_id'),
            'finance_status' => "Approved"
        ];
        $this->notifications_model->notification_add($ndata);

        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " added successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not added, please try again"
            ];
        }
        $this->response($arr);
    }

    function update($data) {
        check_for_access(48);
        $response = $this->{$this->model_name}->update($data, $this->post('id'));
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " updated successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not updated, please try again"
            ];
        }
        $this->response($arr);
    }

    function delete_post() {
        check_for_access(48);
        $id = (int) $this->post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        $this->response($arr);
    }

    function deposited_cash_post($filters) {
        $centers_id = $this->post('centers_id');
        $month = array();
        $month['from_date'] = strtotime(date('1-m-Y'));
        $month['to_date'] = strtotime(date('t-m-Y 23:59 PM'));
        $month['centers_id'] = $centers_id;
        $filters = array();
        $filters["from_date"] = $this->post('from_date') ? strtotime($this->post('from_date')) : '';
        $filters["to_date"] = $this->post('to_date') ? strtotime($this->post('to_date') . ' 11:59 PM') : '';
        $filters['centers_id'] = $centers_id;
        $normal_filters['centers_id'] = $centers_id;
        $arr = [
            'err_code' => "valid",
            "message" => "Deposuted Cash",
            "month" => $this->{$this->model_name}->get_deposited_cash($month),
            "total" => $this->{$this->model_name}->get_deposited_cash($normal_filters),
            "filtered" => $this->{$this->model_name}->get_deposited_cash($filters)
        ];
        $this->response($arr);
    }

}
