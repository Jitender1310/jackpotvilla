<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Reservation_sms_status extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('companies_model');
        $this->load->model('agents_model');
        $this->load->model('centers_model');
        $this->load->model('countries_model');
        $this->load->model('how_did_hear_about_us_model');
        $this->load->model('business_sources_model');
        $this->load->model('tax_slabs_model');
        $this->load->model('states_model');
        $this->load->model('cities_model');
        $this->load->model('locations_model');
        $this->load->model('staying_type_categories_model');
        $this->load->model("reservation_notifications_model");
    }

    function index() {
        $booking_ref_number = $this->input->get_post("booking_ref_number");
        $booking_status = $this->input->get_post("booking_status");
        switch ($booking_status) {
            case "Pending" :
                $this->reservation_notifications_model->send_booking_created_cod_message($booking_ref_number);
                break;
            case "Blocked" :
                $this->reservation_notifications_model->send_booking_created_cod_message($booking_ref_number);
                break;
            case "Confirmed" :
                $this->reservation_notifications_model->send_appointment_accepted_message($booking_ref_number);
                break;
            case "Payment_Failed_Reservation_Cancelled" :
                $this->reservation_notifications_model->send_appointment_canceled_due_to_payment_failed_message($booking_ref_number);
                break;
            case "Payment_Paid_Reservation_Confirmed" :
                $this->reservation_notifications_model->send_booking_created_online_paid_message($booking_ref_number);
                break;
            case "Cancelled" :
                $this->reservation_notifications_model->send_appointment_canceled_message($booking_ref_number);
                break;
            case "Rejected" :
                $this->reservation_notifications_model->send_appointment_rejected_message($booking_ref_number);
                break;
            case "Completed" :
                $this->reservation_notifications_model->send_appointment_completed_message($booking_ref_number);
                break;

            case "Checkin":
                $this->reservation_notifications_model->send_check_in_message($booking_ref_number);
                $this->reservation_notifications_model->send_check_in_mail($booking_ref_number);
                break;
            case "Checkin":
                $this->reservation_notifications_model->send_check_in_message($booking_ref_number);
                $this->reservation_notifications_model->send_check_in_mail($booking_ref_number);
                break;
        }
    }

}
