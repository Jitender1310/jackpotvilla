<?php

    class Lost_and_found extends REST_Controller {

        private $model_name = "lost_and_found_model";
        private $title = "Found Item";

        function __construct() {
            parent::__construct();
            $this->load->model($this->model_name);
            $this->load->model('centers_model');
            $this->load->model('reservations_model');
        }

        function index_post() {
            $filters['from_date'] = $this->post('from_date') ? strtotime($this->post('from_date')) : '';
            $filters['to_date'] = $this->post('to_date') ? strtotime($this->post('to_date') . ' 23:59:59') : '';
            $filters["centers_id"] = $this->post('centers_id') ? $this->post('centers_id') : '';
            $filters["booking_ref_number"] = $this->post('booking_ref_number') ? $this->post('booking_ref_number') : '';
            $filters['item_status'] = $this->post('item_status');
            $filters['search_type'] = $this->post('search_type');
            $data = $this->{$this->model_name}->get($filters);
            foreach ($data as $item) {
                $item->center_name = $this->centers_model->get_center_name($item->centers_id);
                $item->created_at = date('d-M-Y h:i A', $item->created_at);
                $item->founddate = date('d-M-Y h:i A', $item->found_str_date);
                if ($item->returned_str_date != '') {
                    $item->returneddate = date('d-M-Y h:i A', $item->returned_str_date);
                }
                $item->found_to = $this->user_model->get_user_details($item->found_to_users_id)->fullname;
                $item->returned_by = $this->user_model->get_user_details($item->returned_by_users_id)->fullname;
            }
            $arr = [
                'err_code' => "valid",
                "message" => $this->title . " list",
                "data" => $data
            ];
            $this->response($arr);
        }

        function add_post() {

            $this->form_validation->set_rules("item_name", "Item Name", "required");
            $this->form_validation->set_rules("centers_id", "Center", "required");
            $this->form_validation->set_rules("found_date", "Found Date", "required");
            $this->form_validation->set_rules("remarks", "Remarks", "required");
            if ($this->post("id") == '') {
                $this->form_validation->set_rules("item_image", "Photo Of Found Item", "required");
            }
            if ($this->form_validation->run() == FALSE) {
                $arr = [
                    'err_code' => "invalid_form",
                    'title' => "",
                    "message" => validation_errors()
                ];
                $this->response($arr);
                die;
            }
            $data = [
                "booking_ref_number" => $this->post('booking_ref_number') ? $this->post('booking_ref_number') : "",
                "item_name" => $this->post('item_name'),
                "centers_id" => $this->post('centers_id'),
                "found_date" => $this->post('found_date'),
                "found_str_date" => strtotime($this->post('found_date')),
                "found_to_users_id" => get_user_id(),
                "remarks" => $this->post('remarks') ? $this->post('remarks') : "",
                "item_status" => "Found",
            ];
            if ($this->post('booking_ref_number')) {
                $booking_id_check = $this->reservations_model->get_reservation_presence($this->post('booking_ref_number'), $this->post('centers_id'));
                if ($booking_id_check == FALSE) {
                    $arr = [
                        'err_code' => "invalid_form",
                        'title' => "",
                        "message" => "Booking reference id not exists in this center"
                    ];
                    $this->response($arr);
                    die;
                }
            }
            if (is_array($_POST['item_image'])) {
                $extension = pathinfo($_POST['item_image']['filename'], PATHINFO_EXTENSION);
                $file_name = "found_item" . "_" . time() . '.' . $extension;
                $target_path = FILE_UPLOAD_FOLDER . "lost_and_found_items/" . $file_name;

                if (file_put_contents($target_path, base64_decode($_POST['item_image']['base64']))) {
                    $data["item_image"] = $file_name;
                }
            } else {
                $extension = pathinfo($_POST['item_image'], PATHINFO_EXTENSION);
                $file_name = "found_item" . "_" . time() . '.' . 'png';
                $target_path = FILE_UPLOAD_FOLDER . "lost_and_found_items/" . $file_name;

                $_POST['item_image'] = str_replace("data:image/png;base64,", "", $_POST['item_image']);

                if (file_put_contents($target_path, base64_decode($_POST['item_image']))) {
                    $data["item_image"] = $file_name;
                }
            }

            if ($this->post("id") != '') {
                return $this->update($data, $this->post("id"));
            }
            $response = $this->{$this->model_name}->add($data);
            if ($response) {
                $arr = [
                    'err_code' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " added successfully"
                ];
            } else {
                $arr = [
                    'err_code' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not added, please try again"
                ];
            }
            $this->response($arr);
        }

        function return_post() {
            $this->form_validation->set_rules("id_type", "ID Type", "required");
            $this->form_validation->set_rules("collected_person_proof", "Collected Person Image", "required");
            $this->form_validation->set_rules("collected_person_name", "Collected Person Name", "required");
            $this->form_validation->set_rules("collected_person_mobile", "Collected Person Mobile", "required");
            $this->form_validation->set_rules("returned_date", "Retruned Date", "required");
            if ($this->form_validation->run() == FALSE) {
                $arr = [
                    'err_code' => "invalid_form",
                    'title' => "",
                    "message" => validation_errors()
                ];
                $this->response($arr);
                die;
            }
            $data = [
                "id_type" => $this->post('id_type'),
                "collected_person_name" => $this->post('collected_person_name'),
                "collected_person_mobile" => $this->post('collected_person_mobile'),
                "returned_date" => $this->post('returned_date'),
                "returned_str_date" => strtotime($this->post('returned_date')),
                "returned_by_users_id" => get_user_id(),
                "return_remarks" => $this->post('return_remarks'),
                "item_status" => "Returned",
            ];
            if (is_array($_POST['collected_person_proof'])) {
                $extension = pathinfo($_POST['collected_person_proof']['filename'], PATHINFO_EXTENSION);
                $file_name = "collected_person_proof" . "_" . time() . '.' . $extension;
                $target_path = FILE_UPLOAD_FOLDER . "lost_and_found_items/" . $file_name;

                if (file_put_contents($target_path, base64_decode($_POST['collected_person_proof']['base64']))) {
                    $data["collected_person_proof"] = $file_name;
                }
            } else {
                $extension = pathinfo($_POST['collected_person_proof'], PATHINFO_EXTENSION);
                $file_name = "collected_person_proof" . "_" . time() . '.' . 'png';
                $target_path = FILE_UPLOAD_FOLDER . "lost_and_found_items/" . $file_name;

                $_POST['collected_person_proof'] = str_replace("data:image/png;base64,", "", $_POST['collected_person_proof']);

                if (file_put_contents($target_path, base64_decode($_POST['collected_person_proof']))) {
                    $data["collected_person_proof"] = $file_name;
                }
            }
            if ($_FILES["id_proof"]["size"] > 10380531) {
                $arr = [
                    'err_code' => "invalid_form",
                    'title' => "",
                    "message" => "Image size exceeded limit.Size should be below 10mb"
                ];
                $this->response($arr);
                die;
            }

            if (@$_FILES["id_proof"]["name"]) {
                $config['upload_path'] = FILE_UPLOAD_FOLDER . "lost_and_found_items";
                $config['allowed_types'] = 'jpg|jpeg|png|JPG|JPEG|PNG';
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('id_proof')) {
                    $this->data['error'] = array('error' => $this->upload->display_errors());
                    $arr = array(
                        'err_code' => "invalid",
                        "message" => strip_tags($this->data['error']["error"]),
                        "title" => "Invalid",
                        "error" => strip_tags($this->data['error'])
                    );
                    $this->response($arr);
                    die;
                } else {
                    $uploaded_data = array('upload_data' => $this->upload->data());
                    $file_name = $uploaded_data["upload_data"]["file_name"];
                    $data["id_proof"] = $file_name;
                }
            }
            return $this->update($data, $this->post("id"));
        }

        function update($data) {
            $response = $this->{$this->model_name}->update($data, $this->post('id'));
            if ($response) {
                $arr = [
                    'err_code' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " updated successfully"
                ];
            } else {
                $arr = [
                    'err_code' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not updated, please try again"
                ];
            }
            $this->response($arr);
        }

        function delete_post() {
            $id = (int) $this->post("id");
            $response = $this->{$this->model_name}->delete($id);
            if ($response) {
                $arr = [
                    'err_code' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " deleted successfully"
                ];
            } else {
                $arr = [
                    'err_code' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not deleted, please try again"
                ];
            }
            $this->response($arr);
        }

        function counts_get() {
            $data = [
                "foundItems" => $this->{$this->model_name}->get_count_by_status('Found'),
                "returnedItems" => $this->{$this->model_name}->get_count_by_status('Returned'),
            ];


            if ($data) {
                $arr = [
                    'err_code' => "valid",
                    "data" => $data
                ];
            } else {
                $arr = [
                    'err_code' => "invalid",
                    "message" => "no data found"
                ];
            }
            $this->response($arr);
        }

    }
    