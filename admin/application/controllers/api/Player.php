<?php

class Player extends REST_Controller {
    private $model_name = "players_model";
    private $title = "Dashboard";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
    }

    // This API is being used in new backoffice
    function all_get() {
        $all_players = $this->players_model->get_all_players();
        $total_deposits = $this->players_model->get_total_deposits();
        $final_output = array();
        if($all_players)
        {
            foreach($all_players as $player)
            {
                $player_total_deposit = 0;
                foreach($total_deposits as $deposit)
                {
                    if($deposit['wallet_account_id'] == $player->wallet_account_id)
                    {
                        $player_total_deposit = $deposit['total_deposits'];
                    }
                }
                $tempArray['id'] = $player->id;
                $tempArray['AccountID'] = $player->username;
                $tempArray['Fullname'] = $player->firstname . ' ' . $player->lastname;
                $tempArray['DateOfBirth'] = $player->date_of_birth;
                $tempArray['Gender'] = $player->gender;
                $tempArray['Emailaddress'] = $player->email;
                $tempArray['Telephonenumber'] = $player->mobile;
                $tempArray['Currency'] = $player->currency;
                $tempArray['Country'] = $player->country_name;
                $tempArray['CreationDate'] = $player->creation_date;
                $tempArray['LastLogin'] = $player->last_login;
                $tempArray['TotalDeposits'] = round($player_total_deposit, 2);
                $tempArray['LoyaltyPoints'] = $player->loyalty_points;
                $tempArray['LinkedAccount'] = array();
                $final_output[] = $tempArray;
            }
        }

        $arr = $final_output;
        $this->response($arr);
    }

    function index_get(){
        $player_id = $this->get('id');
        $player_detail = $this->players_model->get_player_detail_by_id($player_id);
        $this->response($player_detail);
    }

    function transactions_get(){
        $player_id = $this->get('id');
        $all_transactions = $this->players_model->get_transactions_by_player($player_id);
        $this->response($all_transactions);
    }

    function deposits_get(){
        $player_id = $this->get('id');
        $all_deposits = $this->players_model->get_deposits_by_player($player_id);
        $this->response($all_deposits);
    }

    function withdrawals_get(){
        $player_id = $this->get('id');
        $all_withdrawals = $this->players_model->get_withdrawals_by_player($player_id);
        $this->response($all_withdrawals);
    }

    function loyalty_post(){
        $this->load->model("loyalty_model");

        $wallet_account_id = $this->post('wallet_account_id');
        $loyalty_points = $this->post('loyalty_points');

        $wallet = $this->wallet_model->get_wallet_account($wallet_account_id);
        $actual_loyalty_points = $wallet->loyalty_points;
        $slab_id = $wallet->loyalty_slab_id;

        if($loyalty_points > $actual_loyalty_points){
            $arr = [
                "success" => "false",
                "message" => "Loyalty Points Insufficient"
            ];
        }

        $all_slabs = $this->loyalty_model->get_all_slabs();
        $min_points = 0;
        $max_points = 0;

        foreach($slabs as $slab)
        {
            if($slab['id'] == $slab_id)
            {
                $min_points = $slab['min_points'];
                $max_points = $slab['max_points'];
            }
        }

        $cash = $loyalty_points / 10;
        $new_loyalty_points = $actual_loyalty_points - $loyalty_points;

        if($new_loyalty_points < $min_points)
        {
            foreach($slabs as $slab)
            {
                if($slab['max_points'] >= $new_loyalty_points && $slab['min_points'] < $new_loyalty_points)
                {
                    $slab_id = $slab['id'];
                }
            }
        }

        $wallet_account_update_data = [
            "real_chips_deposit" => $wallet->real_chips_deposit + $cash,
            "loyalty_points" => $new_loyalty_points,
            "loyalty_slab_id" => $slab_id
        ];

        $this->players_model->update_wallet_account($wallet->id, $wallet_account_update_data);

        $history_data = array(
            'transaction_type' => 'Credit Loyalty',
            'amount' => $cash,
            'wallet_account_id' => $wallet->id,
            'remark' => 'Loyalty Points Converted Into Cash',
            'type' => 'Loyalty Conversion',
            'closing_balance' => $wallet->real_chips_deposit + $cash,
            'created_date_time' => date('Y-m-d H:i:s')
        );
        $this->players_model->add_wallet_transaction_history($history_data);

        $arr = [
            "success" => "true",
            "loyalty_points" => $new_loyalty_points
        ];
        $this->response($arr);
    }

}
