<?php

class Testimonials extends REST_Controller {

    private $model_name = "testimonials_model";
    private $title = "Testimonials";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
        check_for_access(25);
    }

    function index_get() {
        $bot_players = 1;
        $data = $this->{$this->model_name}->get($bot_players);
        $arr = [
            'status' => "valid",
            "message" => $this->title . " list",
            "data" => $data
        ];
        $this->response($arr);
    }

    function add_post() {
        if ($this->input->get_post("id")) {
            check_for_access(25, "update");
        } else {
            check_for_access(25, "add");
        }
        $this->form_validation->set_rules("name_and_address", "Name", "required", array(
            'required' => 'Name and address cannot be empty'
        ));

        $this->form_validation->set_rules("prize_title", "Prize Title", "required", array(
            'required' => 'Prize title cannot be empty'
        ));
        
        $this->form_validation->set_rules("message", "Message", "required", array(
            'required' => 'Message cannot be empty'
        ));
        
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "name_and_address" => form_error("name_and_address"),
                "prize_title" => form_error("prize_title"),
                "message" => form_error("message")
            ];
            $arr = [
                "status" => "invalid_form",
                "data" => $errors
            ];
            $this->response($arr);
        }
        
        $data = [
            "name_and_address" => $this->post('name_and_address'),
            "prize_title" => $this->post('prize_title'),
            "message" => $this->post('message')
        ];
        
        
        if (@$_FILES["image"]["name"]) {
            $config['upload_path'] = FILE_UPLOAD_FOLDER."testimonials/";
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 1000;
            $config['overwrite'] = FALSE;
            $config['encrypt_name'] = TRUE;
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image')) {
                $this->data['error'] = array('error' => $this->upload->display_errors());
//                print_r($this->data['error']);
//                die;
                
                $errors = [
                    "title" => form_error("title"),
                    "gender" => form_error("gender"),
                    "image" => strip_tags($this->data['error']["error"]),
                ];
                $arr = [
                    "status" => "invalid_form",
                    "data" => $errors
                ];
                $this->response($arr);
                die;
            } else {
                $uploaded_data = array('upload_data' => $this->upload->data());
                $file_name = $uploaded_data["upload_data"]["file_name"];
                $data["image"] = $file_name;
            }
        }
		
	
        if ($this->input->post("id")) {
            return $this->update($data, $this->post("id"));
        }

        $response = $this->{$this->model_name}->add($data);
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " added successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not added, please try again"
            ];
        }
        $this->response($arr);
    }

    function update($data) {
        check_for_access(25);
        $response = $this->{$this->model_name}->update($data, $this->post('id'));
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " updated successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not updated, please try again"
            ];
        }
        $this->response($arr);
    }

    function delete_post() {
        check_for_access(25);
        $id = (int) $this->post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        $this->response($arr);
    }

    
}
