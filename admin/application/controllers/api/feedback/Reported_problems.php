<?php

class Reported_problems extends REST_Controller {

    private $model_name = "reported_problems_model";
    private $title = "Problem ";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
    }

    function index_get() {
        $data = $this->{$this->model_name}->get();
        $arr = [
            'status' => "valid",
            'title' => "",
            "message" => $this->title . " list",
            "data" => $data
        ];
        $this->response($arr);
    }
    
    function mobile_check($str)
    {
        $this->load->model('players_model');
        if( $this->players_model->get_player_by_mobile($str) )
        {
            return true;
        }
        else
        {
            $this->form_validation->set_message('player_mobile_number', 'The {field} is not exists');
            return false;
        }
    }
    
    function add_post() {
        if ($this->input->get_post("id")) {
            check_for_access(10, "update");
        } else {
            check_for_access(10, "add");
        }
        
        $this->form_validation->set_rules("player_mobile_number", "Player Mobile Number", "required|callback_mobile_check", array(
            'required' => 'Please Enter valid player mobile number'
        ));
        
        $this->form_validation->set_rules("name", "Contact Name", "required", array(
            'required' => 'Please enter contact name'
        ));
        
        $this->form_validation->set_rules('email','Player Email Address', 'required|valid_email', array(
            'required'=>'Please Enter Player Email Address',
            'valid_emali'=>'Player Email Address must be valid.'
        ));
        
        $this->form_validation->set_rules('type_of_issue', 'Type of Issue', 'required', array(
            'required'=>'Please Input Type of Issue'
        ));
        
        $this->form_validation->set_rules('message', 'Report Message', 'required', array(
            'required'=>'Please Enter Report Message'
        ));

        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "player_mobile_number" => form_error("player_mobile_number"),
                "name" => form_error("name"),
                "email" => form_error("email"),
                "type_of_issue" => form_error("type_of_issue"),
                "message" => form_error("message")
            ];
            $arr = [
                "status" => "invalid_form",
                "data" => $errors
            ];
            $this->response($arr);
            die;
        } else {

            $data = [
                "players_id" => $this->players_model->get_player_by_mobile(
                    $this->post("player_mobile_number")
                )['id'],
                "name" => $this->post("name"),
                "email" => $this->post("email"),
                "type_of_issue" => $this->post("type_of_issue"),
                "message" => $this->post("message")
            ];

            if ($this->input->get_post("id")) {
                return $this->update($data, $this->post("id"));
            }
            $response = $this->{$this->model_name}->add($data);
            if ($response) {
                $arr = [
                    'status' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " added successfully"
                ];
            } else {
                $arr = [
                    'status' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not added, please try again"
                ];
            }
            $this->response($arr);
        }
    }

    function update($data) {
        check_for_access(10);
        $response = $this->{$this->model_name}->update($data, $this->post('id'));
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " updated successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . "  not updated, please try again"
            ];
        }
        $this->response($arr);
    }

    function delete_post() {
        check_for_access(10);
        $id = (int) $this->post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        $this->response($arr);
    }
    
    function update_status_post(){
        check_for_access(10);
        
        $this->form_validation->set_rules('id','Reported Problem ID', 'required|callback_valid_problem', array(
            'required' => 'Problem ID is required.',
            'valid_problem' => 'Problem ID is not valid.'
        ));
        
        $this->form_validation->set_rules('status', 'Problem Status', 'required|callback_valid_status', array(
            'required' => 'Problem Status is required',
            'valid_status'=>'Status is not valid.'
        ));
        
        $this->form_validation->set_rules('message', 'Log Message', 'required', array(
            'required' => 'Problem Message is required'
        ));

        if($this->form_validation->run()){

            $data = array(
                'report_problems_id' => $this->input->post('id'),
                'status' => $this->input->post('status'),
                'message' => $this->input->post('message'),
                'users_id' => $this->session->userdata("user_id")
            );

            if( $this->{$this->model_name}->update_status($data) ){
                 $arr = [
                    'status' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " status updated successfully"
                ];
            } else {
                $arr = [
                    'status' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " status could not be updated, please try again"
                ];
            }
        } else {
            echo validation_errors(); die;
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => "Invalid Request."
            ];
        }
        
        $this->response($arr);
    }
    
    function logs_get(){
        check_for_access(10);
        
        $id = $this->input->get('id');
        
        if(is_numeric($id) && $id > 0){
            $data = $this->{$this->model_name}->get_logs($id);
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => $this->title . "logs list",
                "data" => $data
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => "Report ID is not valid, please try again"
            ];
        }
        $this->response($arr);
    }
    
    function valid_problem($id){
        if( $this->{$this->model_name}->get_by_id($id) )
        {
            return true;
        }
        else
        {
            $this->form_validation->set_message('id', 'The {field} is not a valid.');
            return false;
        }
    }
    
    function valid_status($status){
        if(in_array($status,["New","In Progress","Complete","Invalid"])){
            return true;
        } else {
            $this->form_validation->set_message('status','Status is not valid');
            return false;
        }
    }
}
