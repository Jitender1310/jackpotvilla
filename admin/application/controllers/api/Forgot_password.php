<?php

class Forgot_password extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function index_post() {
        $this->form_validation->set_rules("username", "Username", "required");
        if ($this->form_validation->run() == FALSE) {
            $arr = [
                "err_code" => "invalid",
                "message" => validation_errors()
            ];
            $this->response($arr);
            die;
        }

        $user_id = $this->login_model->is_username_exists($this->post("username"));
        if ($user_id == false || $user_id == "") {
            $arr = ['err_code' => "invalid",
                'title' => "",
                "error_type" => "not_exists",
                "message" => "User details not found"
            ];
            $this->response($arr);
            die;
        }
        if ($this->login_model->check_user_status($user_id) == false) {
            $arr = ['err_code' => "invalid",
                'title' => "",
                "error_type" => "account_inactive",
                "message" => "Your account is inactive"
            ];
            $this->response($arr);
            die;
        }
        $this->reset_password($user_id);
    }

    function reset_password($user_id) {
        $result = $this->login_model->reset_password($user_id);
        if ($result) {
            $data = array();
            $data['site_settings'] = (object) array();
            $data['site_settings']->email = 'support@freshup.space';
            $data['result'] = $result;
            $message = $this->load->view('includes/email_templates/password_reset_mail', $data, TRUE);
            if (isset($result->user_meta->email)) {
                if ($this->send_email($result->user_meta->email, 'Forgot Password', $message)) {
                    $arr = [
                        'err_code' => "valid",
                        'title' => "",
                        "message" => "Password Updated Successfully!",
                        "data" => 'Please check your email for details.'
                    ];
                } else {
                    $arr = [
                        'err_code' => "valid",
                        'title' => "",
                        "message" => "Password Updated Successfully!",
                        "data" => 'Error occured while sending email. Please try again later.'
                    ];
                }
            } else {
                $arr = [
                    'err_code' => "invalid",
                    'title' => "",
                    "message" => "Email not found for your account, Please contact admin."
                ];
            }
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => "Error Occured! Please try again."
            ];
        }
        $this->response($arr);
    }

    function send_email($to, $subject, $message) {
        $this->email->from('noreply@freshup.space', SITE_TITLE);
        $this->email->to($to);
        //$this->email->bcc('vijay@thecolourmoon.com,'.$this->data['site_settings']->enquiry_email);
        $this->email->bcc('teja@thecolourmoon.com');
        $this->email->bcc('manikanta@thecolourmoon.com');
        $this->email->subject($subject);
        $this->email->message($message);
        $status = $this->email->send();
        if ($status) {
            return true;
        }
    }

}
