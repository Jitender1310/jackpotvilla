<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Pnr_info extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function pnr_details_get($pnr_number) {
        $url = 'https://www.rr.irctctourism.com/rrservice/rrservice/searchByPNR?pnrNumber=' . $pnr_number;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_POSTFIELDS, NULL);
        $data = curl_exec($curl);
        curl_close($curl);

        if ($data) {
            $data = json_decode($data);
            if ($data->status == "FAILURE") {
                $arr = [
                    'err_code' => "invalid",
                    "title" => $data->status,
                    "message" => $data->message,
                    "data" => $data
                ];
            } else if ($data->message == "PNR service is unavailable. Please try again.") {
                $arr = [
                    'err_code' => "invalid",
                    "title" => "Server Busy",
                    "message" => $data->message,
                    "data" => $data
                ];
            } else if ($data->status == "SUCCESS") {
                $arr = [
                    'err_code' => "valid",
                    "server_id" => 1,
                    "message" => "PNR Details",
                    "data" => $data->data
                ];
            }
        }


        $this->response($arr);
    }

    function pnr_details_server2_get($pnr_number) {
        $pnr = $pnr_number;
        $url = 'https://www.trainman.in/services/predictPnr?pnr=' . $pnr . '&key=012562ae-60a9-4fcd-84d6-f1354ee1ea48';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $data = curl_exec($curl);
        curl_close($curl);

        if ($data) {
            $data = json_decode($data);
            if ($data->message == "PNR FLUSHED") {
                $arr = [
                    'err_code' => "invalid",
                    "title" => "PNR FLUSHED",
                    "message" => "Information not available",
                    "data" => $data
                ];
            } else if ($data->message == "PNR service is unavailable. Please try again.") {
                $arr = [
                    'err_code' => "invalid",
                    "title" => "Server Busy",
                    "message" => $data->message,
                    "data" => $data
                ];
            } else if ($data->message == "Unable to get pnr status. Please try after some time.") {
                $arr = [
                    'err_code' => "invalid",
                    "title" => "Server Busy",
                    "message" => $data->message,
                    "data" => $data
                ];
            } else if ($data->message == "Please enter a valid pnr") {
                $arr = [
                    'err_code' => "invalid",
                    "title" => "PNR Number Required",
                    "message" => $data->message,
                    "data" => $data
                ];
            } else {

                $travel_date = date("Y-m-d " . $data->pnr_data->depart_time, strtotime($data->pnr_data->travel_date));

                $data->pnr_data->travel_date = $travel_date;


//                print_r($data->pnr_data->travel_date);
//                die;
                $arr = [
                    'err_code' => "valid",
                    "server_id" => 2,
                    "message" => "PNR Details",
                    "data" => $data
                ];
            }
        }


        $this->response($arr);
    }

}
