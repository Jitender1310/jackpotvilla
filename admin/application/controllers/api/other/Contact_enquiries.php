<?php

    class Contact_enquiries extends REST_Controller {

        private $model_name = "contact_enquiries_model";
        private $title = "Contact Enquiry";

        function __construct() {
            parent::__construct();
            $this->load->model($this->model_name);
        }

        function index_get() {
            $data = $this->{$this->model_name}->get();
            foreach ($data as $item) {
                $item->created_at = date('d-m-Y H:i:s', $item->created_at);
            }
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => plural($this->title) . " list",
                "data" => $data
            ];
            $this->response($arr);
        }

        function delete_post() {
            check_for_access(39);
            $id = (int) $this->post("id");
            $response = $this->{$this->model_name}->delete($id);
            if ($response) {
                $arr = [
                    'err_code' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " successfully"
                ];
            } else {
                $arr = [
                    'err_code' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not deleted, please try again"
                ];
            }
            $this->response($arr);
        }

        function add_post() {

            check_for_access(39);
            $this->form_validation->set_rules("text", "Text", "required");
            if ($this->form_validation->run() == FALSE) {
                $arr = [
                    'err_code' => "invalid_form",
                    'title' => "",
                    "message" => validation_errors()
                ];
                $this->response($arr);
                die;
            }

            $data = [
                "text" => $this->post('text')
            ];


            if ($this->input->get_post("id")) {
                return $this->update($data, $this->post("id"));
            }
            $response = $this->{$this->model_name}->add($data);
            if ($response) {
                $arr = [
                    'err_code' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " added successfully"
                ];
            } else {
                $arr = [
                    'err_code' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not added, please try again"
                ];
            }
            $this->response($arr);
        }

        function update($data) {
            check_for_access(39);
            $response = $this->{$this->model_name}->update($data, $this->post('id'));
            if ($response) {
                $arr = [
                    'err_code' => "valid",
                    'title' => "",
                    "message" => "Updated successfully"
                ];
            } else {
                $arr = [
                    'err_code' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not updated, please try again"
                ];
            }
            $this->response($arr);
        }

    }
    