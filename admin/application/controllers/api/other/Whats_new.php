<?php

class Whats_new extends REST_Controller {

    private $model_name = "whats_new_model";
    private $title = "Whats New";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
    }

    function index_get() {
        $data = $this->{$this->model_name}->get();
        $arr = [
            'err_code' => "valid",
            'title' => "",
            "message" => plural($this->title) . " list",
            "data" => $data
        ];
        $this->response($arr);
    }

    function add_post() {

        check_for_access(39);
        $this->form_validation->set_rules("image[]", "Image", "required");
        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => validation_errors()
            ];
            $this->response($arr);
            die;
        }

        $data = [
            "title" => $this->post('title'),
            "website_link" => $this->post('website_link')
        ];
        if (is_array($_POST['image'])) {
            $extension = pathinfo($_POST['image']['filename'], PATHINFO_EXTENSION);
            $file_name = time() . '_whats_new.' . $extension;
            $target_path = FILE_UPLOAD_FOLDER . "whats_new/" . $file_name;

            if (file_put_contents($target_path, base64_decode($_POST['image']['base64']))) {
                $data["image"] = $file_name;
            }
        }

        if ($this->input->get_post("id")) {
            return $this->update($data, $this->post("id"));
        }
        $response = $this->{$this->model_name}->add($data);
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " added successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not added, please try again"
            ];
        }
        $this->response($arr);
    }

    function update($data) {
        check_for_access(39);
        $response = $this->{$this->model_name}->update($data, $this->post('id'));
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => "Updated successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not updated, please try again"
            ];
        }
        $this->response($arr);
    }

    function delete_post() {
        check_for_access(39);
        $id = (int) $this->post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        $this->response($arr);
    }

}
