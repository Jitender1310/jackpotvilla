<?php

class Club_types extends REST_Controller {

    private $model_name = "club_types_model";
    private $title = "Club Types";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
    }

    function index_get() {
        $data = $this->{$this->model_name}->get();
        $arr = [
            'err_code' => "valid",
            'title' => "",
            "message" => $this->title . " list",
            "data" => $data
        ];
        $this->response($arr);
    }

    function add_post() {
        if ($this->input->get_post("id")) {
            check_for_access(23, "update");
        } else {
            check_for_access(23, "add");
        }
        
        $this->form_validation->set_rules("name", "Name", "required|callback_name_check", array(
            'required' => 'Name cannot be empty'
        ));
        
        $this->form_validation->set_rules("from_points", "From Points", "required", array(
            'required' => 'From Points cannot be empty'
        ));
        
        $this->form_validation->set_rules("to_points", "To Points", "required", array(
            'required' => 'To Points cannot be empty'
        ));
        
        $this->form_validation->set_rules("priority", "Name", "required", array(
            'required' => 'Priority cannot be empty'
        ));

        $this->form_validation->set_rules("validity_in_days", "Gender", "required", array(
            'required' => 'Validity in days cannot be empty'
        ));
        
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "name" => form_error("name"),
                "priority" => form_error("priority"),
                "from_points" => form_error("from_points"),
                "to_points" => form_error("to_points"),
                "validity_in_days" => form_error("validity_in_days")
            ];
            $arr = [
                "status" => "invalid_form",
                "data" => $errors
            ];
            $this->response($arr);
        }

        $data = [
            "name" => $this->post('name'),
            "priority" => $this->post('priority'),
            "from_points" => $this->post('from_points'),
            "to_points" => $this->post('to_points'),
            "points_to_achieve" => $this->post('to_points'),
            "validity_in_days" => $this->post('validity_in_days'),
        ];

        if ($this->input->post("id")) {
            return $this->update($data, $this->post("id"));
        }

        $response = $this->{$this->model_name}->add($data);
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " added successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not added, please try again"
            ];
        }
        $this->response($arr);
    }

    function update($data) {
        check_for_access(23);
        $response = $this->{$this->model_name}->update($data, $this->post('id'));
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " updated successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                "message" => singular($this->title) . " not updated, please try again"
            ];
        }
        $this->response($arr);
    }

    function delete_post() {
        check_for_access(23);
        $id = (int) $this->post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        $this->response($arr);
    }
    
    function name_check($str) {
        $id = (int) $this->post('id');
        if ($this->{$this->model_name}->is_name_exists_for_adding($str, $id)) {
            $this->form_validation->set_message('name_check', 'The {field} is already existed');
            return FALSE;
        } else {
            return TRUE;
        }
    }
}
