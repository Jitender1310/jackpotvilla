<?php

class Cancellation_charges extends REST_Controller {

    private $model_name = "cancellation_charges_model";
    private $title = "Cancellation Policies";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
        $this->load->model("centers_model");
    }

    function index_get() {
        $data = $this->{$this->model_name}->get();
        foreach ($data as $item) {
            $item->center_name = $this->centers_model->get_center_name($item->centers_id);
        }
        $arr = [
            'err_code' => "valid",
            'title' => "",
            "message" => $this->title . " list",
            "data" => $data
        ];
        $this->response($arr);
    }

    function add_post() {
        if ($this->input->get_post("id")) {
            check_for_access(82, "update");
        } else {
            check_for_access(82, "add");
        }
        $this->form_validation->set_rules("centers_id", "Center", "required");
        $this->form_validation->set_rules("duration_value", "Duration value", "required");
        $this->form_validation->set_rules("duration_before", "Duration Before", "required");
        $this->form_validation->set_rules("refund_value", "Refund Value in %", "required");


        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => validation_errors()
            ];
            $this->response($arr);
            die;
        }

        $data = [
            "centers_id" => $this->post('centers_id'),
            "duration_value" => $this->post('duration_value'),
            "duration_before" => $this->post("duration_before"),
            "refund_value" => $this->post("refund_value"),
            "display_text" => $this->post("display_text")
        ];

        if ($this->input->get_post("id")) {
            return $this->update($data, $this->post("id"));
        }

        $response = $this->{$this->model_name}->add($data);
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " added successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not added, please try again"
            ];
        }
        $this->response($arr);
    }

    function update($data) {
        $response = $this->{$this->model_name}->update($data, $this->post('id'));
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " updated successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not updated, please try again"
            ];
        }
        $this->response($arr);
    }

    function delete_post() {
        $id = (int) $this->post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        $this->response($arr);
    }

}
