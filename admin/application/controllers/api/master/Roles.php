<?php

class Roles extends REST_Controller {

    private $model_name = "roles_model";
    private $title = "Roles";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
    }

    function index_get() {
        $data = $this->{$this->model_name}->get();
        $arr = [
            'status' => "valid",
            "message" => $this->title . " list",
            "data" => $data
        ];
        $this->response($arr);
    }

    function add_post() {

        check_for_access(5);
        $this->form_validation->set_rules("role_name", "Role Name", "required|callback_role_name_check", array(
            'required' => 'Role name cannot be empty'
        ));

        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'status' => "invalid_form",
                'title' => "",
                "message" => validation_errors()
            ];
            $this->response($arr);
            die;
        }

        $data = [
            "role_name" => $this->post('role_name')
        ];

        if ($this->input->get_post("id")) {
            return $this->update($data, $this->post("id"));
        }

        $response = $this->{$this->model_name}->add($data);
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " added successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not added, please try again"
            ];
        }
        $this->response($arr);
    }

    function update($data) {
        check_for_access(5);
        $response = $this->{$this->model_name}->update($data, $this->post('id'));
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " updated successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not updated, please try again"
            ];
        }
        $this->response($arr);
    }

    function delete_post() {
        check_for_access(5);
        $id = (int) $this->post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        $this->response($arr);
    }

    function role_name_check($str) {
        $id = (int) $this->post('id');
        if ($this->{$this->model_name}->is_role_name_exists($str, $id)) {
            $this->form_validation->set_message('role_name_check', 'The {field} is already existed');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function save_privileges_post() {
        $json_object = file_get_contents("php://input");
        $posted_data = json_decode($json_object);
        $selected_role_id = (int) $this->input->get_post('roles_id');
        $response = $this->{$this->model_name}->add_or_update_privileges($posted_data, $selected_role_id);
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "success",
                "message" => singular($this->title) . " privileges updated successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "failed",
                "message" => singular($this->title) . " privileges not updated, please try again"
            ];
        }
        $this->response($arr);
    }

}
