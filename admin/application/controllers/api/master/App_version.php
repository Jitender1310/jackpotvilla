<?php

class App_version extends REST_Controller {

    private $model_name = "app_version_model";
    private $title = "App Version";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
    }

    function index_post() {
        $filters = [];
        
        $filters["game_type"] = $this->post("game_type") ? $this->post("game_type"):null ;
        $filters["game_sub_type"] = $this->post("game_sub_type") ? $this->post("game_sub_type"):null;
        
        
        $data = $this->{$this->model_name}->get($filters);
        $arr = [
            'status' => "valid",
            'title' => "",
            "message" => $this->title . " list",
            "data" => $data
        ];
        $this->response($arr);
    }

    function add_post() {
        if ($this->input->get_post("id")) {
            check_for_access(27, "update");
        } else {
            check_for_access(27, "add");
        }
        $this->form_validation->set_rules("version_android", "Version Android", "required", array(
            'required' => 'Please Enter Version Android'
        ));
        $this->form_validation->set_rules("version_ios", "Version iOS", "required", array(
            'required' => 'Please Enter Version iOS'
        ));
        $this->form_validation->set_rules("description", "Description", "required", array(
            'required' => 'Please Enter Description'
        ));
        $this->form_validation->set_rules("is_force", "Is Force", "required", array(
            'required' => 'Please choose Is Force'
        ));

        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "version_android" => form_error("version_android"),
                "version_ios" => form_error("version_ios"),
                "description" => form_error("description"),
                "is_force" => form_error("is_force")
            ];
            
            $arr = [
                "status" => "invalid_form",
                "data" => $errors 
            ];
            $this->response($arr);
            die;
        } else {

            $data = [
                "version_android" => $this->post("version_android"),
                "version_ios" => $this->post("version_ios"),
                "description" => $this->post("description")
            ];
             
            if ($this->input->get_post("id")) {
                return $this->update($data, $this->post("id"));
            }
            $response = $this->{$this->model_name}->add($data);
            if ($response) {
                $arr = [
                    'status' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " added successfully"
                ];
            } else {
                $arr = [
                    'status' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not added, please try again"
                ];
            }
            $this->response($arr);
        }
    }

    function update($data) {
        check_for_access(10);
        //unset($data);
//        $update_data = [
//            "version" => $this->post("version"),
//            "description" => $this->post("description"),
//            "is_force" => $this->post("is_force")
//        ];
        $update_data = $data;
        
        $response = $this->{$this->model_name}->update($update_data, $this->post('id'));
        if($response==="GAME_IN_PLAY"){
             $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => "Game cannot be deactivated at this time, because some player is playing this game"
            ];
        }else if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " updated successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . "  not updated, please try again"
            ];
        }
        $this->response($arr);
    }

    function delete_post() {
        check_for_access(10);
        $id = (int) $this->post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        $this->response($arr);
    }
}
