<?php

class Users extends REST_Controller {

    private $model_name = "user_model";
    private $title = "Users";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
        check_for_access(6);
    }

    function index_get() {
        $data = $this->{$this->model_name}->get();
        $arr = [
            'status' => "valid",
            "message" => $this->title . " list",
            "data" => $data
        ];
        $this->response($arr);
    }

    function add_post() {
        if ($this->input->get_post("id")) {
            check_for_access(6, "update");
        } else {
            check_for_access(6, "add");
        }
        $this->form_validation->set_rules("role_id", "Role", "required");
        $this->form_validation->set_rules("fullname", "Fullname", "required");
        $this->form_validation->set_rules("login_status", "Login Status", "required");
        if (!$this->input->get_post("id")) {
            if ($this->input->post('login_status') == 1) {
                $this->form_validation->set_rules("username", "Username", "required|callback_username_check");
                $this->form_validation->set_rules("password", "Password", "required");
            }
        }
        if ($this->input->post('login_status') == 1) {
            $chek_username = $this->username_check($this->post('username'));
            if ($chek_username == FALSE) {
                $arr = [
                    'status' => "invalid_form",
                    'title' => "",
                    "message" => "Username Exists"
                ];
                $this->response($arr);
                die;
            }
        }
        $this->form_validation->set_rules("user_meta[designation]", "Designation", "required");
        $this->form_validation->set_rules("user_meta[mobile]", "Mobile", "required");
        $this->form_validation->set_rules("user_meta[email]", "Email", "required|valid_email");
        $this->form_validation->set_rules("user_meta[payroll_status]", "Payroll Status", "required");
        if ($this->post('user_meta')["payroll_status"] == 1) {
            $this->form_validation->set_rules("user_meta[payroll_status]", "Payroll Status", "required");
            $this->form_validation->set_rules("user_meta[ctc]", "CTC", "required");
        }
        $this->form_validation->set_rules("user_meta[gender]", "Gender", "required");
        $this->form_validation->set_rules("user_meta[dob]", "Date of Birth", "required");
        $this->form_validation->set_rules("user_meta[address]", "Address", "required");
        $this->form_validation->set_rules("user_meta[joining_date]", "Date of Joining", "required");

        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'status' => "invalid_form",
                'title' => "",
                "message" => validation_errors()
            ];
            $this->response($arr);
            die;
        }

        $data = [
            "role_id" => $this->post('role_id'),
            "fullname" => $this->post('fullname'),
            "login_status" => $this->post('login_status'),
        ];

        $data["user_meta"] = [
            "designation" => $this->post('user_meta')["designation"],
            "mobile" => $this->post('user_meta')["mobile"],
            "email" => $this->post('user_meta')["email"],
            "gender" => $this->post('user_meta')["gender"],
            "address" => $this->post('user_meta')["address"],
            "dob" => convert_date_to_db_format($this->post('user_meta')["dob"]),
            "joining_date" => convert_date_to_db_format($this->post('user_meta')["joining_date"]),
            "payroll_status" => $this->post('user_meta')["payroll_status"],
        ];

        if ($this->post('user_meta')["payroll_status"] == 1) {
            $data["user_meta"]["ctc"] = $this->post('user_meta')["ctc"];
            $data["user_meta"]["net_salary"] = $this->post('user_meta')["net_salary"];
        }
        if (isset($this->post('user_meta')["pan_no"])) {
            $data["user_meta"]["pan_no"] = $this->post('user_meta')["pan_no"];
        }
        if (isset($this->post('user_meta')["esi_no"])) {
            $data["user_meta"]["esi_no"] = $this->post('user_meta')["esi_no"];
        }
        if (isset($this->post('user_meta')["pf_no"])) {
            $data["user_meta"]["pf_no"] = $this->post('user_meta')["pf_no"];
        }
        if (isset($this->post('user_meta')["license_no"])) {
            $data["user_meta"]["license_no"] = $this->post('user_meta')["license_no"];
        }
        if (isset($this->post('user_meta')["ctc"])) {
            $data["user_meta"]["ctc"] = $this->post('user_meta')["ctc"];
        }
        if (isset($this->post('user_meta')["bank_name"])) {
            $data["user_meta"]["bank_name"] = $this->post('user_meta')["bank_name"];
        }
        if (isset($this->post('user_meta')["branch_name"])) {
            $data["user_meta"]["branch_name"] = $this->post('user_meta')["branch_name"];
        }
        if (isset($this->post('user_meta')["account_no"])) {
            $data["user_meta"]["account_no"] = $this->post('user_meta')["account_no"];
        }
        if (isset($this->post('user_meta')["ifsc_code"])) {
            $data["user_meta"]["ifsc_code"] = $this->post('user_meta')["ifsc_code"];
        }
        if (isset($this->post('user_meta')["micr_code"])) {
            $data["user_meta"]["micr_code"] = $this->post('user_meta')["micr_code"];
        }
        if (isset($this->post('user_meta')["net_salary"])) {
            $data["user_meta"]["net_salary"] = $this->post('user_meta')["net_salary"];
        }

        if ($this->input->post("id")) {
            if ($this->input->post('login_status') == 1) {
                $data["username"] = $this->input->post('username');
                if ($this->input->post('password')) {
                    $data["salt"] = generateRandomString();
                    $data["password"] = md5($this->input->post('password') . $data["salt"]);
                }
            }
        } else {
            $data["token"] = $this->{$this->model_name}->generate_token();
            if ($this->input->post('login_status') == 1) {
                $data["username"] = $this->input->post('username');
                $data["salt"] = generateRandomString();
                $data["password"] = md5($this->input->post('password') . $data["salt"]);
            }
        }

        if ($this->input->post("id")) {
            return $this->update($data, $this->post("id"));
        }

        $response = $this->{$this->model_name}->add($data);
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " added successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not added, please try again"
            ];
        }
        $this->response($arr);
    }

    function update($data) {
        check_for_access(6);
        $response = $this->{$this->model_name}->update($data, $this->post('id'));
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " updated successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not updated, please try again"
            ];
        }
        $this->response($arr);
    }

    function delete_post() {
        check_for_access(6);
        $id = (int) $this->post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        $this->response($arr);
    }

    function username_check($str) {
        $id = (int) $this->post('id');
        if ($this->{$this->model_name}->is_username_exists_for_adding($str, $id)) {
            $this->form_validation->set_message('username_check', 'The {field} is already existed');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function save_privileges_post() {
        $json_object = file_get_contents("php://input");
        $posted_data = json_decode($json_object);
        $user_id = (int) $this->input->get_post('user_id');
        $response = $this->{$this->model_name}->add_or_update_privileges($posted_data, $user_id);
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "success",
                "message" => singular($this->title) . " privileges updated successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "failed",
                "message" => singular($this->title) . " privileges not updated, please try again"
            ];
        }
        $this->response($arr);
    }

}
