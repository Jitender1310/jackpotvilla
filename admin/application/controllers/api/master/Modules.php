<?php

class Modules extends REST_Controller {

    private $model_name = "modules_model";
    private $title = "Modules";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
        $this->load->model("roles_model");
        $this->load->model("user_model");
        check_for_access(3);
    }

    function index_get() {
        $data = $this->{$this->model_name}->get();
        $arr = [
            'status' => "valid",
            'title' => "",
            "message" => $this->title . " list",
            "data" => $data
        ];
        $this->response($arr);
    }

    function add_post() {


        $this->form_validation->set_rules("module_name", "Module Name", "required|callback_module_name_check", array(
            'required' => 'Module name cannot be empty'
        ));

        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "state_name" => form_error("state_name")
            ];
            $arr = [
                "status" => "invalid_form",
                "data" => $errors
            ];
            $this->response($arr);
            die;
        }

        $data = [
            "module_name" => $this->post('module_name'),
            "parent_module_id" => $this->post('parent_module_id'),
            "display_order" => $this->post('display_order')
        ];

        if ($this->input->get_post("id")) {
            return $this->update($data, $this->post("id"));
        }

        $response = $this->{$this->model_name}->add($data);
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " added successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not added, please try again"
            ];
        }
        $this->response($arr);
    }

    function update($data) {
        $response = $this->{$this->model_name}->update($data, $this->post('id'));
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " updated successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not updated, please try again"
            ];
        }
        $this->response($arr);
    }

    function delete_post() {
        $id = (int) $this->post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        $this->response($arr);
    }

    function module_name_check($str) {
        $id = (int) $this->post('id');
        if ($this->{$this->model_name}->is_module_name_exists($str, $id)) {
            $this->form_validation->set_message('module_name_check', 'The {field} is already existed');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function code_check($str) {
        $id = (int) $this->post('id');
        if ($this->{$this->model_name}->is_module_code_exists($str, $id)) {
            $this->form_validation->set_message('code_check', 'The {field} is already existed');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function tree_structure_get() {
        $roles_id = (int) $this->get('roles_id');

        $data = $this->{$this->model_name}->get_tree_level_structure_of_modules($roles_id);
        $arr = [
            'status' => "valid",
            'title' => "",
            "message" => $this->title . " list",
            "data" => $data,
            "privilegies" => $this->{$this->model_name}->get_privileges_column_headings()
        ];
        $this->response($arr);
    }

    function tree_structure_for_user_get() {
        $user_id = (int) $this->get('user_id');

        $data = $this->{$this->model_name}->get_tree_level_structure_of_modules_for_user($user_id);
        $arr = [
            'status' => "valid",
            'title' => "",
            "message" => $this->title . " list",
            "data" => $data,
            "privilegies" => $this->{$this->model_name}->get_privileges_column_headings()
        ];
        $this->response($arr);
    }

}
