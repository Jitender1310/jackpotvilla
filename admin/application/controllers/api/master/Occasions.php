<?php

class Occasions extends REST_Controller {

    private $model_name = "occasions_model";
    private $title = "Occasions";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
        $this->load->model("centers_model");
        check_for_access(21);
    }

    function index_get() {
        $data = $this->{$this->model_name}->get();
        $arr = [
            'err_code' => "valid",
            "message" => $this->title . " list",
            "data" => $data
        ];
        $this->response($arr);
    }

    function add_post() {
        if ($this->input->get_post("id")) {
            check_for_access(21, "update");
        } else {
            check_for_access(21, "add");
        }
        $this->form_validation->set_rules("centers_id", "Center", "required");
        $this->form_validation->set_rules("from_date", "From Date", "required");
        $this->form_validation->set_rules("to_date", "To Date", "required");
        $this->form_validation->set_rules("occasion_name", "Occasion Name", "required");

        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => validation_errors()
            ];
            $this->response($arr);
            die;
        }

        $from_date = convert_date_to_db_format($this->post('from_date'));
        $to_date = convert_date_to_db_format($this->post('to_date'));

        $data = [
            "centers_id" => $this->post('centers_id'),
            "from_date" => $from_date,
            "to_date" => $to_date,
            "occasion_name" => $this->post('occasion_name')
        ];

        if ($this->input->get_post("id")) {
            return $this->update($data, $this->post("id"));
        }

        $response = $this->{$this->model_name}->add($data);
        if ($response) {
            $arr = [
                'err_code' => "valid",
                "message" => singular($this->title) . " added successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                "message" => singular($this->title) . " not added, please try again"
            ];
        }
        $this->response($arr);
    }

    function update($data) {
        $response = $this->{$this->model_name}->update($data, $this->post('id'));
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " updated successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not updated, please try again"
            ];
        }
        $this->response($arr);
    }

    function delete_post() {
        $id = (int) $this->post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        $this->response($arr);
    }

    function by_centers_id_get() {
        $centers_id = (int) $this->get('centers_id');
        $data = $this->{$this->model_name}->get_occasions_by_centers_id($centers_id);
        $arr = [
            'err_code' => "valid",
            'title' => "",
            "message" => "Center Occasions list",
            "data" => $data
        ];
        $this->response($arr);
    }

}
