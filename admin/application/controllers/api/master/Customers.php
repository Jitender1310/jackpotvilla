<?php

class Customers extends REST_Controller {

    private $model_name = "customers_model";
    private $title = "Customers";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
        $this->load->model('countries_model');
    }

    function index_post() {
        check_for_access(36);
        $filters = array();
        //Sample formate for filters starts here
        //array_push($filters,array('key'=>'users.id','value'=>1));
        //array_push($filters,array('key'=>'name','value'=>'vijay'));
        //Sample formate for filters ends here
        $filters["search_key"] = $this->post('search_key') ? $this->post('search_key') : '';
        $filters["sort_by"] = $this->post('sort_by') ? $this->post('sort_by') : '';
        $filters["centers_id"] = $this->post('centers_id') ? $this->post('centers_id') : '';
        $filters["booking_status"] = $this->post('booking_status') ? $this->post('booking_status') : '';

        $limit = $this->input->get_post("limit") ? $this->input->get_post("limit") : 10;
        $_GET['start'] = $this->input->get_post("page");
        $start = $this->input->get_post("page") > 1 ? ($this->input->get_post("page") - 1) * $limit : 0;

        $total_results = $this->{$this->model_name}->get_customers_list($filters);

        $filters["limit"] = $limit;
        $filters["start"] = $start;

        $pagination = my_pagination("master/customers", $limit, $total_results, true);
        $pagination['pagination'] = str_replace("&amp;", "&", $pagination['pagination']);
        $cur_page = ($pagination['pagination_helper']->cur_page != 0) ? ($pagination['pagination_helper']->cur_page - 1) : $pagination['pagination_helper']->cur_page;
        $pagination['initial_id'] = (($cur_page) * $pagination['pagination_helper']->per_page) + 1;

        $data = $this->{$this->model_name}->get_customers_list($filters);



        if (isset($data)) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => plural($this->title) . " List",
                "data" => $data,
                "pagination" => $pagination
            ];
        } else {
            $data = [];
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => "No " . plural($this->title) . "Found",
                "data" => $data,
                "pagination" => array("pagination" => []),
            ];
        }
        $this->response($arr);
    }

    function customers_with_mobile_get() {
        if ($this->get('mobile')) {
            $result = $this->{$this->model_name}->get_customer_details_by_mobile($this->get('mobile'));
            if ($result) {
                $data = [
                    "contact_name" => $result->fullname,
                    "contact_email" => isset($result->customer_meta->email) ? $result->customer_meta->email : '',
                    "alternative_mobile" => isset($result->customer_meta->alternative_mobile) ? $result->customer_meta->alternative_mobile : '',
                    "door_no" => isset($result->customer_meta->door_no) ? $result->customer_meta->door_no : '',
                    "street_name" => isset($result->customer_meta->street_name) ? $result->customer_meta->street_name : '',
                    "location" => isset($result->customer_meta->location_name) ? $result->customer_meta->location_name : '',
                    "city" => isset($result->customer_meta->city_name) ? $result->customer_meta->city_name : '',
                    "district" => isset($result->customer_meta->district_name) ? $result->customer_meta->district_name : '',
                    "state" => isset($result->customer_meta->state_name) ? $result->customer_meta->state_name : '',
                    "zipcode" => isset($result->customer_meta->zipcode) ? $result->customer_meta->zipcode : '',
                    "countries_id" => isset($result->customer_meta->countries_id) ? $result->customer_meta->countries_id : ''
                ];
                $arr = [
                    'err_code' => "valid",
                    'title' => "",
                    "message" => "Customer Details",
                    "data" => $data
                ];
            } else {
                $data = [];
                $arr = [
                    'err_code' => "valid",
                    'title' => "",
                    "message" => "No Customer Details Found",
                    "data" => $data
                ];
            }
        } else {
            $data = [];
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => "Please provide mobile",
                "data" => $data
            ];
        }

        $this->response($arr);
    }

}
