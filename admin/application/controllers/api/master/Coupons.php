<?php

class Coupons extends REST_Controller {

    private $model_name = "coupons_model";
    private $title = "Coupons";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
        $this->load->model("centers_model");
    }

    function index_get() {
        $data = $this->{$this->model_name}->get();
        $arr = [
            'err_code' => "valid",
            'title' => "",
            "message" => $this->title . " list",
            "data" => $data
        ];
        $this->response($arr);
    }

    function add_post() {
        if ($this->input->get_post("id")) {
            check_for_access(37, "update");
        } else {
            check_for_access(37, "add");
        }
        $this->form_validation->set_rules("centers_id[]", "Centers", "required");
        $this->form_validation->set_rules("coupon_code", "Coupon Code", "required");
        $this->form_validation->set_rules("start_date", "Start Date", "required");
        $this->form_validation->set_rules("end_date", "End Date", "required");
        $this->form_validation->set_rules("discount_type", "Discount Type", "required");
        $this->form_validation->set_rules("discount_value", "Discount Value", "required");
        $this->form_validation->set_rules("minimum_to_apply_discount", "Minimum Amount to Apply Discount", "required");
        $this->form_validation->set_rules("applicable_on", "Applicable On", "required");
        $this->form_validation->set_rules("coupon_usage_type", "Coupon Usage Type", "required");
        if ($this->post('coupon_usage_type') != 'single') {
            $this->form_validation->set_rules("coupon_usage_attempts", "Coupon Usage Attempts", "required");
        }

        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => validation_errors()
            ];
            $this->response($arr);
            die;
        }

        $start_date = convert_date_to_db_format($this->post('start_date'));
        $end_date = convert_date_to_db_format($this->post('end_date'));

        $data = [
            "centers_id" => $this->post('centers_id') ? implode(',', $this->post('centers_id')) : '',
            "coupon_code" => strtoupper($this->post('coupon_code')),
            "start_date" => $start_date,
            "end_date" => $end_date,
            "discount_type" => $this->post('discount_type'),
            "discount_value" => $this->post('discount_value'),
            "minimum_to_apply_discount" => $this->post('minimum_to_apply_discount'),
            "applicable_on" => $this->post('applicable_on'),
        ];
        $data['coupon_usage_type'] = $this->post('coupon_usage_type');
        if ($data['coupon_usage_type'] == 'single') {
            $data['coupon_usage_attempts'] = NULL;
        } elseif ($data['coupon_usage_type'] == 'multiple') {
            $data['coupon_usage_attempts'] = $this->post('coupon_usage_attempts');
        }
        if ($this->post("id")) {
            return $this->update($data, $this->post("id"));
        }
        $response = $this->{$this->model_name}->add($data);
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " added successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not added, please try again"
            ];
        }
        $this->response($arr);
    }

    function update($data) {
        check_for_access(37);
        $response = $this->{$this->model_name}->update($data, $this->post('id'));
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " updated successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                "message" => singular($this->title) . " not updated, please try again"
            ];
        }
        $this->response($arr);
    }

    function delete_post() {
        check_for_access(37);
        $id = (int) $this->post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        $this->response($arr);
    }

}
