<?php

    class Tournament_categories extends REST_Controller {

        private $model_name = "tournament_categories_model";
        private $title = "Tournament Categories";

        function __construct() {
            parent::__construct();
            $this->load->model($this->model_name);
        }

        function index_get() {
            $data = $this->{$this->model_name}->get();
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => $this->title . " list",
                "data" => $data
            ];
            $this->response($arr);
        }

        function add_post() {
            if ($this->input->get_post("id")) {
                check_for_access(24, "update");
            } else {
                check_for_access(24, "add");
            }

            $this->form_validation->set_rules("name", "Name", "required|callback_name_check", array(
                'required' => 'Name cannot be empty'
            ));


            if ($this->form_validation->run() == FALSE) {
                $errors = [
                    "name" => form_error("name")
                ];
                $arr = [
                    "status" => "invalid_form",
                    "data" => $errors
                ];
                $this->response($arr);
            }

            $data = [
                "name" => $this->post('name')
            ];
            if (@$_FILES["image"]["name"]) {
                $config['upload_path'] = FILE_UPLOAD_FOLDER;
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = 1000;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')) {
                    $this->data['error'] = array('error' => $this->upload->display_errors());
//                print_r($this->data['error']);
//                die;
//                    $errors = [
//                        "title" => strip_tags($this->data['error']["error"]),
//                    ];
                    $arr = [
                        "status" => "invalid_form",
                        "data" => strip_tags($this->data['error']["error"]),
                    ];
                    $this->response($arr);
                    die;
                } else {
                    $uploaded_data = array('upload_data' => $this->upload->data());
                    $file_name = $uploaded_data["upload_data"]["file_name"];
                    $data["image"] = $file_name;
                }
            }
            if ($this->input->post("id")) {
                return $this->update($data, $this->post("id"));
            }

            $response = $this->{$this->model_name}->add($data);
            if ($response) {
                $arr = [
                    'status' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " added successfully"
                ];
            } else {
                $arr = [
                    'status' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not added, please try again"
                ];
            }
            $this->response($arr);
        }

        function update($data) {
            check_for_access(24);
            $response = $this->{$this->model_name}->update($data, $this->post('id'));
            if ($response) {
                $arr = [
                    'status' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " updated successfully"
                ];
            } else {
                $arr = [
                    'status' => "invalid",
                    "message" => singular($this->title) . " not updated, please try again"
                ];
            }
            $this->response($arr);
        }

        function delete_post() {
            check_for_access(24);
            $id = (int) $this->post("id");
            $response = $this->{$this->model_name}->delete($id);
            if ($response) {
                $arr = [
                    'err_code' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " deleted successfully"
                ];
            } else {
                $arr = [
                    'err_code' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not deleted, please try again"
                ];
            }
            $this->response($arr);
        }

        function name_check($str) {
            $id = (int) $this->post('id');
            if ($this->{$this->model_name}->is_name_exists($str, $id)) {
                $this->form_validation->set_message('name_check', 'The {field} is already existed');
                return FALSE;
            } else {
                return TRUE;
            }
        }

    }
    