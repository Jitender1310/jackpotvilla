<?php

class Global_configuration_constants extends REST_Controller {

    private $model_name = "global_configuration_constants_model";
    private $title = "Global Constants";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
    }

    function index_get() {
        $data = $this->{$this->model_name}->get();
        $arr = [
            'status' => "valid",
            'title' => "",
            "message" => $this->title . " list",
            "data" => $data
        ];
        $this->response($arr);
    }

    function update_post() {
        check_for_access(14);
        $data = json_decode($_POST["constants_obj"]);
        $response = $this->{$this->model_name}->update($data);
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " updated successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . "  not updated, please try again"
            ];
        }
        $this->response($arr);
    }
}
