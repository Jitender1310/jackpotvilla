<?php

class Locations extends REST_Controller {

    private $model_name = "locations_model";
    private $title = "Locations";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
        $this->load->model("countries_model");
        $this->load->model("states_model");
        $this->load->model("cities_model");
    }

    function index_get() {
        $data = $this->{$this->model_name}->get();
        $arr = [
            'err_code' => "valid",
            'title' => "",
            "message" => $this->title . " list",
            "data" => $data
        ];
        $this->response($arr);
    }

    function add_post() {
        if ($this->input->get_post("id")) {
            check_for_access(17, "update");
        } else {
            check_for_access(17, "add");
        }
        $this->form_validation->set_rules("countries_id", "Country", "required");
        $this->form_validation->set_rules("states_id", "State", "required");
        $this->form_validation->set_rules("cities_id", "City", "required");
        $this->form_validation->set_rules("location_name", "Location Name", "required|callback_location_name_check");

        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => validation_errors()
            ];
            $this->response($arr);
            die;
        }

        $data = [
            "countries_id" => $this->post('countries_id'),
            "states_id" => $this->post('states_id'),
            "cities_id" => $this->post("cities_id"),
            "location_name" => $this->post('location_name')
        ];

        if ($this->input->get_post("id")) {
            return $this->update($data, $this->post("id"));
        }

        $response = $this->{$this->model_name}->add($data);
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " added successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not added, please try again"
            ];
        }
        $this->response($arr);
    }

    function update($data) {
        check_for_access(17);
        $response = $this->{$this->model_name}->update($data, $this->post('id'));
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " updated successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not updated, please try again"
            ];
        }
        $this->response($arr);
    }

    function delete_post() {
        check_for_access(17);
        $id = (int) $this->post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        $this->response($arr);
    }

    function location_name_check($str) {
        $id = (int) $this->post('id');
        if ($this->{$this->model_name}->is_location_name_exists($str, $id)) {
            $this->form_validation->set_message('location_name_check', 'The {field} is already existed');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function by_cities_id_get() {
        $cities_id = (int) $this->get('cities_id');
        $data = $this->{$this->model_name}->get_locations_by_cities_id($cities_id);
        $arr = [
            'err_code' => "valid",
            'title' => "",
            "message" => "Cities list",
            "data" => $data
        ];
        $this->response($arr);
    }

}
