<?php

class Games extends REST_Controller {

    private $model_name = "games_model";
    private $title = "Games";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
    }

    function index_post() {
        $filters = [];
        
        $filters["game_type"] = $this->post("game_type") ? $this->post("game_type"):null ;
        $filters["game_sub_type"] = $this->post("game_sub_type") ? $this->post("game_sub_type"):null;
        
        
        $data = $this->{$this->model_name}->get($filters);
        $arr = [
            'status' => "valid",
            'title' => "",
            "message" => $this->title . " list",
            "data" => $data
        ];
        $this->response($arr);
    }

    function add_post() {
        if ($this->input->get_post("id")) {
            check_for_access(10, "update");
        } else {
            check_for_access(10, "add");
        }
        $this->form_validation->set_rules("game_type", "Game Type", "required", array(
            'required' => 'Please choose game type'
        ));
        $this->form_validation->set_rules("game_sub_type", "Game Sub Type", "required", array(
            'required' => 'Please choose game sub type'
        ));
        $this->form_validation->set_rules("seats", "Seats", "required", array(
            'required' => 'Please choose seats'
        ));

        if ($this->post("game_sub_type") == "Points") {
            $this->form_validation->set_rules("number_of_cards", "Number of Cards", "required", array(
                'required' => 'Please choose number of cards'
            ));
            $this->form_validation->set_rules("point_value", "Point Value", "required", array(
                'required' => 'Point value cannot be empty'
            ));
        }else{
            $this->form_validation->set_rules("entry_fee", "Entry Fee", "required", array(
                'required' => 'Entry fee cannot be empty'
            ));
        }

        if ($this->post("game_sub_type") == "Pool") {
            $this->form_validation->set_rules("pool_game_type", "Pool Game Type", "required", array(
                'required' => 'Please choose game type'
            ));
        }

        if ($this->post("game_sub_type") == "Deals") {
            $this->form_validation->set_rules("deals", "Deals", "required", array(
                'required' => 'Please choose Deals'
            ));
        }

        $this->form_validation->set_rules("active", "Status", "required", array(
            'required' => 'Please choose status'
        ));

        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "game_type" => form_error("game_type"),
                "game_sub_type" => form_error("game_sub_type"),
                "seats" => form_error("seats"),
                "entry_fee" => form_error("entry_fee"),
                "number_of_cards" => form_error("number_of_cards"),
                "point_value" => form_error("point_value"),
                "pool_game_type" => form_error("pool_game_type"),
                "number_of_deck" => form_error("number_of_deck"),
                "active" => form_error("active")
            ];
            $arr = [
                "status" => "invalid_form",
                "data" => $errors
            ];
            $this->response($arr);
            die;
        } else {

            $data = [
                "game_type" => $this->post("game_type"),
                "game_sub_type" => $this->post("game_sub_type"),
                "seats" => $this->post("seats"),
                "active" => $this->post("active") ? $this->post("active")  :0,
                "number_of_deck" =>  $this->post("number_of_deck") ? $this->post("number_of_deck")  :2
            ];
            
            if($this->post("number_of_cards")){
                $data["game_title"] = $this->post("game_sub_type") . " (" . $this->post("number_of_cards") . ")";
            }else if($this->post("game_sub_type") == "Pool"){
                $data["game_title"] = $this->post("game_sub_type") . " (" . $this->post("pool_game_type") . ")";;
            }else{
                $data["game_title"] = $this->post("game_sub_type");
            }
            
            if($this->post("number_of_cards")){
                $data["number_of_cards"] = $this->post("number_of_cards");
            }
            if($this->post("point_value")){
                $data["point_value"] = $this->post("point_value");
                $data["entry_fee"] = $this->post("point_value") * 80;
                
            }
            if($this->post("pool_game_type")){
                $data["pool_game_type"] = $this->post("pool_game_type");
            }
            
            if($data['game_sub_type']!="Points"){
                $data["entry_fee"] = $this->post("entry_fee");
            }
            
            if($data['game_sub_type']=="Deals" || $data['game_sub_type']=="Pool"){
                $data["pool_deal_prize"] = $this->post("entry_fee") * $this->post("seats");
            }
            
            if($data['game_sub_type']!="Deals"){
                $data['deals'] = "0";
            }else{
                 $data["deals"] = $this->post("deals");
            }
            
            if($this->post("seats") == 2){
                $data['number_of_deck'] = 2;
            } else if($this->post("seats") == 4 || $this->post("seats") == 6){
                $data['number_of_deck'] = 2;
            }

            if ($this->input->get_post("id")) {
                return $this->update($data, $this->post("id"));
            }
            $response = $this->{$this->model_name}->add($data);
            if ($response) {
                $arr = [
                    'status' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " added successfully"
                ];
            } else {
                $arr = [
                    'status' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not added, please try again"
                ];
            }
            $this->response($arr);
        }
    }

    function update($data) {
        check_for_access(10);
        unset($data);
        $update_data = [
            "active" => $this->post("active") ? $this->post("active")  :0
        ];
        $response = $this->{$this->model_name}->update($update_data, $this->post('id'));
        if($response==="GAME_IN_PLAY"){
             $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => "Game cannot be deactivated at this time, because some player is playing this game"
            ];
        }else if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " updated successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . "  not updated, please try again"
            ];
        }
        $this->response($arr);
    }

    function delete_post() {
        check_for_access(10);
        $id = (int) $this->post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        $this->response($arr);
    }
}
