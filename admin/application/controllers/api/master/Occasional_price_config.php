<?php

class Occasional_price_config extends REST_Controller {

    private $model_name = "occasional_price_config_model";
    private $title = "Occasional Price Config";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
        $this->load->model("roles_model");
    }

    function index_get() {
        if (!$this->input->get_post('centers_id')) {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => "Center id is required"
            ];
            $this->response($arr);
            die;
        } else if (!$this->input->get_post('staying_type_categories_id')) {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => "Category id is required"
            ];
            $this->response($arr);
            die;
        }


        $centers_id = (int) $this->input->get_post("centers_id");
        $staying_type_categories_id = (int) $this->input->get_post("staying_type_categories_id");
        $occasions_id = (int) $this->input->get_post("occasions_id");

        $data = $this->{$this->model_name}->get_price_configuration($centers_id, $staying_type_categories_id, $occasions_id);

        $extend_price_data = $this->{$this->model_name}->get_extended_price_configuration($centers_id, $staying_type_categories_id, $occasions_id);
        $arr = [
            'err_code' => "valid",
            'title' => "",
            "message" => $this->title . " loaded successfully ",
            "data" => $data,
            "extend_price_data" => $extend_price_data
        ];
        $this->response($arr);
    }

    function add_post() {
        if ($this->input->get_post("id")) {
            check_for_access(20, "update");
        } else {
            check_for_access(20, "add");
        }
        $this->form_validation->set_rules("centers_id", "Center", "required");
        $this->form_validation->set_rules("occasions_id", "Occasion", "required");
        $this->form_validation->set_rules("price_config[]", "Price ", "required");

        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => validation_errors()
            ];
            $this->response($arr);
            die;
        }

        $data = [
            "centers_id" => $this->post('centers_id'),
            "staying_type_categories_id" => $this->post("staying_type_categories_id"),
            "occasions_id" => $this->post("occasions_id"),
            "price_config" => $this->post('price_config'),
            "extended_price_config" => $this->post("extended_price_config")
        ];

        $response = $this->{$this->model_name}->add_or_update($data);
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " added successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not added, please try again"
            ];
        }
        $this->response($arr);
    }

}
