<?php

class Tax_slabs extends REST_Controller {

    private $model_name = "tax_slabs_model";
    private $title = "Tax Slabs";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
        $this->load->model("centers_model");
    }

    function index_get() {
        $data = $this->{$this->model_name}->get();
        $arr = [
            'err_code' => "valid",
            'title' => "",
            "message" => $this->title . " list",
            "data" => $data
        ];
        $this->response($arr);
    }

    function add_post() {
        if ($this->input->get_post("id")) {
            check_for_access(27, "update");
        } else {
            check_for_access(27, "add");
        }
        $this->form_validation->set_rules("centers_id", "Center", "required");
        $this->form_validation->set_rules("tax_name", "Tax Name", "required");
        $this->form_validation->set_rules("tax_type", "Tax Type", "required");
        $this->form_validation->set_rules("tax_amount", "Tax Amount", "required");
        $this->form_validation->set_rules("tax_apply_above", "Tax Apply Above", "required");
        $this->form_validation->set_rules("start_date", "Start Date", "required");

        if (!is_numeric($this->post('tax_amount'))) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => "Tax amount is not an integer"
            ];
            $this->response($arr);
            die;
        }
        if (!is_numeric($this->post('tax_apply_above'))) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => "Tax Apply Above is not an integer"
            ];
            $this->response($arr);
            die;
        }
        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => validation_errors()
            ];
            $this->response($arr);
            die;
        }

        $start_date = convert_date_to_db_format($this->post('start_date'));

        if ($this->post('apply_until_changed') == "Yes") {
            $end_date = null;
        } else {
            $end_date = convert_date_to_db_format($this->post('end_date'));
        }


        $data = [
            "centers_id" => $this->post('centers_id'),
            "tax_name" => $this->post('tax_name'),
            "tax_type" => $this->post("tax_type"),
            "tax_amount" => $this->post("tax_amount"),
            "tax_apply_above" => $this->post("tax_apply_above"),
            "start_date" => $start_date,
            "end_date" => $end_date,
            "apply_until_changed" => $this->post('apply_until_changed'),
            "tax_status" => $this->post('tax_status')
        ];

        if ($this->input->get_post("id")) {
            return $this->update($data, $this->post("id"));
        }

        $response = $this->{$this->model_name}->add($data);
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " added successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not added, please try again"
            ];
        }
        $this->response($arr);
    }

    function update($data) {
        check_for_access(27);
        $response = $this->{$this->model_name}->update($data, $this->post('id'));
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " updated successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not updated, please try again"
            ];
        }
        $this->response($arr);
    }

    function delete_post() {
        check_for_access(27);
        $id = (int) $this->post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        $this->response($arr);
    }

    /* function mobile_check($str) {
      $id = (int) $this->post('id');
      if ($this->{$this->model_name}->is_mobile_exists($str, $id)) {
      $this->form_validation->set_message('mobile_check', 'The {field} is already existed');
      return FALSE;
      } else {
      return TRUE;
      }
      } */

    function fetch_by_centers_id_get() {
        if ($this->get('centers_id') && $this->get('check_in_date') && $this->get('check_in_time') && $this->get('duration')) {
            $parameters = (object) array();
            $parameters->centers_id = (int) $this->get("centers_id");
            $parameters->check_in_date = date('Y-m-d', strtotime($this->get('check_in_date')));
            $parameters->duration = $this->get("duration");
            $data = $this->{$this->model_name}->get_by_centers_id($parameters);
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => $this->title . " list",
                "data" => $data
            ];
            $this->response($arr);
        } else {
            $data = [];
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => "Please provide required details",
                "data" => $data
            ];
        }
        $this->response($arr);
    }

}
