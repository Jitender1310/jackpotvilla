<?php

    class Dashboard extends REST_Controller {

        private $model_name = "dashboard_model";
        private $title = "Dashboard";

        function __construct() {
            parent::__construct();
            $this->load->model($this->model_name);
        }

        function index_post() {
            $stats = [];
            $stats["total_real_players_cnt"] = $this->dashboard_model->get_total_real_players();
            $stats["cash_games_cnt"] = $this->dashboard_model->get_total_real_players();
            $stats["practice_games_cnt"] = $this->dashboard_model->get_total_practice_games_cnt();
            $stats["upcoming_tournaments_cnt"] = $this->dashboard_model->get_upcoming_tournaments_cnt();
            $stats["pending_withdraw_requests"] = $this->dashboard_model->get_pending_withdraw_requests_cnt();
            for ($i = 1; $i < 7; $i++) {
                $stats['players'][$i] = $this->dashboard_model->get_monthly_players($i);
            }

            $arr = [
                'err_code' => "valid",
                "message" => $this->title . " list",
                "data" => $stats
            ];
            $this->response($arr);
        }

        function games_post() {
            $filters['day'] = $this->post('day');
            $data['counts']->one_zero_one_pool_rummy = $this->dashboard_model->get_total_games($filters, 'Pool', 101);
            $data['counts']->two_zero_one_pool_rummy = $this->dashboard_model->get_total_games($filters, 'Pool', 201);
            $data['counts']->points_rummy = $this->dashboard_model->get_total_games($filters, 'Points');
            $data['counts']->deals_rummy = $this->dashboard_model->get_total_games($filters, 'Deals');
            $arr = [
                'err_code' => "valid",
                "message" => $this->title . " list",
                "data" => $data
            ];
            $this->response($arr);
        }

        // This API is being used in new backoffice
        function index_get() {
            $all_users = $this->dashboard_model->get_all_players();

            $totalUsers = 0;
            $totalDeposits = 0;
            $totalWithdrawals = 0;
            $totalTransactions = 0;

            $final_output = array();
            $final_output['Users']['totalCount'] = $totalUsers;
            $final_output['Users']['dateCount'] = array();
            $final_output['Deposits']['totalCount'] = $totalDeposits;
            $final_output['Deposits']['dateCount'] = array();
            $final_output['Withdrawals']['totalCount'] = $totalWithdrawals;
            $final_output['Withdrawals']['dateCount'] = array();
            $final_output['Transactions']['totalCount'] = $totalTransactions;
            $final_output['Transactions']['dateCount'] = array();

            if($all_users)
            {
                foreach($all_users as $user)
                {
                    $totalUsers += $user['count'];
                    $tempArray[_id] = array(
                        'year' => $user['year'],
                        'month' => $user['month'],
                        'day' => $user['day']
                    );
                    $tempArray['date'] = $user['year'] . '-' . $user['month'] . '-' . $user['day'];
                    $tempArray['count'] = $user['count'];
                    $dateCount[] = $tempArray;
                }
                $final_output['Users']['totalCount'] = $totalUsers;
                $final_output['Users']['dateCount'] = $dateCount;
            }

            $all_deposits = $this->dashboard_model->get_all_deposits();
            if($all_deposits)
            {
                foreach($all_deposits as $deposit)
                {
                    $totalDeposits += $deposit['count'];
                    $tempArray[_id] = array(
                        'year' => $deposit['year'],
                        'month' => $deposit['month'],
                        'day' => $deposit['day']
                    );
                    $tempArray['date'] = $deposit['year'] . '-' . $deposit['month'] . '-' . $deposit['day'];
                    $tempArray['count'] = $deposit['count'];
                    $dateCount[] = $tempArray;
                }
                $final_output['Deposits']['totalCount'] = $totalDeposits;
                $final_output['Deposits']['dateCount'] = $dateCount;
            }

            $all_withdrawals = $this->dashboard_model->get_all_withdrawals();
            if($all_withdrawals)
            {
                foreach($all_withdrawals as $withdrawal)
                {
                    $totalWithdrawals += $withdrawal['count'];
                    $tempArray[_id] = array(
                        'year' => $withdrawal['year'],
                        'month' => $withdrawal['month'],
                        'day' => $withdrawal['day']
                    );
                    $tempArray['date'] = $withdrawal['year'] . '-' . $withdrawal['month'] . '-' . $withdrawal['day'];
                    $tempArray['count'] = $withdrawal['count'];
                    $dateCount[] = $tempArray;
                }
                $final_output['Withdrawals']['totalCount'] = $totalWithdrawals;
                $final_output['Withdrawals']['dateCount'] = $dateCount;
            }

            $all_transactions = $this->dashboard_model->get_all_transactions();
            if($all_transactions)
            {
                foreach($all_transactions as $transaction)
                {
                    $totalTransactions += $transaction['count'];
                    $tempArray[_id] = array(
                        'year' => $transaction['year'],
                        'month' => $transaction['month'],
                        'day' => $transaction['day']
                    );
                    $tempArray['date'] = $transaction['year'] . '-' . $transaction['month'] . '-' . $transaction['day'];
                    $tempArray['count'] = $transaction['count'];
                    $dateCount[] = $tempArray;
                }
                $final_output['Transactions']['totalCount'] = $totalTransactions;
                $final_output['Transactions']['dateCount'] = $dateCount;
            }

            $arr = $final_output;
            $this->response($arr);
        }

    }
