<?php

class Gameplay_data extends REST_Controller {

    private $model_name = 'gameplay_data_model';
    private $title = "Gameplay Data";
    
    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
    }
    
    function index_get() {
        if( $this->input->get_post('page') ){
            $filters['page'] = $this->input->get_post('page');
        }
        
        if( $this->input->get_post('limit') ){
            $filters['limit'] = $this->input->get_post('limit');
        }
        
        if( $this->input->get_post('page') ){
            $filters['sort_by'] = $this->input->get_post('sort_by');
        }
        
        if( $this->input->get_post('game_type') ){
            $filters['game_type'] = $this->input->get_post('game_type');
        }
        
        if( $this->input->get_post('game_sub_type') ){
            $filters['game_sub_type'] = $this->input->get_post('game_sub_type');
        }
        
        if( $this->input->get_post('rooms_id') ){
            $filters['rooms_id'] = $this->input->get_post('rooms_id');
        }
        
        $total_results =  $this->{$this->model_name}->get_data($filters);
        
        $limit = $this->input->get_post("limit") ? $this->input->get_post("limit") : ORDERS_PER_PAGE;
        $_GET['page'] = $this->input->get_post("page");
        $start = $this->input->get_post("page") > 1 ? ($this->input->get_post("page") - 1) * $limit : 0;
        $filters["limit"] = $limit;
        $filters["start"] = $start;
		
	$result["total_results_found"] = $total_results;
        $result["total_pages"] = ceil($total_results/$limit);
        
        $result["results"] = $this->{$this->model_name}->get_data($filters);

        $pagination = my_pagination("gameplay_data", $limit, $total_results, true);
        $pagination['pagination'] = str_replace("&amp;", "&", $pagination['pagination']);
        $cur_page = ($pagination['pagination_helper']->cur_page != 0) ? ($pagination['pagination_helper']->cur_page - 1) : $pagination['pagination_helper']->cur_page;
        $pagination['initial_id'] = (($cur_page) * $pagination['pagination_helper']->per_page) + 1;
        
        $arr = [
            'status' => "valid",
            "message" => $this->title . " list",
            "data" => $result,
            "pagination" => $pagination
        ];
        $this->response($arr);
    }

}
