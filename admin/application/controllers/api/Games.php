<?php

class Games extends REST_Controller {
    private $model_name = "games_model";
    private $title = "Games";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
    }

    // This API is being used in new backoffice
    function all_get() {
        $all_games = $this->games_model->get_all_games();
        $this->response($all_games);
    }

    function index_put(){
        $game_id = $this->put('id');
        $active = $this->put('active');
        $update_data = [
            "active" => $active
        ];

        $this->games_model->update_game($game_id, $update_data);

        $arr = [
            "success"=> "true"
        ];
        $this->response($arr);
    }

    function transactions_get(){
        $player_id = $this->get('id');
        $all_transactions = $this->players_model->get_transactions_by_player($player_id);
        $this->response($all_transactions);
    }

    function deposits_get(){
        $player_id = $this->get('id');
        $all_deposits = $this->players_model->get_deposits_by_player($player_id);
        $this->response($all_deposits);
    }

    function withdrawals_get(){
        $player_id = $this->get('id');
        $all_withdrawals = $this->players_model->get_withdrawals_by_player($player_id);
        $this->response($all_withdrawals);
    }

}
