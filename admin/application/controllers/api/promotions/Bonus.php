<?php

class Bonus extends REST_Controller {

    private $model_name = "bonus_model";
    private $title = "Bonus";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
    }

    function index_get() {
        $data = $this->{$this->model_name}->get();
        $arr = [
            'status' => "valid",
            'title' => "",
            "message" => $this->title . " list",
            "data" => json_decode(json_encode($data, JSON_NUMERIC_CHECK))
        ];
        $this->response($arr);
    }

    function add_post() {
        if ($this->input->get_post("id")) {
            check_for_access(18, "update");
        } else {
            check_for_access(18, "add");
        }
        $this->form_validation->set_rules("bonus_type", "Bonus type", "required", array(
            'required' => 'Please choose game type'
        ));
        $this->form_validation->set_rules("title", "Game Sub Type", "required", array(
            'required' => 'Please choose game sub type'
        ));
        $this->form_validation->set_rules("description", "Seats", "required", array(
            'required' => 'Please choose seats'
        ));
        
        $this->form_validation->set_rules("points_percentage", "Points Percentage", "required", array(
            'required' => 'Points Percentage cannot be empty'
        ));
        $this->form_validation->set_rules("max_points", "Maximum Limit", "required", array(
            'required' => 'Maximum Limit cannot be empty'
        ));
        
        $this->form_validation->set_rules("from_date_time", "Entry Fee", "required", array(
            'required' => 'Entry fee cannot be empty'
        ));
        $this->form_validation->set_rules("to_date_time", "Entry Fee", "required", array(
            'required' => 'Entry fee cannot be empty'
        ));
        $this->form_validation->set_rules("terms_conditions", "Entry Fee", "required", array(
            'required' => 'Entry fee cannot be empty'
        ));
        
        $this->form_validation->set_rules("usage_times", "Usage times", "required", array(
            'required' => 'Usage times cannot be empty'
        ));
        
        $this->form_validation->set_rules("credit_to", "Credit to", "required", array(
            'required' => 'Choose Credit to'
        ));

        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "coupon_code" => form_error("coupon_code"),
                "bonus_type" => form_error("bonus_type"),
                "title" => form_error("title"),
                "description" => form_error("description"),
                "from_date_time" => form_error("from_date_time"),
                "to_date_time" => form_error("to_date_time"),
                "terms_conditions" => form_error("terms_conditions"),
                "points_percentage" => form_error("points_percentage"),
                "max_points" => form_error("max_points"),
                "usage_times" => form_error("usage_times"),
                "credit_to" => form_error("credit_to"),
                "valid_days" => form_error("valid_days")
            ];
            $arr = [
                "status" => "invalid_form",
                "data" => $errors
            ];
            $this->response($arr);
            die;
        } else {

            $valid_days = 0;
            $points_percentage =$this->post("points_percentage");
            $usage_times = $this->post("usage_times");
            if(!$usage_times){
                $usage_times = 0;
            }
            
            $credit_to = $this->post("credit_to");
            if($credit_to=="Bonus Wallet"){
                $valid_days = $this->post("valid_days");
            }else if($credit_to=="Deposit Wallet"){
                $valid_days = 0;
            }
            $data = [
                "coupon_code" => $this->post("coupon_code"),
                "bonus_type" => $this->post("bonus_type"),
                "title" => $this->post("title"),
                "description" => $this->post("description"),
                "from_date_time" => convert_date_time_to_db_format($this->post("from_date_time")),
                "to_date_time" => convert_date_time_to_db_format($this->post("to_date_time")),
                "terms_conditions" => $this->post("terms_conditions"),
                "points_percentage" => $points_percentage,
                "max_points" => $this->post("max_points"),
                "usage_times" => $usage_times,
                "credit_to" => $credit_to,
                "valid_days" => $valid_days
            ];
            
            if ($this->input->get_post("id")) {
                return $this->update($data, $this->post("id"));
            }
            $response = $this->{$this->model_name}->add($data);
            if ($response) {
                $arr = [
                    'status' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " added successfully"
                ];
            } else {
                $arr = [
                    'status' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not added, please try again"
                ];
            }
            $this->response($arr);
        }
    }

    function update($data) {
        check_for_access(10);
        $response = $this->{$this->model_name}->update($data, $this->post('id'));
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " updated successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . "  not updated, please try again"
            ];
        }
        $this->response($arr);
    }

    function delete_post() {
        check_for_access(18);
        $id = (int) $this->post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        $this->response($arr);
    }
}
