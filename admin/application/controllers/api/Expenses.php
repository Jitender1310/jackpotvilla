<?php

    class Expenses extends REST_Controller {

        private $model_name = "expenses_model";
        private $title = "Expenses";

        function __construct() {
            parent::__construct();
            $this->load->model($this->model_name);
            $this->load->model('centers_model');
            $this->load->model('user_model');
            $this->load->model('expenses_categories_model');
            $this->load->model('notifications_model');
        }

        function index_post() {
            $filters = array();
            $filters["from_date"] = $this->post('from_date') ? strtotime($this->post('from_date')) : '';
            $filters["to_date"] = $this->post('to_date') ? strtotime($this->post('to_date') . ' 23:59:59') : '';
            $filters["centers_id"] = $this->post('centers_id') ? $this->post('centers_id') : '';
            $filters["expense_categories_id"] = $this->post('expense_categories_id') ? $this->post('expense_categories_id') : '';
            $filters["type"] = $this->post('type') ? $this->post('type') : '';
            $filters["finance_status"] = $this->post('finance_status') ? $this->post('finance_status') : '';


            $limit = $this->input->get_post("limit") ? $this->input->get_post("limit") : 10;
            $_GET['start'] = $this->input->get_post("page");
            $start = $this->input->get_post("page") > 1 ? ($this->input->get_post("page") - 1) * $limit : 0;

            $total_results = count($this->{$this->model_name}->get($filters));

            $filters["limit"] = $limit;
            $filters["start"] = $start;
            $pagination = my_pagination("expenses", $limit, $total_results, true);
            $pagination['pagination'] = str_replace("&amp;", "&", $pagination['pagination']);
            $cur_page = ($pagination['pagination_helper']->cur_page != 0) ? ($pagination['pagination_helper']->cur_page - 1) : $pagination['pagination_helper']->cur_page;
            $pagination['initial_id'] = (($cur_page) * $pagination['pagination_helper']->per_page) + 1;
            $data = $this->{$this->model_name}->get($filters);
            foreach ($data as $item) {
                $item->added_time = date('d-m-Y H:i:s', $item->created_at);
                if ($item->updated_at) {
                    $item->updated_time = date('d-m-Y H:i:s', $item->updated_at);
                }
                if ($item->expense_categories_id == 8) {
                    $item->is_other_expense = 'Yes';
                }

                $item->center_name = $this->centers_model->get_center_name($item->centers_id);
                $item->user_name1 = $this->user_model->get_user_details($item->added_by_user_id)->fullname;
                $item->user_name2 = $this->user_model->get_user_details($item->updated_by_user_id)->fullname;
                $item->expense_category = $this->expenses_categories_model->get_category_name($item->expense_categories_id);
                $item->transaction_type_bootstrap_class = (isset($item->type) && ($item->type == 'Credit')) ? 'success' : 'danger';
            }
            $arr = [
                'err_code' => "valid",
                "message" => $this->title . " list",
                "pagination" => $pagination,
                "data" => $data
            ];
            $this->response($arr);
        }

        function get_balance_post() {
            $center_id = $this->post('centers_id');
            $data = $this->{$this->model_name}->get_balance($center_id);
            $arr = [
                'err_code' => "valid",
                "message" => "Balance",
                "data" => $data
            ];
            $this->response($arr);
        }

        function opening_balance_post() {
            $center_id = $this->post('centers_id');
            $data = $this->{$this->model_name}->get_opening_balance($center_id);
            $arr = [
                'err_code' => "valid",
                "message" => "Opening Balance",
                "data" => $data
            ];
            $this->response($arr);
        }

        function get_expenses_post() {
            $centers_id = $this->post('centers_id');
            $data['today_expenses'] = $this->{$this->model_name}->get_expenses($centers_id, 'today')->expenses;
            $data['this_week_expenses'] = $this->{$this->model_name}->get_expenses($centers_id, 'week')->expenses;
            $data['this_month_expenses'] = $this->{$this->model_name}->get_expenses($centers_id, 'month')->expenses;

            $data['total_expenses'] = $this->{$this->model_name}->get_expenses($centers_id)->expenses;

            $arr = [
                'err_code' => "valid",
                "data" => $data
            ];
            $this->response($arr);
        }

        function add_post() {
            $this->form_validation->set_rules("centers_id", "Centers", "required");
            $this->form_validation->set_rules("expense_categories_id", "Expense Category", "required");
            $this->form_validation->set_rules("title", "Title", "required");
            $this->form_validation->set_rules("amount", "Amount", "required");
            $this->form_validation->set_rules("date", "Date", "required");
            $this->form_validation->set_rules("remarks", "Remarks", "required");
            if ($this->form_validation->run() == FALSE) {
                $arr = [
                    'err_code' => "invalid_form",
                    'title' => "",
                    "message" => validation_errors()
                ];
                $this->response($arr);
                die;
            }
            $balance = $this->{$this->model_name}->get_balance($this->post('centers_id'));

            if ($balance < $this->post('amount')) {
                $arr = [
                    'err_code' => "invalid_form",
                    'title' => "Warning",
                    "message" => "Center wallet balance is low please reacharge to make this expense (wallet balance - " . $balance . " Rs/- only)"
                ];
                $this->response($arr);
                die;
            }
            $data = [
                "centers_id" => $this->post('centers_id'),
                "expense_categories_id" => $this->post('expense_categories_id'),
                "title" => $this->post('title'),
                "type" => 'Debit',
                "amount" => $this->post('amount'),
                "date" => $this->post('date'),
                "str_date" => strtotime($this->post('date')),
                "remarks" => $this->post('remarks')
            ];
            $user_id = get_user_id();
            if ($this->post('expense_categories_id') == 8) {
                if ($this->post('finance_status') != '') {
                    $data['finance_status'] = $this->post('finance_status');
                } else {
                    $data['finance_status'] = 'Pending';
                }
            }

            if (@$_FILES["attachment1"]["name"]) {
                $config['upload_path'] = FILE_UPLOAD_FOLDER . "expense_receipts/";
                $config['allowed_types'] = 'jpg|jpeg|png|JPG|JPEG|PNG|PDF|pdf|DOCX|docx|DOC|doc';
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('attachment1')) {
                    $this->data['error'] = array('error' => $this->upload->display_errors());
                    $arr = array(
                        'err_code' => "invalid",
                        "message" => strip_tags($this->data['error']["error"]),
                        "title" => "Invalid",
                        "error" => strip_tags($this->data['error'])
                    );
                    $this->response($arr);
                    die;
                } else {
                    $uploaded_data = array('upload_data' => $this->upload->data());
                    $file_name = $uploaded_data["upload_data"]["file_name"];
                    $data["attachment1"] = $file_name;
                }
            }
            if (@$_FILES["attachment2"]["name"]) {
                $config['upload_path'] = FILE_UPLOAD_FOLDER . "expense_receipts/";
                $config['allowed_types'] = 'jpg|jpeg|png|JPG|JPEG|PNG|PDF|pdf|DOCX|docx|DOC|doc';
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('attachment2')) {
                    $this->data['error'] = array('error' => $this->upload->display_errors());
                    $arr = array(
                        'err_code' => "invalid",
                        "message" => strip_tags($this->data['error']["error"]),
                        "title" => "Invalid",
                        "error" => strip_tags($this->data['error'])
                    );
                    $this->response($arr);
                    die;
                } else {
                    $uploaded_data = array('upload_data' => $this->upload->data());
                    $file_name = $uploaded_data["upload_data"]["file_name"];
                    $data["attachment2"] = $file_name;
                }
            }

            if (@$_FILES["attachment3"]["name"]) {
                $config['upload_path'] = FILE_UPLOAD_FOLDER . "expense_receipts/";
                $config['allowed_types'] = 'jpg|jpeg|png|JPG|JPEG|PNG|PDF|pdf|DOCX|docx|DOC|doc';
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('attachment3')) {
                    $this->data['error'] = array('error' => $this->upload->display_errors());
                    $arr = array(
                        'err_code' => "invalid",
                        "message" => strip_tags($this->data['error']["error"]),
                        "title" => "Invalid",
                        "error" => strip_tags($this->data['error'])
                    );
                    $this->response($arr);
                    die;
                } else {
                    $uploaded_data = array('upload_data' => $this->upload->data());
                    $file_name = $uploaded_data["upload_data"]["file_name"];
                    $data["attachment3"] = $file_name;
                }
            }



            if ($this->post('expense_categories_id') == 8) {
                $center_name = $this->centers_model->get_center_name($this->post('centers_id'));
                $user_name = $this->user_model->get_user_details($user_id)->fullname;
                $title = $this->post('title');
                $amount = $this->post('amount');
                if ($this->post('finance_status') != 'Pending' && $this->post('finance_status') != '') {
                    $notification = "Finance admin " . $user_name . " is " . $this->post('finance_status') . " request for " . $title . " of amount " . $amount . " Rs/-";
                } else {
                    $notification = "From " . $center_name . " center " . $user_name . " is seeking approval for " . $title . " of amount " . $amount . " Rs/-";
                }
                $ndata = [
                    'notification' => $notification,
                    'user_id' => get_user_id(),
                    'centers_id' => $this->post('centers_id'),
                    'finance_status' => $this->post('finance_status')
                ];
                if ($this->post('id')) {

                    $expense = $this->{$this->model_name}->get($filters);
                    if ($expense[0]->finance_status != $this->post('finance_status') || $expense[0]->amount != $this->post('amount')) {
                        $this->notifications_model->notification_add($ndata);
                    }
                }
            }

            if ($this->post("id")) {
                $data['updated_by_user_id'] = get_user_id();
                $this->update($data, $this->post("id"));
            } else {
                $data['added_by_user_id'] = get_user_id();
                $response = $this->{$this->model_name}->add($data);
                if ($this->post('expense_categories_id') == 8) {
                    $this->notifications_model->notification_add($ndata);
                }
            }
            if ($this->post("id") != '') {
                $id = $this->post("id");
            } else {
                $id = $response;
            }


            if ($response) {
                $arr = [
                    'err_code' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " added successfully"
                ];
            } else {
                $arr = [
                    'err_code' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " unable to add expense"
                ];
            }
            $this->response($arr);
        }

        function update($data) {
            $response = $this->{$this->model_name}->update($data, $this->post('id'));

            if ($response) {
                $arr = [
                    'err_code' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " updated successfully"
                ];
            } else {
                $arr = [
                    'err_code' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not updated, please try again"
                ];
            }
            $this->response($arr);
        }

        function delete_post() {
            $id = (int) $this->post("id");
            $response = $this->{$this->model_name}->delete($id);
            if ($response) {
                $arr = [
                    'err_code' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " deleted successfully"
                ];
            } else {
                $arr = [
                    'err_code' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not deleted, please try again"
                ];
            }
            $this->response($arr);
        }

    }
    