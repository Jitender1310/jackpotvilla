<?php

class Send_sms extends REST_Controller {

    private $model_name = "send_sms_model";
    private $title = "Message";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
        $this->load->model('sms_templates_model');
    }

    function index_post() {

        $this->form_validation->set_rules("mobile[]", "Mobile", "required");
        $this->form_validation->set_rules("template_id", "Title", "required");
        $this->form_validation->set_rules("message", "Message", "required");

        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => validation_errors()
            ];
            $this->response($arr);
            die;
        }

        $data['title'] = $this->sms_templates_model->get_message($this->post('template_id'))->title;
        $data['message'] = $this->post('message');
        $mobiles = $this->post('mobile');
        foreach ($mobiles as $key) {
            $this->send_sms_model->send_message($key['text'], $data);
        }
        $arr = [
            'err_code' => "valid",
            'title' => "",
            "message" => "Message sent successfully"
        ];

        $this->response($arr);
    }

}
