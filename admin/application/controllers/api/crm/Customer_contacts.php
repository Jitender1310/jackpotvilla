<?php

class Customer_contacts extends REST_Controller {

    private $model_name = "Customer_contacts_model";
    private $title = "Customers";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
    }

    function index_get() {
        $data = $this->{$this->model_name}->get_customers_list();
        $arr = [
            'err_code' => "valid",
            "message" => $this->title . " list",
            "data" => $data
        ];
        $this->response($arr);
    }

    function delete_post() {
        $id = (int) $this->post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        $this->response($arr);
    }

}
