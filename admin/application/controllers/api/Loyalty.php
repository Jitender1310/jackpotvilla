<?php

class Loyalty extends REST_Controller {
    private $model_name = "loyalty_model";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
    }

    function index_post(){
        $slab_data['name'] = $this->post('name');
        $slab_data['min_points'] = $this->post('min_points');
        $slab_data['max_points'] = $this->post('max_points');
        $slab_data['bonus_percentage'] = $this->post('bonus_percentage');
        $slab_data['wager_factor'] = $this->post('wager_factor');
        $slab_data['eligible_games'] = $this->post('eligible_games') ? json_encode($this->post('eligible_games')) : NULL;
        $slab_data['reward_times'] = $this->post('reward_times');

        $result = $this->loyalty_model->add_slab($slab_data);

        $arr = [
            "success"=> $result
        ];
        $this->response($arr);
    }

    // This API is being used in new backoffice
    function all_get() {
        $all_slabs = $this->loyalty_model->get_all_slabs();

        $arr = $all_slabs;
        $this->response($arr);
    }

    function index_get(){
        $slab_id = $this->get('id');
        $slab_detail = $this->loyalty_model->get_slab_by_id($slab_id);

        $arr = $slab_detail[0];
        $this->response($arr);
    }

    function index_put(){
        $slab_id = $this->put('id');

        $slab_data['name'] = $this->put('name');
        $slab_data['min_points'] = $this->put('min_points');
        $slab_data['max_points'] = $this->put('max_points');
        $slab_data['bonus_percentage'] = $this->put('bonus_percentage');
        $slab_data['wager_factor'] = $this->put('wager_factor');
        $slab_data['eligible_games'] = $this->put('eligible_games') ? json_encode($this->put('eligible_games')) : NULL;
        $slab_data['reward_times'] = $this->put('reward_times');

        $this->loyalty_model->update_slab($slab_id, $slab_data);

        $arr = [
            "success"=> "true"
        ];
        $this->response($arr);
    }
}
