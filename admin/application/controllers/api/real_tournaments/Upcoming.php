<?php

    class Upcoming extends REST_Controller {

        private $model_name = "upcoming_tournaments_model";
        private $title = "Upcoming Tournaments list";

        function __construct() {
            parent::__construct();
            $this->load->model($this->model_name);
            check_for_access(19);
        }

        function index_get() {
            $this->index_post();
        }

        function index_post() {
            $filters['frequency'] = $this->post('frequency');
            $filters['tournament_categories_id'] = $this->post('tournament_categories_id');
            if ($filters['tournament_categories_id'] == 5) {
                $filters['premium_category'] = $this->post('premium_category');
            }

            if ($this->post('from_date')) {
                $filters['from_date'] = convert_date_to_db_format($this->post('from_date'));
            }
            if ($this->post('to_date')) {
                $filters['to_date'] = convert_date_to_db_format($this->post('to_date'));
            }

            if ($this->post('type')) {
                $filters['type'] = $this->post('type');
            } else {
                $filters['type'] = "Upcoming";
            }

            $data = $this->{$this->model_name}->get($filters);

            $arr = json_decode(json_encode([
                'status' => "valid",
                "message" => $this->title . " list",
                "data" => $data
                            ], JSON_NUMERIC_CHECK));

            $this->response($arr);
        }

        function tournament_progress_post() {
            $cloned_tournaments_id = $this->input->get_post("cloned_tournaments_id");
            $response = $this->{$this->model_name}->get_progress($cloned_tournaments_id);
            if ($response) {
                $arr = [
                    "status" => "valid",
                    "title" => "Tournament running info",
                    "message" => "Tournament running info",
                    "data" => [
                        "round_info" => $response
                    ]
                ];
            } else {
                $arr = [
                    "status" => "invalid",
                    "title" => "Game Progress",
                    "message" => "No progress recorded",
                    "data" => [
                        "message" => "Some error occured"
                    ]
                ];
            }
            $this->response($arr);
        }

        function add_post() {
            if ($this->input->get_post("id")) {
                check_for_access(21, "update");
            } else {
                check_for_access(21, "add");
            }
            $this->form_validation->set_rules("tournament_categories_id", "Tournament Category", "required", array(
                'required' => 'Please choose Tournament Category'
            ));
            if ($this->post('tournament_categories_id') == 5) {
                $this->form_validation->set_rules("premium_category", "Premium Category", "required", array(
                    'required' => 'Please choose Premium Category'
                ));
            }
            $this->form_validation->set_rules("tournament_title", "Tournament Title", "required", array(
                'required' => 'Tournament title cannot be empty'
            ));
            $this->form_validation->set_rules("frequency", "Game Sub Type", "required", array(
                'required' => 'Please choose frequency'
            ));

            $this->form_validation->set_rules("entry_type", "Entry type", "required", array(
                'required' => 'Please choose Entry type'
            ));

            $this->form_validation->set_rules("entry_value", "Entry value", "required", array(
                'required' => 'Entry value cannot be blank'
            ));

            $this->form_validation->set_rules("max_players", "Max Players", "required", array(
                'required' => 'Max Players cannot be empty'
            ));

            if ($this->post("frequency") == "Monthly" || $this->post("frequency") == "One Time") {
                $this->form_validation->set_rules("registration_start_date", "Reg Start date", "required", array(
                    'required' => 'Reg Start date cannot be empty'
                ));
                $this->form_validation->set_rules("registration_close_date", "Reg Close date", "required", array(
                    'required' => 'Reg Close date cannot be empty'
                ));
                $this->form_validation->set_rules("tournament_start_date", "Tournament Start date", "required", array(
                    'required' => 'Tournament Start date cannot be empty'
                ));
            } else if ($this->post("frequency") == "Weekly") {
                $this->form_validation->set_rules("week", "Week", "required", array(
                    'required' => 'Week name cannot be empty'
                ));
            } else if ($this->post("frequency") == "Daily") {
                
            } else if ($this->post("frequency") == "One Time") {
                
            }

            $this->form_validation->set_rules("registration_start_time", "Reg Start time", "required", array(
                'required' => 'Reg Start time cannot be empty'
            ));
            $this->form_validation->set_rules("registration_close_time", "Reg Close time", "required", array(
                'required' => 'Reg Close time cannot be empty'
            ));
            $this->form_validation->set_rules("tournament_start_time", "Tournament Start time", "required", array(
                'required' => 'Tournament Start time cannot be empty'
            ));

            $this->form_validation->set_rules("prize_type", "Prize type", "required", array(
                'required' => 'Please choose prize type'
            ));

            $this->form_validation->set_rules("expected_prize_amount", "Prize type", "required", array(
                'required' => 'Expected Prize Amount cannot be empty'
            ));

            if ($this->post('allowed') == "For Selected Clubs") {
                $this->form_validation->set_rules("allowed_club_types", "Allowed Club types", "required", array(
                    'required' => 'Please choose club types'
                ));
            }

            $this->form_validation->set_rules("min_players_to_start_tournament", "Min players to start tournament", "required", array(
                'required' => 'Min players to start tournament cannot be empty'
            ));


            if ($this->form_validation->run() == FALSE) {
                $errors = [];
                foreach ($_POST as $key => $value) {
                    $errors[$key] = form_error($key);
                }
                $arr = [
                    "status" => "invalid_form",
                    "data" => $errors
                ];
                $this->response($arr);
                die;
            } else {
                $frequency = $this->post('frequency');
                $registration_start_date = convert_date_to_db_format($this->post('registration_start_date'));
                $registration_close_date = convert_date_to_db_format($this->post('registration_close_date'));
                $tournament_start_date = convert_date_to_db_format($this->post('tournament_start_date'));

                $registration_start_time = convert_time_db_format($this->post("registration_start_time"));
                $registration_close_time = convert_time_db_format($this->post("registration_close_time"));
                $tournament_start_time = convert_time_db_format($this->post("tournament_start_time"));

                $week = $this->post("week");

                $allowed_club_types = $this->post("allowed_club_types");
                if (is_array($allowed_club_types)) {
                    $allowed_club_types = implode(",", $allowed_club_types);
                } else {
                    $allowed_club_types = "";
                }
                $data = [
                    "tournament_categories_id" => $this->post("tournament_categories_id"),
                    "winning_probability_to" => $this->post("winning_probability_to"),
                    "no_of_bots" => $this->post("no_of_bots"),
                    "allowed_club_types" => $allowed_club_types,
                    "tournament_title" => $this->post("tournament_title"),
                    "frequency" => $frequency,
                    "entry_type" => $this->post("entry_type"),
                    "entry_value" => $this->post("entry_value"),
                    "max_players" => $this->post("max_players"),
                    "token" => $this->post("token"),
                    "min_players_to_start_tournament" => $this->post("min_players_to_start_tournament"),
                    "registration_start_date" => $registration_start_date,
                    "registration_close_date" => $registration_close_date,
                    "registration_close_time" => $registration_close_time,
                    "tournament_start_date" => $tournament_start_date,
                    "registration_start_time" => $registration_start_time,
                    "tournament_start_time" => $tournament_start_time,
                    "prize_type" => $this->post("prize_type"),
                    "expected_prize_amount" => $this->post("expected_prize_amount"),
                    "week" => $week,
                    "prizes_info" => $this->post("prizes_info"),
                    "last_action_by_users_id" => get_user_id(),
                ];
                if ($this->post("tournament_categories_id") == 5) {
                    $data['premium_category'] = $this->post("premium_category");
                }
                if ($this->post("entry_type") == "Free") {
                    $data['tournament_commission_percentage'] = 0;
                } else {
                    $data['tournament_commission_percentage'] = $this->post("tournament_commission_percentage");
                }

                $tournament_structure = [];
                for ($i = 0, $l = count($_POST["tournament_structure"]); $i < $l; $i++) {
                    $tournament_structure[] = [
                        "round_number" => $i + 1,
                        "qualify" => $_POST["tournament_structure"][$i]["qualify"]
                    ];
                }
                $data['tournament_structure'] = json_encode($tournament_structure);


                if ($this->input->get_post("id")) {
                    return $this->update($data, $this->post("id"));
                }
                $response = $this->{$this->model_name}->add($data);
                if ($response) {
                    $arr = [
                        'status' => "valid",
                        'title' => "",
                        "message" => singular($this->title) . " added successfully"
                    ];
                } else {
                    $arr = [
                        'status' => "invalid",
                        'title' => "",
                        "message" => singular($this->title) . " not added, please try again"
                    ];
                }
                $this->response($arr);
            }
        }

        function update($data) {
            check_for_access(21);
            $response = $this->{$this->model_name}->update($data, $this->post('id'));
            if ($response === "GAME_IN_PLAY") {
                $arr = [
                    'status' => "invalid",
                    'title' => "",
                    "message" => "Game cannot be deactivated at this time, because some player is playing this game"
                ];
            } else if ($response) {
                $arr = [
                    'status' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " updated successfully"
                ];
            } else {
                $arr = [
                    'status' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . "  not updated, please try again"
                ];
            }
            $this->response($arr);
        }

        function delete_post() {
            check_for_access(21);
            $id = (int) $this->post("id");
            $response = $this->{$this->model_name}->delete($id);
            if ($response) {
                $arr = [
                    'status' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " deleted successfully"
                ];
            } else {
                $arr = [
                    'status' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not deleted, please try again"
                ];
            }
            $this->response($arr);
        }

    }
    