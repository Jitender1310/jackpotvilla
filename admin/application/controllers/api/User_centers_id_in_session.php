<?php

class User_centers_id_in_session extends REST_Controller {

    private $model_name = "login_model";
    private $title = "Update center id in session";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
        $this->load->model('centers_model');
    }

    function index_get() {
        if ($this->session->userdata('centers_id')) {
            $centers_id = $this->session->userdata('centers_id');
            $arr = [
                'err_code' => "valid",
                'title' => "Selected center",
                "message" => "",
                "data" => $centers_id,
                "available_stay_types" => $this->centers_model->get_available_stay_types_for_reservations($centers_id)
            ];
            $this->response($arr);
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "Oops",
                "message" => "Please choose center",
                "data" => "",
                "available_stay_types" => ""
            ];
            $this->response($arr);
        }
    }

    function update_get() {
        if ($this->get('centers_id')) {
            $centers_id = (int) $this->get('centers_id');
            $user_id = (int) $this->get("users_id");
            $permission = $this->centers_model->is_user_has_center_assigned($user_id, $centers_id);
            if (!$permission) {
                $arr = [
                    'err_code' => "invalid",
                    'title' => "Access Denied",
                    "message" => "You dont have access for this center",
                    "data" => []
                ];
                $this->response($arr);
                die;
            }
            $data = $this->centers_model->get_center_details($centers_id);
            if ($data) {
                if ($data->status == 1) {
                    $centers_id = (int) $data->id;
                    if ($centers_id != '') {
                        $this->session->set_userdata('centers_id', $centers_id);
                    }

                    $arr = [
                        'err_code' => "valid",
                        'title' => "",
                        "message" => "Updated successfully",
                        "data" => []
                    ];
                } else {
                    $arr = [
                        'err_code' => "invalid",
                        'title' => "",
                        "message" => "Center is inactive",
                        "data" => []
                    ];
                }
            } else {
                $arr = [
                    'err_code' => "invalid",
                    'title' => "",
                    "message" => "Center not found",
                    "data" => []
                ];
            }
        } else {
            $this->session->unset_userdata('centers_id');
            $data = [];
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => "Please select center",
                "data" => $data
            ];
        }
        $this->response($arr);
    }

}
