<?php

class Notifications extends REST_Controller {

    private $model_name = "notifications_model";
    private $title = "Notifications";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
        $this->load->model('notifications_model');
    }

    function index_get() {
        $data = $this->{$this->model_name}->get();
        foreach ($data as $item) {
            $item->time = date('d-M-Y H:i:s', $item->created_at);
        }
        if ($data) {
            $arr = [
                'err_code' => "valid",
                "message" => "Notifications",
                "data" => $data,
                "count" => count($data)
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                "message" => "Notifications not found",
            ];
        }
        $this->response($arr);
    }

    function count_get() {
        $data = count($this->{$this->model_name}->get());
        if ($data) {
            $arr = [
                'err_code' => "valid",
                "message" => "Notifications",
                "data" => $data
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                "message" => "Notifications not found",
            ];
        }
        $this->response($arr);
    }

}
