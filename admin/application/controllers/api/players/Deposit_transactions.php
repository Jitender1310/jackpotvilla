<?php

class Deposit_transactions extends REST_Controller {

    private $model_name = "deposit_money_transactions_model";
    private $title = "Deposit Transaction";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
        check_for_access(20);
    }

    function index_post() {
        $filters = [];
        $filters["search_key"] = $this->input->get_post("search_key");
        $filters["sort_by"] = $this->input->get_post("sort_by");
        $filters["from_date"] = $this->post("from_date") ? convert_date_to_db_format($this->post("from_date")) : "";
        $filters["to_date"] = $this->post("to_date") ? convert_date_to_db_format($this->post("to_date")) : "";
        $total_results =  $this->{$this->model_name}->get_deposit_transactions_list($filters);
        
        $limit = $this->input->get_post("limit") ? $this->input->get_post("limit") : ORDERS_PER_PAGE;
        $_GET['page'] = $this->input->get_post("page");
        $start = $this->input->get_post("page") > 1 ? ($this->input->get_post("page") - 1) * $limit : 0;
        $filters["limit"] = $limit;
        $filters["start"] = $start;
		
	$result["total_results_found"] = $total_results;
        $result["total_pages"] = ceil($total_results/ORDERS_PER_PAGE);
        
        $result["results"] = $this->{$this->model_name}->get_deposit_transactions_list($filters);
        
        
        $pagination = my_pagination("players/deposit_transactions", $limit, $total_results, true);
        $pagination['pagination'] = str_replace("&amp;", "&", $pagination['pagination']);
        $cur_page = ($pagination['pagination_helper']->cur_page != 0) ? ($pagination['pagination_helper']->cur_page - 1) : $pagination['pagination_helper']->cur_page;
        $pagination['initial_id'] = (($cur_page) * $pagination['pagination_helper']->per_page) + 1;
        
        $arr = [
            'status' => "valid",
            "message" => $this->title . " list",
            "data" => $result,
            "pagination" => $pagination
        ];
        $this->response($arr);
    }
}
