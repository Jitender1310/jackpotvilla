<?php

    class Real_players extends REST_Controller {

        private $model_name = "players_model";
        private $title = "Players";

        function __construct() {
            parent::__construct();
            $this->load->model($this->model_name);
            check_for_access(6);
        }

        function index_post() {
            $filters = [];

            $filters["search_key"] = $this->input->get_post("search_key");
            $filters["sort_by"] = $this->input->get_post("sort_by");
            $filters["from_date"] = $this->post("from_date") ? convert_date_to_db_format($this->post("from_date")) : "";
            $filters["to_date"] = $this->post("to_date") ? convert_date_to_db_format($this->post("to_date")) : "";
            $filters["kyc_status"] = $this->post("kyc_status") ? $this->post("kyc_status") : "";
            $total_results = $this->{$this->model_name}->get($filters);

            $limit = $this->input->get_post("limit") ? $this->input->get_post("limit") : ORDERS_PER_PAGE;
            $_GET['page'] = $this->input->get_post("page");
            $start = $this->input->get_post("page") > 1 ? ($this->input->get_post("page") - 1) * $limit : 0;
            $filters["limit"] = $limit;
            $filters["start"] = $start;

            $result["total_results_found"] = $total_results;
            $result["total_pages"] = ceil($total_results / ORDERS_PER_PAGE);

            $result["results"] = $this->{$this->model_name}->get($filters);


            $pagination = my_pagination("players/all_players", $limit, $total_results, true);
            $pagination['pagination'] = str_replace("&amp;", "&", $pagination['pagination']);
            $cur_page = ($pagination['pagination_helper']->cur_page != 0) ? ($pagination['pagination_helper']->cur_page - 1) : $pagination['pagination_helper']->cur_page;
            $pagination['initial_id'] = (($cur_page) * $pagination['pagination_helper']->per_page) + 1;


            $arr = [
                'status' => "valid",
                "message" => $this->title . " list",
                "data" => $result,
                "pagination" => $pagination
            ];
            $arr = json_encode($arr);
            $response_arry = json_decode(str_replace('null', '""', $arr));
            if (json_last_error_msg() == "Syntax error") {
                $response_arry = json_decode(str_replace('"null"', '""', $arr));
            }
            $this->response($response_arry);
        }

        function add_post() {
            if ($this->input->get_post("id")) {
                check_for_access(6, "update");
            } else {
                check_for_access(6, "add");
            }
            $this->form_validation->set_rules("firstname", "Firstname", "required", array(
                'required' => 'Firstname cannot be empty'
            ));

            $this->form_validation->set_rules("lastname", "Lastname", "required", array(
                'required' => 'Lastname cannot be empty'
            ));

            $this->form_validation->set_rules("email", "Email", "required|valid_email|callback_email_check", array(
                'required' => 'Email cannot be empty'
            ));

            $this->form_validation->set_rules("mobile", "Mobile", "required|callback_mobile_check", array(
                'required' => 'Mobile cannot be empty'
            ));

            $this->form_validation->set_rules("username", "Username", "required|callback_username_check", array(
                'required' => 'Username cannot be empty'
            ));

            $this->form_validation->set_rules("gender", "Gender", "required", array(
                'required' => 'Please choose your gender'
            ));

            $this->form_validation->set_rules("date_of_birth", "Date of Birth", "required", array(
                'required' => 'Date of Birth cannot be empty'
            ));

            $this->form_validation->set_rules("address_line_1", "Address", "required", array(
                'required' => 'Please enter your address'
            ));

            $this->form_validation->set_rules("pin_code", "Pin code", "required", array(
                'required' => 'Pin code cann\'t be empty'
            ));

            $this->form_validation->set_rules("states_id", "State", "required", array(
                'required' => 'Please choose your state'
            ));

            $this->form_validation->set_rules("city", "City", "required", array(
                'required' => 'Please enter your city name'
            ));

            if ($this->form_validation->run() == FALSE) {
                $errors = [
                    "firstname" => form_error("firstname"),
                    "lastname" => form_error("lastname"),
                    "username" => form_error("username"),
                    "email" => form_error("email"),
                    "mobile" => form_error("mobile"),
                    "gender" => form_error("gender"),
                    "date_of_birth" => form_error("date_of_birth"),
                    "address_line_1" => form_error("address_line_1"),
                    "pin_code" => form_error("pin_code"),
                    "city" => form_error("city"),
                    "states_id" => form_error("states_id")
                ];
                $arr = [
                    "status" => "invalid_form",
                    "data" => $errors
                ];
                $this->response($arr);
            }

            $data = [
                "firstname" => $this->post('firstname'),
                "lastname" => $this->post('lastname'),
                "username" => $this->post('username'),
                "mobile" => $this->post('mobile'),
                "email" => $this->post('email'),
                "gender" => $this->post('gender'),
                "address_line_1" => $this->post('address_line_1'),
                "address_line_2" => $this->post('address_line_2'),
                "date_of_birth" => convert_date_to_db_format($this->post('date_of_birth')),
                "states_id" => $this->post('states_id'),
                "city" => $this->post('city'),
                "pin_code" => $this->post('pin_code'),
            ];


            if ($this->input->post("id")) {
                $data["username"] = $this->input->post('username');
                if ($this->input->post('password')) {
                    $data["salt"] = generateRandomString(25);
                    $data["password"] = md5($this->input->post('password') . $data["salt"]);
                }
            } else {
                $data["username"] = $this->input->post('username');
                $data["salt"] = generateRandomString(25);
                $data["password"] = md5($this->input->post('password') . $data["salt"]);
            }

            if ($this->input->post("id")) {
                return $this->update($data, $this->post("id"));
            }

            $response = $this->{$this->model_name}->add($data);
            if ($response) {
                $arr = [
                    'status' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " added successfully"
                ];
            } else {
                $arr = [
                    'status' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not added, please try again"
                ];
            }
            $this->response($arr);
        }

        function update($data) {
            check_for_access(6);
            $response = $this->{$this->model_name}->update($data, $this->post('id'));
            if ($response) {
                $arr = [
                    'status' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " updated successfully"
                ];
            } else {
                $arr = [
                    'status' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not updated, please try again"
                ];
            }
            $this->response($arr);
        }

        function delete_post() {
            check_for_access(6);
            $id = (int) $this->post("id");
            $response = $this->{$this->model_name}->delete($id);
            if ($response) {
                $arr = [
                    'status' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " deleted successfully"
                ];
            } else {
                $arr = [
                    'status' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not deleted, please try again"
                ];
            }
            $this->response($arr);
        }

        function username_check($str) {
            $id = (int) $this->post('id');
            if ($this->{$this->model_name}->is_username_exists_for_adding($str, $id)) {
                $this->form_validation->set_message('username_check', 'The {field} is already existed');
                return FALSE;
            } else {
                return TRUE;
            }
        }

        function mobile_check($str) {
            $id = (int) $this->post('id');
            if ($this->{$this->model_name}->is_mobile_exists_for_adding($str, $id)) {
                $this->form_validation->set_message('mobile_check', 'The {field} is already existed');
                return FALSE;
            } else {
                return TRUE;
            }
        }

        function email_check($str) {
            $id = (int) $this->post('id');
            if ($this->{$this->model_name}->is_email_exists_for_adding($str, $id)) {
                $this->form_validation->set_message('email_check', 'The {field} is already existed');
                return FALSE;
            } else {
                return TRUE;
            }
        }

        function update_kyc_post() {

            $player_id = (int) $this->post('id');
            $this->form_validation->set_rules("address_proof_type", "Address Proof Type", "required", array(
                'required' => 'Please choose address proof type'
            ));
            $this->form_validation->set_rules("pan_card_number", "PAN Card Number", "required", array(
                'required' => 'PAN Card Number cannot be empty'
            ));
            $this->form_validation->set_rules("address_proof_status", "Address Proof Status", "required", array(
                'required' => 'Please choose address proof status'
            ));
            $this->form_validation->set_rules("pan_card_status", "Pan card Status", "required", array(
                'required' => 'Please choose pan card status'
            ));
            if ($this->form_validation->run() == FALSE) {
                $errors = [
                    "address_proof_type" => form_error("address_proof_type"),
                    "pan_card_number" => form_error("pan_card_number")
                ];
                $arr = [
                    "status" => "invalid_form",
                    "data" => $errors
                ];
                $this->response($arr);
            } else {

                if (@$_FILES["address_proof"]["name"]) {
                    $config['upload_path'] = KYC_ATTACHMENTS_UPLOAD_FOLDER;
                    $config['allowed_types'] = 'jpg|jpeg|pdf';
                    $config['max_size'] = 2000;
                    $config['overwrite'] = FALSE;
                    //$config['encrypt_name'] = TRUE;
                    $config['remove_spaces'] = TRUE;

                    $extension = pathinfo(@$_FILES['address_proof']['name'], PATHINFO_EXTENSION);
                    $file_name = "address_proof" . time() . generateRandomString(30) . '.' . $extension;
                    $config["file_name"] = $file_name;

                    $this->load->library('upload', $config);

                    if (!$this->upload->do_upload('address_proof')) {
                        if ($this->upload->display_errors() == "The file you are attempting to upload is larger than the permitted size.") {
                            $this->data['error'] = array('error' => "The file you are attempting to upload is larger than the permitted size 2MB");
                        } else {
                            $this->data['error'] = array('error' => $this->upload->display_errors());
                        }

                        $errors = [
                            "address_proof_type" => form_error("address_proof_type"),
                            "pan_card_number" => form_error("pan_card_number"),
                            "address_proof" => strip_tags($this->data['error']["error"]),
                        ];
                        $arr = [
                            "status" => "invalid_form",
                            "data" => $errors
                        ];
                        $this->response($arr);
                        die;
                    } else {
                        $uploaded_data = array('upload_data' => $this->upload->data());
                        $file_name = $uploaded_data["upload_data"]["file_name"];
                        $data["address_proof"] = $file_name;
                    }
                }

                if (@$_FILES["pan_card"]["name"]) {
                    $config['upload_path'] = KYC_ATTACHMENTS_UPLOAD_FOLDER;
                    $config['allowed_types'] = 'jpg|jpeg|pdf';
                    $config['max_size'] = 2000;
                    $config['overwrite'] = FALSE;
                    //$config['encrypt_name'] = TRUE;
                    $config['remove_spaces'] = TRUE;

                    $extension = pathinfo(@$_FILES['pan_card']['name'], PATHINFO_EXTENSION);
                    $file_name = "pan_card" . time() . generateRandomString(30) . '.' . $extension;
                    $config["file_name"] = $file_name;

                    $this->load->library('upload', $config);

                    if (!$this->upload->do_upload('pan_card')) {
                        if ($this->upload->display_errors() == "The file you are attempting to upload is larger than the permitted size.") {
                            $this->data['error'] = array('error' => "The file you are attempting to upload is larger than the permitted size 2MB");
                        } else {
                            $this->data['error'] = array('error' => $this->upload->display_errors());
                        }

                        $errors = [
                            "address_proof_type" => form_error("address_proof_type"),
                            "pan_card_number" => form_error("pan_card_number"),
                            "address_proof" => "",
                            "pan_card" => strip_tags($this->data['error']["error"]),
                        ];
                        $arr = [
                            "status" => "invalid_form",
                            "data" => $errors
                        ];
                        $this->response($arr);
                        die;
                    } else {
                        $uploaded_data = array('upload_data' => $this->upload->data());
                        $file_name = $uploaded_data["upload_data"]["file_name"];
                        $data["pan_card"] = $file_name;
                    }
                }

                $data["address_proof_type"] = $this->post("address_proof_type");
                $data["pan_card_number"] = $this->post("pan_card_number");
                $data["address_proof_status"] = $this->post("address_proof_status");
                $data["pan_card_status"] = $this->post("pan_card_status");
                $data["address_proof_rejected_reason"] = $this->post("address_proof_rejected_reason");
                $data["pan_card_rejected_reason"] = $this->post("pan_card_rejected_reason");

                $response = $this->player_kyc_model->update_kyc($data, $player_id);
                if ($response) {
                    $arr = [
                        "status" => "valid",
                        "title" => "KYC Details submitted",
                        "message" => "Kyc details has been submitted",
                        "data" => ""
                    ];
                    $this->response($arr);
                } else {
                    $arr = [
                        "status" => "invalid",
                        "title" => "Error",
                        "message" => "Some error occured",
                        "data" => ""
                    ];
                    $this->response($arr);
                }
            }
        }

    }
    