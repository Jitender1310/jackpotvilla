<?php

class Default_avatars extends REST_Controller {

    private $model_name = "default_avatars_model";
    private $title = "Avatars";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
        check_for_access(19);
    }

    function index_get() {
        $data = $this->{$this->model_name}->get();
        $arr = [
            'status' => "valid",
            "message" => $this->title . " list",
            "data" => $data
        ];
        $this->response($arr);
    }

    function add_post() {
        if ($this->input->get_post("id")) {
            check_for_access(19, "update");
        } else {
            check_for_access(19, "add");
        }
        $this->form_validation->set_rules("title", "title", "required", array(
            'required' => 'Title cannot be empty'
        ));

        $this->form_validation->set_rules("gender", "Gender", "required", array(
            'required' => 'Gender cannot be empty'
        ));
        
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "title" => form_error("title"),
                "gender" => form_error("gender")
            ];
            $arr = [
                "status" => "invalid_form",
                "data" => $errors
            ];
            $this->response($arr);
        }
        
        $data = [
            "title" => $this->post('title'),
            "gender" => $this->post('gender')
        ];
        
        
        if (@$_FILES["image"]["name"]) {
            $config['upload_path'] = DEFAULT_AVATARS_UPLOAD_FOLDER;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 1000;
            $config['overwrite'] = FALSE;
            $config['encrypt_name'] = TRUE;
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image')) {
                $this->data['error'] = array('error' => $this->upload->display_errors());
//                print_r($this->data['error']);
//                die;
                
                $errors = [
                    "title" => form_error("title"),
                    "gender" => form_error("gender"),
                    "image" => strip_tags($this->data['error']["error"]),
                ];
                $arr = [
                    "status" => "invalid_form",
                    "data" => $errors
                ];
                $this->response($arr);
                die;
            } else {
                $uploaded_data = array('upload_data' => $this->upload->data());
                $file_name = $uploaded_data["upload_data"]["file_name"];
                $data["image"] = $file_name;
            }
        }
		
	
        if ($this->input->post("id")) {
            return $this->update($data, $this->post("id"));
        }

        $response = $this->{$this->model_name}->add($data);
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " added successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not added, please try again"
            ];
        }
        $this->response($arr);
    }

    function update($data) {
        check_for_access(19);
        $response = $this->{$this->model_name}->update($data, $this->post('id'));
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " updated successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not updated, please try again"
            ];
        }
        $this->response($arr);
    }

    function delete_post() {
        check_for_access(19);
        $id = (int) $this->post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        $this->response($arr);
    }

    
}
