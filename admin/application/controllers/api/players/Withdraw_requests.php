<?php

    class Withdraw_requests extends REST_Controller {

        private $model_name = "withdraw_requests_model";
        private $title = "Withdraw Requests";

        function __construct() {
            parent::__construct();
            $this->load->model($this->model_name);
            check_for_access(20);
        }

        function index_post() {
            $filters = [];
            $filters["search_key"] = $this->input->get_post("search_key");
            $filters["sort_by"] = $this->input->get_post("sort_by");
            $filters["from_date"] = $this->post("from_date") ? convert_date_to_db_format($this->post("from_date")) : "";
            $filters["to_date"] = $this->post("to_date") ? convert_date_to_db_format($this->post("to_date")) : "";
            $total_results = $this->{$this->model_name}->get_withdraw_requests_list($filters);

            $limit = $this->input->get_post("limit") ? $this->input->get_post("limit") : ORDERS_PER_PAGE;
            $_GET['page'] = $this->input->get_post("page");
            $start = $this->input->get_post("page") > 1 ? ($this->input->get_post("page") - 1) * $limit : 0;
            $filters["limit"] = $limit;
            $filters["start"] = $start;

            $result["total_results_found"] = $total_results;
            $result["total_pages"] = ceil($total_results / ORDERS_PER_PAGE);

            $result["results"] = $this->{$this->model_name}->get_withdraw_requests_list($filters);


            $pagination = my_pagination("players/withdraw_requests", $limit, $total_results, true);
            $pagination['pagination'] = str_replace("&amp;", "&", $pagination['pagination']);
            $cur_page = ($pagination['pagination_helper']->cur_page != 0) ? ($pagination['pagination_helper']->cur_page - 1) : $pagination['pagination_helper']->cur_page;
            $pagination['initial_id'] = (($cur_page) * $pagination['pagination_helper']->per_page) + 1;

            $arr = [
                'status' => "valid",
                "message" => $this->title . " list",
                "data" => $result,
                "pagination" => $pagination
            ];
            $this->response($arr);
        }

        function add_post() {
            $this->form_validation->set_rules("request_status", "request_status", "required", array(
                'required' => 'Request status required'
            ));
            $this->form_validation->set_rules("request_amount", "Request Amount", "required", array(
                'required' => 'Request Amount required'
            ));

            if ($this->form_validation->run() == FALSE) {
                $errors = [
                    "title" => form_error("title")
                ];
                $arr = [
                    "status" => "invalid_form",
                    "data" => $errors
                ];
                $this->response($arr);
            }

            $data = [
                "request_amount" => $this->post('request_amount'),
                "request_status" => $this->post('request_status'),
                "remark" => $this->post('remark'),
                "request_processed_users_id " => get_user_id(),
                "response_date_time " => date('Y-m-d H:i:s', time()),
            ];
            if ($this->input->post("id")) {
                $details = $this->db->get_where('withdraw_request', array('id' => $this->input->post("id")))->row();
                if ($details->request_status != 'Completed' && $this->input->post("request_status") == 'Completed') {
                    $player_details = $this->players_model->get_player_by_id($details->players_id);
                    $message = "You withdraw request accepted and amount " . $this->input->post("request_amount") . "Rs/-  transferred successfully Please check your wallet";
                    send_message($message, $player_details->mobile);
                }
                return $this->update($data, $this->post("id"));
            }
            $response = $this->{$this->model_name}->add($data);
            if ($response) {
                $arr = [
                    'status' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " added successfully"
                ];
            } else {
                $arr = [
                    'status' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not added, please try again"
                ];
            }
            $this->response($arr);
        }

        function update($data) {
            $response = $this->{$this->model_name}->update($data, $this->post('id'));
            if ($response) {
                $arr = [
                    'status' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " updated successfully"
                ];
            } else {
                $arr = [
                    'status' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not updated, please try again"
                ];
            }
            $this->response($arr);
        }

    }
    