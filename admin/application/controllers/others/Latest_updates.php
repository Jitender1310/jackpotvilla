<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Latest_updates extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_required();
        if (!permission_required(122, "view")) {
            redirect("login");
        }
    }

    function index() {
        $this->admin_view("others/latest_updates");
    }

}
