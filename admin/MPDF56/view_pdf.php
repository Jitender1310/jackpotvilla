<?php

$user_agent = 'Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';
$options = array(
    CURLOPT_CUSTOMREQUEST => "GET", //set request type post or get
    CURLOPT_POST => false, //set to GET
    CURLOPT_USERAGENT => $user_agent, //set user agent
    CURLOPT_COOKIEFILE => "cookie.txt", //set cookie file
    CURLOPT_COOKIEJAR => "cookie.txt", //set cookie jar
    CURLOPT_RETURNTRANSFER => true, // return web page
    CURLOPT_HEADER => false, // don't return headers
    CURLOPT_FOLLOWLOCATION => true, // follow redirects
    CURLOPT_ENCODING => "", // handle all encodings
    CURLOPT_AUTOREFERER => true, // set referer on redirect
    CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
    CURLOPT_TIMEOUT => 120, // timeout on response
    CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
);



$url = $_GET['url'];
$ch = curl_init($url);
curl_setopt_array($ch, $options);
$html = curl_exec($ch);
$html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
//$html = file_get_contents($_GET['url']);
//$id = $_GET['order_id'];
//==============================================================
//==============================================================
//==============================================================

include("mpdf.php");
$mpdf = new mPDF('utf-8', 'A4');
$mpdf->debug = true;
$mpdf->WriteHTML($html);
//$mpdf->WriteFixedPosHTML($img, 52, 17, 100, 100);
echo $mpdf->Output();
