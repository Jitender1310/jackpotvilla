$other_modules = ['naif.base64', 'ngTagsInput', 'datatables', 'webcam'];
$available_modules = ['servicesModule', 'ngSanitize', 'ngCookies'];
//This loop is for if new
for (var m = 0; m < $other_modules.length; m++) {
    try {
        if (angular.module($other_modules[m])) {
            $available_modules.push($other_modules[m]);
        }
    } catch (err) {
    }
}
var app = angular.module("frontDesk", $available_modules);
var webServices = angular.module("servicesModule", []);
webServices.service('loaderService', function () {
    return {
        getLoading: function () {},
        closeLoading: function () {}
    };
});

function testInterceptor() {
    return {
        request: function (config) {
            //console.log(config);
            //alert("Request Started");
            return config;
        },
        requestError: function (config) {
            alert("There might be a problem with your network");
            return config;
        },
        response: function (res) {
            return res;
        },
        responseError: function (res) {
            if (angular.isDefined(res.data)) {
                if (res.data.title !== null) {
                    alert(res.data.title);
                }
            } else {
                alert("There might be a problem with your network");
            }
            return res;
        }
    };
}
app.factory('testInterceptor', testInterceptor);
app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('testInterceptor');
});
app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    }]);
var config = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
    }
};
var fileConfig = {
    transformRequest: angular.identity,
    headers: {
        //'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;',
        'Content-Type': undefined,
        'X-Requested-With': 'XMLHttpRequest',
        'Process-Data': false
    }
};
//$version = "/v2";
var u = location.port !== "" ? ':' + location.port : "";
//u += $version;
if (document.domain === "demoworks.in") {
    var baseurl = location.protocol + "//" + document.domain + u + '/php/freshup_front_desk/';
    var apiBaseUrl = location.protocol + "//" + document.domain + u + '/php/freshup_front_desk/api/';
} else if (document.domain === "ready2host.in" || document.domain === "www.ready2host.in") {
    var baseurl = location.protocol + "//" + document.domain + u + '/freshup_front_desk/';
    var apiBaseUrl = location.protocol + "//" + document.domain + u + '/freshup_front_desk/api/';
} else if (document.domain === "localhost") {
    var baseurl = location.protocol + "//" + document.domain + u + '/admin/';
    var apiBaseUrl = location.protocol + "//" + document.domain + u + '/admin/api/';
} else {
    var baseurl = location.protocol + "//" + document.domain + u + '/admin/';
    var apiBaseUrl = location.protocol + "//" + document.domain + u + '/admin/api/';
}
app.filter('newlines', [function () {
        return function (text) {
            return text.replace(/\n/g, '<br/>');
        };
    }]);
console.log("%c WARNING!!!", "color:#FF8F1C; font-size:40px;");
console.log("%c This browser feature is for developers only. Please do not copy-paste any code or run any scripts here. It may cause your account to be compromised.", "color:#003087; font-size:12px;");
console.log("%c For more information, http://en.wikipedia.org/wiki/Self-XSS", "color:#003087; font-size:12px;");
//Directives
app.directive("footerCopyRight", function () {
    var footerCopyRightYear = new Date().getFullYear();
    return {
        restrict: "E",
        template: '<div class="pageFooter" workspace-offset>&copy; CLover Rummy 2019 - ' + footerCopyRightYear + '</div>'
    };
});
var datetimepickericons = {
    time: 'fal fa-time',
    date: 'fal fa-calendar',
    up: 'fal fa-chevron-up',
    down: 'fal fa-chevron-down',
    previous: 'fal fa-chevron-left',
    next: 'fal fa-chevron-right',
    today: 'fal fa-screenshot',
    clear: 'fal fa-trash',
    close: 'fal fa-remove'
}
var dateFormate = 'DD-MM-YYYY';
var timeFormat = 'hh:mm A';
app.directive('datepicker', function ($parse) {
    return {
        require: '?ngModel',
        restrict: 'C',
        link: function (scope, element, attrs, controller) {
            element.datetimepicker({
                icons: datetimepickericons,
                format: dateFormate,
                widgetPositioning: {
                    horizontal: 'right',
                    vertical: 'auto'
                },
                minDate: moment().startOf('d'),
            }).on("dp.change", function (e) {
                scope.date = element.val();
                $parse(attrs.ngModel).assign(scope, scope.date);
                scope.$apply();
            });
        }
    };
});
app.directive('dwpdates', function ($parse) {
    return {
        require: '?ngModel',
        restrict: 'C',
        link: function (scope, element, attrs, controller) {
            if (element.val() == "") {
                var sdate = new Date();
                scope.date = moment().format("DD-MM-YYYY");
                setTimeout(function () {
                    $parse(attrs.ngModel).assign(scope, scope.date);
                    scope.$apply();
                }, 2000);
            }
            element.datetimepicker({
                icons: datetimepickericons,
                format: dateFormate,
                widgetPositioning: {
                    horizontal: 'right',
                    vertical: 'auto'
                },
                maxDate: moment().startOf('d'),
            }).on("dp.change", function (e) {
                scope.date = element.val();
                $parse(attrs.ngModel).assign(scope, scope.date);
                scope.$apply();
            });
        }
    };
});
app.directive('dwpdateswithtime', function ($parse) {
    return {
        require: '?ngModel',
        restrict: 'C',
        link: function (scope, element, attrs, controller) {
            element.datetimepicker({
                icons: datetimepickericons,
                format: dateFormate + " " + timeFormat,
                sideBySide: true,
                widgetPositioning: {
                    horizontal: 'right',
                    vertical: 'auto'
                },
                maxDate: moment().startOf('d'),
            }).on("dp.change", function (e) {
                scope.date = element.val();
                $parse(attrs.ngModel).assign(scope, scope.date);
                scope.$apply();
            });
        }
    };
});
app.directive('dwfdateswithtime', function ($parse) {
    return {
        require: '?ngModel',
        restrict: 'C',
        link: function (scope, element, attrs, controller) {
            element.datetimepicker({
                icons: datetimepickericons,
                format: dateFormate + " " + timeFormat,
                sideBySide: true,
                widgetPositioning: {
                    horizontal: 'right',
                    vertical: 'auto'
                },
                minDate: moment().startOf('d'),
            }).on("dp.change", function (e) {
                scope.date = element.val();
                $parse(attrs.ngModel).assign(scope, scope.date);
                scope.$apply();
            });
        }
    };
});
app.directive('datetimepickerNormal', function ($parse) {
    return {
        require: '?ngModel',
        restrict: 'C',
        link: function (scope, element, attrs, controller) {
            element.datetimepicker({
                icons: datetimepickericons,
                format: dateFormate + " " + timeFormat,
                sideBySide: true,
                maxDate: new Date(),
                widgetPositioning: {
                    horizontal: 'right',
                    vertical: 'auto'
                }
            }).on("dp.change", function (e) {
                scope.date = element.val();
                $parse(attrs.ngModel).assign(scope, scope.date);
                scope.$apply();
            });
        }
    };
});
app.directive('datetimepickerAll', function ($parse) {
    return {
        require: '?ngModel',
        restrict: 'C',
        link: function (scope, element, attrs, controller) {
            element.datetimepicker({
                icons: datetimepickericons,
                format: dateFormate + " " + timeFormat,
                sideBySide: true,
                widgetPositioning: {
                    horizontal: 'right',
                    vertical: 'auto'
                }
            }).on("dp.change", function (e) {
                scope.date = element.val();
                $parse(attrs.ngModel).assign(scope, scope.date);
                scope.$apply();
            });
        }
    };
});

app.directive('datepicker_with_previous_dates', function ($parse) {
    return {
        require: '?ngModel',
        restrict: 'C',
        link: function (scope, element, attrs, controller) {
            element.datetimepicker({
                icons: datetimepickericons,
                format: dateFormate,
                widgetPositioning: {
                    horizontal: 'right',
                    vertical: 'auto'
                },
                maxDate: moment().startOf('d'),
            }).on("dp.change", function (e) {
                scope.date = element.val();
                $parse(attrs.ngModel).assign(scope, scope.date);
                scope.$apply();
            });
        }
    };
});
app.directive('datepickerNormal', function ($parse) {
    return {
        require: '?ngModel',
        restrict: 'C',
        link: function (scope, element, attrs, controller) {
            element.datetimepicker({
                icons: datetimepickericons,
                format: dateFormate,
                widgetPositioning: {
                    horizontal: 'right',
                    vertical: 'auto'
                },
            }).on("dp.change dp.show", function (e) {
                scope.date = element.val();
                $parse(attrs.ngModel).assign(scope, scope.date);
                scope.$apply();
            }).on("dp.show", function (e) {
                if (!$(e.target).hasClass('edit')) {
                    element.data("DateTimePicker").date(new Date());
                }
            });
        }
    };
});
app.directive('datepickerFrom', function ($parse) {
    return {
        require: '?ngModel',
        restrict: 'C',
        link: function (scope, element, attrs, controller) {
            element.datetimepicker({
                icons: datetimepickericons,
                format: dateFormate,
                widgetPositioning: {
                    horizontal: 'right',
                    vertical: 'auto'
                },
                minDate: moment().startOf('d'),
            }).on("dp.change", function (e) {
                scope.date = element.val();
                $parse(attrs.ngModel).assign(scope, scope.date);
                scope.$emit('todate', scope.date);
                scope.$apply();
            });
        }
    };
});
app.directive('datepickerTo', function ($parse) {
    return {
        require: '?ngModel',
        restrict: 'C',
        link: function (scope, element, attrs, controller) {
            element.datetimepicker({
                icons: datetimepickericons,
                format: dateFormate,
                widgetPositioning: {
                    horizontal: 'right',
                    vertical: 'auto'
                },
            }).on("dp.change", function (e) {
                scope.date = element.val();
                $parse(attrs.ngModel).assign(scope, scope.date);
                scope.$apply();
            });
            scope.$on('todate', function (event, result) {
                element.data("DateTimePicker").minDate(moment(result, dateFormate));
                // element.data("DateTimePicker").useCurrent(false);
            });
        }
    };
});
app.directive('timepicker', function ($parse) {
    return {
        require: '?ngModel',
        restrict: 'C',
        link: function (scope, element, attrs, controller) {
            element.datetimepicker({
                icons: datetimepickericons,
                format: timeFormat,
                widgetPositioning: {
                    horizontal: 'right',
                    vertical: 'auto'
                },
            }).on("dp.change", function (e) {
                scope.date = element.val();
                $parse(attrs.ngModel).assign(scope, scope.date);
                scope.$apply();
            });
        }
    };
});
app.directive('matchHeightRow', function ($timeout) {
    return {
        restrict: 'A',
        controller: function ($scope, $element, $attrs, $transclude) {
            var max = 0;
            $element.css({
                "height": "0px",
                "overflow": "hidden"
            });
            $timeout(function () {
                $element.find("[match-height-col]").each(function () {
                    if ($(this).outerHeight() > max) {
                        max = $(this).outerHeight();
                    }
                });
                $element.find("[match-height-col]").css({
                    "min-height": max + 1
                });
                $element.css({
                    "height": "auto",
                    "overflow": "visible"
                });
                console.log(max);
            }, 5000);
        }
    };
});

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

$(function () {
    $('.number').on('keypress', function (evt) {
        //console.log(evt.which);
        if (evt.which == 0) {
            return true; //This is for firefox browser to work arrows action left right...
        }
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        } else {
            if (charCode == 46) {
                return false;
            } else {
                return true;
            }
        }
    });
    $('.number_with_decimals').on('keypress', function (evt) {
        //console.log(evt.which);
        if (evt.which == 0) {
            return true; //This is for firefox browser to work arrows action left right...
        }
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode == 46) {
            return true;
        }
        if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        } else {
            if (charCode == 46) {
                return false;
            } else {
                return true;
            }
        }
    });
});
$(document).ready(function () {
    $(document).on('keydown', '.readonly', function (e) {
        e.preventDefault();
    });
    $(".pageFooter").html("&copy; CLover Rummy Desk - 2018");
})
app.directive('welcome', function ($window) {
    return {
        // name: '',
        // priority: 1,
        // terminal: true,
        // scope: {}, // {} = isolate, true = child, false/undefined = no change
        // controller: function($scope, $element, $attrs, $transclude) {},
        // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
        restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
        // template: '',
        // templateUrl: '',
        // replace: true,
        // transclude: true,
        // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
        link: function ($scope, iElm, iAttrs, controller) {
            function ren() {
                var wh = (($window.innerHeight - 50) - iElm[0].clientHeight) / 2;
                iElm[0].style.marginTop = wh + 'px';
                iElm[0].style.position = 'relative';
                iElm[0].style.top = '-25px';
            }
            ren();
            $window.onresize = ren;
            // console.log($window.innerHeight);   
        }
    };
});