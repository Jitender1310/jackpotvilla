webServices.service('locationsService', function ($http) {
    return {
        save: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "mobile_app/locations/add";
            return $http.post(url, postData, config);
        },
        get: function () {
            var url = apiBaseUrl + "mobile_app/locations";
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "mobile_app/locations/delete";
            return $http.post(url, postData, config);
        }
    };
});