webServices.service('accommodationService', function ($http) {
    return {
        save: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "mobile_app/accommodations/add";
            return $http.post(url, postData, config);
        },
        get: function () {
            var url = apiBaseUrl + "mobile_app/accommodations";
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "mobile_app/accommodations/delete";
            return $http.post(url, postData, config);
        }
    };
});