webServices.service('galleryService', function ($http) {
    return {
        save: function (obj) {
            //var postData = $.param(obj);
            var url = apiBaseUrl + "mobile_app/gallery/add";
            return $http.post(url, obj, fileConfig);
        },
        get: function (obj) {

            var postData = $.param(obj);
            var url = apiBaseUrl + "mobile_app/gallery?";
            if (obj.page) {
                url += "page=" + obj.page;
            }
            return $http.post(url, postData, config);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "mobile_app/gallery/delete";
            return $http.post(url, postData, config);
        }
    };
});