webServices.service('facilitiesService', function ($http) {
    return {
        save: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "mobile_app/facilities/add";
            return $http.post(url, postData, config);
        },
        get: function () {
            var url = apiBaseUrl + "mobile_app/facilities";
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "mobile_app/facilities/delete";
            return $http.post(url, postData, config);
        }
    };
});