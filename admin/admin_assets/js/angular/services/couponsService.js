webServices.service('couponsService', function ($http) {
    return {
        save: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "master/coupons/add";
            return $http.post(url, postData, config);
        },
        get: function () {
            var url = apiBaseUrl + "master/coupons";
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "master/coupons/delete";
            return $http.post(url, postData, config);
        }
    };
});