webServices.service('latestUpdatesService', function ($http) {
    return {
        add: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "other/latest_updates/add";
            return $http.post(url, postData, config);
        },
        get: function () {
            var url = apiBaseUrl + "other/latest_updates";
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "other/latest_updates/delete";
            return $http.post(url, postData, config);
        }
    };
});