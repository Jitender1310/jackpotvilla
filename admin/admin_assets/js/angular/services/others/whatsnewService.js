webServices.service('whatsnewService', function ($http) {
    return {
        save: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "other/whats_new/add";
            return $http.post(url, postData, config);
        },
        get: function () {
            var url = apiBaseUrl + "other/whats_new";
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "other/whats_new/delete";
            return $http.post(url, postData, config);
        }
    };
});