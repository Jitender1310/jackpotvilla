webServices.service('contactEnquiriesService', function ($http) {
    return {
        add: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "other/contact_enquiries/add";
            return $http.post(url, postData, config);
        },
        get: function () {
            var url = apiBaseUrl + "other/contact_enquiries";
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "other/contact_enquiries/delete";
            return $http.post(url, postData, config);
        }
    };
});