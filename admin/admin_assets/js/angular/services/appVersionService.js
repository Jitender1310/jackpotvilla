webServices.service('appVersionService', function ($http) {
    return {
        save: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "master/app_version/add";
            return $http.post(url, postData, config);
        },
        get: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "master/app_version?";
            if (obj.page) {
                url += "page=" + obj.page;
            }
            return $http.post(url, postData, config);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "master/app_version/delete";
            return $http.post(url, postData, config);
        }
    };
});