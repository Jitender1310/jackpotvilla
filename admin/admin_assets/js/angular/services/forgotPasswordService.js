webServices.service('forgotPasswordService', function ($http) {
    return {
        forgotPassword: function (userObj) {
            var postData = $.param(userObj);
            var url = apiBaseUrl + "forgot_password";
            return $http.post(url, postData, config);
        }
    };
});