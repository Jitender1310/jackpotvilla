webServices.service('smsTemplatesService', function ($http) {
    return {
        save: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "crm/sms_templates/add";
            return $http.post(url, postData, config);
        },
        get: function () {
            var url = apiBaseUrl + "crm/sms_templates";
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "crm/sms_templates/delete";
            return $http.post(url, postData, config);
        },
        get_message: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "crm/sms_templates/get_message";
            return $http.post(url, postData, config);
        }

    };
});