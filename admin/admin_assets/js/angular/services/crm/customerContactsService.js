webServices.service('customerContactsService', function ($http) {
    return {
        get: function () {
            var url = apiBaseUrl + "crm/customer_contacts";
            return $http.get(url);
        },

    };
});