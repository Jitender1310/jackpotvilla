webServices.service('withdrawRequestsService', function ($http) {
    return {
        getListOfWithdrawRequests: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "players/withdraw_requests?";
            if (obj.page) {
                url += "page=" + obj.page;
            }
            return $http.post(url, postData, config);
        },
        save: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "players/withdraw_requests/add";
            return $http.post(url, postData, config);
        },
    };
});
