webServices.service('reportedProblemsService', function ($http) {
    return {
        save: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "feedback/reported_problems/add";
            return $http.post(url, postData, config);
        },
        get: function () {
            var url = apiBaseUrl + "feedback/reported_problems";
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "feedback/reported_problems/delete";
            return $http.post(url, postData, config);
        },
        saveLog:function(obj){
            var postData = $.param(obj);
            var url = apiBaseUrl + "feedback/reported_problems/update_status";
            return $http.post(url, postData, config);
        },        
        getLogs:function(report_id){
            var url = apiBaseUrl + "feedback/reported_problems/logs?id="+report_id;
            return $http.get(url);
        }
    };
});