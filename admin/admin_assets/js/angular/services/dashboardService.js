webServices.service('dashboardService', function ($http) {
    return {
        get: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "dashboard";
            return $http.post(url, postData, config);
        },
        get_games: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "dashboard/games";
            return $http.post(url, postData, config);
        }
    };
});