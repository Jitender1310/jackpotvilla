webServices.service('clubTypesService', function ($http) {
    return {
        save: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "master/club_types/add";
            return $http.post(url, postData, config);
        },
        get: function () {
            var url = apiBaseUrl + "master/club_types";
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "master/club_types/delete";
            return $http.post(url, postData, config);
        }
    };
});