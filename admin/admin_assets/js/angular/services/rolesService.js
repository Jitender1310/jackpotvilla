webServices.service('rolesService', function ($http) {
    return {
        save: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "master/roles/add";
            return $http.post(url, postData, config);
        },
        get: function () {
            var url = apiBaseUrl + "master/roles";
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "master/roles/delete";
            return $http.post(url, postData, config);
        },
        savePrivilegesToDB: function (obj, role_id) {
            var url = apiBaseUrl + "master/roles/save_privileges?roles_id=" + role_id;
            return $http.post(url, obj, config);
        },
        getPrivilegesFromDB: function (role_id) {
            var url = apiBaseUrl + "master/modules/tree_structure?roles_id=" + role_id;
            return $http.get(url);
        }
    };
});