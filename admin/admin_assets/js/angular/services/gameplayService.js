webServices.service('gameplayService', function ($http) {
    return {
        list: function( filter ){
            var url = apiBaseUrl + "gameplay_data";
            var getParams = $.param(filter);
            return $http.get(url + '?' + getParams, config);
        }
    };
});