webServices.service('usersService', function ($http) {
    return {
        save: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "master/users/add";
            return $http.post(url, postData, config);
        },
        get: function () {
            var url = apiBaseUrl + "master/users";
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "master/users/delete";
            return $http.post(url, postData, config);
        },
        getUsersByCentersId: function (centers_id) {
            var postData = "centers_id=" + centers_id;
            var url = apiBaseUrl + "master/users/users_by_center_id";
            return $http.post(url, postData, config);
        },

        getUsersByCorporateOfficesId: function (corporate_offices_id) {
            var postData = "corporate_offices_id=" + corporate_offices_id;
            var url = apiBaseUrl + "master/users/users_by_corporate_offices_id";
            return $http.post(url, postData, config);
        },
        savePrivilegesToDBForUser: function (obj, user_id) {
            var url = apiBaseUrl + "master/users/save_privileges?user_id=" + user_id;
            return $http.post(url, obj, config);
        },
        getPrivilegesFromDBForUser: function (user_id) {
            var url = apiBaseUrl + "master/modules/tree_structure_for_user?";
            url += "&user_id=" + user_id;
            return $http.get(url);
        }
    };
});