webServices.service('createRealTournamentService', function ($http) {
    return {
        save: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "master/create_real_tournament/add";
            return $http.post(url, postData, config);
        },
        get: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "master/create_real_tournament";
            return $http.post(url, postData, config);
        }
    };
});