webServices.service('offerNotificationsService', function ($http) {
    return {
        save: function (fd) {
            var url = apiBaseUrl + "promotions/offer_notifications/add";
            return $http.post(url, fd, fileConfig);
        },
        get: function () {
            var url = apiBaseUrl + "promotions/offer_notifications";
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "promotions/offer_notifications/delete";
            return $http.post(url, postData, config);
        }
    };
});