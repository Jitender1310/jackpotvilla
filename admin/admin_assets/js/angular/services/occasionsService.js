webServices.service('occasionsService', function ($http) {
    return {
        saveOccasions: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "master/occasions/add";
            return $http.post(url, postData, config);
        },
        getOccasions: function () {
            var url = apiBaseUrl + "master/occasions";
            return $http.get(url);
        },
        deleteOccasions: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "master/occasions/delete";
            return $http.post(url, postData, config);
        },
        getOccasionsByCentersId: function (centers_id) {
            var url = apiBaseUrl + "master/occasions/by_centers_id?";
            url += "centers_id=" + centers_id;
            return $http.get(url);
        }
    }
});