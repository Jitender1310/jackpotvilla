webServices.service('saleViewService', function ($http) {
    return {
        getListOfSales: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "pos/sales_list?";
            if (obj.page) {
                url += "page=" + obj.page;
            }
            return $http.post(url, postData, config);
        },

        get: function (reservation_id) {
            var url = apiBaseUrl + "reservations/reservation_details?";
            url += "reservations_id=" + reservation_id;
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "reservations/tax_slabs/delete";
            return $http.post(url, postData, config);
        }
    };
});
