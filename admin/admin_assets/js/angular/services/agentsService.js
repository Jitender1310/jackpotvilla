webServices.service('agentsService', function ($http) {
    return {
        save: function (fd) {
            var url = apiBaseUrl + "master/agents/add";
            return $http.post(url, fd, fileConfig);
        },
        get: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "master/agents?";
            if (obj.page) {
                url += "page=" + obj.page;
            }
            return $http.post(url, postData, config);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "master/agents/delete";
            return $http.post(url, postData, config);
        },
        getAgentsListWithSearchKey: function (search_key) {
            var url = apiBaseUrl + "master/agents/agents_with_search_key?";
            url += "search_key=" + search_key;
            return $http.get(url);
        }
    };
});