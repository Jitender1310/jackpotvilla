webServices.service('globalConfigurationsService', function ($http) {
    return {
        save: function (obj) {
            var postData = "constants_obj="+JSON.stringify(obj);
            var url = apiBaseUrl + "master/global_configuration_constants/update";
            return $http.post(url, postData, config);
        },
        get: function () {
            var url = apiBaseUrl + "master/global_configuration_constants";
            return $http.get(url);
        }
    };
});