webServices.service('priceConfigService', function ($http) {
    return {
        saveRegularPrices: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "master/regular_price_config/add";
            return $http.post(url, postData, config);
        },
        getRegularPrices: function (centers_id, staying_type_categories_id) {
            var url = apiBaseUrl + "master/regular_price_config?";
            url += "centers_id=" + centers_id;
            url += "&staying_type_categories_id=" + staying_type_categories_id;
            return $http.get(url);
        },
        getOccasionalPrices: function (centers_id, staying_type_categories_id, occasions_id) {
            var url = apiBaseUrl + "master/occasional_price_config?";
            url += "centers_id=" + centers_id;
            url += "&staying_type_categories_id=" + staying_type_categories_id;
            url += "&occasions_id=" + occasions_id;
            return $http.get(url);
        },
        saveOccasionalPrices: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "master/Occasional_price_config/add";
            return $http.post(url, postData, config);
        }
    };
});