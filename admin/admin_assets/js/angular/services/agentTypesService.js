webServices.service('agentTypesService', function ($http) {
    return {
        save: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "master/agent_types/add";
            return $http.post(url, postData, config);
        },

        get: function () {
            var url = apiBaseUrl + "master/agent_types";
            return $http.get(url);
        },
        getAgentTypes: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "master/agent_types/agent_types_by_user_id";
            return $http.post(url, postData, config);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "master/agent_types/delete";
            return $http.post(url, postData, config);
        }
    };
});