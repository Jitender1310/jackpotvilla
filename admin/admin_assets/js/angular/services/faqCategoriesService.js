webServices.service('faqCategoriesService', function ($http) {
    return {
        save: function (obj) {
            var url = apiBaseUrl + "faq_categories/add";
            return $http.post(url, obj, fileConfig);
        },
        get: function () {
            var url = apiBaseUrl + "faq_categories";
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "faq_categories/delete";
            return $http.post(url, postData, config);
        },
    };
});