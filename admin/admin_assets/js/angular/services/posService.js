webServices.service('posService', function ($http) {
    return {
        save: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "pos/add";
            return $http.post(url, postData, config);
        },
        getProductsList: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "pos/create_sale/products_by_center_id";
            return $http.post(url, postData, config);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "pos/delete";
            return $http.post(url, postData, config);
        },
        createSale: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "pos/create_sale";
            return $http.post(url, postData, config);
        },
        updatePaymentDetails: function (obj, users_id) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "pos/update_payment_information/add?";
            url += 'users_id=' + users_id;
            return $http.post(url, postData, config);
        },
        get: function (sales_id) {
            var url = apiBaseUrl + "pos/sale_details?";
            url += "sales_id=" + sales_id;
            return $http.get(url);
        },
        getPosPaymentinfo: function (booking_ref_number) {
            var url = apiBaseUrl + "pos/payment_info_for_reservation?";
            url += "reservations_id=" + booking_ref_number;
            return $http.get(url);
        },
        updatePosAllSalesPaymentDetails: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "pos/update_payment_information/all?";
            return $http.post(url, postData, config);
        }
    };
});