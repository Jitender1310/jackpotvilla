webServices.service('agentWalletService', function ($http) {
    return {
        getListOfWallet: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "master/agents/wallet?";
            if (obj.page) {
                url += "page=" + obj.page;
            }
            return $http.post(url, postData, config);
        },
        updateAgentPaymentDetails: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "master/agents/update_agent_payment_details";
            return $http.post(url, postData, config);
        },

        getWalletBalance: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "master/agents/getWalletBalance";
            return $http.post(url, postData, config);
        }

    };
});
