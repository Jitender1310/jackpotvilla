webServices.service('botPlayersService', function ($http) {
    return {
        save: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "players/bots/add";
            return $http.post(url, postData, config);
        },
        get: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "players/bots?";
            if (obj.page) {
                url += "page=" + obj.page;
            }
            return $http.post(url, postData, config);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "players/bots/delete";
            return $http.post(url, postData, config);
        }
    };
});