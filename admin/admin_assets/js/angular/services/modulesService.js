webServices.service('modulesService', function ($http) {
    return {
        save: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "master/modules/add";
            return $http.post(url, postData, config);
        },
        get: function () {
            var url = apiBaseUrl + "master/modules";
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "master/modules/delete";
            return $http.post(url, postData, config);
        }
    };
});