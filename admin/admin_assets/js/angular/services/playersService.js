webServices.service('playersService', function ($http) {
    return {
        save: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "players/real_players/add";
            return $http.post(url, postData, config);
        },
        get: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "players/real_players?";
            if (obj.page) {
                url += "page=" + obj.page;
            }
            return $http.post(url, postData, config);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "players/real_players/delete";
            return $http.post(url, postData, config);
        },
        update_kyc: function (fd) {
            //var postData = $.param(obj);
            var url = apiBaseUrl + "players/real_players/update_kyc";
            return $http.post(url, fd, fileConfig);
        }
    };
});