webServices.service('depositTransactionsService', function ($http) {
    return {
        getListOfDepositTransactions: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "players/deposit_transactions?";
            if (obj.page) {
                url += "page=" + obj.page;
            }
            return $http.post(url, postData, config);
        },
        save: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "reservations/tax_slabs/add";
            return $http.post(url, postData, config);
        },
        get: function (reservation_id) {
            var url = apiBaseUrl + "reservations/reservation_details?";
            url += "reservations_id=" + reservation_id;
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "reservations/tax_slabs/delete";
            return $http.post(url, postData, config);
        }
    };
});
