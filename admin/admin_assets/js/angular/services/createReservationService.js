webServices.service('createReservationService', function ($http) {
    return {
        addReservation: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "reservations/create_reservation";
            return $http.post(url, postData, config);
        },
        getStayAvailabilityByCentersId: function (obj) {
            var url = apiBaseUrl + "reservations/create_reservation/stay_availability_by_center?";
            url += "centers_id=" + obj.centers_id;
            url += "&availability_type=" + obj.availability_type;
            url += "&staying_types_id=" + obj.staying_types_id;
            url += "&check_in_date=" + obj.check_in_date;
            url += "&check_in_time=" + obj.check_in_time;
            url += "&duration=" + obj.duration;
            url += "&persons=" + obj.persons;
            url += "&is_past_booking=" + obj.is_past_booking;
            return $http.get(url);
        },
        getStayAvailabilityByCentersIdAndBookingId: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "reservations/update_arrival_time/stay_availability_by_center_and_reservation_id";
            return $http.post(url, postData, config);
        },
        addQuickReservation: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "reservations/quick_reservation_create";
            return $http.post(url, postData, config);
        },
        getPNRInfo: function (pnr_number) {
            var url = apiBaseUrl + "pnr_info/pnr_details_server2/" + pnr_number;
            return $http.get(url);
        }
    };
});