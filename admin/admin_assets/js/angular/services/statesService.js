webServices.service('statesService', function ($http) {
    return {
        save: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "master/states/add";
            return $http.post(url, postData, config);
        },
        get: function () {
            var url = apiBaseUrl + "master/states";
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "master/states/delete";
            return $http.post(url, postData, config);
        },
        getByCountryId: function (countries_id) {
            var url = apiBaseUrl + "master/states/by_countries_id?";
            url += "countries_id=" + countries_id;
            return $http.get(url);
        }
    };
});