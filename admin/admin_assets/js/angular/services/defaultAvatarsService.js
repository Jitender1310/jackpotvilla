webServices.service('defaultAvatarsService', function ($http) {
    return {
        save: function (fd) {
            var url = apiBaseUrl + "players/default_avatars/add";
            return $http.post(url, fd, fileConfig);
        },
        get: function () {
            var url = apiBaseUrl + "players/default_avatars";
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "players/default_avatars/delete";
            return $http.post(url, postData, config);
        }
    };
});