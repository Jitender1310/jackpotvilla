webServices.service('changePasswordService', function ($http) {
    return {
        update: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "change_password/update";
            return $http.post(url, postData, config);
        }
    };
});