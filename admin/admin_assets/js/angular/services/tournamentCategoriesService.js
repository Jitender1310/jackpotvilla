webServices.service('tournamentCategoriesService', function ($http) {
    return {

        save: function (fd) {
            var url = apiBaseUrl + "master/tournament_categories/add";
            return $http.post(url, fd, fileConfig);
        },
        get: function () {
            var url = apiBaseUrl + "master/tournament_categories";
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "master/tournament_categories/delete";
            return $http.post(url, postData, config);
        }
    };
});