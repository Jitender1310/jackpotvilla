webServices.service('taxSlabsService', function ($http) {
    return {
        save: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "master/tax_slabs/add";
            return $http.post(url, postData, config);
        },
        get: function () {
            var url = apiBaseUrl + "master/tax_slabs";
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "master/tax_slabs/delete";
            return $http.post(url, postData, config);
        },
        getByCentersId: function (obj) {
            var url = apiBaseUrl + "master/tax_slabs/fetch_by_centers_id?";
            url += "centers_id=" + obj.centers_id;
            url += "&check_in_date=" + obj.check_in_date;
            url += "&check_in_time=" + obj.check_in_time;
            url += "&duration=" + obj.duration;
            return $http.get(url);
        }
    };
});