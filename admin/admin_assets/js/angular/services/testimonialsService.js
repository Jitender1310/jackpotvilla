webServices.service('testimonialsService', function ($http) {
    return {
        save: function (fd) {
            var url = apiBaseUrl + "feedback/testimonials/add";
            return $http.post(url, fd, fileConfig);
        },
        get: function () {
            var url = apiBaseUrl + "feedback/testimonials";
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "feedback/testimonials/delete";
            return $http.post(url, postData, config);
        }
    };
});