webServices.service('customersService', function ($http) {
    return {
        getCustomerDetailsWithMobile: function (mobile) {
            var url = apiBaseUrl + "master/customers/customers_with_mobile?";
            url += "mobile=" + mobile;
            return $http.get(url);
        },
        getListOfCustomers: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "master/customers?";
            if (obj.page) {
                url += "page=" + obj.page;
            }
            return $http.post(url, postData, config);
        }
    };
});