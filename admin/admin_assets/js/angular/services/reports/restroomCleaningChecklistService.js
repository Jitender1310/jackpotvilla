webServices.service('restroomCleaningChecklistService', function ($http) {
    return {
        get: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "reports/restroom_cleaning_checklist?";
            if (obj.page) {
                url += "page=" + obj.page;
            }
            return $http.post(url, postData, config);
        },
        getStaff: function (centers_id) {
            var url = apiBaseUrl + "reports/restroom_cleaning_checklist/get_staff?";
            url += "centers_id=" + centers_id;
            return $http.get(url);
        },

    };
});