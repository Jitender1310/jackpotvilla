webServices.service('salesReportsService', function ($http) {
    return {
        get: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "reports/sales_reports?";
            return $http.post(url, postData, config);
        }
    };
});