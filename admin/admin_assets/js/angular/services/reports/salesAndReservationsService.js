webServices.service('salesAndReservationsService', function ($http) {
    return {
        get: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "reports/sales_and_reservations?";
            if (obj.page) {
                url += "page=" + obj.page;
            }
            return $http.post(url, postData, config);
        }
    };
});