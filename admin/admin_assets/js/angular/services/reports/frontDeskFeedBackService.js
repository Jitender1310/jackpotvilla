webServices.service('frontDeskFeedBackService', function ($http) {
    return {
        get: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "reports/frontdesk_feedback?";
            if (obj.page) {
                url += "page=" + obj.page;
            }
            return $http.post(url, postData, config);
        }
    };
});