webServices.service('dailyWiseArrivalReportService', function ($http) {
    return {
        get: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "reports/daily_wise_arrival_report?";
            if (obj.page) {
                url += "page=" + obj.page;
            }
            return $http.post(url, postData, config);
        }
    };
});