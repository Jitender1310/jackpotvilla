webServices.service('occupancyService', function ($http) {
    return {
        get: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "reports/category_wise_hours_booked?";
            return $http.post(url, postData, config);
        }
    };
});