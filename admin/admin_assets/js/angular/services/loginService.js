webServices.service('loginService', function ($http) {
    return {
        login: function (userObj) {
            var postData = $.param(userObj);
            var url = apiBaseUrl + "login";
            return $http.post(url, postData, config);
        }
    };
});