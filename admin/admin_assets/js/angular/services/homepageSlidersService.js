webServices.service('homepageSlidersService', function ($http) {
    return {
        save: function (fd) {
            var url = apiBaseUrl + "master/homepage_sliders/add";
            return $http.post(url, fd, fileConfig);
        },
        get: function () {
            var url = apiBaseUrl + "master/homepage_sliders";
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "master/homepage_sliders/delete";
            return $http.post(url, postData, config);
        }
    };
});