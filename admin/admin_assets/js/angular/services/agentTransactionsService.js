webServices.service('agentTransactionsService', function ($http) {
    return {
        getListOfTransactions: function (obj) {
            var postData = $.param(obj);
            var url = apiBaseUrl + "master/agents/transactions?";
            if (obj.page) {
                url += "page=" + obj.page;
            }
            return $http.post(url, postData, config);
        }
    };
});
