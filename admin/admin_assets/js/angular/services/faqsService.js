webServices.service('faqsService', function ($http) {
    return {
        save: function (obj) {
            var url = apiBaseUrl + "faqs/add";
            return $http.post(url, obj, fileConfig);
        },
        get: function () {
            var url = apiBaseUrl + "faqs";
            return $http.get(url);
        },
        delete: function (id) {
            var postData = "id=" + id;
            var url = apiBaseUrl + "faqs/delete";
            return $http.post(url, postData, config);
        },
    };
});