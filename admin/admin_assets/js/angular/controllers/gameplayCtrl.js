app.controller("gameplayCtrl", function ($scope, $timeout, gameplayService, $sce, DTOptionsBuilder) {
    $scope.filterObj = {
        rooms_id: '',
        game_sub_type: '',
        game_type: '',
        page: 1,
        limit: 10,
        sort_by: 'Latest'
    };

    $scope.gameplayItems = [];
    $scope.curr_game_info = false;

    $scope.GetGamePlayData = function () {
        

        var filter = {
            page: $scope.filterObj.page,
            from_date: $scope.filterObj.from_date,
            to_date: $scope.filterObj.to_date,
            limit: $scope.filterObj.limit,
            sort_by: $scope.filterObj.sort_by
        };

        if ($scope.filterObj.game_type) {
            filter.game_type = $scope.filterObj.game_type;
        }
        if ($scope.filterObj.game_sub_type) {
            filter.game_sub_type = $scope.filterObj.game_sub_type;
        }
        if ($scope.filterObj.rooms_id) {
            filter.rooms_id = $scope.filterObj.rooms_id;
        }
        $scope.spinny(true);
        gameplayService.list(filter).then(function (response) {
            $scope.gameplayItems = response.data.data.results;
            
            $scope.gamePlayDataPagination = response.data.pagination;
            if ($scope.gamePlayDataPagination.pagination.length > 0) {
                $scope.gamePlayDataPagination.pagination = $sce.trustAsHtml($scope.gamePlayDataPagination.pagination);
            }
            $scope.spinny(false);
        });
    };
    $timeout(function () {
        $scope.GetGamePlayData();
    }, 600);
    

    $scope.loadGameplayData = function (index) {
       
        if ($scope.gameplayItems[ index ]) {
             $scope.spinny(true);
            $scope.curr_game_info = JSON.parse($scope.gameplayItems[ index ].game_detail_json);
//            $scope.curr_game_info['type'] = $scope.gameplayItems[ index ].game_type;
//            $scope.curr_game_info['subtype'] = $scope.gameplayItems[ index ].game_sub_type;
            $scope.curr_game_info['prize_amount'] = 0;
            $scope.curr_game_info['prize_amount_after_detection'] = 0;
            for (var i = 0; i < $scope.curr_game_info.gameResult.players_game_details.length; i++) {
                if(parseFloat($scope.curr_game_info.gameResult.players_game_details[i].winnings)>0){
                    $scope.curr_game_info['prize_amount'] += parseFloat($scope.curr_game_info.gameResult.players_game_details[i].winnings) + parseFloat($scope.curr_game_info.gameResult.players_game_details[i].admin_commission_amount) + parseFloat($scope.curr_game_info.gameResult.players_game_details[i].tds_amount);
                    $scope.curr_game_info['prize_amount_after_detection'] = parseFloat($scope.curr_game_info.gameResult.players_game_details[i].winnings);
                }
            }
            
            $timeout(function () {
                angular.element('#gameplay_single_modal').modal('show');
                $scope.spinny(false);
            }, 500); 
        } else {
            swal('No Item Found');
        }
         
    };

    $scope.card_image_link = function (cardObj) {
        return STATIC_ADMIN_IMAGES_PATH + 'cards/' + cardObj.suit + '_' + cardObj.rank + '.png';
    };

    $scope.calculate_prize_wo_comm = function (curr_game) {
        return parseFloat(curr_game.prize_amount) - parseFloat(curr_game.gameResult.admin_commission_amount) - parseFloat(curr_game.gameResult.tds_amount) - parseFloat(curr_game.gameResult.gst_amount)
    }

    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', false)
            .withOption('bPaginate', false)
            .withOption('lengthChange', false)
            .withOption('bInfo', false)
            .withOption('bSort', false);
    
    $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filterObj.page = $(this).attr("data-ci-pagination-page");
        $scope.GetGamePlayData();
    });
});