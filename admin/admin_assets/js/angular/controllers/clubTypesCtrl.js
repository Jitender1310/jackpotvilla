angular.element("#clubTypeForm").validate({
    rules: {
        name: {
            required: true
        },
        from_points: {
            required: true,
            number:true
        },
        to_points:{
            required: true,
            number:true
        },
        priority: {
            required: true,
            number:true
        }        
    }, messages: {
        name: {
            required: "Club Type Name is required"
        },
        from_points: {
            required: "From points cannot be empty",
            number:"Please enter valid number"
        },
        to_points:{
            required: "To points cannot be empty",
            number:"Please enter valid number"
        },
        priority: {
            required: true,
            number:"Please enter valid number"
        }
    }
});
app.controller("clubTypesCtrl", function ($scope, $timeout, clubTypesService, DTOptionsBuilder, loaderService) {
    $scope.showLoader = false;
    function initializeObject() {
        $scope.clubType = {
            name: '',
            priority:"",
            from_points:"",
            to_points:"",
            validity_in_days: '',
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };
    $scope.AddOrUpdateClubType = function () {
        $scope.error_message = "";
        if (angular.element("#clubTypeForm").valid()) {
            $scope.spinny(true);
            clubTypesService.save($scope.clubType).then(function (response) {
                $scope.spinny(false);
                if (response.data.status === "valid") {
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.GetClubTypes();
                } else if (response.data.status === "invalid_form") {
                    $scope.g_error = response.data.data;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                    $scope.noty('warning', response.data.title, response.data.message);
                }
            });
        }
    };
    $scope.GetClubTypes = function () {
        $scope.spinny(true);
        clubTypesService.get().then(function (response) {
            $scope.spinny(false);
            $scope.clubTypes = response.data.data;
        });
    };
    
    $timeout(function(){
       $scope.GetClubTypes(); 
    },1000);
    
    
    $scope.EditClubType = function (obj) {
        $scope.clubType = angular.copy(obj);
    };
    $scope.DeleteClubType = function (item) {
        $scope.sweety(item.name, function () {
            clubTypesService.delete(item.id).then(function (response) {
                if (response.data.err_code === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetClubTypes();
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);
});