angular.element("#moduleForm").validate({
    rules: {
        module_name: {
            required: true,
//            letterswithbasicpunc: true
        }
    }, messages: {
        module_name: {
            required: "Enter module name",
//            letterswithbasicpunc: "Module name should be alphabets only"
        }
    }
});
app.controller("modulesCtrl", function ($scope, $timeout, modulesService, DTOptionsBuilder, loaderService) {
    $scope.showLoader = false;
    function initializeObject() {
        $scope.moduleObj = {
            parent_module_id: '',
            module_name: '',
            display_order: 10
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };
    $scope.AddOrUpdateModule = function () {
        $scope.error_message = "";
        $scope.m_error = {};
        if (angular.element("#moduleForm").valid()) {
            $scope.showLoader = true;
            modulesService.save($scope.moduleObj).then(function (response) {
                $scope.showLoader = false;
                if (response.data.status === "valid") {
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.GetModules();
                } else if (response.data.status === "invalid_form") {
                    $scope.m_error = response.data.data;
                    $scope.error_message = response.data.message;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };
    $scope.GetModules = function () {
        modulesService.get().then(function (response) {
            $scope.modulesList = response.data.data;
        });
    };
    
    $timeout(function(){
        $scope.GetModules();
    },500);
    
    $scope.EditModule = function (obj) {
        $scope.moduleObj = angular.copy(obj);
    };
    $scope.DeleteModule = function (item) { 
        $scope.sweety(item.module_name, function () {
            modulesService.delete(item.id).then(function (response) {
                if (response.data.status === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetModules();
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);

});