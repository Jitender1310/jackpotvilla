angular.element("#avatarForm").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        title: {
            required: true
        },
        gender: {
            required: true
        }
    }, messages: {
        title: {
            required: "Please enter avatar name"
        },
        gender: {
            required: "Plesae select gender"
        }
    }
});
app.controller("testimonialsCtrl", function ($scope, $timeout, testimonialsService, DTOptionsBuilder) {
    $scope.showLoader = false;
    function initializeObject() {
        $scope.testimonialObj = {
            id: '',
            name_and_address: '',
            prize_title: '',
            message: '',
            image: ''
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };
    $scope.GetTestimonialsList = function () {
        testimonialsService.get().then(function (response) {
            $scope.testimonialsList = response.data.data;
        });
    };
    
    $timeout(function(){
        $scope.GetTestimonialsList();
    }, 500);

    $scope.AddOrUpdateTestimonial = function () {
        $scope.error_message = "";
        $scope.p_error = {};
        if (angular.element("#testimonialForm").valid()) {
            
            var fd = new FormData();
            var ObjectKeys = Object.keys($scope.testimonialObj);
            console.log(ObjectKeys);

            for (var i = 0; i < ObjectKeys.length; i++) {
                if (ObjectKeys[i] === "image") {
                    continue;
                }
                fd.append(ObjectKeys[i], $scope.testimonialObj[ObjectKeys[i]]);
            }

            if (document.getElementById("image") != null) {
                fd.append("image", document.getElementById("image").files[0]);
            }
            
            
            $scope.spinny(true);
            testimonialsService.save(fd).then(function (response) {
                $scope.spinny(false);
                if (response.data.status === "valid") {
                    angular.element("#testimonial-modal-popup").modal("hide");
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.GetTestimonialsList();
                } else if (response.data.status === "invalid_form") {
                    $scope.t_error = response.data.data;
                    $scope.error_message = response.data.message;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };


    $scope.DeleteTestimonial = function (item) {
        $scope.sweety(item.fullname, function () {
            testimonialsService.delete(item.id).then(function (response) {
                if (response.data.status === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetTestimonialsList();
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);
    
    $scope.ShowTestimonialAddForm = function () {
        initializeObject();
        angular.element("#testimonial-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };

});