angular.element("#latsetUpdateForm").validate({
    rules: {
        text: {
            required: true
        }
    }, messages: {
        text: {
            required: "Notification text is required"
        }
    }
});
app.controller("latestUpdatesCtrl", function ($scope, latestUpdatesService, DTOptionsBuilder) {
    $scope.filter = {};
    //$scope.filter.page = 1;
    $scope.showLoader = false;
    function initializeObject() {
        $scope.latestUpdateObj = {
            text: ''
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };

    $scope.AddOrUpdatelatestUpdates = function () {
        $scope.error_message = "";
        if (angular.element("#latsetUpdateForm").valid()) {
            $scope.showLoader = true;
            latestUpdatesService.add($scope.latestUpdateObj).then(function (response) {
                $scope.showLoader = false;
                if (response.data.err_code === "valid") {
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.GetLatestUpdatesList();
                } else if (response.data.err_code === "invalid_form") {
                    $scope.error_message = response.data.message;
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };


    $scope.GetLatestUpdatesList = function () {
        latestUpdatesService.get($scope.filter).then(function (response) {
            $scope.latestUpdatesList = response.data.data;
        });
    };


    $scope.EditLatestUpdate = function (obj) {
        $scope.latestUpdateObj = angular.copy(obj);
    };
    $scope.DeleteLatestUpdate = function (item) {
        $scope.sweety(item.title, function () {
            latestUpdatesService.delete(item.id).then(function (response) {
                if (response.data.err_code === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetLatestUpdatesList();
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('bInfo', false);
});