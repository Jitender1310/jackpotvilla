angular.element("#latsetUpdateForm").validate({
    rules: {
        text: {
            required: true
        }
    }, messages: {
        text: {
            required: "Notification text is required"
        }
    }
});
app.controller("contactEnquiriesCtrl", function ($scope, contactEnquiriesService, DTOptionsBuilder) {
    $scope.filter = {};
    //$scope.filter.page = 1;
    $scope.showLoader = false;
    function initializeObject() {
        $scope.latestUpdateObj = {
            text: ''
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };
//
//    $scope.AddOrUpdatelatestUpdates = function () {
//        $scope.error_message = "";
//        if (angular.element("#latsetUpdateForm").valid()) {
//            $scope.showLoader = true;
//            contactEnquiriesService.add($scope.latestUpdateObj).then(function (response) {
//                $scope.showLoader = false;
//                if (response.data.err_code === "valid") {
//                    $scope.noty('success', response.data.title, response.data.message);
//                    initializeObject();
//                    $scope.GetContactEnquiriesList();
//                } else if (response.data.err_code === "invalid_form") {
//                    $scope.error_message = response.data.message;
//                } else if (response.data.err_code === "invalid") {
//                    $scope.error_message = response.data.message;
//                }
//            });
//        }
//    };


    $scope.GetContactEnquiriesList = function () {
        contactEnquiriesService.get($scope.filter).then(function (response) {
            $scope.contactEnquiriesList = response.data.data;
        });
    };


//    $scope.EditLatestUpdate = function (obj) {
//        $scope.latestUpdateObj = angular.copy(obj);
//    };
    $scope.DeleteContactEnquiry = function (item) {
        $scope.sweety(item.title, function () {
            contactEnquiriesService.delete(item.id).then(function (response) {
                if (response.data.err_code === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetContactEnquiriesList();
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('bInfo', false);
});