angular.element("#tournamentForm").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        tournament_categories_id: {
            required: true,
        },
    }, messages: {
        tournament_categories_id: {
            required: "Select Tournament Category",
        },
    }
});
app.controller("createRealTournamentCtrl", function ($timeout, $scope, tournamentCategoriesService, createRealTournamentService, clubTypesService, DTOptionsBuilder) {
    $scope.showLoader = false;
    function initializeObject() {
        $scope.tournamentObj = {
            tournaments_id: '',
            tournament_categories_id: '',
            allowed_club_types: '',
            tournament_title: '',
            tournament_format: '',
            no_of_bots: '',
            winning_probability_to: '',
            entry_type: '',
            entry_value: 0,
            tournament_commission_percentage: 0,
            prizes_info: [],
            tournament_structure: []
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };
    $scope.ChangeComission = function () {
        if ($scope.tournamentObj.entry_type == 'Free') {
            $scope.tournamentObj.tournament_commission_percentage = '0';
        }
    }
    $scope.getTournamentsInfo = function (tournaments_id) {
        if (tournaments_id === '') {
            return false;
        }
        createRealTournamentService.get($scope.tournaments_id).then(function (response) {
            $scope.tournamentObj = response.data.data;
            $timeout(function () {
                $scope.calculatePrize();
            }, 500);
        });
    }

    $timeout(function () {
        $scope.getTournamentsInfo($scope.tournaments_id);
    }, 500);
    $scope.GetTournamentCategories = function () {
        tournamentCategoriesService.get().then(function (response) {
            $scope.tournamentCategoires = response.data.data;
        });
    };
    $scope.GetTournamentCategories();
    $scope.AddOrUpdateClonedTournaments = function () {
        $scope.error_message = "";
        $scope.g_error = {};
        if (angular.element("#tournamentForm").valid()) {
            if ($scope.tournamentObj.expected_prize_amount != $scope.totalPrize) {
                $scope.noty('warning', "Tournament Creation Error", "Expected prize should match with total prize distributions amount");
                return;
            }
            $scope.spinny(true);
            createRealTournamentService.save($scope.tournamentObj).then(function (response) {
                $scope.spinny(false);
                if (response.data.status === "valid") {
                    //                angular.element("#tournaments-modal-popup").modal("hide");
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    //                    $scope.getTournamentsList();
                    // window.location.href = baseurl + "real_tournaments/upcoming";
                    location.href = baseurl + "real_tournaments/upcoming";
                } else if (response.data.status === "invalid_form") {
                    $scope.t_error = response.data.data;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                    $scope.noty('warning', response.data.title, response.data.message);
                }
            });
        }
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);
    $scope.ShowTournamentsAddForm = function () {
        initializeObject();
        angular.element("#tournaments-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };
    $scope.GetClubTypes = function () {
        clubTypesService.get().then(function (response) {
            $scope.clubTypes = response.data.data;
        });
    };
    $scope.GetClubTypes();
    $scope.weeks = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    $scope.calculateExpectedPrize = function () {
        $scope.calculatePrize();
        $scope.tournamentObj.tournament_commission_percentage = parseFloat($scope.tournamentObj.tournament_commission_percentage);
        $scope.tournamentObj.max_players = parseInt($scope.tournamentObj.max_players);
//        $scope.tournamentObj.entry_value = parseInt($scope.tournamentObj.entry_value);
//        var prizeValue = ($scope.tournamentObj.max_players * $scope.tournamentObj.entry_value);
        var entry_fee_value = 0;
        if($scope.tournamentObj.entry_type == "Free"){
            $scope.tournamentObj.entry_value = parseInt($scope.tournamentObj.entry_fee);
            entry_fee_value = $scope.tournamentObj.entry_fee;
        }else{
            $scope.tournamentObj.entry_value = parseInt($scope.tournamentObj.entry_value);
            entry_fee_value = $scope.tournamentObj.entry_value;
        }
        var prizeValue = ($scope.tournamentObj.max_players * entry_fee_value);
        $scope.tournamentObj.expected_prize_amount = prizeValue - (prizeValue * $scope.tournamentObj.tournament_commission_percentage / 100);
//        console.log($scope.tournamentObj.expected_prize_amount);
    };
    $scope.tournamentObj.tournament_structure = [];
    $scope.AddTournamentRow = function () {
        $scope.tournamentObj.tournament_structure.push({
            round_number: "",
            qualify: ""
        });
    };
    $scope.AddTournamentRow();
    $scope.AddPrizeRow = function () {
        $scope.tournamentObj.prizes_info.push({
            from_rank: "",
            to_rank: "",
            min_prize_value: 0,
            prize_value: 0
        });
    };
    $scope.AddPrizeRow();
    $scope.totalPrize = 0;
    $scope.RemovePrizeRow = function (pos) {
        var newDataList = [];
        for (var i = 0, l = $scope.tournamentObj.prizes_info.length; i < l; i++) {
            if (i !== pos) {
                newDataList.push($scope.tournamentObj.prizes_info[i]);
            }
        }
        $scope.tournamentObj.prizes_info = newDataList;
        $scope.calculatePrize();
    };
    
    $scope.calculatePrize = function () {
        $scope.totalPrize = 0;
        for (var i = 0; i < $scope.tournamentObj.prizes_info.length; i++) {
            $scope.tournamentObj.prizes_info[i].prize_value = parseInt($scope.tournamentObj.prizes_info[i].prize_value);
            if ($scope.tournamentObj.prizes_info[i].prize_value > 0) {
                
                if($scope.tournamentObj.prizes_info[i].to_rank > 0){
                    $scope.tournamentObj.prizes_info[i].row_total_prize = ((parseFloat($scope.tournamentObj.prizes_info[i].to_rank)+1) - parseFloat($scope.tournamentObj.prizes_info[i].from_rank) ) *  parseFloat($scope.tournamentObj.prizes_info[i].prize_value);
                    $scope.totalPrize += $scope.tournamentObj.prizes_info[i].row_total_prize;
                }else{
                    $scope.tournamentObj.prizes_info[i].row_total_prize = parseFloat($scope.tournamentObj.prizes_info[i].prize_value);
                    $scope.totalPrize += $scope.tournamentObj.prizes_info[i].row_total_prize;
                    
                }
                
                console.log($scope.totalPrize);
            }
        }
    }

});