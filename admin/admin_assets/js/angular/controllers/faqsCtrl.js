angular.element("#faqsForm").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        faq_categories_id: {
            required: true
        },
        title: {
            required: true
        },
        description: {
            required: true
        }
    }, messages: {
        category: {
            required: "Please enter category name"
        },
        title: {
            required: "Plesae enter title"
        },
        description: {
            required: "Plesae enter description"
        },
    }
});
app.controller("faqsCtrl", function ($scope, $timeout, faqsService, faqCategoriesService, DTOptionsBuilder) {
    $scope.showLoader = false;
    function initializeObject() {
        $scope.faqsObj = {
            id: '',
            faq_categories_id: '',
            title: '',
            description: '',
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };
    $scope.GetFaqsList = function () {
        faqsService.get().then(function (response) {
            $scope.faqsList = response.data.data;
        });
    };

    $timeout(function () {
        $scope.GetFaqsList();
    }, 500);

    $scope.GetFaqCategorysList = function () {
        faqCategoriesService.get().then(function (response) {
            $scope.faqCategoriesList = response.data.data;
        });
    };

    $timeout(function () {
        $scope.GetFaqCategorysList();
    }, 500);

    $scope.AddOrUpdateFaq = function () {
        $scope.error_message = "";
        $scope.p_error = {};
        if (angular.element("#faqsForm").valid()) {

            var fd = new FormData();
            var ObjectKeys = Object.keys($scope.faqsObj);
//            console.log(ObjectKeys);

            for (var i = 0; i < ObjectKeys.length; i++) {
                if (ObjectKeys[i] === "icon") {
                    continue;
                }
                fd.append(ObjectKeys[i], $scope.faqsObj[ObjectKeys[i]]);
            }

            $scope.spinny(true);
            faqsService.save(fd).then(function (response) {
                $scope.spinny(false);
                if (response.data.status === "valid") {
                    angular.element("#faqs-modal-popup").modal("hide");
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.GetFaqsList();
                } else if (response.data.status === "invalid_form") {
                    $scope.t_error = response.data.data;
                    $scope.error_message = response.data.message;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };


    $scope.DeleteFaq = function (item) {
        $scope.sweety(item.title, function () {
            faqsService.delete(item.id).then(function (response) {
                if (response.data.status === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetFaqsList();
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);

    $scope.ShowFaqsAddForm = function () {
        initializeObject();
        angular.element("#faqs-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };

    $scope.EditFaq = function (obj) {
        $scope.faqsObj = angular.copy(obj);
        angular.element("#faqs-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };

});