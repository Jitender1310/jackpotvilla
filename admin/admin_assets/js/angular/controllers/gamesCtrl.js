angular.element("#gameForm").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        game_type: {
            required: true
        },
        game_sub_type: {
            required: true
        },
        seats: {
            required: true
        },
        entry_fee: {
            required: true
        }
    }, messages: {
        game_type: {
            required: "Please Choose Game Type"
        },
        game_sub_type: {
            required: "Please Choose Game Sub Type"
        },
        seats: {
            required: "Please choose number of seats"
        },
        entry_fee: {
            required: "Entry free cannot be empty"
        }
    }
});
app.controller("gamesCtrl", function ($scope, gamesService, DTOptionsBuilder) {
    $scope.showLoader = false;
    function initializeObject() {
        $scope.gameObj = {
            id: '',
            game_type: '',
            game_sub_type: '',
            number_of_cards: '',
            seats: '',
            point_value: '',
            entry_fee: '',
            pool_deal_prize: '',
            pool_game_type: '',
            deals: ''
        };
    }

    $scope.filter = {
        //page: 1,
        game_type: '',
        game_sub_type: ''
    };
    $scope.ResetFilter = function () {
        $scope.filter = {
            //page: 1,
            game_type: '',
            game_sub_type: ''
        };
        $scope.getGamesList();
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };
    $scope.getGamesList = function () {
        $scope.spinny(true);
        gamesService.get($scope.filter).then(function (response) {
            $scope.spinny(false);
            $scope.gamesList = response.data.data;
        });
    };

    setTimeout(function () {
        $scope.getGamesList();
    }, 150);
    $scope.AddOrUpdateGame = function () {
        $scope.error_message = "";
        $scope.g_error = {};
        if (angular.element("#gameForm").valid()) {
            $scope.spinny(true);
            gamesService.save($scope.gameObj).then(function (response) {
                $scope.spinny(false);
                if (response.data.status === "valid") {
                    angular.element("#game-modal-popup").modal("hide");
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.getGamesList();
                } else if (response.data.status === "invalid_form") {
                    $scope.g_error = response.data.data;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                    $scope.noty('warning', response.data.title, response.data.message);
                }
            });
        }
    };

    $scope.EditGame = function (obj) {
        $scope.gameObj = angular.copy(obj);
        angular.element("#game-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };

    $scope.DeleteGame = function (item) {
        $scope.sweety(item.fullname, function () {
            gamesService.delete(item.id).then(function (response) {
                if (response.data.status === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.getGamesList();
                } else if (response.data.status === "invalid_form") {
                    $scope.g_error = response.data.data;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);

    $scope.ShowGameAddForm = function () {
        initializeObject();
        angular.element("#game-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };

    $scope.ViewGame = function (obj) {
        $scope.gameObj = angular.copy(obj);
        angular.element("#view-game-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };

    $scope.CalculatePrizeValue = function () {
        if ($scope.gameObj.game_sub_type === "Deals" || $scope.gameObj.game_sub_type === "Pool") {
            $scope.gameObj.pool_deal_prize = parseInt($scope.gameObj.entry_fee) * parseInt($scope.gameObj.seats);
        }
    };
});