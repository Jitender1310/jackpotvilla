angular.element("#template_form").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        title: {
            required: true
        },
        message: {
            required: true
        },

    }, messages: {
        title: {
            required: "Title is required"
        },
        message: {
            required: "Message is required"
        },

    }
});
app.controller("smsTemplatesCtrl", function ($scope, smsTemplatesService, DTOptionsBuilder, loaderService) {
    $scope.showLoader = false;
    function initializeObject() {
        $scope.templateObj = {
            title: '',
            message: '',
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };

    $scope.AddOrUpdateTemplate = function () {
        $scope.error_message = "";
        if (angular.element("#template_form").valid()) {
            $scope.showLoader = true;
            smsTemplatesService.save($scope.templateObj).then(function (response) {
                $scope.showLoader = false;
                if (response.data.err_code === "valid") {
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.GetTemplatesList();
                } else if (response.data.err_code === "invalid_form") {
                    $scope.error_message = response.data.message;
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };


    $scope.GetTemplatesList = function () {
        smsTemplatesService.get().then(function (response) {
            $scope.sms_templatesList = response.data.data;
        });
    };


    $scope.EditTemplate = function (obj) {
        $scope.templateObj = angular.copy(obj);
    };
    $scope.DeleteTemplate = function (item) {
        $scope.sweety(item.title, function () {
            smsTemplatesService.delete(item.id).then(function (response) {
                if (response.data.err_code === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetTemplatesList();
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);
});