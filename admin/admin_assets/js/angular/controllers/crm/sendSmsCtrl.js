angular.element("#sms_form").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        mobile: {
            required: true
        },
        title: {
            required: true
        },
        message: {
            required: true
        },

    }, messages: {
        title: {
            required: "Title is required"
        },
        mobile: {
            required: "Mobile is required"
        },
        message: {
            required: "Message is required"
        },

    }
});
app.controller("sendSmsCtrl", function ($scope, sendSmsService, smsTemplatesService, loaderService) {
    $scope.showLoader = false;
    function initializeObject() {
        $scope.smsObj = {
            title: '',
            message: '',
            mobile: '',
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };

    $scope.SendSms = function () {
        $scope.error_message = "";
        if (angular.element("#sms_form").valid()) {
            $scope.showLoader = true;
            sendSmsService.send($scope.smsObj).then(function (response) {
                $scope.showLoader = false;
                if (response.data.err_code === "valid") {
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.GetTemplatesList();
                } else if (response.data.err_code === "invalid_form") {
                    $scope.error_message = response.data.message;
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };


    $scope.GetTemplatesList = function () {
        smsTemplatesService.get().then(function (response) {
            $scope.sms_templatesList = response.data.data;
        });
    };
    $scope.GetTemplatesList();

    $scope.GetMessage = function (id) {
        $scope.smsObj.message = "";
        smsTemplatesService.get_message(id).then(function (response) {
            $scope.smsObj.message = response.data.data;
        });
    };

});