app.controller("customerContactsCtrl", function ($scope, customerContactsService, DTOptionsBuilder, loaderService) {
    $scope.showLoader = false;
    $scope.GetCustomersList = function () {
        customerContactsService.get().then(function (response) {
            $scope.customersList = response.data.data;
        });
    };
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', true)
            .withOption('searching', true);
});