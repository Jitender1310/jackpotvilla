app.controller("customersCtrl", function ($scope, $sce, customersService, DTOptionsBuilder) {
    $scope.filter = {};
    $scope.customersList = [];

    $scope.GetCustomersList = function () {
        $scope.spinny(true);
        customersService.getListOfCustomers($scope.filter).then(function (response) {
            $scope.spinny(false);
            $scope.customersList = response.data.data;
            $scope.customersPagination = response.data.pagination;
            $scope.customersPagination.pagination = $sce.trustAsHtml($scope.customersPagination.pagination);
        });
    };


    $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filter.page = $(this).attr("data-ci-pagination-page");
        $scope.GetCustomersList();
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', false)
            .withOption('bPaginate', false)
            .withOption('lengthChange', false)
            .withOption('bInfo', false)
            .withOption('bSort', false);

    $scope.ViewCustomerInfo = function (item) {
        $scope.customerObj = item;
        angular.element("#customer_view_modal").modal("show");
    };
});
