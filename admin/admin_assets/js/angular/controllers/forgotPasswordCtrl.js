angular.element("#forgotPasswordForm").validate({
    rules: {
        username: {
            required: true
        }
    }, messages: {
        username: {
            required: "Username is required"
        }
    }
});
app.controller("forgotPasswordCtrl", function ($scope, forgotPasswordService, $cookies, loaderService) {
    $scope.showLoader = false;
    function initializeObject() {
        $scope.user = {
            username: ''
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };
    $scope.ForgotPassword = function () {
        $scope.error_message = "";
        if (angular.element("#forgotPasswordForm").valid()) {
            $scope.spinny(true);
            forgotPasswordService.forgotPassword($scope.user).then(function (response) {
                $scope.spinny(false);
                if (response.data.err_code === "valid") {
                    $scope.noty('success', response.data.message, response.data.data);
                    initializeObject();
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };
});