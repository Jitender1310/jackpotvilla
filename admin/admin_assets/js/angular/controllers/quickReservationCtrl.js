angular.element("#createReservationForm").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".custom-group"));
    },
    rules: {
        contact_mobile: {
            required: true,
            digits: true,
            minlength: 10,
            maxlength: 10
        },
        contact_name: {
            required: true
        },
        contact_email: {
            required: true,
            email: true
        },
        zipcode: {
            required: true
        },
        check_in_date: {
            required: true
        },
        check_in_time: {
            required: true
        },
        duration: {
            required: true
        }
    },
    messages: {
        contact_mobile: {
            required: "Contact mobile is required",
            minlength: "Enter valid mobile number",
            maxlength: "Enter valid mobile number"
        },
        contact_name: {
            required: "Contact name is required",
        },
        contact_email: {
            required: "Contact email is required",
        }
    }
});
app.controller("quickReservationCtrl", function ($scope, createReservationService, leadsService, customersService, countriesService, centersService, howDidHearAboutUsService, businessSourcesService, companiesService, agentsService, agentTypesService, taxSlabsService, DTOptionsBuilder, loaderService, $timeout) {
    $scope.showLoader = false;
    $scope.validation_triggered = 0;
    $scope.resetFormValidations = function () {
        if ($scope.validation_triggered === 0) {
            return;
        }
        var validator = angular.element("#createReservationForm").validate();
        validator.resetForm();
        angular.element("#createReservationForm").valid();
    };

    function initializeObject() {
        $scope.tempBookingObjStructure = {
            stay_category_name: "",
            staying_type_categories_id: "",
            items: []
        };
        $scope.tempItemObjStructure = {
            staying_type_categories_id: "",
            qty: 1,
            unit_price: '',
            mobile_number: ''
        };

        $scope.reservationObj = {
            creation_source: 'front_desk_portal',
            agent_leads_id: '',
            centers_id: '',
            contact_mobile: '',
            contact_name: '',
            contact_email: '',
            billing_to: 'Customer',
            door_no: '',
            street_name: '',
            location: '',
            city: '',
            district: '',
            state: '',
            zipcode: '',
            countries_id: '',
            staying_types_id: 2,
            check_in_date: '',
            check_in_time: '',
            duration: '',
            duration_type: '',
            persons: '',
            how_did_hear_about_us_id: '',
            agent_mobile: '',
            agent_commission: '',
            booking_status: 'Pending',
            business_sources_id: '',
            alternative_mobile: '',
            complimentary: '',
            selected_stay_categories: [],
            booking_items: [],
            payment_info: {
                new_items: 0,
                amount: 0,
                applied_taxes: [
                    /*{
                     tax_name: "SGST",
                     tax_amount: 20,
                     tax_calculation_type: 'pencentage',
                     applied_tax_amount: 0
                     }*/
                ],
                sub_total: 0,
                grand_total: 0,
                payments_total: 0,
                applied_tax_amount: 0,
                discount_amount: 0,
                discount_value: 0,
                discount_type: '',
                payments_list: [

                ]
            },
            activity_tracker: []
        };
        $scope.companiesList = [];
        $scope.stayAvailabilityList = [];
        $scope.taxFilterObj = {
            centers_id: $scope.reservationObj.centers_id,
            check_in_date: '',
            check_in_time: '',
            duration: ''
        };
        $scope.taxesList = [];
        $scope.allowBooking = false;
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };

    $scope.activityTrackingObj = {
        component: "",
        comment: "",
        activity_start_time: "",
        activity_completed_time: "",
        activity_source: "front_desk_portal"
    };

    function initializeCompanyObject() {
        $scope.companyObj = {
            company_name: '',
            tax_identity_number: ''
        };
    }
    initializeCompanyObject();
    $scope.ResetCompanyForm = function () {
        initializeCompanyObject();
    };

    function initializeAgentObject() {
        $scope.agentObj = {
            agent_types_id: '',
            agent_name: '',
            mobile: '',
            email: '',
            commission: '',
            login_required: '0',
            password: ''
        };
    }
    initializeAgentObject();
    $scope.ResetAgentForm = function () {
        initializeAgentObject();
    };

    function initializeAvailabilitySearchObject() {
        $scope.availabilitySearchObj = {
            check_in_date: '',
            check_in_time: '',
            duration: '',
            persons: ''
        };
    }
    initializeAvailabilitySearchObject();
    $scope.ResetAvailabilitySearchForm = function () {
        initializeAvailabilitySearchObject();
    };



    $scope.FetchCustomerData = function () {
        if ($scope.reservationObj.contact_mobile.length >= 10) {
            $scope.spinny(true);
            customersService.getCustomerDetailsWithMobile($scope.reservationObj.contact_mobile).then(function (response) {
                $scope.spinny(false);
                if (response.data.data != '') {
                    $scope.resetFormValidations();
                    $scope.reservationObj.contact_name = response.data.data.contact_name;
                    $scope.reservationObj.contact_email = response.data.data.contact_email;
                    $scope.reservationObj.alternative_mobile = response.data.data.alternative_mobile;
                    $scope.reservationObj.door_no = response.data.data.door_no;
                    $scope.reservationObj.street_name = response.data.data.street_name;
                    $scope.reservationObj.location = response.data.data.location;
                    $scope.reservationObj.city = response.data.data.city;
                    $scope.reservationObj.district = response.data.data.district;
                    $scope.reservationObj.state = response.data.data.state;
                    $scope.reservationObj.zipcode = response.data.data.zipcode;
                    $scope.reservationObj.countries_id = response.data.data.countries_id;

                    $scope.SaveOrUpdateActivityTime(1);
                }
            });
        }
    };

    $scope.GetCountriesList = function () {
        countriesService.get().then(function (response) {
            $scope.countriesList = response.data.data;
        });
    };
    $scope.GetCountriesList();

    $scope.centersSpinner = false;
    $scope.GetCentersList = function (users_id) {
        $scope.centersSpinner = true;
        centersService.getOnlyAssignedCenters(users_id).then(function (response) {
            $scope.centersSpinner = false;
            $scope.centersList = response.data.data;
            $scope.GetSessionCenterId();
        });
    };
    $scope.freshupBookingAvailable = false;
    $scope.hotelBookingAvailable = false;
    $scope.GetSessionCenterId = function () {
        $scope.freshupBookingAvailable = false;
        $scope.hotelBookingAvailable = false;
        centersService.getSelectedCenterId().then(function (response) {
            $scope.selected_center_id = response.data.data;
            $scope.reservationObj.centers_id = response.data.data;
            $scope.available_stay_types = response.data.available_stay_types;
            for (var i = 0; i < $scope.available_stay_types.length; i++) {
                if ($scope.available_stay_types[i].staying_types_id == 2) {
                    $scope.freshupBookingAvailable = true;
                } else if ($scope.available_stay_types[i].staying_types_id == 1) {
                    $scope.hotelBookingAvailable = true;
                }
            }

            $scope.CheckIsIRCTCCenter();
        });
    };

    $scope.GetReferencesList = function () {
        howDidHearAboutUsService.get().then(function (response) {
            $scope.referencesList = response.data.data;
        });
    };
    $scope.GetReferencesList();

    $scope.GetBusinessSourcesList = function () {
        businessSourcesService.get().then(function (response) {
            $scope.businessSourcesList = response.data.data;
        });
    };
    $scope.GetBusinessSourcesList();

    $scope.companiesList_display = false;
    $scope.GetCompaniesListWithSearchKey = function () {
        $scope.companySpinner = true;
        companiesService.getCompaniesListWithSearchKey($scope.reservationObj.company_name).then(function (response) {
            $scope.companySpinner = false;
            if (response.data.data.length > 0) {
                $scope.companiesList = response.data.data;
                $scope.companiesList_display = true;
            } else {
                $scope.companiesList_display = false;
            }
        });
    };

    $scope.companiesListClick = function (obj) {
        $scope.reservationObj.companies_id = obj.id;
        $scope.reservationObj.company_name = obj.company_name;
        $scope.reservationObj.door_no = obj.door_no;
        $scope.reservationObj.street_name = obj.street_name;
        $scope.reservationObj.location = obj.location_name;
        $scope.reservationObj.city = obj.city_name;
        $scope.reservationObj.district = obj.district_name;
        $scope.reservationObj.state = obj.state_name;
        $scope.reservationObj.zipcode = obj.zipcode;
        $scope.reservationObj.countries_id = obj.countries_id;
        $scope.reservationObj.payer_gst_number = obj.tax_identity_number;
        $scope.companiesList_display = false;
    }

    $scope.ShowCompanyAddForm = function () {
        angular.element("#company-add").modal({
            backdrop: 'static',
            keyboard: false
        });
    };

    $scope.agentsList_display = false;
    $scope.GetAgentsListWithSearchKey = function () {
        $scope.agentSpinner = true;
        if ($scope.reservationObj.agent_mobile === '') {
            $scope.reservationObj.agent_commission = "";
        }
        agentsService.getAgentsListWithSearchKey($scope.reservationObj.agent_mobile).then(function (response) {
            $scope.agentSpinner = false;
            if (response.data.data.length > 0) {
                $scope.agentsList = response.data.data;
                $scope.agentsList_display = true;
            } else {
                $scope.agentsList_display = false;
            }
        });
    };

    $scope.agentsListClick = function (obj) {
        $scope.reservationObj.agent_mobile = obj.mobile;
        $scope.reservationObj.agent_commission = obj.commission;
        $scope.agentsList_display = false;
    };


    $scope.ShowAgentAddForm = function () {
        initializeAgentObject();
        angular.element("#agent-modal-popup").modal({
            backdrop: 'static',
            keyboard: false
        });
    };


    $scope.is_searched = false;

    $scope.getStayAvailability = function () {
        $scope.check_in_error_message = "";
        $scope.stayAvailabilityList = [];
        if ($scope.reservationObj.check_in_date === '') {
            $scope.check_in_error_message = "Please choose check-in date";
        } else if ($scope.reservationObj.check_in_time === '') {
            $scope.check_in_error_message = "Please choose check-in time";
        } else if ($scope.reservationObj.duration === '') {
            $scope.check_in_error_message = "Please choose duration";
        } else {
            $scope.availabilitySearchObj.centers_id = $scope.reservationObj.centers_id;
            $scope.availabilitySearchObj.availability_type = 1;
            $scope.availabilitySearchObj.staying_types_id = $scope.reservationObj.staying_types_id;
            $scope.availabilitySearchObj.check_in_date = $scope.reservationObj.check_in_date;
            $scope.availabilitySearchObj.check_in_time = $scope.reservationObj.check_in_time;
            $scope.availabilitySearchObj.duration = $scope.reservationObj.duration;
            $scope.availabilitySearchObj.persons = 1;
            $scope.availabilitySearchObj.is_past_booking = $scope.reservationObj.is_past_booking;
            $scope.spinny(true);
            createReservationService.getStayAvailabilityByCentersId($scope.availabilitySearchObj).then(function (response) {
                $scope.spinny(false);
                if (response.data.err_code === "valid") {
                    $scope.GetTaxSlabsList();
                    $scope.noty('success', response.data.title, response.data.message);
                    $scope.reservationObj.centers_id = $scope.availabilitySearchObj.centers_id;
                    $scope.reservationObj.duration_type = $scope.availabilitySearchObj.availability_type;
                    initializeAvailabilitySearchObject();
                    $scope.reservationObj.booking_items = [];
                    $scope.UpdatePaymentInfo();
                    $scope.stayAvailabilityList = response.data.data;
                    $scope.is_searched = true;
                } else if (response.data.err_code === "invalid_form") {
                    $scope.check_in_error_message = response.data.message;
                } else if (response.data.err_code === "invalid") {
                    //$scope.error_message = response.data.message;
                    if (angular.isDefined(response.data.err_type)) {
                        $scope.unavailabilityObjMessage = response.data.message;
                        angular.element("#bookingFormMessage").modal("show");
                    } else {
                        $scope.noty('invalid', response.data.title, response.data.message);
                    }
                }
            });
        }
    }

    $scope.GetTaxSlabsList = function () {
        $scope.taxFilterObj.centers_id = $scope.reservationObj.centers_id;
        $scope.taxFilterObj.check_in_date = $scope.reservationObj.check_in_date;
        $scope.taxFilterObj.check_in_time = $scope.reservationObj.check_in_time;
        $scope.taxFilterObj.duration = $scope.reservationObj.duration;
        $scope.spinny(true);
        taxSlabsService.getByCentersId($scope.taxFilterObj).then(function (response) {
            $scope.spinny(false);
            if (response.data.err_code === "valid") {
                $scope.taxesList = response.data.data;
                $scope.UpdatePaymentInfo();
            } else if (response.data.err_code === "invalid_form") {
                //$scope.error_message = response.data.message;
            } else if (response.data.err_code === "invalid") {
                //$scope.error_message = response.data.message;
            }
        });
    };

    $scope.ShowPaymentForm = function () {
        $scope.error_message = "";
        if (angular.element("#createReservationForm").valid()) {
            if ($scope.reservationObj.booking_items.length === 0) {
                $scope.noty("warning", "Invalid booking information", "To create booking you should require select items");
                return;
            }

            $scope.SaveOrUpdateActivityTime(5);
            angular.element("#payment").modal({
                backdrop: 'static',
                keyboard: false
            });
        }
    };

    $scope.SubmitReservationForm = function () {
        var minimum_payment_amount = Math.round((($scope.reservationObj.payment_info.amount * $scope.minimum_payment_percentage) / 100));
        if (($scope.user_role_id != 2) && ($scope.user_role_id != 12) && $scope.reservationObj.payment_info.payments_total < minimum_payment_amount) {
            $scope.noty('warning', "Required", "Please pay minimum " + minimum_payment_amount)
            return false;
        }

        $scope.error_message = "";
        $scope.validation_triggered = 1;
        if (angular.element("#createReservationForm").valid()) {
            if ($scope.reservationObj.booking_items.length === 0) {
                $scope.noty("warning", "Invalid booking information", "To create booking you should require select items");
                return;
            } else {
                $scope.spinny(true);
                createReservationService.addQuickReservation($scope.reservationObj).then(function (response) {
                    $scope.spinny(false);
                    if (response.data.err_code === "valid") {
                        $scope.noty('success', response.data.title, response.data.message);
                        $scope.spinny(true);
                        initializeObject();
                        location.href = baseurl + "reservations/quick_view?booking_id=" + response.data.data;
                    } else if (response.data.err_code === "invalid_form") {
                        $scope.error_message = response.data.message;
                    } else if (response.data.err_code === "invalid") {
                        $scope.error_message = response.data.message;
                        if (angular.isDefined(response.data.err_code)) {
                            if (response.data.err_identity == "STOCK_NOT_AVAILABLE") {
                                $scope.error_message = "";
                                $scope.noty('warning', response.data.title, response.data.message);
                                angular.element("#payment").modal("hide");
                            }
                        }
                    }
                });
            }
        }
    };


    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);


    $scope.updateItemQty = function () {
        $scope.reservationObj.booking_items = [];
        //$scope.reservationObj.booking_items[booking_items_count] = angular.copy($scope.tempBookingObjStructure);
        var p = 0;
        for (var i = 0; i < $scope.stayAvailabilityList.length; i++) {
            if ($scope.stayAvailabilityList[i].selectedQty > 0) {
                $scope.reservationObj.booking_items[p] = angular.copy($scope.tempBookingObjStructure);

                $scope.reservationObj.booking_items[p].stay_category_name = $scope.stayAvailabilityList[i].category_name;
                $scope.reservationObj.booking_items[p].staying_type_categories_id = $scope.stayAvailabilityList[i].id;

                console.log($scope.reservationObj.booking_items);
                for (var j = 0; j < $scope.stayAvailabilityList[i].selectedQty; j++) {
                    var tempObj = angular.copy($scope.tempItemObjStructure);
                    tempObj.staying_type_categories_id = $scope.stayAvailabilityList[i].id;

                    console.log(tempObj.category_name);
                    if ($scope.stayAvailabilityList[i].no_of_persons_allowed_per_item > 1) {
                        tempObj.sub_items = [];
                        for (var k = 1; k < $scope.stayAvailabilityList[i].no_of_persons_allowed_per_item; k++) {
                            tempObj.sub_items.push(angular.copy($scope.tempItemObjStructure));
                        }
                    }
                    $scope.reservationObj.booking_items[p].items.push(angular.copy(tempObj));
                }
                p++;
            }
        }
        console.log($scope.reservationObj.booking_items);
        $scope.UpdatePaymentInfo();
    };


    $scope.PrepareCart = function () {
        for (var i = 0, l = $scope.reservationObj.booking_items.length; i < l; i++) {
            $scope.reservationObj.booking_items[i].total_amount = 0;
            for (var j = 0, jl = $scope.reservationObj.booking_items[i].items.length; j < jl; j++) {
                $scope.reservationObj.booking_items[i].total_amount += $scope.reservationObj.booking_items[i].items.unit_price;
            }
        }
    };

    function initializePaymentObj() {
        if (!$scope.amount) {
            $scope.amount = "";
        }

        $scope.payment_method = "";
        $scope.transaction_number = "";
        $scope.card_number = "";
        $scope.cheque_number = "";
        $scope.bank_account_number = "";
        $scope.date = "";
        $scope.remarks = "";
    }
    initializePaymentObj();

    $scope.ValidateAmount = function (amount, max_amount) {
        if (amount > max_amount) {
            $scope.amount = max_amount;
        }
    };

    $scope.AddPaymentRow = function () {
        if (($scope.amount <= 0) || ($scope.amount === '')) {
            $scope.noty('warning', "Required", "Please enter valid amount")
            return false;
        }
        if (!angular.isDefined($scope.payment_method)) {
            $scope.noty('warning', "Required", "Please choose payment method")
            return false;
        }
        if ($scope.payment_method === "") {
            $scope.noty('warning', "Required", "Please choose payment method")
            return false;
        }

        var minimum_payment_amount = Math.round((($scope.reservationObj.payment_info.amount * $scope.minimum_payment_percentage) / 100));
        if (($scope.user_role_id != 2) && ($scope.user_role_id != 12) && $scope.reservationObj.payment_info.payments_total < minimum_payment_amount) {
            if ($scope.amount < minimum_payment_amount) {
                $scope.noty('warning', "Required", "Please pay minimum " + minimum_payment_amount)
                return false;
            }
        }

        if ($scope.payment_method === "Credit Card") {
            if ($scope.transaction_number === "") {
                $scope.noty('warning', "Required", "Please enter Card Transaction Number")
                return false;
            } else if ($scope.card_number === "") {
                $scope.noty('warning', "Required", "Please enter Card Number")
                return false;
            }
        }

        if ($scope.payment_method === "Cheque") {
            if ($scope.cheque_number === "") {
                $scope.noty('warning', "Required", "Please enter Cheque Number")
                return false;
            } else if ($scope.date === "") {
                $scope.noty('warning', "Required", "Please enter Date")
                return false;
            }
        }

        if ($scope.payment_method === "Bank Transfer") {
            if ($scope.bank_account_number === "") {
                $scope.noty('warning', "Required", "Please enter Bank Account Number")
                return false;
            } else if ($scope.date === "") {
                $scope.noty('warning', "Required", "Please enter Date")
                return false;
            }
        }

        if ($scope.payment_method === "Phonepay" || $scope.payment_method === "Paytm") {
            if ($scope.transaction_number === "") {
                $scope.noty('warning', "Required", "Please enter Transaction Number")
                return false;
            } else if ($scope.date === "") {
                $scope.noty('warning', "Required", "Please enter Date")
                return false;
            }
        }

        $scope.reservationObj.payment_info.payments_list.push({
            amount: $scope.amount,
            payment_method: $scope.payment_method,
            transaction_number: $scope.transaction_number,
            card_number: $scope.card_number,
            cheque_number: $scope.cheque_number,
            bank_account_number: $scope.bank_account_number,
            date: $scope.date,
            remarks: $scope.remarks
        });
        $scope.UpdatePaymentInfo();
        initializePaymentObj();
        setTimeout(function () {
            $scope.amount = angular.copy($scope.reservationObj.payment_info.amount_due);
        }, 1500);
    };


    $scope.RemovePaymentItem = function (pos) {
        var newDataList = [];
        for (var i = 0, l = $scope.reservationObj.payment_info.payments_list.length; i < l; i++) {
            if (i !== pos) {
                newDataList.push($scope.reservationObj.payment_info.payments_list[i]);
            }
        }
        $scope.reservationObj.payment_info.payments_list = newDataList;
        $scope.UpdatePaymentInfo();
    };

    $scope.UpdateDiscount = function () {
        if ($scope.reservationObj.payment_info.amount > 0) {
            if (($scope.reservationObj.payment_info.discount_type != '') && ($scope.reservationObj.payment_info.discount_value > -1)) {
                if ($scope.reservationObj.payment_info.discount_value == 0) {
                    $scope.discount_text = "";
                }
                if ($scope.reservationObj.payment_info.discount_type == 'flat') {
                    if ($scope.reservationObj.payment_info.amount < $scope.reservationObj.payment_info.discount_value) {
                        $scope.noty('warning', 'Alert', 'Discount amount should not be greater than due amount');
                        return false;
                    }
                    if ($scope.reservationObj.payment_info.discount_value <= $scope.reservationObj.payment_info.amount) {
                        $scope.reservationObj.payment_info.discount_amount = $scope.reservationObj.payment_info.discount_value;
                        $scope.discount_text = '<i class="fa fa-rupee-sign" aria-hidden="true"></i> ' + $scope.reservationObj.payment_info.discount_value;
                    } else {
                        $scope.reservationObj.payment_info.discount_value = 0;
                        $scope.noty('warning', 'Alert', 'Discount amount should not be greater than sub total');
                        return;
                    }
                } else if ($scope.reservationObj.payment_info.discount_type == 'percentage') {
                    if ($scope.reservationObj.payment_info.discount_value <= 100) {
                        $discount_amount = (($scope.reservationObj.payment_info.amount * $scope.reservationObj.payment_info.discount_value) / 100);
                        $due_amount = $scope.reservationObj.payment_info.amount_due;
                        /*if ($due_amount < $discount_amount) {
                         // $scope.reservationObj.payment_info.discount_value = 0;
                         $scope.noty('warning', 'Alert', 'Discount amount should not be greater than due amount');
                         return false;
                         }*/
                        $scope.reservationObj.payment_info.discount_amount = $discount_amount;
                        $scope.discount_text = $scope.reservationObj.payment_info.discount_value + ' <i class="fa fa-percent" aria-hidden="true"></i> ';
                    } else {
                        $scope.reservationObj.payment_info.discount_value = 0;
                        $scope.noty('warning', 'Alert', 'Discount percent should not be greater than 100%');
                        return;
                    }
                }
                $scope.UpdatePaymentInfo();
                $scope.noty('success', 'Success', 'Discount applied successfully');
                angular.element("#modal-discount").modal("hide");
            } else if ($scope.reservationObj.payment_info.discount_type == '') {
//                /$scope.noty('warning', 'Alert', 'Please choose discount type');
            } else {
                $scope.noty('warning', 'Alert', 'Please enter discount value');
            }
        } else {
            $scope.noty('warning', 'Alert', 'Please choose atleast one item');
            angular.element("#modal-discount").modal("hide");
        }
    };
    $scope.ResetDiscount = function () {
        $scope.reservationObj.payment_info.discount_amount = 0;
        $scope.reservationObj.payment_info.discount_value = 0;
        $scope.reservationObj.payment_info.discount_type = '';
        $scope.discount_text = '';
        $scope.UpdatePaymentInfo();
        angular.element("#modal-discount").modal("hide");
    };

    $scope.UpdatePaymentInfo = function () {
        $scope.reservationObj.persons = 0;

        $scope.reservationObj.payment_info.amount = 0;
        $scope.reservationObj.payment_info.sub_total = 0;
        $scope.reservationObj.payment_info.grand_total = 0;
        $scope.reservationObj.payment_info.payments_total = 0;
        $scope.reservationObj.payment_info.applied_tax_amount = 0;
        $scope.reservationObj.payment_info.applied_taxes = [];
        if ($scope.reservationObj.booking_items.length > 0) {
            for (var i = 0; i < $scope.reservationObj.booking_items.length; i++) {
                $scope.reservationObj.persons += $scope.reservationObj.booking_items[i].items.length;
                if ($scope.reservationObj.booking_items[i] !== null) {
                    for (var j = 0; j < $scope.reservationObj.booking_items[i].items.length; j++) {
                        for (var k = 0; k < $scope.stayAvailabilityList.length; k++) {
                            if ($scope.stayAvailabilityList[k].id == $scope.reservationObj.booking_items[i].items[j].staying_type_categories_id) {
                                $scope.reservationObj.payment_info.amount += parseFloat($scope.stayAvailabilityList[k].price);
                            }
                        }
                    }
                    for (var k = 0; k < $scope.stayAvailabilityList.length; k++) {
                        if ($scope.stayAvailabilityList[k].id == $scope.reservationObj.booking_items[i].staying_type_categories_id) {
                            $scope.reservationObj.booking_items[i].total_amount = $scope.reservationObj.booking_items[i].items.length * parseFloat($scope.stayAvailabilityList[k].price);
                        }
                    }
                }
            }
            $scope.reservationObj.payment_info.sub_total = (($scope.reservationObj.payment_info.amount - $scope.reservationObj.payment_info.discount_amount));
            $scope.reservationObj.payment_info.grand_total = $scope.reservationObj.payment_info.sub_total;
            //Fetching taxs script starts here
            var arr = [];
            for (var i = 0; i < $scope.taxesList.length; i++) {
                if ($scope.reservationObj.payment_info.sub_total > $scope.taxesList[i].tax_apply_above) {
                    var applied_tax_amount = 0;
                    if ($scope.taxesList[i].tax_type === 'Percentage') {
                        applied_tax_amount = ($scope.reservationObj.payment_info.sub_total * $scope.taxesList[i].tax_amount) / 100;
                    } else if ($scope.taxesList[i].tax_type === 'Flat Amount') {
                        applied_tax_amount = $scope.taxesList[i].tax_amount;
                    }
                    $scope.taxTemp = {
                        tax_name: $scope.taxesList[i].tax_name,
                        tax_amount: parseFloat($scope.taxesList[i].tax_amount),
                        tax_calculation_type: $scope.taxesList[i].tax_type,
                        applied_tax_amount: applied_tax_amount
                    };
                    arr.push($scope.taxTemp);
                    $scope.reservationObj.payment_info.grand_total += parseFloat(applied_tax_amount);
                    $scope.reservationObj.payment_info.applied_tax_amount += parseFloat(applied_tax_amount);
                }
            }
            $scope.reservationObj.payment_info.applied_taxes = arr;
            //Fetching taxs script ends here
            //Calculating payments_total starts here
            for (var i = 0; i < $scope.reservationObj.payment_info.payments_list.length; i++) {
                $scope.reservationObj.payment_info.payments_total += parseFloat($scope.reservationObj.payment_info.payments_list[i].amount);
            }
            $scope.reservationObj.payment_info.payments_total = $scope.reservationObj.payment_info.payments_total;

            $scope.reservationObj.payment_info.amount_due = Math.round($scope.reservationObj.payment_info.grand_total - $scope.reservationObj.payment_info.payments_total);
            $scope.reservationObj.payment_info.grand_total = Math.round($scope.reservationObj.payment_info.grand_total);

            $scope.amount = angular.copy($scope.reservationObj.payment_info.amount_due);
            $scope.reservationObj.payment_info.new_items = 0;
            for (var i = 0; i < $scope.reservationObj.payment_info.payments_list.length; i++) {
                if (!angular.isDefined($scope.reservationObj.payment_info.payments_list[i].id)) {
                    $scope.reservationObj.payment_info.new_items++;
                }
            }
            console.log($scope.reservationObj.payment_info.payments_list);
            //Calculating payments_total ends here
        }
    };


    var temp_center_id = "";
    $scope.$watch("reservationObj.centers_id", function (newValue, oldValue) {
        temp_center_id = oldValue;
    });

    $scope.CheckIsIRCTCCenter = function () {
        $scope.is_irctc_center = false;
        $scope.reservationObj.ticket_type = "";
        $scope.reservationObj.ticket_number = "";
        $scope.reservationObj.ticket_attachment = "";
        $scope.reservationObj.pnr_info = "";

        console.log($scope.reservationObj.centers_id);
        if (angular.isDefined($scope.centersList)) {
            for (var i = 0; i < $scope.centersList.length; i++) {
                if ($scope.centersList[i].id == $scope.reservationObj.centers_id && $scope.centersList[i].is_irctc_based_center == 1) {
                    $scope.is_irctc_center = true;
                }
            }
        }

    };

    $scope.UpdateCenterSession = function (users_id, centers_id) {
        if (centers_id == null) {
            return;
        }
        /* if ($scope.reservationObj.booking_items.length > 0) {
         if (!confirm("Your booking already in process, if you change the center data will be reset")) {
         $scope.reservationObj.centers_id = temp_center_id;
         return false;
         }
         }*/
        $scope.spinny(true);
        centersService.updateCenterSelection(users_id, centers_id).then(function (response) {
            $scope.spinny(false);
            if (response.data.err_code === "valid") {
                $scope.noty('success', response.data.title, response.data.message);
                $scope.GetSessionCenterId();
                $scope.error_message = "";
                initializeAvailabilitySearchObject();
                //Search data not reset when i changed center
                /*$scope.reservationObj.check_in_date = "";
                 $scope.reservationObj.check_in_time = "";
                 $scope.reservationObj.duration = "";
                 $scope.reservationObj.persons = "";*/
                $scope.reservationObj.booking_items = [];
                $scope.stayAvailabilityList = [];
                $scope.UpdatePaymentInfo();
            } else if (response.data.err_code === "invalid_form") {
                $scope.noty('warning', response.data.title, response.data.message);
            } else if (response.data.err_code === "invalid") {
                $scope.noty('warning', response.data.title, response.data.message);
            }
        });
    };

    $scope.ApplyIdentiyNumberValidation = function (sub_item) {
        sub_item.identity_number = "";
        if (sub_item.identity_type === "Aadhaar Card") {
            sub_item.max_length = 12;
            sub_item.min_length = 12;
        } else if (sub_item.identity_type === "Voter ID") {
            sub_item.max_length = 10;
            sub_item.min_length = 5;
        } else if (sub_item.identity_type === "PAN Card") {
            sub_item.max_length = 10;
            sub_item.min_length = 10;
        } else if (sub_item.identity_type === "Driving License") {
            sub_item.max_length = 20;
            sub_item.min_length = 9;
        } else if (sub_item.identity_type === "Passport") {
            sub_item.max_length = 15;
            sub_item.min_length = 8;
        } else if (sub_item.identity_type === "Other") {
            sub_item.max_length = 32;
            sub_item.min_length = 1;
        }
    };

    $scope.FetchLeadDetails = function (agent_leads_id) {
        leadsService.get({'agent_leads_id': agent_leads_id}).then(function (response) {
            console.log(response.data);
            if (response.data.err_code === "valid") {
                //assign lead data to reservervation obj=ect
                $scope.reservationObj.contact_name = response.data.data[0].name;
                $scope.reservationObj.contact_mobile = response.data.data[0].mobile;
                $scope.reservationObj.check_in_date = response.data.data[0].expected_date_of_check_in;
                $scope.reservationObj.centers_id = response.data.data[0].centers_id;
                $scope.reservationObj.agent_mobile = response.data.data[0].agent_mobile;
                $scope.reservationObj.agent_leads_id = response.data.data[0].id;
                $scope.reservationObj.agent_commission = Math.trunc(response.data.data[0].agent_commission);
            } else if (response.data.err_code === "invalid_form") {
                $scope.noty('warning', response.data.title, response.data.message);
            } else if (response.data.err_code === "invalid") {
                $scope.noty('warning', response.data.title, response.data.message);
            }
        });
    };
    $scope.$watch("reservationObj.booking_items", function (newValue, oldValue) {
        if ($scope.is_searched) {
            if ($scope.reservationObj.booking_items.length > 0) {
                $scope.disableStayForm = true;
            } else {
                $scope.disableStayForm = false;
            }
        }
        if ($scope.reservationObj.booking_items.length > 0) {
            $scope.SaveOrUpdateActivityTime(4);
        }
    });

    $scope.$watch("reservationObj.duration", function (newValue, oldValue) {
        if ($scope.reservationObj.check_in_date != "" && $scope.reservationObj.check_in_time != "" && $scope.reservationObj.persons > -1) {
            $scope.getStayAvailability();
        }
    });

    $scope.PrepareActivityTrackerObj = function () {
        $scope.reservationObj.activity_tracker.push(angular.copy($scope.activityTrackingObj));
        $scope.reservationObj.activity_tracker[0].component = "Customer Information";
        $scope.reservationObj.activity_tracker[0].comment = "Customer Information taking Started, and booking process initiated";


        $scope.reservationObj.activity_tracker.push(angular.copy($scope.activityTrackingObj));
        $scope.reservationObj.activity_tracker[1].component = "Billing Information";
        $scope.reservationObj.activity_tracker[1].comment = "Billing Information taking Started";

        $scope.reservationObj.activity_tracker.push(angular.copy($scope.activityTrackingObj));
        $scope.reservationObj.activity_tracker[2].component = "CheckIn Details";
        $scope.reservationObj.activity_tracker[2].comment = "Check In information taking Started";


        $scope.reservationObj.activity_tracker.push(angular.copy($scope.activityTrackingObj));
        $scope.reservationObj.activity_tracker[3].component = "Other Information";
        $scope.reservationObj.activity_tracker[3].comment = "Other Information details asking to customer started";

        $scope.reservationObj.activity_tracker.push(angular.copy($scope.activityTrackingObj));
        $scope.reservationObj.activity_tracker[4].component = "Guests Information";
        $scope.reservationObj.activity_tracker[4].comment = "Guests Infomartion details asking";

        $scope.reservationObj.activity_tracker.push(angular.copy($scope.activityTrackingObj));
        $scope.reservationObj.activity_tracker[5].component = "Add Payment";
        $scope.reservationObj.activity_tracker[5].comment = "Payments information asking";
    };

    $scope.PrepareActivityTrackerObj();

    $scope.SaveOrUpdateActivityTime = function (indexPos, component, comment) {
        if (!angular.isDefined($scope.reservationObj.activity_tracker[indexPos])) {
            return;
        }
        if ($scope.reservationObj.activity_tracker[indexPos].activity_start_time == "") {
            $scope.reservationObj.activity_tracker[indexPos].activity_start_time = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
        }

        if (angular.isDefined(component)) {
            $scope.reservationObj.activity_tracker[indexPos].component = component;
        }

        if (angular.isDefined(comment)) {
            $scope.reservationObj.activity_tracker[indexPos].comment = comment;
        }

        //console.log($scope.reservationObj.activity_tracker);
    };

    $scope.$watch("reservationObj.contact_mobile", function (newValue) {
        if (newValue.length > 0) {
            $scope.SaveOrUpdateActivityTime(0);
        }
    });

    $scope.$watch("reservationObj.billing_to", function (newValue, oldValue) {
        if (newValue != oldValue) {
            $scope.SaveOrUpdateActivityTime(1);
        }
    });

    $scope.$watch("reservationObj.check_in_date", function (newValue) {
        if (newValue.length > 0) {
            $scope.SaveOrUpdateActivityTime(2);
        }
    });


    $scope.$watch("reservationObj.how_did_hear_about_us_id", function (newValue, oldValue) {
        if (newValue != oldValue) {
            $scope.SaveOrUpdateActivityTime(3);
        }
    });
    $scope.$watch("reservationObj.agent_mobile", function (newValue, oldValue) {
        if (newValue != oldValue) {
            $scope.SaveOrUpdateActivityTime(3);
        }
    });
    $scope.$watch("reservationObj.booking_status", function (newValue, oldValue) {
        if (newValue != oldValue) {
            $scope.SaveOrUpdateActivityTime(3);
        }
    });
    $scope.$watch("reservationObj.business_sources_id", function (newValue, oldValue) {
        if (newValue != oldValue) {
            $scope.SaveOrUpdateActivityTime(3);
        }
    });


    $scope.ShowUpdateBillingInformation = function () {
        angular.element("#billing-information-add").modal({
            backdrop: 'static',
            keyboard: false
        });
    };

    $scope.ShowUpdateOtherInformation = function () {
        angular.element("#other-information-add").modal({
            backdrop: 'static',
            keyboard: false
        });
    };

    $scope.HighlightSelectedCategory = function (selectedCategoryId) {
        $scope.selected_category = selectedCategoryId;
    };

    $scope.FetchPNRInfo = function () {
        $scope.reservationObj.valid_pnr_number = false;
        if (!angular.isDefined($scope.reservationObj.ticket_number)) {
            return;
        }
        if ($scope.reservationObj.ticket_number.length != 10) {
            return;
        }
        $scope.spinny(true);
        createReservationService.getPNRInfo($scope.reservationObj.ticket_number).then(function (response) {
            $scope.spinny(false);
            if (response.data.err_code === "valid") {
                $scope.reservationObj.valid_pnr_number = true;

                if (response.data.server_id == 2) {
                    //$scope.reservationObj.contact_name = response.data.passengerDetailsDTO.name;
                    $scope.reservationObj.pnr_info = response.data.data;
                } else if (response.data.server_id == 1) {
                    $scope.reservationObj.contact_name = response.data.passengerDetailsDTO.name;
                }

            } else if (response.data.err_code === "invalid_form") {
                $scope.noty('warning', response.data.title, response.data.message);
            } else if (response.data.err_code === "invalid") {
                $scope.noty('warning', response.data.title, response.data.message);
            }
        });
    };
});
app.filter('range', function () {
    return function (input, total) {
        total = parseInt(total);
        for (var i = 0; i < total; i++) {
            input.push(i);
        }
        return input;
    };
})