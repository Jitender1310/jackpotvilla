angular.element("#stateForm").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        state_name: {
            required: true,
            letterswithbasicpunc: true
        }
    }, messages: {
        state_name: {
            required: "State name is required",
            letterswithbasicpunc: "State name should be alphabets only"
        }
    }
});
app.controller("statesCtrl", function ($scope, statesService, $timeout, DTOptionsBuilder) {
    $scope.showLoader = false;
    function initializeObject() {
        $scope.stateObj = {
            state_name: ''
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };



    $scope.AddOrUpdateState = function () {
        $scope.error_message = "";
        $scope.s_error = {};
        if (angular.element("#stateForm").valid()) {
            $scope.showLoader = true;
            statesService.save($scope.stateObj).then(function (response) {
                $scope.showLoader = false;
                if (response.data.status === "valid") {
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.GetStatesList();
                } else if (response.data.status === "invalid_form") {
                    $scope.s_error = response.data.data;
                    $scope.error_message = response.data.message;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };
    $scope.GetStatesList = function () {
        statesService.get().then(function (response) {
            $scope.statesList = response.data.data;
        });
    };
    
    $timeout(function(){
        $scope.GetStatesList();
    }, 500);
    
    $scope.EditState = function (obj) {
        $scope.stateObj = angular.copy(obj);
    };
    $scope.DeleteState = function (item) {
        $scope.sweety(item.state_name, function () {
            statesService.delete(item.id).then(function (response) {
                if (response.data.status === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetStatesList();
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);
});