app.controller("dailyWiseArrivalReportCtrl", function ($scope, $sce, centersService, dailyWiseArrivalReportService, DTOptionsBuilder) {
    $scope.filter = {};
    //$scope.filter.page = 1;
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    var today = dd + '-' + mm + '-' + yyyy;
    var first_date_of_month = '01' + '-' + mm + '-' + yyyy;
    $scope.filter.from_date = first_date_of_month;
    $scope.filter.to_date = today;
    $scope.showLoader = false;
    $scope.ResetFilter = function () {
        $scope.filter = {
            page: 1,
            from_date: '',
            to_date: '',
            centers_id: '',
        };
        $scope.GetDailyWiseArrivalReportList();
        $scope.showLoader = true;
        $scope.filter.from_date = first_date_of_month;
        $scope.filter.to_date = today;
        $scope.showLoader = false;

    };

    $scope.centersSpinner = false;
    $scope.GetCentersList = function (users_id) {
        $scope.centersSpinner = true;
        centersService.getOnlyAssignedCenters(users_id).then(function (response) {
            $scope.centersSpinner = false;
            $scope.centersList = response.data.data;
        });
    };

    $scope.GetDailyWiseArrivalReportList = function () {
        $scope.spinny(true);
        dailyWiseArrivalReportService.get($scope.filter).then(function (response) {
            $scope.spinny(false);
            $scope.dailyWiseArrivalReportList = response.data.data;
            $scope.dailyWiseArrivalReportListPaginantion = response.data.pagination;
            if ($scope.dailyWiseArrivalReportListPaginantion.pagination.length > 0) {
                $scope.dailyWiseArrivalReportListPaginantion.pagination = $sce.trustAsHtml($scope.dailyWiseArrivalReportListPaginantion.pagination);
            }
            $scope.center_name = response.data.center_name;
        });
    };
    $scope.ExportToExcel = function () {
        window.open(baseurl + "reports/daily_wise_arrival_report/download?direct_download=true&" + $.param($scope.filter), '_blank', 'scrollbars=no,menubar=no,height=600,width=800,resizable=yes,toolbar=no,status=no');
    };


    $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filter.page = $(this).attr("data-ci-pagination-page");
        $scope.GetDailyWiseArrivalReportCtrl();
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true)
            .withOption('bPaginate', false)
            .withOption('lengthChange', false)
            .withOption('bInfo', false)
            .withOption('bSort', false);


});