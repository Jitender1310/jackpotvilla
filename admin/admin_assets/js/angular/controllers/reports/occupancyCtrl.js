app.controller("occupancyCtrl", function ($scope, $sce, centersService, stayingCategoriesInventoryService, occupancyService, DTOptionsBuilder) {
    $scope.filter = {};
    //$scope.filter.page = 1;
    $scope.showLoader = false;
    function initializeObject() {
        $scope.agentObj = {
            agent_types_id: '',
            agent_name: '',
            business_name: '',
            gst_number: '',
            business_location: '',
            mobile: '',
            email: '',
            login_required: '0',
            commission: '',
            password: ''
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };

    $scope.centersSpinner = false;
    $scope.GetCentersList = function (users_id) {
        $scope.centersSpinner = true;
        centersService.getOnlyAssignedCenters(users_id).then(function (response) {
            $scope.centersSpinner = false;
            $scope.centersList = response.data.data;
        });
    };

    $scope.GetCategoryWiseDurationBookingReport = function () {
        occupancyService.get($scope.filter).then(function (response) {
            $scope.dataList = response.data.data;
        });
    };

    $scope.GetAllStayingCategoriesList = function () {
        $scope.stayingTypesSpinner = true;
        stayingCategoriesInventoryService.getAllStayingCategories().then(function (response) {
            $scope.stayingTypesSpinner = false;
            $scope.stayingCategoriesList = response.data.data;
        });
    };

    $scope.GetAllStayingCategoriesList();

    $scope.GetStayingCategoriesListForCenter = function (centers_id, staying_types_id) {
        if (centers_id == "") {
            return $scope.GetAllStayingCategoriesList();
        }
        $scope.stayingTypesSpinner = true;
        stayingCategoriesInventoryService.getStayingTypesCategories(centers_id, staying_types_id).then(function (response) {
            $scope.stayingTypesSpinner = false;
            $scope.stayingCategoriesList = response.data.data;
        });
    };


    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', false)
            .withOption('bPaginate', false)
            .withOption('lengthChange', false)
            .withOption('bInfo', false)
            .withOption('bSort', true);

    $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filter.page = $(this).attr("data-ci-pagination-page");
        $scope.GetAgentsList();
    });

});