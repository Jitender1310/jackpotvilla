app.controller("salesReportsCtrl", function ($scope, $sce, centersService, salesReportsService, DTOptionsBuilder) {
    $scope.filter = {};
    //$scope.filter.page = 1;
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    var today = dd + '-' + mm + '-' + yyyy;
    var first_date_of_month = '01' + '-' + mm + '-' + yyyy;
    $scope.filter.from_date = first_date_of_month;
    $scope.filter.to_date = today;

    $scope.showLoader = false;
    function initializeObject() {
        $scope.filterObj = {
            fromt_date: '',
            to_date: '',
            centers_id: '',
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };


    $scope.centersSpinner = false;
    $scope.GetCentersList = function (users_id) {
        $scope.centersSpinner = true;
        centersService.getOnlyAssignedCenters(users_id).then(function (response) {
            $scope.centersSpinner = false;
            $scope.centersList = response.data.data;
        });
    };

    $scope.GetSalesList = function () {
        $scope.spinny(true);
        salesReportsService.get($scope.filter).then(function (response) {
            $scope.spinny(false);
            $scope.salesList = response.data.data.sales;
            $scope.cash_sales = response.data.cash_sales;
            $scope.credit_card_sales = response.data.credit_card_sales;
            $scope.online_sales = response.data.online_sales;
            $scope.other_sales = response.data.other_sales;
            $scope.total_sales = response.data.total_sales;
            $scope.center_name = response.data.center_name;
        });
    };
    $scope.ExportToExcel = function () {
        window.open(baseurl + "reports/sales_reports/download?direct_download=true&" + $.param($scope.filter), '_blank', 'scrollbars=no,menubar=no,height=600,width=800,resizable=yes,toolbar=no,status=no');
    };

});