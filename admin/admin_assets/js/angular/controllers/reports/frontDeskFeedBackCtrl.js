app.controller("frontDeskFeedBackCtrl", function ($scope, $sce, centersService, frontDeskFeedBackService, DTOptionsBuilder) {
    $scope.filter = {
        page: 1,
        from_date: '',
        to_date: '',
        centers_id: '',
        rate: ''
    };
    $scope.showLoader = false;
    $scope.ResetFilter = function () {
        $scope.filter = {
            page: 1,
            from_date: '',
            to_date: '',
            centers_id: '',
            rate: '',
        };
        $scope.GetFrontDeskFeedBack();
    };


    $scope.centersSpinner = false;
    $scope.GetCentersList = function (users_id) {
        $scope.centersSpinner = true;
        centersService.getOnlyAssignedCenters(users_id).then(function (response) {
            $scope.centersSpinner = false;
            $scope.centersList = response.data.data;
        });
    };

    $scope.GetFrontDeskFeedBack = function () {
        $scope.spinny(true);
        frontDeskFeedBackService.get($scope.filter).then(function (response) {
            $scope.spinny(false);
            $scope.frontDeskFeedBackList = response.data.data;
            $scope.frontDeskFeedBackPagination = response.data.pagination;
            if ($scope.frontDeskFeedBackPagination.pagination.length > 0) {
                $scope.frontDeskFeedBackPagination.pagination = $sce.trustAsHtml($scope.frontDeskFeedBackPagination.pagination);
            }
            $scope.counts = response.data.counts;
            $scope.center_name = response.data.center_name;
            $scope.fromdate = response.data.fromdate;
            $scope.todate = response.data.todate;
            $("#feedback").empty();
            Morris.Donut({
                element: 'feedback',
                colors: ['#00c292', '#03a9f3', '#fec107', '#8d9ea7', '#fb9678'],
                data: [
                    {
                        label: 'Excellent',
                        value: $scope.counts.Excellent
                    },
                    {
                        label: 'Good',
                        value: $scope.counts.Good
                    },
                    {
                        label: 'Average',
                        value: $scope.counts.Average
                    },
                    {
                        label: 'Poor',
                        value: $scope.counts.Poor
                    },
                    {
                        label: 'Very Poor',
                        value: $scope.counts.Very_Poor
                    }],
                formatter: function (y) {
                    return y;
                }
            });
        });
    };

    $scope.ExportToExcel = function () {
        window.open(baseurl + "reports/frontdesk_feedback/generate?direct_download=true&" + $.param($scope.filter), '_blank', 'scrollbars=no,menubar=no,height=600,width=800,resizable=yes,toolbar=no,status=no');
    };



    $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filter.page = $(this).attr("data-ci-pagination-page");
        $scope.GetFrontDeskFeedBack();
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(20)
            .withOption('bLengthChange', false)
            .withOption('searching', true)
            .withOption('bPaginate', false)
            .withOption('lengthChange', false)
            .withOption('bInfo', false)
            .withOption('bSort', false);


});