app.controller("restroomCleaningChecklistCtrl", function ($scope, $sce, centersService, restroomCleaningChecklistService, restroomCleaningChecklistCategoriesService, DTOptionsBuilder) {
    $scope.filter = {
        page: 1,
        from_date: '',
        to_date: '',
        centers_id: '',
    };
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    var today = dd + '-' + mm + '-' + yyyy;
    $scope.filter.from_date = today;
    $scope.filter.to_date = today;
    $scope.showLoader = false;
    $scope.ResetFilter = function () {
        center_id = $scope.filter.centers_id;
        $scope.filter = {
            page: 1,
            from_date: today,
            to_date: today,
        };
        $scope.filter.centers_id = center_id;
        $scope.GetRestRoomCleaningChecklist();
    };
    $scope.centersSpinner = false;
    $scope.GetCentersList = function (users_id) {
        $scope.centersSpinner = true;
        centersService.getOnlyAssignedCenters(users_id).then(function (response) {
            $scope.centersSpinner = false;
            $scope.centersList = response.data.data;
            $scope.filter.centers_id = $scope.centersList[0].id;
            $scope.GetStaffList($scope.filter.centers_id);
            $scope.GetRestRoomCleaningChecklist();
        });
    };

    $scope.GetStaffList = function (centers_id) {
        $scope.centersSpinner = true;
        restroomCleaningChecklistService.getStaff(centers_id).then(function (response) {
            $scope.centersSpinner = false;
            $scope.staffList = response.data.data;
        });
    };

    $scope.GetRestRoomCleaningChecklist = function () {
        $scope.spinny(true);
        restroomCleaningChecklistService.get($scope.filter).then(function (response) {
            $scope.spinny(false);
            $scope.restRoomCleaningCheckList = response.data.data;
        });
    };
    $scope.GetRestroomCleaningChecklistCategoriesList = function () {
        restroomCleaningChecklistCategoriesService.get().then(function (response) {
            $scope.restroomCleaningChecklistCategoriesList = response.data.data;
        });
    };

    $scope.ExportToExcel = function () {
        window.open(baseurl + "reports/restroom_cleaning_checklist/download?direct_download=true&" + $.param($scope.filter), '_blank', 'scrollbars=no,menubar=no,height=600,width=800,resizable=yes,toolbar=no,status=no,download=yes', );
    };



    $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filter.page = $(this).attr("data-ci-pagination-page");
        $scope.GetRestRoomCleaningChecklist();
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(20)
            .withOption('bLengthChange', false)
            .withOption('searching', true)
            .withOption('bPaginate', true)
            .withOption('lengthChange', false)
            .withOption('bInfo', false)
            .withOption('bSort', false);


});