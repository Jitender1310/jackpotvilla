app.controller("salesAndReservationsCtrl", function ($scope, $sce, centersService, salesAndReservationsService, DTOptionsBuilder) {
    $scope.filter = {
        page: 1,
        from_date: '',
        to_date: '',
        centers_id: '',
        payment_method: ''
    };
    $scope.showLoader = false;
    $scope.ResetFilter = function () {
        $scope.filter = {
            page: 1,
            from_date: '',
            to_date: '',
            centers_id: '',
            payment_method: '',
        };
        $scope.GetSalesAndReservations();
    };

    $scope.centersSpinner = false;
    $scope.GetCentersList = function (users_id) {
        $scope.centersSpinner = true;
        centersService.getOnlyAssignedCenters(users_id).then(function (response) {
            $scope.centersSpinner = false;
            $scope.centersList = response.data.data;
        });
    };

    $scope.GetSalesAndReservations = function () {
        $scope.spinny(true);
        salesAndReservationsService.get($scope.filter).then(function (response) {

            if ($scope.filter.from_date != '' || $scope.filter.to_date != '' || $scope.filter.centers_id != '' || $scope.filter.payment_method != '') {
                $scope.salesAndReservationsList = response.data.data;
                $scope.salesAndReservationsPaginantion = response.data.pagination;
                if ($scope.salesAndReservationsPaginantion.pagination.length > 0) {
                    $scope.salesAndReservationsPaginantion.pagination = $sce.trustAsHtml($scope.salesAndReservationsPaginantion.pagination);
                }
            } else {
                $scope.salesAndReservationsList = [];
                $scope.salesAndReservationsPaginantion = [];
            }
            $scope.spinny(false);
            $scope.cash_sales = response.data.cash_sales;
            $scope.credit_card_sales = response.data.credit_card_sales;
            $scope.online_sales = response.data.online_sales;
            $scope.other_sales = response.data.other_sales;
            $scope.center_name = response.data.center_name;
        });
    };

    $scope.ExportToExcel = function () {
        window.open(baseurl + "reports/sales_and_reservations/download?direct_download=true&" + $.param($scope.filter), '_blank', 'scrollbars=no,menubar=no,height=600,width=800,resizable=yes,toolbar=no,status=no');
    };



    $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filter.page = $(this).attr("data-ci-pagination-page");
        $scope.GetSalesAndReservations();
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true)
            .withOption('bPaginate', false)
            .withOption('lengthChange', false)
            .withOption('bInfo', false)
            .withOption('bSort', false);


});