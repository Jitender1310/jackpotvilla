angular.element("#rolesForm").validate({
    rules: {
        role_name: {
            required: true,
            letterswithbasicpunc: true
        }
    },
    messages: {
        role_name: {
            required: "Enter role name",
            letterswithbasicpunc: "Role name should be alphabets only"
        }
    }
});
app.controller("rolesCtrl", function ($scope, $timeout, rolesService, modulesService, DTOptionsBuilder) {
    $scope.showLoader = false;
    function initializeObject() {
        $scope.roleObj = {
            role_name: ''
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };
    $scope.AddOrUpdateRole = function () {
        $scope.error_message = "";
        $scope.r_error = {};
        if (angular.element("#roleForm").valid()) {
            $scope.spinny(true);
            rolesService.save($scope.roleObj).then(function (response) {
                $scope.spinny(false);
                if (response.data.status === "valid") {
                    $scope.noty('success', response.data.status, response.data.message);
                    initializeObject();
                    $scope.GetRoles();
                } else if (response.data.status === "invalid_form") {
                    $scope.r_error = response.data.data;
                    $scope.error_message = response.data.message;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };
    $scope.GetRoles = function () {
        rolesService.get().then(function (response) {
            $scope.rolesList = response.data.data;
        });
    };
    
    $timeout(function(){
        $scope.GetRoles();
    }, 500);
    
    $scope.EditRole = function (obj) {
        $scope.roleObj = angular.copy(obj);
    };
    $scope.DeleteRole = function (item) {
        $scope.sweety(item.role_name, function () {
            rolesService.delete(item.id).then(function (response) {
                if (response.data.status === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetRoles();
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions().withDisplayLength(10).withOption('bLengthChange', false).withOption('searching', true);
    $scope.EditPrivileges = function (role_id) {
        $scope.selected_role_id = role_id;
        angular.element("#privilegesModalPopup").modal("show");
        $scope.GetPrivileges();
    };
    $scope.GetPrivileges = function () {
        $scope.spinny(true);
        rolesService.getPrivilegesFromDB($scope.selected_role_id).then(function (response) {
            $scope.spinny(false);
            $scope.modulesList = response.data.data;
            $scope.privilegie = response.data.privilegies;
            $scope.selectedAll = response.data.privilegies.selectedAll;
        });
    };

    $scope.SavePrivileges = function () {
        rolesService.savePrivilegesToDB($scope.modulesList, $scope.selected_role_id).then(function (response) {
            $scope.noty('success', response.data.title, response.data.message);
            angular.element("#privilegesModalPopup").modal("hide");
        });
    };

    $scope.privilegie = {
        view_permission: false,
        add_permission: false,
        edit_permission: false,
        delete_permission: false,
        all_permission: false
    };

    $scope.ToggleSelectionForAllPrivileges = function () {
        var b = $scope.privilegie.all_permission ? true : false;
        for (var i = 0; i < $scope.modulesList.length; i++) {
            $scope.modulesList[i].access_rights.view_permission = b;
            $scope.modulesList[i].access_rights.add_permission = b;
            $scope.modulesList[i].access_rights.edit_permission = b;
            $scope.modulesList[i].access_rights.delete_permission = b;
            $scope.modulesList[i].access_rights.all_permission = b;
            $scope.RecursiveToggleSelectionForAll($scope.modulesList[i].sub_modules);
        }
        $scope.privilegie = {
            view_permission: b,
            add_permission: b,
            edit_permission: b,
            delete_permission: b,
            all_permission: b
        };
    };

    $scope.RecursiveToggleSelectionForAll = function (item) {
        var b = $scope.privilegie.all_permission ? true : false;
        for (var i = 0; i < item.length; i++) {
            item[i].access_rights.view_permission = b;
            item[i].access_rights.add_permission = b;
            item[i].access_rights.edit_permission = b;
            item[i].access_rights.delete_permission = b;
            item[i].access_rights.all_permission = b;
            if (item[i].sub_modules.length > 0) {
                $scope.RecursiveToggleSelectionForAll(item[i].sub_modules);
            }
        }
    };

    var cnt = 1;
    $scope.CheckIsAllRowValuesSelected = function (item) {
        console.log(cnt++);
        if (item.delete_permission
                && item.add_permission
                && item.view_permission
                && item.edit_permission) {
            item.all_permission = true;
        } else {
            $scope.privilegie.all_permission = false;
        }
    };

    $scope.ToggleSelectionForColumn = function (type) {
        cnt = 1;
        for (var i = 0; i < $scope.modulesList.length; i++) {
            $scope.modulesList[i].access_rights[type] = $scope.privilegie[type];
            $scope.CheckIsAllRowValuesSelected($scope.modulesList[i].access_rights);
            $scope.RecursiveToggleSelectionForColumn($scope.modulesList[i].sub_modules, type);
        }
    };
    $scope.RecursiveToggleSelectionForColumn = function (item, type) {
        for (var i = 0; i < item.length; i++) {
            item[i].access_rights[type] = $scope.privilegie[type];
            $scope.CheckIsAllRowValuesSelected(item[i].access_rights);
            if (item[i].sub_modules.length > 0) {
                $scope.RecursiveToggleSelectionForColumn(item[i].sub_modules, type);
            }
        }
    };

    $scope.ToggleSelectionForRow = function (item) {
        var b = item.all_permission ? true : false;
        item.view_permission = b;
        item.add_permission = b;
        item.edit_permission = b;
        item.delete_permission = b;
        item.all_permission = b;
    };



    /*$(document).on("change", function () {
     //$scope.privilegie.all_permission = true;
     for (var i = 0; i < $scope.modulesList.length; i++) {
     if ($scope.modulesList[i].access_rights.view_permission
     && $scope.modulesList[i].access_rights.add_permission
     && $scope.modulesList[i].access_rights.edit_permission
     && $scope.modulesList[i].access_rights.delete_permission) {
     $scope.modulesList[i].access_rights.all_permission = true;
     }
     check_is_selected($scope.modulesList[i].sub_modules);
     }
     
     });
     
     function check_is_selected(item) {
     for (var j = 0; j < item.length; j++) {
     if (item[j].access_rights.all_permission === false || item[j].access_rights.all_permission == 0) {
     $scope.privilegie.all_permission = false;
     break;
     }
     if (item[j].sub_modules.length > 0) {
     check_is_selected(item[j].sub_modules);
     }
     }
     } */
});