angular.element("#agentForm").validate({
    rules: {
        agent_types_id: {
            required: true
        },
        agent_name: {
            required: true
        },
        business_name: {
            required: true
        },
        gst_number: {
            //required: true,
            minlength: 15,
            maxlength: 15
        },
        business_location: {
            required: true
        },
        mobile: {
            required: true,
            digits: true
        },
        login_required: {
            required: true
        },
        commission: {
            required: true
        }
    }, messages: {
        agent_types_id: {
            required: "Agent Type is required"
        },
        agent_name: {
            required: "Agent Name is required"
        },
        business_name: {
            required: "Business Name is required"
        },
        gst_number: {
            //required: "GST number is required",
            minlength: "Invalid GST Number Length",
            maxlength: "Invalid GST Number Length"
        },
        business_location: {
            required: "Business Location is required"
        },
        mobile: {
            required: "Mobile No is required"
        },
        login_required: {
            required: "Login Required is required"
        },
        commission: {
            required: "Commission Required is required"
        }
    }
});