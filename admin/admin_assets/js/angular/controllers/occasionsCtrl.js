angular.element("#occasionForm").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        occasion_name: {
            required: true,
            letterswithbasicpunc: true
        },
        centers_id: {
            required: true
        },
        from_date: {
            required: true
        },
        to_date: {
            required: true
        }
    }, messages: {
        occasion_name: {
            required: "Occasion name is required",
            letterswithbasicpunc: "Occasion name should be alphabets only"
        },
        centers_id: {
            required: "Center is required",
        }
    }
});
app.controller("occasionsCtrl", function ($scope, occasionsService, centersService, DTOptionsBuilder, loaderService) {
    $scope.showLoader = false;
    function initializeObject() {
        $scope.occasionObj = {
            occasion_name: '',
            centers_id: '',
            from_date: '',
            to_date: '',
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };

    $scope.centersSpinner = false;
    $scope.GetCentersList = function () {
        $scope.centersSpinner = true;
        centersService.getOnlyCenterTitles().then(function (response) {
            $scope.centersList = response.data.data;
            $scope.centersSpinner = false;
        });
    };
    $scope.GetCentersList();

    $scope.AddOrUpdateOccasion = function () {
        $scope.error_message = "";
        if (angular.element("#occasionForm").valid()) {
            $scope.spinny(true);
            occasionsService.saveOccasions($scope.occasionObj).then(function (response) {
                $scope.spinny(false);
                if (response.data.err_code === "valid") {
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.GetOccasionsList();
                } else if (response.data.err_code === "invalid_form") {
                    $scope.error_message = response.data.message;
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };
    $scope.GetOccasionsList = function () {
        occasionsService.getOccasions().then(function (response) {
            $scope.occasionsList = response.data.data;
        });
    };
    $scope.EditOccasion = function (obj) {
        $scope.occasionObj = angular.copy(obj);
    };
    $scope.DeleteOccasion = function (item) {
        $scope.sweety(item.occasion_name, function () {
            occasionsService.deleteOccasions(item.id).then(function (response) {
                if (response.data.err_code === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetOccasionsList();
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);
});