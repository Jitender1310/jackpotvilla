angular.element("#avatarForm").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        title: {
            required: true
        },
        display_in: {
            required: true
        }
    }, messages: {
        title: {
            required: "Please enter avatar name"
        },
        display_in: {
            required: "Plesae select gender"
        }
    }
});
app.controller("homepageSlidersCtrl", function ($scope, $timeout, homepageSlidersService, DTOptionsBuilder) {
    $scope.showLoader = false;
    function initializeObject() {

        $scope.homepageSliderObj = {
            id: '',
            title: '',
            display_in: '',
            image: ''
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };
    $scope.GetHomepageSlidersList = function () {
        homepageSlidersService.get().then(function (response) {
            $scope.slidersList = response.data.data;
        });
    };
    
    $timeout(function(){
        $scope.GetHomepageSlidersList();
    }, 500);

    $scope.AddOrUpdateSlider = function () {
        $scope.error_message = "";
        $scope.p_error = {};
        if (angular.element("#sliderForm").valid()) {
            
            var fd = new FormData();
            var ObjectKeys = Object.keys($scope.homepageSliderObj);
            console.log(ObjectKeys);

            for (var i = 0; i < ObjectKeys.length; i++) {
                if (ObjectKeys[i] === "image") {
                    continue;
                }
                fd.append(ObjectKeys[i], $scope.homepageSliderObj[ObjectKeys[i]]);
            }

            if (document.getElementById("image") != null) {
                fd.append("image", document.getElementById("image").files[0]);
            }
            
            
            $scope.spinny(true);
            homepageSlidersService.save(fd).then(function (response) {
                $scope.spinny(false);
                if (response.data.status === "valid") {
                    angular.element("#slider_banner-modal-popup").modal("hide");
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.GetHomepageSlidersList();
                } else if (response.data.status === "invalid_form") {
                    $scope.p_error = response.data.data;
                    $scope.error_message = response.data.message;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };


    $scope.DeleteSlider = function (item) {
        $scope.sweety(item.fullname, function () {
            homepageSlidersService.delete(item.id).then(function (response) {
                if (response.data.status === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetHomepageSlidersList();
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);
    
    $scope.ShowBannerAddForm = function () {
        initializeObject();
        angular.element("#slider_banner-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };

});