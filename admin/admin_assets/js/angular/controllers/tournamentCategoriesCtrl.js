angular.element("#tournamentCategoryForm").validate({
    rules: {
        name: {
            required: true
        }
    }, messages: {
        name: {
            required: "Tournament Category is required"
        }
    }
});
app.controller("tournamentCategoriesCtrl", function ($timeout, $scope, tournamentCategoriesService, DTOptionsBuilder, loaderService) {
    $scope.showLoader = false;
    function initializeObject() {
        $scope.tournamentCategory = {
            name: '',
        };
        angular.element("input[type='file']").val('');
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };
    $scope.AddOrUpdateTournamentCategory = function () {
        $scope.error_message = "";
        if (angular.element("#tournamentCategoryForm").valid()) {
            var fd = new FormData();
            var ObjectKeys = Object.keys($scope.tournamentCategory);
//            console.log(ObjectKeys);
            for (var i = 0; i < ObjectKeys.length; i++) {
//                if (ObjectKeys[i] === "image") {
//                    continue;
//                }
                fd.append(ObjectKeys[i], $scope.tournamentCategory[ObjectKeys[i]]);
            }

            if (document.getElementById("image") != null) {
                fd.append("image", document.getElementById("image").files[0]);
            }
            $scope.spinny(true);
            tournamentCategoriesService.save(fd).then(function (response) {
                $scope.spinny(false);
                if (response.data.status === "valid") {
                    initializeObject();
                    $scope.GetTournamentCategories();
                    $scope.noty('success', response.data.title, response.data.message);
                } else if (response.data.status === "invalid_form") {
                    $scope.g_error = response.data.data;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                    $scope.noty('warning', response.data.title, response.data.message);
                }
            });
        }
    };

    $scope.GetTournamentCategories = function () {
        tournamentCategoriesService.get().then(function (response) {
            $scope.tournamentCategoires = response.data.data;
        });
    };
    $timeout(function () {
        $scope.GetTournamentCategories();
    }, 500);

    $scope.EditTournamentCategory = function (obj) {
        $scope.tournamentCategory = angular.copy(obj);
    };
    $scope.DeleteTournamentCategory = function (item) {
        $scope.sweety(item.name, function () {
            tournamentCategoriesService.delete(item.id).then(function (response) {
                if (response.data.err_code === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetTournamentCategories();
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);
});