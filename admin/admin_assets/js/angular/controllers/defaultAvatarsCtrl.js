angular.element("#avatarForm").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        title: {
            required: true
        },
        gender: {
            required: true
        }
    }, messages: {
        title: {
            required: "Please enter avatar name"
        },
        gender: {
            required: "Plesae select gender"
        }
    }
});
app.controller("defaultAvatarsCtrl", function ($scope, $timeout, defaultAvatarsService, DTOptionsBuilder) {
    $scope.showLoader = false;
    function initializeObject() {

        $scope.avatarObj = {
            id: '',
            title: '',
            gender: '',
            image: ''
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };
    $scope.GetAvatarsList = function () {
        defaultAvatarsService.get().then(function (response) {
            $scope.avatarsList = response.data.data;
        });
    };
    
    $timeout(function(){
        $scope.GetAvatarsList();
    }, 500);

    $scope.AddOrUpdateAvatar = function () {
        $scope.error_message = "";
        $scope.p_error = {};
        if (angular.element("#avatarForm").valid()) {
            
            var fd = new FormData();
            var ObjectKeys = Object.keys($scope.avatarObj);
            console.log(ObjectKeys);

            for (var i = 0; i < ObjectKeys.length; i++) {
                if (ObjectKeys[i] === "image") {
                    continue;
                }
                fd.append(ObjectKeys[i], $scope.avatarObj[ObjectKeys[i]]);
            }

            if (document.getElementById("image") != null) {
                fd.append("image", document.getElementById("image").files[0]);
            }
            
            
            $scope.spinny(true);
            defaultAvatarsService.save(fd).then(function (response) {
                $scope.spinny(false);
                if (response.data.status === "valid") {
                    angular.element("#avatar-modal-popup").modal("hide");
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.GetAvatarsList();
                } else if (response.data.status === "invalid_form") {
                    $scope.p_error = response.data.data;
                    $scope.error_message = response.data.message;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };


    $scope.DeleteAvatar = function (item) {
        $scope.sweety(item.fullname, function () {
            defaultAvatarsService.delete(item.id).then(function (response) {
                if (response.data.status === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetAvatarsList();
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);
    
    $scope.ShowAvatarAddForm = function () {
        initializeObject();
        angular.element("#avatar-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };

});