angular.element("#reportForm").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        player_mobile_number: {
            required: true
        },
        name: {
            required: true
        },
        email: {
            required: true,
            email: true
        },
        type_of_issue: {
            required: true
        },
        message: {
            required: true
        }
    }, messages: {
        player_mobile_number: {
            required: "Please Enter Mobile Number"
        },
        name: {
            required: "Please Enter Contact Name"
        },
        email: {
            required: "Please Enter Email Address",
            email: "Please Enter Valid Email Address"
        },
        type_of_issue: {
            required: "Please Choose Type of Issue"
        },
        message: {
            required: "Please Enter Report Message"
        }
    }
});
angular.element('#reportLogForm').validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        status: {
            required: true
        },
        message: {
            required: true
        }
    }, messages: {
        status: {
            required: "Please Select Status"
        },
        message:{
            required:"A Message is required."
        }
    }
});
app.controller("reportedProblemsCtrl", function ($scope, $timeout, reportedProblemsService, DTOptionsBuilder) {
    $scope.showLoader = false;
    $scope.logList = [];
    function initializeObject() {
        $scope.reportObj = {
            id: '',
            player_mobile_number:'',
            name:'',
            email:'',
            type_of_issue:'',
            message:''
        };
    }
    
    function initLogObj(){
        $scope.report_log = {
            id:0,
            status:'New',
            message:''
        };
    }

    initializeObject(); initLogObj();
    $scope.ResetForm = function () {
        initializeObject();
    };
    
    $scope.getProblemsList = function () {
         reportedProblemsService.get().then(function (response) {
             $scope.problemList = response.data.data;
         });
     };
     
     $timeout(function(){
         $scope.getProblemsList();
     },500);

    $scope.ViewProblem = function (obj) {
        $scope.logList = [];
        $scope.report_log.id = obj.id;
        $scope.report_log.status = obj.status;
        
        reportedProblemsService.getLogs(obj.id).then(function(response){
            $scope.logList = response.data.data;
        })
        
        $scope.reportObj = angular.copy(obj);
        angular.element("#view-problem-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };
    
    $scope.AddOrUpdateProblem = function () {
        $scope.error_message = "";
        $scope.g_error = {};
        if (angular.element("#reportForm").valid()) {
            $scope.spinny(true);
            reportedProblemsService.save($scope.reportObj).then(function (response) {
                $scope.spinny(false);
                if (response.data.status === "valid") {
                    angular.element("#report-modal-popup").modal("hide");
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.getProblemsList();
                } else if (response.data.status === "invalid_form") {
                    $scope.g_error = response.data.data;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                    $scope.noty('warning', response.data.title, response.data.message);
                }
            });
        }
    };

    $scope.EditProblem = function (obj) {
        $scope.reportObj = angular.copy(obj);
        angular.element("#report-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };

    $scope.DeleteProblem = function (item) {
        $scope.sweety(item.player_mobile_number, function () {
            reportedProblemsService.delete(item.id).then(function (response) {
                if (response.data.status === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.getProblemsList();
                } else if (response.data.status === "invalid_form") {
                    $scope.g_error = response.data.data;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);

    $scope.ShowProblemAddForm = function () {
        initializeObject();
        angular.element("#report-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };
    
    $scope.AddProblemLog = function(){
        $scope.error_message = "";
        $scope.g_error = {};
        if (angular.element("#reportLogForm").valid()) {
            $scope.spinny(true);
            reportedProblemsService.saveLog($scope.report_log).then(function (response) {
                $scope.spinny(false);
                if (response.data.status === "valid") {
                    angular.element("#view-problem-modal-popup").modal("hide");
                    $scope.noty('success', response.data.title, response.data.message);
                    initLogObj();
                    $scope.getProblemsList();
                } else if (response.data.status === "invalid_form") {
                    $scope.g_error = response.data.data;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                    $scope.noty('warning', response.data.title, response.data.message);
                }
            });
        }
    };
});