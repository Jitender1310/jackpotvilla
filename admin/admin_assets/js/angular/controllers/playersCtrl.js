angular.element("#playerForm").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        firstname: {
            required: true
        },
        lastname: {
            required: true
        },
        username: {
            required: true
        },
        password: {
            required: true
        },
        confirm_password: {
            required: true,
            equalTo: "#password",
        },
        designation: {
            required: true
        },
        mobile: {
            required: true,
            digits: true,
            minlength: 10,
            maxlength: 10
        },
        email: {
            required: true,
            email: true
        },
        gender: {
            required: true
        },
        date_of_birth: {
            required: true
        },
        address_line_1: {
            required: true
        },
        city: {
            required: true
        },
        states_id: {
            required: true
        },
        pin_code: {
            required: true
        },
    }, messages: {
        firstname: {
            required: "Firstname is required"
        },
        lastname: {
            required: "Lastname is required"
        },
        username: {
            required: "Username is required"
        },
        password: {
            required: "Password is required"
        },
        confirm_password: {
            required: "Confirm Password is required",
            equalTo: "Password and Confirm Password should be same"
        },
        mobile: {
            required: "Mobile is required",
            digits: "Please enter numeric only"
        },
        email: {
            required: "Email is required"
        },
        gender: {
            required: "Gender is required"
        },
        date_of_birth: {
            required: "Date of Birth is required"
        },
        address_line_1: {
            required: "Address is required"
        },
        city: {
            required: "City is required"
        },
        states_id: {
            required: "State is required"
        },
        pin_code: {
            required: "Pin code is required"
        }
    }
});
app.controller("playersCtrl", function ($timeout, $scope, playersService, $sce, statesService, DTOptionsBuilder) {
    $scope.showLoader = false;
    $scope.filter = {
        page: 1,
        search_key: '',
        from_date: '',
        to_date: ''
    };

    $scope.ResetFilter = function () {
        $scope.filter = {
            page: 1,
            search_key: '',
            from_date: '',
            to_date: ''
        };
        $scope.GetPlayersList();
    }

    function initializeObject() {

        $scope.playerObj = {
            id: '',
            username: '',
            password: '',
            confirm_password: '',
            firstname: '',
            lastname: '',
            email: '',
            mobile: '',
            city: '',
            states_id: '',
            address_line_1: '',
            address_line_2: '',
            date_of_birth: '',
            gender: ''
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };
    $scope.GetPlayersList = function () {
        $scope.spinny(true);
        playersService.get($scope.filter).then(function (response) {
            $scope.spinny(false);
            $scope.playersList = response.data.data.results;
            $scope.total_players_list = response.data.data.total_results_found;
            $scope.playersPagination = response.data.pagination;
            if ($scope.playersPagination.pagination.length > 0) {
                $scope.playersPagination.pagination = $sce.trustAsHtml($scope.playersPagination.pagination);
            }
        });
    };
    $timeout(function () {
        $scope.GetPlayersList();
    }, 500);
    $scope.GetStatesList = function () {
        statesService.get().then(function (response) {
            $scope.statesList = response.data.data;
        });
    };

    $scope.GetStatesList();

    $scope.AddOrUpdatePlayer = function () {
        $scope.error_message = "";
        $scope.p_error = {};
        if (angular.element("#playerForm").valid()) {
            $scope.spinny(true);
            playersService.save($scope.playerObj).then(function (response) {
                $scope.spinny(false);
                if (response.data.status === "valid") {
                    angular.element("#player-modal-popup").modal("hide");
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.GetPlayersList();
                } else if (response.data.status === "invalid_form") {
                    $scope.p_error = response.data.data;
                    $scope.error_message = response.data.message;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };

    $scope.EditPlayer = function (obj) {
        $scope.playerObj = angular.copy(obj);
        angular.element("#player-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };

    $scope.EditKYCInformation = function (obj) {
        $scope.editProfileObj = angular.copy(obj);
        $scope.playerObj = angular.copy(obj);
        angular.element("#editKycModal").modal({backdrop: 'static',
            keyboard: false});
    };

    $scope.updateKycDetails = function () {

        var fd = new FormData();
        var ObjectKeys = Object.keys($scope.editProfileObj);
        console.log(ObjectKeys);

        for (var i = 0; i < ObjectKeys.length; i++) {
            if (ObjectKeys[i] === "address_proof" || ObjectKeys[i] === "pan_card") {
                continue;
            }
            if (typeof $scope.editProfileObj[ObjectKeys[i]] === 'object' || $scope.editProfileObj[ObjectKeys[i]].constructor === Array) {
                fd.append(ObjectKeys[i], JSON.stringify($scope.editProfileObj[ObjectKeys[i]]));
            } else {
                fd.append(ObjectKeys[i], $scope.editProfileObj[ObjectKeys[i]]);
            }
        }

        if (document.getElementById("address_proof") != null) {
            fd.append("address_proof", document.getElementById("address_proof").files[0]);
        }

        if (document.getElementById("pan_card") != null) {
            fd.append("pan_card", document.getElementById("pan_card").files[0]);
        }

        $scope.kyc_error = {};
        $scope.spinny(true);
        playersService.update_kyc(fd).then(function (response) {
            $scope.spinny(false);
            if (response.data.status === "valid") {
                angular.element("#editKycModal").modal("hide");
                $scope.noty('success', response.data.title, response.data.message);
                $scope.GetPlayersList();
            } else if (response.data.status === "invalid_form") {
                $scope.kyc_error = response.data.data;
            } else if (response.data.status === "invalid") {
                $scope.noty('warning', response.data.title, response.data.message);
            }
        });
    };

    $scope.DeletePlayer = function (item) {
        $scope.sweety(item.fullname, function () {
            playersService.delete(item.id).then(function (response) {
                if (response.data.status === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetPlayersList();
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', false)
            .withOption('bPaginate', false)
            .withOption('lengthChange', false)
            .withOption('bInfo', false)
            .withOption('bSort', false);

    $scope.ShowPlayerAddForm = function () {
        initializeObject();
        angular.element("#player-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };

    $scope.ViewPlayer = function (obj) {
        $scope.playerObj = angular.copy(obj);
        angular.element("#view-player-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };

    $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filter.page = $(this).attr("data-ci-pagination-page");
        $scope.GetPlayersList();
    });
});