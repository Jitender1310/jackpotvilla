app.controller("dashboardCtrl", function ($timeout, $scope, dashboardService, $sce, DTOptionsBuilder) {
    $scope.filter = {
        day: 'Today'
    };

    $scope.GetDashboardDetails = function () {
        dashboardService.get($scope.filter).then(function (response) {
            $scope.stats = response.data.data;

            if ($("#players").length > 0) {
                Morris.Bar({
                    element: 'players',
                    data: [{
                            option: $scope.stats.players[6].month,
                            value: $scope.stats.players[6].count
                        }, {
                            option: $scope.stats.players[5].month,
                            value: $scope.stats.players[5].count
                        }, {
                            option: $scope.stats.players[4].month,
                            value: $scope.stats.players[4].count
                        }, {
                            option: $scope.stats.players[3].month,
                            value: $scope.stats.players[3].count
                        }, {
                            option: $scope.stats.players[2].month,
                            value: $scope.stats.players[2].count
                        }, {
                            option: $scope.stats.players[1].month,
                            value: $scope.stats.players[1].count
                        }],
                    xkey: 'option',
                    ykeys: ['value'],
                    labels: ['Members'],
                    barColors: ["#00c292"],
                    barRatio: 0.4,
                    hideHover: 'auto'
                });
            }
        });
    };
    $scope.GetGames = function () {
        dashboardService.get_games($scope.filter).then(function (response) {
            $scope.games = response.data.data;
            $("#games").empty();
            Morris.Donut({
                element: 'games',
//                colors: ['#00c292', '#03a9f3', '#fec107', '#8d9ea7', '#fb9678'],
                colors: ['#00c292', '#03a9f3', '#fec107', '#8d9ea7'],
                data: [
                    {
                        label: '101 Pool Rummy',
                        value: $scope.games.counts.one_zero_one_pool_rummy
                    },
                    {
                        label: '201 Pool Rummy',
                        value: $scope.games.counts.two_zero_one_pool_rummy
                    },
                    {
                        label: 'Points Rummy',
                        value: $scope.games.counts.points_rummy
                    },
                    {
                        label: 'Deals Rummy',
                        value: $scope.games.counts.deals_rummy
                    },
                ],
                formatter: function (y) {
                    return y;
                }
            });
        });

    };


    $timeout(function () {
        $scope.GetGames();
    }, 1000);

    $timeout(function () {
        $scope.GetDashboardDetails();
    }, 1000);





    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', false)
            .withOption('bPaginate', false)
            .withOption('lengthChange', false)
            .withOption('bInfo', false)
            .withOption('bSort', false);

    $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filter.page = $(this).attr("data-ci-pagination-page");
        $scope.GetAgentsList();
    });


});