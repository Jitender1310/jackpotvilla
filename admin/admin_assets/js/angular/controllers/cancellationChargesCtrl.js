angular.element("#cancellationChargesForm").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        centers_id: {
            required: true
        },
        duration_value: {
            required: true
        },
        duration_before: {
            required: true
        },
        refund_value: {
            required: true
        }
    }, messages: {
        centers_id: {
            required: "Center is required"
        },
        duration_value: {
            required: "Duration is required"
        },
        duration_before: {
            required: "Duration Type is required"
        },
        refund_value: {
            required: "Refund percentage(%) is required"
        }
    }
});
app.controller("cancellationChargesCtrl", function ($scope, cancellationChargesService, centersService, DTOptionsBuilder, loaderService) {
    $scope.showLoader = false;
    function initializeObject() {
        $scope.cancellationChargesObj = {
            centers_id: '',
            duration_value: '',
            duration_before: 'Hours',
            refund_value: ''
        };
        $scope.cancellationChargesObj.duration_before = 'Hours';

    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
        $scope.cancellationChargesObj.duration_before = 'Hours';
    };

    $scope.GetCentersList = function () {
        centersService.get().then(function (response) {
            $scope.centersList = response.data.data;
        });
    };
    $scope.GetCentersList();


    $scope.AddOrUpdateCancellationCharges = function () {
        $scope.error_message = "";
        if (angular.element("#cancellationChargesForm").valid()) {
            $scope.showLoader = true;
            cancellationChargesService.save($scope.cancellationChargesObj).then(function (response) {
                $scope.showLoader = false;
                if (response.data.err_code === "valid") {
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.GetCancellationChargesList();
                } else if (response.data.err_code === "invalid_form") {
                    $scope.error_message = response.data.message;
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };


    $scope.GetCancellationChargesList = function () {
        cancellationChargesService.get().then(function (response) {
            $scope.cancellationChargesList = response.data.data;
        });
    };


    $scope.EditCancellationCharges = function (obj) {
        $scope.cancellationChargesObj = angular.copy(obj);
    };
    $scope.DeleteCancellationCharges = function (item) {
        $scope.sweety(item.tax_name, function () {
            cancellationChargesService.delete(item.id).then(function (response) {
                if (response.data.err_code === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetCancellationChargesList();
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);
});