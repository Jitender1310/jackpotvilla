angular.element("#updatePasswordForm").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        current_password: {
            required: true,
            minlength: 6,
            maxlength: 32
        },
        new_password: {
            required: true,
            minlength: 6,
            maxlength: 32
        },
        confirm_new_password: {
            required: true,
            equalTo: '#password'
        }
    },
    messages: {
        current_password: {
            required: "Enter your old password",
            minlength: "Please enter valid ",
            maxlength: 32
        },
        new_password: {
            required: "Enter new password",
        },
        confirm_new_password: {
            required: "Reenter the password",
            equalTo: "Password mismatch"
        }
    }
});
app.controller("changePasswordCtrl", function ($scope, changePasswordService) {


    function initializeObject() {
        $scope.passwordObj = {
            current_password: '',
            new_password: '',
            confirm_new_password: ''
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };

    $scope.UpdatePassword = function () {
        $scope.error_message = "";
        $scope.cp_error = {};
        if (angular.element("#updatePasswordForm").valid()) {
            $scope.showLoader = true;
            changePasswordService.update($scope.passwordObj).then(function (response) {
                $scope.showLoader = false;
                if (response.data.status === "valid") {
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                } else if (response.data.status === "invalid_form") {
                    $scope.cp_error = response.data.data;
                    $scope.error_message = response.data.message;
                } else if (response.data.status === "invalid") {
                    $scope.noty('error', response.data.title, response.data.message);
                    $scope.error_message = response.data.message;
                }
            });
        }
    };

});