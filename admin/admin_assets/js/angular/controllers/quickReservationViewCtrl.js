angular.element("#agentForm").validate({

    rules: {
        agent_types_id: {
            required: true
        },
        agent_name: {
            required: true
        },
        mobile: {
            required: true,
            digits: true
        },
        login_required: {
            required: true
        },
        commission: {
            required: true
        }
    }, messages: {
        agent_types_id: {
            required: "Agent Type is required"
        },
        agent_name: {
            required: "Agent Name is required"
        },
        mobile: {
            required: "Mobile No is required"
        },
        login_required: {
            required: "Login Required is required"
        },
        commission: {
            required: "Commission Required is required"
        }
    }
});
angular.element("#reservation_discount_form").validate({

    rules: {
        discount_type: {
            required: true
        },
        discount_value: {
            required: true
        },
    }, messages: {
        discount_type: {
            required: "Discount Type is required"
        },
        discount_value: {
            required: "Discount is required"
        },

    }
});
angular.element("#checkin_form").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        guest: {
            required: true
        },
        proof_type: {
            required: true
        },
        proof_number: {
            required: true
        },
        proof_image: {
            required: true
        }
    },
    messages: {
        guest: {
            required: "Guest selection is required"
        },
        proof_type: {
            required: "Proof type is required"
        },
        proof_number: {
            required: "Proof number is required"
        },
        proof_image: {
            required: "Upload proof image"
        }
    }
});
angular.element("#customer_form").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
        error.appendTo(element.closest(".custom-group"));
    },
    rules: {
        contact_mobile: {
            required: true,
            digits: true
        },
        contact_name: {
            required: true
        },
        contact_email: {
            required: true,
            email: true
        },
        alternative_mobile: {
            digits: true,
            notEqualTo: '[name="contact_mobile"]'
        }
    },
    messages: {
        contact_mobile: {
            required: "Enter customer mobile number"
        },
        contact_name: {
            required: "Enter customer name"
        },
        contact_email: {
            required: "Enter Contact email",
            email: "Enter valid email address"
        },
        alternative_mobile: {
            digits: "Enter valid mobile number",
            notEqualTo: "Enter different mobile number"
        }
    }
});

angular.element("#other_info_form").valid({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        how_did_hear_about_us_id: {
            required: false
        },
        agent_mobile: {
            required: false
        },
        agent_commission: {
            required: false
        },
        booking_status: {
            required: true
        },
        business_sources_id: {
            required: false
        }
    },
    messages: {
        booking_status: {
            required: "Please choose booking status"
        }
    }
});

angular.element("#edit_arrival_time_form").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".custom-group"));
    },
    rules: {
        check_in_date: {
            required: true
        },
        check_in_time: {
            required: true
        },
        duration: {
            required: true
        }
    },
    messages: {
        check_in_date: {
            required: "Please choose Check In Date"
        },
        check_in_time: {
            required: "Please choose Check In Time"
        },
        duration: {
            required: "Please choose Duration"
        }
    }
});
angular.element("#billing_form").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".custom-group"));
    },
    rules: {
        billing_to: {
            required: true
        },
        door_no: {
            required: true
        },
        street_name: {
            required: true
        },
        location: {
            required: true
        },
        city: {
            required: true
        },
        district: {
            required: true
        },
        state: {
            required: true
        },
        zipcode: {
            required: true
        },
        countries_id: {
            required: true
        },
        payer_gst_number: {
            minlength: 15,
            maxlength: 15
        }
    }, messages: {
        billing_to: {
            required: "Bill to is required",
        },
        door_no: {
            required: "Door No is required",
        },
        street_name: {
            required: "Street Name is required",
        },
        location: {
            required: "Location is required",
        },
        city: {
            required: "City is required",
        },
        district: {
            required: "District is required",
        },
        state: {
            required: "State is required",
        },
        zipcode: {
            required: "Zipcode is required",
        },
        countries_id: {
            required: "Countries is required",
        },
        payer_gst_number: {
            minlength: "Invalid GST Number",
            maxlength: "Invalid GST Number"
        }
    }
});
app.controller("quickReservationViewCtrl", function ($scope, reservationViewService,
        createReservationService, taxSlabsService, agentsService, countriesService, agentTypesService,
        howDidHearAboutUsService, businessSourcesService,
        companiesService, customersService, posService
        ) {

    var _video = null,
            patData = null;
    $scope.patOpts = {x: 0, y: 0, w: 25, h: 25};
    // Setup a channel to receive a video property
    // with a reference to the video element
    // See the HTML binding in main.html
    $scope.channel = {};
    $scope.webcamError = false;
    $scope.onError = function (err) {
        $scope.$apply(
                function () {
                    $scope.webcamError = err;
                }
        );
    };
    $scope.onSuccess = function () {
        // The video element contains the captured camera data
        _video = $scope.channel.video;
        $scope.$apply(function () {
            $scope.patOpts.w = _video.width;
            $scope.patOpts.h = _video.height;
            //$scope.showDemos = true;
        });
    };
    $scope.onStream = function (stream) {
        // You could do something manually with the stream.
    };
    $scope.makeSnapshot = function () {
        if (_video) {
            var patCanvas = document.querySelector('#snapshot');
            if (!patCanvas)
                return;
            patCanvas.width = _video.width;
            patCanvas.height = _video.height;
            var ctxPat = patCanvas.getContext('2d');
            var idata = getVideoData($scope.patOpts.x, $scope.patOpts.y, $scope.patOpts.w, $scope.patOpts.h);
            ctxPat.putImageData(idata, 0, 0);
            sendSnapshotToServer(patCanvas.toDataURL());
            patData = idata;
        }
    };
    /**
     * Redirect the browser to the URL given.
     * Used to download the image by passing a dataURL string
     */
    $scope.downloadSnapshot = function downloadSnapshot(dataURL) {
        window.location.href = dataURL;
    };
    var getVideoData = function getVideoData(x, y, w, h) {
        var hiddenCanvas = document.createElement('canvas');
        hiddenCanvas.width = _video.width;
        hiddenCanvas.height = _video.height;
        var ctx = hiddenCanvas.getContext('2d');
        ctx.drawImage(_video, 0, 0, _video.width, _video.height);
        return ctx.getImageData(x, y, w, h);
    };
    /**
     * This function could be used to send the image data
     * to a backend server that expects base64 encoded images.
     *
     * In this example, we simply store it in the scope for display.
     */

    $scope.ShowCheckInPopUp = function () {
        $scope.spinny(true);
        $scope.error_message = "";
        var param = {booking_ref_number: $scope.reservationObj.booking_ref_number};
        reservationViewService.checkPaymentIsPending(param).then(function (response) {
            $scope.spinny(false);
            if (response.data.err_code === "valid") {
                $scope.noty('success', response.data.title, response.data.message);
                Webcam.set({
                    width: 400,
                    height: 300,
                    image_format: 'png',
                    jpeg_quality: 90
                });
                Webcam.attach('#my_camera');
                angular.element("#modal-checkin").modal("show");
                initializeObject();
            } else if (response.data.err_code === "invalid_form") {
                $scope.error_message = response.data.message;
                $scope.noty('warning', response.data.title, response.data.message);
            } else if (response.data.err_code === "invalid") {
                $scope.error_message = response.data.message;
                $scope.noty('warning', response.data.title, response.data.message);
                $scope.ShowPaymentForm();
            }
        });
    };
    Webcam.on('error', function (err) {
        if (err == "NotReadableError: Failed to allocate videosource") {
            alert("Camera is already using in other browser/tab, please close previous browser/tab");
        } else if (err == "WebcamError: Webcam is not loaded yet") {
            angular.element("#modal-checkin").modal("hide");
            setTimeout(function () {
                $scope.ShowCheckInPopUp();
            }, 500);
        } else {
            alert(err);
        }
    });
    var sendSnapshotToServer = function sendSnapshotToServer(imgBase64) {
        $scope.snapshotData = imgBase64;
    };
    $('#modal-checkin').on('hidden.bs.modal', function () {
        $scope.enable_camera = 0;
        $scope.captured_snap = 0;
        Webcam.reset();
    });
    /*-------------------------------------*/


    //Script to capture image by vijay starts here
    $scope.TakeSnapshot = function TakeSnapshot() {

        Webcam.snap(function (data_uri) {
            $scope.snapshotData = data_uri;
            document.getElementById('results').innerHTML = '<img src="' + data_uri + '"/>';
        });
    }
//Script to capture image by vijay ends here


    $scope.reservationObj = {};
    function initializeObject() {
        $scope.taxFilterObj = {
            centers_id: 1,
            check_in_date: '',
            check_in_time: '',
            duration: ''
        };
        $scope.taxesList = [];
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };
    function initializeAgentObject() {
        $scope.agentObj = {
            agent_types_id: '',
            agent_name: '',
            mobile: '',
            email: '',
            commission: '',
            login_required: '0',
            password: ''
        };
    }
    initializeAgentObject();
    $scope.ResetAgentForm = function () {
        initializeAgentObject();
    };
    $scope.FetchCustomerData = function () {
        if ($scope.reservationObj.contact_mobile.length >= 10) {
            $scope.spinny(true);
            customersService.getCustomerDetailsWithMobile($scope.reservationObj.contact_mobile).then(function (response) {
                $scope.spinny(false);
                if (response.data.data) {
                    $scope.billingInfoObj.door_no = response.data.data.door_no;
                    $scope.billingInfoObj.street_name = response.data.data.street_name;
                    $scope.billingInfoObj.location = response.data.data.location;
                    $scope.billingInfoObj.city = response.data.data.city;
                    $scope.billingInfoObj.district = response.data.data.district;
                    $scope.billingInfoObj.state = response.data.data.state;
                    $scope.billingInfoObj.zipcode = response.data.data.zipcode;
                    $scope.billingInfoObj.countries_id = response.data.data.countries_id;
                }
            });
        }
    };
    $scope.GetCountriesList = function () {
        countriesService.get().then(function (response) {
            $scope.countriesList = response.data.data;
        });
    };
    $scope.GetCountriesList();
    $scope.UpdateCheckInformation = function () {
        $scope.error_message = "";
        if (angular.element("#checkin_form").valid()) {
            $scope.spinny(true);
            $scope.reservationObj.check_in_details.captured_image = $scope.snapshotData;
            $scope.reservationObj.check_in_details.booking_ref_number = $scope.reservationObj.booking_ref_number;
            console.log($scope.reservationObj.booking_ref_number);
            reservationViewService.updateQuickCheckInformation($scope.reservationObj.check_in_details, $scope.users_id).then(function (response) {
                $scope.spinny(false);
                if (response.data.err_code === "valid") {
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.GetReservationDetails();
                    angular.element("#modal-checkin").modal("hide");
                } else if (response.data.err_code === "invalid_form") {
                    $scope.error_message = response.data.message;
                    $scope.noty('warning', response.data.title, response.data.message);
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                    $scope.noty('warning', response.data.title, response.data.message);
                }
            });
        }
    };
    $scope.GetReservationDetails = function () {
        $scope.spinny(true);
        reservationViewService.get($scope.reservations_id).then(function (response) {
            $scope.spinny(false);
            $scope.reservationObj = response.data.data;
            $scope.reservationObj.duration = parseInt($scope.reservationObj.duration);
            $scope.paymentDetails = response.data.data.payment_info.payments_list;
            $scope.amount = angular.copy($scope.reservationObj.payment_info.amount_due);
            if ($scope.reservationObj.payment_info.discount_type == 'flat') {
                $scope.discount_text = '<i class="fa fa-rupee-sign" aria-hidden="true"></i> ' + $scope.reservationObj.payment_info.discount_value;
            } else if ($scope.reservationObj.payment_info.discount_type == 'percentage') {
                $scope.discount_text = '<i class="fa fa-percent" aria-hidden="true"></i> ' + $scope.reservationObj.payment_info.discount_value;
            }

            var customer_check_in_at = $scope.reservationObj.check_in_details.customer_checked_in_at;
            $scope.reservationObj.check_in_details = {
                captured_image: angular.copy($scope.reservationObj.check_in_details.captured_image),
                contact_name: angular.copy($scope.reservationObj.booking_items[0].items[0].person_name),
                contact_mobile: angular.copy($scope.reservationObj.booking_items[0].items[0].mobile_number),
                reservation_items_id: angular.copy($scope.reservationObj.booking_items[0].items[0].id),
                customer_checked_in_at: customer_check_in_at
            };
        });
    };
    // $scope.GetReservationDetails();

    $scope.MarkAsCheckOut = function (reservations_booking_ref_number) {
        $scope.checkoutObj = {};
        $scope.checkoutObj.booking_ref_number = reservations_booking_ref_number;
        $scope.checkoutObj.customer_checked_out_at = $scope.reservationObj.check_out_details.customer_checked_out_at;
        $scope.checkoutObj.users_id = $scope.users_id;
        angular.element("#modal-checkout").modal("hide");
        $scope.spinny(true);
        reservationViewService.updateMarkAsCheckout($scope.checkoutObj, $scope.users_id).then(function (response) {
            $scope.spinny(false);
            if (response.data.err_code === "valid") {
                $scope.noty('success', response.data.title, response.data.message);
                initializeObject();
                $scope.GetReservationDetails();
                angular.element("#modal-checkout").modal("hide");
            } else if (response.data.err_code === "invalid_form") {
                $scope.error_message = response.data.message;
            } else if (response.data.err_code === "invalid") {
                $scope.noty('warning', response.data.title, response.data.message);
            }
        });
    };
    $scope.ShowPaymentForm = function () {
        if ($scope.reservationObj.booking_items.length === 0) {
            $scope.noty("warning", "Invalid booking information", "To create booking you should require select items");
            return;
        }
        angular.element("#payment").modal({backdrop: 'static',
            keyboard: false});
        $scope.GetTaxSlabsList();
    };
    function initializePaymentObj() {
        if (!$scope.amount) {
            $scope.amount = "";
        }
        $scope.payment_method = "";
        $scope.transaction_number = "";
        $scope.card_number = "";
        $scope.cheque_number = "";
        $scope.bank_account_number = "";
        $scope.date = "";
        $scope.remarks = "";
    }
    initializePaymentObj();
    $scope.UpdatePaidAmountInfoForReservation = function () {
        $scope.spinny(true);
        reservationViewService.updatePaymentDetails($scope.reservationObj, $scope.users_id).then(function (response) {
            if (response.data.err_code === "valid") {
                $scope.noty('success', response.data.title, response.data.message);
                initializeObject();
                $scope.GetReservationDetails();
                angular.element("#payment").modal("hide");
                $scope.spinny(false);
            } else if (response.data.err_code === "invalid_form") {
                $scope.error_message = response.data.message;
            } else if (response.data.err_code === "invalid") {
                $scope.error_message = response.data.message;
            }
        });
    };
    $scope.ValidateAmount = function (amount, max_amount) {
        if (amount > max_amount) {
            $scope.amount = max_amount;
        }
    }

    $scope.RunPaymentFieldValidation = function () {
        console.log($scope.payment_method);
        if (($scope.amount <= 0) || ($scope.amount === '')) {
            $scope.noty('warning', "Required", "Please enter amount")
            return false;
        }
        if (!angular.isDefined($scope.payment_method)) {
            $scope.noty('warning', "Required", "Please choose payment method")
            return false;
        }
        if ($scope.payment_method === "") {
            $scope.noty('warning', "Required", "Please choose payment method")
            return false;
        }

        if ($scope.payment_method === "Credit Card") {
            if ($scope.transaction_number === "") {
                $scope.noty('warning', "Required", "Please enter Card Transaction Number")
                return false;
            } else if ($scope.card_number === "") {
                $scope.noty('warning', "Required", "Please enter Card Number")
                return false;
            }
        }

        if ($scope.payment_method === "Cheque") {
            if ($scope.cheque_number === "") {
                $scope.noty('warning', "Required", "Please enter Cheque Number")
                return false;
            } else if ($scope.date === "") {
                $scope.noty('warning', "Required", "Please enter Date")
                return false;
            }
        }

        if ($scope.payment_method === "Bank Transfer") {
            if ($scope.bank_account_number === "") {
                $scope.noty('warning', "Required", "Please enter Bank Account Number")
                return false;
            } else if ($scope.date === "") {
                $scope.noty('warning', "Required", "Please enter Date")
                return false;
            }
        }

        if ($scope.payment_method === "Phonepay" || $scope.payment_method === "Paytm") {
            if ($scope.transaction_number === "") {
                $scope.noty('warning', "Required", "Please enter Transaction Number")
                return false;
            } else if ($scope.date === "") {
                $scope.noty('warning', "Required", "Please enter Date")
                return false;
            }
        }

        if ($scope.payment_method === "Other") {
            if ($scope.remarks === "") {
                $scope.noty('warning', "Required", "Please enter remarks")
                return false;
            }
        }
    }

    $scope.AddPaymentRow = function () {
        if ($scope.RunPaymentFieldValidation() == false) {
            return false;
        }

        $scope.spinny(true);
        $scope.reservationObj.payment_info.payments_list.push({
            amount: parseFloat($scope.amount),
            payment_method: $scope.payment_method,
            transaction_number: $scope.transaction_number,
            card_number: $scope.card_number,
            cheque_number: $scope.cheque_number,
            bank_account_number: $scope.bank_account_number,
            date: $scope.date,
            remarks: $scope.remarks
        });
        $scope.UpdatePaymentInfo();
        initializePaymentObj();
        setTimeout(function () {
            $scope.amount = angular.copy($scope.reservationObj.payment_info.amount_due);
        }, 1500);
    };
    $scope.RemovePaymentItem = function (pos) {
        var newDataList = [];
        for (var i = 0, l = $scope.reservationObj.payment_info.payments_list.length; i < l; i++) {
            if (i !== pos) {
                newDataList.push($scope.reservationObj.payment_info.payments_list[i]);
            }
        }
        $scope.reservationObj.payment_info.payments_list = newDataList;
        $scope.UpdatePaymentInfo();
    };
    $scope.UpdateDiscount = function () {
        $scope.error_message = "";
        if (angular.element("#reservation_discount_form").valid()) {
            if ($scope.reservationObj.payment_info.amount > 0) {
                if ($scope.reservationObj.payment_info.discount_type != '') {

                    $scope.reservationObj.payment_info.discount_value = parseFloat($scope.reservationObj.payment_info.discount_value);
                    $scope.reservationObj.payment_info.amount = parseFloat($scope.reservationObj.payment_info.amount);
                    if ($scope.reservationObj.payment_info.discount_type == 'flat') {
                        $scope.reservationObj.payment_info.discount_amount = $scope.reservationObj.payment_info.discount_value;
                        $scope.discount_text = '<i class="fa fa-rupee-sign" aria-hidden="true"></i> ' + $scope.reservationObj.payment_info.discount_value;
                    } else if ($scope.reservationObj.payment_info.discount_type == 'percentage') {
                        $scope.reservationObj.payment_info.discount_amount = ($scope.reservationObj.payment_info.amount * $scope.reservationObj.payment_info.discount_value) / 100;
                        $scope.discount_text = '<i class="fa fa-percent" aria-hidden="true"></i> ' + $scope.reservationObj.payment_info.discount_value;
                    }
                }
                $scope.UpdatePaymentInfo();
                $scope.noty('success', 'Success', 'Discount applied successfully');
                angular.element("#modal-discount").modal("hide");
            }
        }
    };
    $scope.ResetDiscount = function () {
        $scope.reservationObj.payment_info.discount_amount = 0;
        $scope.reservationObj.payment_info.discount_value = 0;
        $scope.reservationObj.payment_info.discount_type = '';
        $scope.discount_text = '';
        $scope.UpdatePaymentInfo();
        angular.element("#modal-discount").modal("hide");
    }

    $scope.UpdatePaymentInfo = function () {
        $scope.reservationObj.payment_info.amount = 0;
        $scope.reservationObj.payment_info.sub_total = 0;
        $scope.reservationObj.payment_info.grand_total = 0;
        $scope.reservationObj.payment_info.payments_total = 0;
        $scope.reservationObj.payment_info.applied_tax_amount = 0;
        $scope.reservationObj.payment_info.applied_taxes = [];
        if ($scope.reservationObj.booking_items.length > 0) {
            for (var i = 0; i < $scope.reservationObj.booking_items.length; i++) {
                if ($scope.reservationObj.booking_items[i] !== null) {
                    for (var j = 0; j < $scope.reservationObj.booking_items[i].items.length; j++) {
                        $scope.reservationObj.payment_info.amount += parseFloat($scope.reservationObj.booking_items[i].items[j].total_price);
                    }
                }
            }
            $scope.reservationObj.payment_info.sub_total = ((parseFloat($scope.reservationObj.payment_info.amount) - parseFloat($scope.reservationObj.payment_info.discount_amount)));
            $scope.reservationObj.payment_info.grand_total = $scope.reservationObj.payment_info.sub_total;
            //Fetching taxs script starts here
            var arr = [];
            for (var i = 0; i < $scope.taxesList.length; i++) {
                if ($scope.reservationObj.payment_info.sub_total > $scope.taxesList[i].tax_apply_above) {
                    var applied_tax_amount = 0;
                    if ($scope.taxesList[i].tax_type === 'Percentage') {
                        applied_tax_amount = (parseFloat($scope.reservationObj.payment_info.sub_total) * parseFloat($scope.taxesList[i].tax_amount)) / 100;
                    } else if ($scope.taxesList[i].tax_type === 'Flat Amount') {
                        applied_tax_amount = parseFloat($scope.taxesList[i].tax_amount);
                    }

                    $scope.taxTemp = {
                        id: angular.isDefined($scope.taxesList[i].id) ? $scope.taxesList[i].id : '',
                        tax_name: $scope.taxesList[i].tax_name,
                        tax_amount: parseFloat($scope.taxesList[i].tax_amount),
                        tax_calculation_type: $scope.taxesList[i].tax_type,
                        applied_tax_amount: applied_tax_amount
                    };
                    console.log($scope.taxTemp);
                    arr.push($scope.taxTemp);
                    $scope.reservationObj.payment_info.grand_total += parseFloat(applied_tax_amount);
                    $scope.reservationObj.payment_info.applied_tax_amount += parseFloat(applied_tax_amount);
                }
            }

            $scope.reservationObj.payment_info.applied_taxes = arr;
            //Fetching taxs script ends here
            //Calculating payments_total starts here
            for (var i = 0; i < $scope.reservationObj.payment_info.payments_list.length; i++) {
                $scope.reservationObj.payment_info.payments_total += $scope.reservationObj.payment_info.payments_list[i].amount;
            }
            $scope.reservationObj.payment_info.payments_total = $scope.reservationObj.payment_info.payments_total;
            $scope.reservationObj.payment_info.amount_due = Math.round($scope.reservationObj.payment_info.grand_total - $scope.reservationObj.payment_info.payments_total);
            $scope.reservationObj.payment_info.grand_total = Math.round($scope.reservationObj.payment_info.grand_total);
            $scope.amount = angular.copy($scope.reservationObj.payment_info.amount_due);
            $scope.reservationObj.payment_info.new_items = 0;
            for (var i = 0; i < $scope.reservationObj.payment_info.payments_list.length; i++) {
                if (!angular.isDefined($scope.reservationObj.payment_info.payments_list[i].id)) {
                    $scope.reservationObj.payment_info.new_items++;
                }
            }
            //Calculating payments_total ends here
        }
        $scope.spinny(false);
    };
    $scope.GetTaxSlabsList = function () {
        $scope.taxFilterObj.centers_id = $scope.reservationObj.centers_id;
        $scope.taxFilterObj.check_in_date = $scope.reservationObj.check_in_date;
        $scope.taxFilterObj.check_in_time = $scope.reservationObj.check_in_time;
        $scope.taxFilterObj.duration = $scope.reservationObj.duration;
        $scope.spinny(true);
        taxSlabsService.getByCentersId($scope.taxFilterObj).then(function (response) {
            $scope.spinny(false);
            if (response.data.err_code === "valid") {
                $scope.taxesList = response.data.data;
            } else if (response.data.err_code === "invalid_form") {
                $scope.error_message = response.data.message;
            } else if (response.data.err_code === "invalid") {
                $scope.error_message = response.data.message;
            }
        });
    };
    $scope.customerObj = {};
    $scope.EditCustomerInfo = function () {
        $scope.customerObj = angular.copy({
            booking_ref_number: $scope.reservationObj.booking_ref_number,
            contact_mobile: $scope.reservationObj.contact_mobile,
            contact_name: $scope.reservationObj.contact_name,
            contact_email: $scope.reservationObj.contact_email,
            alternative_mobile: $scope.reservationObj.alternative_mobile
        });
        angular.element("#customer_info_popup").modal({backdrop: 'static',
            keyboard: false});
    };
    $scope.UpdateCustomerInfo = function () {
        if (angular.element("#customer_form").valid()) {
            $scope.spinny(true);
            reservationViewService.updateCustomerInformation($scope.customerObj).then(function (response) {
                $scope.spinny(false);
                if (response.data.err_code === "valid") {
                    angular.element("#customer_info_popup").modal("hide");
                    $scope.GetReservationDetails();
                    $scope.noty('success', response.data.title, response.data.message);
                } else if (response.data.err_code === "invalid_form") {
                    $scope.error_message = response.data.message;
                } else if (response.data.err_code === "invalid") {
                    $scope.noty('warning', response.data.title, response.data.message);
                }
            });
        }
    };
    $scope.otherInfoObj = {};
    $scope.EditOtherInfo = function () {
        $scope.otherInfoObj = angular.copy({
            booking_ref_number: $scope.reservationObj.booking_ref_number,
            how_did_hear_about_us_id: $scope.reservationObj.how_did_hear_about_us_id,
            agent_mobile: $scope.reservationObj.agent_mobile,
            agent_commission: $scope.reservationObj.agent_commission,
            booking_status: $scope.reservationObj.booking_status,
            business_sources_id: $scope.reservationObj.business_sources_id
        });
        angular.element("#other_info_popup").modal({backdrop: 'static',
            keyboard: false});
    };
    $scope.UpdateOtherInfo = function () {
        if (angular.element("#other_info_form").valid()) {
            /*if ($scope.otherInfoObj.agent_mobile != '' && $scope.otherInfoObj.agent_commission == '') {
             $scope.noty('warning', 'Comission required', 'please enter commison %');
             return false;
             }*/
            $scope.spinny(true);
            reservationViewService.updateOtherInformation($scope.otherInfoObj).then(function (response) {
                $scope.spinny(false);
                if (response.data.err_code === "valid") {
                    angular.element("#other_info_popup").modal("hide");
                    $scope.GetReservationDetails();
                    $scope.noty('success', response.data.title, response.data.message);
                } else if (response.data.err_code === "invalid_form") {
                    $scope.error_message = response.data.message;
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };
    $scope.GetReferencesList = function () {
        howDidHearAboutUsService.get().then(function (response) {
            $scope.referencesList = response.data.data;
        });
    };
    $scope.GetReferencesList();
    $scope.GetBusinessSourcesList = function () {
        businessSourcesService.get().then(function (response) {
            $scope.businessSourcesList = response.data.data;
        });
    };
    $scope.GetBusinessSourcesList();
    $scope.stayInfoObj = {};
    $scope.EditStayInfo = function () {
        $scope.error_message = "";
        $scope.stayInfoObj = angular.copy({
            booking_ref_number: $scope.reservationObj.booking_ref_number,
            check_in_date: $scope.reservationObj.check_in_date,
            check_in_time: $scope.reservationObj.check_in_time,
            duration: $scope.reservationObj.duration
        });
        angular.element("#edit_arrival_time_popup").modal({backdrop: 'static',
            keyboard: false});
    };
    $scope.UpdateArrivalTime = function () {
        if (angular.element("#edit_arrival_time_form").valid()) {
            $scope.spinny(true);
            reservationViewService.updateArrivalTime($scope.stayInfoObj).then(function (response) {
                $scope.spinny(false);
                if (response.data.err_code === "valid") {
                    angular.element("#edit_arrival_time_popup").modal("hide");
                    $scope.GetReservationDetails();
                    $scope.noty('success', response.data.title, response.data.message);
                } else if (response.data.err_code === "invalid_form") {
                    $scope.error_message = response.data.message;
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };
    $scope.sweetyDisplay = false;
    $scope.MarkAsCheckoutForSinglePerson = function (item, category_name) {
        $scope.sweetyDisplay = true;
        item.category_name = category_name;
        item.booking_ref_number = $scope.reservationObj.booking_ref_number;
        $scope.sweetyInfo = item;
        $scope.action_fn = function (id) {
            if (id === true) {
                $scope.spinny(true);
                reservationViewService.updateIndividualCheckout(item, $scope.users_id).then(function (response) {
                    $scope.spinny(false);
                    if (response.data.err_code === "valid") {
                        $scope.GetReservationDetails();
                        $scope.noty('success', response.data.title, response.data.message);
                    } else if (response.data.err_code === "invalid_form") {
                        $scope.noty('warning', response.data.title, response.data.message);
                    } else if (response.data.err_code === "invalid") {
                        $scope.noty('warning', response.data.title, response.data.message);
                    }
                });
                $scope.GetReservationDetails();
                //server call goes here
                $scope.sweetyDisplay = false;
            } else {
                $scope.sweetyDisplay = false;
            }
        };
    };
    $scope.SetUserId = function (users_id) {
        $scope.users_id = users_id;
    };
    $scope.agentsList_display = false;
    $scope.GetAgentsListWithSearchKey = function () {
        $scope.agentSpinner = true;
        agentsService.getAgentsListWithSearchKey($scope.otherInfoObj.agent_mobile).then(function (response) {
            $scope.agentSpinner = false;
            if (response.data.data.length > 0) {
                $scope.agentsList = response.data.data;
                $scope.agentsList_display = true;
            } else {
                $scope.agentsList_display = false;
            }
        });
    };
    $scope.agentsListClick = function (obj) {
        $scope.otherInfoObj.agent_mobile = obj.mobile;
        $scope.otherInfoObj.agent_commission = obj.commission;
        $scope.agentsList_display = false;
    };
    $scope.AddOrUpdateAgent = function () {
        $scope.error_message = "";
        if (angular.element("#agentForm").valid()) {
            $scope.spinny(true);
            agentsService.save($scope.agentObj).then(function (response) {
                $scope.spinny(false);
                if (response.data.err_code === "valid") {
                    angular.element("#agent-modal-popup").modal("hide");
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeAgentObject();
                } else if (response.data.err_code === "invalid_form") {
                    $scope.error_message = response.data.message;
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };
    $scope.ShowAgentAddForm = function () {
        initializeAgentObject();
        angular.element("#agent-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };
    $scope.GetAgentTypesList = function () {
        agentTypesService.get().then(function (response) {
            $scope.agentTypesList = response.data.data;
        });
    };
    $scope.GetAgentTypesList();
    $scope.billingInfoObj = {};
    $scope.EditBillingInfo = function () {
        $scope.error_message = "";
        $scope.billingInfoObj = angular.copy({
            booking_ref_number: $scope.reservationObj.booking_ref_number,
            billing_to: $scope.reservationObj.billing_to,
            company_name: $scope.reservationObj.company_name,
            billing_to_others_text: $scope.reservationObj.billing_to_others_text,
            door_no: $scope.reservationObj.door_no,
            street_name: $scope.reservationObj.street_name,
            location: $scope.reservationObj.location,
            city: $scope.reservationObj.city,
            district: $scope.reservationObj.district,
            state: $scope.reservationObj.state,
            zipcode: $scope.reservationObj.zipcode,
            countries_id: $scope.reservationObj.countries_id,
            payer_gst_number: $scope.reservationObj.payer_gst_number
        });
        angular.element("#billing_info_popup").modal({backdrop: 'static',
            keyboard: false});
    };
    $scope.UpdateBillingInfo = function () {
        if (angular.element("#billing_form").valid()) {
            $scope.spinny(true);
            reservationViewService.updateBillingInformation($scope.billingInfoObj).then(function (response) {
                $scope.spinny(false);
                if (response.data.err_code === "valid") {
                    angular.element("#billing_info_popup").modal("hide");
                    $scope.GetReservationDetails();
                    $scope.noty('success', response.data.title, response.data.message);
                } else if (response.data.err_code === "invalid_form") {
                    $scope.error_message = response.data.message;
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };
    $scope.companiesList_display = false;
    $scope.GetCompaniesListWithSearchKey = function () {
        $scope.companySpinner = true;
        companiesService.getCompaniesListWithSearchKey($scope.billingInfoObj.company_name).then(function (response) {
            $scope.companySpinner = false;
            if (response.data.data.length > 0) {
                $scope.companiesList = response.data.data;
                $scope.companiesList_display = true;
            } else {
                $scope.companiesList_display = false;
            }
        });
    };
    $scope.companiesListClick = function (obj) {
        $scope.billingInfoObj.companies_id = obj.id;
        $scope.billingInfoObj.company_name = obj.company_name;
        $scope.billingInfoObj.door_no = obj.door_no;
        $scope.billingInfoObj.street_name = obj.street_name;
        $scope.billingInfoObj.location = obj.location_name;
        $scope.billingInfoObj.city = obj.city_name;
        $scope.billingInfoObj.district = obj.district_name;
        $scope.billingInfoObj.state = obj.state_name;
        $scope.billingInfoObj.zipcode = obj.zipcode;
        $scope.billingInfoObj.countries_id = obj.countries_id;
        $scope.billingInfoObj.payer_gst_number = obj.tax_identity_number;
        $scope.companiesList_display = false;
    }

    $scope.ShowCompanyAddForm = function () {
        angular.element("#company-add").modal({backdrop: 'static',
            keyboard: false});
    };
    $scope.HideCheckInPopup = function () {
        angular.element("#modal-checkin").modal("hide");
    };
    $scope.HideCheckOutPopup = function () {
        angular.element("#modal-checkout").modal("hide");
    };


    $scope.ApplyIdentiyNumberValidation = function () {
        $scope.reservationObj.check_in_details.proof_number = "";
        if ($scope.reservationObj.check_in_details.proof_type === "Aadhaar Card") {
            $scope.reservationObj.check_in_details.max_length = 12;
            $scope.reservationObj.check_in_details.min_length = 12;
        } else if ($scope.reservationObj.check_in_details.proof_type === "Voter ID") {
            $scope.reservationObj.check_in_details.max_length = 10;
            $scope.reservationObj.check_in_details.min_length = 10;
        } else if ($scope.reservationObj.check_in_details.proof_type === "PAN Card") {
            $scope.reservationObj.check_in_details.max_length = 10;
            $scope.reservationObj.check_in_details.min_length = 10;
        } else if ($scope.reservationObj.check_in_details.proof_type === "Driving License") {
            $scope.reservationObj.check_in_details.max_length = 20;
            $scope.reservationObj.check_in_details.min_length = 9;
        } else if ($scope.reservationObj.check_in_details.proof_type === "Passport") {
            $scope.reservationObj.check_in_details.max_length = 15;
            $scope.reservationObj.check_in_details.min_length = 8;
        } else if ($scope.reservationObj.check_in_details.proof_type === "Other") {
            $scope.reservationObj.check_in_details.max_length = 32;
            $scope.reservationObj.check_in_details.min_length = 1;
        }
    };

    $scope.ApplyIdentiyNumberValidationGuestsDetail = function (sub_item) {
        sub_item.identity_number = "";
        if (sub_item.identity_type === "Aadhaar Card") {
            sub_item.max_length = 12;
            sub_item.min_length = 12;
        } else if (sub_item.identity_type === "Voter ID") {
            sub_item.max_length = 10;
            sub_item.min_length = 5;
        } else if (sub_item.identity_type === "PAN Card") {
            sub_item.max_length = 10;
            sub_item.min_length = 10;
        } else if (sub_item.identity_type === "Driving License") {
            sub_item.max_length = 20;
            sub_item.min_length = 9;
        } else if (sub_item.identity_type === "Passport") {
            sub_item.max_length = 15;
            sub_item.min_length = 8;
        } else if (sub_item.identity_type === "Other") {
            sub_item.max_length = 32;
            sub_item.min_length = 1;
        }
    };

    $scope.ApplyIdentiyNumberValidationGuestsDetailInit = function (sub_item) {
        if (sub_item.identity_type === "Aadhaar Card") {
            sub_item.max_length = 12;
            sub_item.min_length = 12;
        } else if (sub_item.identity_type === "Voter ID") {
            sub_item.max_length = 10;
            sub_item.min_length = 5;
        } else if (sub_item.identity_type === "PAN Card") {
            sub_item.max_length = 10;
            sub_item.min_length = 10;
        } else if (sub_item.identity_type === "Driving License") {
            sub_item.max_length = 20;
            sub_item.min_length = 9;
        } else if (sub_item.identity_type === "Passport") {
            sub_item.max_length = 15;
            sub_item.min_length = 8;
        } else if (sub_item.identity_type === "Other") {
            sub_item.max_length = 32;
            sub_item.min_length = 1;
        }
    };

    $scope.AutoFillProofDetails = function () {
        var obj = $scope.guests;
        if (obj !== '') {
            if (obj.identity_type !== '' && obj.identity_type) {
                $scope.reservationObj.check_in_details.proof_type = obj.identity_type;
                $scope.ApplyIdentiyNumberValidation();
            }
            if (obj.identity_number !== '' && obj.identity_number) {
                $scope.reservationObj.check_in_details.proof_number = obj.identity_number;
            }
            if (obj.identity_image !== '') {
                $scope.reservationObj.check_in_details.proof_already_existed = true;
                $scope.reservationObj.check_in_details.existed_proof_image = obj.identity_image;
            }
        } else {
            $scope.reservationObj.check_in_details.proof_type = '';
            $scope.reservationObj.check_in_details.proof_number = '';
            $scope.reservationObj.check_in_details.proof_already_existed = false;
            $scope.reservationObj.check_in_details.existed_proof_image = "";
        }
    };
    function initializeAvailabilitySearchObject() {
        $scope.availabilitySearchObj = {
            check_in_date: '',
            check_in_time: '',
            duration: '',
            booking_ref_number: ''
        };
    }
    initializeAvailabilitySearchObject();
    $scope.ResetAvailabilitySearchForm = function () {
        initializeAvailabilitySearchObject();
    };
    $scope.getStayAvailability = function () {
        $scope.error_message = "";
        $scope.stayAvailabilityList = [];
        console.log($scope.reservationObj.centers);
        if ($scope.stayInfoObj.check_in_date === '') {
            $scope.error_message = "Please choose check-in date";
        } else if ($scope.stayInfoObj.check_in_time === '') {
            $scope.error_message = "Please choose check-in time";
        } else if ($scope.stayInfoObj.duration === '') {
            $scope.error_message = "Please choose duration";
        } else {
            $scope.availabilitySearchObj.centers_id = $scope.reservationObj.centers_id;
            $scope.availabilitySearchObj.availability_type = 1;
            $scope.availabilitySearchObj.staying_types_id = $scope.reservationObj.staying_types_id;
            $scope.availabilitySearchObj.check_in_date = $scope.stayInfoObj.check_in_date;
            $scope.availabilitySearchObj.check_in_time = $scope.stayInfoObj.check_in_time;
            $scope.availabilitySearchObj.duration = $scope.stayInfoObj.duration;
            $scope.availabilitySearchObj.booking_ref_number = $scope.reservationObj.booking_ref_number;
            $scope.spinny(true);
            createReservationService.getStayAvailabilityByCentersIdAndBookingId($scope.availabilitySearchObj).then(function (response) {
                $scope.spinny(false);
                console.log(response.data.data);
                if (response.data.err_code === "valid") {
                    $scope.GetTaxSlabsList();
                    $scope.noty('success', response.data.title, response.data.message);
                    $scope.UpdatePaymentInfo();
                    $scope.UpdateArrivalTime();
                    //$scope.stayAvailabilityList = response.data.data;
                } else if (response.data.err_code === "invalid_form") {
                    $scope.error_message = response.data.message;
                } else if (response.data.err_code === "invalid") {
                    //$scope.error_message = response.data.message;
                    $scope.noty('invalid', response.data.title, response.data.message);
                }
            });
        }
    };

    $scope.ShowGuestDetailsEditPopUp = function (booking_items) {
        $scope.edit_booking_items = angular.copy(booking_items);
        angular.element("#guest-details-edit").modal({backdrop: 'static',
            keyboard: false});
        setTimeout(function () {
            angular.element("#guest-details-edit").validate();
        }, 1000);
    };

    $scope.HideGuestDetailsPopup = function () {
        angular.element("#guest-details-edit").modal("hide");
    };

    $scope.UpdateGuestsDetails = function () {
        if (angular.element("#guest-details-form").valid()) {
            $scope.spinny(true);
            $scope.guests_details = {
                booking_ref_number: $scope.reservationObj.booking_ref_number,
                booking_items: $scope.edit_booking_items
            };
            reservationViewService.updateGuestsDetail($scope.guests_details).then(function (response) {
                $scope.spinny(false);
                if (response.data.err_code === "valid") {
                    $scope.HideGuestDetailsPopup();
                    $scope.GetReservationDetails();
                    $scope.noty('success', response.data.title, response.data.message);
                } else if (response.data.err_code === "invalid_form") {
                    $scope.error_message = response.data.message;
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };

    function CheckFileSizeIsValidForGuest(file_element_id) {
        alert();
        fsize = document.getElementById(file_element_id).files[0].size;
        var iSize = (fsize / 1024);
        iSize = (Math.round(iSize * 100) / 100)

        if (iSize > 5000) {
            alert("The file you are attempting to upload is larger than the permitted size 5MB");
            return false;
        }
    }

    $scope.guestItemValidation = function (guest_item) {
        guest_item.validation_required = false;
        if (guest_item.identity_type != "") {
            if (!angular.isDefined(guest_item.identity_number)) {
                guest_item.validation_required = true;
            }
        }
    }

    $scope.UpdateGuestInfo = function (guest_item, file_element_id) {
        guest_item.validation_required = false;
        if (guest_item.identity_type != "") {
            if (!angular.isDefined(guest_item.identity_number)) {
                guest_item.validation_required = true;
                return;
            }
        }
        guest_item.booking_ref_number = $scope.reservationObj.booking_ref_number;
        if (document.getElementById(file_element_id).files[0] !== undefined) {
            fsize = document.getElementById(file_element_id).files[0].size;
            var iSize = (fsize / 1024);
            iSize = (Math.round(iSize * 100) / 100)

            if (iSize > 5000) {
                $scope.noty('warning', "Error", "The file you are attempting to upload is larger than the permitted size 5MB");
                return false;
            }
        }

        $scope.spinny(true);

        var fd = new FormData();
        var ObjectKeys = Object.keys(guest_item);

        for (var i = 0; i < ObjectKeys.length; i++) {
            if (ObjectKeys[i] === "identity_image") {
                continue;
            }
            if (typeof guest_item[ObjectKeys[i]] === 'object' || guest_item[ObjectKeys[i]].constructor === Array) {
                fd.append(ObjectKeys[i], JSON.stringify(guest_item[ObjectKeys[i]]));
            } else {
                fd.append(ObjectKeys[i], guest_item[ObjectKeys[i]]);
            }
        }

        fd.append("identity_image_file", document.getElementById(file_element_id).files[0]);

        reservationViewService.updateSingleDetail(fd).then(function (response) {
            $scope.spinny(false);
            if (response.data.err_code === "valid") {
///                $("#" + file_element_id).unwrap();
                $scope.reservationObj = {};
                $scope.GetReservationDetails();
                $scope.spinny(true);
                setTimeout(function () {
                    $("a[href='#guests-details']").click();
                    $scope.spinny(false);
                }, 2000)

                //location.reload();
                $scope.noty('success', response.data.title, response.data.message);
            } else if (response.data.err_code === "invalid_form") {
                $scope.noty('warning', response.data.title, response.data.message);
            } else if (response.data.err_code === "invalid") {
                $scope.noty('warning', response.data.title, response.data.message);
            }
        });
    };

    /*------------------------------------------------------------Code For FB sales--------------------------------------------------------------*/

    function initializePOSPaymentObj() {
        if (!$scope.amount) {
            $scope.amount = "";
        }

        $scope.payment_method = "";
        $scope.transaction_number = "";
        $scope.card_number = "";
        $scope.cheque_number = "";
        $scope.bank_account_number = "";
        $scope.date = "";
        $scope.remarks = "";
    }
    initializePOSPaymentObj();

    $scope.ShowPOSPaymentForm = function (fb_pos_item) {
        $scope.posObj = angular.copy(fb_pos_item);
        console.log($scope.posObj);
        angular.element("#pos_payment_screen").modal({backdrop: 'static',
            keyboard: false});

        $scope.amount = angular.copy(fb_pos_item.payment_info.amount_due);

    };

    $scope.ValidatePOSAmount = function (amount, max_amount) {
        if (amount > max_amount) {
            $scope.amount = max_amount;
        }
    };

    $scope.AddPOSPaymentRow = function () {
        if ($scope.RunPaymentFieldValidation() == false) {
            return false;
        }

        $scope.posObj.payment_info.payments_list.push({
            amount: $scope.amount,
            payment_method: $scope.payment_method,
            transaction_number: $scope.transaction_number,
            card_number: $scope.card_number,
            cheque_number: $scope.cheque_number,
            bank_account_number: $scope.bank_account_number,
            date: $scope.date,
            remarks: $scope.remarks
        });
        $scope.UpdatePosPaymentInfo();
        initializePOSPaymentObj();
        setTimeout(function () {
            $scope.amount = angular.copy($scope.posObj.payment_info.amount_due);
        }, 1500);
    };


    $scope.UpdatePosPaymentInfo = function () {
        $scope.posObj.payment_info.total_items = 0;
        $scope.posObj.payment_info.amount = 0;
        $scope.posObj.payment_info.sub_total = 0;
        $scope.posObj.payment_info.grand_total = 0;
        $scope.posObj.payment_info.payments_total = 0;
        $scope.posObj.payment_info.applied_tax_amount = 0;
        $scope.posObj.payment_info.applied_taxes = [];

        console.log($scope.posObj.cart_items);

        for (var i = 0; i < $scope.posObj.cart_items.length; i++) {
            console.log($scope.posObj.cart_items[i]);
            $scope.posObj.total_items++;
            $scope.posObj.cart_items[i].sub_total = $scope.posObj.cart_items[i].qty * parseFloat($scope.posObj.cart_items[i].total_price);
            $scope.posObj.payment_info.amount += $scope.posObj.cart_items[i].sub_total;
        }
        $scope.posObj.payment_info.sub_total = $scope.posObj.payment_info.amount - $scope.posObj.payment_info.discount_amount;
        $scope.posObj.payment_info.grand_total = $scope.posObj.payment_info.sub_total;


        //Calculating payments_total starts here
        for (var i = 0; i < $scope.posObj.payment_info.payments_list.length; i++) {
            $scope.posObj.payment_info.payments_total += parseFloat($scope.posObj.payment_info.payments_list[i].amount);
        }
        $scope.posObj.payment_info.payments_total = $scope.posObj.payment_info.payments_total;

        $scope.posObj.payment_info.amount_due = Math.round($scope.posObj.payment_info.grand_total - $scope.posObj.payment_info.payments_total);
        $scope.posObj.payment_info.grand_total = Math.round($scope.posObj.payment_info.grand_total);

        $scope.amount = angular.copy($scope.posObj.payment_info.amount_due);
        $scope.posObj.payment_info.new_items = 0;
        for (var i = 0; i < $scope.posObj.payment_info.payments_list.length; i++) {
            if (!angular.isDefined($scope.posObj.payment_info.payments_list[i].id)) {
                $scope.posObj.payment_info.new_items++;
            }
        }
        console.log($scope.posObj.payment_info.payments_list);
    };

    $scope.RemovePOSPaymentItem = function (pos) {
        var newDataList = [];
        for (var i = 0, l = $scope.posObj.payment_info.payments_list.length; i < l; i++) {
            if (i !== pos) {
                newDataList.push($scope.posObj.payment_info.payments_list[i]);
            }
        }
        $scope.posObj.payment_info.payments_list = newDataList;
        $scope.UpdatePosPaymentInfo();
    };

    $scope.UpdatePaidAmountInfoForPOS = function () {
        $scope.spinny(true);
        posService.updatePaymentDetails($scope.posObj, $scope.users_id).then(function (response) {
            if (response.data.err_code === "valid") {
                $scope.noty('success', response.data.title, response.data.message);
                initializeObject();
                $scope.GetReservationDetails();
                angular.element("#pos_payment_screen").modal("hide");
                $scope.spinny(false);
            } else if (response.data.err_code === "invalid_form") {
                $scope.error_message = response.data.message;
            } else if (response.data.err_code === "invalid") {
                $scope.error_message = response.data.message;
            }
        });
    };

    /*----------------------------------Code For FB sales End------------------------------*/

    $scope.UpdateAllPosPaymentInfo = function () {
        $scope.fbAllSalePaymentObj.paid_amount = 0;
        //Calculating payments_total starts here
        for (var i = 0; i < $scope.fbAllSalePaymentObj.payment_list.length; i++) {
            $scope.fbAllSalePaymentObj.paid_amount += $scope.fbAllSalePaymentObj.payment_list[i].amount;
        }
        console.log($scope.fbAllSalePaymentObj.paid_amount);

        $scope.fbAllSalePaymentObj.amount_due = Math.round($scope.fbAllSalePaymentObj.grand_total - $scope.fbAllSalePaymentObj.paid_amount);
        //$scope.fbAllSalePaymentObj.grand_total = Math.round($scope.fbAllSalePaymentObj.payment_info.grand_total);
        $scope.amount = angular.copy($scope.fbAllSalePaymentObj.amount_due);

        $scope.spinny(false);
    };

    $scope.ShowAllPOSPaymentForm = function (booking_ref_number) {
        $scope.spinny(true);
        posService.getPosPaymentinfo(booking_ref_number).then(function (response) {
            $scope.fbAllSalePaymentObj = response.data.data;
            $scope.fbAllSalePaymentObj.amount = angular.copy(response.data.data.pending_amount);
            angular.element("#pos_all_payment_paid_once").modal({backdrop: 'static',
                keyboard: false});
            $scope.amount = angular.copy($scope.fbAllSalePaymentObj.amount);
            $scope.spinny(false);
        });
    };

    $scope.ValidateAllPOSAmount = function (amount, max_amount) {
        if (amount > max_amount) {
            $scope.amount = max_amount;
        }
    };
    $scope.AddAllPOSPaymentRow = function () {
        if ($scope.RunPaymentFieldValidation() == false) {
            return false;
        }
        $scope.spinny(true);
        console.log($scope.fbAllSalePaymentObj.payment_list);

        $scope.fbAllSalePaymentObj.payment_list.push({
            amount: parseFloat($scope.amount),
            payment_method: $scope.payment_method,
            transaction_number: $scope.transaction_number,
            card_number: $scope.card_number,
            cheque_number: $scope.cheque_number,
            bank_account_number: $scope.bank_account_number,
            date: $scope.date,
            remarks: $scope.remarks
        });
        $scope.UpdateAllPosPaymentInfo();
        initializePaymentObj();
        setTimeout(function () {
            $scope.amount = angular.copy($scope.fbAllSalePaymentObj.amount_due);
        }, 1500);
    };

    $scope.RemoveAllPOSPaymentItem = function (pos) {
        var newDataList = [];
        for (var i = 0, l = $scope.fbAllSalePaymentObj.payment_list.length; i < l; i++) {
            if (i !== pos) {
                newDataList.push($scope.fbAllSalePaymentObj.payment_list[i]);
            }
        }
        $scope.fbAllSalePaymentObj.payment_list = newDataList;
        $scope.UpdateAllPosPaymentInfo();
    };

    $scope.UpdatePaidAmountInfoForAllPOS = function () {
        $scope.fbAllSalePaymentObj.reservations_booking_ref_number = $scope.reservationObj.booking_ref_number;
        posService.updatePosAllSalesPaymentDetails($scope.fbAllSalePaymentObj).then(function (response) {
            if (response.data.err_code === "valid") {
                $scope.noty('success', response.data.title, response.data.message);
                location.reload();
                angular.element("#pos_all_payment_paid_once").modal("hide");
                $scope.spinny(false);
            } else if (response.data.err_code === "invalid") {
                $scope.noty('warning', response.data.title, response.data.message);
            }
        });
    };
    /*------------------------------------------------------------Code For FB sales End--------------------------------------------------------------*/

    $scope.UpdateTicketAttachment = function (ticket_attachment) {
        console.log();
        $scope.ticket_attachment_data = {
            booking_ref_number: $scope.reservationObj.booking_ref_number,
            ticket_attachment: ticket_attachment
        };
        $scope.spinny(true);
        reservationViewService.updateTicketAttachment($scope.ticket_attachment_data).then(function (response) {
            $scope.spinny(false);
            if (response.data.err_code === "valid") {
                $scope.noty('success', response.data.title, response.data.message);
                location.reload();
            } else if (response.data.err_code === "invalid") {
                $scope.noty('warning', response.data.title, response.data.message);
            }
        })
    };

    $scope.ShowCancellationPopUp = function () {
        angular.element("#cancellation-info-modal").modal("hide");
        setTimeout(function () {
            angular.element("#guests-info-modal").modal("show");
        }, 500);
    };

    $scope.ShowCancellationDetailsPopUp = function () {
        $scope.selected_ids_for_cancel = [];
        for (var i = 0; i < $scope.reservationObj.booking_items.length; i++) {
            for (j = 0; j < $scope.reservationObj.booking_items[i].items.length; j++) {
                if (angular.isDefined($scope.reservationObj.booking_items[i].items[j].is_checked)) {
                    if ($scope.reservationObj.booking_items[i].items[j].is_checked == true) {
                        $scope.selected_ids_for_cancel.push($scope.reservationObj.booking_items[i].items[j].id);
                    }
                }
            }
        }

        if ($scope.selected_ids_for_cancel.length == 0) {
            $scope.noty('warning', "Required", "Please choose atleast one item to proceed");
            return false;
        }

        angular.element("#guests-info-modal").modal("hide");

        var cancellation_items = {
            "booking_ref_number": $scope.reservationObj.booking_ref_number,
            selected_ids_for_cancel: $scope.selected_ids_for_cancel.join()
        };

        $scope.spinny(true);
        reservationViewService.getCancellationDetails(cancellation_items).then(function (response) {
            $scope.spinny(false);
            if (response.data.err_code == "valid") {
                angular.element("#cancellation-info-modal").modal("show");
                $scope.refund_info = response.data.data;
                $scope.refund_info.cancel_reason = "";
                $scope.refund_info.cancel_notes = "";
            } else if (response.data.err_code == "invalid") {
                $scope.refund_info = {};
                if (angular.isDefined(response.data.err_type)) {
                    $scope.noty('warning', response.data.title, response.data.message);
                } else {
                    $scope.noty('warning', response.data.title, response.data.message);
                }
            }
        });
    };

    $scope.DoCancelReservation = function () {

        $scope.refund_info.selected_ids_for_cancel = $scope.selected_ids_for_cancel.join();
        $scope.spinny(true);
        reservationViewService.cancelReservation($scope.refund_info).then(function (response) {
            $scope.spinny(false);
            if (response.data.err_code == "valid") {
                $scope.noty('success', response.data.title, response.data.message);
                location.reload();
            } else if (response.data.err_code == "invalid") {
                angular.element("#cancellation-info-modal").modal("hide");
                $scope.noty('warning', response.data.title, response.data.message);
            }
        });
    };

    $scope.toggleAll = function () {
        for (var i = 0; i < $scope.reservationObj.booking_items.length; i++) {
            for (var j = 0; j < $scope.reservationObj.booking_items[i].items.length; j++) {
                $scope.reservationObj.booking_items[i].items[j].is_checked = $scope.is_selected_all;
            }
        }
    };
});
