angular.element("#appVersionForm").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        game_type: {
            required: true
        },
        game_sub_type: {
            required: true
        },
        seats: {
            required: true
        },
        entry_fee: {
            required: true
        }
    }, messages: {
        game_type: {
            required: "Please Choose Game Type"
        },
        game_sub_type: {
            required: "Please Choose Game Sub Type"
        },
        seats: {
            required: "Please choose number of seats"
        },
        entry_fee: {
            required: "Entry free cannot be empty"
        }
    }
});
app.controller("gamesCtrl", function ($scope, appVersionService, DTOptionsBuilder) {
    $scope.showLoader = false;
    function initializeObject() {
        $scope.appVersionObj = {
            id: '',
            version: '',
            description: '',
            is_force: ''
        };
    }

    $scope.filter = {
        //page: 1,
        game_type: '',
        game_sub_type: ''
    };
    $scope.ResetFilter = function () {
        $scope.filter = {
            //page: 1,
            game_type: '',
            game_sub_type: ''
        };
        appVersionService.get($scope.filter).then(function (response) {
            $scope.spinny(false);
            $scope.appVersionList = response.data.data;
        });
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };
    $scope.appVersionList = function () {
        $scope.spinny(true);
        appVersionService.get($scope.filter).then(function (response) {
            $scope.spinny(false);
            $scope.appVersionList = response.data.data;
        });
    };

    setTimeout(function () {
        $scope.appVersionList();
    }, 150);
    $scope.AddOrUpdateAppVersion = function () {
        $scope.error_message = "";
        $scope.g_error = {};
        if (angular.element("#appVersionForm").valid()) {
            $scope.spinny(true);
            appVersionService.save($scope.appVersionObj).then(function (response) {
                $scope.spinny(false);
                if (response.data.status === "valid") {
                    angular.element("#game-modal-popup").modal("hide");
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    appVersionService.get($scope.filter).then(function (response) {
                        $scope.spinny(false);
                        $scope.appVersionList = response.data.data;
                    });
                } else if (response.data.status === "invalid_form") {
                    $scope.g_error = response.data.data;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                    $scope.noty('warning', response.data.title, response.data.message);
                }
            });
        }
    };

    $scope.EditGame = function (obj) {
        $scope.appVersionObj = angular.copy(obj);
        angular.element("#game-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };

    $scope.DeleteGame = function (item) {
        $scope.sweety(item.fullname, function () {
            appVersionService.delete(item.id).then(function (response) {
                if (response.data.status === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    console.log("delete");
                   $scope.spinny(true);
                    appVersionService.get($scope.filter).then(function (response) {
                        $scope.spinny(false);
                        $scope.appVersionList = response.data.data;
                    });
                } else if (response.data.status === "invalid_form") {
                    $scope.g_error = response.data.data;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };

    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);

    $scope.ShowGameAddForm = function () {
        initializeObject();
        angular.element("#game-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };

    $scope.ViewGame = function (obj) {
        $scope.appVersionObj = angular.copy(obj);
        angular.element("#view-game-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };
});