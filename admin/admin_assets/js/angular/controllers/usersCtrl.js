angular.element("#userForm").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        role_id: {
            required: true
        },
        fullname: {
            required: true
        },
        username: {
            required: true
        },
        login_status: {
            required: true
        },
        password: {
            required: true
        },
        confirm_password: {
            required: true,
            equalTo: "#password",
        },
        designation: {
            required: true
        },
        mobile: {
            required: true,
            digits: true,
            minlength: 10,
            maxlength: 10
        },
        email: {
            required: true,
            email: true
        },
        payroll_status: {
            required: true
        },
        gender: {
            required: true
        },
        dob: {
            required: true
        },
        address: {
            required: true
        },
        joining_date: {
            required: true
        },
        ctc: {
            required: true,
            digits: true
        },
        net_salary: {
            required: true,
            digits: true
        }
    }, messages: {
        role_id: {
            required: "Role is required"
        },
        fullname: {
            required: "Fullname is required"
        },
        username: {
            required: "Username is required"
        },
        login_status: {
            required: "Login status is required"
        },
        password: {
            required: "Password is required"
        },
        confirm_password: {
            required: "Confirm Password is required",
            equalTo: "Password and Confirm Password should be same"
        },
        designation: {
            required: "Designation is required"
        },
        mobile: {
            required: "Mobile is required",
            digits: "Please enter numeric only"
        },
        email: {
            required: "Email is required"
        },
        payroll_status: {
            required: "Payroll Status is required"
        },
        gender: {
            required: "Gender is required"
        },
        dob: {
            required: "Date of Birth is required"
        },
        address: {
            required: "Address is required"
        },
        joining_date: {
            required: "Date of Joining is required"
        },
        ctc: {
            required: "CTC is required"
        },
        net_salary: {
            required: "Net Salary is required"
        }
    }
});
app.controller("usersCtrl", function ($scope, $timeout, usersService, rolesService, DTOptionsBuilder) {
    $scope.showLoader = false;
    function initializeObject() {
        $scope.agentTypeAccessObj = {};

        $scope.userObj = {
            id: '',
            username: '',
            password: '',
            confirm_password: ''
        };
        $scope.userObj.user_meta = {
            pan_no: '',
            esi_no: '',
            pf_no: '',
            license_no: '',
            ctc: '',
            bank_name: '',
            branch_name: '',
            account_no: '',
            ifsc_code: '',
            micr_code: '',
            net_salary: '',
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };
    $scope.GetUsersList = function () {
        usersService.get().then(function (response) {
            $scope.usersList = response.data.data;
        });
    };
    
    $timeout(function(){
        $scope.GetUsersList();
    }, 500);

    $scope.AddOrUpdateUser = function () {
        $scope.error_message = "";
        if (angular.element("#userForm").valid()) {
            $scope.spinny(true);
            usersService.save($scope.userObj).then(function (response) {
                $scope.spinny(false);
                if (response.data.status === "valid") {
                    angular.element("#user-modal-popup").modal("hide");
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.GetUsersList();
                } else if (response.data.status === "invalid_form") {
                    $scope.error_message = response.data.message;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };

    $scope.EditUser = function (obj) {
        $scope.userObj = angular.copy(obj);
        angular.element("#user-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };

    $scope.DeleteUser = function (item) {
        $scope.sweety(item.fullname, function () {
            usersService.delete(item.id).then(function (response) {
                if (response.data.status === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetUsersList();
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);
    $scope.ShowUserAddForm = function () {
        initializeObject();
        angular.element("#user-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };
    $scope.rolesSpinner = true;
    rolesService.get().then(function (response) {
        $scope.rolesSpinner = false;
        $scope.rolesList = response.data.data;
    });
    $scope.GetPrivileges = function () {
        if ($scope.selected_user_details.id === null) {
            return;
        }
        $scope.spinny(true);
        if (angular.isDefined($scope.selected_user_details.id) && ($scope.selected_user_details.id != '')) {
            usersService.getPrivilegesFromDBForUser($scope.selected_user_details.id).then(function (response) {
                $scope.spinny(false);
                $scope.modulesList = response.data.data;
                $scope.privilegie = response.data.privilegies;
                $scope.selectedAll = response.data.privilegies.selectedAll;
            });
        } else {
            rolesService.getPrivilegesFromDB($scope.userObj.role_id).then(function (response) {
                $scope.spinny(false);
                $scope.modulesList = response.data.data;
                $scope.privilegie = response.data.privilegies;
                $scope.selectedAll = response.data.privilegies.selectedAll;
            });
        }
    };
    $scope.ToggleSelectionForAllPrivileges = function () {
        var b = $scope.privilegie.all_permission ? true : false;
        for (var i = 0; i < $scope.modulesList.length; i++) {
            $scope.modulesList[i].access_rights.view_permission = b;
            $scope.modulesList[i].access_rights.add_permission = b;
            $scope.modulesList[i].access_rights.edit_permission = b;
            $scope.modulesList[i].access_rights.delete_permission = b;
            $scope.modulesList[i].access_rights.all_permission = b;
            $scope.RecursiveToggleSelectionForAll($scope.modulesList[i].sub_modules);
        }
        $scope.privilegie = {
            view_permission: b,
            add_permission: b,
            edit_permission: b,
            delete_permission: b,
            all_permission: b
        };
    };
    $scope.RecursiveToggleSelectionForAll = function (item) {
        var b = $scope.privilegie.all_permission ? true : false;
        for (var i = 0; i < item.length; i++) {
            item[i].access_rights.view_permission = b;
            item[i].access_rights.add_permission = b;
            item[i].access_rights.edit_permission = b;
            item[i].access_rights.delete_permission = b;
            item[i].access_rights.all_permission = b;
            if (item[i].sub_modules.length > 0) {
                $scope.RecursiveToggleSelectionForAll(item[i].sub_modules);
            }
        }
    };
    var cnt = 1;
    $scope.CheckIsAllRowValuesSelected = function (item) {
        console.log(cnt++);
        if (item.delete_permission
                && item.add_permission
                && item.view_permission
                && item.edit_permission) {
            item.all_permission = true;
        } else {
            $scope.privilegie.all_permission = false;
        }
    };
    $scope.ToggleSelectionForColumn = function (type) {
        cnt = 1;
        for (var i = 0; i < $scope.modulesList.length; i++) {
            $scope.modulesList[i].access_rights[type] = $scope.privilegie[type];
            $scope.CheckIsAllRowValuesSelected($scope.modulesList[i].access_rights);
            $scope.RecursiveToggleSelectionForColumn($scope.modulesList[i].sub_modules, type);
        }
    };
    $scope.RecursiveToggleSelectionForColumn = function (item, type) {
        for (var i = 0; i < item.length; i++) {
            item[i].access_rights[type] = $scope.privilegie[type];
            $scope.CheckIsAllRowValuesSelected(item[i].access_rights);
            if (item[i].sub_modules.length > 0) {
                $scope.RecursiveToggleSelectionForColumn(item[i].sub_modules, type);
            }
        }
    };
    $scope.ToggleSelectionForRow = function (item) {
        var b = item.all_permission ? true : false;
        item.view_permission = b;
        item.add_permission = b;
        item.edit_permission = b;
        item.delete_permission = b;
        item.all_permission = b;
    };
    $scope.EditPrivileges = function (user_details) {
        $scope.selected_user_details = user_details;
        angular.element("#privilegesModalPopup").modal("show");
        $scope.GetPrivileges();
    };
    $scope.SavePrivileges = function () {
        $scope.spinny(true);
        usersService.savePrivilegesToDBForUser($scope.modulesList, $scope.selected_user_details.id).then(function (response) {
            $scope.spinny(false);
            $scope.noty('success', response.data.title, response.data.message);
            angular.element("#privilegesModalPopup").modal("hide");
        });
    };
    $scope.ViewUser = function (obj) {
        $scope.userObj = angular.copy(obj);
        angular.element("#view-user-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };
});