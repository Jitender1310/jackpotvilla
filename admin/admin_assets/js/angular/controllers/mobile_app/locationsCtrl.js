angular.element("#locationform").validate({
    rules: {
        title: {
            required: true
        },
        description: {
            required: true
        },
        availability_status: {
            required: true
        },
        image: {
            required: true
        },
        background_image: {
            required: true
        }
    }, messages: {
        title: {
            required: "Location title"
        },
        availability_status: {
            required: "Availability Status"
        },
        description: {
            required: "Location description"
        }
    }
});
app.controller("locationsCtrl", function ($scope, locationsService, DTOptionsBuilder) {
    $scope.filter = {};
    //$scope.filter.page = 1;
    $scope.showLoader = false;
    function initializeObject() {
        $scope.locationObj = {
            title: '',
            description: '',
            image: '',
            background_image: ''
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };
    $scope.AddOrUpdateLocation = function () {
        $scope.error_message = "";
        if (angular.element("#locationform").valid()) {
            $scope.showLoader = true;
            locationsService.save($scope.locationObj).then(function (response) {
                $scope.showLoader = false;
                if (response.data.err_code === "valid") {
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.GetLocationsList();
                } else if (response.data.err_code === "invalid_form") {
                    $scope.error_message = response.data.message;
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };
    $scope.GetLocationsList = function () {
        locationsService.get($scope.filter).then(function (response) {
            $scope.locationsList = response.data.data;
        });
    };
    $scope.EditLocation = function (obj) {
        $scope.locationObj = angular.copy(obj);
    };
    $scope.DeleteLocation = function (item) {
        $scope.sweety(item.title, function () {
            locationsService.delete(item.id).then(function (response) {
                if (response.data.err_code === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetLocationsList();
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('bInfo', false);
});