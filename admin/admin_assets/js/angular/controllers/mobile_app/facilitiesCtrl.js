angular.element("#facilityForm").validate({
    rules: {
        title: {
            required: true
        },
        description: {
            required: true
        },
        image: {
            required: true
        }
    }, messages: {
        title: {
            required: "Facility title"
        },
        description: {
            required: "Facility description"
        }
    }
});
app.controller("facilitiesCtrl", function ($scope, facilitiesService, $sce, DTOptionsBuilder) {
    $scope.filter = {};
    //$scope.filter.page = 1;
    $scope.showLoader = false;
    function initializeObject() {
        $scope.facilityObj = {
            title: '',
            description: '',
            image: ''
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };

    $scope.AddOrUpdateFacility = function () {
        $scope.error_message = "";
        if (angular.element("#facilityForm").valid()) {
            $scope.showLoader = true;
            facilitiesService.save($scope.facilityObj).then(function (response) {
                $scope.showLoader = false;
                if (response.data.err_code === "valid") {
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.GetFacilitiesList();
                } else if (response.data.err_code === "invalid_form") {
                    $scope.error_message = response.data.message;
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };


    $scope.GetFacilitiesList = function () {
        facilitiesService.get($scope.filter).then(function (response) {
            $scope.facilitiesList = response.data.data;
        });
    };


    $scope.EditFacility = function (obj) {
        $scope.facilityObj = angular.copy(obj);
    };
    $scope.DeleteFacility = function (item) {
        $scope.sweety(item.title, function () {
            facilitiesService.delete(item.id).then(function (response) {
                if (response.data.err_code === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetFacilitiesList();
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('bInfo', false);
});