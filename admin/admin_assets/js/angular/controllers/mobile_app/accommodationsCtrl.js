angular.element("#accommodationForm").validate({
    rules: {
        title: {
            required: true
        },
        description: {
            required: true
        },
        image: {
            required: true
        }
    }, messages: {
        title: {
            required: "Facility title"
        },
        description: {
            required: "Facility description"
        }
    }
});
app.controller("accommodationsCtrl", function ($scope, accommodationService, DTOptionsBuilder) {
    $scope.filter = {};
    //$scope.filter.page = 1;
    $scope.showLoader = false;
    function initializeObject() {
        $scope.accommodationObj = {
            title: '',
            description: '',
            image: ''
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };

    $scope.AddOrUpdateAccommodation = function () {
        $scope.error_message = "";
        if (angular.element("#accommodationForm").valid()) {
            $scope.showLoader = true;
            accommodationService.save($scope.accommodationObj).then(function (response) {
                $scope.showLoader = false;
                if (response.data.err_code === "valid") {
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.GetAccommodationsList();
                } else if (response.data.err_code === "invalid_form") {
                    $scope.error_message = response.data.message;
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };


    $scope.GetAccommodationsList = function () {
        accommodationService.get($scope.filter).then(function (response) {
            $scope.accommodationsList = response.data.data;
        });
    };


    $scope.EditAccommodation = function (obj) {
        $scope.accommodationObj = angular.copy(obj);
    };
    $scope.DeleteAccommodation = function (item) {
        $scope.sweety(item.title, function () {
            accommodationService.delete(item.id).then(function (response) {
                if (response.data.err_code === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetAccommodationsList();
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('bInfo', false);
});