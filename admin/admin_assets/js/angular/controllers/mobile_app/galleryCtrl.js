angular.element("#galleryForm").validate({
    rules: {
        centers_id: {
            required: true
        },
        staying_type_categories_id: {
            required: true
        },
        image: {
            required: true
        },
        website_image: {
            required: true
        }
    }, messages: {
        centers_id: {
            required: "Center is required"
        },
        staying_type_categories_id: {
            required: "Category is required"
        }
    }
});
app.controller("galleryCtrl", function ($scope, galleryService, centersService, stayingCategoriesService, DTOptionsBuilder) {
    $scope.filter = {};
    //$scope.filter.page = 1;
    $scope.showLoader = false;
    function initializeObject() {
        $scope.galleryObj = {
            staying_type_categories_id: '',
            image: '',
            website_image: ''
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };
    $scope.AddOrUpdateGallery = function () {
        $scope.error_message = "";
        if (angular.element("#galleryForm").valid()) {

            var fd = new FormData();
            var ObjectKeys = Object.keys($scope.galleryObj);
            for (var i = 0; i < ObjectKeys.length; i++) {
                if (ObjectKeys[i] == "website_image" || ObjectKeys[i] == "image") {
                    continue;
                }
                if (typeof $scope.galleryObj[ObjectKeys[i]] === 'object' || $scope.galleryObj[ObjectKeys[i]].constructor === Array) {
                    fd.append(ObjectKeys[i], JSON.stringify($scope.galleryObj[ObjectKeys[i]]));
                } else {
                    fd.append(ObjectKeys[i], $scope.galleryObj[ObjectKeys[i]]);
                }
            }
            fd.append("app_image", document.getElementById("app_image").files[0]);
            fd.append("website_image", document.getElementById("website_image").files[0]);


            $scope.spinny(true);
            galleryService.save(fd).then(function (response) {
                $scope.spinny(false);
                if (response.data.err_code === "valid") {
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.GetGalleryList();
                } else if (response.data.err_code === "invalid_form") {
                    $scope.error_message = response.data.message;
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };
    $scope.galleryList = [];
    $scope.GetGalleryList = function () {
        galleryService.get($scope.filter).then(function (response) {
            $scope.galleryList = response.data.data;
        });
    };
    $scope.EditGallery = function (obj) {
        $scope.galleryObj = angular.copy(obj);
    };
    $scope.DeleteGallery = function (item) {
        $scope.sweety(item.title, function () {
            galleryService.delete(item.id).then(function (response) {
                if (response.data.err_code === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetGalleryList();
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    $scope.GetStayingCategoriesList = function () {
        stayingCategoriesService.get().then(function (response) {
            $scope.stayingCategoriesList = response.data.data;
        });
    };
    $scope.GetStayingCategoriesList();
    $scope.GetCentersList = function () {
        centersService.getOnlyCenterTitles().then(function (response) {
            $scope.centersList = response.data.data;
        });
    };
    $scope.GetCentersList();
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('bInfo', false);
});