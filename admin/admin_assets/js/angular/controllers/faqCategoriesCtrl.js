angular.element("#categoriesForm").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        category: {
            required: true
        },
        icon: {
            required: true
        }
    }, messages: {
        category: {
            required: "Please enter category name"
        },
        icon: {
            required: "Plesae select icon"
        }
    }
});
app.controller("faqCategoriesCtrl", function ($scope, $timeout, faqCategoriesService, DTOptionsBuilder) {
    $scope.showLoader = false;
    function initializeObject() {
        $scope.faqCategoriesObj = {
            id: '',
            category: '',
        };
        angular.element("input[type='file']").val(null);
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };
    $scope.GetFaqCategorysList = function () {
        faqCategoriesService.get().then(function (response) {
            $scope.faqCategoriesList = response.data.data;
        });
    };

    $timeout(function () {
        $scope.GetFaqCategorysList();
    }, 500);

    $scope.AddOrUpdateFaqCategory = function () {
        $scope.error_message = "";
        $scope.p_error = {};
        if (angular.element("#categoriesForm").valid()) {

            var fd = new FormData();
            var ObjectKeys = Object.keys($scope.faqCategoriesObj);
//            console.log(ObjectKeys);

            for (var i = 0; i < ObjectKeys.length; i++) {
                if (ObjectKeys[i] === "icon") {
                    continue;
                }
                fd.append(ObjectKeys[i], $scope.faqCategoriesObj[ObjectKeys[i]]);
            }

            if (document.getElementById("icon") != null) {
                fd.append("icon", document.getElementById("icon").files[0]);
            }
            $scope.spinny(true);
            faqCategoriesService.save(fd).then(function (response) {
                $scope.spinny(false);
                if (response.data.status === "valid") {
                    angular.element("#faqcategory-modal-popup").modal("hide");
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.GetFaqCategorysList();
                } else if (response.data.status === "invalid_form") {
                    $scope.t_error = response.data.data;
                    $scope.error_message = response.data.message;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };


    $scope.DeleteFaqCategory = function (item) {
        $scope.sweety(item.category, function () {
            faqCategoriesService.delete(item.id).then(function (response) {
                if (response.data.status === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetFaqCategorysList();
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);

    $scope.ShowFaqCategoryAddForm = function () {
        initializeObject();
        angular.element("#faqcategory-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };

    $scope.EditFaqCategory = function (obj) {
        $scope.faqCategoriesObj = angular.copy(obj);
        angular.element("#faqcategory-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };

});