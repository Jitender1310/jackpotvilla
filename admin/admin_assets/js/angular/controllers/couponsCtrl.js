angular.element("#couponForm").validate({
    rules: {
        centers_id: {
            required: true
        },
        coupon_code: {
            required: true
        },
        start_date: {
            required: true
        },
        end_date: {
            required: true
        },
        discount_type: {
            required: true
        },
        discount_value: {
            required: true
        },
        minimum_to_apply_discount: {
            required: true
        },
        applicable_on: {
            required: true
        },
        coupon_usage_type: {
            required: true
        },
        coupon_usage_attempts: {
            required: true
        }
    }, messages: {
        centers_id: {
            required: "Choose Centers"
        },
        coupon_code: {
            required: "Coupon name is required"
        },
        start_date: {
            required: "Start Date is required"
        },
        end_date: {
            required: "End Date is required"
        },
        discount_type: {
            required: "Discount Type is required"
        },
        discount_value: {
            required: "Discount Value is required"
        },
        minimum_to_apply_discount: {
            required: "Discount Apply Above is required"
        },
        applicable_on: {
            required: "Applicable on is required"
        },
        coupon_usage_type: {
            required: "Usage type is required"
        },
        coupon_usage_attempts: {
            required: "usage attempts is required"
        }
    }
});
app.controller("couponsCtrl", function ($scope, couponsService, centersService, DTOptionsBuilder) {
    $scope.showLoader = false;
    function initializeObject() {
        $scope.couponObj = {
            coupon_code: '',
            centers_id: '',
            start_date: "",
            end_date: '',
            discount_value: "",
            discount_type: "",
            minimum_to_apply_discount: "",
            applicable_on: "",
            coupon_usage_type: "",
            coupon_usage_attempts: ""
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
        angular.element("#coupon-add").modal("hide");
    };
    $scope.GetCentersList = function () {
        centersService.get().then(function (response) {
            $scope.centersList = response.data.data;
        });
    };
    $scope.GetCentersList();
    $scope.AddOrUpdateCoupon = function () {
        $scope.error_message = "";
        if (angular.element("#couponForm").valid()) {
            $scope.spinny(true);
            couponsService.save($scope.couponObj).then(function (response) {
                $scope.spinny(false);
                if (response.data.err_code === "valid") {
                    angular.element("#coupon-add").modal("hide");
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.GetCouponsList();
                } else if (response.data.err_code === "invalid_form") {
                    $scope.error_message = response.data.message;
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };
    $scope.GetCouponsList = function () {
        couponsService.get().then(function (response) {
            $scope.couponsList = response.data.data;
        });
    };
    $scope.EditCoupon = function (obj) {
        $scope.couponObj = angular.copy(obj);
        $scope.couponObj.centers_id = $scope.couponObj.coupon_centers;
        angular.element("#coupon-add").modal({backdrop: 'static',
            keyboard: false});
    };
    $scope.DeleteCoupon = function (item) {
        $scope.sweety(item.coupon_name, function () {
            couponsService.delete(item.id).then(function (response) {
                if (response.data.err_code === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetCouponsList();
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);
    $scope.ShowCouponAddForm = function () {
        initializeObject();
        angular.element("#coupon-add").modal({backdrop: 'static',
            keyboard: false});
    };
});