angular.element("#withdrawRequestForm").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        request_amount: {
            required: true
        },
        request_status: {
            required: true
        },

    }, messages: {
        request_amount: {
            required: "Request Amount is required"
        },
        request_status: {
            required: "Request Status is required"
        },
    }
});
app.controller("withdrawRequestsCtrl", function ($timeout, $scope, $sce, withdrawRequestsService, DTOptionsBuilder, $rootScope) {
    $scope.filter = {
        page: 1,
        search_key: '',
        from_date: '',
        to_date: '',
        players_id: '',
    };
    $scope.ResetFilter = function () {
        $scope.filter = {
            page: 1,
            search_key: '',
            from_date: '',
            to_date: '',
            players_id: '',
        };
        $scope.GetWithdrawRequestsList();
    }

    $scope.withdrawRequestsList = [];
    $scope.filter.request_status = 'all';

    $scope.GetWithdrawRequestsList = function () {
        $scope.spinny(true);
        withdrawRequestsService.getListOfWithdrawRequests($scope.filter).then(function (response) {
            $scope.spinny(false);
            $scope.withdrawRequestsList = response.data.data.results;
            $scope.withdrawRequestsPagination = response.data.pagination;
            if ($scope.withdrawRequestsPagination.pagination.length > 0) {
                $scope.withdrawRequestsPagination.pagination = $sce.trustAsHtml($scope.withdrawRequestsPagination.pagination);
            }
        });
    };

    $timeout(function () {
        $scope.GetWithdrawRequestsList();
    }, 500);

    $scope.EditRequest = function (obj) {
        $scope.withdrawRequestObj = angular.copy(obj);
        angular.element("#withdraw-request-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };

    $scope.AddOrUpdateRequest = function () {
        $scope.error_message = "";
        $scope.p_error = {};
        if (angular.element("#withdrawRequestForm").valid()) {
            $scope.spinny(true);
            withdrawRequestsService.save($scope.withdrawRequestObj).then(function (response) {
                $scope.spinny(false);
                if (response.data.status === "valid") {
                    angular.element("#withdraw-request-modal-popup").modal("hide");
                    $scope.noty('success', response.data.title, response.data.message);
                    $scope.GetWithdrawRequestsList();
                    initializeObject();
                } else if (response.data.status === "invalid_form") {
                    $scope.p_error = response.data.data;
                    $scope.error_message = response.data.message;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };
    $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filter.page = $(this).attr("data-ci-pagination-page");
        $scope.GetWithdrawRequestsList();
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', false)
            .withOption('bPaginate', false)
            .withOption('lengthChange', false)
            .withOption('bInfo', false)
            .withOption('bSort', false);
});
