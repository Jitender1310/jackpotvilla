app.controller("depositTransactionsCtrl", function ($scope, $sce, depositTransactionsService, DTOptionsBuilder, $rootScope) {
    $scope.filter = {
        page: 1,
        search_key: '',
        from_date: '',
        to_date: '',
        players_id: '',
    };
    $scope.ResetFilter = function(){
        $scope.filter = {
            page: 1,
            search_key: '',
            from_date: '',
            to_date: '',
            players_id: '',
        };
        $scope.GetTransactionsList();
    }
    
    $scope.transactionList = [];
    $scope.filter.transaction_status = 'all';

    $scope.GetTransactionsList = function () {

        $scope.spinny(true);
        depositTransactionsService.getListOfDepositTransactions($scope.filter).then(function (response) {
            $scope.spinny(false);
            $scope.transactionList = response.data.data.results;
            $scope.transactionsPagination = response.data.pagination;
            if ($scope.transactionsPagination.pagination.length > 0) {
                $scope.transactionsPagination.pagination = $sce.trustAsHtml($scope.transactionsPagination.pagination);
            }
        });
    };


    $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filter.page = $(this).attr("data-ci-pagination-page");
        $scope.GetTransactionsList();
    });

    $scope.ExportToExcel = function () {
        window.open(baseurl + "reservations/export_to_excel/generate?direct_download=true&" + $.param($scope.filter), '_blank', 'scrollbars=no,menubar=no,height=600,width=800,resizable=yes,toolbar=no,status=no');
    };

    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', false)
            .withOption('bPaginate', false)
            .withOption('lengthChange', false)
            .withOption('bInfo', false)
            .withOption('bSort', false);
});
