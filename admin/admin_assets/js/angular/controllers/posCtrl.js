angular.element("#posForm").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        centers_id: {
            required: true
        },
        customer_type: {
            required: true
        }
    }, messages: {
        centers_id: {
            required: "Center is required"
        },
        customer_type: {
            required: "Customer type is required",
        }
    }
});
app.controller("posCtrl", function ($scope, centersService, reservationViewService, posService, DTOptionsBuilder) {
    $scope.filter = {};

    function initializeObject() {
        $scope.posObj = {
            creation_source: 'front_desk_portal',
            centers_id: '',
            reservations_id: '',
            customer_type: '',
            cart_items: [],
            payment_info: {
                total_items: 0,
                amount: 0,
                applied_taxes: [
                    /*{
                     tax_name: "SGST",
                     tax_amount: 20,
                     tax_calculation_type: 'pencentage',
                     applied_tax_amount: 0
                     }*/
                ],
                sub_total: 0,
                grand_total: 0,
                payments_total: 0,
                applied_tax_amount: 0,
                discount_amount: 0,
                discount_value: 0,
                discount_type: '',
                payments_list: [

                ]
            }
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };
    $scope.centersSpinner = false;
    $scope.GetCentersList = function (users_id) {
        $scope.centersSpinner = true;
        centersService.getOnlyAssignedCenters(users_id).then(function (response) {
            $scope.centersSpinner = false;
            $scope.centersList = response.data.data;
        });
    };

    $scope.GetPosProductsList = function (centers_id) {
        if (centers_id === null) {
            return;
        }
        $scope.posProductsList = [];
        $scope.availableProducts = [];
        $scope.posObj.cart_items = [];
        $scope.UpdatePosPaymentInfo();
        $scope.spinny(true);
        posService.getProductsList({"centers_id": centers_id}).then(function (response) {
            $scope.spinny(false);
            if (response.data.err_code == "valid") {
                $scope.posProductsList = response.data.data;
                if ($scope.posProductsList.length > 0) {
                    $scope.availableProducts = $scope.posProductsList[0].products;
                }
            } else if (response.data.err_code == "invalid") {
                $scope.noty("warning", response.data.title, response.data.message);
            }
        });
    };

    $scope.ClearCart = function () {
        $scope.posObj.cart_items = [];
        $scope.UpdatePosPaymentInfo();
    };

    $scope.ShowSelectedProducts = function (items) {
        $scope.availableProducts = items.products;
    };

    $scope.GetInHouseCustomers = function () {
        $scope.filter.booking_status = "inhouse_customers";
        $scope.filter.centers_id = $scope.posObj.centers_id;
        $scope.filter.start = 0;
        $scope.filter.limit = 150;
        $scope.spinny(true);
        reservationViewService.getListOfReservations($scope.filter).then(function (response) {
            $scope.spinny(false);
            $scope.reservationsList = response.data.data;
        });
    };

    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);


    $scope.$watch("posObj.customer_type", function (newValue, oldValue) {
        if (newValue === "Inhouse-Guest") {
            $scope.GetInHouseCustomers();
        }
    });


    $scope.CheckQtyAvailability = function (selected_cart_item) {
        console.log($scope.posProductsList);
        console.log(selected_cart_item);
        for (var i = 0; i < $scope.posProductsList.length; i++) {
            for (var j = 0; j < $scope.posProductsList[i].products.length; j++) {
                if (selected_cart_item.stock_products_id == $scope.posProductsList[i].products[j].id) {
                    if (selected_cart_item.qty >= $scope.posProductsList[i].products[j].available_qty) {
                        $scope.noty("warning", "Selected qty not available", "");
                        selected_cart_item.qty = $scope.posProductsList[i].products[j].available_qty;
                    }
                }
            }
        }
        $scope.UpdatePosPaymentInfo();
    };

    $scope.AddToCart = function (product_item) {
        for (var i = 0; i < $scope.posObj.cart_items.length; i++) {
            if ($scope.posObj.cart_items[i].stock_products_id == product_item.id) {
                if ($scope.posObj.cart_items[i].qty >= product_item.available_qty) {
                    $scope.noty("warning", "Selected qty not available", "");
                    return;
                }
                $scope.posObj.cart_items[i].qty++;
                $scope.posObj.cart_items[i].sub_total = $scope.posObj.cart_items[i].qty * $scope.posObj.cart_items[i].price;
                $scope.UpdatePosPaymentInfo();
                return;
            }
        }
        var pObj = {
            item_name: product_item.item_name,
            price: product_item.sale_price,
            qty: 1,
            stock_products_id: product_item.id,
            sub_total: parseFloat(product_item.sale_price) * 1
        };
        $scope.posObj.cart_items.push(pObj);
        $scope.UpdatePosPaymentInfo();
    };


    $scope.UpdatePosPaymentInfo = function () {
        $scope.posObj.payment_info.total_items = 0;
        $scope.posObj.payment_info.amount = 0;
        $scope.posObj.payment_info.sub_total = 0;
        $scope.posObj.payment_info.grand_total = 0;
        $scope.posObj.payment_info.payments_total = 0;
        $scope.posObj.payment_info.applied_tax_amount = 0;
        $scope.posObj.payment_info.applied_taxes = [];


        for (var i = 0; i < $scope.posObj.cart_items.length; i++) {
            console.log($scope.posObj.cart_items[i]);
            $scope.posObj.total_items++;
            $scope.posObj.cart_items[i].sub_total = $scope.posObj.cart_items[i].qty * parseFloat($scope.posObj.cart_items[i].price);
            $scope.posObj.payment_info.amount += $scope.posObj.cart_items[i].sub_total;
        }
        $scope.posObj.payment_info.sub_total = $scope.posObj.payment_info.amount - $scope.posObj.payment_info.discount_amount;
        $scope.posObj.payment_info.grand_total = $scope.posObj.payment_info.sub_total;


        //Calculating payments_total starts here
        for (var i = 0; i < $scope.posObj.payment_info.payments_list.length; i++) {
            $scope.posObj.payment_info.payments_total += parseFloat($scope.posObj.payment_info.payments_list[i].amount);
        }
        $scope.posObj.payment_info.payments_total = $scope.posObj.payment_info.payments_total;

        $scope.posObj.payment_info.amount_due = Math.round($scope.posObj.payment_info.grand_total - $scope.posObj.payment_info.payments_total);
        $scope.posObj.payment_info.grand_total = Math.round($scope.posObj.payment_info.grand_total);

        $scope.amount = angular.copy($scope.posObj.payment_info.amount_due);
        $scope.posObj.payment_info.new_items = 0;
        for (var i = 0; i < $scope.posObj.payment_info.payments_list.length; i++) {
            if (!angular.isDefined($scope.posObj.payment_info.payments_list[i].id)) {
                $scope.posObj.payment_info.new_items++;
            }
        }
        console.log($scope.posObj.payment_info.payments_list);
    };

    $scope.removeCartItem = function (pos) {
        var newData = [];
        for (var i = 0; i < $scope.posObj.cart_items.length; i++) {
            if (pos != i) {
                newData.push($scope.posObj.cart_items[i]);
            }
        }
        $scope.posObj.cart_items = newData;
        $scope.UpdatePosPaymentInfo();
    };

    $scope.$watch("posObj.cart_items", function (newValue, oldValue) {
        if (newValue === "Inhouse-Guest") {
            $scope.GetInHouseCustomers();
        }
    });

    $scope.ShowPosPaymentScreen = function () {
        if (angular.element("#posForm").valid()) {
            if ($scope.posObj.cart_items.length == 0) {
                $scope.noty("warning", "Please select at least one item", "");
                return;
            }
            if ($scope.posObj.customer_type == "Inhouse-Guest") {
                angular.element("#payment_action_screen").modal({backdrop: 'static',
                    keyboard: false});
            } else {
                $scope.ShowPaymentScreen();
            }
        }
    };

    $scope.ShowPaymentScreen = function () {
        if (angular.element("#posForm").valid()) {
            if ($scope.posObj.cart_items.length == 0) {
                $scope.noty("warning", "Please select at least one item", "");
                return;
            }
            angular.element("#pos_payment_screen").modal({backdrop: 'static',
                keyboard: false});
        }
    };

    $scope.CreateSaleOrder = function () {
        if ($scope.posObj.cart_items.length == 0) {
            $scope.noty("warning", "Please select at least one item", "");
            return;
        } else {
            $scope.spinny(true);
            posService.createSale($scope.posObj).then(function (response) {
                $scope.spinny(false);
                if (response.data.err_code === "valid") {
                    $scope.noty('success', response.data.title, response.data.message);
                    $scope.spinny(true);
                    initializeObject();
                    alert("Sale Created Successfully Ref Number : #" + response.data.data);
                    location.reload();
                    //location.href = baseurl + "pos/view?sale_id=" + response.data.data;
                } else if (response.data.err_code === "invalid_form") {
                    $scope.error_message = response.data.message;
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                    if (angular.isDefined(response.data.err_code)) {
                        if (response.data.err_identity == "STOCK_NOT_AVAILABLE") {
                            $scope.error_message = "";
                            $scope.noty('warning', response.data.title, response.data.message);
                            angular.element("#payment").modal("hide");
                        }
                    }
                }
            });
        }

    };


    $scope.UpdatePOSDiscount = function () {
        $scope.error_message = "";
        if (angular.element("#pos_discount_form").valid()) {
            if ($scope.posObj.payment_info.amount > 0) {
                if ($scope.posObj.payment_info.discount_type != '') {

                    $scope.posObj.payment_info.discount_value = parseFloat($scope.posObj.payment_info.discount_value);
                    $scope.posObj.payment_info.amount = parseFloat($scope.posObj.payment_info.amount);
                    if ($scope.posObj.payment_info.discount_type == 'flat') {
                        $scope.posObj.payment_info.discount_amount = $scope.posObj.payment_info.discount_value;
                        $scope.discount_text = '<i class="fa fa-rupee-sign" aria-hidden="true"></i> ' + $scope.posObj.payment_info.discount_value;
                    } else if ($scope.posObj.payment_info.discount_type == 'percentage') {
                        $scope.posObj.payment_info.discount_amount = ($scope.posObj.payment_info.amount * $scope.posObj.payment_info.discount_value) / 100;
                        $scope.discount_text = '<i class="fa fa-percent" aria-hidden="true"></i> ' + $scope.posObj.payment_info.discount_value;
                    }
                }
                $scope.UpdatePosPaymentInfo();
                $scope.noty('success', 'Success', 'Discount applied successfully');
                angular.element("#modal-pos-discount").modal("hide");
            }
        }
    };
    $scope.ResetDiscount = function () {
        $scope.posObj.payment_info.discount_amount = 0;
        $scope.posObj.payment_info.discount_value = 0;
        $scope.posObj.payment_info.discount_type = '';
        $scope.discount_text = '';
        $scope.UpdatePosPaymentInfo();
        angular.element("#modal-pos-discount").modal("hide");
    };

    function initializePOSPaymentObj() {
        if (!$scope.amount) {
            $scope.amount = "";
        }

        $scope.payment_method = "";
        $scope.transaction_number = "";
        $scope.card_number = "";
        $scope.cheque_number = "";
        $scope.bank_account_number = "";
        $scope.date = "";
        $scope.remarks = "";
    }
    initializePOSPaymentObj();

    $scope.ValidatePOSAmount = function (amount, max_amount) {
        if (amount > max_amount) {
            $scope.amount = max_amount;
        }
    };

    $scope.AddPOSPaymentRow = function () {
        if (($scope.amount < 0) || ($scope.amount === '')) {
            $scope.noty('warning', "Required", "Please enter amount")
            return false;
        }
        if (!angular.isDefined($scope.payment_method)) {
            $scope.noty('warning', "Required", "Please choose payment method")
            return false;
        }
        if ($scope.payment_method === "") {
            $scope.noty('warning', "Required", "Please choose payment method")
            return false;
        }

        if ($scope.payment_method === "Credit Card") {
            if ($scope.transaction_number === "") {
                $scope.noty('warning', "Required", "Please enter Card Transaction Number")
                return false;
            } else if ($scope.card_number === "") {
                $scope.noty('warning', "Required", "Please enter Card Number")
                return false;
            }
        }

        if ($scope.payment_method === "Cheque") {
            if ($scope.cheque_number === "") {
                $scope.noty('warning', "Required", "Please enter Cheque Number")
                return false;
            } else if ($scope.date === "") {
                $scope.noty('warning', "Required", "Please enter Date")
                return false;
            }
        }

        if ($scope.payment_method === "Bank Transfer") {
            if ($scope.bank_account_number === "") {
                $scope.noty('warning', "Required", "Please enter Bank Account Number")
                return false;
            } else if ($scope.date === "") {
                $scope.noty('warning', "Required", "Please enter Date")
                return false;
            }
        }

        if ($scope.payment_method === "Phonepay" || $scope.payment_method === "Paytm") {
            if ($scope.transaction_number === "") {
                $scope.noty('warning', "Required", "Please enter Transaction Number")
                return false;
            } else if ($scope.date === "") {
                $scope.noty('warning', "Required", "Please enter Date")
                return false;
            }
        }

        $scope.posObj.payment_info.payments_list.push({
            amount: $scope.amount,
            payment_method: $scope.payment_method,
            transaction_number: $scope.transaction_number,
            card_number: $scope.card_number,
            cheque_number: $scope.cheque_number,
            bank_account_number: $scope.bank_account_number,
            date: $scope.date,
            remarks: $scope.remarks
        });
        $scope.UpdatePosPaymentInfo();
        initializePOSPaymentObj();
        setTimeout(function () {
            $scope.amount = angular.copy($scope.posObj.payment_info.amount_due);
        }, 1500);
    };


    $scope.RemovePOSPaymentItem = function (pos) {
        var newDataList = [];
        for (var i = 0, l = $scope.posObj.payment_info.payments_list.length; i < l; i++) {
            if (i !== pos) {
                newDataList.push($scope.posObj.payment_info.payments_list[i]);
            }
        }
        $scope.posObj.payment_info.payments_list = newDataList;
        $scope.UpdatePosPaymentInfo();
    };

});