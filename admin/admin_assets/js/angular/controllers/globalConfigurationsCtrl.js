app.controller("globalConfigurationsCtrl", function ($scope, globalConfigurationsService, DTOptionsBuilder) {
    $scope.showLoader = false;
    
    $scope.getConstantsList = function () {
        globalConfigurationsService.get().then(function (response) {
            $scope.gamesList = response.data.data;
        });
    };
    
    setTimeout(function(){
        $scope.getConstantsList();    
    }, 550);
    
    $scope.updateConstants = function () {
        $scope.error_message = "";
        $scope.g_error = {};
        if (angular.element("#constantsForm").valid()) {
            $scope.spinny(true);
            globalConfigurationsService.save($scope.editConstantObj).then(function (response) {
                $scope.spinny(false);
                if (response.data.status === "valid") {
                    angular.element("#edit_constants_modal").modal("hide");
                    $scope.noty('success', response.data.title, response.data.message);
                    $scope.getConstantsList();
                } else if (response.data.status === "invalid_form") {
                    $scope.error_message = response.data.message;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };

    
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);
   
    $scope.showConstantsEditForm = function(){
        $scope.editConstantObj = angular.copy($scope.gamesList);
        angular.element("#edit_constants_modal").modal("show");
    };
});