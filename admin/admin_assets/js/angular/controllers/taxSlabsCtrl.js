angular.element("#taxSlabForm").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        centers_id: {
            required: true
        },
        tax_name: {
            required: true
        },
        tax_type: {
            required: true
        },
        tax_amount: {
            required: true
        },
        tax_apply_above: {
            required: true
        },
        start_date: {
            required: true
        },
        end_date: {
            required: true
        },
        status: {
            required: true
        },
        tax_status: {
            required: true
        },
        apply_until_changed: {
            required: true
        }
    }, messages: {
        centers_id: {
            required: "Center is required"
        },
        tax_name: {
            required: "Tax Name is required"
        },
        tax_type: {
            required: "Tax Type is required"
        },
        tax_amount: {
            required: "Tax Amount is required"
        },
        tax_apply_above: {
            required: "Tax Apply Above is required"
        },
        start_date: {
            required: "Start Date is required"
        },
        end_date: {
            required: "End Date is required"
        },
        status: {
            required: "Tax Status is required"
        },
        tax_status: {
            required: "Tax Status is required"
        },
        apply_until_changed: {
            required: "Apply Until Changed is required"
        }
    }
});
app.controller("taxSlabsCtrl", function ($scope, taxSlabsService, centersService, DTOptionsBuilder, loaderService) {
    $scope.showLoader = false;
    function initializeObject() {
        $scope.taxSlabObj = {
            centers_id: '',
            tax_name: '',
            tax_type: '',
            tax_amount: '',
            tax_apply_above: '',
            start_date: '',
            end_date: '',
            apply_until_changed: '',
            tax_status: ''
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };

    $scope.GetCentersList = function () {
        centersService.get().then(function (response) {
            $scope.centersList = response.data.data;
        });
    };
    $scope.GetCentersList();


    $scope.AddOrUpdateTaxSlab = function () {
        $scope.error_message = "";
        if (angular.element("#taxSlabForm").valid()) {
            $scope.showLoader = true;
            taxSlabsService.save($scope.taxSlabObj).then(function (response) {
                $scope.showLoader = false;
                if (response.data.err_code === "valid") {
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.GetTaxSlabsList();
                } else if (response.data.err_code === "invalid_form") {
                    $scope.error_message = response.data.message;
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };


    $scope.GetTaxSlabsList = function () {
        taxSlabsService.get().then(function (response) {
            $scope.agentsList = response.data.data;
        });
    };


    $scope.EditTaxSlab = function (obj) {
        $scope.taxSlabObj = angular.copy(obj);
    };
    $scope.DeleteTaxSlab = function (item) {
        $scope.sweety(item.tax_name, function () {
            taxSlabsService.delete(item.id).then(function (response) {
                if (response.data.err_code === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetTaxSlabsList();
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);
});