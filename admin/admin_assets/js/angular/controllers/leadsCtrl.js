app.controller("leadsCtrl", function ($scope, $sce, leadsService, centersService, DTOptionsBuilder) {
    $scope.filter = {};
    $scope.leadsList = [];
    $scope.filter.booking_status = 'all';

    $scope.GetCentersList = function (users_id) {
        centersService.getOnlyAssignedCenters(users_id).then(function (response) {
            $scope.centersList = response.data.data;
            $scope.GetSessionCenterId();
        });
    };
    $scope.GetSessionCenterId = function () {
        centersService.getSelectedCenterId().then(function (response) {
            $scope.selected_center_id = response.data.data;
        });
    };
    $scope.UpdateCenterSession = function (users_id, centers_id) {
        if (centers_id == null) {
            return;
        }
        $scope.spinny(true);
        centersService.updateCenterSelection(users_id, centers_id).then(function (response) {
            $scope.spinny(false);
            if (response.data.err_code === "valid") {
                $scope.noty('success', response.data.title, response.data.message);
            } else if (response.data.err_code === "invalid_form") {
                $scope.noty('warning', response.data.title, response.data.message);
            } else if (response.data.err_code === "invalid") {
                $scope.noty('warning', response.data.title, response.data.message);
            }
        });
    };

    $scope.GetLeadsList = function () {
        $scope.spinny(true);
        leadsService.get($scope.filter).then(function (response) {
            $scope.spinny(false);
            $scope.leadsList = response.data.data;
            $scope.leadsPagination = response.data.pagination;
            if ($scope.leadsPagination.pagination.length > 0) {
                $scope.leadsPagination.pagination = $sce.trustAsHtml($scope.leadsPagination.pagination);
            }
        });
    };


    $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filter.page = $(this).attr("data-ci-pagination-page");
        $scope.GetLeadsList();
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', false)
            .withOption('bPaginate', false)
            .withOption('lengthChange', false)
            .withOption('bInfo', false)
            .withOption('bSort', false);
});
