app.controller("joinedPlayersCtrl", function ($timeout, $scope, $sce, joinedPlayersService, DTOptionsBuilder, $rootScope) {

    $scope.filter = {
        page: 1,
        search_key: '',
        from_date: '',
        to_date: '',
        players_id: '',
        ref_id: '',
    };
    $scope.ResetFilter = function () {
        $scope.filter = {
            page: 1,
            search_key: '',
            from_date: '',
            to_date: '',
            players_id: '',
        };
        $scope.GetJoinedPlayersList();
    }

    $scope.GetJoinedPlayersList = function () {
        $scope.spinny(true);
        joinedPlayersService.getListOfJoinedPlayers($scope.filter).then(function (response) {
            $scope.spinny(false);
            $scope.joinedPlayersList = response.data.data.results;
            $scope.joinedPlayersPagination = response.data.pagination;
            if ($scope.joinedPlayersPagination.pagination.length > 0) {
                $scope.joinedPlayersPagination.pagination = $sce.trustAsHtml($scope.joinedPlayersPagination.pagination);
            }
        });
    };


    $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filter.page = $(this).attr("data-ci-pagination-page");
        $scope.GetJoinedPlayersList();
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', false)
            .withOption('bPaginate', false)
            .withOption('lengthChange', false)
            .withOption('bInfo', false)
            .withOption('bSort', false);
});
