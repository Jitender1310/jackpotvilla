angular.element("#tournamentForm").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        tournament_categories_id: {
            required: true,
        },
    }, messages: {
        tournament_categories_id: {
            required: "Select Tournament Category",
        },
    }
});
app.controller("upcomingTournamentsCtrl", function ($timeout, $scope, upcomingTournamentsService, clubTypesService, tournamentCategoriesService, DTOptionsBuilder, loaderService) {
    $scope.showLoader = false;
    function initializeObject() {
        $scope.tournamentObj = {
            id: '',
            tournament_categories_id: '',
            allowed_club_types: '',
            tournament_title: '',
            tournament_format: '',
            no_of_bots: '',
            winning_probability_to: '',
            entry_type: '',
            entry_value: 0,
            tournament_commission_percentage: 0,
            prizes_info: [],
            tournament_structure: [],
        };
    }
    initializeObject();
    function initializeFilterObject() {
        $scope.filter = {
            tournament_categories_id: '',
            premium_category: '',
            frequency: '',
            from_date: '',
            to_date: '',
            type: 'Upcoming',
            page: 1
        };
    }
    initializeFilterObject();
    $scope.ResetForm = function () {
        initializeObject();
    };
    $scope.ResetFilter = function () {
        initializeFilterObject();
        $scope.GetUpcomingTournaments();
    };

    $scope.GetUpcomingTournaments = function () {
        upcomingTournamentsService.get($scope.filter).then(function (response) {
            $scope.upcomingTournamentsList = response.data.data;
        });
    };
    $timeout(function () {
        $scope.GetUpcomingTournaments();
    }, 500);
    $scope.GetTournamentCategories = function () {
        tournamentCategoriesService.get().then(function (response) {
            $scope.tournamentCategoires = response.data.data;
        });
    };
    $scope.GetTournamentCategories();
    $scope.AddOrUpdateClonedTournaments = function () {
        $scope.error_message = "";
        $scope.g_error = {};
        $scope.tournamentObj.tournament_structure = $scope.tournamentObj.tournament_structure_for_edit;
        $scope.spinny(true);
        upcomingTournamentsService.save($scope.tournamentObj).then(function (response) {
            $scope.spinny(false);
            if (response.data.status === "valid") {
                angular.element("#tournaments-modal-popup").modal("hide");
                $scope.noty('success', response.data.title, response.data.message);
                initializeObject();
                $scope.GetUpcomingTournaments();
            } else if (response.data.status === "invalid_form") {
                $scope.g_error = response.data.data;
            } else if (response.data.status === "invalid") {
                $scope.error_message = response.data.message;
                $scope.noty('warning', response.data.title, response.data.message);
            }
        });

    };
    $scope.GetGameProgress = function (cloned_tournaments_id) {
        loaderService.getLoading();
        upcomingTournamentsService.game_progress(cloned_tournaments_id).then(function (response) {
            loaderService.closeLoading();
            if (response.data.status === "valid") {
                $scope.gameProgress = response.data.data.round_info;
            }
        });
    };
    $scope.GetTablesInfo = function (obj) {
        $scope.roundno = obj.round_number;
        loaderService.getLoading();
//        console.log(obj.players_info);
        if (obj.players_info.length > 0) {
            loaderService.closeLoading();
            $scope.tablesInfo = obj.players_info;
        } else {
            $scope.table_error_msg = "No Tables Found";
        }
    };
    $scope.GetPlayers = function (obj) {
        $scope.tableid = obj.tournamentTableId;
        loaderService.getLoading();
//        console.log(obj[index].players_info);
        if (obj.players.length > 0) {
            loaderService.closeLoading();
            $scope.playersInfo = obj.players;
        } else {
            $scope.players_error_msg = "No Players Found";
        }
    };


    $scope.EditTournament = function (obj) {
        $scope.tournamentObj = angular.copy(obj);
        angular.element("#tournaments-modal-popup").modal({backdrop: 'static',
            keyboard: false});
        $scope.calculatePrize();
    };

    $scope.ViewTournamentInfo = function (obj) {
        $scope.viewTournamentObj = angular.copy(obj);
        angular.element("#view-tournament-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };

    $scope.showTournamentDeatilsPopup = function (obj) {
        $scope.gameProgress = "";
        $scope.tablesInfo = "";
        $scope.playersInfo = "";
        $scope.roundno = "";
        $scope.tableid = "";
        $scope.tournamentDetailsObj = angular.copy(obj);
        angular.element("#tournamentDetails").modal("show");
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);


    $scope.ViewGame = function (obj) {
        $scope.gameObj = angular.copy(obj);
        angular.element("#view-game-modal-popup").modal({backdrop: 'static',
            keyboard: false});
    };

    $scope.GetClubTypes = function () {
        clubTypesService.get().then(function (response) {
            $scope.clubTypes = response.data.data;
        });
    };
    $scope.GetClubTypes();

    $scope.weeks = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    $scope.calculateExpectedPrize = function () {
        $scope.calculatePrize();
        $scope.tournamentObj.tournament_commission_percentage = parseFloat($scope.tournamentObj.tournament_commission_percentage);
        $scope.tournamentObj.max_players = parseInt($scope.tournamentObj.max_players);
        var entry_fee_value = 0;
        if($scope.tournamentObj.entry_type == "Free"){
            $scope.tournamentObj.entry_value = parseInt($scope.tournamentObj.entry_fee);
            entry_fee_value = $scope.tournamentObj.entry_fee;
        }else{
            $scope.tournamentObj.entry_value = parseInt($scope.tournamentObj.entry_value);
            entry_fee_value = $scope.tournamentObj.entry_value;
        }
        var prizeValue = ($scope.tournamentObj.max_players * entry_fee_value);
        $scope.tournamentObj.expected_prize_amount = prizeValue - (prizeValue * $scope.tournamentObj.tournament_commission_percentage / 100);
        console.log($scope.tournamentObj.expected_prize_amount);
    };


    $scope.tournamentObj.tournament_structure = [];
    $scope.AddTournamentRow = function () {
        $scope.tournamentObj.tournament_structure.push({
            round_number: "",
            qualify: ""
        });
    };
    $scope.AddTournamentRow();


    $scope.AddPrizeRow = function () {
        $scope.tournamentObj.prizes_info.push({
            from_rank: "",
            to_rank: "",
            min_prize_value: 0,
            prize_value: 0
        });
    };
    $scope.AddPrizeRow();

    $scope.totalPrize = 0;
    $scope.RemovePrizeRow = function (pos) {
        var newDataList = [];
        for (var i = 0, l = $scope.tournamentObj.prizes_info.length; i < l; i++) {
            if (i !== pos) {
                newDataList.push($scope.tournamentObj.prizes_info[i]);
            }
        }
        $scope.tournamentObj.prizes_info = newDataList;
        $scope.calculatePrize();
    };

    $scope.calculatePrize = function () {
        $scope.totalPrize = 0;
        for (var i = 0; i < $scope.tournamentObj.prizes_info.length; i++) {
            $scope.tournamentObj.prizes_info[i].prize_value = parseInt($scope.tournamentObj.prizes_info[i].prize_value);
            if ($scope.tournamentObj.prizes_info[i].prize_value > 0) {
                $scope.totalPrize += $scope.tournamentObj.prizes_info[i].prize_value;
            }
        }
    }

    $scope.DeleteTournament = function (item) {
        $scope.sweety(item.tournament_title, function () {
            upcomingTournamentsService.delete(item.id).then(function (response) {
                if (response.data.status === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetUpcomingTournaments();
                } else if (response.data.status === "invalid_form") {
                    $scope.g_error = response.data.data;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
});