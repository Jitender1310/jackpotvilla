angular.element("#loginForm").validate({
    rules: {
        username: {
            required: true
        },
        password: {
            required: true,
            minlength: 6,
            maxlength: 32
        }
    }, messages: {
        username: {
            required: "Username is required"
        },
        password: {
            required: "Password is required",
            minlength: "Invaild Password",
            maxlength: "Invaild Password"
        }
    }
});
app.controller("loginCtrl", function ($scope, loginService, $cookies) {
    $scope.showLoader = false;
    $scope.user = {
        username: '',
        password: ''
    };
    $scope.VerifyLogin = function () {
        $scope.login_error = {};
        $scope.error_message = "";
        if (angular.element("#loginForm").valid()) {
            $scope.spinny(true);
            loginService.login($scope.user).then(function (response) {
                $scope.spinny(false);
                if (response.data.status === "valid") {
                    $scope.noty('success', response.data.title, response.data.message);
                    $cookies.put("token", response.data.data.token);
                    location.reload();
                } else if (response.data.status === "invalid_form") {
                    $scope.login_error = response.data.data;
                } else if (response.data.status === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };
});