app.controller("commonCtrl", function ($scope, $timeout) {

    $scope.notyDisplay = false;
    $scope.noty = function (type, title, message) {
        $scope.notyDisplay = true;
        $scope.notyType = type;
        $scope.notyTitle = title;
        $scope.notyMessage = message;
        $timeout(function () {
            $scope.notyDisplay = false;
        }, 3000);
    }
    // Notifications code ends Here ++++++++++++++++++++++++++++++
    // Alertbox code starts Here ++++++++++++++++++++++++++++++
    $scope.sweetyDisplay = false;
    $scope.sweety = function (title, yesFn) {
        $scope.sweetyDisplay = true;
        $scope.sweetyTitle = title;
        $scope.action = function (id) {
            if (id === true) {
                $scope.type = yesFn;
                $scope.type();
                $scope.sweetyDisplay = false;
            } else {
                $scope.sweetyDisplay = false;
            }
        };
    }
    // Alertbox code ends Here ++++++++++++++++++++++++++++++
    // spinner code starts Here ++++++++++++++++++++++++++++++
    $scope.spinny = function (state) {
        $scope.spinnyLoader = false;
        if (state == true) {
            $scope.spinnyLoader = true;
        } else {
            $scope.spinnyLoader = false;
        }
    }
    // spinner code ends Here ++++++++++++++++++++++++++++++
    app.directive("filesInput", function () {
        return {
            require: "ngModel",
            link: function postLink(scope, elem, attrs, ngModel) {
                elem.on("change", function (e) {
                    var files = elem[0].files;
                    ngModel.$setViewValue(files);
                })
            }
        }
    });
    $scope.tags = [{
            text: 'siva.nargana66@gmail.com'
        }, {
            text: 'team@thecolourmoon.com'
        }];

    $scope.convertTagsInputObjectToString = function (obj) {
        console.log(obj);
        var $str = "";
        for (var $i = 0; $i < obj.length; $i++) {
            $str += obj[$i].text + ",";
        }
        $str = $str.slice(0, -1)
        return $str;
    };
    $scope.hasPermission = function (id) {
        for (var i = 0; i < $scope.permission_enabled_modules.length; i++) {
            if ($scope.permission_enabled_modules[i] === id) {
                return true;
            }
        }
    };

    app.directive("selectbox", function () {
        return {
            restrict: 'EA',
            template: "<input type='text' />"
        };
    });
});

