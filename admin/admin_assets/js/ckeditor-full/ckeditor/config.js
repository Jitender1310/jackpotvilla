/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';
    config.height = 400;
    config.removeButtons = 'Underline,Subscript,Superscript,Image,Preview';
    config.extraPlugins = 'panel,listblock,button,base64image,justify,copyformatting,tableresize,panelbutton,colorbutton,font,richcombo,floatpanel,stylescombo'
};
