var freshupdesk = {
    sidebarWorkspace: function () {
        var sidebar = $(".pageSidebar");
        var sidebarBody = $(".SidebarBody");
        var sidebarOffset = $(".offset");
        if (sidebar.length > 0 && sidebarBody.length > 0 && $(window).width() > 768) {
            function sidebarWorkspaceFn() {
                sidebar.each(function () {
                    offset = 0;
                    $(this).find(sidebarOffset).each(function () {
                        offset += $(this).outerHeight();
                    });
                    $(this).find(sidebarBody).css({
                        "min-height": $(this).outerHeight() - offset,
                        "max-height": $(this).outerHeight() - offset,
                        "overflow-y": "auto"
                    });
                });
            }
            $(window).on("load", sidebarWorkspaceFn);
            $(window).on("resize", sidebarWorkspaceFn);
        }
    },
    pageWrapper: function () {
        $(".pageWrapper").each(function () {
            var $this = $(this);
            var $sidebar = $this.find(".pageSidebar");
            if ($sidebar.length > 0) {
                $this.addClass("sidebarIn");
                if ($sidebar.hasClass("right")) {
                    $this.addClass("sidebarRight");
                } else {
                    $this.removeClass("sidebarRight");
                }
            } else {
                $this.removeClass("sidebarIn");
            }
        });
    },
    dataTable: function () {
        var dataTable = $(".data-table");
        if (dataTable.length > 0) {
            var table = dataTable.DataTable({
                responsive: true,
                language: {
                    paginate: {
                        previous: "<i class='fal fa-arrow-left'></i>",
                        next: "<i class='fal fa-arrow-right'></i>",
                    }
                },
                columnDefs: [{
                        targets: 'no-sort',
                        orderable: false
                    }],
                searching: false,
                lengthChange: false
            });
        }
    },
    fit: function () {
        var parent = $(".fit-parent");
        var child = $(".fit-child");
        if (parent.length > 0 && child.length > 0) {
            scale = 0;

            function fitFn() {
                var scale = Math.min(parent.width() / child.width(), parent.height() / child.height());
                child.css({
                    'transform': 'scale(' + scale + ') translate(0px,0px)',
                    'transform-origin': 'left top'
                });
                parent.css({
                    'height': child.height() * scale,
                    'width': child.width() * scale
                });
            }
            $(window).on("load", fitFn);
            $(window).on("resize", fitFn);
        }
    },
    sameWidth: function () {
        var wrapper = $(".table-cell-width");
        var fixedCol = $(".fixed-col");
        var adjustableCol = $(".adjustable-col");
        if (wrapper.length > 0 && adjustableCol.length > 0) {
            function sameWidthFn() {
                wrapper.each(function () {
                    var tableWidth = $(this).outerWidth();
                    var fixedColWidth = 0;
                    $(this).find(fixedCol).each(function () {
                        fixedColWidth += $(this).outerWidth();
                    });
                    var remainingWidth = (tableWidth - fixedColWidth);
                    var adjustableColWidth = (remainingWidth / $(this).find(adjustableCol).length);
                    $(this).find(adjustableCol).each(function () {
                        $(this).outerWidth(adjustableColWidth);
                    });
                });
            }
            $(window).on("load", sameWidthFn);
            $(window).on("resize", sameWidthFn);
        }
    },
    sameHeight: function () {
        var sameHeightRow = $("[same-height-row]");
        var sameHeightCol = $("[same-height-col]");
        if (sameHeightRow.length > 0 && sameHeightCol.length > 0 && $(window).width() > 768) {
            function sameHeightFn() {
                sameHeightRow.each(function () {
                    var max = 0;
                    $(this).find(sameHeightCol).each(function () {
                        if ($(this).outerHeight() > max) {
                            max = $(this).outerHeight();
                        }
                    });
                    $(this).find(sameHeightCol).css({
                        "min-height": max + 1
                    });
                });
            }
            // $(window).on("load", sameHeightFn);
            sameHeightFn();
            $(window).on("resize", sameHeightFn);
        }
    },
    loader: function () {
        var afterLoading = $("[after-loading]");
        var beforeLoading = $("[before-loading]");
        if (afterLoading.length > 0 && beforeLoading.length > 0) {
            setTimeout(function () {
                beforeLoading.removeClass("active").addClass("inactive");
                afterLoading.removeClass("inactive").addClass("active");
            }, 5000);
        }
    },
    valign: function () {
        var valignHolder = $("[valign-holder]");
        var valignParent = $("[valign-parent]");
        if (valignHolder.length > 0 && valignParent.length > 0 && $(window).width() > 768) {
            function valignFn() {
                valignHolder.each(function () {
                    var $this = $(this);
                    var vh_height = $this.outerHeight();
                    var vp_height = $this.closest(valignParent).outerHeight();
                    var vh_position = (vp_height - vh_height) / 2;
                    $this.css({
                        "position": "relative",
                        "top": vh_position
                    });
                });
            }
            $(window).on("load", valignFn);
            $(window).on("resize", valignFn);
        }
    },
    workspace: function () {
        var workspace = $("[workspace]");
        var workspaceOffset = $("[workspace-offset]");
        if (workspace.length > 0 && $(window).width() > 768) {
            function workspaceFn() {
                var offset = 0;
                workspaceOffset.each(function () {
                    offset += $(this).outerHeight();
                });
                workspace.css({
                    "height": $(window).height() - offset
                });
            }
            $(window).on("load", workspaceFn);
            $(window).on("resize", workspaceFn);
        }
    },
    charts: function () {
        if ($("#hero-graph").length > 0) {
            Morris.Line({
                element: 'hero-graph',
                lineColors: ['#fb9678', '#00c292'],
                data: [{
                        period: '2010 Q1',
                        Expense: 2666,
                        Income: null
                    }, {
                        period: '2010 Q2',
                        Expense: 2778,
                        Income: 2294
                    }, {
                        period: '2010 Q3',
                        Expense: 4912,
                        Income: 1969
                    }, {
                        period: '2010 Q4',
                        Expense: 3767,
                        Income: 3597
                    }, {
                        period: '2011 Q1',
                        Expense: 6810,
                        Income: 1914
                    }, {
                        period: '2011 Q2',
                        Expense: 5670,
                        Income: 4293
                    }, {
                        period: '2011 Q3',
                        Expense: 4820,
                        Income: 3795
                    }, {
                        period: '2011 Q4',
                        Expense: 15073,
                        Income: 5967
                    }, {
                        period: '2012 Q1',
                        Expense: 10687,
                        Income: 4460
                    }, {
                        period: '2012 Q2',
                        Expense: 8432,
                        Income: 5713
                    }],
                xkey: 'period',
                ykeys: ['Expense', 'Income'],
                labels: ['Expense', 'Income']
            });
        }
        if ($("#hero-donut").length > 0) {
            Morris.Donut({
                element: 'hero-donut',
                colors: ['#fb9678', '#00c292', '#03a9f3', '#fec107', '#ab8ce4'],
                data: [{
                        label: 'Total',
                        value: 100
                    }, {
                        label: 'Bunker Beds',
                        value: 25
                    }, {
                        label: 'Chaise Lounge',
                        value: 25
                    }, {
                        label: 'Wing Chair',
                        value: 25
                    }, {
                        label: 'Recliner',
                        value: 25
                    }],
                formatter: function (y) {
                    return y
                }
            });
        }
        if ($("#today-accomidation-sales").length > 0) {
            Morris.Donut({
                element: 'today-accomidation-sales',
                colors: ['#fec107', '#03a9f3', '#00c292'],
                data: [{
                        label: 'Cash',
                        value: 100
                    }, {
                        label: 'Card',
                        value: 25
                    }, {
                        label: 'Paytm',
                        value: 25
                    }, ],
                formatter: function (y) {
                    return y
                }
            });
        }
        if ($("#today-fnb-sales").length > 0) {
            Morris.Donut({
                element: 'today-fnb-sales',
                colors: ['#fec107', '#03a9f3', '#00c292'],
                data: [{
                        label: 'Cash',
                        value: 25
                    }, {
                        label: 'Card',
                        value: 100
                    }, {
                        label: 'Paytm',
                        value: 25
                    }, ],
                formatter: function (y) {
                    return y
                }
            });
        }
        if ($("#hero-donut2").length > 0) {
            Morris.Donut({
                element: 'hero-donut2',
                colors: ['#fb9678', '#00c292', '#03a9f3', '#fec107', '#ab8ce4'],
                data: [{
                        label: 'Total',
                        value: 100
                    }, {
                        label: 'Bunker Beds',
                        value: 25
                    }, {
                        label: 'Chaise Lounge',
                        value: 25
                    }, {
                        label: 'Wing Chair',
                        value: 25
                    }, {
                        label: 'Recliner',
                        value: 25
                    }],
                formatter: function (y) {
                    return y
                }
            });
        }
        if ($("#hero-bar").length > 0) {
            Morris.Bar({
                element: 'hero-bar',
                data: [{
                        option: 'Option 1',
                        value: 136
                    }, {
                        option: 'Option 2',
                        value: 137
                    }, {
                        option: 'Option 3',
                        value: 275
                    }, {
                        option: 'Option 4',
                        value: 380
                    }, {
                        option: 'Option 5',
                        value: 655
                    }, {
                        option: 'Option 6',
                        value: 1571
                    }],
                xkey: 'option',
                ykeys: ['value'],
                labels: ['Value'],
                barColors: ["#00c292"],
                barRatio: 0.4,
                hideHover: 'auto'
            });
        }
    }
}
$(document).ready(function () {
    // if ($("#hero-graph").length > 0) {}
    // if ($("#hero-graph").length > 0) {}
    // if ($("#hero-graph").length > 0) {}
    try {
        $(".multipleTags").tagsinput();
    } catch (err) {
    }
});
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})
/*
 $(".spaceType").each(function () {
 var parent = $(this);
 var selected;
 parent.find("input[type=checkbox]").each(function () {
 var child = $(this);
 function checkbox(selector) {
 if (selector.is(":checked")) {
 selector.closest("label").addClass("active");
 $(".whiteboxpanel#" + selector.val()).show();
 } else {
 selector.closest("label").removeClass("active");
 $(".whiteboxpanel#" + selector.val()).hide();
 }
 console.log(selected);
 }
 checkbox(selector = child);
 child.on("click", function () {
 checkbox(selector = child);
 });
 });
 });*/
// Calander Scripts Starts Here
$(document).on("mouseenter", ".HoursCol", function () {
    $("[bookingInfo]").html("<b>Room</b>: " + $(this).data("room") + " | <b>Date</b>: " + $(this).data("date") + " | <b>Hour</b>: " + $(this).data("hour") + ' ' + $(this).data("meridiem"));
}).on("mouseleave", function () {
    $("[bookingInfo]").html("");
});

function resetHours() {
    $(".HoursCol").removeClass("HoursColSelected");
}
$(".HoursRow").bind("mousedown", function (e) {
    e.metaKey = true;
}).selectable({
    filter: ".HoursCol",
    selecting: function () {
        $(".ui-selecting").each(function () {
            $(this).removeClass("ui-selecting");
            $(this).addClass("HoursColSelected");
        });
    },
    stop: function (event, ui) {
        var length = $(".HoursColSelected").length;
        var confirm = false;
        var limit = false;
        var duplicate = false;
        $(".HoursColSelected").each(function (index, element) {
            if ($(this).hasClass('booked')) {
                duplicate = true;
                confirm = false;
                resetHours();
                return false;
            } else if (length < 3) {
                limit = true;
                resetHours();
            } else {
                confirm = true;
                if (index === (length - 1)) {
                    $("[bookingToDay]").html($(this).data('day'));
                    $("[bookingToHour]").html($(this).data('hour'));
                    $("[bookingToMeridiem]").html($(this).data('meridiem'));
                } else if (index === 0) {
                    $("[bookingType]").html($(this).data('type'));
                    $("[bookingRoom]").html($(this).data('room'));
                    $("[bookingFromDay]").html($(this).data('day'));
                    $("[bookingFromHour]").html($(this).data('hour'));
                    $("[bookingFromMeridiem]").html($(this).data('meridiem'));
                }
            }
        });
        if (confirm == true) {
            $("#modal-booking").modal("show");
        } else if (duplicate == true) {
            $("#modal-duplicate").modal("show");
        } else if (limit == true) {
            $("#modal-limit").modal("show");
        }
    }
});
$('#modal-booking').on('hidden.bs.modal', function (e) {
    resetHours();
});


$(".ratingOutput").rateYo({
    starWidth: "16px",
    normalFill: "#ddd",
    ratedFill: "#fc0",
    rating: 3.2,
    readOnly: true
});

function chartsWidth() {
    $(".chart-container").each(function () {
        $(this).find(".chart").css({
            "width": $(this).outerWidth()
        })
    });
}
$(window).on("load", chartsWidth);
$(window).on("resize", chartsWidth);
$(".toggle a").on("click", function (e) {
    e.preventDefault();
    if (!$(this).parent().hasClass("active")) {
        $(this).parent().addClass("active");
        $(".mmenu .inner").css({
            "height": "auto",
            "overflow": "visible"
        });
    } else {
        $(this).parent().removeClass("active");
        $(".mmenu .inner").css({
            "height": "0px",
            "overflow": "hidden"
        });
    }
});

$(".menu ul li").each(function () {
    if ($(this).has("ul").length > 0) {
        $(this).addClass("hasChild");
    } else {
        $(this).removeClass("hasChild");
    }
});
$(document).ready(function () {
    freshupdesk.fit();
    freshupdesk.sameWidth();
    freshupdesk.sameHeight();
    freshupdesk.loader();
    freshupdesk.valign();
    // freshupdesk.workspace();
    freshupdesk.dataTable();
    freshupdesk.sidebarWorkspace();
    freshupdesk.pageWrapper();
    freshupdesk.charts();
});



$('.modal').on('show.bs.modal', function (event) {
    var idx = $('.modal:visible').length;
    $(this).css('z-index', 1040 + (10 * idx));
});
$('.modal').on('shown.bs.modal', function (event) {
    var idx = ($('.modal:visible').length) - 1; // raise backdrop after animation.
    $('.modal-backdrop').not('.stacked').css('z-index', 1039 + (10 * idx));
    $('.modal-backdrop').not('.stacked').addClass('stacked');
});



$(document).on("click", ".notifications-toggle", function (e) {
    e.preventDefault();
    e.stopPropagation();
    $(this).closest(".notifications").toggleClass("active");
});

$(document).on("click", function () {

    $(".notifications").removeClass("active");

});