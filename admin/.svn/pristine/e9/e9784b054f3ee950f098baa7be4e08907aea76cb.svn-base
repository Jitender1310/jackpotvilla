<?php

    class Tournaments_model extends CI_Model {

        private $table_name = "tournaments";

        function add($data) {
            $this->db->trans_begin();
            $data["token"] = $this->generate_tournament_token();
            $prizes_info = $data["prizes_info"];
            unset($data["prizes_info"]);
            $this->db->set($data);
            $this->db->set("created_at", time());
            $response = $this->db->insert($this->table_name);

            if ($response) {
                $inserted_id = $this->db->insert_id();

                $this->add_or_update_prize_distributions($prizes_info, $inserted_id);

                $this->log_model->create_log($this->table_name, __FUNCTION__, $inserted_id);
            }

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        }

        function add_or_update_prize_distributions($prizes_info, $tournaments_id) {
            $this->db->where("tournaments_id", $tournaments_id);
            $this->db->delete("tournaments_prize_distribution_config");
            for ($i = 0, $l = count($prizes_info); $i < $l; $i++) {
                $prizes_info[$i] = (object) $prizes_info[$i];
                $id = $this->get_max_id() + 1;
                $this->db->set("id", $id);
                $this->db->set("from_rank", $prizes_info[$i]->from_rank);
                $this->db->set("to_rank", $prizes_info[$i]->to_rank);
                $this->db->set("prize_value", $prizes_info[$i]->prize_value);
                $this->db->set("min_prize_value", $prizes_info[$i]->min_prize_value);
                $this->db->set("tournaments_id", $tournaments_id);
                $this->db->insert("tournaments_prize_distribution_config");
            }
        }

        function get_max_id() {
            $this->db->select_max("id");
            return $this->db->get("tournaments_prize_distribution_config")->row()->id;
        }

        function update($data, $id) {
            $this->db->trans_begin();
            $prizes_info = $data["prizes_info"];
            unset($data["prizes_info"]);
            $this->db->set($data);
            $this->db->where("id", $id);
            $this->db->set("updated_at", time());
            $this->db->update($this->table_name);

            $this->add_or_update_prize_distributions($prizes_info, $id);
            $this->log_model->create_log($this->table_name, __FUNCTION__, $id);

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        }

        function delete($id) {
            $this->db->where("id", $id);
            $this->db->set("status", 0);
            $this->db->set("updated_at", time());
            $response = $this->db->update($this->table_name);
            if ($response) {
                $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
            }
            return $response;
        }

        function get($filters) {
            $this->db->where("status", 1);
            if ($filters['frequency'] && $filters['frequency'] != '') {
                $this->db->where("frequency", $filters['frequency']);
            }
            if ($filters['tournament_categories_id'] && $filters['tournament_categories_id'] != '') {
                $this->db->where("tournament_categories_id", $filters['tournament_categories_id']);
            }
            if ($filters['premium_category'] && $filters['premium_category'] != '') {
                $this->db->where("premium_category", $filters['premium_category']);
            }
            if (isset($filters['to_date']) && isset($filters['from_date'])) {
//                if ($filters['from_date'] != '') {
//                    $this->db->where("strtotime(registration_start_date) >=", strtotime($filters['from_date']));
//                }
//                $this->db->where("strtotime(registration_start_date) <=", strtotime($filters['to_date']));
                $this->db->where('registration_start_date BETWEEN "' . $filters['from_date'] . '" AND "' . $filters['to_date'] . '"', '', false);
            }

            $this->db->order_by("id", "desc");

            $data = $this->db->get($this->table_name)->result();
            if ($data) {
                foreach ($data as $item) {
                    $item->tournament_category = $this->tournament_categories_model->get_tournament_category_name($item->tournament_categories_id);
                    $item->allowed_club_types_text = [];

                    if ($item->allowed == "All") {
                        $club_types = $this->club_types_model->get();
                        foreach ($club_types as $c_item) {
                            $item->allowed_club_types_text[] = $c_item->name;
                        }
                        $item->allowed_club_types_text = implode(", ", $item->allowed_club_types_text);
                    } else {
                        $selected_club_types = explode(",", $item->allowed_club_types);
                        for ($i = 0; $i < count($selected_club_types); $i++) {
                            $this->db->where("id", $selected_club_types[$i]);
                            $club_types = $this->club_types_model->get();
                            foreach ($club_types as $c_item) {
                                $item->allowed_club_types_text[] = $c_item->name;
                            }
                        }
                        $item->allowed_club_types_text = implode(", ", $item->allowed_club_types_text);
                    }

                    if ($item->registration_start_date) {
                        $item->registration_start_date = convert_date_to_display_format($item->registration_start_date);
                    }

                    if ($item->registration_close_date) {
                        $item->registration_close_date = convert_date_to_display_format($item->registration_close_date);
                    }

                    if ($item->tournament_start_date) {
                        $item->tournament_start_date = convert_date_to_display_format($item->tournament_start_date);
                    }

                    $item->registration_start_time = date("h:i A", strtotime(date("Y-m-d" . " " . $item->registration_start_time)));
                    $item->registration_close_time = date("h:i A", strtotime(date("Y-m-d" . " " . $item->registration_close_time)));
                    $item->tournament_start_time = date("h:i A", strtotime(date("Y-m-d" . " " . $item->tournament_start_time)));

                    $item->created_at = date(DATE_TIME_HUMAN_READABLE_FORMAT, $item->created_at);
                    if ($item->updated_at) {
                        $item->updated_at = date(DATE_TIME_HUMAN_READABLE_FORMAT, $item->updated_at);
                    } else {
                        $item->updated_at = "N/a";
                    }


                    if (!$item->updated_at) {
                        $item->updated_at = "";
                    }
                    if ($item->active) {
                        $item->active_text = "Active";
                        $item->active_bootstrap_css = "text-success";
                    } else {
                        $item->active_text = "Inactive";
                        $item->active_bootstrap_css = "text-danger";
                    }
                    $this->db->where("tournaments_id", $item->id);
                    $item->prizes_info = $this->db->get("tournaments_prize_distribution_config")->result();
                    if ($item->tournament_structure) {
                        $item->tournament_structure = json_decode($item->tournament_structure);
                    } else {
                        $item->tournament_structure = [];
                    }
                }
                return $data;
            } else {
                return [];
            }
        }

        function is_token_exists($token) {
            $this->db->where("access_token", $token);
            return $this->db->get($this->table_name)->num_rows();
        }

        function generate_tournament_token() {
            $tournament_token = generateRandomString(30);
            if ($this->db->get_where($this->table_name, ["token" => $tournament_token])->num_rows() == 0) {
                return $tournament_token;
            } else {
                return $this->generate_tournament_token();
            }
        }

        function get_tournament_structure($tournament_obj) {
            //print_r($tournament_obj);
        }

    }
    