<div class="modal fade" id="tournaments-modal-popup">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{{tournamentObj.id?"Update":"Add"}} Tournaments</h4>
            </div>
            <div class="modal-body">
                <form ng-submit="AddOrUpdateTournaments()" id="tournamentForm " ng-keyup="t_error               =                                   {}; calculateExpectedPrize()">
                    <div class="row">
                        <div class="col-md-12">
                            <span class="text-danger" ng-bind-html="error_message"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Category <span class="text-danger">*</span></label>
                                <div class="custom-input">
                                    <select class="form-control" name="tournament_categories_id" ng-model="tournamentObj.tournament_categories_id" required >
                                        <option value="" selected disabled="">Choose Category</option>
                                        <option  ng-repeat="item in tournamentCategoires track by $index" ng-value="{{item.id}}">{{item.name}}</option>
                                    </select>
                                    <span class="ci-icon">
                                        <i class="fal fa-chevron-down"></i>
                                    </span>
                                </div>
                            </div>
                            <label class="error" ng-if="t_error.game_type">{{t_error.game_type}}</label>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tournament Title <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" required name="tournament_title" ng-model="tournamentObj.tournament_title">
                            </div>
                            <label class="error" ng-if="t_error.tournament_title">{{t_error.tournament_title}}</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Allowed <span class="text-danger">*</span></label>
                                <div class="custom-input">
                                    <select class="form-control" ng-model="tournamentObj.allowed" required name="allowed">
                                        <option value="">Choose</option>
                                        <option>All</option>
                                        <option>For Selected Clubs</option>
                                    </select>
                                    <span class="ci-icon">
                                        <i class="fal fa-chevron-down"></i>
                                    </span>
                                </div>
                            </div>
                            <label class="error" ng-if="t_error.allowed">{{t_error.allowed}}</label>
                        </div>
                        <div class="col-md-6" ng-if="tournamentObj.allowed == 'For Selected Clubs'">
                            <div class="form-group">
                                <label>Club Types <span class="text-danger">*</span></label>
                                <div class="custom-input">
                                    <select class="form-control" ng-model="tournamentObj.allowed_club_types" name="allowed_club_types" multiple="">
                                        <option value="" selected disabled="">Choose</option>
                                        <option ng-repeat="item in clubTypes track by $index" ng-value="item.id">{{item.name}}</option>
                                    </select>
                                    <span class="ci-icon">
                                        <i class="fal fa-chevron-down"></i>
                                    </span>
                                </div>
                            </div>
                            <label class="error" ng-if="t_error.allowed_club_types">{{t_error.allowed_club_types}}</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-line text-left"><span class="h4">Frequency</span></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Frequency <span class="text-danger">*</span></label>
                                <div class="custom-input">
                                    <select class="form-control" ng-model="tournamentObj.frequency" required name="frequency">
                                        <option value="" disabled>Choose</option>
                                        <option>Monthly</option>
                                        <option>Weekly</option>
                                        <option>Daily</option>
                                        <option>One Time</option>
                                    </select>
                                    <span class="ci-icon">
                                        <i class="fal fa-chevron-down"></i>
                                    </span>
                                </div>
                            </div>
                            <label class="error" ng-if="t_error.frequency">{{t_error.frequency}}</label>
                        </div>

                        <div class="col-md-3" ng-if="tournamentObj.frequency == 'Weekly'">
                            <div class="form-group">
                                <label>Week<span class="text-danger">*</span></label>
                                <div class="custom-input">
                                    <select class="form-control" ng-model="tournamentObj.week" required name="week">
                                        <option value="" disabled>Choose</option>
                                        <option ng-repeat="witem in weeks">{{witem}}</option>                                        
                                    </select>
                                    <span class="ci-icon">
                                        <i class="fal fa-chevron-down"></i>
                                    </span>
                                </div>
                            </div>
                            <label class="error" ng-if="t_error.week">{{t_error.week}}</label>
                        </div>

                        <div class="col-md-3" ng-if="tournamentObj.frequency == 'Monthly' || tournamentObj.frequency == 'One Time'">
                            <div class="form-group">
                                <label>Registration Start Date <span class="text-danger">*</span></label>
                                <input type="text" class="form-control datepickerFrom" ng-model="tournamentObj.registration_start_date" name="registration_start_date" required>
                            </div>
                            <label class="error" ng-if="t_error.registration_start_date">{{t_error.registration_start_date}}</label>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Registration Start Time <span class="text-danger">*</span></label>
                                <input type="text" class="form-control timepicker" ng-model="tournamentObj.registration_start_time" name="registration_start_time" required>
                            </div>
                            <label class="error" ng-if="t_error.registration_start_time">{{t_error.registration_start_time}}</label>
                        </div>

                        <div class="col-md-3" ng-if="tournamentObj.frequency == 'Monthly' || tournamentObj.frequency == 'One Time'">
                            <div class="form-group">
                                <label>Registration Closes Date <span class="text-danger">*</span></label>
                                <input type="text" class="form-control datepickerFrom" ng-model="tournamentObj.registration_close_date" name="registration_close_date" required>
                            </div>
                            <label class="error" ng-if="t_error.registration_close_date">{{t_error.registration_close_date}}</label>
                        </div>

                        <div class="col-md-3" >
                            <div class="form-group">
                                <label>Registration Closes Time <span class="text-danger">*</span></label>
                                <input type="text" class="form-control timepicker" ng-model="tournamentObj.registration_close_time" name="registration_close_time" required>
                            </div>
                            <label class="error" ng-if="t_error.registration_close_time">{{t_error.registration_close_time}}</label>
                        </div>

                        <div class="col-md-3" ng-if="tournamentObj.frequency != 'Daily'">
                            <div class="form-group">
                                <label>Tournament Start Date <span class="text-danger">*</span></label>
                                <input type="text" class="form-control datepickerFrom" ng-model="tournamentObj.tournament_start_date" name="tournament_start_date" required>
                            </div>
                            <label class="error" ng-if="t_error.tournament_start_date">{{t_error.tournament_start_date}}</label>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Tournament Start Time <span class="text-danger">*</span></label>
                                <input type="text" class="form-control timepicker" ng-model="tournamentObj.tournament_start_time" name="tournament_start_time" required>
                            </div>
                            <label class="error" ng-if="t_error.tournament_start_date">{{t_error.tournament_start_time}}</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-line text-left"><span class="h4">Entry Fee and Tournament conducting Admin Charge</span></div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Entry Fee Type <span class="text-danger">*</span></label>
                                <div class="custom-input">
                                    <select class="form-control" name="entry_type" required ng-model="tournamentObj.entry_type">
                                        <option value="" selected disabled="">Choose</option>
                                        <option>Cash</option>
                                        <option>Free</option>
                                    </select>
                                    <span class="ci-icon">
                                        <i class="fal fa-chevron-down"></i>
                                    </span>
                                </div>
                            </div>
                            <label class="error" ng-if="t_error.entry_type">{{t_error.entry_type}}</label>
                        </div>

                        <div class="col-md-6" ng-if="tournamentObj.entry_type == 'Free'">
                            <div class="form-group">
                                <label>Entry Fee (Given by Admin for prize calculation only)<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" required ng-model="tournamentObj.entry_fee">
                            </div>
                            <label class="error" ng-if="t_error.entry_fee">{{t_error.entry_fee}}</label>
                        </div>

                        <div class="col-md-6" ng-if="tournamentObj.entry_type == 'Cash'">
                            <div class="form-group">
                                <label>Entry Fee (Cash)<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" required name="entry_value" ng-model="tournamentObj.entry_value">
                            </div>
                            <label class="error" ng-if="t_error.entry_value">{{t_error.entry_value}}</label>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Max Players in Tournament<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" required name="max_players" ng-model="tournamentObj.max_players">
                            </div>
                            <label class="error" ng-if="t_error.max_players">{{t_error.max_players}}</label>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Min Players to Start Tournament<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" required name="min_players_to_start_tournament" ng-model="tournamentObj.min_players_to_start_tournament">
                            </div>
                            <label class="error" ng-if="t_error.min_players_to_start_tournament">{{t_error.min_players_to_start_tournament}}</label>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label>(%) Charge for conducting Tournament <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" ng-model="tournamentObj.tournament_commission_percentage" required>
                            </div>
                            <label class="error" ng-if="t_error.tournament_commission_percentage">{{t_error.tournament_commission_percentage}}</label>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-line text-left"><span class="h4">Prize info</span></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Prize Type <span class="text-danger">*</span></label>
                                <div class="custom-input">
                                    <select class="form-control" name="prize_type" required ng-model="tournamentObj.prize_type">
                                        <option value="" selected disabled="">Choose</option>
                                        <!--<option>Fixed Prize</option>-->
                                        <option>Depend on Players Joined</option>
                                    </select>
                                    <span class="ci-icon">
                                        <i class="fal fa-chevron-down"></i>
                                    </span>
                                </div>
                            </div>
                            <label class="error" ng-if="t_error.entry_type">{{t_error.entry_type}}</label>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Expected Prize Amount<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="expected_prize_amount" ng-model="tournamentObj.expected_prize_amount" required>
                            </div>
                            <label class="error" ng-if="t_error.expected_prize_amount">{{t_error.expected_prize_amount}}</label>
                        </div>
                    </div>

                    <h3 class="hidden">Tournament Structure</h3>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Round</th>
                                            <th>Qualify Per Table</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="p_row in tournamentObj.tournament_structure track by $index">
                                            <td>Round {{$index + 1}}</td>
                                            <td><input type="number" min="1" class="form-control" required ng-model="p_row.qualify"></td>
                                        </tr>
                                        <tr>
                                            <td>Total Rounds : {{tournamentObj.tournament_structure.length}}</td>
                                            <td><button class="btn btn-primary pull-right" type="button" ng-click="AddTournamentRow()">Add Round</button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <h3>Prize Positions</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>From Position</th>
                                            <th>To Position</th>
                                            <th>Minimum Prize</th>
                                            <th>Prize Cash</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="p_row in tournamentObj.prizes_info track by $index">
                                            <td>{{$index + 1}}</td>
                                            <td><input type="number" class="form-control" min="1" required ng-model="p_row.from_rank"></td>
                                            <td><input type="number" class="form-control" min="1" required ng-model="p_row.to_rank"></td>
                                            <td><input type="number" class="form-control"  min="1" required ng-model="p_row.min_prize_value"></td>
                                            <td><input type="number" class="form-control"  min="1" required ng-model="p_row.prize_value"></td>

                                            <td><button type="button" class="btn btn-danger" ng-click="RemovePrizeRow()">Remove</button></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                Expected Prize Amount {{expected_prize_amount}}
                                            </td>
                                            <td>Total Prize</td>
                                            <td></td>
                                            <td>{{totalPrize}}</td>
                                            <td><button class="btn btn-primary pull-right" type="button" ng-click="AddPrizeRow()">Add Row</button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <button class="btn btn-default " ng-click="ResetForm()" data-dismiss="modal"><b>Cancel <i class="fal fa-times"></i></b></button>
                    <button class="btn btn-primary pull-right" type="submit"><b>{{tournamentObj.id?"Update":"Create Tournament"}} <i class="fal fa-arrow-right"></i></b></button>
                </form>

                <br/>
                Note : 
                <p>Estimated duration  : Total Rounds * 20 minutes</p>
                <p>Total Prizes  : (Joined Players * Entry Fee) - Tournament Conduction Charge Percentage</p>
            </div>
        </div>
    </div>
</div>