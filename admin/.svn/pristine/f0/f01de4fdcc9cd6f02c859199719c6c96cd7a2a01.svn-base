<?php

    class Create_real_tournament_model extends CI_Model {

        private $table_name = "tournaments";
        private $table_name2 = "cloned_tournaments";

        function add($data) {
            $this->db->trans_begin();
            unset($data['token']);
            $data["ref_id"] = $this->generate_tournament_ref_id();
            $data["token"] = $this->generate_tournament_token();
            $prizes_info = $data["prizes_info"];
            unset($data["prizes_info"]);
            $this->db->set($data);
            $this->db->set("created_at", time());
            $response = $this->db->insert($this->table_name2);

            if ($response) {
                $inserted_id = $this->db->insert_id();

                $this->add_or_update_prize_distributions($prizes_info, $inserted_id);

                $this->log_model->create_log($this->table_name2, __FUNCTION__, $inserted_id);
            }

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        }

        function add_or_update_prize_distributions($prizes_info, $cloned_tournaments_id) {
            $this->db->where("cloned_tournaments_id", $cloned_tournaments_id);
            $this->db->delete("cloned_tournaments_prize_distribution_config");
            for ($i = 0, $l = count($prizes_info); $i < $l; $i++) {
                $prizes_info[$i] = (object) $prizes_info[$i];
                $id = $this->get_max_id() + 1;
                $this->db->set("id", $id);
                $this->db->set("from_rank", $prizes_info[$i]->from_rank);
                $this->db->set("to_rank", $prizes_info[$i]->to_rank);
                $this->db->set("prize_value", $prizes_info[$i]->prize_value);
                $this->db->set("min_prize_value", $prizes_info[$i]->min_prize_value);
                $this->db->set("cloned_tournaments_id", $cloned_tournaments_id);
                $this->db->insert("cloned_tournaments_prize_distribution_config");
            }
        }

        function get_max_id() {
            $this->db->select_max("id");
            return $this->db->get("cloned_tournaments_prize_distribution_config")->row()->id;
        }

        function update($data, $id) {
            $this->db->trans_begin();
            $prizes_info = $data["prizes_info"];
            unset($data["prizes_info"]);
            $this->db->set($data);
            $this->db->where("id", $id);
            $this->db->set("updated_at", time());
            $this->db->update($this->table_name2);

            $this->add_or_update_prize_distributions($prizes_info, $id);
            $this->log_model->create_log($this->table_name2, __FUNCTION__, $id);

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        }

//        function delete($id) {
//            $this->db->where("id", $id);
//            $this->db->set("status", 0);
//            $this->db->set("updated_at", time());
//            $response = $this->db->update($this->table_name);
//            if ($response) {
//                $this->log_model->create_log($this->table_name, __FUNCTION__, $id);
//            }
//            return $response;
//        }

        function get($tournaments_id) {
            $this->db->where("status", 1);
            $this->db->where("id", $tournaments_id);
            $item = $this->db->get($this->table_name)->row();

            if ($item) {
                $item->tournament_category = $this->tournament_categories_model->get_tournament_category_name($item->tournament_categories_id);
                $item->allowed_club_types_text = [];

                if ($item->allowed == "All") {
                    $club_types = $this->club_types_model->get();
                    foreach ($club_types as $c_item) {
                        $item->allowed_club_types_text[] = $c_item->name;
                    }
                    $item->allowed_club_types_text = implode(", ", $item->allowed_club_types_text);
                } else {
                    $selected_club_types = explode(",", $item->allowed_club_types);
                    for ($i = 0; $i < count($selected_club_types); $i++) {
                        $this->db->where("id", $selected_club_types[$i]);
                        $club_types = $this->club_types_model->get();
                        foreach ($club_types as $c_item) {
                            $item->allowed_club_types_text[] = $c_item->name;
                        }
                    }
                    $item->allowed_club_types_text = implode(", ", $item->allowed_club_types_text);
                }

                if ($item->registration_start_date) {
                    $item->registration_start_date = convert_date_to_display_format($item->registration_start_date);
                }

                if ($item->registration_close_date) {
                    $item->registration_close_date = convert_date_to_display_format($item->registration_close_date);
                }

                if ($item->tournament_start_date) {
                    $item->tournament_start_date = convert_date_to_display_format($item->tournament_start_date);
                }

                $item->registration_start_time = date("h:i A", strtotime(date("Y-m-d" . " " . $item->registration_start_time)));
                $item->registration_close_time = date("h:i A", strtotime(date("Y-m-d" . " " . $item->registration_close_time)));
                $item->tournament_start_time = date("h:i A", strtotime(date("Y-m-d" . " " . $item->tournament_start_time)));

                $item->created_at = date(DATE_TIME_HUMAN_READABLE_FORMAT, $item->created_at);
                if ($item->updated_at) {
                    $item->updated_at = date(DATE_TIME_HUMAN_READABLE_FORMAT, $item->updated_at);
                } else {
                    $item->updated_at = "N/a";
                }


                if (!$item->updated_at) {
                    $item->updated_at = "";
                }
                if ($item->active) {
                    $item->active_text = "Active";
                    $item->active_bootstrap_css = "text-success";
                } else {
                    $item->active_text = "Inactive";
                    $item->active_bootstrap_css = "text-danger";
                }
                $this->db->where("tournaments_id", $item->id);
                $item->prizes_info = $this->db->get("tournaments_prize_distribution_config")->result();
                if ($item->tournament_structure) {
                    $item->tournament_structure = json_decode($item->tournament_structure);
                } else {
                    $item->tournament_structure = [];
                }
                $item->tournaments_id = $item->id;
                unset($item->id);
                return $item;
            } else {
                return [];
            }
        }

        function is_token_exists($token) {
            $this->db->where("access_token", $token);
            return $this->db->get($this->table_name)->num_rows();
        }

        function generate_tournament_ref_id() {
            $tournament_token = rand(11111111, 99999999);
            if ($this->db->get_where($this->table_name2, ["ref_id" => $tournament_token])->num_rows() == 0) {
                return $tournament_token;
            } else {
                return $this->generate_tournament_token();
            }
        }

        function generate_tournament_token() {
            $tournament_token = generateRandomString(30);
            if ($this->db->get_where($this->table_name2, ["token" => $tournament_token])->num_rows() == 0) {
                return $tournament_token;
            } else {
                return $this->generate_tournament_token();
            }
        }

        function get_tournament_structure($tournament_obj) {
            //print_r($tournament_obj);
        }

    }
    