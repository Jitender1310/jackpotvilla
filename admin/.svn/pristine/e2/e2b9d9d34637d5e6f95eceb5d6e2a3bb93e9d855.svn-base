<section class="pageWrapper" ng-controller="stayCategoriesInventoryConfigurationCtrl" ng-init="GetCentersList('<?= get_user_id() ?>');">
    <div class="pageHeader" workspace-offset valign-parent>
        <div class="row">
            <div class="col-md-6"><strong>Stay Categories Inventory List</strong></div>
            <div class="col-md-6">
                <div valign-holder>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="custom-input">
                                <select class="form-control input-sm" ng-change="GetAssignedStayingTypes(inventoryObj.centers_id)" ng-model="inventoryObj.centers_id" ng-disabled="!centersList.length > 0">
                                    <option value="">Choose Center</option>
                                    <option ng-value="item.id" value="item.id" ng-repeat="item in centersList">{{item.center_name}}</option>
                                </select>
                                <span class="ci-icon">
                                    <i ng-show="centersSpinner" class="fal fa-circle-notch fa-spin"></i>
                                    <i ng-show="!centersSpinner" class="fal fa-chevron-down"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4 pl-10-xs">
                            <div class="custom-input">
                                <select class="form-control input-sm" ng-change="GetStayingCategoriesInventoryListForCenter(inventoryObj.centers_id, inventoryObj.staying_types_id); GetStayingTypesCategoriesForCenter(inventoryObj.centers_id, inventoryObj.staying_types_id)" ng-model="inventoryObj.staying_types_id" ng-disabled="!stayingTypesList.length > 0">
                                    <option value="">Choose Type</option>
                                    <option value="{{item.staying_types_id}}" ng-repeat="item in stayingTypesList track by $index">{{item.name}}</option>
                                </select>
                                <span class="ci-icon">
                                    <i ng-show="!stayingTypesSpinner" class="fal fa-chevron-down"></i>
                                    <i ng-show="stayingTypesSpinner" class="fal fa-circle-notch fa-spin"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div valign-holder class="text-right">
                                <button type="button" class="btn btn-primary" ng-click="ShowAddInventoryForm()"><i class="fal fa-plus"></i> Add Inventory</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pageBody" workspace>
        <div class="responsive-table" ng-if="inventoryObj.centers_id && inventoryObj.staying_types_id">
            <table class="table table-custom data-table" datatable="ng" dt-options="dtOptions">
                <thead>
                    <tr>
                        <th width="120">S.No</th>
                        <th>Category</th>
                        <th>Identity Code</th>
                        <th>Availability Status</th>
                        <th>Available From</th>
                        <th class="no-sort text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in stayingCategoriesInventoryList track by $index">
                        <td data-label="SNO">{{$index + 1}}</td>
                        <td data-label="Category">{{item.category_name}}</td>
                        <td data-label="Identity Code">{{item.identity_code}}</td>
                        <td data-label="Availability Status">
                            <span class="text-{{item.availability_status_bootstrap_class}}">{{item.availability_status}}</span>
                        </td>
                        <td data-label="Availability From">
                            {{item.availability_from_date}}
                        </td>
                        <td data-label="Action" class="text-right">
                            <span class="dropdown">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="clear"></div>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li ng-click="EditInventory(item)"><a href="#">Edit</a></li>
                                    <li ng-click="DeleteInventory(item)"><a href="#">Delete</a></li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pageFooter" workspace-offset>&copy; Freshup Desk - 2017</div>

    <div class="modal fade" id="inventory-modal-popup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{inventoryObj.id?'Update':'Add'}} Inventory</h4>
                </div>
                <div class="modal-body">
                    <form ng-submit="AddOrUpdateInventory()" id="inventoryForm">
                        <span class="text-danger" ng-bind-html="error_message"></span>
                        <div class="form-group">
                            <label>Choose Category</label>
                            <select class="form-control" name="staying_type_categories_id" ng-model="inventoryObj.staying_type_categories_id" ng-readonly="!stayingTypeCategories.length > 0">
                                <option value="" selected="">Choose Category</option>
                                <option value="{{item.id}}" ng-repeat="item in stayingTypeCategories track by $index">{{item.category_name}}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Identity Code</label>
                            <input type="text" class="form-control" name="identity_code" ng-model="inventoryObj.identity_code">
                        </div>
                        <div class="form-group">
                            <label>Availability from date (Start date of item)</label>
                            <input type="text" class="form-control datetimepickerAll" autocomplete="ava_f_d" required onkeydown="event.preventDefault()" name="availability_from_date" ng-model="inventoryObj.availability_from_date">
                        </div>
                        <div class="form-group">
                            <label>Availability Status</label>
                            <select class="form-control" name="availability_status" ng-model="inventoryObj.availability_status" ng-change="inventoryObj.comment = ''">
                                <option value="" selected disabled="">Choose Availability Status</option>
                                <option value="Active" >Active</option>
                                <option value="Inactive">Inactive</option>
                            </select>
                        </div>
                        <div class="form-group" ng-show="inventoryObj.availability_status == 'Inactive'">
                            <label>Inactive Reason</label>
                            <textarea class="form-control" ng-model="inventoryObj.comment" ng-required="inventoryObj.availability_status == 'Inactive'"></textarea>
                        </div>
                        <div class="text-right">
                            <button class="btn btn-default" type="button" ng-click="ResetForm()"><b>Cancel <i class="fal fa-times"></i></b></button>
                            <button type="submit" class="btn btn-primary"><b>{{inventoryObj.id?'Update':'Add'}} <i class="fal fa-plus"></i></b></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->

</section>

<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>controllers/stayCategoriesInventoryConfigurationCtrl.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/centersService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/stayingCategoriesService.js?r=<?= time() ?>"></script>
<script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>services/stayingCategoriesInventoryService.js?r=<?= time() ?>"></script>
