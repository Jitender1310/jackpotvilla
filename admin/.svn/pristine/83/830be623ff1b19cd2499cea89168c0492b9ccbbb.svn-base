<?php

class Bots extends REST_Controller {

    private $model_name = "players_model";
    private $title = "Bot";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
        check_for_access(9);
    }

    function index_post() {
        $filters = [];
        
        $filters["search_key"] = $this->input->get_post("search_key");
        $filters["sort_by"] = $this->input->get_post("sort_by");
        $filters["is_bot_players_required"] = 1;
        $filters["from_date"] = $this->post("from_date") ? convert_date_to_db_format($this->post("from_date")) : "";
        $filters["to_date"] = $this->post("to_date") ? convert_date_to_db_format($this->post("to_date")) : "";
        $filters["bot_status"] = $this->post("bot_status") ? $this->post("bot_status") : null;
        $total_results =  $this->{$this->model_name}->get($filters);
        
        $limit = $this->input->get_post("limit") ? $this->input->get_post("limit") : ORDERS_PER_PAGE;
        $_GET['page'] = $this->input->get_post("page");
        $start = $this->input->get_post("page") > 1 ? ($this->input->get_post("page") - 1) * $limit : 0;
        $filters["limit"] = $limit;
        $filters["start"] = $start;
		
	$result["total_results_found"] = $total_results;
        $result["total_pages"] = ceil($total_results/ORDERS_PER_PAGE);
        
        $result["results"] = $this->{$this->model_name}->get($filters);
        
        
        $pagination = my_pagination("players/bots", $limit, $total_results, true);
        $pagination['pagination'] = str_replace("&amp;", "&", $pagination['pagination']);
        $cur_page = ($pagination['pagination_helper']->cur_page != 0) ? ($pagination['pagination_helper']->cur_page - 1) : $pagination['pagination_helper']->cur_page;
        $pagination['initial_id'] = (($cur_page) * $pagination['pagination_helper']->per_page) + 1;
        
        
        $arr = [
            'status' => "valid",
            "message" => $this->title . " list",
            "data" => $result,
            "pagination" => $pagination
        ];
        $arr = json_encode($arr);
        $response_arry = json_decode(str_replace('null', '""', $arr));
        if (json_last_error_msg() == "Syntax error") {
            $response_arry = json_decode(str_replace('"null"', '""', $arr));
        }
        $this->response($response_arry);
    }

    function add_post() {
        if ($this->input->get_post("id")) {
            check_for_access(9, "update");
        } else {
            check_for_access(9, "add");
        }
        $this->form_validation->set_rules("firstname", "Firstname", "required", array(
            'required' => 'Firstname cannot be empty'
        ));

        $this->form_validation->set_rules("lastname", "Lastname", "required", array(
            'required' => 'Lastname cannot be empty'
        ));
        $this->form_validation->set_rules("username", "Username", "required|callback_username_check", array(
            'required' => 'Username cannot be empty'
        ));

        $this->form_validation->set_rules("gender", "Gender", "required", array(
            'required' => 'Please choose your gender'
        ));

        $this->form_validation->set_rules("states_id", "State", "required", array(
            'required' => 'Please choose your state'
        ));


        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "firstname" => form_error("firstname"),
                "lastname" => form_error("lastname"),
                "states_id" => form_error("states_id"),
                "username" => form_error("username"),
            ];
            $arr = [
                "status" => "invalid_form",
                "data" => $errors
            ];
            $this->response($arr);
        }

        $data = [
            "firstname" => $this->post('firstname'),
            "lastname" => $this->post('lastname'),
            "mobile" => "",
            "email" => $this->post('email'),
            "gender" => $this->post('gender'),
            "address_line_1" => "",
            "address_line_2" => "",
            "date_of_birth" => date("Y-m-d"),
            "states_id" => $this->post('states_id'),
            "bot_player" => 1,
            "email_verified" => 1,
            "mobile_verified" => 1,
            "bot_status" => $this->post("bot_status")
        ];


        if ($this->input->post("id")) {
            $data["username"] = $this->input->post('username');
            if ($this->input->post('password')) {
                $data["salt"] = generateRandomString(25);
                $data["password"] = md5($this->input->post('password') . $data["salt"]);
            }
        } else {
            $data["username"] = $this->input->post('username');
            $data["salt"] = generateRandomString(25);
            $data["password"] = md5($this->input->post('password') . $data["salt"]);
        }

        if ($this->input->post("id")) {
            return $this->update($data, $this->post("id"));
        }

        $response = $this->{$this->model_name}->add($data);
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " added successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not added, please try again"
            ];
        }
        $this->response($arr);
    }

    function update($data) {
        check_for_access(9);
        $response = $this->{$this->model_name}->update($data, $this->post('id'));
         if($response==="GAME_IN_PLAY"){
             $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => "Game cannot be deactivated at this time, this bot player is playing this game"
            ];
        }else  if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " updated successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not updated, please try again"
            ];
        }
        $this->response($arr);
    }

    function delete_post() {
        check_for_access(9);
        $id = (int) $this->post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        $this->response($arr);
    }

    function username_check($str) {
        $id = (int) $this->post('id');
        if ($this->{$this->model_name}->is_username_exists_for_adding($str, $id)) {
            $this->form_validation->set_message('username_check', 'The {field} is already existed');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function mobile_check($str) {
        $id = (int) $this->post('id');
        if ($this->{$this->model_name}->is_mobile_exists_for_adding($str, $id)) {
            $this->form_validation->set_message('mobile_check', 'The {field} is already existed');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function email_check($str) {
        $id = (int) $this->post('id');
        if ($this->{$this->model_name}->is_email_exists_for_adding($str, $id)) {
            $this->form_validation->set_message('email_check', 'The {field} is already existed');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}
