<!DOCTYPE html>
<html lang="en" ng-app="frontDesk" ng-cloak>
    <head> 
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= SITE_TITLE ?></title>
        <!-- Bootstrap CSS --> 
        <link rel="shortcut icon" href="<?= STATIC_ADMIN_IMAGES_PATH ?>favicon.png" />
        <link rel="stylesheet" type="text/css" href="<?= STATIC_ADMIN_CSS_PATH ?>bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?= STATIC_ADMIN_CSS_PATH ?>jquery.rateyo.min.css">
        <link rel="stylesheet" type="text/css" href="<?= STATIC_ADMIN_ASSETS_PATH ?>fonts/fontawesome/css/fontawesome-all.min.css">
        <link rel="stylesheet" type="text/css" href="<?= STATIC_ADMIN_CSS_PATH ?>morris.css">
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" type="text/css" href="<?= STATIC_ADMIN_CSS_PATH ?>style.css?i=<?php echo time(); ?>">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script> 
        <![endif]-->
        <!-- jQuery -->
        <script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>jquery.min.js"></script>
        <script type="text/javascript" src="<?= STATIC_ADMIN_JS_PATH ?>jquery.validate.min.js"></script>
        <script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>lib/angular.min.js"></script>
        <script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>lib/angular-base64-upload.min.js"></script>
        <script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>lib/ng-tags-input.min.js"></script>
        <script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>lib/angular-cookies.min.js"></script>
        <script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>lib/angular-datatables.min.js"></script>

        <script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>app.js?r=<?= time() ?>"></script>
        <script type="text/javascript" src="<?= STATIC_ADMIN_ANGULAR_PATH ?>commonCtrl.js?r=<?= time() ?>"></script>

        <script>
            var projectConfig = {
            date_format: 'dd-mm-yy'
            };
        </script>
    </head>
    <body ng-controller="commonCtrl" ng-init="permission_enabled_modules =<?= get_access_enabled_modules_list() ?>;">

        <div class="noty"  data-show="{{notyDisplay}}" ng-class="notyType">
            <i></i>
            <strong>{{notyTitle}}</strong>
            <p ng-bind-html="notyMessage"></p>
        </div>


        <div class="sweetyOverlay" data-show="{{sweetyDisplay}}"></div>
        <div class="sweety" data-show="{{sweetyDisplay}}">
            <div class="sweetyTitle">Are you sure you wish to delete this? <strong>{{sweetyTitle}}</strong></div>
            <button type="button" ng-click="action(true)" class="btn btn-primary">Yes</button>
            <button type="button" ng-click="action(false)" class="btn btn-default">No</button>
        </div>


        <div class="spinny" ng-show="spinnyLoader">
            <div class="spinnyLoader"><i class="fal fa-circle-notch fa-spin"></i></div>
        </div>


        <section class="navigation" workspace-offset>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2 col-xs-8">
                        <a href="<?= base_url() ?>dashboard" class="logo"><?= SITE_TITLE ?> </a>
                    </div>
                    <div class="col-md-7 hidden-sm hidden-xs">
                        <div class="menu">
                            <ul class="ul">
                                <li ng-if="hasPermission(1)"> 
                                    <a href="">Master <i class="fal fa-chevron-down"></i></a>
                                    <ul>
                                        <li><a href="<?= base_url() ?>master/states" ng-if="hasPermission(2)">States</a></li>
                                        <li class="dropdown-submenu" ng-if="hasPermission(18)"><a class="sub" href="#">Prices Configuration</a>
                                            <ul>
                                                <li><a href="<?= base_url() ?>master/price_configuration/regular" ng-if="hasPermission(19)">Regular Prices</a></li>
                                                <li><a href="<?= base_url() ?>master/occasions" ng-if="hasPermission(21)">Occasions</a></li>
                                                <li><a href="<?= base_url() ?>master/price_configuration/occasional" ng-if="hasPermission(20)">Occasional Prices</a></li>
                                                <li><a href="<?= base_url() ?>master/cancellation_charges" ng-if="hasPermission(82)">Cancellation Charges</a></li>
                                            </ul>
                                        </li>

                                        
                                        
                                        <li class="dropdown-submenu" ng-if="hasPermission(4)"><a class="sub" href="#">Roles & Employees</a>
                                            <ul>
                                                <li><a href="<?= base_url() ?>master/roles" ng-if="hasPermission(5)">Roles</a></li>
                                                <li><a href="<?= base_url() ?>master/users" ng-if="hasPermission(6)">Employees</a></li>
                                            </ul>
                                        </li>

                                        <li><a href="<?= base_url() ?>master/modules" ng-if="hasPermission(3)">Modules</a></li>
                                        <li><a href="<?= base_url() ?>master/games" ng-if="hasPermission(10)">Games</a></li>
                                        <li><a href="<?= base_url() ?>master/global_configurations" ng-if="hasPermission(14)">Global Configurations</a></li>
                                        <li><a href="<?= base_url() ?>master/coupons" ng-if="hasPermission(37)">Coupons</a></li>
										
										<li class="dropdown-submenu" ng-if="hasPermission(18)"><a class="sub" href="#">CMS Pages</a>
                                            <ul>
                                                <li><a href="<?= base_url() ?>cms/about_us" ng-if="true">About Us</a></li>
												<li><a href="<?= base_url() ?>cms/privacy_policy" ng-if="true">Privacy Policy</a></li>
												<li><a href="<?= base_url() ?>cms/refund_policy" ng-if="true">Refund Policy</a></li>
												<li><a href="<?= base_url() ?>cms/terms_and_conditions" ng-if="true">Terms and Conditions</a></li>
												<li><a href="<?= base_url() ?>cms/legality" ng-if="true">Legality</a></li>
												<li><a href="<?= base_url() ?>cms/faqs" ng-if="true">FAQs</a></li>
                                            </ul>
                                        </li>
                                        


                                    </ul>
                                </li>

                                <li ng-if="hasPermission(7)">
                                    <a href="">Players <i class="fal fa-chevron-down"></i></a>
                                    <ul>
                                        <li ng-if="hasPermission(7)"><a href="<?= base_url() ?>players/all_players"><i class="fal fa-users mr-10-xs"></i>Real Players</a></li>
                                        <li ng-if="hasPermission(9)"><a href="<?= base_url() ?>players/bot_players"><i class="fal fa-rocket mr-10-xs"></i>All Bots</a></li>
                                        <li ng-if="hasPermission(19)"><a href="<?= base_url() ?>players/default_avatars"><i class="fal fa-images mr-10-xs"></i>Avatars</a></li>
                                        <li ng-if="hasPermission(20)"><a href="<?= base_url() ?>players/deposit_transactions"><i class="fal fa-images mr-10-xs"></i>Deposit Transactions</a></li>
                                    </ul>
                                </li>

                                <li ng-if="hasPermission(11)">
                                    <a href="">Game Play Data <i class="fal fa-chevron-down"></i></a>
                                    <ul>
                                        <li ng-if="hasPermission(12)"><a href="<?= base_url() ?>game_play_information/"><i class="fal fa-gamepad mr-10-xs"></i>Practice Games</a>
                                            <ul>
                                                <li><a href="<?= base_url() ?>master/cms/about_us" ng-if="hasPermission(8)">Points</a></li>
                                                <li><a href="<?= base_url() ?>master/cms/pricing_policy" ng-if="hasPermission(8)">Pool</a></li>
                                                <li><a href="<?= base_url() ?>master/cms/privacy_policy" ng-if="hasPermission(8)">Deal</a></li>
                                            </ul>
                                        </li>
                                        <li ng-if="hasPermission(13)"><a href="<?= base_url() ?>players/all_players"><i class="fal fa-rupee-sign mr-10-xs"></i> <i class="fal fa-gamepad mr-10-xs"></i> Cash Games</a></li>
                                    </ul>
                                </li>
                                <li ng-if="hasPermission(15)">
                                    <a href="">Feedback <i class="fal fa-chevron-down"></i></a>
                                    <ul>
                                        <li ng-if="hasPermission(16)"><a href="<?= base_url() ?>feedback/reported_problems"><i class="fal fa-ticket mr-10-xs"></i> Reported Problems</a></li>
                                    </ul>
                                </li>
                                
                                <li ng-if="hasPermission(17)">
                                    <a href="">Promotion System <i class="fal fa-chevron-down"></i></a>
                                    <ul>
                                        <li ng-if="hasPermission(18)"><a href="<?= base_url() ?>promotions/bonus_system"><i class="fal fa-ticket mr-10-xs"></i> Bonus System</a></li>
                                    </ul>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 hidden-sm hidden-xs">
                        <div class="menu menuRight">
                            <ul class="ul">
                                <!--<li class="icon active"><a href="" data-toggle="tooltip" data-placement="bottom" title="Room View"><i class="fal fa-window-restore"></i></a></li>-->
                                <!--<li class="icon"><a href="<?= base_url() ?>admin/pages/dashboard" data-toggle="tooltip" data-placement="bottom" title="Dashboard"><i class="fal fa-columns"></i></a></li>-->
                                <!--<li class="icon"><a href="<?= base_url() ?>admin/pages/quick_view" data-toggle="tooltip" data-placement="bottom" title="Quick View"><i class="fal fa-th-large"></i></a></li>-->

                                <li ng-if="hasPermission(76)" class="icon notifications " ><a href="<?= base_url() ?>reservations/view_all?selected_type=completed_but_proofs_updation_pending" data-toggle="tooltip" data-placement="bottom" title="Pending Proof updations"><i class="fal fa-desktop"></i> <span class="lbl">{{completed_but_proofs_updation_pending_count}}</span></a></li>
                                <!--                                <li ng-if="hasPermission(75)"  class="icon notifications {{notificationsCount?notificationsCount:'hide'}} ">
                                                                    <a class="notifications-toggle" data-toggle="tooltip" data-placement="bottom" title="Notifications"><i class="fal fa-bell"></i> <span class="lbl {{notificationsCount?notificationsCount:'hide'}}">{{notificationsCount}}</span></a>
                                
                                                                    <div class="notifications-menu" >
                                                                        <a class="unread" href="" ng-repeat="items in notificationsList">{{items.notification}} at {{items.time}} </a>
                                                                    </div>
                                                                </li>-->



                                <li ng-if="hasPermission(77)" class="icon"><a href="<?= base_url() ?>reservations/reservation_availability" data-toggle="tooltip" data-placement="bottom" title="Reservation Availability"><i class="fal fa-calendar"></i></a></li>
                                <li>
                                    <a href=""><?= $this->user_model->get_user_info($this->session->userdata("user_id"))->fullname; ?> <i class="fal fa-chevron-down"></i></a>
                                    <ul class="right">
                                        <li><a href="<?= base_url() ?>change_password"><i class="fal fa-key mr-10-xs"></i>Change Password</a></li>
                                        <li class="divider"></li>
                                        <li><a href="<?= base_url() ?>logout"><i class="fal fa-sign-out-alt mr-10-xs"></i>Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-4 hidden-lg hidden-md visible-sm visible-xs">
                        <div class="menu menuRight">
                            <ul class="ul">
                                <li class="icon toggle"><a href=""><i class="fal fa-bars"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>