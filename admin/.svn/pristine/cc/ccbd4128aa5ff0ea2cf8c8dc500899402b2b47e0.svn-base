angular.element("#corporate_form").validate({
    errorPlacement: function (error, element) {
        error.appendTo(element.closest(".form-group"));
    },
    rules: {
        countries_id: {
            required: true
        },
        states_id: {
            required: true
        },
        cities_id: {
            required: true
        },
        locations_id: {
            required: true
        },
        corporate_office_name: {
            required: true,
            letterswithbasicpunc: true
        },
        email: {
            required: true
        },
        mobile: {
            required: true
        },
        address: {
            required: true
        },
        gst_number: {
            required: true,
            minlength: 15,
            maxlength: 15
        },
        office_status: {
            required: true,
        }
    }, messages: {
        countries_id: {
            required: "Country is required"
        },
        states_id: {
            required: "State is required"
        },
        cities_id: {
            required: "City is required"
        },
        corporate_office_name: {
            required: "Corporate Office name is required",
            letterswithbasicpunc: "Corporate Office name should be alphabets only"
        },
        address: {
            required: "Corporate Office address is required"
        },
        gst_number: {
            required: "GST Number is mandatory",
            minlength: "Invalid GST Number",
            maxlength: "Invalid GST Number"
        },
        office_status: {
            required: "Status is Required",
        }
    }
});


app.controller("corporateOfficesCtrl", function ($scope, corporateOfficesService, locationsService, citiesService, countriesService, statesService, DTOptionsBuilder, loaderService) {
    $scope.showLoader = false;
    function initializeObject() {
        $scope.corporateOfficesObj = {
            countries_id: '',
            states_id: '',
            cities_id: '',
            locations_id: '',
            corporate_office_name: '',
            address: '',
            email: '',
            mobile: '',
            gst_number: '',
            office_status: '',
        };
    }
    initializeObject();
    $scope.ResetForm = function () {
        initializeObject();
    };

    $scope.GetCountriesList = function () {
        countriesService.get().then(function (response) {
            $scope.countriesList = response.data.data;
        });
    };
    $scope.GetCountriesList();

    $scope.stateSpinner = false;
    $scope.GetStatesList = function (countries_id) {
        if (countries_id === null) {
            return;
        }
        $scope.stateSpinner = true;
        statesService.getByCountryId(countries_id).then(function (response) {
            $scope.statesList = response.data.data;
            $scope.stateSpinner = false;
        });
    };

    $scope.citySpinner = false;
    $scope.GetCitiesList = function (states_id) {
        if (states_id === null) {
            return;
        }
        $scope.citySpinner = true;
        citiesService.getByStateId(states_id).then(function (response) {
            $scope.citiesList = response.data.data;
            $scope.citySpinner = false;
        });
    };

    $scope.AddOrUpdateCorporateOffice = function () {

        $scope.error_message = "";
        if (angular.element("#corporate_form").valid()) {
            $scope.spinny(true);
            corporateOfficesService.save($scope.corporateOfficesObj).then(function (response) {
                $scope.spinny(false);
                if (response.data.err_code === "valid") {
                    angular.element("#corporate_office_form").modal("hide");
                    $scope.noty('success', response.data.title, response.data.message);
                    initializeObject();
                    $scope.GetCorporateOfficesList();
                } else if (response.data.err_code === "invalid_form") {
                    $scope.error_message = response.data.message;
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        }
    };

    $scope.GetCorporateOfficesList = function () {
        $scope.spinny(true);
        corporateOfficesService.get().then(function (response) {
            $scope.spinny(false);
            $scope.corporateOfficesList = response.data.data;
        });
    };

    $scope.GetLocationsList = function (cities_id) {
        if (cities_id === null) {
            return;
        }
        $scope.locationSpinner = true;
        locationsService.getLocationsByCitiesId(cities_id).then(function (response) {
            $scope.locationSpinner = false;
            $scope.locationsList = response.data.data;
        });
    };


    $scope.EditCorporateOffice = function (obj) {
        $scope.GetStatesList(obj.countries_id);
        $scope.GetCitiesList(obj.states_id);
        $scope.GetLocationsList(obj.cities_id);
        $scope.corporateOfficesObj = angular.copy(obj);
        angular.element("#corporate_office_form").modal({backdrop: 'static',
            keyboard: false});
    };



    $scope.DeleteCorporateOffice = function (item) {
        $scope.sweety(item.corporate_office_name, function () {
            corporateOfficesService.delete(item.id).then(function (response) {
                if (response.data.err_code === "valid") {
                    $scope.noty('success', 'Deleted', response.data.message);
                    $scope.GetCorporateOfficesList();
                } else if (response.data.err_code === "invalid") {
                    $scope.error_message = response.data.message;
                }
            });
        });
    };
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(10)
            .withOption('bLengthChange', false)
            .withOption('searching', true);

    $scope.ShowCorporateOfficeAddForm = function () {
        initializeObject();
        angular.element("#corporate_office_form").modal({backdrop: 'static',
            keyboard: false});
    };

});