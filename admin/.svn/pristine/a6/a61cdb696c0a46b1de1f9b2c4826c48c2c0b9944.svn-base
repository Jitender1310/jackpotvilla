<?php

/**
  @  Helper functions
  @ By Manikanta
 */
defined('BASEPATH') OR exit('No direct script access allowed');

function get_option($opname) {
    $utility = new Utility();
    return $utility->get_option($opname);
}

function get_admin_menu() {
    $utility = new Utility();
    return $utility->get_admin_menu();
}

function get_last_login() {
    $m = load_instance();
    return $m->get_last_login();
}

function my_header_location($uri) {
    echo "<script>location.href='" . site_url($uri) . "'</script>";
    redirect($uri);
}

function get_user_id() {
    $ci = & get_instance();
    return $ci->session->userdata('user_id');
}

function get_user_role_id() {
    $ci = & get_instance();
    $user_id = $ci->session->userdata('user_id');
    if ($user_id) {
        $ci->db->where('id', $user_id);
        return $ci->db->get('users')->row()->role_id;
    }
}

function convert_date_to_db_format($datetime, $from_date_format = null) {
    if (!strpos($datetime, "-")) {
        return;
    }
    try {
        if ($from_date_format) {
            $date_obj = date_create_from_format($from_date_format, $datetime);
        } else {
            $date_obj = date_create_from_format(DATE_FORMAT, $datetime);
        }
        $db_date = date_format($date_obj, 'Y-m-d');
        return $db_date;
    } catch (Exception $ex) {
        
    }
}

function convert_date_time_to_db_format($datetime, $from_date_format = null) {
    if (!strpos($datetime, "-")) {
        return;
    }
    try {
        if ($from_date_format) {
            $date_obj = date_create_from_format($from_date_format, $datetime);
        } else {
            $date_obj = date_create_from_format(DATE_TIME_HUMAN_READABLE_FORMAT, $datetime);
        }
        $db_date = date_format($date_obj, 'Y-m-d H:i:s');
        return $db_date;
    } catch (Exception $ex) {
        
    }
}

function convert_time_db_format($time, $from_date_format = null) {

    $datetime = date("d-m-Y ") . $time;
    try {
        if ($from_date_format) {
            $date_obj = date_create_from_format($from_date_format, $datetime);
        } else {
            $date_obj = date_create_from_format(DATE_TIME_HUMAN_READABLE_FORMAT, $datetime);
        }
        $db_date = date_format($date_obj, 'H:i:s');
        return $db_date;
    } catch (Exception $ex) {
        
    }
}

function convert_date_to_display_format($datetime, $to_date_format = null) {
    if ($to_date_format) {
        $date_obj = date_create_from_format($to_date_format, $datetime);
    } else {
        $date_obj = date_create_from_format("Y-m-d", $datetime);
    }
    if (isset($date_obj) && !is_bool($date_obj)) {
        $display_date = date_format($date_obj, DATE_FORMAT);
        return $display_date;
    }
    return "";
}

function convert_date_time_to_display_format($datetime, $to_date_format = null) {
    if ($to_date_format) {
        $date_obj = date_create_from_format($to_date_format, $datetime);
    } else {
        $date_obj = date_create_from_format("Y-m-d H:i:s", $datetime);
    }
    $display_date = date_format($date_obj, DATE_TIME_HUMAN_READABLE_FORMAT);
    return $display_date;
}

//For time prefix
function ordinal($number) {
    $ends = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
    if ((($number % 100) >= 11) && (($number % 100) <= 13))
        return $number . 'th';
    else
        return $number . $ends[$number % 10];
}


function calculate_rounds($sturcture_obj, $round_number, $number_of_players) {
    $round_qualify_info = $sturcture_obj;
    //print_r($round_qualify_info);
    $number_of_tables = ceil($number_of_players / 6);
    $qualify = 1;
    $round_number++;
    foreach ($round_qualify_info as $item) {
        if ($item->round_number == $round_number) {
            $qualify = $item->qualify;
            break;
        }
    }

    $qualified_persons = $qualify * $number_of_tables;

    return (object) [
                "round_number" => $round_number,
                "qualified" => $qualified_persons,
                "players_text" => $number_of_players . " Players on " . $number_of_tables . " Table(s)",
                "qualifiction_text" => $qualify . " player(s) per table",
                "deals_text" => "1 x players on table"
    ];
}

function get_round_titles($rounds_arr) {
    $r = 0;
    for ($i = count($rounds_arr) - 1, $l = 0; $i >= $l; $i--) {
        if ($r == 0) {
            $rounds_arr[$i]->round_title = "Final";
        } else if ($r == 1) {
            $rounds_arr[$i]->round_title = "Semi Final";
        } else if ($r == 2) {
            $rounds_arr[$i]->round_title = "Quarter  Final";
        } else {
            $rounds_arr[$i]->round_title = "";
        }
        $r++;
    }
    return array_reverse($rounds_arr);
}
