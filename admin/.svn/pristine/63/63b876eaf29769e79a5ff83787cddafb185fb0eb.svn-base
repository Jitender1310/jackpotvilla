<?php

class States extends REST_Controller {

    private $model_name = "states_model";
    private $title = "States";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
    }

    function index_get() {
        $data = $this->{$this->model_name}->get();
        $arr = [
            'status' => "valid",
            'title' => "",
            "message" => $this->title . " list",
            "data" => $data
        ];
        $this->response($arr);
    }

    function add_post() {
        if ($this->input->get_post("id")) {
            check_for_access(2, "update");
        } else {
            check_for_access(2, "add");
        }
        $this->form_validation->set_rules("state_name", "State Name", "required|callback_state_name_check", array(
            'required' => 'Username cannot be empty'
        ));
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "state_name" => form_error("state_name")
            ];
            $arr = [
                "status" => "invalid_form",
                "data" => $errors
            ];
            $this->response($arr);
            die;
        } else {

            $data = [
                "state_name" => $this->post('state_name')
            ];

            if ($this->input->get_post("id")) {
                return $this->update($data, $this->post("id"));
            }
            $response = $this->{$this->model_name}->add($data);
            if ($response) {
                $arr = [
                    'status' => "valid",
                    'title' => "",
                    "message" => singular($this->title) . " added successfully"
                ];
            } else {
                $arr = [
                    'status' => "invalid",
                    'title' => "",
                    "message" => singular($this->title) . " not added, please try again"
                ];
            }
            $this->response($arr);
        }
    }

    function update($data) {
        check_for_access(2);
        $response = $this->{$this->model_name}->update($data, $this->post('id'));
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " updated successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . "  not updated, please try again"
            ];
        }
        $this->response($arr);
    }

    function delete_post() {
        check_for_access(10);
        $id = (int) $this->post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'status' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'status' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        $this->response($arr);
    }

    function state_name_check($str) {
        $id = (int) $this->post('id');
        if ($this->{$this->model_name}->is_state_name_exists($str, $id)) {
            $this->form_validation->set_message('state_name_check', 'The {field} is already existed');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}
