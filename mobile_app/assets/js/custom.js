$.get('assets/images/icons.svg', function(svg) {
    $(".loadicons").html(svg);
}, 'text');

new Swiper('.promotions-slider', {
	loop: true,
    slidesPerView: 1,
     autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
               pagination: {
        el: '.promotions-slider-pagination',
        clickable: true,
      },
       navigation: {
        nextEl: '.promotions-slider-right',
        prevEl: '.promotions-slider-left',
      }

});

new Swiper('.testimonial-slider', {
  loop: true,
     autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
       navigation: {
        nextEl: '.testimonial-slider-right',
        prevEl: '.testimonial-slider-left',
      }

});